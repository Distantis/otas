<?
set_time_limit(0);
 //ID de la tabla hotel que representa a CTS
$id_cts = array(1134,1138,3229,4300,4301,4302,4303,4304);
$id_europa = array(1,3,4,5,6);

function PerteneceCTS($id_empresa){
	global $id_cts;
	$id_cts = array(1134,1138,3229,4300,4301,4302,4303,4304);
	return in_array($id_empresa,$id_cts);
}

function PerteneceEuropa($id_cont){
	global $id_europa;
	$id_europa = array(1,3,4,5,6);
	return in_array($id_cont,$id_europa);
}
function validaFecha($fecha='', $formato='Y-m-d'){
	if($fecha!=''){
		$fec = new DateTime($fecha);
		return $fec->format($formato);
	}else{
		return 'InvalidDate-Control:validaFecha';
	}
}
// JGaete
function ValorTransporteConMarkupEspecial($db1, $tra_codigo, $tra_pas1, $tra_pas2){
	// echo "Entrando";
	$sql="	SELECT round(neto / ((100-mkup)/100), 2) AS precioventa
			FROM serv_markup_especial
			WHERE codigo = ".$tra_codigo." 
			AND pax_i = ".$tra_pas1." AND pax_f = ".$tra_pas2;
	
	$trans = $db1->SelectLimit($sql) or die("Control - ValorTransporteConMarkupEspecial : ".__LINE__." ".$db1->ErrorMsg());

	$cantRows = $trans->RecordCount();
	
	if($cantRows>0){
		$trans->MoveFirst();
		return $trans->Fields("precioventa");
	}else{
		return 0;
	}
}
//FUNCION revisa servicios nulos para mail generaMail_servicios
function TieneServAnu($db1, $id_cot){
		$rows = 0;
		$query = 'SELECT DISTINCT 
		  COUNT(*) AS conteo
		FROM
		  cotpas c 
		  INNER JOIN pais p 
		    ON c.id_pais = p.id_pais 
		  INNER JOIN cotser s 
		    ON s.id_cotpas = c.id_cotpas 
		    AND s.id_cot = c.id_cot 
		  INNER JOIN trans t 
		    ON t.id_trans = s.id_trans 
		  INNER JOIN ciudad cc 
		    ON cc.id_ciudad = t.id_ciudad 
		  INNER JOIN mon m 
		    ON m.id_mon = t.id_mon 
		WHERE c.id_cot = '.$id_cot.' 
		  AND c.cp_estado = 0 
		  AND s.cs_estado = 1';
		$response =$db1->SelectLimit($query) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		$rows = $response->Fields('conteo');
		return $rows;
}
// FIN mail servicios
 
function ConsultaCotizacion($db1,$id_cot){
	global $id_cts;
	$query_cot = "SELECT *,
			DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
			DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta,
			DATE_FORMAT(c.cot_fecdesde, '%Y-%m-%d') as cot_fecdesde1,
			DATE_FORMAT(c.cot_fechasta, '%Y-%m-%d') as cot_fechasta1,
			DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y 23:59:59') as cot_fecanula,
			YEAR(c.cot_fecdesde) as anod, MONTH(c.cot_fecdesde) as mesd, DAY(c.cot_fecdesde) as diad,
			YEAR(c.cot_fechasta) as anoh, MONTH(c.cot_fechasta) as mesh, DAY(c.cot_fechasta) as diah,
			DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fecd,
			DATE_FORMAT(c.cot_fec, '%H:%i:%s') as cot_fech,
			if(c.id_operador not in (".implode(",",$id_cts).") , o.hot_comhot , h.hot_comhot) as comhot,
			if(c.id_opcts is null,c.id_operador,c.id_opcts) as id_mmt,
			if(c.id_opcts is null,o.hot_comesp,h.hot_comesp) as hot_comesp,
			if(c.id_opcts is null,o.id_continente,h.id_continente) as id_cont,
			IF(c.id_opcts IS NULL,o.id_grupo,IF(h.id_grupo IS NULL, o.id_grupo, h.id_grupo)) AS id_grupo,
			if(c.id_opcts is null,o.id_ciudad,h.id_ciudad) as op_ciudad,
			if(c.id_opcts is null,o.hot_diasrestriccion_conf,h.hot_diasrestriccion_conf) as hot_diasrestriccion_conf,
			o.hot_nombre as op,
			h.hot_nombre as op2,
			now() as ahora,
			c.cot_numpas as cot_numpas,
			c.id_tipopack as id_tipopack,
			c.id_area as id_area
		FROM cot c
		INNER JOIN seg s ON s.id_seg = c.id_seg
		INNER JOIN hotel o ON o.id_hotel = c.id_operador
		LEFT JOIN hotel h ON h.id_hotel = c.id_opcts
		INNER JOIN tipopack tp ON tp.id_tipopack = c.id_tipopack WHERE c.id_cot = ".GetSQLValueString($id_cot,"int");
	$cot = $db1->SelectLimit($query_cot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo $query_cot;
	return $cot;
}

function ConsultaDestinos($db1,$id_cot,$join_hotel){
	$query_destinos = "SELECT *,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%Y') as ano1,DATE_FORMAT(c.cd_fecdesde, '%m') as mes1,DATE_FORMAT(c.cd_fecdesde, '%d') as dia1,
		DATE_FORMAT(c.cd_fechasta, '%Y') as ano2,DATE_FORMAT(c.cd_fechasta, '%m') as mes2,DATE_FORMAT(c.cd_fechasta, '%d') as dia2,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		c.id_hotel as id_hotel2,
		c.id_ciudad as id_ciudad,c.cd_hab1,c.cd_hab2,c.cd_hab3,c.cd_hab4 
	FROM cotdes c
	LEFT JOIN seg s ON s.id_seg = c.id_seg";
	if($join_hotel){
		$query_destinos.= " INNER JOIN hotel h ON h.id_hotel = c.id_hotel
							INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion";
	}else{
		$query_destinos.= " LEFT JOIN hotel h ON h.id_hotel = c.id_hotel";
	}
	$query_destinos.= " INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN hotdet hd ON c.id_hotdet = hd.id_hotdet
	LEFT JOIN tipotarifa tt ON hd.id_tipotarifa = tt.id_tipotarifa
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 and id_cotdespadre = 0";
	// if  ($_SESSION['id'] == 3839){
	// echo $query_destinos."<hr>";
	// }
 
    
	$destinos = $db1->SelectLimit($query_destinos) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos;
}

function ConsultaNoxAdi($db1,$id_cot,$id_cotdes,$confirmadas=true){
	$query_nochead = "SELECT *,
		DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		DATEDIFF(cd_fecdesde,cd_fechasta) as dias FROM cotdes 
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND id_cotdespadre = ".GetSQLValueString($id_cotdes,"int")." AND cd_estado = 0";
		if($confirmadas)$query_nochead.=" and id_seg=7";
	$nochead = $db1->SelectLimit($query_nochead) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo $query_nochead."<hr>";
	return $nochead;
}

function ConsultaNoxAdiXcot($db1,$id_cot,$confirmadas=true){
	$query_destinos2 = "SELECT c.*,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		h.id_hotel as hot_padre, h.id_tipohabitacion as th_padre
	FROM cotdes c
	LEFT JOIN cotdes h ON h.id_cotdes = c.id_cotdespadre
	WHERE c.id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 AND c.id_cotdespadre !=0";
	if($confirmadas)$query_destinos2.=" and c.id_seg=7";
	$destinos2 = $db1->SelectLimit($query_destinos2) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos2;
}

function ConsultaPackDetalle($db1,$id_packdetalle){
	$hq = "SELECT p.*, h1.hot_nombre as hot_nombre1, h2.hot_nombre as hot_nombre2, h3.hot_nombre as hot_nombre3, h4.hot_nombre as hot_nombre4,
				h1.hot_diasrestriccion_conf as hot_diasrestriccion_conf1 ,h2.hot_diasrestriccion_conf as hot_diasrestriccion_conf2 , h3.hot_diasrestriccion_conf as hot_diasrestriccion_conf3 , h4.hot_diasrestriccion_conf as hot_diasrestriccion_conf4 ,
				h1.hot_direccion as hot_direccion1,
				h2.hot_direccion as hot_direccion2,
				h3.hot_direccion as hot_direccion3,
				h4.hot_direccion as hot_direccion4
		FROM packdetalle p
		LEFT JOIN hotel h1 ON p.id_hotel1 = h1.id_hotel
		LEFT JOIN hotel h2 ON p.id_hotel2 = h2.id_hotel
		LEFT JOIN hotel h3 ON p.id_hotel3 = h3.id_hotel
		LEFT JOIN hotel h4 ON p.id_hotel4 = h4.id_hotel
		WHERE p.id_packdetalle = ".GetSQLValueString($id_packdetalle,"int");
	//echo $hq."<hr>";
	$h = $db1->SelectLimit($hq) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $h;
}

function ConsultaPack($db1, $id_pack){
	$query_pack = "SELECT *,
		(p.pac_cantdes-1) as cantdes,
		DATE_FORMAT(p.pac_fecdesde,'%d-%m-%Y') as pac_fecdesde1,
		DATE_FORMAT(p.pac_fechasta,'%d-%m-%Y') as pac_fechasta1,
		DATE_FORMAT(p.pac_fecdesde,'%d') as idia,
		DATE_FORMAT(p.pac_fecdesde,'%m') as imes,
		DATE_FORMAT(p.pac_fecdesde,'%Y') as iano,
		DATE_FORMAT(p.pac_fechasta,'%d') as fdia,
		DATE_FORMAT(p.pac_fechasta,'%m') as fmes,
		DATE_FORMAT(p.pac_fechasta,'%Y') as fano
	FROM pack as p
	LEFT JOIN packdet d ON p.id_pack = d.id_pack and d.pd_estado = 0
	WHERE p.id_pack = ".GetSQLValueString($id_pack,"int");
	
	//echo $query_pack."<br>";
	$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pack;
}

function PuedeModificar($db1,$id_cot){
	$mod_sql = 'SELECT DATE_FORMAT(ha_hotanula,"%Y-%m-%d %H:%m:%s") AS anulacion, DATE_FORMAT(NOW(),"%Y-%m-%d %H:%m:%s") as ahora, id_seg FROM cot WHERE id_cot = '.GetSQLValueString($id_cot,"int");
	$mod = $db1->SelectLimit($mod_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$fec1 = new DateTime($mod->Fields('ahora'));
	$fec2 = new DateTime($mod->Fields('anulacion'));
	if($fec1<$fec2 && $mod->Fields('id_seg')!=13){
		return true;
	}else{
		return false;	
	}
}

function ValidarValorTransportes($id_cot,$id_continente){
	global $db1,$cot,$markup_trans,$opcomtrapro,$opcomtra,$opcomexc,$opcomhtl,$opcomnie,$opcomesp;
	
	$query_pasajeros = "
		SELECT *, 
				c.id_pais as id_pais,
				DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCION__." : ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
	
	
	while(!$pasajeros->EOF){
		$id_cotpas = $pasajeros->Fields('id_cotpas');
		$servicios = ConsultaServiciosXcotpas($db1,$id_cotpas);
		
		while (!$servicios->EOF) {
			$id_cotser = $servicios->Fields('id_cotser');
			$id_ttagrupa = $servicios->Fields('id_ttagrupa');
			$cs_fecped = $servicios->Fields('cs_fecped');
				
			$trans_array[$id_cotpas][$cs_fecped][$id_ttagrupa] = $id_cotser;
				
			$servicios->MoveNext();
		}
		$pasajeros->MoveNext();
	}
	$pasajeros->MoveFirst();
	
	if(isset($trans_array)){
		foreach($trans_array as $id_cotpas1=>$a1){
			foreach($trans_array as $id_cotpas2=>$a2){
				if($id_cotpas1!=$id_cotpas2){
					foreach($a1 as $cs_fecped1=>$b1){
						foreach($a2 as $cs_fecped2=>$b2){
							foreach($b1 as $id_ttagrupa1=>$id_cotser1){
								foreach($b2 as $id_ttagrupa2=>$id_cotser2){
									if(($cs_fecped1==$cs_fecped2)and($id_ttagrupa1==$id_ttagrupa2)){
										$serv_iguales[$id_ttagrupa1][$cs_fecped1][$id_cotpas1]=$id_cotser1;
										$serv_iguales[$id_ttagrupa2][$cs_fecped2][$id_cotpas2]=$id_cotser2;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(count($serv_iguales)>0){
			foreach($serv_iguales as $id_ttagrupa=>$datos1){
				foreach($datos1 as $cs_fecped=>$datos2){
					$count = count($datos2);
					$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = $id_ttagrupa and tra_pas1 <= $count and tra_pas2 >= $count";
					$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
					
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser in (".implode(",",$datos2).") and id_cot=".GetSQLValueString($id_cot, "int")." and DATE_FORMAT(cs_fecped, '%d-%m-%Y') = '".$cs_fecped."'";
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
		}
		
		$sql_trans1 = "SELECT cs.id_cotser,tr.id_ttagrupa,cs.id_trans,tr.tra_codigo,count(*) as cant, tra_markup
			FROM cotser as cs
			INNER JOIN trans as tr ON tr.id_trans = cs.id_trans
			WHERE cs.id_cot = ".GetSQLValueString($id_cot, "int")." and cs.cs_estado = 0 GROUP BY cs.id_trans, cs.cs_fecped";
		$trans1 = $db1->Execute($sql_trans1) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
		
		while(!$trans1->EOF){
			if($trans1->Fields('cant')==1){
				$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = ".$trans1->Fields('id_ttagrupa')." and tra_pas1 <= 1 and tra_pas2 >= 1";
				$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				
				if($id_trans->RecordCount()>0){
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}else{
					$update_id_trans="UPDATE cotser SET cs_estado = 1 WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
			$trans1->MoveNext();
		}
		
		$id_trans = ConsultaServiciosXcot($db1,$id_cot);
		
		while(!$id_trans->EOF){
			if(PerteneceEuropa($id_continente)){
				$cs_valor = round($id_trans->Fields('tra_valor2')/(1-($id_trans->Fields('tra_markup')/100)),2);
			}else{
					
					// if($id_trans->Fields('id_trans')==2595||$id_trans->Fields('id_trans')==493){
					if($id_trans->Fields('tra_codigoprocesado')==0){
						$trans_neto = $id_trans->Fields('tra_valor');
					}else{
						$valTransporte= ValorTransporteConMarkupEspecial($db1, $id_trans->Fields('tra_codigo'), $id_trans->Fields('tra_pas1'), $id_trans->Fields('tra_pas2'));
						if($valTransporte<>0)
							$trans_neto=$valTransporte;
						else
							$trans_neto = round($id_trans->Fields('tra_valor2')/(1-($id_trans->Fields('tra_markup')/100)),2);
					}
					/*						
					if(($id_trans->Fields('id_trans')==2595)
							||($id_trans->Fields('tra_codigo')==19542)
							||($id_trans->Fields('tra_codigo')==19543)
							||($id_trans->Fields('tra_codigo')==19587)
							||($id_trans->Fields('tra_codigo')==19589)
						)
						$trans_neto = $id_trans->Fields('tra_valor');
					else
						$trans_neto = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
					*/					
				
				if($id_trans->Fields('id_tipotrans') == 15){
					// echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$opcomtrapro;
					
					$ser_trans2 = ($trans_neto*$opcomtrapro)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$trans_neto."-".$opcomtrapro."-".$cs_valor."<br>";
					//$cs_valor = round($trans_neto/(1+($opcomtrapro/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == 0){
					$ser_trans2 = ($trans_neto*$opcomtra)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomtra/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == $cot->Fields('id_mmt')){
					$ser_trans2 = ($trans_neto*$opcomesp)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomesp/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 14){
					$ser_trans2 = ($trans_neto*$opcomnie)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomnie/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 4){
					$ser_trans2 = ($trans_neto*$opcomexc)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomexc/100)),1);
				}
			}
			
			$update_id_trans="UPDATE cotser SET cs_valor = ".GetSQLValueString($cs_valor, "double")." WHERE id_cotser = ".$id_trans->Fields('id_cotser');
			$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
			$id_trans->MoveNext();
		}
	}
}

function CalcularTransTotal($db1,$id_cot){
	$cs_valorSQL = "SELECT SUM(cs_valor) as cs_valor FROM cotser WHERE cs_estado = 0 and id_cot = ".GetSQLValueString($id_cot,"int");
	$cs_valor = $db1->SelectLimit($cs_valorSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $cs_valor->Fields('cs_valor');
}

function CalcularValorCot($db1,$id_cot,$guardar,$estado){
	$valorcs=0;
	$valorhc2=0;
	
	$query_cotser1 = "SELECT *
					FROM cotser
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$cotser1 = $db1->SelectLimit($query_cotser1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$cotser1->EOF){
		$valorcs+= $cotser1->Fields('cs_valor');
		$cotser1->MoveNext();
	}
	$query_hotocu1 = "SELECT *
					FROM hotocu
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND hc_estado = $estado";
	$hotocu1 = $db1->SelectLimit($query_hotocu1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$hotocu1->EOF){
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor1');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor2');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor3');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor4');
		$hotocu1->MoveNext();
	}
	if(count($valorhc)>0){
		foreach($valorhc as $id_cotdes=>$cd_valor){
			if($guardar){
				$query = sprintf("update cotdes set cd_valor=%s where id_cotdes=%s",GetSQLValueString($cd_valor, "double"),GetSQLValueString($id_cotdes, "int"));
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$valorhc2+=$cd_valor;
		}
	}
	$valor_tot = $valorcs+$valorhc2;

	if($guardar){
		$query = sprintf("update cot set cot_valor=%s where id_cot=%s",GetSQLValueString($valor_tot, "double"),GetSQLValueString($id_cot, "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	return $valor_tot;
}

function InsertarLog($db1,$id_cot,$id_accion,$id_user){
	$log_q = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", GetSQLValueString($id_user, "int"), GetSQLValueString($id_accion, "int"), GetSQLValueString($id_cot, "int"));					
	$log = $db1->Execute($log_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
}

function OperadoresActivos($db1){
	$query_opcts = "SELECT * FROM hotel WHERE id_tipousuario = 3 and hot_activo = 0 and hot_estado = 0 and id_area = 1 ORDER BY hot_nombre";
	//echo $query_opcts;
	$rsOpcts = $db1->SelectLimit($query_opcts) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsOpcts;
}

function Categorias($db1){
	$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
	$categoria = $db1->SelectLimit($query_categoria) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $categoria;
}

function ConsultaServiciosXcotdes($db1,$id_cotdes,$groupby=false){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped";
	
	if($groupby)$query_servicios.=", count(*) as cuenta, sum(c.cs_valor) as cs_valor";
	$query_servicios.=" FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0";
	if($groupby)$query_servicios.=" GROUP BY c.id_trans,c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}
function ConsultaServiciosXcot($db1,$id_cot){

	require_once('/var/www/distantis/clases/cotizacion.php');
	return cotizacion::getMeServsByCot($db1,$id_cot);
}

function ConsultaServXcotdesGrupo($db1,$id_cotdes){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,count(*) as cuenta
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0 GROUP BY c.id_trans";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServiciosXcotpas($db1,$id_cotpas){
	$query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
			FROM cotser c 
			INNER JOIN trans t on t.id_trans = c.id_trans
			WHERE c.cs_estado=0 and c.id_cotpas = ".GetSQLValueString($id_cotpas,"int")."
			ORDER BY c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaListaTransportes($db1,$id_operador,$id_ciudad=96,$fec_entrada=NULL,$fec_salida=NULL){
	$query_tipotrans = "SELECT * 
		FROM trans t
		INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
		WHERE tra_estado = 0 AND i.tpt_estado = 0 and t.id_hotel IN (0,$id_operador) and t.id_area = 1 and t.id_ciudad = $id_ciudad";
	if(isset($fec_entrada) and isset($fec_salida))$query_tipotrans.= " and t.tra_facdesde <= '$fec_entrada' and t.tra_fachasta >= '$fec_salida'";
	$query_tipotrans.= " GROUP BY id_ttagrupa ORDER BY tra_orderhot desc, tpt_nombre";
	$rstipotrans = $db1->SelectLimit($query_tipotrans) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $rstipotrans;
}

function ConsultaPasajeros($db1,$id_cot){
	$query_pasajeros = "
		SELECT *,c.id_pais as id_pais,DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pasajeros;
}

function MarkupPrograma($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuppro');
}

function MarkupHoteleria($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	//echo $q_cont."<br>";
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuphtl');
}

function MarkupTransporte($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
		
		//echo $q_cont;
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuptra');
}

//Funcion para obtener la comision del operador segun el id_comdet y el id_grupo
function ComisionOP($db1,$id_comdet,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE id_comdet = $id_comdet and id_grupo = $id_grupo";
	// 	echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

//Funcion para obtener la comision del operador segun el tipo de comision y el id_grupo
function ComisionOP2($db1,$com_tipo,$id_grupo){
	//echo $id_grupo;
	$com_op_q = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
	//echo $_SESSION['id_grupo'];
	//echo $com_op_q;
	//die();
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

function ordenar_matriz_nxn($m,$ordenar,$direccion) {
	if(count($m)==0) return $m;
	// 	$m //es vuestra matriz
	// 	$ordenar //es el campo a ordenar
	// 	$direccion //aquí podéis elegir de forma ASC o DESC

	usort($m, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	//usort($m, create_function('$item1, $item2', 'return strtoupper($item1[\'destacado\']) ' . ($direccion === 'asc' ? '>' : '<') . ' strtoupper($item2[\'destacado\']);'));
	
	return $m;
}

function ordenar_matriz_multidimensionada($m) {
    if(count($m)==0) return $m;
    $ordenar='valor';
    $direccion = 'ASC';
    for($x=0; $x<count($m);$x++){
        $matriz[$x]['num']=$m[$x][0]['num'];
        $matriz[$x]['valor']=$m[$x][0]['valorsss'];
    }
	
	usort($matriz, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	
	for($x=0; $x<count($m);$x++){
		$matrizFinal[$x]=$m[$matriz[$x]['num']]; 
	}
	
	return $matrizFinal;
}

//funcion para reporte de disponibilidad (FELIPE) -> Modif Juan Gaete - 21-03-2014 - Se agrega detalleHotel para mostrar reporte por hotel con detalle de tarifas
function rptDispo($db1, $txt_f1, $txt_f2, $ciudad=0, $cat=0,$hotel=0,$area,$ttarifa='',$debugar=false,$debugq=false, $detalleHotel=false){
	
	$sql = "SELECT
	h.id_hotel,
	  h.hot_nombre,
	  ciu.ciu_nombre,
	  hd.id_hotdet,
	  hd.hd_fecdesde,
	  hd.hd_fechasta,
	  hd.id_area,
	  th.th_nombre,
	  tt.tt_nombre,
	  hd.hd_sgl,
	  hd.hd_dbl,
	  hd.hd_tpl,
	  a.area_nombre,
	  m.mon_nombre,
	  IFNULL(ocu.hc_fecha, '-') AS hc_fecha,
	  dispo.sc_fecha,
	  IFNULL(dispo.sc_hab1, 0) AS sc_hab1,
	  IFNULL(dispo.sc_hab2, 0) AS sc_hab2,
	  IFNULL(dispo.sc_hab3, 0) AS sc_hab3,
	  IFNULL(dispo.sc_hab4, 0) AS sc_hab4,  
	  IFNULL(ocu.hc_hab1, 0) AS hc_hab1,
	  IFNULL(ocu.hc_hab2, 0) AS hc_hab2,
	  IFNULL(ocu.hc_hab3, 0) AS hc_hab3,
	  IFNULL(ocu.hc_hab4, 0) AS hc_hab4
	FROM hotdet hd
	INNER JOIN hotel h ON h.id_hotel = hd.id_hotel 
	INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
	INNER JOIN tipotarifa tt ON tt.id_tipotarifa = hd.id_tipotarifa
	INNER JOIN area a ON a.id_area = hd.id_area
	INNER JOIN mon m ON m.id_mon = hd.hd_mon
	INNER JOIN ciudad ciu on h.id_ciudad = ciu.id_ciudad
	INNER JOIN
	    (SELECT
	        id_hotdet,
	        sc_fecha,
	        SUM(sc_hab1) AS sc_hab1, SUM(sc_hab2) AS sc_hab2,SUM(sc_hab3) AS sc_hab3,SUM(sc_hab4) AS sc_hab4
	      FROM stock
	      WHERE
	        sc_fecha BETWEEN '$txt_f1' AND '$txt_f2' 
	      AND  sc_estado = 0
	      GROUP BY id_hotdet, sc_fecha) dispo ON dispo.id_hotdet = hd.id_hotdet
	LEFT JOIN 
	    (SELECT
	        id_hotdet,
	        hc_fecha,
	        SUM(hc_hab1) AS hc_hab1, SUM(hc_hab2) AS hc_hab2,SUM(hc_hab3) AS hc_hab3,SUM(hc_hab4) AS hc_hab4
	      FROM hotocu
	      WHERE hc_estado = 0  and hc_fecha BETWEEN '$txt_f1' AND '$txt_f2' 
	      GROUP BY id_hotdet, hc_fecha) ocu ON ocu.id_hotdet = hd.id_hotdet AND ocu.hc_fecha = dispo.sc_fecha
	WHERE
	1=1    
	AND hd.hd_estado = 0 
	AND h.hot_estado = 0 
    AND h.hot_activo = 0 ";
	if($ttarifa!='') $sql.= " AND hd.id_tipotarifa IN ($ttarifa) ";
	if($hotel!= 0) $sql.= " AND h.id_hotel = $hotel";
	if($ciudad!=0) $sql.= " AND h.id_ciudad = $ciudad";
	if($area== 1) $sql.= " AND hd.id_area = $area";
	if($area== 2) $sql.= " AND hd.id_area = $area";
	if($cat!=0) $sql.= " AND h.id_cat = $cat";	
	
	if($detalleHotel) //JG 21-03-2014
		$sql.= " ORDER BY hd.id_hotdet, sc_fecha";
	else
		$sql.= " ORDER BY h.id_ciudad, h.hot_nombre, sc_fecha";
		
	if($debugq){
		die("<pre>".$sql."</pre>");
	}
	//echo $sql."<br><br>";
	$rpt = $db1->SelectLimit($sql) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_listado1 = $rpt->RecordCount();
	if($debugar){
		debugger($totalRows_listado1);
		$row=0;
	}

	while(!$rpt->EOF){
		$row++;
		if ($debugar){echo $row."<br>";};

		$hotel=$rpt->Fields('id_hotel');
		$hotdet=$rpt->Fields('id_hotdet');
		$hotnom=$rpt->Fields('hot_nombre');
		$fecha=$rpt->Fields('sc_fecha');
		$hdfec1=$rpt->Fields('hd_fecdesde');
		$hdfec2=$rpt->Fields('hd_fechasta');
		$hotdet=$rpt->Fields('id_hotdet');
		$tipo=$rpt->Fields('id_tipotarifa');
		$area=$rpt->Fields('id_area');
		$areanom=$rpt->Fields('area_nombre');
		$tarnom=$rpt->Fields('th_nombre');
		$tartip=$rpt->Fields('tt_nombre');
		$ciunom=$rpt->Fields('ciu_nombre');
		$ciudad=$rpt->Fields('id_ciudad');

		if(($rpt->Fields('sc_hab1')*1)<0){
			$dispsin= 0;	
		}else{
			$dispsin= $rpt->Fields('sc_hab1')*1;	
		}
		if(($rpt->Fields('sc_hab3')*1)<0){
			$dispDob=0;
		}else{
			$dispDob=$rpt->Fields('sc_hab3')*1;
		}
		if(($rpt->Fields('sc_hab2')*1)<0){
			$dispTwin=0;
		}else{
			$dispTwin=$rpt->Fields('sc_hab2')*1;
		}
		if(($rpt->Fields('sc_hab4')*1)<0){
			$dispTrip=0;
		}else{
			$dispTrip=$rpt->Fields('sc_hab4')*1;
		}

		if(($rpt->Fields('hc_hab1')*1)<0){
			$ocusin= 0;	
		}else{
			$ocusin= $rpt->Fields('hc_hab1')*1;	
		}
		if(($rpt->Fields('hc_hab3')*1)<0){
			$ocuDob=0;
		}else{
			$ocuDob=$rpt->Fields('hc_hab3')*1;
		}
		if(($rpt->Fields('hc_hab2')*1)<0){
			$ocuTwin=0;
		}else{
			$ocuTwin=$rpt->Fields('hc_hab2')*1;
		}
		if(($rpt->Fields('hc_hab4')*1)<0){
			$ocuTrip=0;
		}else{
			$ocuTrip=$rpt->Fields('hc_hab4')*1;
		}
		

		$tipo=$rpt->Fields('id_tipotarifa');
		if($detalleHotel){ //JG 21-03-2014
			// $arreglo["hotnomm"] = $hotnom; 
			$arreglo[$hotdet][$fecha]['id_hotel'] = $hotel;
			$arreglo[$hotdet][$fecha]['tt_nombre'] = $tartip;
			$arreglo[$hotdet][$fecha]['th_nombre'] = $tarnom;
			$arreglo[$hotdet][$fecha]['area'] = $areanom;
			$arreglo[$hotdet][$fecha]['dispsin'] += $dispsin;
			$arreglo[$hotdet][$fecha]['dispDob'] += $dispDob;
			$arreglo[$hotdet][$fecha]['dispTwin'] += $dispTwin;
			$arreglo[$hotdet][$fecha]['dispTrip']+= $dispTrip;
			$arreglo[$hotdet][$fecha]['ocusin']+= $ocusin;
			$arreglo[$hotdet][$fecha]['ocudob ']+= $ocuDob ;
			$arreglo[$hotdet][$fecha]['ocutwin'] += $ocuTwin;
			$arreglo[$hotdet][$fecha]['ocutrip']+= $ocuTrip;
		}else{
			$arreglo[$hotel][$fecha]['hotnom'] = $hotnom;
			$arreglo[$hotel][$fecha]['id_hotel'] = $hotel;
			$arreglo[$hotel][$fecha]['dispsin'] += $dispsin;
			$arreglo[$hotel][$fecha]['dispDob'] += $dispDob;
			$arreglo[$hotel][$fecha]['dispTwin'] += $dispTwin;
			$arreglo[$hotel][$fecha]['dispTrip']+= $dispTrip;
			$arreglo[$hotel][$fecha]['ocusin']+= $ocusin;
			$arreglo[$hotel][$fecha]['ocudob ']+= $ocuDob ;
			$arreglo[$hotel][$fecha]['ocutwin'] += $ocuTwin;
			$arreglo[$hotel][$fecha]['ocutrip']+= $ocuTrip;
		}
		$rpt->MoveNext();
	}

	if($debugar2){
		echo "debugging<br>";
		echo "<pre>";
		var_dump($arreglo);
		echo "</pre>";
	}
	return $arreglo;
}

//Funcion para obtener la disponibilidad
function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin , $doble , $triple, $comercial,$id_pack,$codigo=null,$max_sgl=0,$max_twin=0,$max_mat=0,$max_tpl=0,$ciudad=0){
	$condtarifa= " AND id_tipotarifa in (2,4,9,16,17,25,30,31,32,41,43,49,95,119,120,121,139,160,161,162,163,164,165,166,168)";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,4,9,16,17,25,30,31,32,41,43,49,95,119,120,121,139,160,161,162,163,164,165,166,168) ";

	///////////////////////////////////////////////
	////////////markup dinamico parte 1////////////
	///////////////////////////////////////////////
	
	$idcot=0;
	if(isset($_GET['id_cot'])){
		$idcot = $_GET['id_cot'];
	}
	if(isset($_POST['id_cot'])){
		$idcot = $_POST['id_cot'];
	}

	$mkopehot = array();
	//preguntamos datos de operador de cot, mk imperativo y continente
	$sqlopeinfo = "SELECT IFNULL(id_opcts, id_operador) AS ope, 
		IFNULL(op2.id_continente, op.id_continente) AS  id_cont,
		IFNULL(op2.mark_ope_imp, op.mark_ope_imp) AS mkimp,
		IFNULL(op2.mark_operador, op.mark_operador) AS mkope,
		c.id_area
		FROM cot c INNER JOIN hotel op ON c.id_operador = op.id_hotel LEFT JOIN hotel op2 ON c.id_opcts = op2.id_hotel
		WHERE id_cot = ".$idcot;
	$opeinfo = $db1->SelectLimit($sqlopeinfo) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	$esimpe = false;
	$mkope = 1;
	//si el markup general de operador no es cero, preguntamos si es imperativo y flagueamos; dejamos en una variable aparte el markup general de operador
	if($opeinfo->Fields('mkope')!=0){
		if($opeinfo->Fields('mkimp')==0){
			$esimpe = true;
		}
		$mkope = $opeinfo->Fields('mkope');
	}

	//si el markup de operador es 1 (desactivado equivalente a 0), preguntamos por el markup de continente al que pertence el operador y lo guardamos en markup por default
	if($mkope==1){
		$mkdefault = 1;
		$sqlmkcont = "SELECT o.cont_markuphtl
			FROM hotel h 
			INNER JOIN cont o ON h.id_continente = o.id_cont 
			WHERE h.id_hotel = ".$opeinfo->Fields('ope')." AND cont_estado = 0 AND o.area_cont = ".$opeinfo->Fields('id_area');
		$resmkcont = $db1->SelectLimit($sqlmkcont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

		if($resmkcont->RecordCount()>0){
			$mkdefault = $resmkcont->Fields('cont_markuphtl');
		}
		//si no encuentra markup de contienente, preguntamos por el de area y lo guardamos en markup por default.
		if($mkdefault==1){
			$sqlmkarea = "SELECT area_markup FROM area where id_area = ".$opeinfo->Fields('id_area');
			$resmkarea =  $db1->SelectLimit($sqlmkarea) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			$mkdefault = $resmkarea->Fields('area_markup');
		}
	}
	/////////////////////////////////////////////////
	///////////fin markup dinamico parte 1///////////
	/////////////////////////////////////////////////



	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
	
	if($codigo == ''){
		$codigo2 = 0;
	}else{
		$codigo2 = $codigo;
	}
		
	if(isset($id_pack)){

		
		$query_pack = "select id_tipotarifa,pac_n,id_tipoprog from pack where id_pack = ".$id_pack;
		$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
		if($pack->Fields('id_tipoprog')== 1){
		
			if($pack->Fields('id_tipotarifa') == 0){
				$condtarifa= " AND id_tipotarifa in (2,127,128,129,130,131,132,133,4,140,141,142,143,144)";
			}
			if($pack->Fields('id_tipotarifa') == 2){
				//echo "entro tipo 2";
				//echo $id_pack;
	 			if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (2)";
				if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (133)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (132)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (127)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (128)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (129)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (130)";
				if($pack->Fields('pac_n') == 8) $condtarifa= " AND id_tipotarifa in (131)";
				
				if($id_pack == "190")  $condtarifa= " AND id_tipotarifa in (135)";
				if($id_pack == "620") $condtarifa= " AND id_tipotarifa in (151)";
				//$condtarifa= " AND id_tipotarifa in (2,127,128,129,130,131,132,133)";
				
				//echo "<br>".$id_pack."<br>";
				//echo $pack->Fields('id_tipotarifa')."<br>";
				//echo $pack->Fields('pac_n')."<br>";
				//echo $condtarifa;
			}
			if($pack->Fields('id_tipotarifa') == 4){
				//echo "entro";
				if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (4)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (119,140)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (120,141)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (121,142)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (143)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (144, 160)";
			}
		
			if($pack->Fields('id_tipotarifa') == 9){
				//echo "entro";
				//if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (4)";
				if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (153)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (140)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (141)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (142)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (143)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (144)";
			}

		}
		
		if($pack->Fields('id_tipoprog')== 2){
		
			if($pack->Fields('id_tipotarifa') == 2){
				//echo "entro tipo 2";
				//echo $id_pack;
	 			if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (2)";
				if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (133)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (132)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (127)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (128)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (129)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (130)";
				if($pack->Fields('pac_n') == 8) $condtarifa= " AND id_tipotarifa in (131)";

			}
			
			if($pack->Fields('id_tipotarifa') == 4){
				//echo "entro";
				if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (4)";
				if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (119)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (120,140)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (121,141)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (142)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (143)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (144)";
			}
			
			if($pack->Fields('id_tipotarifa') == 9){
				//echo "entro";
				//if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (4)";
				//if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (119)";
				if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (153)";
				if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (140)";
				if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (141)";
				if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (142)";
				if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (143)";
				if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (144)";
			}
		}  
		if($pack->Fields('id_tipoprog')== 4){
			//echo "entro2";
			if($pack->Fields('id_tipotarifa') == 0){
				$condtarifa= " AND id_tipotarifa in (2)";
			}
			if($pack->Fields('id_tipotarifa') == 2){
			   // echo "entro 3";
				$condtarifa= " AND id_tipotarifa in (2)";
			}

			if($pack->Fields('id_tipotarifa') == 4){
				$condtarifa= " AND id_tipotarifa in (2)";
			}		
		}
		
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}

	$condciu ="";
	if($ciudad!=0){
		$condciu = " AND hot.id_ciudad = ".$ciudad;
	}
	$condin = " AND (ifnull(FIND_IN_SET(0,id_cliente),0)<>0 OR ifnull(FIND_IN_SET($codigo2 , id_cliente),0)<>0) ";
	$condex = " AND IFNULL(FIND_IN_SET($codigo2 , id_excluye),0)=0 ";

	$condtarifa = str_replace('id_tipotarifa', 'a.id_tipotarifa', $condtarifa);
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
		FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox,hot.`hot_destacado`, hot.mark_hotel, tt.es_promo
			FROM     hotdet a
    		INNER JOIN stock b ON a.id_hotdet = b.id_hotdet
    		INNER JOIN hotel hot ON a.id_hotel = hot.id_hotel 
			INNER JOIN tipotarifa tt ON a.id_tipotarifa = tt.id_tipotarifa
			WHERE a.id_hotdet=b.id_hotdet 
				AND b.sc_fecha>='$fechaInicio' 
				AND b.sc_fecha<='$fechaFin'
				AND hot.hot_barco = 0
				AND hot.hot_activo = 0
				AND hot.hot_estado = 0
				$condin
				$condex
				AND a.max_sgl >= $max_sgl
				AND a.max_dbl >= $max_twin
				AND a.max_dbl >= $max_mat
				AND a.max_tpl >= $max_tpl
				AND a.id_area = 1
				and b.sc_cerrado = 0
				AND hot.id_hotel=a.id_hotel 
				$condciu
				AND ((tt.es_promo = 1 AND tt_minnoche = DATEDIFF('$fechaFin','$fechaInicio'))
					OR
					(tt.es_promo = 0 AND tt_minnoche <= DATEDIFF('$fechaFin','$fechaInicio')))
				$condtarifa
				AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
		LEFT JOIN 
			(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
				FROM hotocu 
				WHERE hc_fecha>='$fechaInicio' 
					AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
		ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha) ORDER BY hot_destacado DESC, t1.hd_sgl ASC";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());


	if ($_SESSION["id"] == 2444){

		echo $sqlDisp."<br><br>";

	}

   

	$totalRows_listado1 = $disp->RecordCount();
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		$es_promo = $disp->Fields('es_promo');
		if(isset($id_pack)){
			$es_promo = 0;
		}


		$tipohab = buscar_habitacion_global($db1,$grupo);

		if($tipohab=="")
			$tipohab = $grupo;


		$singlexdoble=0;
		//echo $disp->Fields('sc_hab1')."---".($disp->Fields('ocuhab1')*1)."<br>";
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		//echo $stocksingle."<br>";
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		//echo $stocksingle."<br>";
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;


		//echo $hotel."<=>".$hotdet."<=> triple : ".$stocktriple."<br>";

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		

		///////////////////////////////////////////////
		////////////markup dinamico parte 2////////////
		///////////////////////////////////////////////
		$keeplooking = true;
		//si el markup de operador no es imperativo o no lo encontró empezamos a recorrer los eslabones superiores de gerarquia
		if(!$esimpe){
			//markup especifico de operador por tarifa.
			$sqlmkhdesp = "SELECT hbmk_markup_sgl, hbmk_markup_dbt, hbmk_markup_dbm, hbmk_markup_tpl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet = ".$hotdet." 
				AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." AND id_mkgrupo IS NULL AND '".$fecha."' 
				BETWEEN hbmk_fecdesde AND hbmk_fechasta";
			$resmkhdesp = $db1->SelectLimit($sqlmkhdesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			//si encuentra markup especifico, setea el arreglo de markups y cambia el flag a falso para que deje de buscar en el resto de eslabones
			if($resmkhdesp->RecordCount()>0){
				$mkused['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
				$mkused['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
				$mkused['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
				$mkused['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
				$keeplooking = false;
			}
			if($keeplooking){
				//si no ha sido seteado previamente el hotel, busca markup especifico de operador por hotel
				if(!isset($mkopehot[$fecha][$hotel])){    
					$sqlmkhotesp = "SELECT hbmk_markup_sgl, hbmk_markup_dbt, hbmk_markup_dbm, hbmk_markup_tpl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet IS NULL AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." 
						AND id_mkgrupo IS NULL AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
					$resmkhdesp = $db1->SelectLimit($sqlmkhotesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
					//si encuentra markup especifico, setea el arreglo de markups y cambia el flag a falso para que deje de buscar en el resto de eslabones
					if($resmkhdesp->RecordCount()>0){
						$mkused['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
						$mkused['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
						$mkused['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
						$mkused['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
						//agrega el markup para ese día y ese hotel a un arreglo para no volver a ejecutar la query si se pregunta denuevo por ese hotel y ese día
						$mkopehot[$fecha][$hotel]['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
						$mkopehot[$fecha][$hotel]['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
						$mkopehot[$fecha][$hotel]['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
						$mkopehot[$fecha][$hotel]['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
						$keeplooking = false;
					}
				//si ya esta seteado, ocupa lo que está y marca el flag a falso para que deje de buscar en el resto de eslabones
				}else{
					$mkused['sgl'] = $mkopehot[$fecha][$hotel]['sgl'];
					$mkused['dbt'] = $mkopehot[$fecha][$hotel]['dbt'];
					$mkused['dbm'] = $mkopehot[$fecha][$hotel]['dbm'];
					$mkused['tpl'] = $mkopehot[$fecha][$hotel]['tpl'];
					$keeplooking = false;
				}
			}
			if($keeplooking){
				//markup especifico de grupo por tarifa
				$sqlmkgrupoesp = "SELECT hbmk_markup_sgl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet = ".$hotdet." AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." 
					AND id_mkgrupo IS NOT NULL AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
				$resmkgrupoesp = $db1->SelectLimit($sqlmkgrupoesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				//si encuentra un grupo en el que esta el operador y la tarifa, setea el arreglo de markup y cambia el flag a falso para que deje de buscar en el resto de eslabones
				if($resmkgrupoesp->RecordCount()>0){
					$mkused['sgl'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['dbt'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['dbm'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['tpl'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$keeplooking = false;
				}
			}

			if($keeplooking){
				//markup especifico por grupo
				if(!isset($mkopehot[$fecha][$hotel])){
					$sqlmkgrupo = "SELECT hbmk_markup_sgl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet IS NULL AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." AND id_mkgrupo IS NOT NULL 
					AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
					$resmkgrupo = $db1->SelectLimit($sqlmkgrupo) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
					//si encuentra un grupo en el que esta el operador y el hotel, setea el arreglo de markup y cambia el flag a falso para que deje de buscar en el resto de eslabones
					if($resmkgrupo->RecordCount()>0){
						$mkused['sgl'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['dbt'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['dbm'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['tpl'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						//agrega el markup para ese día y ese hotel a un arreglo para no volver a ejecutar la query si se pregunta denuevo por ese hotel y ese día
						$mkopehot[$fecha][$hotel]['sgl'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['dbt'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['dbm'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['tpl'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$keeplooking = false;
					}
				}
			}
			//si no encontró nada en los eslabones superiores, valida que el markup de operador no sea 1, si pasa, setea el arreglo de markups 
			if($mkope!=1 && $keeplooking){
				$mkused['sgl'] = $mkope;
				$mkused['dbt'] = $mkope;
				$mkused['dbm'] = $mkope;
				$mkused['tpl'] = $mkope;
			//si no pasa trata de buscar en los eslabones que siguen
			}else{
				//si el markup general de tarifa no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
				if($keeplooking && $disp->Fields('hd_markup')!=1){
					$mkused['sgl'] = $disp->Fields('hd_markup');
					$mkused['dbt'] = $disp->Fields('hd_markup');
					$mkused['dbm'] = $disp->Fields('hd_markup');
					$mkused['tpl'] = $disp->Fields('hd_markup');
					$keeplooking = false;
				}
				//si el markup general de hotel no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
				if($keeplooking){
					if(!isset($mkhot[$hotel])){
						$sqlmkhot = "SELECT markdin_valor FROM markdin_gral WHERE id_area = ".$opeinfo->Fields('id_area')." AND id_hotel = $hotel AND markdin_estado = 0";
						$resmkhot = $db1->SelectLimit($sqlmkhot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
						if($resmkhot->RecordCount()>0){
							$mkhot[$hotel] = $resmkhot->Fields('markdin_valor');
						}
					}
					if(isset($mkhot[$hotel])){
						if($mkhot[$hotel]!=1 AND $mkhot[$hotel]!=0){
							$mkused['sgl'] = $mkhot[$hotel];
							$mkused['dbt'] = $mkhot[$hotel];
							$mkused['dbm'] = $mkhot[$hotel];
							$mkused['tpl'] = $mkhot[$hotel];
							$keeplooking = false;
						}
					}	
				}
				//Si no encontró nada, setea el arreglo con el markup por default
				if($keeplooking){
					$mkused['sgl'] = $mkdefault;
					$mkused['dbt'] = $mkdefault;
					$mkused['dbm'] = $mkdefault;
					$mkused['tpl'] = $mkdefault;
				}
			}
		//si es imperativo, setea el arreglo de markups con el markup de operador.
		}else{
			$mkused['sgl'] = $mkope;
			$mkused['dbt'] = $mkope;
			$mkused['dbm'] = $mkope;
			$mkused['tpl'] = $mkope;
		}
		///////////////////////////////////////////////////////
		//////////////////fin markup dinamico//////////////////
		///////////////////////////////////////////////////////



		////////////////////////////////////////////////
		//////////Implementación stock global///////////
		//////////////////////////////////////////////////

		if(!$tienedisp){
			if($tienedisp){
				$ete1 = "true";
			}else{
				$ete1 = "false";	
			} 
			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hoteles.hotelesmerge hm WHERE hm.id_hotel_cts =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0){
				$idpk = -1;
			}else{
				$idpk = $rspk->Fields('idpk');
			}

			$querydispdis = "SELECT 
				exo.id_stock_global,
				id_tipohab,
				sc_fecha,
				id_pk,
				sc_minnoche,
				IF((sc_hab1 - hc_hab1) < 0, 0,(sc_hab1 - hc_hab1)) AS dhab1,
  				IF((sc_hab2 - hc_hab2) < 0, 0,(sc_hab2 - hc_hab2)) AS dhab2,
  				IF((sc_hab3 - hc_hab3) < 0, 0,(sc_hab3 - hc_hab3)) AS dhab3,
  				IF((sc_hab4 - hc_hab4) < 0, 0,(sc_hab4 - hc_hab4)) AS dhab4
			FROM(
				SELECT 
					dispo.id_stock_global,
					id_tipohab,
					sc_fecha,
					id_pk,
					sc_minnoche,
					IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
				  	IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
				  	IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
				  	IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
				  	IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
				  	IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
				  	IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
				  	IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
				FROM(
					SELECT 
					    id_stock_global,
					    id_tipohab,
					    sc_fecha,
					    sc_hab1,
					    sc_hab2,
					    sc_hab3,
					    sc_hab4,
					    sc_minnoche,
					    id_pk 
  					FROM
    					hoteles.stock_global sc 
  					WHERE sc_estado = 0 
    					AND sc_cerrado = 0 
    					AND id_pk IN ($idpk) 
    					AND id_tipohab IN ($tipohab) 
    					AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
			LEFT JOIN 
				(SELECT 
  					id_stock_global,
				    hc_fecha,
				    SUM(hc_hab1) AS hc_hab1,
				    SUM(hc_hab2) AS hc_hab2,
				    SUM(hc_hab3) AS hc_hab3,
				    SUM(hc_hab4) AS hc_hab4 
				FROM
  					hoteles.hotocu hc 
					WHERE hc.hc_estado = 0 
					GROUP BY id_stock_global,
 		 			hc_fecha)  ocu 
					ON (
  						dispo.id_stock_global = ocu.id_stock_global 
  						AND dispo.sc_fecha = ocu.hc_fecha
  					) 
  				)  exo
			INNER JOIN hoteles.clientes_stock cs 
				ON exo.id_stock_global = cs.id_stock_global 
				WHERE 1 = 1 
				AND cs.id_cliente = 1 
				AND cs.cs_estado = 0

				";
  			
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				if($rspk->Fields('id_cadena')==11){
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					$stocknecesario = 0;
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
				}

				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}else{
				 		$tripledis = 0;
				 	}
				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			//echo "-->".$agregaralvalor;
			//echo "entro 2";
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			//echo "entro 3";
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_desde"]=$disp->Fields('lib_desde');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_hasta"]=$disp->Fields('lib_hasta');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_desde"]=$disp->Fields('comp_desde');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_hasta"]=$disp->Fields('comp_hasta');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_sgl"]=$disp->Fields('max_sgl');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_dbl"]=$disp->Fields('max_dbl');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_tpl"]=$disp->Fields('max_tpl');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["politica_adic"]=$disp->Fields('politica_adic');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_porc"]=$disp->Fields('monto_adic_porc');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_fijo"]=$disp->Fields('monto_adic_fijo');
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_desde"]=$disp->Fields('lib_desde');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_hasta"]=$disp->Fields('lib_hasta');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_desde"]=$disp->Fields('comp_desde');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_hasta"]=$disp->Fields('comp_hasta');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_sgl"]=$disp->Fields('max_sgl');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_dbl"]=$disp->Fields('max_dbl');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_tpl"]=$disp->Fields('max_tpl');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["politica_adic"]=$disp->Fields('politica_adic');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_porc"]=$disp->Fields('monto_adic_porc');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_fijo"]=$disp->Fields('monto_adic_fijo');
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_desde"]=$disp->Fields('lib_desde');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["lib_hasta"]=$disp->Fields('lib_hasta');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_desde"]=$disp->Fields('comp_desde');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["comp_hasta"]=$disp->Fields('comp_hasta');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_sgl"]=$disp->Fields('max_sgl');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_dbl"]=$disp->Fields('max_dbl');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["max_tpl"]=$disp->Fields('max_tpl');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["politica_adic"]=$disp->Fields('politica_adic');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_porc"]=$disp->Fields('monto_adic_porc');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["monto_adic_fijo"]=$disp->Fields('monto_adic_fijo');
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}



function buscar_habitacion_global($db1,$id_tipohabitacion){


	$consulta = "select id_tipohab from tipohabitacion where id_tipohabitacion = $id_tipohabitacion";
	$traer = $db1->SelectLimit($consulta);

	return $traer->Fields("id_tipohab");

}




 
//MATRIZ DISPONIBILIDAD PERO EXEPTUANDO UNA COT EN HOTOCU POR PARAMETRO ENTRANTE
function matrizDisponibilidad_exeptionCot($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$id_cot_ex,$id_cotdes,$otros=false){
	$condtarifa= " AND id_tipotarifa in (2,4,9,16,17,25,30,31,32,41,43,49,95,119,120,121,139,160,161,162,163,164,165,166,168)";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,4,9,16,17,25,30,31,32,41,43,49,95,119,120,121,139,160,161,162,163,164,165,166,168) ";


	///////////////////////////////////////////////
	////////////markup dinamico parte 1////////////
	///////////////////////////////////////////////
	
	$idcot=$id_cot_ex;
	if(isset($_GET['id_cot'])){
		$idcot = $_GET['id_cot'];
	}
	if(isset($_POST['id_cot'])){
		$idcot = $_POST['id_cot'];
	}

	$mkopehot = array();
	//preguntamos datos de operador de cot, mk imperativo y continente
	$sqlopeinfo = "SELECT IFNULL(id_opcts, id_operador) AS ope,
		IFNULL(op2.id_continente, op.id_continente) AS  id_cont,
		IFNULL(op2.mark_ope_imp, op.mark_ope_imp) AS mkimp,
		IFNULL(op2.mark_operador, op.mark_operador) AS mkope,
		c.id_area
		FROM cot c INNER JOIN hotel op ON c.id_operador = op.id_hotel LEFT JOIN hotel op2 ON c.id_opcts = op2.id_hotel
		WHERE id_cot = ".$idcot;
	$opeinfo = $db1->SelectLimit($sqlopeinfo) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	$esimpe = false;
	$mkope = 1;
	//si el markup general de operador no es cero, preguntamos si es imperativo y flagueamos; dejamos en una variable aparte el markup general de operador
	if($opeinfo->Fields('mkope')!=0){
		if($opeinfo->Fields('mkimp')==0){
			$esimpe = true;
		}
		$mkope = $opeinfo->Fields('mkope');
	}

	//si el markup de operador es 1 (desactivado equivalente a 0), preguntamos por el markup de continente al que pertence el operador y lo guardamos en markup por default
	if($mkope==1){
		$mkdefault = 1;
		$sqlmkcont = "SELECT o.cont_markuphtl
			FROM hotel h 
			INNER JOIN cont o ON h.id_continente = o.id_cont 
			WHERE h.id_hotel = ".$opeinfo->Fields('ope')." AND cont_estado = 0 AND o.area_cont = ".$opeinfo->Fields('id_area');
		$resmkcont = $db1->SelectLimit($sqlmkcont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

		if($resmkcont->RecordCount()>0){
			$mkdefault = $resmkcont->Fields('cont_markuphtl');
		}
		//si no encuentra markup de contienente, preguntamos por el de area y lo guardamos en markup por default.
		if($mkdefault==1){
			$sqlmkarea = "SELECT area_markup FROM area where id_area = ".$opeinfo->Fields('id_area');
			$resmkarea =  $db1->SelectLimit($sqlmkarea) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			$mkdefault = $resmkarea->Fields('area_markup');
		}
	}
	/////////////////////////////////////////////////
	///////////fin markup dinamico parte 1///////////
	/////////////////////////////////////////////////
	
	if(isset($id_pack)){
		//Si esta seteado el ID Pack, quiere decir que la cotizacion es un programa - promocion, etc...
		// en ese caso se va a buscar el tipo de tarifa seteado en el programa
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}

		if(isset($tarifas)){
			//Si el programa tiene seteado tipo(s) de tarifa entonces se usan esos, en caso contrario se usan
			//las que estan en la linea 728, que son Convenio, Especial y Promocional respectivamente, no la tarifa
			//usada en el pack
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
		else{
			//JG - 15-OCT-2013 (DESDE EL ELSE DE LA LINEA 746)
			//Se agrega query para ir a buscar ESPECIFICAMENTE el tipo de tarifa utilizado por la cotizacion al momento
			//de confirmarlo. Si y solo si la Cotizacion viene como parametro
			if(isset($id_cot_ex)){
				$pack_tar_q = "select distinct h.id_tipotarifa
								from 
									cotdes c
									inner join hotdet h on h.id_hotdet = c.id_hotdet
								where c.id_cot = ".$id_cot_ex."
								and c.cd_estado = 0
								and h.hd_estado = 0";
				$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				while(!$pack_tar->EOF){
					if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
					$pack_tar->MoveNext();
				}

				if(isset($tarifas)){
					//de ser encontrados los tipos de tarifa de la cotizacion, se utilizan para calcular la disponibilidad
					$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
				}								
			}
		}//FIN JG - 15-OCT-2013
	}
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
	if(isset($id_cotdes)){
		$sql_hotel = "SELECT h.id_hotel, h.id_ciudad FROM cotdes cd inner join hotel h on cd.id_hotel = h.id_hotel WHERE id_cotdes = $id_cotdes";
		$hotelR = $db1->SelectLimit($sql_hotel) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		$condCiu = " AND hot.id_ciudad = ".$hotelR->Fields("id_ciudad");
		$condHot="";
		if(!$otros){
			$condHot = "AND hot.id_hotel = ".$hotelR->Fields("id_hotel");
		}
	}
	$codigo2 = $opeinfo->Fields('ope');
	$condin = " AND (ifnull(FIND_IN_SET(0,id_cliente),0)<>0 OR ifnull(FIND_IN_SET($codigo2 , id_cliente),0)<>0) ";
	$condex = " AND IFNULL(FIND_IN_SET($codigo2 , id_excluye),0)=0 ";

	$condtarifa = str_replace('id_tipotarifa', 'a.id_tipotarifa', $condtarifa);
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
	FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox, tt.es_promo
	FROM     hotdet a
	INNER JOIN stock b ON a.id_hotdet = b.id_hotdet
	INNER JOIN hotel hot ON a.id_hotel = hot.id_hotel 
	INNER JOIN tipotarifa tt ON a.id_tipotarifa = tt.id_tipotarifa
	WHERE a.id_hotdet=b.id_hotdet
	AND b.sc_fecha>='$fechaInicio'
	AND b.sc_fecha<='$fechaFin'
	AND a.id_area = 1
	AND hot.hot_barco = 0
	and b.sc_cerrado = 0
	AND hot.id_hotel=a.id_hotel
	AND ((tt.es_promo = 1 AND tt_minnoche = DATEDIFF('$fechaFin','$fechaInicio'))
		OR
	(tt.es_promo = 0 AND tt_minnoche <= DATEDIFF('$fechaFin','$fechaInicio')))
	$condHot
	$condtarifa
	AND a.hd_estado = 0 AND b.sc_estado = 0
	$condin
	$condex) AS t1
	LEFT JOIN
	(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4
	FROM hotocu
	WHERE hc_fecha>='$fechaInicio'
	AND hc_fecha<='$fechaFin' AND hc_estado = 0
	AND id_cot <> $id_cot_ex
	GROUP BY hc_fecha, id_hotdet) AS t2
	ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha) ORDER BY t1.hd_sgl ASC";

	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();
	// 	echo $sqlDisp;die('aqui');
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');

	$cd_fecdesde = $fechaInicio;
	$cd_fechasta = $fechaFin;

	//if(isset($id_cotdes)){
	//	$query_cotdes = "SELECT *, DATE_FORMAT(cd_fecdesde,'%d-%m-%y') as cd_fecdesde1,DATE_FORMAT(cd_fechasta,'%d-%m-%y') as cd_fechasta1 FROM cotdes WHERE id_cotdes = $id_cotdes";
	//	$cotdes = $db1->SelectLimit($query_cotdes) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//	
	//	$cd_fecdesde = new DateTime($cotdes->Fields('cd_fecdesde1'));
	//	$cd_fechasta = new DateTime($cotdes->Fields('cd_fechasta1'));
	//}

	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		$es_promo = $disp->Fields('es_promo');
		if(isset($id_pack)){
			$es_promo = 0;
		}

		$singlexdoble=0;

		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		//if(isset($id_cotdes)){
		//	$fec_actual = new DateTime($fecha);
		//	
		//	if($cd_fecdesde<=$fec_actual and $cd_fechasta>=$fec_actual){
		//		if ($stocksingle<0) $stocksingle=$cotdes->Fields('cd_hab1');
		//		if ($stockdoble<0) $stockdoble=$cotdes->Fields('cd_hab2');
		//		if ($stocktwin<0) $stocktwin=$cotdes->Fields('cd_hab3');
		//		if ($stocktriple<0) $stocktriple=$cotdes->Fields('cd_hab4');
		//	}else{
		//		if ($stocksingle<0) $stocksingle=0;
		//		if ($stockdoble<0) $stockdoble=0;
		//		if ($stocktwin<0) $stocktwin=0;
		//		if ($stocktriple<0) $stocktriple=0;
		//	}
		//}else{
			if ($stocksingle<0) $stocksingle=0;
			if ($stockdoble<0) $stockdoble=0;
			if ($stocktwin<0) $stocktwin=0;
			if ($stocktriple<0) $stocktriple=0;
		//}

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		///////////////////////////////////////////////
		////////////markup dinamico parte 2////////////
		///////////////////////////////////////////////
		$keeplooking = true;
		//si el markup de operador no es imperativo o no lo encontró empezamos a recorrer los eslabones superiores de gerarquia
		if(!$esimpe){
			//markup especifico de operador por tarifa.
			$sqlmkhdesp = "SELECT hbmk_markup_sgl, hbmk_markup_dbt, hbmk_markup_dbm, hbmk_markup_tpl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet = ".$hotdet." 
				AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." AND id_mkgrupo IS NULL AND '".$fecha."' 
				BETWEEN hbmk_fecdesde AND hbmk_fechasta";
			$resmkhdesp = $db1->SelectLimit($sqlmkhdesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			//si encuentra markup especifico, setea el arreglo de markups y cambia el flag a falso para que deje de buscar en el resto de eslabones
			if($resmkhdesp->RecordCount()>0){
				$mkused['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
				$mkused['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
				$mkused['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
				$mkused['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
				$keeplooking = false;
			}
			if($keeplooking){
				//si no ha sido seteado previamente el hotel, busca markup especifico de operador por hotel
				if(!isset($mkopehot[$fecha][$hotel])){    
					$sqlmkhotesp = "SELECT hbmk_markup_sgl, hbmk_markup_dbt, hbmk_markup_dbm, hbmk_markup_tpl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet IS NULL AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." 
						AND id_mkgrupo IS NULL AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
					$resmkhdesp = $db1->SelectLimit($sqlmkhotesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
					//si encuentra markup especifico, setea el arreglo de markups y cambia el flag a falso para que deje de buscar en el resto de eslabones
					if($resmkhdesp->RecordCount()>0){
						$mkused['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
						$mkused['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
						$mkused['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
						$mkused['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
						//agrega el markup para ese día y ese hotel a un arreglo para no volver a ejecutar la query si se pregunta denuevo por ese hotel y ese día
						$mkopehot[$fecha][$hotel]['sgl'] = $resmkhdesp->Fields('hbmk_markup_sgl');
						$mkopehot[$fecha][$hotel]['dbt'] = $resmkhdesp->Fields('hbmk_markup_dbt');
						$mkopehot[$fecha][$hotel]['dbm'] = $resmkhdesp->Fields('hbmk_markup_dbm');
						$mkopehot[$fecha][$hotel]['tpl'] = $resmkhdesp->Fields('hbmk_markup_tpl');
						$keeplooking = false;
					}
				//si ya esta seteado, ocupa lo que está y marca el flag a falso para que deje de buscar en el resto de eslabones
				}else{
					$mkused['sgl'] = $mkopehot[$fecha][$hotel]['sgl'];
					$mkused['dbt'] = $mkopehot[$fecha][$hotel]['dbt'];
					$mkused['dbm'] = $mkopehot[$fecha][$hotel]['dbm'];
					$mkused['tpl'] = $mkopehot[$fecha][$hotel]['tpl'];
					$keeplooking = false;
				}
			}
			if($keeplooking){
				//markup especifico de grupo por tarifa
				$sqlmkgrupoesp = "SELECT hbmk_markup_sgl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet = ".$hotdet." AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." 
					AND id_mkgrupo IS NOT NULL AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
				$resmkgrupoesp = $db1->SelectLimit($sqlmkgrupoesp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				//si encuentra un grupo en el que esta el operador y la tarifa, setea el arreglo de markup y cambia el flag a falso para que deje de buscar en el resto de eslabones
				if($resmkgrupoesp->RecordCount()>0){
					$mkused['sgl'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['dbt'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['dbm'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$mkused['tpl'] = $resmkgrupoesp->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
					$keeplooking = false;
				}
			}

			if($keeplooking){
				//markup especifico por grupo
				if(!isset($mkopehot[$fecha][$hotel])){
					$sqlmkgrupo = "SELECT hbmk_markup_sgl FROM mk_ope_hotdet WHERE hbmk_estado = 0 AND id_hotdet IS NULL AND id_hot = ".$hotel." AND id_hotel = ".$opeinfo->Fields('ope')." AND id_mkgrupo IS NOT NULL 
					AND '".$fecha."' BETWEEN hbmk_fecdesde AND hbmk_fechasta";
					$resmkgrupo = $db1->SelectLimit($sqlmkgrupo) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
					//si encuentra un grupo en el que esta el operador y el hotel, setea el arreglo de markup y cambia el flag a falso para que deje de buscar en el resto de eslabones
					if($resmkgrupo->RecordCount()>0){
						$mkused['sgl'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['dbt'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['dbm'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						$mkused['tpl'] = $resmkgrupo->Fields('hbmk_markup_sgl');
						//agrega el markup para ese día y ese hotel a un arreglo para no volver a ejecutar la query si se pregunta denuevo por ese hotel y ese día
						$mkopehot[$fecha][$hotel]['sgl'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['dbt'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['dbm'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$mkopehot[$fecha][$hotel]['tpl'] = $resmkgrupo->Fields('hbmk_markup_sgl');//se supone que sea solo por sgl (no fue vendida mia);
						$keeplooking = false;
					}
				}
			}
			//si no encontró nada en los eslabones superiores, valida que el markup de operador no sea 1, si pasa, setea el arreglo de markups 
			if($mkope!=1 && $keeplooking){
				$mkused['sgl'] = $mkope;
				$mkused['dbt'] = $mkope;
				$mkused['dbm'] = $mkope;
				$mkused['tpl'] = $mkope;
			//si no pasa trata de buscar en los eslabones que siguen
			}else{
				//si el markup general de tarifa no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
				if($keeplooking && $disp->Fields('hd_markup')!=1){
					$mkused['sgl'] = $disp->Fields('hd_markup');
					$mkused['dbt'] = $disp->Fields('hd_markup');
					$mkused['dbm'] = $disp->Fields('hd_markup');
					$mkused['tpl'] = $disp->Fields('hd_markup');
					$keeplooking = false;
				}
				//si el markup general de hotel no es 1, setea el arreglo de markups y flaguea en falso para que deje de buscar.
				if($keeplooking){
					if(!isset($mkhot[$hotel])){
						$sqlmkhot = "SELECT markdin_valor FROM markdin_gral WHERE id_area = ".$opeinfo->Fields('id_area')." AND id_hotel = $hotel AND markdin_estado = 0";
						$resmkhot = $db1->SelectLimit($sqlmkhot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
						if($resmkhot->RecordCount()>0){
							$mkhot[$hotel] = $resmkhot->Fields('markdin_valor');
						}
					}
					if(isset($mkhot[$hotel])){
						if($mkhot[$hotel]!=1 AND $mkhot[$hotel]!=0){
							$mkused['sgl'] = $mkhot[$hotel];
							$mkused['dbt'] = $mkhot[$hotel];
							$mkused['dbm'] = $mkhot[$hotel];
							$mkused['tpl'] = $mkhot[$hotel];
							$keeplooking = false;
						}
					}	
				}
				//Si no encontró nada, setea el arreglo con el markup por default
				if($keeplooking){
					$mkused['sgl'] = $mkdefault;
					$mkused['dbt'] = $mkdefault;
					$mkused['dbm'] = $mkdefault;
					$mkused['tpl'] = $mkdefault;
				}
			}
		//si es imperativo, setea el arreglo de markups con el markup de operador.
		}else{
			$mkused['sgl'] = $mkope;
			$mkused['dbt'] = $mkope;
			$mkused['dbm'] = $mkope;
			$mkused['tpl'] = $mkope;
		}
		///////////////////////////////////////////////////////
		//////////////////fin markup dinamico//////////////////
		///////////////////////////////////////////////////////

		////////////////////////////////////////////////
		//////////Implementación stock global///////////
		////////////////////////////////////////////////

		$socu= "SELECT * FROM hoteles.hotocu WHERE id_cot = $id_cot_ex AND id_cliente = 1 AND hc_fecha = '$fecha' AND hc_estado = 0";
		$ocug = $db1->SelectLimit($socu) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		if($ocug->RecordCount()>0){
			$stocksingle=$stocksingle-$ocug->Fields('hc_hab1');
			$stockdoble = $stockdoble-$ocug->Fields('hc_hab3');
			$stocktwin = $stocktwin-$ocug->Fields('hc_hab2');
			$stocktriple = $stocktriple-$ocug->Fields('hc_hab4');
			$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		//die($querydispdis);

		}
			

		if(!$tienedisp){

			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hoteles.hotelesmerge hm WHERE hm.id_hotel_cts =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0)
				$idpk = -1;
			else
				$idpk = $rspk->Fields('idpk');

			$querydispdis = "SELECT 
			  exo.id_stock_global,
			  id_tipohab,
			  sc_fecha,
			  id_pk,
			  sc_minnoche,
			IF(
			    (sc_hab1 - hc_hab1) < 0,
			    0,
			    (sc_hab1 - hc_hab1)
			  ) AS dhab1,
			  IF(
			    (sc_hab2 - hc_hab2) < 0,
			    0,
			    (sc_hab2 - hc_hab2)
			  ) AS dhab2,
			  IF(
			    (sc_hab3 - hc_hab3) < 0,
			    0,
			    (sc_hab3 - hc_hab3)
			  ) AS dhab3,
			  IF(
			    (sc_hab4 - hc_hab4) < 0,
			    0,
			    (sc_hab4 - hc_hab4)
			  ) AS dhab4

			 FROM(

			SELECT 
			  dispo.id_stock_global,
			  id_tipohab,
			  sc_fecha,
			  id_pk,
			  sc_minnoche,
			  IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
			  IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
			  IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
			  IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
			  IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
			  IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
			  IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
			  IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
			FROM
			  (SELECT 
			    id_stock_global,
			    id_tipohab,
			    sc_fecha,
			    sc_hab1,
			    sc_hab2,
			    sc_hab3,
			    sc_hab4,
			    sc_minnoche,
			    id_pk 
			  FROM
			    hoteles.stock_global sc 
			  WHERE sc_estado = 0 
			    AND sc_cerrado = 0 
			    AND id_pk IN ($idpk) 
			    AND id_tipohab IN ($grupo) 
			    AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
			  LEFT JOIN 
			    (SELECT 
			      id_stock_global,
			      hc_fecha,
			      SUM(hc_hab1) AS hc_hab1,
			      SUM(hc_hab2) AS hc_hab2,
			      SUM(hc_hab3) AS hc_hab3,
			      SUM(hc_hab4) AS hc_hab4 
			    FROM
			      hoteles.hotocu hc 
			    WHERE hc.hc_estado = 0 
			    AND id_cot <> $id_cot_ex
			    GROUP BY id_stock_global,
			      hc_fecha)  ocu 
			    ON (
			      dispo.id_stock_global = ocu.id_stock_global 
			      AND dispo.sc_fecha = ocu.hc_fecha
			    ) )  exo
			  INNER JOIN hoteles.clientes_stock cs 
			    ON exo.id_stock_global = cs.id_stock_global 
			WHERE 1 = 1 
			  AND cs.id_cliente = 1 
			  AND cs.cs_estado = 0";
				//die($querydispdis);
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				if($rspk->Fields('id_cadena')==11){
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					$stocknecesario = 0;
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
					
				}

				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}
				 	else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}
				 	else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}
				 	else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}
				 	else{
				 		$tripledis = 0;
				 	}

				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
				
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
		if(($diaspro <$minNox) && $id_pack==''  ){ $tienedisp=false;$usasglobal="N";}
		}
		/////////////////////////////////////////////////

	$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
	$tipo=$disp->Fields('id_tipotarifa');

	$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;

		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;

		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;

		if ($tienedisp) $dispaux="I"; else $dispaux="R";

		if (!$bloqueado) {
			
			if($arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
				$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=='R')){
				
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
					$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"])){

						$arreglo[$hotel][$grupo][$es_promo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["usaglo"]=$usasglobal;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["sdis"]=$singledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["ddis"]=$dobledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["twdis"]=$twindis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["trdis"]=$tripledis;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["idsg"]=$id_stock;
						$arreglo[$hotel][$grupo][$es_promo][$fecha]["markup"] = $mkused;
					}
				}
			}
		}
		$disp->MoveNext();
	}
	return $arreglo;
}

function matrizDisponibilidadPromocion($db1,$cant_noches,$fechaInicio,$fechaFin,$single, $twin, $doble , $triple,$id_pack=0){
	
	
	if($id_pack==0){
	if($cant_noches==1) $condtarifa="2";
	if($cant_noches==2) $condtarifa="2,164";
	if($cant_noches==3) $condtarifa="119";
	if($cant_noches==4) $condtarifa="120,137";
	if($cant_noches==5) $condtarifa="121";
	}else{
	//echo "entro 3";
		$query_pack = "select * from pack where id_pack = ".$id_pack;
		//echo $query_pack."<br>";
		$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
	// 	if($id_pack == "696") $condtarifa= " AND id_tipotarifa in (160)";
		//echo $pack->Fields('id_tipotarifa');
		if($pack->Fields('id_tipotarifa') == 2){
			$condtarifa= "2";
		}
		// echo "</br>".$cant_noches;
		/*
		if($pack->Fields('id_tipotarifa') == 4){
			if($cant_noches==1) $condtarifa="2";
			if($cant_noches==2) $condtarifa="2";
			if($cant_noches==3) $condtarifa="119";
			if($cant_noches==4) $condtarifa="120,137";
			if($cant_noches==5) $condtarifa="121";*/
			if($pack->Fields('id_tipotarifa') == 4){
			//echo $pack->Fields('pac_n');
			//if($pack->Fields('pac_n') == 1) $condtarifa= "4";
			//if($pack->Fields('pac_n') == 2) $condtarifa= "119";
			
			if($cant_noches == 3) $condtarifa= "119,140";
			if($cant_noches == 4) $condtarifa= "120,141";
			if($cant_noches == 5) $condtarifa= "121,142";
			if($cant_noches == 6) $condtarifa= "143";
			if($cant_noches == 7) $condtarifa= "144, 160";
		
			//echo "<br>".$id_pack."<br>";
			//echo $pack->Fields('id_tipotarifa')."<br>";
			//echo $cant_noches."<br>";
			//echo $condtarifa;
		}
		
		if($pack->Fields('id_tipotarifa') == 9){
			//echo $pack->Fields('pac_n');
			//if($pack->Fields('pac_n') == 1) $condtarifa= "4";
			//if($pack->Fields('pac_n') == 2) $condtarifa= "119";
			if($cant_noches == 2) $condtarifa= "153,164";
			if($cant_noches == 3) $condtarifa= "140";
			if($cant_noches == 4) $condtarifa= "141";
			if($cant_noches == 5) $condtarifa= "142";
			if($cant_noches == 6) $condtarifa= "143";
			if($cant_noches == 7) $condtarifa= "144";
		
			//echo "<br>".$id_pack."<br>";
			//echo $pack->Fields('id_tipotarifa')."<br>";
			//echo $cant_noches."<br>";
			//echo $condtarifa;
		}
		//if($pack->Fields('id_tipotarifa') == 5){
		//	if($cant_noches == 7) $condtarifa= "160";
		//}

		
		if($pack->Fields('id_tipotarifa') == 0){
			if($cant_noches==1) $condtarifa="2";
			if($cant_noches==2) $condtarifa="2,164";
			if($cant_noches==3) $condtarifa="2,119";
			if($cant_noches==4) $condtarifa="2,120,137";
			if($cant_noches==5) $condtarifa="2,121";
		}		
	}
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND a.id_area = 1
						and b.sc_cerrado = 0
						AND hot.id_hotel=a.id_hotel 
						and a.id_tipotarifa in ($condtarifa)
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	//echo $cant_noches."<br><br>";		
	//
		//	if ($_SESSION['id']== 3839)
		//	{
		//		echo $sqlDisp."<br>";
		//	}

	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();
	//echo $totalRows_listado1;
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
	//echo $ll;
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/* comentado para que no venda soble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		/*
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=80;
			if($doble>0) $thab3=44.88;
			if($twin>0) $thab2=44.88;
			if($triple>0) $thab4=0;
		}*/
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
	//print_r($arreglo);
return $arreglo;
}
function matrizDisponibilidadCruceLagos($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack){
	//echo "Matiz: $fechaInicio - $fechaFin - $single - $single - $twin - $doble - $triple";
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND a.id_area = 1
						and b.sc_cerrado = 0
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/* comentado para que no venda soble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		/*
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=77;
			if($doble>0) $thab3=44;
			if($twin>0) $thab2=44;
			if($triple>0) $thab4=36.66;
		}*/
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}
//Funcion para obtener la url correspondiente, segun el id_tipopack y al id_seg
function get_url($db1,$id_seg,$id_tipopack,$url_tipo){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url
		WHERE id_seg = $id_seg and id_tipopack = $id_tipopack and url_tipo = $url_tipo";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo $query_seg;
	return $seg;
}

function get_url2($db1,$url){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url	WHERE url_direccion = '$url'";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg;
}

//Funcion para verificar si esta en la direccion correcta
function v_url($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot,$estricto = false){
	require('lan/idiomas.php');
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$tempurl2 = get_url2($db1,$url);
	$url_direccion = $tempurl->Fields('url_direccion');
	$url_confirmado = $tempurl->Fields('url_confirmado');
	$url_direccion2 = $tempurl2->Fields('url_direccion');
	$url_confirmado2 = $tempurl2->Fields('url_confirmado');
	
	if($url_confirmado==$url_confirmado2){
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}else{
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.alert('$programaconfirmado.');window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}
}

//Funcion para verificar si esta la cotizacion esta en disponibilidad inmediata o on-request y redirigir si ingreso mal la direccion
function v_or($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$url_direccion = $tempurl->Fields('url_direccion');
	if((substr($url,-2,2)!=substr($url_direccion,-2,2))and(isset($url_direccion))){
		if(stristr($url,"_or")){
			$url = str_replace("_or","",$url);
		}else{
			$url.="_or";
		}
		
		die("<script>window.location='$url.php?id_cot=$id_cot';</script>");
	}
}

//Funcion para redirigir cuando ya esta confirmada una cotizacion
function redir($dir, $seg, $pconf, $pfinal){
	require('lan/idiomas.php');
	if(($seg == 17)or($seg == 20)or($seg == 23)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 18){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if(($seg == 7)or($seg == 19)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 13){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	return false;
}

function mail_disponibilidad($db1,$id_cot){
	return true;
	/*
	$hotocu_q = "SELECT
			hc.id_hotel,
			hc.id_hotdet,
			hc.hc_fecha,
			DATE_FORMAT(hc.hc_fecha, '%d-%m-%Y') AS hc_fecha2,
			tt.tt_nombre,
			th.th_nombre,
			hot.hot_nombre
		FROM
			hotocu AS hc
		INNER JOIN hotdet AS hd ON hd.id_hotdet = hc.id_hotdet
		INNER JOIN tipotarifa AS tt ON tt.id_tipotarifa = hd.id_tipotarifa
		INNER JOIN tipohabitacion AS th ON th.id_tipohabitacion = hd.id_tipohabitacion
		INNER JOIN hotel as hot ON hot.id_hotel = hc.id_hotel
		WHERE
			hc.id_cot = $id_cot
		AND hc.hc_estado = 0";
	$hotocu = $db1->SelectLimit($hotocu_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$id_hotel = 0;
	
	while(!$hotocu->EOF){
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$id_hotel = $hotocu->Fields("id_hotel");
			$send = false;
			$hotel_mail = '
			<table cellpadding="2" cellspacing="0" border="1" align="center">
				<tr>
					<td align="center" valign="middle"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>ALERTA DE DISPONIBILIDAD<br>'.$hotocu->Fields('hot_nombre').'</td>
					<td align="center" colspan="12">Informe de las habitaciones que cuentan con 1 o menos de disponibilidad<br><br>Para modificar la disponibilidad visite este <a href="http://cts.distantis.com/distantis/hot_disponible.php">vinculo</a></td>
				</tr>';
		}
		
		$disp_q = "SELECT sc.sc_hab1, sc.sc_hab2, sc.sc_hab3, sc.sc_hab4, sum(hc.hc_hab1) AS hc_hab1, sum(hc.hc_hab2) AS hc_hab2, sum(hc.hc_hab3) AS hc_hab3, sum(hc.hc_hab4) AS hc_hab4
		FROM stock AS sc
		INNER JOIN hotocu AS hc ON hc.hc_estado = 0 AND hc.id_hotdet = sc.id_hotdet AND hc.hc_fecha = sc.sc_fecha
		WHERE sc.id_hotdet = ".$hotocu->Fields("id_hotdet")." AND sc.sc_estado = 0 AND sc_fecha = '".$hotocu->Fields("hc_fecha")."' GROUP BY hc.hc_fecha";
		$disp = $db1->SelectLimit($disp_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
		
		if($disp->Fields('hc_hab1')==''){$ocu_hab1 = 0;}else{$ocu_hab1 = $disp->Fields('hc_hab1');};
		if($disp->Fields('hc_hab2')==''){$ocu_hab2 = 0;}else{$ocu_hab2 = $disp->Fields('hc_hab2');};
		if($disp->Fields('hc_hab3')==''){$ocu_hab3 = 0;}else{$ocu_hab3 = $disp->Fields('hc_hab3');};
		if($disp->Fields('hc_hab4')==''){$ocu_hab4 = 0;}else{$ocu_hab4 = $disp->Fields('hc_hab4');};
		
		$tot_hab1 = $disp->Fields('sc_hab1')-$ocu_hab1;
		if($tot_hab1<0) $tot_hab1 = 0;
		if($tot_hab1<1){$color1="red";
		}elseif($tot_hab1==1){$color1="yellow";
		}else{$color1="#EEE";}
		
		$tot_hab2 = $disp->Fields('sc_hab2')-$ocu_hab2;
		if($tot_hab2<0) $tot_hab2 = 0;
		if($tot_hab2<1){$color2="red";
		}elseif($tot_hab2==1){$color2="yellow";
		}else{$color2="#EEE";}
		
		$tot_hab3 = $disp->Fields('sc_hab3')-$ocu_hab3;
		if($tot_hab3<0) $tot_hab3 = 0;
		if($tot_hab3<1){$color3="red";
		}elseif($tot_hab3==1){$color3="yellow";
		}else{$color3="#EEE";}
		
		$tot_hab4 = $disp->Fields('sc_hab4')-$ocu_hab4;
		if($tot_hab4<0) $tot_hab4 = 0;
		if($tot_hab4<1){$color4="red";
		}elseif($tot_hab4==1){$color4="yellow";
		}else{$color4="#EEE";}
		
		if($tot_hab1<=1 or $tot_hab2<=1 or $tot_hab3<=1 or $tot_hab4<=1){
			$hotel_mail.= ' 
			<tr>
				<td align="center" valign="middle" rowspan="3" width="200">'.$hotocu->Fields('hc_fecha2')."<br>".$hotocu->Fields('tt_nombre')." - ".$hotocu->Fields('th_nombre').'</td>
				<td align="center" colspan="3" width="100">Single</td>
				<td align="center" colspan="3" width="100">Doble Twin</td>
				<td align="center" colspan="3" width="100">Doble Matrimonial</td>
				<td align="center" colspan="3" width="100">Triple</td>
			</tr>
			<tr>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
			</tr>
			<tr>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab1').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab1').'</td>
                <td align="center" valign="middle" bgcolor="'.$color1.'">'.$tot_hab1.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab2').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab2').'</td>
                <td align="center" valign="middle" bgcolor="'.$color2.'">'.$tot_hab2.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab3').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab3').'</td>
                <td align="center" valign="middle" bgcolor="'.$color3.'">'.$tot_hab3.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab4').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab4').'</td>
                <td align="center" valign="middle" bgcolor="'.$color4.'">'.$tot_hab4.'</td>
			</tr>
            ';
			$send = true;
		}
		
		$hotocu->MoveNext();
		
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$hotel_mail.='</table><center>Este mail fue generado automáticamente por CTS ONLINE, En caso de cualquier duda o consulta, escríbenos un mail a <a href="mailto:cts@distantis.com">cts@distantis.com</a></center>.';
			if($send){
				$hotocu->MoveLast();
				$message_alt='Si ud no puede ver este email por favor contactese con CTS-ONLINE &copy; 2011-2012';
				
				$mail_to = 'ctsonline@vtsystems.cl';
				
				//$mail_to = 'sguzman@vtsystems.cl';
	
				$opSup="SELECT id_usuario,usu_mail FROM usuarios
					WHERE usu_enviar_correo = 1 and not usu_mail = '' and id_empresa = ".$id_hotel;
				$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
				if($rs_opSup->RecordCount()>0){
					while (!$rs_opSup->EOF){
						$mail_to.=";".$rs_opSup->Fields('usu_mail');
						$rs_opSup->MoveNext(); 
					}
					$subject = 'CTS-ONLINE - ALERTA DE DISPONIBILIDAD '.$hotocu->Fields('hot_nombre');
					$ok = envio_mail($mail_to,$copy_to,$subject,$hotel_mail,$message_alt);
				}
				$hotocu->MoveNext();
			}
		}
	}
	*/
}

function Cmb_Pais($db1){
	$sqlPais = "
		SELECT id_pais, 
			CASE '".$_SESSION['idioma']."' 
			WHEN 'sp' THEN pai_nombre
			WHEN 'po' THEN pai_noming
			WHEN 'en' THEN pai_noming
			END as pai_nombre 
		FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
	$rsPais = $db1->SelectLimit($sqlPais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $rsPais;
}

function MonedaNombre($db1,$id_mon){
	$mon_q = "SELECT * FROM mon WHERE id_mon = $id_mon";
	$mon = $db1->SelectLimit($mon_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $mon->Fields('mon_nombre');
}

function ErrorPhp($db1,$id_cot,$linea,$error,$url){

	$err = str_replace("'","-",$error);
	$insertSQL = sprintf("INSERT INTO phperror (id_cot, phe_desc, phe_fecha, phe_linea, php_url) VALUES (%s, %s, now(), %s, %s)",
			GetSQLValueString($id_cot, "int"),
			GetSQLValueString($err, "text"),
			//now()
			GetSQLValueString($linea, "int"),
			GetSQLValueString($url, "text")
	);
	//echo "<hr>".$insertSQL."<hr>";
	$insert = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $linea." - ".$error;
}

function addServInit(){
	global $db1,$cot,$ciudad;
	
	$query_ciudad = "SELECT c.ciu_nombre,c.id_ciudad
	FROM trans as t
	inner join transcont tc on tc.id_trans = t.id_trans
	INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
	WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now()
	GROUP BY t.id_ciudad ORDER BY ciu_nombre";
	$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$query_transportes = "SELECT * FROM trans as t
						inner join transcont tc on tc.id_trans = t.id_trans
						INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
						WHERE t.id_area = 1 and tra_estado = 0 AND i.tpt_estado = 0 and t.id_mon = 1 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now() GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
	$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$transportes->EOF){
		if($_SESSION['idioma'] == 'sp'){
			$tra_obs = $transportes->Fields('tra_obs');
		}
		if($_SESSION['idioma'] == 'po'){
			$tra_obs = $transportes->Fields('tra_obspor');
		}
		if($_SESSION['idioma'] == 'en'){
			$tra_obs = $transportes->Fields('tra_obsing');
		}
		$temp[$transportes->Fields('id_ciudad')][] = array('id_trans'=>$transportes->Fields('id_trans'),
															'id_hotel'=>$transportes->Fields('id_hotel'),
															'tra_codigo'=>$transportes->Fields('tra_codigo'),
															'id_tipotrans'=>$transportes->Fields('id_tipotrans'),
															'tra_nombre'=>utf8_encode($transportes->Fields('tra_nombre')),
															'tra_obs'=>utf8_encode($tra_obs));
		$transportes->MoveNext();
	}
	?> 
	<script language="JavaScript">
	function M(field) { field.value = field.value.toUpperCase() }
		var transportes = <?= json_encode($temp) ?>;
		function trans(pas){
			var ciudad = $("#id_ciudad_"+pas+" option:selected").val();
			var tservicio = $("#id_tservicio_"+pas+" option:selected").val();
			$('#id_ttagrupa_'+pas).empty();
			transportes[ciudad].forEach(function(fn, scope) {
				if(fn['id_tipotrans']==tservicio){
				if(fn['id_hotel']==0){
					$('#id_ttagrupa_'+pas).append('<option value="'+fn['tra_codigo']+'" title="'+fn['tra_obs']+'">'+fn['tra_nombre']+'</option>');
				}else{
					$('#id_ttagrupa_'+pas).append('<option value="'+fn['tra_codigo']+'" title="'+fn['tra_obs']+'" style="background-color:#FFFF00">'+fn['tra_nombre']+'</option>');
				}
				}
			});
		};
	</script>
	<?
}

function addServProc(){
	global $db1,$cot;
	global $advertenciaserv,$noexisteserv;
	
	foreach ($_POST as $keys => $values){    //Search all the post indexes 
		if(strpos($keys,"=")){              //Only edit specific post fields 
			$vars = explode("=",$keys);     //split the name variable at your delimiter
			$_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
			unset($_POST[$keys]);           //unset the temporary post index. 
		} 
	}
	
	if ((isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s,
			cp_numvuelo=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "int"),
			GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		//SELECTS DE VALIDACION BLOQUEO DE SERVICIOS 
		//busqueda de bloqueos para el servicio
		$qbloq = "SELECT *,DATE_FORMAT(fec_desde, '%d-%m-%Y') AS bloq_desde,DATE_FORMAT(fec_hasta, '%d-%m-%Y') AS bloq_hasta FROM trans_bloq WHERE bloq_estado = 0 AND tra_codigo =  '".$_POST['id_ttagrupa_'.$_POST['agrega']]."' ";
		$rbloq = $db1->SelectLimit($qbloq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

		$diaserv1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$diaserv = $diaserv1[2].'-'.$diaserv1[1].'-'.$diaserv1[0];

		$flag_bloqueo = false;
		if($rbloq->RecordCount()>0){
			//preguntamos por el dia de la semana del servicio
			$qdia = "SELECT DAYOFWEEK('$diaserv') AS dia_semana";
			
			if($rbloq->Fields('fec_desde')!=NULL){ $qdia.=",IF('$diaserv' BETWEEN '".$rbloq->Fields('fec_desde')."' AND '".$rbloq->Fields('fec_hasta')."',0,1) AS bloq_fec";}
			

			$rdia = $db1->SelectLimit($qdia) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			if($rbloq->Fields('fec_desde')!=NULL){if($rdia->Fields('bloq_fec')==0) $flag_bloqueo = true;}
			$diabloq = $rdia->Fields('dia_semana');

			if($rbloq->Fields('dia'.$diabloq)==0)$flag_bloqueo = true;
			

		}

		if($flag_bloqueo){
			echo "<script type='text/javascript' charset='utf-8'>alert('Este servicio se encuentra bloqueado para esta fecha');</script>";
		}else{
			
			//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
			if($_POST['pax_max'] == '1'){
				//partimos del c = 1 hasta donde llege el c por POST
				$contador = $_POST['c'];
				$insertarReg=true;
				$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
				$dateasdf = new DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
				$id_trans_sql ="select*
					from trans 
					where tra_codigo='".$_POST['id_ttagrupa_'.$_POST['agrega']]."' 
					and  tra_fachasta >= '".$dateasdf->format('Y-m-d')."' and tra_estado = 0
					and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
				//echo $id_trans_sql;
				$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
				
				if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
		
				//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
				if($totalRows_id_trans_rs > 0){	
					//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
					for ($x=1; $x <=$contador ; $x++) {
						$verifica_sql = "select*from trans t 
			inner join cotser cs on t.id_trans = cs.id_trans 
			where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
						$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
						if($verifica->RecordCount()>0){
							$insertarReg=false;break;
						}
					}
					if($insertarReg===false){
						echo '<script>
								alert("'.html_entity_decode($advertenciaserv).'");
						</script>';
					}
					for($i = 1 ; $i<= $contador ; $i++){
						$insertSQL = sprintf("INSERT INTO cotser (id_cotpas,id_cotdes, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s,%s, %s, %s, %s, %s, %s, %s ,%s, 0)",
							GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
							GetSQLValueString($_POST['id_cotdes_'.$i], "int"),
							1,
							GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
							GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
							GetSQLValueString($_GET['id_cot'], "int"),
							GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
							GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
							GetSQLValueString($seg, "int"));
							$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					}
				}else{
					echo '<script type="text/javascript" charset="utf-8">
							alert("-'.html_entity_decode($noexisteserv).'.");
					</script>';
				}
			}else{
				$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
				$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
					
				$id_trans_sql ="select*
					from trans 
					where tra_codigo='".$_POST['id_ttagrupa_'.$_POST['agrega']]."' 
					and  tra_fachasta>= '".$dateasdf->format('Y-m-d')."' and tra_estado = 0
					and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
				
				
				
				$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
				
				if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
		
				//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
				if($totalRows_id_trans_rs > 0){
					//validamos de que antes no esté ingresado el mismo servicio
					$verifica_sql = "select*from trans t 
			inner join cotser cs on t.id_trans = cs.id_trans 
			where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
					$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
							
					if($verifica->RecordCount() >0){
						echo '<script type="text/javascript">
								alert("'.html_entity_decode($advertenciaserv).'");
						</script>';
					}
					
					$insertSQL = sprintf("INSERT INTO cotser (id_cotpas,id_cotdes, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s,%s, %s, %s, %s ,%s, %s, %s ,%s, 0)",
											GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
											GetSQLValueString($_POST['id_cotdes_'.$_POST["agrega"]], "int"),
											1,
											GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
											GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
											GetSQLValueString($_GET['id_cot'], "int"),
											GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
											GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
											GetSQLValueString($seg, "int")							
											);
					$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				}else{
					echo '<script type="text/javascript" charset="utf-8">
							alert("-'.html_entity_decode($noexisteserv).'.");
					</script>';
		
				}
			}
		}
		
		ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	}
}

function addServHTML($num_pas){
	global $db1,$destinos,$pasajeros,$ciudad;
	global $crea_serv,$nomserv,$todos_pax,$fechaserv,$destino,$numtrans,$observa,$agregar;
	?>
	<script>
	$(function(){
		$("#datepicker_<?=$num_pas?>").datepicker({
			minDate: new Date(),
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...',
     onSelect: function( selectedDate ) {
      //var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      //dates.not( this ).datepicker( "option", option, date );
      validate("true",this.id);
     }
		});
		
		$('#id_ttagrupa_<?=$num_pas?>').change(function(){
			var selected = $('#id_ttagrupa_<?=$num_pas?> option:selected').attr('title');
			
			if(selected){
				$('#label_ttagrupa_<?=$num_pas?>').show().html('*'+selected);
				alert(selected);
			}else{
				$('#label_ttagrupa_<?=$num_pas?>').hide();
			}
		})
		
		$("#id_ciudad_<?=$num_pas?>").change(function(e) {
			trans(<?=$num_pas?>);
		});
		
		$("#id_tservicio_<?=$num_pas;?>").change(function(e) {
			trans(<?=$num_pas;?>);
		});
		
		trans(<?=$num_pas?>);
	});
	</script>
	
	<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="2">
                      <select name="id_ttagrupa_<?= $num_pas ?>" id="id_ttagrupa_<?= $num_pas ?>" style="width:480px;"></select>
                      <label id="label_ttagrupa_<?= $num_pas ?>" for="id_ttagrupa_<?= $num_pas ?>" style="color:red;"></label>
                      </td>
                      <td><button name="agrega=<?= $num_pas ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $num_pas==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>
                  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?= $num_pas ?>" name="datepicker_<?= $num_pas ?>" value="<?= $destinos->Fields('cd_fecdesde1') ?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $destino ?> :</td>
                    <td width="316"><select name="id_ciudad_<?= $num_pas ?>" id="id_ciudad_<?= $num_pas ?>" >
                    <? while(!$ciudad->EOF){ ?>
                    <option value="<?= $ciudad->Fields('id_ciudad') ?>" <? if ($ciudad->Fields('id_ciudad') == $destinos->Fields('id_ciudad')) {echo "SELECTED";} ?>><?= $ciudad->Fields('ciu_nombre') ?></option>
                    <? $ciudad->MoveNext(); } $ciudad->MoveFirst(); ?>
                  </select></td>
                  </tr>
					  <?
						$query_tiposervicio = "SELECT DISTINCT 
												  t.id_tipotrans,
												  tt.`tpt_nombre`
												FROM
												  trans t
												INNER JOIN tipotrans tt
												ON t.`id_tipotrans` = tt.`id_tipotrans`
												WHERE tra_fachasta > NOW() 
												AND t.`tra_estado` = 0
												AND t.`id_area` = 1
												GROUP BY tra_codigo";
						
						$tiposervicio = $db1->SelectLimit($query_tiposervicio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						
						?>
                  <tr valign="baseline">
                    <td align="left"><?= $numtrans ?> :</td>
                    <td><input type="text" name="txt_numtrans_<?= $num_pas ?>" id="txt_numtrans_<?= $num_pas ?>" value="" onchange="M(this)" /></td>
					<td>
					<?	if($_SESSION['idioma'] == 'sp'){
							echo "Tipo Servicio :";
						}
						if($_SESSION['idioma'] == 'po'){
							echo "Tipo Servi&ccedil;o :";
						}
						if($_SESSION['idioma'] == 'en'){
							echo "Type of Service :";
						}

					?>
					</td>
				  <td>
						<select name="id_tservicio_<?=$num_pas;?>" id="id_tservicio_<?=$num_pas;?>" >
							<?	 	
							while(!$tiposervicio->EOF){
							?>
												<option value="<? echo $tiposervicio->Fields('id_tipotrans')?>" <?  if($tiposervicio->Fields('id_tipotrans') == 12){echo "SELECTED";} ?>><? echo $tiposervicio->Fields('tpt_nombre')?></option>
												<?	 	
							$tiposervicio->MoveNext();
							}
							?>
						</select>
				  </td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="3"><input type="text" name="txt_obs_<?= $num_pas ?>" id="txt_obs_<?= $num_pas ?>" value="" onchange="M(this)" style="width:500px;" /></td>
                  </tr>
                </table>
<? }

?>