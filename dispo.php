
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Hotelería 2.0</title>
		<!--meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"-->
        <link href="../h2o/prueba/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
        <link href="../h2o/prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="../h2o/prueba/css/jquery.gritter.css" rel="stylesheet">
        <link href="../h2o/prueba/css/style_gritter.css"  rel="stylesheet">
        <link href="../h2o/prueba/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../h2o/prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
        <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
		<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
        <link href="../h2o/prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../h2o/prueba/js/jquery.niftymodals/css/component.css"/>
        <link rel="stylesheet" href="../h2o/prueba/css/jquery.mCustomScrollbar.css" type="text/css" />
        <link rel="stylesheet" href="../h2o/prueba/css/protip.min.css">
        
        <style type="text/css">

            #datos{
                height : 400px !important;

            }


        </style>
      </head>
    <body>


    <div class="container">

             <div class="row">
        
               
               
                   <div class="row"><br><br></div>
                             
                    <div align='center' class="row">

                        <div class="col-md-12">

                            <div class="col-md-2">
                                <input type="text" class="datepicker desactivados" name="fecha_inicio" id="desde" placeholder="Fecha desde" data-date-format="dd-mm-yyyy">
                            </div>

                            <div class="col-md-2">
                                <input type="text" class="datepicker desactivados" name="fecha_hasta" id="hasta" placeholder="Fecha hasta" data-date-format="dd-mm-yyyy">
                            </div>

                            <div class="col-md-2">
                                <select id="ciudad" name="ciudad" class="form-control select2 pull-left desactivados" ></select>
                            </div>

                            <div class="col-md-2 ">
                                <button id="buscando" class="btn btn-success desactivados">Buscar</button>
                            </div>

                            <div class="col-md-2">
                                    <input type="text" class='form-control kwd_search desactivados' placeholder = "BUSCAR"/>

                            </div>


                        </div>


                        <div class="row">
                        
                             <div class="col-md-12 centered">
                              <br><br><br>


                                <div class="col-md-10 col-md-offset-1">
                                    <div  id="datos"></div>
                                </div>
                            
                             </div>

                        </div>


                    </div>
   
            </div>


        </div>

        <script src="../h2o/prueba/js/jquery.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="../h2o/prueba/js/jquery.gritter.js" type="text/javascript"></script>
        <script src="../h2o/prueba/js/notificaciones.js" type="text/javascript"></script>
        <script src="../h2o/prueba/js/jquery.numeric.js"></script>
        <script src="../h2o/prueba/js/tableHeadFixer.js"></script>
        <script src="../h2o/prueba/js/protip.min.js"></script>
        <script type="text/javascript" src="../h2o/prueba/js/jquery.mCustomScrollbar.concat.min.js"></script>
        
        <script src="../h2o/prueba/js/traer_dispo_por_habitacion.js"></script>
        <script src="../h2o/prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>
        <script type="text/javascript" src="../h2o/prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
        <script type="text/javascript" charset="utf-8" src="../h2o/prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../h2o/prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
        <script src="../h2o/prueba/js/bootstrap-datepicker.js" type="text/javascript"></script>
        
    </body>
</html>