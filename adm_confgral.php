<?php	 	  
require_once('Connections/db1.php');
require_once("secure.php");
require_once('clases/distantis.php');

$funciones = new distantis();

if(isset($_POST['save'])){
	$funciones->insMkGral($db1,$_SESSION['Usuario']->id_usuario,$_POST['markup'], $_POST['datepicker']);
}

$mkGral = $funciones->getMkGral($db1);
$mkGralHist=$funciones->getMkGral($db1,false);


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
	 	<title><?=$agencia->nombre_plataforma;?></title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<script src="js/jquery-3.2.1.min.js"></script>
  		<script src="js/jquery-ui/jquery-ui.js"></script>
    	<link href="css/test.css" rel="stylesheet" type="text/css"/>
    	<link rel="stylesheet" href="js/jquery-ui/jquery-ui.css">
		<script type="text/javascript">

			$(function(){
			    var dates = $("#datepicker").datepicker({
			     	//defaultDate: "+1w",
			   		changeMonth: true,
			   		changeYear:true,
			     	numberOfMonths: 1,
			     	dateFormat: 'dd-mm-yy',
			     	showOn: "button",
					minDate: new Date(<? echo date('Y');?>, <? echo date('m');?> - 1, <? echo date('d');?>),
			      	buttonText: '...'
			    });	
			});
		</script>
	</head>


	<body>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="50%" class="titulo">Configuraci&oacute;n General Sistema</td>
		    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
		  </tr>
		</table>
		<form name="enviar_form2" id="enviar_form2" method="post">
			<table class='mainstream2'>
				<tr>
					<td>
						<table id='filtros'>
							<tr>
								<td>Markup General:</td>
								<td>
								 	<input type="number" name="markup" id="markup" value="<?=$mkGral->Fields('mg_markup') ?>" step='.01' required>
									<font color="red"><b>última actualizaci&oacute;n -> <?php echo $mkGral->Fields("mg_fecha"); ?></b></font>
									&nbsp;, por <?php echo $mkGral->Fields("creador"); ?>
									&nbsp;vigente desde: <input type='text' id='datepicker' name='datepicker' value='<?php echo $mkGral->Fields("vig1_f"); ?>' style='text-align: center;' readonly='readonly'/>
								</td>
							</tr>
							<tr>
								<td colspan="2"> <br><input type="submit" value="Enviar" name="save"> </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br><br>
		</form>
	
		<center>
			<table class="mainstream">
				<tr>
					<th colspan="4"> Hist&oacute;rico Markup General</th>
				</tr>
				<tr> 
					<th>markup</th>
					<th>Fecha Creación</th>
					<th>Vigente desde</th>
					<th>Usuario</th> 
				</tr>

				<? while (!$mkGralHist->EOF){ ?>
					<tr>
						<td> <? echo $mkGralHist->Fields('mg_markup'); ?> </td>
						<td> <? echo $mkGralHist->Fields('mg_fecha'); ?> </td>
						<td> <? echo $mkGralHist->Fields('vig1_f'); ?> </td>
						<td> <? echo $mkGralHist->Fields('creador'); ?> </td>
					</tr>
				 <? $mkGralHist->MoveNext(); 
				 } ?>
			</table>	
		</center>
	</body>
</html>