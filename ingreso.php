<?php	 	 
require_once('Connections/db1.php');
require_once("clases/usuario.php");
 
$usuario =  new Usuario();
@session_start();



$KT_LoginAction = $_SERVER["REQUEST_URI"];
if (isset($_POST["login"])) {
	$llega = $usuario-> checkLogin($db1,$_POST['login'],$_POST['pass']);
	if($llega->estado){
		$_SESSION['Usuario'] = $llega;
		echo "<script>alert('- ".$llega->mensaje.".');</script>";
		if($llega->usu_portal==0){
			if($llego->id_tipo==1){
				$url = "manager.htm";
			}else{
				$url = "compra_opaca.php";
			}

		}else{
			$url = "webservice/index.php";
		}
	}else{
		die ("<meta http-equiv='content-type' content='text/html;charset=UTF-8' /><script>alert('- ".$llega->mensaje.".');window.location='ingreso.php'</script>");
	}
	KT_redir($url);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?=$usuario->nombre_plataforma;?></title>	
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
		<meta name="keywords" content=""></meta>
		<meta name="description" content=""></meta>
		<link rel="stylesheet" href="css/screen-sm.css" media="screen" />
		<link rel="stylesheet" href="css/easy.css" media="screen" />
		<link rel="stylesheet" href="css/easyprint.css" media="print" />
		<script src="js/MainJs.js"></script>
	</head>
	<BODY onLoad="document.getElementById('<? if (isset($_COOKIE['CookieErp'])) {?>pass<? } else { ?>login<? } ?>').focus();" >

		<div id="container" class="login">

			<div id="header">
				<h1><?=$usuario->nombre_plataforma;?></h1>		
			</div>

			<div class="content">
				<div class="main">
					<form method="post" class="login" name="form">	
						<fieldset class="cols">
							<div class="col first">
								<label for="usuario">Usuario</label>
	                    		<INPUT name="login" id="login" value="<?php if(isset($_COOKIE['CookieErp'])){ echo $_COOKIE['CookieErp'];}?>" size="20" maxlength="30" onChange="M(this)" tabindex="1" class="field required">
							</div>
							<div class="col">
								<label for="pass">Password</label>
								<input type="password" name="pass" id="pass" size="20" class="field" />
							</div>
							<div class="col first">
								<label><input type="checkbox" name="mantenermeconectado" value="1" checked="checked" class="required" /> Mantenerme conectado</label>
							</div>									
							<div class="submit">
								<button type="submit">Entrar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>	
		<div class="content">
	</body>
</html>