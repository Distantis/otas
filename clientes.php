<?
require_once('clases/tarifa.php');
require_once('Connections/db1.php');
$usuario = $_SESSION['Usuario'];
$permiso = 501;
require('secure.php');


$tarifa = new Tarifa();

$clientes = $tarifa->getClientes($db1);
$areas = $tarifa->getAreas($db1);

if(isset($_POST['add'])){
	$cliente = $tarifa->getMeThatClient($db1,$_POST['cliente']);
	$respuesta = "";
	$respuesta.="<td>".$_POST['cliente']."</td>";
	$respuesta.="<td>".$cliente['name']."</td>";
	$respuesta.="<td>".$cliente['bd']."</td>";
	$respuesta.="<td><select id='tgandhi_".$_POST['cliente']."'><option value='0' ";
	if($cliente['tghandi']==0)$respuesta.="Selected";
	$respuesta.=">Si</option><option value='1' ";
	if($cliente['tghandi']==1)$respuesta.="Selected";
	$respuesta.=" >No</option></select></td>";

	while(!$areas->EOF){
		$respuesta.="<td><input type='number' step='any' id='markup_".$_POST['cliente']."_".$areas->Fields('id_area')."' value='";
		if(isset($cliente['mk'][$areas->Fields('id_area')])){
			$respuesta.=$cliente['mk'][$areas->Fields('id_area')];
		}
		$respuesta.="' ".(($areas->Fields('area_estado')==0)?'required':'')."></td>";
		$areas->MoveNext();
	}
	$areas->MoveFirst();
	
	$respuesta.="<td><input type='text' id='id_user_".$_POST['cliente']."' value='".$cliente['id_user']."' required></td>
		<td><input type='text' id='opebuyer_".$_POST['cliente']."' value='".$cliente['opebuyer']."' required></td>
		<td><input type='text' id='id_agencia_".$_POST['cliente']."' value='".$cliente['id_agencia']."' required></td>";




	$respuesta.="<td><img src='images/guardar.png' width='20px' onclick=guardar(".$_POST['cliente'].") border='0' alt='Guardar Registro' style='cursor:pointer'></td>";
	die($respuesta);
}


if(isset($_POST['save'])){
	
	$tarifa->ModCliente($db1,$_POST['cliente'],$_POST['tgandhi'],$_POST['markup'],$_POST['id_user'],$_POST['opebuyer'],$_POST['id_agencia']);
	$tarifa->checkDispo = false;
	$tarifa->tarHandler($db1, $_POST['cliente'], null, null, null, null, null, null, null, null, null, null, 0);
	$clientes = $tarifa->getClientes($db1,true);
	$cliente = $clientes[$_POST['cliente']];
	
	$respuesta = "";
	$respuesta.="<td>".$_POST['cliente']."</td>";
	$respuesta.="<td>".$cliente['name']."</td>"; 
	$respuesta.="<td>".$cliente['bd']."</td>";
	$respuesta.="<td>";
	if($cliente['tghandi']==0){
		$respuesta.="Si";
	}else{
		$respuesta.="No";
	}
	$respuesta.="</td>";

	while(!$areas->EOF){
		$respuesta.="<td>";
		if(isset($cliente['mk'][$areas->Fields('id_area')])){
			$respuesta.=$cliente['mk'][$areas->Fields('id_area')];
		}
		$respuesta.="</td>";
		$areas->MoveNext();
	}
	$areas->MoveFirst();
	
	$respuesta.="<td>".$cliente['id_user']."</td>
				<td>".$cliente['opebuyer']."</td>
				<td>".$cliente['id_agencia']."</td>";
	$respuesta.="<td><img src='images/editar.png' onclick=editar(".$_POST['cliente'].") border='0' alt='Editar Registro' style='cursor:pointer'></td>";
	
	die($respuesta);
}

?>
<html>
	<head>
		<title><?=$tarifa->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="css/test.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-3.2.1.min.js"></script>
		<style>
			.modal {
				display:    none;
				position:   fixed;
				z-index:    1000;
				top:        0;
				left:       0;
				height:     100%;
				width:      100%;
				background: rgba( 157, 248, 252, 0.3 ) 
				url('http://i.stack.imgur.com/FhHRx.gif') 
				50% 50% 
				no-repeat;
			}
			body.loading {
				overflow: hidden;   
			}
			body.loading .modal {
				display: block;
			}
    </style>
		<script>
			function editar(id){
					$.ajax({
						type: 'POST',
						url: 'clientes.php',
						data: {
							add: true,
							cliente: id
						},
						success:function(result){
							$("#datos_"+id).html(result);
						},
						error:function(){
							alert("Error al traer los datos");
						}
					});
				
			}
			
			function guardar(id){
				$body = $("body");
				$body.addClass("loading");
				var markups = {};
				$("[id*=markup_"+id).each(function(){
					var area = this.id.replace("markup_"+id+"_","");
					markups[area] = this.value;
				});
				$.ajax({
					type: 'POST',
					url: 'clientes.php',
					data: {
						save: true,
						cliente: id,
						tgandhi: $("#tgandhi_"+id).val(),
						markup: markups,
						id_user: $("#id_user_"+id).val(),
						opebuyer: $("#opebuyer_"+id).val(),
						id_agencia: $("#id_agencia_"+id).val()

					},
			        beforeSend:function(){ 

				        $body = $("body");
				        $body.addClass("loading");
			        },
			        complete:function(){
			          	$body.removeClass("loading");
			        },

					success:function(result){
						$("#datos_"+id).html(result);
						alert("Cambio Guardado");
					},
					error:function(){
						alert("Error al traer los datos");
					}
				});
				$body.removeClass("loading");
			}
			
		</script>
	</head>
	<body> 
		<br><br><br>
		<div class='divlista'>
			<table class='listaBoni'>
				<tr>
					<th  id='thtitulo' colspan="11"><div align="center">Lista Clientes</div></th>
				</tr>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Base de Datos</th>
					<th>Cargar Tarifas a Otas</th>
					<?
					while(!$areas->EOF){
						echo "<th>Markup ".$areas->Fields('area_nombre')."</th>";
						$areas->MoveNext();
					}
					$areas->MoveFirst();
					?>
					
					<th>ID User para Gandhi</th>
					<th>ID Operador Online</th>
					<th>ID Agencia Otas</th>
					<th></th>
				</tr>
				<?foreach($clientes as $id => $cliente){?>

				<tr id="datos_<?=$id?>">
					<td><?=$id?></td>
					<td><?=$cliente['name']?></td>
					<td><?=$cliente['bd']?></td>
					<td><? if($cliente['tghandi']==0)echo "Si";else echo"No";?></td>
					<?
					while(!$areas->EOF){
						echo "<td>".((isset($cliente['mk'][$areas->Fields('id_area')]))?$cliente['mk'][$areas->Fields('id_area')]:'')."</td>";
						$areas->MoveNext();
					}
					$areas->MoveFirst();
					?>
					
					<td><?=$cliente['id_user']?></td>
					<td><?=$cliente['opebuyer']?></td>
					<td><?=$cliente['id_agencia']?></td>


					





					<td>
						
						<img src="images/editar.png" onclick=editar(<?=$id?>) border="0" alt="Editar Registro" style="cursor:pointer">
							
					</td>
				</tr>
				<?}?>
			</table>
		</div>
		<div class="modal"><!-- div para el loading --></div>
	</body>
</html>