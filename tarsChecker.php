<?php
require_once('Connections/db1.php');
require_once('clases/tarifa.php');
require_once('clases/cotizacion.php');
require_once('clases/agencia.php');



$ageClass = new Agencia();
$tarClass = new Tarifa();
$cotClass = new Cotizacion();


?>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../otas/css/w3.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<div id='container' class="w3-sand w3-grayscale" style='height:100%; width:100%'>
		<div style='display: inline-block;'>
			<form name='shitty' method="POST" action="">
				<table class='w3-table w3-small w3-center' style='width:400px'>
					<tr>
						<td>hotdet:<input type='text' name='hotdet' value='<?=isset($_POST['hotdet'])?$_POST['hotdet']:'';?>'></td>
						<td>vendedor: <select name='id_cliente'>
							
						<?
						$clientes = $tarClass->getClientes($db1);
						foreach ($clientes as $id_cliente => $shitty) {
							echo "<option value='".$id_cliente."'";
							if(isset($_POST['id_cliente'])){
								if($_POST['id_cliente'] == $id_cliente){
									echo " SELECTED";
								}
							}
							echo ">".$shitty['name']."</option>";
						}

						?>
						</select></td>
					</tr>
					<tr>
						<td>fecha desde:<input id='fechad' name='fechad' class="w3-input w3-border" type="date" required placeholder="Date" value="<?=(isset($_POST['fechad']))?$_POST['fechad']:date('Y-m-d');?>"/></td>
						<td>fecha hasta:<input id='fechah' name='fechah' class="w3-input w3-border" type="date" required placeholder="Date" value="<?=(isset($_POST['fechah']))?$_POST['fechah']:date('Y-m-d');?>"/></td>
					</tr>

					<tr>
						<td>sgl:<input type='number' name='sgl' value='<?=(isset($_POST['sgl']))?$_POST['sgl']:0?>'></td>
						<td>twin:<input type='number' name='twin' value='<?=(isset($_POST['twin']))?$_POST['twin']:0?>'></td>
					</tr>
					<tr>
						<td>mat:<input type='number' name='mat' value='<?=(isset($_POST['mat']))?$_POST['mat']:0?>'></td>
						<td>tpl:<input type='number' name='tpl' value='<?=(isset($_POST['tpl']))?$_POST['tpl']:0?>'></td>
					</tr>
					<tr>
						<td>comprador:<select name='id_agencia'><?
						$agencias = $ageClass->getMeAgencies($db1, true);
					 	while(!$agencias->EOF){
						 	echo "<option value='".$agencias->Fields('id_agencia')."'";

						 	if(isset($_POST['id_agencia'])){
						 		if($_POST['id_agencia'] == $agencias->Fields('id_agencia')){
						 			echo " SELECTED";
						 		}
						 	}
						 	echo ">".$agencias->Fields('ag_nombre')."</option>";
						 	$agencias->MoveNext();
					 	}
					?></select></td>

					<td><input type='submit' value='GO GO GO'></td></tr>
				</table>
			</form>
		</div>




<?
if(isset($_POST['id_cliente'])){
	$cliente = $tarClass->getMeThatClient($db1, $_POST['id_cliente']);
	$bdcli = $cliente['bd'];
	$namecli = $cliente['name'];
	$strExtraConds = "";
	//columnas especiales que deben ser consideradas por cliente... nemo y esas shits...
	if(isset($cliente['esColls']['hotdet'])){
		$extraConds = array();
		foreach ($cliente['esColls']['hotdet'] as $col => $val){
			$arConds = array();
			$strAux = "(";
			$nums = array();
			foreach ($val as $index=> $valorValido){
				if($valorValido==''){
					array_push($arConds, "IFNULL(".$col.",'') = ''");
				}else{
					if(is_numeric($valorValido)){
						array_push($nums, $valorValido);
					}else{
						array_push($arConds, $col." = '".$valorValido."'");
					}
				}
			}
			if(count($nums)>0){
				array_push($arConds, $col." in (".implode(",", $nums).")");
			}
			$strAux.= implode(" OR ",$arConds);
			$strAux.= ") ";
			array_push($extraConds, $strAux);
		}
		$strExtraConds="if(".implode(" AND ", $extraConds).", 'S','N') as hd_valida,";
	}else{
		$strExtraConds="'S' as hd_valida,";
	}
	$strExtraConds = str_replace("id_cliente", "hd.id_cliente", $strExtraConds);
	$sql = "SELECT 
		$strExtraConds
		hd.id_hotdet, 
		hd.hd_fecdesde,
		hd.hd_fechasta,
		hd.hd_mon,
		IFNULL(hd.hd_sgl, 0) AS hd_sgl,
		IFNULL(hd.hd_dbl,0) AS hd_dbl,
		IFNULL(hd.hd_tpl,0) AS hd_tpl,
		hd.hd_estado,
		hd.id_area,
		hg.id_hotel AS hot_gandhi,
		hdg.id_hotel AS hot_targ,
		hg.id_ciudad AS ciu_gandhi,
		ciu.aplica_iva,
		hg.hot_estado AS hot_estadog,
		hm.id_pk,
		tt.id_tipotar AS tt_gandhi,
		th.id_tipohab AS th_gandhi,
		tt.tt_estado, 
		h.hot_estado,
		h.hot_activo,
		h.hot_sellota,
		hd.hd_iva,
		hdg.id_hotdet AS id_hotdetg,
		h.prepago,
		htt.tt_aplicamkotas,
		hg.hot_nombre,
		htt.tt_nombre,
		th.th_nombre
	FROM $bdcli.hotdet hd 
		INNER JOIN $bdcli.tipohabitacion th ON hd.id_tipohabitacion = th.id_tipohabitacion
		INNER JOIN $bdcli.hotel h ON hd.id_hotel = h.id_hotel
		INNER JOIN ".$tarClass->dbhot.".hotelesmerge hm ON hd.id_hotel = hm.id_hotel_$namecli
		INNER JOIN $bdcli.tipotarifa tt ON hd.id_tipotarifa = tt.id_tipotarifa
		INNER JOIN ".$tarClass->dbhot.".tipotarifa htt ON tt.id_tipotar = htt.id_tipotarifa
		LEFT JOIN ".$tarClass->dbMain.".hotel hg ON hm.id_pk = hg.id_pk
		LEFT JOIN ".$tarClass->dbMain.".ciudad ciu ON hg.id_ciudad = ciu.id_ciudad
		LEFT JOIN ".$tarClass->dbMain.".hotdet hdg ON hd.id_hotdet = hdg.id_hotdet_cliente AND hdg.id_cliente = ".$_POST['id_cliente']."
		WHERE hd.id_area IS NOT NULL AND hd.hd_mon IS NOT NULL AND hd.id_hotdet = ".$_POST['hotdet'];

	$RESgetTars = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	echo "<div style='display: inline-block;'>";
	echo "<table class='w3-sand w3-grayscale w3-table w3-responsive w3-small'>";
	echo "<tr><th>------- primary validation -------</th></tr>";
	$tetengofe = true;
	if($RESgetTars->Fields('tt_gandhi')==''){
		echo "<tr><td>Failure: tt_gandhi is nothing</td></tr>";
		$tetengofe = false;
	}

	if($RESgetTars->Fields('th_gandhi')=='' && $RESgetTars->Fields('th_gandhi')==0){
		echo "<tr><td>Failure: th_gandhi is nothing</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('hot_gandhi')==''){
		echo "<tr><td>Failure: hot_gandhi is nothing</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('hot_estado')!=0){
		echo "<tr><td>Failure: client's hot_estado is closed</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('hot_activo')!=0){
		echo "<tr><td>Failure: client's hot_activo is not active</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('tt_estado')!=0){
		echo "<tr><td>Failure: client's tt_estado is not active</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('hot_estadog')!=0){
		echo "<tr><td>Failure: gandhi's hot_estadog is not active</td></tr>";
		$tetengofe = false;	
	}
	if($RESgetTars->Fields('hot_sellota')!=0){
		echo "<tr><td>Failure: client's hot_estadog is not allowed</td></tr>";
		$tetengofe = false;	
	}

	echo "<tr><th>------ primary validation status: ".(($tetengofe)?'success':'failure')." ------------</th></tr><tr><th>&nbsp;</th></tr>";


	echo "<tr><th>------- secondary validation -------</th></tr>";
	$tetengofe = true;

	if($RESgetTars->Fields('hd_estado')!=0){
		echo "<tr><td>Failure: client's hd_estado is not active</td></tr>";
		$tetengofe = false;
	}
	if($RESgetTars->Fields('hd_valida')!='S'){
		echo "<tr><td>Failure: client's especial validations are not valid ($strExtraConds)</td></tr>";
		$tetengofe = false;
	}

	if($RESgetTars->Fields('hd_mon')==2){
		if($RESgetTars->Fields('hd_iva')==1 && !$tarClass->allowIvaIncluded){
			echo "<tr><td>Failure: clp rate has IVA included. iva included is not currently allowed</td></tr>";
			$tetengofe = false;
		}
	}



	$minValue = ($RESgetTars->Fields('hd_mon')==1)?$tarClass->getMinValueAllowed() : $tarClass->getClpMinValueAllowed();
	if($tarClass->getValidateMinValue()){
		$hd_sgl = (($RESgetTars->Fields('hd_sgl') < $minValue)?0:$RESgetTars->Fields('hd_sgl'));
		$hd_dbl = (($RESgetTars->Fields('hd_dbl') < ($minValue/2))?0:$RESgetTars->Fields('hd_dbl'));
		$hd_tpl = (($RESgetTars->Fields('hd_tpl') < ($minValue/3))?0:$RESgetTars->Fields('hd_tpl'));
		//validacion de valor minimo permitido
		if(($hd_sgl < $minValue && $hd_dbl < ($minValue/2) && $hd_tpl < ($minValue/3))){
			echo "<tr><td>Failure: rate value is lower than minValue allowed for this currency";
			echo "<br>--->MinValue: ".$minValue;
			echo "<br>-->sgl: ".$RESgetTars->Fields('hd_sgl')."  -  dbl: ".$RESgetTars->Fields('hd_dbl')."  -  tpl: ".$RESgetTars->Fields('hd_tpl')."</td></tr>";
			$tetengofe = false;
		}
	}
	
	$calclines = $tarClass->getCalcLines($db1,$_POST['id_cliente']);
	if(!isset($calclines[$_POST['id_cliente']]['hotocu'][$RESgetTars->Fields('hd_mon')]) || !isset($calclines[$_POST['id_cliente']]['cotdes'][$RESgetTars->Fields('hd_mon')])){
		echo "<tr><td>Failure: CalcLines not found</td></tr>";
		$tetengofe = false;
	}

	if($RESgetTars->Fields('id_hotdetg')!=''){
		echo "<tr><td>ID Gandhi: ".$RESgetTars->Fields('id_hotdetg')."</td></tr>";
	}
	
	echo "<tr><th>------ secondary validation status: ".(($tetengofe)?'success':'failure')." ------------</th></tr><tr><th>&nbsp;</th></tr></table></div>";



	echo "<table class='w3-sand w3-grayscale w3-table w3-responsive w3-small'>
	<tr>
		<th>hotel</th>
		<th>tipo</th>
		<th>vigencia</th>
		<th>moneda</th>
		<th>estado</th>
	</tr>
	<tr>
		<td>".$RESgetTars->Fields('hot_nombre')."</td>
		<td>".$RESgetTars->Fields('tt_nombre')." - ".$RESgetTars->Fields('th_nombre')."</td>
		<td>".substr($RESgetTars->Fields('hd_fecdesde'), 0, 10)." - ".substr($RESgetTars->Fields('hd_fechasta'), 0, 10)."</td>
		<td>";
	if($RESgetTars->Fields('hd_mon')==1){
		echo "USD";
	}else{
		echo "CLP";
	}
	echo "</td><td>";
	if($RESgetTars->Fields('hd_estado')==0){
		echo "Activa";
	}else{
		echo "Inactiva";
	}


	echo "</td></tr></table>";


	if($RESgetTars->Fields('hot_gandhi')!=''){
		echo "<table class='w3-sand w3-grayscale w3-responsive w3-table-all w3-hoverable w3-small' style='height:600px'>
		<thead class='w3-light-grey'>
			<tr>
				<th colspan='23' class='w3-light-grey'>---------- matrix validations -------------- </th>
			</tr>";


		$totalRequest = $_POST['sgl'] + $_POST['twin'] + $_POST['mat'] + $_POST['tpl'];
		$fechaHasta = "ADDDATE('".$_POST['fechah']."',-1)";
		$nightDiff="DATEDIFF('".$_POST['fechah']."','".$_POST['fechad']."')";
		
		$moneda = $RESgetTars->Fields('hd_mon');
		
		$id_agencia = $_POST['id_agencia'];
		$arconds1 = array();
		$arconds2 = array();
		if($_POST['sgl']>0){
			array_push($arconds1, "ds_cerrado_sgl = 0");
			array_push($arconds2, "hd_sgl > 0 AND ds_hab1 >= ".$_POST['sgl']);
		}
		if($_POST['twin']>0){
			array_push($arconds1, "ds_cerrado_twn = 0");
			array_push($arconds2, "hd_dbl > 0 AND ds_hab2 >= ".$_POST['twin']);
		}
		if($_POST['mat']>0){
			array_push($arconds1, "ds_cerrado_mat = 0");
			array_push($arconds2, "hd_dbl > 0 AND ds_hab3 >= ".$_POST['mat']);
		}
		if($_POST['tpl']>0){
			array_push($arconds1, "ds_cerrado_tpl = 0");
			array_push($arconds2, "hd_tpl > 0 AND ds_hab4 >= ".$_POST['tpl']);
		}

		$sqlDisp="SELECT 
			hd.id_hotdet, 
			ds_fecha,
			IF((h.hot_habxvender >= ($totalRequest) OR h.hot_habxvender = 0),'si','no') as val_habxvender,
			IF(tt_estado = 0,'si','no') as val_ttestado,
			IF(th_estado = 0,'si','no') as val_thestado,
			IF(th_sellotas = 0,'si','no') as val_thsellotas,
			IF(hd_estado = 0,'si','no') as val_hdestado,
			IF(ha.id_agencia = $id_agencia AND hotxag_estado = 0,'si','no') as val_hotasignado,
			IF(hot_estado = 0,'si','no') AS val_hotestado,
			IF(hot_sellota = 0,'si','no') AS val_hotsellota,
			IF(ds_estado = 0,'si','no') AS val_dsestado,
			IF(ds_cerrado = 0,'si','no') AS val_dscerrado,
			IF(c.estado = 0,'si','no') AS val_cliestado,
			IF(c.trabaja_gandhi = 0,'si','no') as val_clientsell,
			IF(ciu_estado = 0,'si','no') as val_citystate,
			IF((tt_maxnoc >= $nightDiff OR tt_maxnoc = 0),'si','no') as val_maxnocs,
			IF(ds_tarifa = 0,'si','no') as val_rateopen,
			IF(hd_prepago = 0,'si','no') as val_prepago,
			IF(".(implode(" AND ", $arconds1)).",'si','no') as val_habsopen,
			IF(".(implode(" AND ", $arconds2)).",'si','no') as val_habsdispo,
			IF(((tt.tt_espromo = 0 AND tt_minnoc <= DATEDIFF('2018-01-14', '2018-01-12')) OR (tt.tt_espromo = 1 AND tt_minnoc = DATEDIFF('2018-01-14', '2018-01-12'))),'si','no') as val_promo,
			 hd_sgl_cli, hd_dbl_cli, hd_tpl_cli,
			h.id_pk, 
			DATEDIFF(ds_fecha, '".$_POST['fechad']."') AS dayPos,
			hd_sgl, 
			hd_dbl, 
			hd_tpl,
			hd_sgl_cli, hd_dbl_cli, hd_tpl_cli,
			hd_iva, 
			hd_usamarkup, 
			ds_hab1, 
			ds_hab2, 
			ds_hab3, 
			ds_hab4, 
			ds_minnoche, 
			h.id_ciudad,
			idpk_tipohabitacion as id_tipohabitacion, 
			idpk_tipotarifa as id_tipotarifa, 
			hd.id_cliente, 
			id_hotdet_cliente, 
			hot_nombre, 
			hot_email, 
			tt_nombre, 
			th_nombre,
			tt_espromo, 
			tt_minnoc, 
			tt_maxnoc, 
			id_mon, 
			hotxag_estado, 
			ca.modo AS modo_global, 
			hm.ver AS es_global, 
			hot_markdin,
			hd_markup,
			IF(mk.`markdin_estado`=1,1,IFNULL(mk.markdin_valor,1)) AS mark_hot,
			$nightDiff as daysDiff,
			tipocambio_gandhi as tcambio,
			d.id_stock_cli,
			IFNULL(ha_dias,".$cotClass->cancelationDays.") as ha_dias,
			date_add('".$_POST['fechad']."', interval -(IFNULL(ha_dias,".$cotClass->cancelationDays.")) day) as ha_fec,
			hd_refundable,
			id_regimen,
			c.nombre as cli_nombre
			FROM ".$cotClass->dbMain.".hotdet hd 
			INNER JOIN ".$cotClass->dbMain.".dispo d ON hd.id_hotdet = d.id_hotdet
			INNER JOIN ".$cotClass->dbMain.".hotel h ON hd.id_hotel = h.id_hotel
			LEFT JOIN ".$cotClass->dbMain.".hotelxagencia ha ON h.id_hotel = ha.id_hotel AND hd.id_cliente = ha.id_cliente AND ($id_agencia = ha.id_agencia or ha.id_agencia is null)
			INNER JOIN ".$cotClass->dbhot.".hotelesmerge hm ON h.id_pk = hm.id_pk 
			INNER JOIN ".$cotClass->dbhot.".clientes c ON hd.id_cliente = c.id_cliente
			INNER JOIN ".$cotClass->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
			INNER JOIN ".$cotClass->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion
			INNER JOIN ".$cotClass->dbhot.".cadena ca ON hm.id_cadena = ca.id_cadena
			INNER JOIN ".$cotClass->dbMain.".ciudad ciu ON h.id_ciudad = ciu.id_ciudad
			LEFT JOIN  ".$cotClass->dbMain.".mk_hot_area mk ON h.id_hotel = mk.id_hotel AND mk.id_area = $moneda
			LEFT JOIN  ".$cotClass->dbMain.".hotanula af ON h.id_hotel = af.id_hotel
			WHERE ds_fecha BETWEEN '".$_POST['fechad']."' AND $fechaHasta
			AND hd.id_mon = $moneda
			AND hd.id_hotel = ".$RESgetTars->Fields('hot_gandhi')." ORDER BY hot_nombre ASC, hd.id_cliente ASC, th.id_tipohabitacion ASC, id_hotdet ASC, ds_fecha ASC";
			
		$resDisp = $db1->Execute($sqlDisp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		if($resDisp->RecordCount()>0){
			
			echo "<tr>
					<td>vendedor</td>
					<td>id_hotdetG</td>
					<td>info</td>
					<td>fecha</td>
					<td>habxvender</td>
					<td>ttestado</td>
					<td>thestado</td>
					<td>thsellotas</td>
					<td>hdestado</td>
					<td>hotasignado</td>
					<td>hotestado</td>
					<td>hotsellota</td>
					<td>dsestado</td>
					<td>dscerrado</td>
					<td>cliestado</td>
					<td>clientsell</td>
					<td>citystate</td>
					<td>minnocs</td>
					<td>maxnocs</td>
					<td>rateopen</td>
					<td>prepago</td>
					<td>habsopen</td>
					<td>valpromo</td>
					<td>Disp.Normal/global</td>
					<td>values</td>
				</tr></thead>";
			while(!$resDisp->EOF){
				$tienedisp=(($resDisp->Fields('ds_hab1')>=$_POST['sgl']) && ($resDisp->Fields('ds_hab2')>=$_POST['twin']) && ($resDisp->Fields('ds_hab3')>=$_POST['mat']) && ($resDisp->Fields('ds_hab4')>=$_POST['tpl']));
				if($resDisp->Fields('ds_minoche') >= $resDisp->Fields('tt_minnoc')){
					$minimoNoches = $resDisp->Fields('ds_minoche');
				}else{
					$minimoNoches = $resDisp->Fields('tt_minnoc');
				}
				if(!$tienedisp && $resDisp->Fields('es_global')==0){
					if(!isset($globDisp[$resDisp->Fields('id_pk')])){
						//query global 2.0... trae todas las tarifas autorizadas para venta del hotel y las deja guardadas en el arreglo
						$sqlGlob = "SELECT sc.id_stock_global,
							sc.id_tipohab,
							sc_fecha,
							sc.id_pk,
							sc_minnoche,
							sc_hab1 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab1,0),0))) AS ds_hab1,
							sc_hab2 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab2,0),0))) AS ds_hab2,
							sc_hab3 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab3,0),0))) AS ds_hab3,
							sc_hab4 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab4,0),0))) AS ds_hab4
							FROM ".$cotClass->dbhot.".stock_global sc 
							LEFT JOIN ".$cotClass->dbhot.".hotocu hc ON
							sc.id_stock_global = hc.id_stock_global 
							INNER JOIN ".$cotClass->dbhot.".tipohabitacion th ON
							sc.id_tipohab = th.id_tipohabitacion 
							INNER JOIN ".$cotClass->dbhot." .hotdet hd ON
							sc.id_hotdet = hd.id_hotdet
							WHERE sc_estado = 0
							AND sc_cerrado = 0 
							AND hd_estado = 0
							AND sc.id_pk = ".$resDisp->Fields('id_pk')."
							AND th_sellotas = 0
							AND sc_fecha BETWEEN '".$_POST['fechad']."' AND $fechaHasta
							GROUP BY sc.id_stock_global, sc_fecha
							ORDER BY id_tipohab ASC, sc_fecha ASC";       
						$resGlob = $db1->Execute($sqlGlob) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
						if($resGlob->RecordCount()>0){
							$globDisp = array();
							while(!$resGlob->EOF){
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab1'] = $resGlob->Fields('ds_hab1');
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab2'] = $resGlob->Fields('ds_hab2');
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab3'] = $resGlob->Fields('ds_hab3');
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab4'] = $resGlob->Fields('ds_hab4');
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['minnoc'] = $resGlob->Fields('sc_minnoche');
								$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['idscglob'] = $resGlob->Fields('id_stock_global');
								$resGlob->MoveNext();
							}
						}else{
							$globDisp[$resDisp->Fields('id_pk')] = -1;
						}
					}
					$singleNeed = 0;
					$twinNeed = 0;
					$matNeed = 0;
					$tplNeed = 0;
					

					if(isset($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')])){
						$stockEnjoy = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1'];
						if($resDisp->Fields('ds_hab1')<$_POST['sgl']){
							$singleNeed = $_POST['sgl']-$resDisp->Fields('ds_hab1');
						}
						if($resDisp->Fields('ds_hab2')<$_POST['twin']){
							$twinNeed=$_POST['twin']-$resDisp->Fields('ds_hab2');
						}
						if($resDisp->Fields('ds_hab3')<$_POST['mat']){
							$matNeed=$_POST['mat']-$resDisp->Fields('ds_hab3');
						}
						if($resDisp->Fields('ds_hab4')<$_POST['tpl']){
							$tplNeed=$_POST['tpl']-$resDisp->Fields('0ds_hab4');
						}
						if($resDisp->Fields('modo_global')==1){
							$tienedisp = (($singleNeed + $twinNeed + $matNeed + $tplNeed)<=$stockEnjoy);
						}else{
							$tienedisp=(
								($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1']>=$singleNeed) && 
								($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab3']>=$twinNeed) && 
								($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab2']>=$matNeed) && 
								($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab4']>=$tplNeed)
							);
						}
						if($tienedisp){
							$usaGlobal = true;
							if($minimoNoches < $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['minnoc']){
								$minimoNoches = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['minnoc'];
							}
						}
					}else{
						$tienedisp=false;
					}
				}

				if($minimoNoches>$resDisp->Fields('daysDiff')){
					$valminocs = 'no';
				}else{
					$valminocs = 'si';
				}

				echo "<tr";
				if($resDisp->Fields('id_hotdet_cliente') == $_POST['hotdet']){
					echo " class='w3-yellow'";
				}
				echo ">";
				echo "<td>".$resDisp->Fields('cli_nombre')."</td>";
					echo "<td>".$resDisp->Fields('id_hotdet')." (".$resDisp->Fields('id_hotdet_cliente').")</td>";
					echo "<td>".$resDisp->Fields('tt_nombre')." - ".$resDisp->Fields('th_nombre')."</td>";
					echo "<td>".$resDisp->Fields('ds_fecha')."</td>";
					echo "<td>".$resDisp->Fields('val_habxvender')."</td>";
					echo "<td>".$resDisp->Fields('val_ttestado')."</td>";
					echo "<td>".$resDisp->Fields('val_thestado')."</td>";
					echo "<td>".$resDisp->Fields('val_thsellotas')."</td>";
					echo "<td>".$resDisp->Fields('val_hdestado')."</td>";
					echo "<td>".$resDisp->Fields('val_hotasignado')."</td>";
					echo "<td>".$resDisp->Fields('val_hotestado')."</td>";
					echo "<td>".$resDisp->Fields('val_hotsellota')."</td>";
					echo "<td>".$resDisp->Fields('val_dsestado')."</td>";
					echo "<td>".$resDisp->Fields('val_dscerrado')."</td>";
					echo "<td>".$resDisp->Fields('val_cliestado')."</td>";
					echo "<td>".$resDisp->Fields('val_clientsell')."</td>";
					echo "<td>".$resDisp->Fields('val_citystate')."</td>";
					echo "<td>$valminocs</td>";
					echo "<td>".$resDisp->Fields('val_maxnocs')."</td>";
					echo "<td>".$resDisp->Fields('val_rateopen')."</td>";
					echo "<td>".$resDisp->Fields('val_prepago')."</td>";
					echo "<td>".$resDisp->Fields('val_habsopen')."</td>";
					echo "<td>".$resDisp->Fields('val_promo')."</td>";
					echo "<td>".$resDisp->Fields('val_habsdispo')." - ".(($tienedisp)?'si':'no')."</td>";
					echo "<td>s:".$resDisp->Fields('hd_sgl')." - d:".$resDisp->Fields('hd_dbl')." - t:".$resDisp->Fields('hd_tpl')."</td>";
				echo "</tr>";
				$resDisp->MoveNext();
			}
		}
	}
}


?>
</div>
</body>