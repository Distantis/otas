<?php
require_once('Connections/db1.php');
require_once('clases/agencia.php');

$agenClass = new Agencia();
if(isset($_GET['id_agencia'])){
	$id_agencia = $_GET['id_agencia'];
}else{
	$id_agencia = $_POST['id_agencia'];
}
$agencia = $agenClass->getMeThatAgency($db1,$id_agencia,true);


if(isset($_POST['btn_submit'])){
	if(!$agenClass->validateSuperPassword($_POST['password'])){
		die("<script>alert('Password incorrecto')</script>");
	}else{
		while(!$agencia->EOF){
			$id_mon = $agencia->Fields('id_mon');
			$vars['shoutat'] = $_POST["txt_alertat_$id_mon"];
			$vars['stopsell'] = $_POST["stopsell_$id_mon"];

			if($_POST['new'][$agencia->Fields('id_mon')]=='s'){
				$vars['restante'] = $_POST["txt_inicial_$id_mon"];
				$vars['inicial'] = $_POST["txt_inicial_$id_mon"];
			}else{
				//updates
				$vars['restante'] = $agencia->Fields('creag_restante');
				if(isset($_POST["txt_morecredit_$id_mon"])){
					if($_POST["txt_morecredit_$id_mon"]!=0){
						$vars['restante']+= $_POST["txt_morecredit_$id_mon"];
					}
					if($vars['restante']>$agencia->Fields('creag_inicial')){
						$vars['inicial'] = $vars['restante'];
					}
				}
			}
			$agenClass->manageAgencyCredit($db1,$id_agencia,$id_mon,$vars);
			$agencia->MoveNext();
		}
		$agencia = $agenClass->getMeThatAgency($db1,$id_agencia,true);
	}
}
?>
<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   		<link href="css/test.css" rel="stylesheet" type="text/css" />
    	<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/MainJs.js"></script>
		<script type="text/javascript">
			function enableMoreCredit(id){
				if($("[name=chk_addcredit][value="+id+"]").prop('checked')){
					$("[name=txt_morecredit_"+id+"]").removeAttr('disabled');
				}else{
					$("[name=txt_morecredit_"+id+"]").attr('disabled','true');
					$("[name=txt_morecredit_"+id+"]").val("0");
				}
			}




		</script>




	</head>
	<body>
		<div><center>Credito Agencia <?=ucfirst(strtolower($agencia->Fields('ag_nombre')))?></center>
			<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
			<input type='hidden' name='id_agencia' value='<?=$id_agencia?>'>
			<?
			while(!$agencia->EOF){
			?>
				<div style='display: inline-block;'>
		  			<table class='mainstream'>
		  				<input type='hidden' name='new[<?=$agencia->Fields('id_mon')?>]' value='<?if($agencia->Fields('creag_inicial')==''){echo "s";}else{echo "n";} ?>'>
		  				<tr>
		  					<th>Moneda:</th>
		  					<td><?=$agencia->Fields('mon_nombre')?></td>
		  					<th>Credito total:</th>
		  					<td>
		  						<?	
			  					echo "<input type='text' name='txt_inicial_".$agencia->Fields('id_mon')."' value='".$agencia->Fields('creag_inicial')."'";
			  					if($agencia->Fields('creag_inicial')!=''){
			  						echo "disabled";
			  					}
			  					echo "/>";
			  					?>
		  					</td>
		  				</tr>
		  				<tr>
		  					<th>Credito restante:</th>
		  					<td>
		  						<input type='text' name='txt_credrest_<?=$agencia->Fields('id_mon')?>' value='<?if($agencia->Fields('creag_restante')==''){echo '0';}else{echo $agencia->Fields('creag_restante');}?>' disabled/>
		  						<?
			  					if($agencia->Fields('creag_restante')!=''){
			  						echo "<input type='text' name='txt_morecredit_".$agencia->Fields('id_mon')."' value='0' disabled/>";
			  						echo "<input type='checkbox' name='chk_addcredit' value='".$agencia->Fields('id_mon')."' onChange='enableMoreCredit(".$agencia->Fields('id_mon').")'/> añadir credito";
			  					}
			  					?>
		  					</td>
		  					<th>Alertar a los:</th>
		  					<td>
		  						<input type='text' name='txt_alertat_<?=$agencia->Fields('id_mon');?>' value='<?if($agencia->Fields('creag_shoutat')==''){echo "0";}else{echo $agencia->Fields('creag_shoutat');}?>'/>
		  					</td>
		  				</tr>
		  				<tr>
		  					<th>Detener Venta:</th>
		  					<td>
		  						<input type='radio' name='stopsell_<?=$agencia->Fields('id_mon')?>' value='0' <?if($agencia->Fields('creag_stopsell')==0){echo 'checked';} ?> /> si
		  						<input type='radio' name='stopsell_<?=$agencia->Fields('id_mon')?>' value='1'<?if($agencia->Fields('creag_stopsell')==1){echo 'checked';}?>/>no
		  					</td>
		  				</tr>
		  			</table>
			  	</div>
	  		
				<?
				$agencia->MoveNext();
			}
			?>
			<table>
				<tr>
					<td>Contraseña:</td>
					<td><input type='password' name='password'></td>
					<td><input type='submit' value='Guardar cambios' name='btn_submit'></td>
				</tr>
			</table>
	  		</form>
	  	</div>

	</body>