<?php
//Connection statement
require_once('Connections/db1.php');
require_once('clases/usuario.php');
require_once('clases/agencia.php');

$agencia = new Agencia();
$usuario = new Usuario();

require('secure.php');
$data = array();
if(isset($_POST['sel_estado'])){
  if($_POST['sel_estado']!=''){
    $data['usu_estado']=$_POST['sel_estado'];
  }
}
if(isset($_POST['txt_id'])){
  if($_POST['txt_id']!=''){
    $data['id_usuario'] = $_POST['txt_id'];
  }
}
  
if(isset($_POST['txt_nombre'])){
  if($_POST['txt_nombre']!=''){
    $data['usu_nombre']=$_POST['txt_nombre'];
  }
}
if(isset($_POST['txt_login'])){
  if($_POST['txt_login']!=''){
    $data['usu_login']=$_POST['txt_login'];
  }
}
if(isset($_POST['sel_agencia'])){
  if($_POST['sel_agencia']!=0){
    $data['id_agencia']=$_POST['sel_agencia'];
  }
}


if($_SESSION['Usuario']->id_tipo==1){
  $id_agencia = null;
}else{
  $id_agencia = $_SESSION['Usuario']->id_agencia;
}
$agencias = $agencia->getMeAgencies($db1, true, '', null,$id_agencia);
$listado = $usuario->getUsers($db1,$data);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?=$agencia->nombre_plataforma;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/test.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
  </head>
<script type="text/javascript">

  

</script>


<body onLoad="document.form.txt_nombre.focus();">
  <table>
    <tr>
      <td class="titulo">Buscar Usuarios</td>
    </tr>
  </table>
<table class='mainstream2' >
  <form id="form" name="form" method="post" action="">
    <tr>
      <td>
        <table id="filtros">
          <tr>
            <td>ID:</td>
            <td><input type='text' name='txt_id' value='<? if(isset($_POST['txt_id'])){ echo $_POST['txt_id'];} ?>'></td>
            <td>Login:</td>
            <td><input type='text' name='txt_login' value='<? if(isset($_POST['txt_login'])){ echo $_POST['txt_login'];} ?>'><td>
            <td>Nombre:</td>
            <td><input type="text" name="txt_nombre" value="<? if(isset($_POST['txt_nombre'])){ echo $_POST['txt_nombre'];} ?>"></td>
           
            
          </tr>
          <tr>
            <td>Agencia:</td>
            <td>
              <select name="sel_agencia" id="sel_agencia">
                <option value="">-= TODAS =-</option>
                <?php
                while(!$agencias->EOF){
                  ?>
                  <option value="<?php echo $agencias->Fields('id_id_agencia')?>" <?php 
                    if(isset($_POST['sel_agencia'])){
                      if ($agencias->Fields('id_agencia') == $_POST['sel_agencia']) {
                        echo "SELECTED";
                      }
                    } ?>
                  ><?php echo ucfirst(strtolower($agencias->Fields('ag_nombre')))?></option>
                  <?php
                  $agencias->MoveNext();
                }
                $agencias->MoveFirst();
                ?>
              </select>
            </td>
             <td>Activos :</td>
            <td>
              <select name="sel_estado" id="sel_estado">
                <option value="" <? if(isset($_POST['sel_estado'])){if($_POST['sel_estado'] == ''){?> selected <? }}?>>Todos</option>
                <option value="0" <? if(isset($_POST['sel_estado'])){if($_POST['sel_estado'] == '0'){?> selected <? }}?>>Activos</option>
                <option value="1" <? if(isset($_POST['sel_estado'])){if($_POST['sel_estado'] == '1'){?> selected <? }}?>>Inactivos</option>
              </select>
            </td>
            <td colspan="2">
              <button name="busca" type="submit" >Buscar</button>
              <button name="limpia" type="button" onClick="window.location='musu_search.php'">Limpiar</button>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </form>
</table>

<br>


<table class='listaBoni'  >
  <thead>
    <tr>
      <th>N&ordm;</th>
      <th>ID</th>
      <th>Nombre</th>
      <th>Apellidos</th>
      <th>Agencia</th>
      <th>Email</th>
      <th>Login</th>
      <th>Password</th>
      <th>Estado</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php
    while (!$listado->EOF) {
      ?>
      <tr title='N°<?php echo $listado->CurrentRow()+1;?>' >
        <th><?php echo $listado->CurrentRow()+1;?></th>
        <td align="center"><?php echo $listado->Fields('id_usuario'); ?></td>
        <td><?php echo $listado->Fields('usu_nombre'); ?></td>
        <td><?php echo $listado->Fields('usu_pat')." ".$listado->Fields('usu_mat'); ?></td>
        <td><?php echo $listado->Fields('ag_nombre'); ?></td>
        <td><?php echo $listado->Fields('usu_mail'); ?></td>
        <td><?php echo $listado->Fields('usu_login');?></td>
        <td><?php 
            
            if($_SESSION['Usuario']->id_tipo==1 || $listado->Fields('id_agencia')==$_SESSION['usuario']->id_agencia){
              echo $listado->Fields('usu_password');
            }else{
              echo "********";
            }

          ?>


        </td>
        
        <td>
          <input type='button' onClick='stateSwitcher(<?=$listado->Fields('usu_estado');?>, this)' class=
            <?
            if($listado->Fields('usu_estado') == 0){
              echo "'btnEstados btnActivo' value='Activo'";
            }else{
              echo "'btnEstados btnInactivo' value='Inactivo'";
            }?>
          >
        <td align="center"><a href="musu_add.php?id_usuario=<?php echo $listado->Fields('id_usuario')?>"><img src="images/editar.png" border='0' alt="Editar Registro"></a> </td>
      </tr>
      <?php 
      $listado->MoveNext(); 
    }
    ?>
  </tbody>
</table>
<?php
$listado->Close();
?>
</body>
</html>