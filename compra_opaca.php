<?php
include("Connections/db1.php");
include("/var/www/otas/clases/cotizacion.php");
include("/var/www/otas/clases/language.php");
include("/var/www/otas/clases/usuario.php");
include('/var/www/otas/clases/agencia.php');
$ageClass = new Agencia();
$cotClass = new Cotizacion();
$result = new \stdClass();
$userClass = new Usuario();
$lanClass = new Language();

//$db1->debug=true;
//$cotClass->debug=true;
include('/var/www/otas/secure_opaco.php');

//si la session de agencia sigue sin estar seteada aqui es porque el timeout de la sesion se fue a la chucha.
if(!isset($_SESSION['idagencia'])){
	$result->status = 'error';
	$result->data = $lanClass->msg_timeout;
	die(json_encode($result));
}

if(isset($_POST['btnsubmit'])){
	
	//asi se transforma esta mierda (json) en arreglo.
	$arbtnstr = str_replace('\"', '"',$_POST['btnstr']);
	$arbtnstr = json_decode($arbtnstr,true);
	
	$habs = array();
	$paxcount = 0;
	if($arbtnstr['s']>0){
		$habs['sgl'] = $arbtnstr['s'];
		$paxcount += $arbtnstr['s'];
	}
	if($arbtnstr['tw']>0){
		$habs['twin'] =$arbtnstr['tw'];
		$paxcount += $arbtnstr['tw'] * 2;
	}
	if($arbtnstr['m']>0){
		$habs['mat'] = $arbtnstr['m'];
		$paxcount += $arbtnstr['m'] * 2;
	}
	if($arbtnstr['tp']>0){
		$habs['tpl'] = $arbtnstr['tp'];
		$paxcount += $arbtnstr['tp'] * 3;
	}
	$paxes = array();
	for ($i=0; $i < $paxcount; $i++) { 
		array_push($paxes, array("name"=>$_POST['namepax'][$i], "lastname"=>$_POST['lnamepax'][$i], "passport"=>$_POST['passport'][$i], "countryid"=>$_POST['countrypax'][$i]));
	}
	//validamos de donde viene este weon
	if(($arbtnstr['ex'] && ($cotClass->nationalCountryID == $paxes[0]['countryid'])) || (!$arbtnstr['ex'] && ($cotClass->nationalCountryID != $paxes[0]['countryid']))){
		die("<script>alert('".$lanClass->msg_wrongcountry."');document.location.href='compra_opaca.php'</script>");
	}

	//mandamos a buscar la dispo pa esta basura con lo que mandó en el post.
	$chkmatriz = $cotClass->matrizDisponibilidadG($db1, $arbtnstr['c'], $arbtnstr['fd'], $arbtnstr['fh'], $_SESSION['idagencia'], $habs, $arbtnstr['ho'], $arbtnstr['th'], $arbtnstr['ex']);
	//$db1->debug=true;
	//si lo que viene en la matriz coincide en precio... le damos pa adelante.
	if($chkmatriz[$arbtnstr['ho']]['info']['habs'][$arbtnstr['th']]['totValue'] == $arbtnstr['tv']){
		$id_area = (($arbtnstr['ex']==true)?1:2);
		//nos aseguramos de que en la matriz solo quede 1 resultado para que no quede la zorra en cotCreator.
		if(count($chkmatriz)>1){
			$auxmatriz = array();
			$auxmatriz[$arbtnstr['ho']]['info']['habs'][$arbtnstr['th']] = $chkmatriz[$arbtnstr['ho']]['info']['habs'][$arbtnstr['th']];
			$auxmatriz[$arbtnstr['ho']]['habs'][$arbtnstr['th']] = $chkmatriz[$arbtnstr['ho']]['habs'][$arbtnstr['th']];
			$chkmatriz = $auxmatriz;
		}

		$markup_comprador = 1;
		//si el cliente esta seteado, tenemos que crear la reserva comprador en el cliente. (have fun tracking this shit).
		//if(isset($_SESSION['id_cliente'])){
		//	$iscotclient = true;
		//	$cliente = $cotClass->getMeThatClient($db1,$_SESSION['id_cliente']);
		//	$idcotbuyer = $cotClass->createBuyerCot($db1,$cliente,$id_area);
		//	if($idcotbuyer==null){
		//		die("<script>alert('".$lanClass->msg_couldntcreatecotbuyer."');document.location.href='compra_opaca.php'</script>");
		//	}
		//	$extras['clientcode'] = $idcotbuyer;
		//	//validamos si hay que aplicar markup de comprador sobre esta basura.
		//	if($_SESSION['applymk']){	
		//		$mkups = $cotClass->getMkCompraOpaca($db1, $cliente, $arbtnstr['c'], $id_area, $arbtnstr['ho']);
		//		$markup_comprador = $mkups->Fields('mkco_markup');
		//	}
		//}else{
		$iscotclient = false;
		$extras['clientcode'] = $_POST['txt_correlativo'];
		$extras['id_subag'] = $_POST['subagen'];
		//}
		$extras['notes'] = $_POST['observ'];
		$extras['fullcredit'] = false;
		if($chkmatriz[$arbtnstr['ho']]['info']['habs'][$arbtnstr['th']]['directa']==0 && isset($_POST['fullcredit'])){
			if($_POST['fullcredit']==1){
				$extras['fullcredit'] = true;
			}
		}
		$result = $cotClass->cotCreator($db1, $chkmatriz, $habs, $paxes, $_SESSION['iduseragencia'], $arbtnstr['c'],$extras,null,$iscotclient);
		if(isset($result['bookingcode'])){
			if(isset($_SESSION['id_cliente'])){
				$buyerok = $cotClass->finishBuyerCot($db1,$cliente,$idcotbuyer,$result['bookingcode'],$markup_comprador);
				if($buyerok){
					$vars['cotbuyer'] = $idcotbuyer;
					$cotClass->redirectByPost('detalle_opaca.php', $vars,$lanClass->msg_cotcreated);
				}else{
					//decidio crear la reserva en gandhi pero no en el comprador.
					die("<script>alert('".$lanClass->msg_cotcreatedwitherrors.$result['bookingcode']."')</script>");
				}
			}else{
				$vars['id_cot'] = $result['bookingcode'];
				$cotClass->redirectByPost('detalle_opaca.php', $vars,$lanClass->msg_cotcreated);
			}
		}
	}else{
		//matamos esta wea por cambio de tarifa disponible.
		die("<script>alert('".$lanClass->msg_dispoconsumed."');document.location.href='compra_opaca.php'</script>");
	}
}

if(isset($_POST['flag'])){
	switch ($_POST['flag']){
		case 'finddispo':
			$result->status = 'ok';
		
			$matriz = $cotClass->matrizDisponibilidadG($db1, $_POST['ciudad'], $_POST['fecdesde'], $_POST['fechasta'], $_SESSION['idagencia'], $_POST['habs'], null, null, $_POST['extranjero']);
			if(!$matriz){
				$result->status = 'error';
				$result->data = null;
				die(json_encode($result));
			}
			if($_SESSION['applymk']){
				$cliente = $cotClass->getMeThatClient($db1,$_SESSION['id_cliente']);
				$mkups = $cotClass->getMkCompraOpaca($db1, $cliente, $_POST['ciudad'], (($_POST['extranjero']==true)?1:2));
				while(!$mkups->EOF){
					$armkups[$mkups->Fields('id_pk')] = $mkups->Fields('mkco_markup');
					$mkups->MoveNext();
				}
			}
			$resultData = [];
			//esta mierda es pa devolver la funcking matriz sin tanta información, ordenada y calculada en caso de que aplique markup extra.
			foreach($matriz as $id_hotel => $hotel_data){
				//echo $id_hotel." - ".$hotel_data['info']['hotdesc']."<br>";
				$resultData[$id_hotel]['info']['id_hotel'] = $id_hotel;
				$resultData[$id_hotel]['info']['id_pk'] = $hotel_data['info']['id_pk'];
				$resultData[$id_hotel]['info']['direccion'] = utf8_encode($hotel_data['info']['direccion']);
				$resultData[$id_hotel]['info']['name'] = utf8_encode($hotel_data['info']['name']);
				$resultData[$id_hotel]['info']['deadline'] = $hotel_data['info']['deadline'];
				$resultData[$id_hotel]['info']['checkin'] = $hotel_data['info']['checkin'];
				$resultData[$id_hotel]['info']['checkout'] = $hotel_data['info']['checkout'];
				$resultData[$id_hotel]['info']['hotimg'] = $hotel_data['info']['hotimg'];
				$resultData[$id_hotel]['info']['hotweb'] = $hotel_data['info']['hotweb'];
				$resultData[$id_hotel]['info']['hotdesc'] = utf8_encode($hotel_data['info']['hotdesc']);
				$resultData[$id_hotel]['info']['amenities'] = str_replace(",", ", ",  $hotel_data['info']['amenities']);
				



				foreach($hotel_data['info']['habs'] as $id_tipohab => $habs_info){
					$arhab['totValue'] = $habs_info['totValue'];
					if($_SESSION['applymk']){
						$arhab['totValue'] = round($arhab['totValue']/$armkups[$hotel_data['info']['id_pk']], (($_POST['extranjero'])?$cotClass->decimales:0));
					}
					$arhab['aplan'] = $habs_info['aplan'];
					$arhab['thname'] = $habs_info['thname'];
					$arhab['directa'] = $habs_info['directa'];
					$arhab['prepago'] = $habs_info['prepago'];
					if($_POST['debug']){
						//idcliente que pasa el hotdet
						$arhab['debug'] = "c-".$hotel_data['habs'][$id_tipohab][$_POST['fecdesde']]['idcliente'];
						//id del hotdet en gandhi
						$arhab['debug'].= "|1hdg-".$hotel_data['habs'][$id_tipohab][$_POST['fecdesde']]['hotdet'];
						//id del hotdet cliente
						$arhab['debug'].= "|2hdc-".$hotel_data['habs'][$id_tipohab][$_POST['fecdesde']]['hotdet_cli'];
						//si ocupa o no stock global
						$arhab['debug'].= "|1ug-".((isset($hotel_data['habs'][$id_tipohab][$_POST['fecdesde']]['idsl']))?'S':'N');
						//valor total original
						$arhab['debug'].= "|tv-".$habs_info['totValue'];
						//directa
						$arhab['debug'].="|di-".$habs_info['directa'];
						//prepago
						$arhab['debug'].="|prep-".$habs_info['prepago'];
						if($_SESSION['applymk'] ){
							//en caso de aplicar, el markup extra agregado
							$arhab['debug'].= "|mco-".$armkups[$hotel_data['info']['id_pk']];
						}
					}else{
						$arhab['debug'] = "";
					}
					
					$strhabs = "|s:".((isset($_POST['habs']['sgl']))?$_POST['habs']['sgl']:0);
					$strhabs.= "|tw:".((isset($_POST['habs']['twin']))?$_POST['habs']['twin']:0);
					$strhabs.= "|m:".((isset($_POST['habs']['mat']))?$_POST['habs']['mat']:0);
					$strhabs.= "|tp:".((isset($_POST['habs']['tpl']))?$_POST['habs']['tpl']:0);
					$btnstr = "c:".$_POST['ciudad']."|fd:".$_POST['fecdesde']."|fh:".$_POST['fechasta'].$strhabs."|ho:".$id_hotel."|th:".$id_tipohab."|ex:".$_POST['extranjero']."|tv:".$habs_info['totValue']."|di:".$habs_info['directa']."|afc:".$_POST['agcredit'];
					$arhab['btnstr'] = $btnstr;
					if(isset($resultData[$id_hotel]['info']['habs'])){
						for ($i=count($resultData[$id_hotel]['info']['habs'])-1; $i >=0; $i--){ 
							if($resultData[$id_hotel]['info']['habs'][$i]['totValue'] > $arhab['totValue']){
								$resultData[$id_hotel]['info']['habs'][$i+1] = $resultData[$id_hotel]['info']['habs'][$i];
								$resultData[$id_hotel]['info']['habs'][$i] = $arhab;
							}else{
								$resultData[$id_hotel]['info']['habs'][$i+1] = $arhab;
								$i=-1;
							}
						}
					}else{
						$resultData[$id_hotel]['info']['habs'] = array();
						array_push($resultData[$id_hotel]['info']['habs'], $arhab);
					}
				}
			}
			$result->data = $resultData;
			die(json_encode($result));
		break;
	}
}


$regimes = $cotClass->GetMealPlan($db1);
$countries = $cotClass->getCountrys($db1, false);
$arregimes = array();
while(!$regimes->EOF){
	$arregimes[$regimes->Fields('id_regimen')] = $regimes->Fields("nom_regimen".(($_SESSION['language']=='SP')?'':'_en'));
	$regimes->MoveNext();
}
$arcountries = array();
while(!$countries->EOF){
	$arcountries[$countries->Fields('id_pais')] = utf8_encode($countries->Fields("pai_nombre".(($_SESSION['language']=='SP')?'':'_en')));
	$countries->MoveNext();
}
$agencyData = $ageClass->getMeThatAgency($db1,$_SESSION['idagencia']);
$subagencies = $ageClass->getSubAgenciesByAgency($db1, $_SESSION['idagencia']);
include("superior.php");
?>

<html>
	<head>
		<title><?=$cotClass->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../otas/css/w3.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../otas/css/fontawesome-all.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
		<script src="../otas/js/jquery-3.2.1.min.js"></script>
		<script src="../otas/js/jquery-ui/jquery-ui.js"></script>
		<script src="../otas/js/MainJs.js"></script>
		<style>
			html, body {
			    height: 100%;
			    font-family: "Inconsolata", sans-serif;
			}
			input[type=date]::-webkit-inner-spin-button {
    			-webkit-appearance: none;
    			display: none;
			}
			.modal {
			    display:    none;
			    position:   fixed;
			    z-index:    1000;
			    top:        0;
			    left:       0;
			    height:     100%;
			    width:      100%;
			    background: rgba( 157, 248, 252, 0.3 ) 
			    url('http://i.stack.imgur.com/FhHRx.gif') 
			    50% 50% 
			    no-repeat;
			}
			body.loading {
			    overflow: hidden;   
			}
			body.loading .modal {
			    display: block;
			}
		</style>

		<script type="text/javascript">
			var regimes = <?=json_encode($arregimes);?>;
			var countries = <?=json_encode($arcountries);?>;
			var fichas= [];
			var modal = document.getElementById('d_ficha');

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (event.target == modal) {
			    modal.style.display = "none";
			  }
			}

			$(function(){
			    $("#dpfec_1").change(function(){
			    	$("#dpfec_2").attr("min",$("#dpfec_1").val());
			    	if($("#dpfec_1").val()>$("#dpfec_2").val()){
			    		$("#dpfec_2").val($("#dpfec_1").val());
			    	}
			    });
			    $("#dpfec_2").change(function(){
			    	$("#dpfec_1").attr("max",$("#dpfec_2").val());
			    });
			});




			function showHotSheet(idhot){
				console.log(fichas[idhot]);
				$("#f_hotname").text(window.fichas[idhot]['hotname']);
				$("#f_img").attr("src", window.fichas[idhot]['hotimg']);
				$("#f_web").attr("href", window.fichas[idhot]['hotweb']);
				$("#f_web").text(window.fichas[idhot]['hotweb']);
				$("#f_desc").text(window.fichas[idhot]['hotdesc']);
				$("#f_checkin").text(window.fichas[idhot]['checkin']);
				$("#f_checkout").text(window.fichas[idhot]['checkout']);
				$("#f_amenities").text(window.fichas[idhot]['amenities']);
				document.getElementById('d_ficha').style.display='block';
			}
			

			function buscaDispo(){
				checkSessAlive();
				var suma = 0;
				var habs = {}

				if($("#hab_1").val()>0){
					habs['sgl'] = $("#hab_1").val();
					suma+=$("#hab_1").val();
				}
				if($("#hab_2").val()>0){
					habs['twin'] = $("#hab_2").val();
					suma+=$("#hab_2").val();
				}
				if($("#hab_3").val()>0){
					habs['mat'] = $("#hab_3").val();
					suma+=$("#hab_3").val();
				}
				if($("#hab_4").val()>0){
					habs['tpl'] = $("#hab_4").val();
					suma+=$("#hab_4").val();
				}
				if(suma==0){
					alert("<?=$lanClass->msg_rooms;?>");
					return false;
				}

				$.ajax({
					type: 'POST',
					url: 'compra_opaca.php',
					dataType:'json',
					data:{
						flag:'finddispo',
						fecdesde: $("#dpfec_1").val(),
						fechasta: $("#dpfec_2").val(),
						ciudad: $("#ciudad").val(),
						extranjero: ($("#outsider").val()==2)?1:0,
						habs: habs,
						debug: $("#debug").val(),
						agcredit: $("#subagen :selected").attr("ag_credit")
					},
					beforeSend:function(){
						$body = $("body");
						$body.addClass("loading");
					},
					complete:function(){
						$body.removeClass("loading");
					},
					success:function(result){
						if(result.status=='ok'){
							var extra = "<tr ><td colspan='7'><button id='btn_own' at_dir='0' type='button' class='w3-btn w3-white w3-border w3-border-green w3-round-large w3-hover-green'>Mostrar Solo Directas</button>";
							extra+=" <span class='w3-tag w3-amber w3-padding-large w3-right'>Tarifa directa</span> 	<span class='w3-tag w3-Aqua w3-padding-large w3-right'>Tarifa Prepago</span></td></tr>";
							$("#disptable").html(extra+"<tr class='w3-dark-grey'><td>Nº</td><td style='width:320px;'>Hotel</td><td style='width:96px;'>Deadline</td><td>plan alimenticio</td><td>Tipo Habitación</td><td>Valor</td><td></td></tr>");
							var trnumber = 1;
							var htmlrow = "";
							$.each(result.data, function (id_hotel, data_hotel){
								var haveSheet = 0;
								if(data_hotel.info.checkin !==null){
									fichas[id_hotel] = [];
									fichas[id_hotel]['amenities'] = data_hotel.info.amenities;
									fichas[id_hotel]['checkin'] = data_hotel.info.checkin;
									fichas[id_hotel]['checkout'] = data_hotel.info.checkout;
									fichas[id_hotel]['hotimg'] = data_hotel.info.hotimg;
									fichas[id_hotel]['hotweb'] = data_hotel.info.hotweb;
									fichas[id_hotel]['hotdesc'] = data_hotel.info.hotdesc;
									fichas[id_hotel]['hotname'] = data_hotel.info.name;
									haveSheet = 1;

								}
								var trsubnumber = 1;
								$.each(data_hotel.info.habs, function(keyhabarray, info_hab){

									var idtr = "";
									var icon = "";
									var magicNumber = trnumber;
									var direccion=((data_hotel.info.direccion==null)?'':data_hotel.info.direccion);

									if(trsubnumber==1 && haveSheet == 1){
										var sheet = "<i class='far fa-file-alt' style='font-size:20px;cursor:pointer;' onClick='showHotSheet("+id_hotel+")'/>";
									}else{
										var sheet = "";
									}
									
									if(Object.keys(data_hotel.info.habs).length > 1){
										if(trsubnumber==1){
											idtr+="trmain-";
											icon = "<i id='i-"+trnumber+"' class='fa fa-plus' style='font-size:20px;cursor:pointer;' onClick='showMoreRooms(this, "+trnumber+")'/>";
										}else{
											idtr+="trsub-";
											direccion = "";
										}
										magicNumber = trnumber+"."+trsubnumber;
									}
									idtr+=trnumber+"-"+trsubnumber;
									var clase = "w3-hover-grey";
									var clase2 = "";
									if(info_hab.directa==0){
										clase2 =" w3-amber";
									}
									if(info_hab.prepago==1){
										clase2 = " w3-Aqua";
									}
									htmlrow="<tr id='"+idtr+"' class='"+clase+" "+clase2+"' at_dir='"+info_hab.directa+"'>";
									htmlrow+="<td>"+magicNumber+"</td>";
									htmlrow+="<td><strong>"+data_hotel.info.name;
									htmlrow+="</strong>  "+icon+"  "+sheet+"<br><label>"+direccion+"</label>";
									//debug de esta wea
									htmlrow+="<br><label style='font-size:8px;'>"+info_hab.debug+"</label>";
									htmlrow+="</td>";
									htmlrow+="<td>"+data_hotel.info.deadline+"</td>";
									htmlrow+="<td>"+regimes[info_hab.aplan]+"</td>";
									htmlrow+="<td>"+info_hab.thname+"</td>";
									htmlrow+="<td>"+(($("#outsider").val()==2)?'USD$':'CLP$')+" "+info_hab.totValue+"</td>";
									htmlrow+="<td><input type='button' value='Reservar' class='w3-button w3-black w3-round-large' onclick='reservar(\""+info_hab.btnstr+"\")'>";
									htmlrow+="</tr>";
									$("#disptable").append(htmlrow);
									$("#container").attr('style','height:100%');
									trsubnumber++;
								});
								trnumber++;
							});
							$("[id*=trsub]").hide();

							$("#btn_own").click(function(){
								if($("#btn_own").attr('at_dir')==0){
									$("#btn_own").attr('at_dir',1);
									$("#disptable tr[at_dir='1']").hide(500);
									$("#disptable tr[at_dir='0']").show(500);
									$("#btn_own").attr('class', 'w3-btn w3-green w3-border w3-border-red w3-round-large w3-hover-white');
								}else{
									$("#btn_own").attr('at_dir',0);
									$("#disptable tr[at_dir='1']").show(500);
									$("[id*=trsub]").hide();
									$("#btn_own").attr('class', 'w3-btn w3-white w3-border w3-border-green w3-round-large w3-hover-green')
								}
							});




						}else{
							alert("<?=$lanClass->noresults?>");
							$("#disptable").html("");
						}
					},
					error:function(){
						alert("Error al traer los datos.");
						$("#disptable").html("");
					}
				});
			}

			function reservar(btnstr){
				checkSessAlive();
				var reql = btnstr.split("|");
				reql.forEach(function(val, key){
					reql[key] = '"'+val.replace(":",'":"')+'"';
				});
				jsonstr = "{ "+reql.join()+"}";
				jsonobj = JSON.parse(jsonstr);
				var paxes = 0;
				if(jsonobj.s>0){
					paxes += jsonobj.s*1;
				}
				if(jsonobj.tw>0){
					paxes += jsonobj.tw*2;
				}
				if(jsonobj.m>0){
					paxes += jsonobj.m*2;
				}
				if(jsonobj.tp>0){
					paxes += jsonobj.tp*3;
				}
				//c:102|fd:2017-12-11|fh:2017-12-12|s:1|tw:0|m:0|tp:0|ho:36|th:1|ex:0|tv:40403
				var htmlrow = "<input type='hidden' value='"+JSON.stringify(jsonobj)+"' name='btnstr'>";
				for(var i = 1; i <= paxes; i++){
					htmlrow+="<tr><td><center>";

					htmlrow+="<table class='w3-table w3-bordered w3-small' style='width: 600px;'>";
					htmlrow+="<tr><th colspan='2'>Pasajero "+i+"</th><tr>";
					htmlrow+="<tr><td><label>Nombre</label><br><input type='text' name='namepax[]'></td>";
					htmlrow+="<td><label>Apellido</label><br><input type='text' name='lnamepax[]'></td></tr>";
					htmlrow+="<tr><td><label>Pais</label><br><select name='countrypax[]'>";
					
					if(jsonobj.ex == 1){
						$.each(countries, function(key, value){
							if(key != <?=$cotClass->nationalCountryID;?>){
								htmlrow+="<option value='"+key+"'>"+value+"</option>";
							}
						});
					}else{
						htmlrow+="<option value='"+<?=$cotClass->nationalCountryID;?>+"'>"+countries[<?=$cotClass->nationalCountryID;?>]+"</option>";
					}
					
					htmlrow+="</select></td><td><label>passport</label><br><input type='text' name='passport[]'></td></tr>";
					htmlrow+="<tr><td></td><td>";
					if(i==1 && paxes>1){
						htmlrow+="<i class='fa fa-clone' style='font-size:20px;cursor:pointer;' onClick='clonePax()'/>";
					}
					htmlrow+="</td></tr></table>";
					htmlrow+="</center></td></tr>";
				}
				htmlrow+="<tr><td colspan='2'></td></tr>";
				htmlrow+="<tr><td colspan='2'><center><label>Observaciones Reserva</label><br>";
				htmlrow+="<textarea rows='1' name='observ' style='width: 600px;'></textarea></center></td></tr>";
				if(jsonobj.di==0 && jsonobj.afc==0){
					htmlrow+="<tr><td><center>Full Credit: <input type='checkbox' name='fullcredit' value='0'></center></td></tr><br>";
				}
				htmlrow+="<tr><td><center><input type='submit' name='btnsubmit' onclick='submitthis()' class='w3-button w3-black' value='Completar reserva'/></center></td></tr>";
				$("#disptable").html(htmlrow);
			}

			function submitthis(){
				$("#formdisp").submit();
			}

			function clonePax(){
				$("input[name=namepax\\[\\]").val($("input[name=namepax\\[\\]")[0].value);
				$("input[name=lnamepax\\[\\]").val($("input[name=lnamepax\\[\\]")[0].value);
				$("select[name=countrypax\\[\\]").val($("select[name=countrypax\\[\\]")[0].value);
				$("input[name=passport\\[\\]").val($("input[name=passport\\[\\]")[0].value);
			}

			function showMoreRooms(obj, maintr){
				$("[id*=trsub-"+maintr+"]").show(500);
				$("#"+obj.id).attr("class","fa fa-minus");
				$("#"+obj.id).attr("onclick","showLessRooms(this, "+maintr+")");
			}
			function showLessRooms(obj, maintr){
				$("[id*=trsub-"+maintr+"]").hide(500);
				$("#"+obj.id).attr("class","fa fa-plus");
				$("#"+obj.id).attr("onclick","showMoreRooms(this, "+maintr+")");
			}

			
		</script>
</head>
	<body> 
		<div id='container' class="w3-sand w3-grayscale w3-large" style='height:100%'>
			<div class='w3-content ' style="max-width:700px">
				
				<table class='w3-table w3-bordered w3-small'>
					<tr>
						<td colspan='4'>
							<h5 class='w3-center'>
								<span class='w3-tag w3-wide'>Reserva Nueva</span><br>

								<?
								if($agencyData->Fields('ag_logo')!=''){
									echo "<img src='images/".$agencyData->Fields('ag_logo')."'>";
								}
								?>
							</h5>


						</td>

						<input type='hidden' name='debug' id='debug' value='0'>
					</tr>
					<tr>
						<td colspan='2'>
							<label><?=ucfirst($lanClass->fecha_desde);?></label>
							<input id='dpfec_1' name='dpfec_1' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d'); ?>"/>
						</td>
						<td colspan='2'>
							<label><?=ucfirst($lanClass->fecha_hasta);?></label>
							<input id='dpfec_2' name='dpfec_2' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d', strtotime('+1 day')); ?>"/>
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<label><?=ucfirst($lanClass->ciudad);?></label>
							<select name='ciudad' id='ciudad' class="w3-select">
								<?
									$cities = $cotClass->getCities($db1, true,$cotClass->nationalCountryID);
									$arcities = array();
									while(!$cities->EOF){
										echo "<option value='".$cities->Fields('id_ciudad')."' ".(($cities->Fields('id_ciudad')==102)?'SELECTED':'').">".utf8_encode($cities->Fields('ciu_nombre'))."</option>";
										$cities->MoveNext();
									}
								?>
							</select>
						</td>
						<td colspan='2'>
						<label><?=ucfirst($lanClass->origen_paxes);?></label>
							<select name='outsider' id='outsider' class="w3-select">
								<option value='1'><?=ucfirst($lanClass->chileno);?></option>
								<option value='2'><?=ucfirst($lanClass->extranjero);?></option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label><?=ucfirst($lanClass->correlativo);?></label>
							<input type='text' name='txt_correlativo' id='txt_correlativo' class="w3-input w3-border">
						</td>
						<td colspan="2">
							<label><?=ucfirst($lanClass->subagencia);?></label>
							<select name='subagen' id='subagen' class="w3-select">
								<option value='0' ag_credit='0'>-= <?=ucfirst($lanClass->ninguno);?> =-</option>
								<?
								while(!$subagencies->EOF){
									echo "<option value='".$subagencies->Fields('id_subagencia')."' ag_credit='".$subagencies->Fields('axsa_fullcredit')."'>".$subagencies->Fields('sa_nombre')."</option>";
									$subagencies->MoveNext();
								}
								?>
							</select>

						</td>


					</tr>

					<?
					$counter = 1;
					while($counter <= 4){
						
						$namevar = "tdhab$counter";
						$$namevar= "<select name='hab_$counter' id='hab_$counter'>";
						for($i=0; $i <= 6; $i++){
							if($counter==1 && $i==1){
								$selected = "SELECTED";
							}else{
								$selected= "";
							}
							$$namevar.= "<option value='$i' $selected>$i</option>";
						}
						$counter++;
					};
					?>
					<tr>
						<th colspan='4'><?=$lanClass->habitaciones;?></th>
					</tr>
					<tr>
						<td>
							<label>Single</label>
							<?=$tdhab1;?>
						</td>
						<td>
							<label>Twin</label>
							<?=$tdhab2;?>
						</td>
						<td>
							<label>Matrimonial</label>
							<?=$tdhab3;?>
						</td>
						<td>
							<label>Triple</label>
							<?=$tdhab4;?>
						</td>
					</tr>
					
					<tr>
						<td colspan='3'></td>
						<td><center><button type="button" name='btnsrch' class='w3-btn w3-black w3-round-large' onclick='buscaDispo()'><?=ucfirst($lanClass->buscar);?></button></center></td>
					</tr>
				</table>
			</div>
			<div class=' w3-sand' style='width: 100%'>
				<br>
				<div class='w3-content'>
					<h5 class="w3-center"><span class="w3-tag w3-wide"><?=ucfirst($lanClass->confirmacion_instantanea);?></span></h5>
					<h6 class="w3-center"><?=ucfirst($lanClass->noincluyeiva);?></h6>
						<form method='POST' action='' name='formdisp' id='formdisp'>
							<table id="disptable" class='w3-table w3-small'></table>
						</form>
				</div>
			</div>
		</div>
		<div class="modal"><!-- div para el loading --></div>

		<div id='d_ficha' class="w3-modal">
			<div class="w3-modal-content w3-card-4 w3-animate-zoom w3-padding-16" style="max-width:600px">
				<header class='w3-container  w3-center'>
					<span onclick="document.getElementById('d_ficha').style.display='none'" class="w3-large w3-button w3-display-topright"><i class="far fa-window-close"></i></span>
				</header>
				<div class='w3-container  w3-center'>
					<b><label id='f_hotname' class='w3-serif w3-padding-hor-4'></label></b><br>
					<img id='f_img' src="" style='width:30%' class="w3-circle w3-margin-top">
					<p id='f_desc'></p>
					<i class='fas fa-link'/></i>
					<a href='' id='f_web'></a>
				</div>
				<div class='w3-container  w3-center'>
					<p id='f_desc'></p>
				</div>
				<div class='w3-container  w3-center'>
					<div class='w3-left'>
						<i class='far fa-clock'/></i>
						<b>Check in:</b> <label id='f_checkin'></label> hrs.
					</div>
					<div class='w3-right'>
						<i class='far fa-clock'/></i>
						<b>Check out:</b> <label id='f_checkout'></label> hrs.
					</div>
				</div>
				<div class='w3-container'>
					<div class='w3-left'>
						<i class="far fa-list-alt"></i>
						<b>Cuenta Con:</b> <label id='f_amenities'></label>.
					</div>
				</div>	
			</div>
		</div>


	</body>
</html>



