<?php 
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/v1.1/wsdl/bookingroomwsdl.php?WSDL");
$server->addFunction("bookingroom");
$server->handle();
function bookingroom($formdata){
	$id_webservice=3;
	require_once('../../Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = get_object_vars($formdata);
	$user = $formdata["user"];
	$password = $formdata["password"];
	$extras['clientcode'] = $formdata["cod_referencia"];
	$foreign = false;
	$checkin = $formdata["FechaLlegada"];
	$checkout = $formdata["noches"];
	$hotelid = $formdata["idHotel"];
	$roomid = $formdata["IdTipoHabitacion"];
	$currency= "CLP";
	$total= $formdata["Precio"];
	$extras['notes']= "";
	$extras['contactos']= $formdata["contacto"];
	$sgl[0] = $formdata["single"];
	$twin[0] = $formdata["dobleTwin"];
	$mat[0] = $formdata["dobleMatri"];
	$tpl[0] = $formdata["triple"];
	if(isset($formdata["Pasajeros"]->Pasajero->Nombre)){
		$paxs[0][0]["name"] = $formdata["Pasajeros"]->Pasajero->Nombre;
		$paxs[0][0]["lastname"] = $formdata["Pasajeros"]->Pasajero->Apellido;
		$paxs[0][0]["passport"] = $formdata["Pasajeros"]->Pasajero->Pasaporte;
		$paxs[0][0]["NumVuelo"] = $formdata["Pasajeros"]->Pasajero->NumVuelo;
		$paxs[0][0]["countryid"] = $formdata["Pasajeros"]->Pasajero->Pais;
	}else{
		foreach($formdata["Pasajeros"]->Pasajero as $pasajero=>$pax_data){
			$paxs[0][$pasajero]["name"] = $pax_data->Nombre;
			$paxs[0][$pasajero]["lastname"] = $pax_data->Apellido;
			$paxs[0][$pasajero]["passport"] = $pax_data->Pasaporte;
			$paxs[0][$pasajero]["NumVuelo"] = $pax_data->NumVuelo;
			$paxs[0][$pasajero]["countryid"] = $pax_data->Pais;
			
		}
	}
	//print_r($paxs);die();
	if(strtoupper($foreign)=="FALSE" || $foreign=='0'){
		$currency_aux="CLP";
		$foreign=false;
	}else{
		$currency_aux="USD";
		$foreign=true;
	}
	$res=new stdClass();
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($usuario_rs->RecordCount()==0){
		$res->Status='ERROR';
		$res->Mensaje="Invalid user";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		$res->Status='ERROR';
		$res->Mensaje='Not allowed user';
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	if($currency_aux!=$currency){
		$res->Status='ERROR';
		$res->Mensaje="The currency doesn't match with the tag foreign";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	$pais_rq  ="select * from pais where id_pais = ".$paxs[0][0]["countryid"];
	$pais_rs = $db1->Execute($pais_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($pais_rs->RecordCount()==0){
		$res->Status='ERROR';
		$res->Mensaje="First pax country is mandatory.";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	if(($foreign && $paxs[0][0]["countryid"] == $cot->nationalCountryID) || (!$foreign && $paxs[0][0]["countryid"] != $cot->nationalCountryID)){
		$res->Status='ERROR';
		$res->Mensaje="First pax country doesn't match with foreign tag";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	$cant_room = count($paxs);
	$habs['sgl']=0;
	$habs['twin']=0;
	$habs['mat']=0;
	$habs['tpl']=0;
	for($i=0;$i<=$cant_room;$i++){
		$total_paxs=0;
		if(is_numeric($sgl[$i])){
			$total_paxs+=$sgl[$i];
			$habs['sgl']+=$sgl[$i];			
		}
		if(is_numeric($twin[$i])){
			$total_paxs+=$twin[$i]*2;
			$habs['twin']+=$twin[$i];
		}
		if(is_numeric($mat[$i])){
			$total_paxs+=$mat[$i]*2;
			$habs['mat']+=$mat[$i];
		}
		if(is_numeric($tpl[$i])){
			$total_paxs+=$tpl[$i]*3;
			$habs['tpl']+=$tpl[$i];
		}
		if($total_paxs<count($paxs[$i])){
			$res->Status='ERROR';
			$res->Mensaje="Number of paxs doesnt match with rooms($i)";
			$res->CodigoCompra="";
			$res->CodReferencia='';
			$lista[]=$res;
			return $lista;
		}
		foreach($paxs[$i] as $index=>$pasajero){
			$paxes[]=$paxs[$i][$index];
		}
	}	
	if(is_numeric($hotelid) && is_numeric($roomid)){
		$hotel_rq  ="select * from hotel where id_hotel =".$hotelid;
		$hotel_rs = $db1->Execute($hotel_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($hotel_rs->RecordCount()>=0){
			$cityid=$hotel_rs->Fields('id_ciudad');
		}
	}else{
		$res->Status='ERROR';
		$res->Mensaje="Invalid hotel or room";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	$arr = $cot->matrizDisponibilidadG($db1, $cityid, $checkin, $checkout, $id_agencia, $habs, $hotelid, $roomid, $foreign);
	if(round($arr[$hotelid]['info']['habs'][$roomid]['totValue'], $cot->decimales)!=round($total, $cot->decimales)){
		$res->Status='ERROR';
		$res->Mensaje="There's no stock in the requested room";
		$res->CodigoCompra="";
		$res->CodReferencia='';
		$lista[]=$res;
		return $lista;
	}
	if(trim($extras['contactos'])=='' || trim($extras['contactos'])=='?' || !isset($extras['contactos'])){
		$extras['contactos']='';
	}else{
		$extras['contactos'] = str_replace(',',';',$extras['contactos']);
	}
	if(trim($extras['notes'])=='' || trim($extras['notes'])=='?' || !isset($extras['notes'])){
		$extras['notes']='';
	}
	if(trim($extras['clientcode'])=='' || trim($extras['clientcode'])=='?' || !isset($extras['clientcode'])){
		$extras['clientcode']='';
	}
	$result = $cot->cotCreator($db1, $arr, $habs, $paxes,$id_usuario,$cityid,$extras);
	if(!isset($result['refundable'])){
		$refundable='';
	}else{
		$refundable=($result['refundable']==0)?"TRUE":"FALSE";
	}
	$res->Status=$result['status'];	
	$res->Mensaje=$result['description']." / Refundable_info: ".$refundable;
	$res->CodigoCompra=$result['bookingcode'];
	$res->CodReferencia="";	
	$lista[]=$res;
	return $lista;
}
?>