<?php 
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/wsdl/cancelbookingwsdl.php?WSDL");
$server->addFunction("cancelbooking");
$server->handle();
function cancelbooking($formdata){
	$id_webservice=4;
	require_once('../Connections/db1.php');
	require_once('../clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = get_object_vars($formdata);
	$user = $formdata["user"];
	$password = $formdata["password"];
	$bookingcode = $formdata["bookingcode"];
	$usuario_rq  ="select u.*, a.ag_espanula from usuarios u join agencia a 
		on u.id_agencia = a.id_agencia where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$res= new stdClass();
	if($usuario_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->description='Invalid user';
		$res->penalty='';
		$lista[]=$res;
		return $lista;
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->description='Not allowed user';
		$res->penalty='';
		$lista[]=$res;
		return $lista;
	}
	$cot_rq  ="select * from mundotour.cot where id_cot = $bookingcode and id_seg = 7 and cot_estado = 0";

	$cot_rs = $db1->Execute($cot_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($cot_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->description='Booking must be confirmed';
		$res->penalty='';
		$lista[]=$res;
		return $lista;
	}
	$sql= "SELECT TIME_TO_SEC(TIMEDIFF(NOW(), ha_hotanula)) AS diff FROM cot WHERE id_cot = $bookingcode";
	$rs= $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($rs->Fields("diff")>=0 && $usuario_rs->Fields("ag_espanula")==1){
		$res->status='ERROR';
		$res->description='User can not cancel out of deadline';
		$res->penalty='';
		$lista[]=$res;
		return $lista;
	}
	$result = $cot->cotAnulator($db1, $bookingcode,$id_usuario);
	$res->status=$result['status'];
	$res->description=$result['description'];
	$res->penalty=$result['penalty'];
	$lista[]=$res;
	return $lista;	
}
