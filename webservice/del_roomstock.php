<?php
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/wsdl/del_roomstockwsdl.php?WSDL");
$server->addFunction("roomstock");
$server->handle();
function roomstock($formdata) {

   $id_webservice = 2;
   require_once('../Connections/db1.php');
   #require_once('../clases/cotizacion.php');
   require_once('../clases/del_cotizacion.php');

   $cot = new Cotizacion();
   $formdata = get_object_vars($formdata);
   $user = $formdata["user"];
   $password = $formdata["password"];
   $foreign = $formdata["foreign"];
   $checkin = $formdata["checkin"];
   $checkout = $formdata["checkout"];
   $hotelid = $formdata["hotelid"];
   $cityid = $formdata["cityid"];

   if (isset($formdata["roomrq"]->adults)) {
      $room[0]["adults"] = $formdata["roomrq"]->adults;
   } else {
      foreach ($formdata["roomrq"] as $index => $room_data) {
         $room[$index]["adults"] = $room_data->adults;
      }
   }
   //Login de usuario
   $usuario_rq = "select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
   $usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
   if ($usuario_rs->RecordCount() == 0) {
      return (0);
   }

   #dd($usuario_rs->Fields('id_agencia'));

   $id_agencia = $usuario_rs->Fields('id_agencia');
   $agws_rq = "select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";

   #dd($agws_rq);

   $agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
   if ($agws_rs->RecordCount() == 0) {
      return (0);
   }
   if (is_numeric($hotelid) || is_array($hotelid)) {
      if (count($hotelid) == 1) {
         $hotelarr[] = $hotelid;
      } else {
         $hotelarr = $hotelid;
      }
      $hotel_rq = "select * from hotel where id_hotel =" . $hotelarr[0];
      $hotel_rs = $db1->Execute($hotel_rq) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
      if ($hotel_rs->RecordCount() >= 0) {
         $cityid = $hotel_rs->Fields('id_ciudad');
      }
   } else {
      $hotelarr = null;
   }
   if (strtoupper($foreign) == "FALSE" || $foreign == '0') {
      $currency = "CLP";
      $foreign = false;
   } else {
      $currency = "USD";
      $foreign = true;
   }
   $habs[0]['sgl'] = 0;
   $habs[0]['twin'] = 0;
   $habs[0]['mat'] = 0;
   $habs[0]['tpl'] = 0;
   $haydbbl = false;
   foreach ($room as $id => $val) {
      if ($val['adults'] == 1) {
         $habs[0]['sgl'] += 1;
      } elseif ($val['adults'] == 2) {
         $haydbbl = true;
         $habs[0]['twin'] += 1;
      } elseif ($val['adults'] == 3) {
         $habs[0]['tpl'] += 1;
      } else {
         return (0);
      }
   }
   // Pasan las dobles a matri
   if ($haydbbl) {
      $habs[1]['sgl'] = $habs[0]['sgl'];
      $habs[1]['twin'] = 0;
      $habs[1]['mat'] = $habs[0]['twin'];
      $habs[1]['tpl'] = $habs[0]['tpl'];
   }

   //Busca dispo por cantidad de habitaciones solicitadas
   for ($i = 0; $i < count($habs); $i++) {
      $arr[$i] = $cot->matrizDisponibilidadG($db1, $cityid, $checkin, $checkout, $id_agencia, $habs[$i], $hotelarr, null, $foreign);
   }
   foreach ($arr[0] as $key => $ways) {
      $res = new stdClass();
      $res->hotelid = $key;
      $res->hotelname = utf8_encode($ways['info']['name']);
      $res->deadline = $ways['info']['deadline'];
      $res->cancellationpolicy = $ways['info']['cancellationPolicy'];
      foreach ($ways['info']['habs'] as $thab => $infohab) {
         $roomdata = new stdClass();
         $roomdata->roomid = $thab;
         $roomdata->room = $infohab['thname'];
         $roomdata->refundable = ($infohab['refundable'] == 0) ? "TRUE" : "FALSE";
         $roomdata->penalty = $infohab['penalty'];
         $roomdata->mealplanid = $infohab['aplan'];
         $roomdata->currency = $currency;
         $roomdata->total = $infohab['totValue'];
         for ($i = 0; $i < count($habs); $i++) {
            $roomdetails = new stdClass();
            foreach ($ways['habs'][$thab] as $fecha => $infoday) {
               $roomdetails->sglcost += round($infoday['sgl'] * $habs[$i]['sgl'] / $infoday['markup'][0]['sgl'], $cot->decimales);
               $roomdetails->twincost += round($infoday['dbl'] * $habs[$i]['twin'] / $infoday['markup'][0]['twin'] * 2, $cot->decimales);
               $roomdetails->matcost += round($infoday['dbl'] * $habs[$i]['mat'] / $infoday['markup'][0]['mat'] * 2, $cot->decimales);
               $roomdetails->tplcost += round($infoday['tpl'] * $habs[$i]['tpl'] / $infoday['markup'][0]['tpl'] * 3, $cot->decimales);
            }
            $roomdetails->sgl = $habs[$i]['sgl'] . "/" . $infoday['hotdet_cli'];
            $roomdetails->twin = $habs[$i]['twin'] . "/" . $infoday['hotdet_cli'];
            $roomdetails->mat = $habs[$i]['mat'] . "/" . $infoday['hotdet_cli'];
            $roomdetails->tpl = $habs[$i]['tpl'] . "/" . $infoday['hotdet_cli'];
            $roomdata->roomdetails[] = $roomdetails;
         }
         $res->roomdata[] = $roomdata;
      }
      $lista[] = $res;
   }

   return $lista;
}

/*
 * Dump but not die
 * */
function dump($var) {
   echo "<pre>";
   echo "\n";
   var_dump(json_decode(json_encode($var, JSON_PRETTY_PRINT)));
   echo "\n";
   #var_dump($var);
   echo "</pre>";
   return;
}

/*
 * Die with var_dump message
 * */
function d($var) {
   var_dump(json_decode(json_encode($var, JSON_PRETTY_PRINT)));
   return die;
}

/*
 * Dump and die with pre tag
 * */
function dd($var) {
   echo "<pre>";
   #var_dump(json_decode(json_encode($var, JSON_PRETTY_PRINT)));
   var_dump($var);
   echo "</pre>";
   return die;
}

/*
 * Dump with custom message
 * */
function dcm ($var, $custom_message) {
   echo "<pre>";
   #var_dump(json_decode(json_encode($var, JSON_PRETTY_PRINT)));
   #var_dump($var);
   echo $custom_message;
   echo "</pre>";
   return die;
}



?>