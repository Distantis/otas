<?php
include("Connections/db1.php");
include("/var/www/otas/clases/cotizacion.php");
$cotClass = new Cotizacion();

header('Content-type: application/json');
$result = array();
if(isset($_POST['pass'])){
	$valid = $cotClass->validateSuperPassword($_POST['pass']);
}else{
	array_push($result, "nopass");
	die(json_encode($result));
}


$habs = array();
if($_POST['sgl']>0){
	$habs['sgl'] = $_POST['sgl'];
}
if($_POST['twin']>0){
	$habs['twin'] = $_POST['twin'];
}
if($_POST['mat']>0){
	$habs['mat'] = $_POST['mat'];
}
if($_POST['tpl']>0){
	$habs['tpl'] = $_POST['tpl'];
}
if(isset($_POST['thab'])){
	$id_tipohab = $_POST['thab'];
}else{
	$id_tipohab = null;
}

$matriz = $cotClass->matrizDisponibilidadG($db1, null, $_POST['fechad'], 1, $_POST['agencia'], $habs,  $_POST['hotel'], $id_tipohab, $_POST['extranjero'], null,true);

if($matriz->RecordCount()>0){
	foreach($matriz as $id_hotel => $hot_data){
		$hot_info = $hot_data['info'];
		unset($matriz[$id_hotel]['info']);
		foreach($hot_data as $id_tipohabitacion => $fecha_data){
			$matriz[$id_hotel][$id_tipohabitacion][$_POST['fechad']]['thname'] = $hot_info['habs'][$id_tipohabitacion]['thnombre'];
			$matriz[$id_hotel][$id_tipohabitacion][$_POST['fechad']]['refundable'] = $hot_info['habs'][$id_tipohabitacion]['refundable'];
			$matriz[$id_hotel][$id_tipohabitacion][$_POST['fechad']]['directa'] = $hot_info['habs'][$id_tipohabitacion]['directa'];
		}
	}
	$result = $matriz;
}else{
	array_push($result, "no");
}
die(json_encode($result));



?>