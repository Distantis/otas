﻿<?xml version="1.0" encoding="utf-8" ?>
<!--Created with Liquid XML 2015 Designer Edition (Trial) 13.0.14.5873 (http://www.liquid-technologies.com)-->
<xs:schema xmlns:jxb="http://java.sun.com/xml/ns/jaxb" jxb:version="1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:include schemaLocation="switchfly_search_objects.xsd" />
    <xs:include schemaLocation="switchfly_booking_objects.xsd" />
    <xs:element name="AvailabilityRQ">
        <xs:annotation>
            <xs:documentation>An availability request consists of a collection of specifications defining segments in which the customer is
                interested. Any number of specifications may be sent, each giving key attributes about the desired segments. This message is designed
                to allow the client to collect full information about a desired itinerary and send a single message to get availability information
                for each segment.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRQType">
                    <xs:sequence>
                        <xs:element name="CreateSession" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>True/False flag which determines whether Switchfly will maintain a user session on our server
                                    containing the results of the availability request. If true, a SessionToken element will be returned in the
                                    AvailabilityRS
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:choice>
                            <xs:sequence>
                                <xs:element name="RoomSearch" type="RoomSearchType" minOccurs="0" maxOccurs="unbounded" />
                            </xs:sequence>
                        </xs:choice>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="AvailabilityRS">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRSType">
                    <xs:sequence>
                        <xs:annotation>
                            <xs:documentation>Any search response can be empty. I.e., it is not an error to make a search that returns no results.
                            </xs:documentation>
                        </xs:annotation>
                        <xs:element name="RoomSearchResponse" type="RoomSearchResponseType" minOccurs="0" maxOccurs="unbounded" />
                        <xs:element name="SessionToken" type="xs:string" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>A token that points back to an Switchfly session key on the server. This element will exist if the
                                    AvailabilityRQ passed true in the CreateSession element.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="CancelPnrRQ">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRQType">
                    <xs:sequence>
                        <xs:element name="RecordLocator" type="xs:string">
                            <xs:annotation>
                                <xs:documentation>Switchfly assigned record locator</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="IsAutoCancel" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>Sometimes an Switchfly API client must interact with multiple servers to book a package. In this
                                    case, the client will sometimes discover after booking via the Switchfly API that they are unable to complete the
                                    package and must cancel. Some Switchfly API servers are configured to prevent cancellations, but an exception is
                                    made in this particular scenario and setting this flag to true will override this policy. Misuse of this flag can
                                    lead to retroactive booking fees.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="RefundPayment" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>Determine if cancel a booking should refund payment. Refunding a payment is only supported for
                                    bookings limited to air, room, car, and activity components.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="PushPnr" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>The flag determines if PushPnr is required after cancelation.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="SendConfirmation" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>If true, confirmation is sent to the pnr owner. If false or missing, no confirmation is sent.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="CancelPnrRS">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRSType">
                    <xs:sequence>
                        <xs:element name="Pnr" type="PnrType" />
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="CreatePnrRQ">
        <xs:annotation>
            <xs:documentation>An Switchfly PNR (booking) is created using a single message that includes all information needed to create one or more
                PNR's on the underlying reservation systems. Switchfly ties these all together under a master Switchfly record locator, but also
                provide the underlying locators for use when necessary.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyPaymentRQType">
                    <xs:sequence>
                        <xs:element name="AddToRecordLocator" type="xs:string" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>This is the record locator for a previously created booking to which the products in this
                                    CreatePnrRQ message will be added. If this tag is omitted, a new booking is created. If it is included, an
                                    existing booking is "added to". A booking can only be added to if it has not been cancelled, the agents are the
                                    same, the cobrands are the same, and they have the same currency.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="PnrOwner" type="TravelerType">
                            <xs:annotation>
                                <xs:documentation>The person who created the PNR and is responsible for it. This person need not be traveling.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="LeadTraveler" type="TravelerType">
                            <xs:annotation>
                                <xs:documentation>The traveler who is the main contact. This person is used for traveler information on products that
                                    do not include traveler lists.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="RoomSegmentRequest" minOccurs="0" maxOccurs="unbounded">
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="CorrelationId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>This id, if supplied, is returned in the response along with the record locator.
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="RoomSegment" type="xs:string">
                                        <xs:annotation>
                                            <xs:documentation>The BookingDescriptor string that was returned in the availability response.
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="CustomFields" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Custom fields for the contract</xs:documentation>
                                        </xs:annotation>
                                        <xs:complexType>
                                            <xs:sequence>
                                                <xs:element name="CustomField" type="CustomFieldType" minOccurs="0" maxOccurs="unbounded" />
                                            </xs:sequence>
                                        </xs:complexType>
                                    </xs:element>
                                    <xs:element name="RoomingList" type="GuestListType" minOccurs="0" />
                                    <xs:element name="SpecialRequests" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>A string indicating any special requests for this room.</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="RequestedService" type="xs:string" minOccurs="0" maxOccurs="unbounded">
                                        <xs:annotation>
                                            <xs:documentation>A string indicating the code of the available request service returned in the
                                                availability response.
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="AgencyCreditCard" type="CreditCardType" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>The agency credit card for merchanting this component</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                        <xs:element name="BookingHoldDays" type="xs:integer" minOccurs="0" />
                        <xs:element name="BookingMemo" type="xs:string" minOccurs="0" />
                        <xs:element name="ProcessPayment" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>This option is available only to in-house agents. The flag determines if payment should be processed
                                    by the server. If true, then payment will be processed by the payment gateway set up on the server. Payment will
                                    be for the amount specified as ProcessPaymentAmount field in PaymentOption. If ProcessPaymentAmount is not
                                    specified, payment will be for the full amount of the non-passed through price. If false, no payment will be made.
                                    If ProcessPayment is false, any credit card is used only for pass through payments. In
                                    this case, the client system is assumed to have collected the money from the customer and settlement between the
                                    client and server systems will be handled separately. If ProcessPayment is true, you have to chose one
                                    PaymentOption element.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="ProcessPaymentCurrency" type="CurrencyCodeType" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>Payment currency. Must match customer currency of the original booking.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="PushPnr" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>The flag determines if PushPnr is required after booking.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="SendConfirmation" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>If true, confirmation is sent to the pnr owner. If false or missing, no confirmation is sent.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="ClientReservation" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Information about a booking on the client system related to the booking being requested on the
                                    server receiving this message.
                                </xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="Number" type="xs:string">
                                        <xs:annotation>
                                            <xs:documentation>Reservation number on external system</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="LoyaltyNumber" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Loyalty number for the lead traveler on external system</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                        <xs:element name="HoldSupplierConfirmation" type="xs:boolean" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>If true, do not send supplier confirmation immediately. It may be sent later via a ConfirmPnrRQ
                                    request with a false value for this element.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="DiscountCode" type="xs:string" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>This string represents discount code.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="CreatePnrRS">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRSType">
                    <xs:choice>
                        <xs:sequence>
                            <xs:annotation>
                                <xs:documentation>This sequences of elements occurs for certain error conditions.</xs:documentation>
                            </xs:annotation>
                            <xs:element name="UnavailableBookingComponent" type="xs:string" minOccurs="0" maxOccurs="unbounded">
                                <xs:annotation>
                                    <xs:documentation>If a booking component became unavailable between the time of the search and the time of the
                                        booking attempt, this element gives the booking descriptor for that component.
                                    </xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="PriceChange" minOccurs="0" maxOccurs="unbounded">
                                <xs:annotation>
                                    <xs:documentation>If a booking component changed price between the time of the search and the time of the booking
                                        attempt, this element gives the details of that price change. The change is saved so that attempt to book a
                                        second time will succeed.
                                    </xs:documentation>
                                </xs:annotation>
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="BookingDescriptor" type="xs:string" />
                                        <xs:element name="Currency" type="xs:string" />
                                        <xs:element name="OriginalPrice" type="xs:double" />
                                        <xs:element name="NewPrice" type="xs:double" />
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                        <xs:sequence>
                            <xs:element name="SpecialMarkup" type="xs:double" />
                            <xs:element name="TicketingDeadline" type="IsoDateType" minOccurs="0" />
                            <xs:element name="Segment" minOccurs="0" maxOccurs="unbounded">
                                <xs:annotation>
                                    <xs:documentation>Used to set booked segment info for clients that use correlation IDs</xs:documentation>
                                </xs:annotation>
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="CorrelationId" type="xs:string" minOccurs="0">
                                            <xs:annotation>
                                                <xs:documentation>If present, the correlation id is copied from the creation message.
                                                </xs:documentation>
                                            </xs:annotation>
                                        </xs:element>
                                        <xs:element name="SegmentLocator" type="xs:string">
                                            <xs:annotation>
                                                <xs:documentation>An id that can be used in booking modification messages. This should probably be
                                                    renamed, since it can refer to a room or car segment, but also to an air itinerary, which has
                                                    multiple segments.
                                                </xs:documentation>
                                            </xs:annotation>
                                        </xs:element>
                                        <xs:element name="CrsRecordLocator" type="xs:string" minOccurs="0">
                                            <xs:annotation>
                                                <xs:documentation>The is record locator used in the crs where Switchfly made the booking. It
                                                    represents the booking and would be used to read or cancel the booking. In the case of single CRS,
                                                    this is the key identifier for the PNR. In the case where the booking was made through an
                                                    intermediary CRS (such as a GDS), this is the GDS record locator. The record locator for the
                                                    ultimate supplier is different and might be required if calling the supplier directly. See the
                                                    SupplierLocator tag.
                                                </xs:documentation>
                                            </xs:annotation>
                                        </xs:element>
                                        <xs:element name="SupplierLocator" type="xs:string" minOccurs="0">
                                            <xs:annotation>
                                                <xs:documentation>This is the locator for the booking on the supplier's CRS. In an Switchfly booking,
                                                    for example, there is an Switchfly booking ID and a segment locator. This holds the identifier
                                                    returned from the CRS in which the server made the booking. This might be, for example, a Sabre
                                                    record locator. This is purely informational and is never an input parameter to an Switchfly API
                                                    message.
                                                </xs:documentation>
                                            </xs:annotation>
                                        </xs:element>
                                        <xs:element name="Currency" type="xs:string" />
                                        <xs:element name="TotalPrice" type="CurrencyAmountType">
                                            <xs:annotation>
                                                <xs:documentation>The total price calculated during booking.</xs:documentation>
                                            </xs:annotation>
                                        </xs:element>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="PaymentRedirectUrl" type="xs:string" minOccurs="0">
                                <xs:annotation>
                                    <xs:documentation>This is the url to proceed with the payment, if the PaymentRedirectUrlParameters element exist
                                        you need to send all the parameters specified as a POST request.
                                    </xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="PaymentRedirectUrlParameters" minOccurs="0">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="Parameter" maxOccurs="unbounded">
                                            <xs:complexType>
                                                <xs:sequence>
                                                    <xs:element name="Name" type="xs:string" />
                                                    <xs:element name="Value" type="xs:string" />
                                                </xs:sequence>
                                            </xs:complexType>
                                        </xs:element>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="PrintUrlForOfflineBankTransfer" type="xs:string" minOccurs="0">
                                <xs:annotation>
                                    <xs:documentation>This is the url to get the boleto in case the book was made using offline bank transfer.
                                    </xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="Pnr" type="PnrType" />
                        </xs:sequence>
                    </xs:choice>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="HotelDetailsRQ">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRQType">
                    <xs:sequence>
                        <xs:element name="Hotel" type="xs:string" />
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
    <xs:element name="HotelDetailsRS">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="SwitchflyRSType">
                    <xs:sequence>
                        <xs:element name="Hotel" type="HotelType" />
                    </xs:sequence>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
</xs:schema>
