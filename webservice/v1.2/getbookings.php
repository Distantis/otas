<?php 
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/wsdl/getbookingswsdl.php?WSDL");
$server->addFunction("getbookings");
$server->handle();
function getbookings($formdata){
	$id_webservice=9;
	require_once('../Connections/db1.php');
	require_once('../clases/distantis.php');
	$distantis = new distantis();
	$formdata = get_object_vars($formdata);
	$user = $formdata['user'];
	$password = $formdata['password'];
	$thisuser = $formdata['thisuser'];
	$creationdatefrom = $formdata['creationdatefrom'];
	$creationdateto = $formdata['creationdateto'];
	$checkin = $formdata['checkin'];
	$checkout = $formdata['checkout'];
	$bookingcode = $formdata['bookingcode'];
	$clientcode = $formdata['clientcode'];
	$cityid = $formdata['cityid'];
	$hotelid = $formdata['hotelid'];
	$includenoshow = $formdata['includenoshow'];
	$includenonref = $formdata['includenonref'];
	$anystatus = $formdata['anystatus'];
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($usuario_rs->RecordCount()==0){
		return (0);
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		return (0);
	}
	$data['id_agencia']=$usuario_rs->Fields('id_agencia');
	$data['id_usuario']=$usuario_rs->Fields('id_usuario');
	if(strtoupper($thisuser)=="FALSE"){
		$data['id_usuario']='';
	}
	if(trim($creationdatefrom)!='' && trim($creationdatefrom)!='?' && isset($creationdatefrom)){
		$data['cot_fecfrom']=$creationdatefrom;
	}
	if(trim($creationdateto)!='' && trim($creationdateto)!='?' && isset($creationdateto)){
		$data['cot_fecto']=$creationdateto;
	}
	if(trim($checkin)!='' && trim($checkin)!='?' && isset($checkin)){
		$data['checkin']=$checkin;
	}
	if(trim($checkout)!='' && trim($checkout)!='?' && isset($checkout)){
		$data['checkout']=$checkout;
	}
	if(trim($bookingcode)!='' && trim($bookingcode)!='?' && isset($bookingcode)){
		$data['bookingcode']=$bookingcode;
	}
	if(trim($clientcode)!='' && trim($clientcode)!='?' && isset($clientcode)){
		$data['clientcode']=$clientcode;
	}
	if(trim($cityid)!='' && trim($cityid)!='?' && isset($cityid)){
		$data['cityid']=$cityid;
	}
	if(trim($hotelid)!='' && trim($hotelid)!='?' && isset($hotelid)){
		$data['hotelid']=$hotelid;
	}
	$data['include_noshow']=(strtoupper($includenoshow)=="TRUE")?true:false;
	$data["includenonref"]=(strtoupper($includenonref)=="TRUE")?true:false;
	$data['any_estado']=(strtoupper($anystatus)=="TRUE")?true:false;
	$rs = $distantis->GetBookings($db1,$data);
	while(!$rs->EOF){
		$desde = new DateTime($rs->Fields('cot_fecdesde'));
		$hasta = new DateTime($rs->Fields('cot_fechasta'));
		$ha_hotanula = new DateTime($rs->Fields('ha_hotanula'));	
		$res= new stdClass();		
		$res->status=($rs->Fields('cot_estado')==0)?"CONFIRMED":"CANCELED";
		$res->noshow=($rs->Fields('cot_noshow')==0)?"TRUE":"FALSE";
		$res->refundable=($rs->Fields('cot_refundable')==0)?"TRUE":"FALSE";		
		$res->bookingcode=$rs->Fields('id_cot');
		$res->hotelid=$rs->Fields('cot_prihotel');
		$res->deadline=$desde->format('Y-m-d');
		$res->checkin=$hasta->format('Y-m-d');
		$res->checkout=$ha_hotanula->format('Y-m-d');
		$lista[]=$res;
		$rs->MoveNext();
	}
	return $lista;
}
?>