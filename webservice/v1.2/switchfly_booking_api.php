﻿<?php 
error_reporting(0);
ini_set('display_errors', 0);
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//$respuesta = call_user_func_array($funcion, array(simpleXml));
//print_r($respuesta);
$fileContents = file_get_contents("php://input");
$fileContentsOrig = $fileContents;
$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
$fileContents = substr($fileContents, 4, (strlen($fileContents)-1));
$fileContents = urldecode ($fileContents);
$fileContents = trim(str_replace('"', "'", $fileContents));
require('/var/www/otas/Connections/db1.php');
$simpleXml = simplexml_load_string($fileContents);
$funcion = substr($simpleXml->getName(), 0, (strlen($simpleXml->getName())-2));
$respuesta = $funcion($simpleXml, $fileContents, $fileContentsOrig);
echo $respuesta;


function Availability($formdata, $fileContents, $fileContentsOrig){
	$formdata = get_object_vars($formdata);
	require('/var/www/otas/Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = json_decode(json_encode($formdata), TRUE);
	//print_r($formdata);
	$id_webservice=13;    
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
	$currency = "CLP";
    //$currency = $formdata["Currency"];
    if(isset($formdata["RoomSearch"]["CheckinDate"])){
		if(isset($formdata["RoomSearch"]["SearchLocation"]["Location"]) || isset($formdata["RoomSearch"]["SearchLocation"]["AirportCode"])){
            $Location[0][0] = (isset($formdata["RoomSearch"]["SearchLocation"]["Location"]))?$formdata["RoomSearch"]["SearchLocation"]["Location"]:null;
			$AirportCode[0][0] = (isset($formdata["RoomSearch"]["SearchLocation"]["AirportCode"]))?$formdata["RoomSearch"]["SearchLocation"]["AirportCode"]:null;
			$contsearchloc[0] = 0;
        }else{
            foreach($formdata["RoomSearch"]["SearchLocation"] as $location_index=>$location_data){
                $Location[0][$location_index] = (isset($location_data["Location"]))?$location_data["Location"]:null;
				$AirportCode[0][$location_index] = (isset($location_data["AirportCode"]))?$location_data["AirportCode"]:null;
            }
			$contsearchloc[0] = count($formdata["RoomSearch"]["SearchLocation"]);
        }
        $RoomSearch[0]["MaxResults"] = $formdata["RoomSearch"]["MaxResults"];
		if(isset($formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"])){
            $adults[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"];
            $children[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["children"];
            if($children[0][0]==1){
				$ChildAge[0][0][0]=$formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"];
            }
            if($children[0][0]>1){
				foreach($formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
                    $ChildAge[0][0][$childindex] = $childdata["ChildAge"];
                }
            }
        }else{
            foreach($formdata["RoomSearch"]["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
                $adults[0][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
                $children[0][$roomocup_index] = $roomocu_data["@attributes"]["children"];
                if($children[0][$roomocup_index]==1){
					$ChildAge[0][$roomocup_index][0]= $roomocu_data["ChildAge"];
                }
                if($children[0][$roomocup_index]>1){
					foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
                        $ChildAge[0][$roomocup_index][$childindex] = $childdata["ChildAge"];
                    }
                }
            }
        }
        $RoomSearch[0]["CheckinDate"] = $formdata["RoomSearch"]["CheckinDate"];
        $RoomSearch[0]["CheckoutDate"] = $formdata["RoomSearch"]["CheckoutDate"];
    }else{
		foreach($formdata["RoomSearch"] as $roomindex=>$roomdata){
			if(isset($roomdata["SearchLocation"]["Location"]) || isset($roomdata["SearchLocation"]["AirportCode"])){
				$Location[$roomindex][0] = (isset($roomdata["SearchLocation"]["Location"]))?$roomdata["SearchLocation"]["Location"]:null;
				$AirportCode[$roomindex][0] = (isset($roomdata["SearchLocation"]["AirportCode"]))?$roomdata["SearchLocation"]["AirportCode"]:null;
				$contsearchloc[$roomindex] = 0;
			}else{
				foreach($roomdata["SearchLocation"] as $location_index=>$location_data){
					$Location[$roomindex][$location_index] = (isset($location_data["Location"]))?$location_data["Location"]:null;
					$AirportCode[$roomindex][$location_index] = (isset($location_data["AirportCode"]))?$location_data["AirportCode"]:null;
				}
				$contsearchloc[$roomindex] = count($roomdata["SearchLocation"]);
			}
			$RoomSearch[$roomindex]["MaxResults"] = $roomdata["MaxResults"];
			if(isset($roomdata["RoomOccupancy"]["@attributes"]["adults"])){
				$adults[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["adults"];
				$children[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["children"];
				if($children[$roomindex][0]==1){
					$ChildAge[$roomindex][0][0]=$roomdata["RoomOccupancy"]["ChildAge"];
				}
				if($children[$roomindex][0]>1){
					foreach($roomdata["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
						$ChildAge[$roomindex][0][$childindex] = $childdata["ChildAge"];
					}
				}
			}else{
				foreach($roomdata["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
					$adults[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
					$children[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["children"];
					if($children[$roomindex][$roomocup_index]==1){
						$ChildAge[$roomindex][$roomocup_index][0]=$roomocu_data["ChildAge"];
					}
					if($children[$roomindex][$roomocup_index]>1){
						foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
							$ChildAge[$roomindex][$roomocup_index][$childindex] = $childdata["ChildAge"];
						}
					}
				}
			}
			$RoomSearch[$roomindex]["CheckinDate"] = $roomdata["CheckinDate"];
			$RoomSearch[$roomindex]["CheckoutDate"] = $roomdata["CheckoutDate"];
		}
	}
	if($currency=="CLP"){
		$foreign = false;
	}elseif($currency=="USD"){
		$foreign = true;
	}else{
		return (0);
	}
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$AvailabilityRS = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><AvailabilityRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Success/>$string</AvailabilityRS>");
	for($i=0;$i<count($RoomSearch);$i++){
		$habs[0]['sgl']=0;
		$habs[0]['twin']=0;
		$habs[0]['mat']=0;
		$habs[0]['tpl']=0;
		foreach($adults[$i] as $adult_index => $adult_data){
			if($adult_data==1){
				$habs[0]['sgl']+=1;
			}elseif($adult_data==2){
				$haydbbl=true;
				$habs[0]['twin']+=1;
			}elseif($adult_data==3){
				$habs[0]['tpl']+=1;
			}
		}
		if($haydbbl){
			$habs[1]['sgl']=$habs[0]['sgl'];
			$habs[1]['twin']=0;
			$habs[1]['mat']=$habs[0]['twin'];
			$habs[1]['tpl']=$habs[0]['tpl'];
		}
		for($o=0;$o<count($contsearchloc[$i]);$o++){
			if($Location[$i][$o] != null){
				$descompone = explode(",", $Location[$i][$o]);		
				$ciudad = $descompone[0];
				$pais = $descompone[1];
				$sql = "select c.* from ciudad c inner join pais p on c.id_pais = p.id_pais where c.ciu_nombre like '%".trim($ciudad)."%' and p.pai_nombre like '%".trim($pais)."%'";
				$get_ciudad = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$cityid = $get_ciudad->Fields("id_ciudad");
				$countryid = $get_ciudad->Fields("id_pais");
				$airportcode = null;
			}else{
				$cityid=null;
				$airportcode = $AirportCode[$i][$o];
				$countryid = 4;
			}
			for($u=0;$u<count($habs);$u++){
				$childs=null;
				$arr[$u] = $cot->matrizDisponibilidadG($db1, $cityid, $RoomSearch[$i]["CheckinDate"], $RoomSearch[$i]["CheckoutDate"], $id_agencia, $habs[$u], null,null, $foreign,$childs, false, $airportcode);
			}			
			//$contHotel = (count($AvailabilityRS->RoomSearchResponse->Hotel)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel):0;
			$contHotel=0;
			foreach($arr[0] as $key=>$ways){
				$contRO = 0;
				foreach ($ways['info']['habs'] as $thab => $infohab){
					$u=0;
					$cadena_hotel = "".$key."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/".$countryid;					
					if(!isset($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor)){
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->AirportCode = utf8_encode($ways['info']['airportcode']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor = base64_encode($cadena_hotel);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelName=utf8_encode($ways['info']['name']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelCode=utf8_encode($key);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomType=utf8_encode($infohab['thname']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Address->AddressLine= utf8_encode($ways['info']['direccion']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Address->City= $ways['info']['City'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Address->State= $ways['info']['State'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Address->Country= $ways['info']['Country'];
						//echo $ways['info']['amenities'];
						//$ways['info']['amenities'] = "picina, bar";
						//var_dump($ways['info']['amenities']);die();
						if(isset($ways['info']['amenities']) && trim($ways['info']['amenities'])!=""){
							$ArrAmenity = explode(',',$ways['info']['amenities']);
							foreach($ArrAmenity as $indice => $amenitydata){
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Amenity[$indice] = $amenitydata;
							}
						}						
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Rating = $ways['info']['cat'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->PropertyDescription = utf8_encode($ways['info']['hotdesc']);						
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Checkin = utf8_encode($RoomSearch[$i]["CheckinDate"]."T".$ways['info']['checkin']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Checkout = utf8_encode($RoomSearch[$i]["CheckoutDate"]."T".$ways['info']['checkout']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Currency = $currency;
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->MinimumPrice = $infohab['totValue'];
					}
					$iva = ($infohab["taxes"]>0)?1.19:1;
					if($habs[$u]['sgl']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/1/0/0/0/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['sgl'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]."T".$ways['info']['checkin']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]."T".$ways['info']['checkout']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Code = "INCL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Name = $infohab['roomamenities'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);						
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= utf8_encode($ways['info']['direccion']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "SGL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal = round(($iva-1)*($infoday['sgl']/$infoday['markup'][0]['sgl']*count($ways['habs'][$thab])),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round(($iva-1)*($infoday['sgl']/$infoday['markup'][0]['sgl']*count($ways['habs'][$thab])),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
					if($habs[$u]['twin']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/1/0/0/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['twin'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]."T".$ways['info']['checkin']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]."T".$ways['info']['checkout']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Code = "INCL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Name = $infohab['roomamenities'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= utf8_encode($ways['info']['direccion']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "TWIN";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round(($iva-1)*($infoday['dbl']*2/$infoday['markup'][0]['twin']*count($ways['habs'][$thab])),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round(($iva-1)*($infoday['dbl']*2/$infoday['markup'][0]['twin']*count($ways['habs'][$thab])),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
					if($habs[$u]['tpl']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/0/0/1/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['tpl'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]."T".$ways['info']['checkin']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]."T".$ways['info']['checkout']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Code = "INCL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Name = $infohab['roomamenities'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= utf8_encode($ways['info']['direccion']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "TPL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round(($iva-1)*($infoday['tpl']*3/$infoday['markup'][0]['tpl']*count($ways['habs'][$thab])),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round(($iva-1)*($infoday['tpl']*3/$infoday['markup'][0]['tpl']*count($ways['habs'][$thab])),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
				}
				foreach($arr[1][$key]['info']['habs'] as $thab => $infohab){
					$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/0/1/0/".$countryid."/".$infohab['totValue'];
					$u=1;
					if($habs[1]['mat']>0){
						for($x=0;$x<$habs[1]['mat'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]."T".$ways['info']['checkin']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]."T".$ways['info']['checkout']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Code = "INCL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->IncludedService->Name = $infohab['roomamenities'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= utf8_encode($ways['info']['direccion']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "MATRIMONIAL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round(($iva-1)*($infoday['dbl']*2/$infoday['markup'][0]['mat']*count($ways['habs'][$thab])),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round(($iva-1)*($infoday['dbl']*2/$infoday['markup'][0]['mat']*count($ways['habs'][$thab])),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat'],$cot->decimales);
										$contaux++;
									}
								}									
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
				}
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->ThumbnailUrl = "https://distantis.com/otas/".$ways['info']['hotimg'];
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->Type= "Exterior";
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->Url= "https://distantis.com/otas/".$ways['info']['hotimg'];
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Thumbnail = "https://distantis.com/otas/".$ways['info']['hotimg'];

				$contHotel++;
			}
		}
	}  
	//die();
	//print_r($AvailabilityRS);
	return $AvailabilityRS->asXML();
	//$domxml = dom_import_simplexml($AvailabilityRS);
	//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
}

function Availability_163($formdata, $fileContents, $fileContentsOrig){
	$formdata = get_object_vars($formdata);
	require('/var/www/otas/Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = json_decode(json_encode($formdata), TRUE);
	//print_r($formdata);
	$id_webservice=13;    
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
	$currency = "CLP";
    //$currency = $formdata["Currency"];
    if(isset($formdata["RoomSearch"]["CheckinDate"])){
		if(isset($formdata["RoomSearch"]["SearchLocation"]["Location"])){
            $Location[0][0] = $formdata["RoomSearch"]["SearchLocation"]["Location"];
        }else{
            foreach($formdata["RoomSearch"]["SearchLocation"] as $location_index=>$location_data){
                $Location[0][$location_index]["Nombre"] = $location_data["Location"];
            }
        }
        $RoomSearch[0]["MaxResults"] = $formdata["RoomSearch"]["MaxResults"];
		if(isset($formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"])){
            $adults[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"];
            $children[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["children"];
            if($children[0][0]==1){
				$ChildAge[0][0][0]=$formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"];
            }
            if($children[0][0]>1){
				foreach($formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
                    $ChildAge[0][0][$childindex] = $childdata["ChildAge"];
                }
            }
        }else{
            foreach($formdata["RoomSearch"]["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
                $adults[0][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
                $children[0][$roomocup_index] = $roomocu_data["@attributes"]["children"];
                if($children[0][$roomocup_index]==1){
					$ChildAge[0][$roomocup_index][0]= $roomocu_data["ChildAge"];
                }
                if($children[0][$roomocup_index]>1){
					foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
                        $ChildAge[0][$roomocup_index][$childindex] = $childdata["ChildAge"];
                    }
                }
            }
        }
        $RoomSearch[0]["CheckinDate"] = $formdata["RoomSearch"]["CheckinDate"];
        $RoomSearch[0]["CheckoutDate"] = $formdata["RoomSearch"]["CheckoutDate"];
    }else{
		foreach($formdata["RoomSearch"] as $roomindex=>$roomdata){
			if(isset($roomdata["SearchLocation"]["Location"])){
				$Location[$roomindex][0] = $roomdata["SearchLocation"]["Location"];
			}else{
				foreach($roomdata["SearchLocation"] as $location_index=>$location_data){
					$Location[$roomindex][$location_index]["Nombre"] = $location_data["Location"];
				}
			}
			$RoomSearch[$roomindex]["MaxResults"] = $roomdata["MaxResults"];
			if(isset($roomdata["RoomOccupancy"]["@attributes"]["adults"])){
				$adults[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["adults"];
				$children[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["children"];
				if($children[$roomindex][0]==1){
					$ChildAge[$roomindex][0][0]=$roomdata["RoomOccupancy"]["ChildAge"];
				}
				if($children[$roomindex][0]>1){
					foreach($roomdata["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
						$ChildAge[$roomindex][0][$childindex] = $childdata["ChildAge"];
					}
				}
			}else{
				foreach($roomdata["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
					$adults[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
					$children[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["children"];
					if($children[$roomindex][$roomocup_index]==1){
						$ChildAge[$roomindex][$roomocup_index][0]=$roomocu_data["ChildAge"];
					}
					if($children[$roomindex][$roomocup_index]>1){
						foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
							$ChildAge[$roomindex][$roomocup_index][$childindex] = $childdata["ChildAge"];
						}
					}
				}
			}
			$RoomSearch[$roomindex]["CheckinDate"] = $roomdata["CheckinDate"];
			$RoomSearch[$roomindex]["CheckoutDate"] = $roomdata["CheckoutDate"];
		}
	}
	if($currency=="CLP"){
		$foreign = false;
	}elseif($currency=="USD"){
		$foreign = true;
	}else{
		return (0);
	}
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$AvailabilityRS = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><AvailabilityRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Success/>$string</AvailabilityRS>");
	for($i=0;$i<count($RoomSearch);$i++){
		$habs[0]['sgl']=0;
		$habs[0]['twin']=0;
		$habs[0]['mat']=0;
		$habs[0]['tpl']=0;
		foreach($adults[$i] as $adult_index => $adult_data){
			if($adult_data==1){
				$habs[0]['sgl']+=1;
			}elseif($adult_data==2){
				$haydbbl=true;
				$habs[0]['twin']+=1;
			}elseif($adult_data==3){
				$habs[0]['tpl']+=1;
			}
		}
		if($haydbbl){
			$habs[1]['sgl']=$habs[0]['sgl'];
			$habs[1]['twin']=0;
			$habs[1]['mat']=$habs[0]['twin'];
			$habs[1]['tpl']=$habs[0]['tpl'];
		}
		for($o=0;$o<count($Location[$i]);$o++){
			$descompone = explode(",", $Location[$i][$o]);		
			$ciudad = $descompone[0];
			$pais = $descompone[1];
			$sql = "select c.* from ciudad c inner join pais p on c.id_pais = p.id_pais where c.ciu_nombre like '%".trim($ciudad)."%' and p.pai_nombre like '%".trim($pais)."%'";
			$get_ciudad = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$cityid = $get_ciudad->Fields("id_ciudad");
			$countryid = $get_ciudad->Fields("id_pais");
			for($u=0;$u<count($habs);$u++){
				$childs=null;
				$arr[$u] = $cot->matrizDisponibilidadG($db1, $cityid, $RoomSearch[$i]["CheckinDate"], $RoomSearch[$i]["CheckoutDate"], $id_agencia, $habs[$u], null,null, $foreign,$childs);
				$contRO = 0;
				$contHotel = (count($AvailabilityRS->RoomSearchResponse->Hotel)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel):0;
				//$contHotel=0;
				foreach($arr[$u] as $key=>$ways){
					foreach ($ways['info']['habs'] as $thab => $infohab){
						$cadena_hotel = "".$key."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$cityid."/".$habs[$u]['sgl']."/".$habs[$u]['twin']."/".$habs[$u]['mat']."/".$habs[$u]['tpl']."/".$countryid;
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$cityid."/".$habs[$u]['sgl']."/".$habs[$u]['twin']."/".$habs[$u]['mat']."/".$habs[$u]['tpl']."/".$countryid."/".$infohab['totValue'];
						if(!isset($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor)){
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor = base64_encode($cadena_hotel);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelName=utf8_encode($ways['info']['name']);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelCode=utf8_encode($key);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomType=utf8_encode($infohab['thname']);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->AddressLine= $ways['info']['AddressLine'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->AddressLine= "adressline";
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->City= $ways['info']['City'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->State= $ways['info']['State'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->Country= $ways['info']['Country'];														
							$ways['info']['amenities'] = "picina, bar";
							$ArrAmenity = explode(',',$ways['info']['amenities']);
							foreach($ArrAmenity as $indice => $amenitydata){
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Amenity[$indice] = $amenitydata;
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Rating = $ways['info']['rating'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Currency = $currency;
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->MinimumPrice = $infohab['totValue'];
						}						
						$iva = ($infohab["taxes"]>0)?1.19:1;
						$contRO = (count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy):0;
						//echo "INICIO    u: $u    contHotel : $contHotel   contRO: $contRO";
						if($habs[$u]['sgl']>0){
							for($x=0;$x<$habs[$u]['sgl'];$x++){
								$contdia = 0;
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									//$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Adults=$adults[$i][$u];
									//$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->HotelCode=utf8_encode($key);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->BookingDescriptor= base64_encode($cadena);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RateName= "SGL";
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Currency = $currency;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->PassThrough = false;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Total =round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->TaxTotal =round($infoday['sgl']/$infoday['markup'][0]['sgl']*count($ways['habs'][$thab]),$cot->decimales);
									if($contdia==0){
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotal[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva,$cot->decimales);
											$contaux++;
										}
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl'],$cot->decimales);
											$contaux++;
										}
									}
									if($contdia<=count($ways['habs'][$thab])){
										$contdia++;
									}
								}
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							}
						}
						$contRO = (count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy):0;
						if($habs[$u]['twin']>0){
							for($x=0;$x<$habs[$u]['twin'];$x++){
								$contdia = 0;
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->HotelCode=utf8_encode($key);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->BookingDescriptor= base64_encode($cadena);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RateName= "TWIN";
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Currency = $currency;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->PassThrough = false;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup'][0]['twin']*count($ways['habs'][$thab]),$cot->decimales);
									if($contdia==0){
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva,$cot->decimales);
											$contaux++;
										}
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin'],$cot->decimales);
											$contaux++;
										}
									}
									if($contdia<=count($ways['habs'][$thab])){
										$contdia++;
									}
								}
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							}
						}
						$contRO = (count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy):0;
						if($habs[$u]['mat']>0){
							for($x=0;$x<$habs[$u]['mat'];$x++){
								$contdia = 0;
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->HotelCode=utf8_encode($key);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->BookingDescriptor= base64_encode($cadena);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RateName= "MATRIMONIAL";
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Currency = $currency;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->PassThrough = false;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup'][0]['mat']*count($ways['habs'][$thab]),$cot->decimales);									
									if($contdia==0){
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva,$cot->decimales);
											$contaux++;
										}
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat'],$cot->decimales);
											$contaux++;
										}
									}									
									if($contdia<=count($ways['habs'][$thab])){
										$contdia++;
									}
								}
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							}
						}
						$contRO = (count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy):0;
						if($habs[$u]['tpl']>0){
							for($x=0;$x<$habs[$u]['tpl'];$x++){
								$contdia = 0;
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Adults=$adults[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->Children=$children[$i][$u];
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->HotelCode=utf8_encode($key);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->BookingDescriptor= base64_encode($cadena);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RateName= "TPL";
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Currency = $currency;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->PassThrough = false;
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->Total =round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->TaxTotal =round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*count($ways['habs'][$thab]),$cot->decimales);
									if($contdia==0){
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotal[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva,$cot->decimales);
											$contaux++;
										}
										$contaux =0;
										foreach($ways['habs'][$thab] as $fecha => $infoday) {
											$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl'],$cot->decimales);
											$contaux++;
										}
									}
									if($contdia<=count($ways['habs'][$thab])){
										$contdia++;
									}
								}
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomOccupancy[$contRO]->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							}
						}
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->ThumbnailUrl = "https://distantis.com/otas/images/imgfichahotel/descarga.jpg";
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->Type= "Exterior";
					}
				}
			}
		}
	}
	//print_r($AvailabilityRS);
	return $AvailabilityRS->asXML();
	//$domxml = dom_import_simplexml($AvailabilityRS);
	//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
}

function CancelPnr($formdata, $fileContents, $fileContentsOrig){	
	$id_webservice=13;
	require('/var/www/otas/Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$sql = 'insert into test.xmls (operador, rq_original, rq_procesado, fecha_rq) values("LATAM", "'.ereg_replace('"', "'",$fileContentsOrig).'","'.ereg_replace('"', "'", $fileContents).'",now())';
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$last_xml=$db1->Insert_ID();
	$cot = new Cotizacion();
	$formdata = get_object_vars($formdata);
	$formdata = json_decode(json_encode($formdata), TRUE);
	$user = $formdata["UserId"];
	$password = $formdata["Password"];
	$bookingcode = $formdata["RecordLocator"];
	$usuario_rq  ="select u.*, a.ag_espanula from usuarios u join agencia a 
		on u.id_agencia = a.id_agencia where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$CancelRS = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><CancelPnrRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>$string</CancelPnrRS>");
	if($usuario_rs->RecordCount()==0){
		$CancelRS->Failure = "Invalid user";
		$CancelRS->Failure->addAttribute("type", "error");
		$CancelRS->Failure->addAttribute("code", "INTERNAL_ERROR");
		$CancelRS->Failure->addAttribute("translation", "translate");
		$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$CancelRS->Pnr->Status =$result['description'];
		$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		$CancelRS->Failure = "Not allowed user";
		$CancelRS->Failure->addAttribute("type", "error");
		$CancelRS->Failure->addAttribute("code", "INTERNAL_ERROR");
		$CancelRS->Failure->addAttribute("translation", "translate");
		$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$CancelRS->Pnr->Status =$result['description'];
		$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}
	$cot_rq  ="select * from cot where id_cot = $bookingcode and id_seg = 7 and cot_estado = 0";
	$cot_rs = $db1->Execute($cot_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($cot_rs->RecordCount()==0){
		$CancelRS->Failure = "Booking must be confirmed";
		$CancelRS->Failure->addAttribute("type", "error");
		$CancelRS->Failure->addAttribute("code", "INTERNAL_ERROR");
		$CancelRS->Failure->addAttribute("translation", "translate");
		$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$CancelRS->Pnr->Status =$result['description'];
		$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}
	$sql= "SELECT TIME_TO_SEC(TIMEDIFF(NOW(), ha_hotanula)) AS diff FROM cot WHERE id_cot = $bookingcode";
	$rs= $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($rs->Fields("diff")>=0 && $usuario_rs->Fields("ag_espanula")==1){
		$CancelRS->Failure = "User can not cancel out of deadline";
		$CancelRS->Failure->addAttribute("type", "error");
		$CancelRS->Failure->addAttribute("code", "INTERNAL_ERROR");
		$CancelRS->Failure->addAttribute("translation", "translate");
		$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$CancelRS->Pnr->Status =$result['description'];
		$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}
	$result = $cot->cotAnulator($db1, $bookingcode,$id_usuario);
	if($result['penalty']!=0 || $result['status']!='SUCCESS'){
		$CancelRS->Failure = $result['description'];
		$CancelRS->Failure->addAttribute("type", "error");
		$CancelRS->Failure->addAttribute("code", "INTERNAL_ERROR");
		$CancelRS->Failure->addAttribute("translation", "translate");
		$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$CancelRS->Pnr->Status =$result['description'];
		$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}else{		
		$CancelRS->Success = $result['status'];
		//$CancelRS->Pnr->RecordLocator = $formdata["RecordLocator"];
		//$now = getdate();
		//$CancelRS->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		//$CancelRS->Pnr->Status =$result['status'];
		//$CancelRS->Pnr->SegmentTotals->AirTotal = 0;
		//$CancelRS->Pnr->SegmentTotals->AirTaxTotal = 0;
		//$CancelRS->Pnr->SegmentTotals->RoomTotal = 0;
		//$CancelRS->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		//$CancelRS->Pnr->SegmentTotals->CarTotal = 0;
		//$CancelRS->Pnr->SegmentTotals->CarTaxTotal = 0;
		//$CancelRS->Pnr->SegmentTotals->ActivityTotal = 0;
		/*$CancelRS->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTotal = 0;
		$CancelRS->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$CancelRS->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$CancelRS->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeClient = 0;
		$CancelRS->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkup = 0;
		$CancelRS->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$CancelRS->Pnr->SegmentTotals->DiscountTotal = 0;
		$CancelRS->Pnr->SegmentTotals = "";
		$CancelRS->Pnr->DepartureDate = "";
		$CancelRS->Pnr->PnrOwner  = "";
		$CancelRS->Pnr->LeadTraveler  = "";
		$CancelRS->Pnr->SystemPricingSummary  = "";
		$CancelRS->Pnr->CustomerPricingSummary = "";*/
		$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CancelRS->asXML()).'" where id_xml ='.$last_xml;
		$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $CancelRS->asXML();
		//$domxml = dom_import_simplexml($CancelRS);
		//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	}
}

function testRQ($formdata, $fileContents, $fileContentsOrig){
	//echo "entro";
	//print_r($formdata);die();
	$formdata = get_object_vars($formdata);
	//print_r(json_decode($formdata));die();
	$formdata = json_decode(json_encode($formdata), TRUE);
	//print_r($formdata);die();
	$xml = simplexml_load_string("<root>$string</root>");
	
	//$domxml = dom_import_simplexml($xml);
	//$domxml->ownerDocument->encoding ='UTF-8';
	//return $domxml->ownerDocument->saveXML();
	$xml->bye[0]->hello = 'something';
	$xml->bye[0]->hello->addAttribute("nombre", "pesrito");
	$xml->bye[0]->wtf[] = null;
	
	$var =  count($xml->bye);
	$xml->bye[$var]->seeyou = 'later';
	$xml->bye[$var]->seeyou->addAttribute("nombre", "gata");
	$var =  count($xml->bye);
	$xml->bye[$var]->never = 'lose';
	$xml->bye[$var]->never->addAttribute("nombre", "mind");
	$xml->bye[$var]->wtf="";
	//print_r($xml);
	//return strip_tags($xml->asXML());
	$domxml = dom_import_simplexml($xml);
	$domxml->ownerDocument->encoding ='UTF-8';
	return $domxml->ownerDocument->saveXML();
	return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
	//return ;
}

function AvailabilityPruebas($formdata, $fileContents, $fileContentsOrig){
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	$formdata = get_object_vars($formdata);
	require('/var/www/otas/Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = json_decode(json_encode($formdata), TRUE);
	//print_r($formdata);
	$id_webservice=13;    
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
	$currency = "CLP";
    //$currency = $formdata["Currency"];
    if(isset($formdata["RoomSearch"]["CheckinDate"])){
		if(isset($formdata["RoomSearch"]["SearchLocation"]["Location"]) || isset($formdata["RoomSearch"]["SearchLocation"]["AirportCode"])){
            $Location[0][0] = (isset($formdata["RoomSearch"]["SearchLocation"]["Location"]))?$formdata["RoomSearch"]["SearchLocation"]["Location"]:null;
			$AirportCode[0][0] = (isset($formdata["RoomSearch"]["SearchLocation"]["AirportCode"]))?$formdata["RoomSearch"]["SearchLocation"]["AirportCode"]:null;
			$contsearchloc[0] = 0;
        }else{
            foreach($formdata["RoomSearch"]["SearchLocation"] as $location_index=>$location_data){
                $Location[0][$location_index] = (isset($location_data["Location"]))?$location_data["Location"]:null;
				$AirportCode[0][$location_index] = (isset($location_data["AirportCode"]))?$location_data["AirportCode"]:null;
            }
			$contsearchloc[0] = count($formdata["RoomSearch"]["SearchLocation"]);
        }
        $RoomSearch[0]["MaxResults"] = $formdata["RoomSearch"]["MaxResults"];
		if(isset($formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"])){
            $adults[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["adults"];
            $children[0][0] = $formdata["RoomSearch"]["RoomOccupancy"]["@attributes"]["children"];
            if($children[0][0]==1){
				$ChildAge[0][0][0]=$formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"];
            }
            if($children[0][0]>1){
				foreach($formdata["RoomSearch"]["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
                    $ChildAge[0][0][$childindex] = $childdata["ChildAge"];
                }
            }
        }else{
            foreach($formdata["RoomSearch"]["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
                $adults[0][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
                $children[0][$roomocup_index] = $roomocu_data["@attributes"]["children"];
                if($children[0][$roomocup_index]==1){
					$ChildAge[0][$roomocup_index][0]= $roomocu_data["ChildAge"];
                }
                if($children[0][$roomocup_index]>1){
					foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
                        $ChildAge[0][$roomocup_index][$childindex] = $childdata["ChildAge"];
                    }
                }
            }
        }
        $RoomSearch[0]["CheckinDate"] = $formdata["RoomSearch"]["CheckinDate"];
        $RoomSearch[0]["CheckoutDate"] = $formdata["RoomSearch"]["CheckoutDate"];
    }else{
		foreach($formdata["RoomSearch"] as $roomindex=>$roomdata){
			if(isset($roomdata["SearchLocation"]["Location"]) || isset($roomdata["SearchLocation"]["AirportCode"])){
				$Location[$roomindex][0] = (isset($roomdata["SearchLocation"]["Location"]))?$roomdata["SearchLocation"]["Location"]:null;
				$AirportCode[$roomindex][0] = (isset($roomdata["SearchLocation"]["AirportCode"]))?$roomdata["SearchLocation"]["AirportCode"]:null;
				$contsearchloc[$roomindex] = 0;
			}else{
				foreach($roomdata["SearchLocation"] as $location_index=>$location_data){
					$Location[$roomindex][$location_index] = (isset($location_data["Location"]))?$location_data["Location"]:null;
					$AirportCode[$roomindex][$location_index] = (isset($location_data["AirportCode"]))?$location_data["AirportCode"]:null;
				}
				$contsearchloc[$roomindex] = count($roomdata["SearchLocation"]);
			}
			$RoomSearch[$roomindex]["MaxResults"] = $roomdata["MaxResults"];
			if(isset($roomdata["RoomOccupancy"]["@attributes"]["adults"])){
				$adults[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["adults"];
				$children[$roomindex][0] = $roomdata["RoomOccupancy"]["@attributes"]["children"];
				if($children[$roomindex][0]==1){
					$ChildAge[$roomindex][0][0]=$roomdata["RoomOccupancy"]["ChildAge"];
				}
				if($children[$roomindex][0]>1){
					foreach($roomdata["RoomOccupancy"]["ChildAge"] as $childindex => $childdata){
						$ChildAge[$roomindex][0][$childindex] = $childdata["ChildAge"];
					}
				}
			}else{
				foreach($roomdata["RoomOccupancy"] as $roomocup_index=>$roomocu_data){
					$adults[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["adults"];
					$children[$roomindex][$roomocup_index] = $roomocu_data["@attributes"]["children"];
					if($children[$roomindex][$roomocup_index]==1){
						$ChildAge[$roomindex][$roomocup_index][0]=$roomocu_data["ChildAge"];
					}
					if($children[$roomindex][$roomocup_index]>1){
						foreach($roomocu_data["ChildAge"] as $childindex => $childdata){
							$ChildAge[$roomindex][$roomocup_index][$childindex] = $childdata["ChildAge"];
						}
					}
				}
			}
			$RoomSearch[$roomindex]["CheckinDate"] = $roomdata["CheckinDate"];
			$RoomSearch[$roomindex]["CheckoutDate"] = $roomdata["CheckoutDate"];
		}
	}
	if($currency=="CLP"){
		$foreign = false;
	}elseif($currency=="USD"){
		$foreign = true;
	}else{
		return (0);
	}
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$AvailabilityRS = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><AvailabilityRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Success/>$string</AvailabilityRS>");
	for($i=0;$i<count($RoomSearch);$i++){
		$habs[0]['sgl']=0;
		$habs[0]['twin']=0;
		$habs[0]['mat']=0;
		$habs[0]['tpl']=0;
		foreach($adults[$i] as $adult_index => $adult_data){
			if($adult_data==1){
				$habs[0]['sgl']+=1;
			}elseif($adult_data==2){
				$haydbbl=true;
				$habs[0]['twin']+=1;
			}elseif($adult_data==3){
				$habs[0]['tpl']+=1;
			}
		}
		if($haydbbl){
			$habs[1]['sgl']=$habs[0]['sgl'];
			$habs[1]['twin']=0;
			$habs[1]['mat']=$habs[0]['twin'];
			$habs[1]['tpl']=$habs[0]['tpl'];
		}
		for($o=0;$o<count($contsearchloc[$i]);$o++){
			if($Location[$i][$o] != null){
				$descompone = explode(",", $Location[$i][$o]);		
				$ciudad = $descompone[0];
				$pais = $descompone[1];
				$sql = "select c.* from ciudad c inner join pais p on c.id_pais = p.id_pais where c.ciu_nombre like '%".trim($ciudad)."%' and p.pai_nombre like '%".trim($pais)."%'";
				$get_ciudad = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$cityid = $get_ciudad->Fields("id_ciudad");
				$countryid = $get_ciudad->Fields("id_pais");
				$airportcode = null;
			}else{
				$cityid=null;
				$airportcode = $AirportCode[$i][$o];
				$countryid = 4;
			}
			for($u=0;$u<count($habs);$u++){
				$childs=null;
				$arr[$u] = $cot->matrizDisponibilidadG($db1, $cityid, $RoomSearch[$i]["CheckinDate"], $RoomSearch[$i]["CheckoutDate"], $id_agencia, $habs[$u], null,null, $foreign,$childs, false, $airportcode);
			}			
			//$contHotel = (count($AvailabilityRS->RoomSearchResponse->Hotel)>=1)?count($AvailabilityRS->RoomSearchResponse->Hotel):0;
			$contHotel=0;
			foreach($arr[0] as $key=>$ways){
				$contRO = 0;
				foreach ($ways['info']['habs'] as $thab => $infohab){
					$u=0;
					$cadena_hotel = "".$key."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/".$countryid;					
					if(!isset($AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor)){
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->AirportCode = utf8_encode($ways['info']['airportcode']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->BookingDescriptor = base64_encode($cadena_hotel);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelName=utf8_encode($ways['info']['name']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->HotelCode=utf8_encode($key);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomType=utf8_encode($infohab['thname']);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->AddressLine= $ways['info']['AddressLine'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->AddressLine= "adressline";
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->City= $ways['info']['City'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->State= $ways['info']['State'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Adress->Country= $ways['info']['Country'];
						$ways['info']['amenities'] = "picina, bar";
						$ArrAmenity = explode(',',$ways['info']['amenities']);
						foreach($ArrAmenity as $indice => $amenitydata){
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Amenity[$indice] = $amenitydata;
						}
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Rating = $ways['info']['rating'];
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Checkin = utf8_encode($RoomSearch[$i]["CheckinDate"]);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Checkout = utf8_encode($RoomSearch[$i]["CheckoutDate"]);
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Currency = $currency;
						$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->MinimumPrice = $infohab['totValue'];
					}
					$iva = ($infohab["taxes"]>0)?1.19:1;
					if($habs[$u]['sgl']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/1/0/0/0/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['sgl'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);						
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= $ways['info']['AddressLine'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= "adressline";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "SGL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round($infoday['sgl']/$infoday['markup'][0]['sgl']*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round($infoday['sgl']/$infoday['markup'][0]['sgl']*count($ways['habs'][$thab]),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['sgl']/$infoday['markup'][0]['sgl'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
					if($habs[$u]['twin']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/1/0/0/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['twin'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= $ways['info']['AddressLine'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= "adressline";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "TWIN";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup'][0]['twin']*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round($infoday['dbl']*2/$infoday['markup'][0]['twin']*count($ways['habs'][$thab]),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['twin'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
					if($habs[$u]['tpl']>0){
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/0/0/1/".$countryid."/".$infohab['totValue'];
						for($x=0;$x<$habs[$u]['tpl'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= $ways['info']['AddressLine'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= "adressline";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "TPL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*count($ways['habs'][$thab]),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['tpl']*3/$infoday['markup'][0]['tpl'],$cot->decimales);
										$contaux++;
									}
								}
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
				}
				foreach($arr[1][$key]['info']['habs'] as $thab => $infohab){
					$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$ways['info']['id_ciudad']."/0/0/1/0/".$countryid."/".$infohab['totValue'];
					$u=1;
					if($habs[1]['mat']>0){
						for($x=0;$x<$habs[1]['mat'];$x++){
							$contdia = 0;
							foreach($ways['habs'][$thab] as $fecha => $infoday) {
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->BookingDescriptor= base64_encode($cadena);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkin= utf8_encode($RoomSearch[$i]["CheckinDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Checkout= utf8_encode($RoomSearch[$i]["CheckoutDate"]);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adults=$adults[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Children=$children[$i][$u];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelCode=utf8_encode($key);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->HotelName=utf8_encode($ways['info']['name']);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= $ways['info']['AddressLine'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->AddressLine= "adressline";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->City= $ways['info']['City'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->State= $ways['info']['State'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->Adress->Country= $ways['info']['Country'];
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomName= "MATRIMONIAL";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->IncludesTaxesAndExtraCharges = "true";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Currency = $currency;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->PassThrough = "false";
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup'][0]['mat']*count($ways['habs'][$thab]),$cot->decimales);
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax = null;
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Type", "CL_IVA");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Name", "Other");
								$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->TaxDetails->Tax->addAttribute("Amount", round($infoday['dbl']*2/$infoday['markup'][0]['mat']*count($ways['habs'][$thab]),$cot->decimales));
								if($contdia==0){
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotal[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat']*$iva,$cot->decimales);
										$contaux++;
									}
									$contaux =0;
									foreach($ways['habs'][$thab] as $fecha => $infoday) {
										$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomPrice->DailyTotalNoTax[$contaux]=round($infoday['dbl']*2/$infoday['markup'][0]['mat'],$cot->decimales);
										$contaux++;
									}
								}									
								if($contdia<=count($ways['habs'][$thab])){
									$contdia++;
								}
							}
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->Description = "En caso de cancelación desde del día ".$ways['info']['deadline'].",se aplicará unos gastos de CLP".$infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->addAttribute("Currency", $currency);
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount = $infohab["penalty"];
							$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->RoomSegment[$contRO]->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->addAttribute("Currency", $currency);
							$contRO++;
						}
					}
				}
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->ThumbnailUrl = "https://distantis.com/otas/images/imgfichahotel/descarga.jpg";
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->Type= "Exterior";
				$AvailabilityRS->RoomSearchResponse->Hotel[$contHotel]->Image->Url= "https://distantis.com/otas/images/imgfichahotel/descarga.jpg";
				$contHotel++;
			}
		}
	}
	//die();
	//print_r($AvailabilityRS);
	return $AvailabilityRS->asXML();
	//$domxml = dom_import_simplexml($AvailabilityRS);
	//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
}

function CreatePnr($formdata, $fileContents, $fileContentsOrig){
    $id_webservice=13;
    require('/var/www/otas/Connections/db1.php');
    require_once('/var/www/otas/clases/cotizacion.php');
	$sql = 'insert into test.xmls (operador, rq_original, rq_procesado, fecha_rq) values("LATAM", "'.ereg_replace('"', "'",$fileContentsOrig).'","'.ereg_replace('"', "'", $fileContents).'",now())';
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$last_xml=$db1->Insert_ID();
    $cot = new Cotizacion();
    $formdata = get_object_vars($formdata);
	$formdata = json_decode(json_encode($formdata), TRUE);
	//print_r($formdata);
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
	$currency = "CLP";
    //$currency = $formdata["Currency"];
	if(isset($formdata["RoomSegmentRequest"]["RoomSegment"])){
		$extras[0]['clientcode'] = $formdata["RoomSegmentRequest"]["CorrelationId"];
		$RoomSegmentRequest[0] = base64_decode($formdata["RoomSegmentRequest"]["RoomSegment"]);
		$aux[0] = explode("/", $RoomSegmentRequest[0]);
		if(isset($formdata["RoomSegmentRequest"]["RoomingList"]["Guest"])){
			//cuando es un romminglist en un roomsegment
			if(isset($formdata["RoomSegmentRequest"]["RoomingList"]["Guest"]["FirstName"])){
				//cuando es un pasajero en un rominglist
				$paxs[0][0][0]["name"] = $formdata["RoomSegmentRequest"]["RoomingList"]["Guest"]["FirstName"];
				$paxs[0][0][0]["lastname"] = $formdata["RoomSegmentRequest"]["RoomingList"]["Guest"]["LastName"];
				$paxs[0][0][0]["passport"] = $formdata["RoomSegmentRequest"]["RoomingList"]["Guest"]["OfficialTravelerId"];
				$paxs[0][0][0]["countryid"] = $aux[0][9];
			}else{
				foreach($formdata["RoomSegmentRequest"]["RoomingList"]["Guest"] as $paxindex => $paxdata){
					//cuando es mas de un pasajero en un roominglist
					$paxs[0][0][$paxindex]["name"] = $paxdata["FirstName"];
					$paxs[0][0][$paxindex]["lastname"] = $paxdata["LastName"];
					$paxs[0][0][$paxindex]["passport"] = $paxdata["OfficialTravelerId"];
					$paxs[0][0][$paxindex]["countryid"] = $aux[0][9];
				}
			}				
		}else{
			//cuando es mas de un roomingList en un roomsegment
			foreach($formdata["RoomSegmentRequest"]["RoomingList"] as $roomlindex => $roomldata){
				if(isset($roomldata["Guest"]["FirstName"])){
					//cuando es un pasajero en mas de un roomminglist
					$paxs[0][$roomlindex][0]["name"] = $roomldata["Guest"]["FirstName"];
					$paxs[0][$roomlindex][0]["lastname"] = $roomldata["Guest"]["LastName"];
					$paxs[0][$roomlindex][0]["passport"] = $roomldata["Guest"]["OfficialTravelerId"];
					$paxs[0][$roomlindex][0]["countryid"] = $aux[0][9];				
				}else{
					foreach($roomldata["Guest"] as $paxindex => $paxdata){
						//cuando es mas de un pasajero en mas de un roomingList
						$paxs[0][$roomlindex][$paxindex]["name"] = $paxdata["FirstName"];
						$paxs[0][$roomlindex][$paxindex]["lastname"] = $paxdata["LastName"];
						$paxs[0][$roomlindex][$paxindex]["passport"] = $paxdata["OfficialTravelerId"];
						$paxs[0][$roomlindex][$paxindex]["countryid"] = $aux[0][9];	
					}
				}
			}
		}
	}else{
		//cuando es mas de un roomsegment
		foreach($formdata["RoomSegmentRequest"] as $roomsindex => $roomsdata){
			$extras[$roomsindex]['clientcode'] = $roomsdata["CorrelationId"];
			$RoomSegmentRequest[$roomsindex] = base64_decode($roomsdata["RoomSegment"]);
			$aux[$roomsindex] = explode("/", $RoomSegmentRequest[$roomsindex]);
			if(isset($roomsdata["RoomingList"]["Guest"])){
				//cuando es un romminglist en mas de un roomsegment
				if(isset($roomsdata["RoomingList"]["Guest"]["FirstName"])){
					//cuando es un pasajero en un rominglist
					$paxs[$roomsindex][0][0]["name"] = $roomsdata["RoomingList"]["Guest"]["FirstName"];
					$paxs[$roomsindex][0][0]["lastname"] = $roomsdata["RoomingList"]["Guest"]["LastName"];
					$paxs[$roomsindex][0][0]["passport"] = $roomsdata["RoomingList"]["Guest"]["OfficialTravelerId"];
					$paxs[$roomsindex][0][0]["countryid"] = $aux[$roomsindex][9];
				}else{
					foreach($roomsdata["RoomingList"]["Guest"] as $paxindex => $paxdata){
						//cuando es mas de un pasajero en un roominglist
						$paxs[$roomsindex][0][$paxindex]["name"] = $paxdata["FirstName"];
						$paxs[$roomsindex][0][$paxindex]["lastname"] = $paxdata["LastName"];
						$paxs[$roomsindex][0][$paxindex]["passport"] = $paxdata["OfficialTravelerId"];
						$paxs[$roomsindex][0][$paxindex]["countryid"] = $aux[$roomsindex][9];
					}
				}				
			}else{
				//cuando es mas de un roomingList en mas de un roomsegment
				foreach($roomsdata["RoomingList"] as $roomlindex => $roomldata){
					if(isset($roomldata["Guest"]["FirstName"])){
						//cuando es un pasajero en mas de un roomminglist
						$paxs[$roomsindex][$roomlindex][0]["name"] = $roomldata["Guest"]["FirstName"];
						$paxs[$roomsindex][$roomlindex][0]["lastname"] = $roomldata["Guest"]["LastName"];
						$paxs[$roomsindex][$roomlindex][0]["passport"] = $roomldata["Guest"]["OfficialTravelerId"];
						$paxs[$roomsindex][$roomlindex][0]["countryid"] = $aux[$roomsindex][9];				
					}else{
						foreach($roomldata["Guest"] as $paxindex => $paxdata){
							//cuando es mas de un pasajero en mas de un roomingList
							$paxs[$roomsindex][$roomlindex][$paxindex]["name"] = $paxdata["FirstName"];
							$paxs[$roomsindex][$roomlindex][$paxindex]["lastname"] = $paxdata["LastName"];
							$paxs[$roomsindex][$roomlindex][$paxindex]["passport"] = $paxdata["OfficialTravelerId"];
							$paxs[$roomsindex][$roomlindex][$paxindex]["countryid"] = $aux[$roomsindex][9];
						}
					}
				}
			}
		}
	}
	$CreatePnr = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><CreatePnrRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>$string</CreatePnrRS>");
	for($i=0;$i<count($RoomSegmentRequest);$i++){
		$foreign = ($paxs[$i][0][0]["countryid"]==4)?"FALSE":"TRUE";
		if(strtoupper($foreign)=="FALSE" || $foreign=='0'){
			$currency_aux="CLP";
			$foreign=false;
		}else{
			$currency_aux="USD";
			$foreign=true;
		}
		$Pnr=new stdClass();
		$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
		$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($usuario_rs->RecordCount()==0){		
			$CreatePnr->Pnr[$i]->Failure = "Invalid user";
			$CreatePnr->Pnr[$i]->Failure->addAttribute("type","error");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("code","INTERNAL_ERROR");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("translation","translate");
			$i->MoveNext();
		}
		$id_agencia = $usuario_rs->Fields('id_agencia');
		$id_usuario = $usuario_rs->Fields('id_usuario');
		$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
		$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($agws_rs->RecordCount()==0){
			$CreatePnr->Pnr[$i]->Failure = "Not allowed user";
			$CreatePnr->Pnr[$i]->Failure->addAttribute("type","error");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("code","INTERNAL_ERROR");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("translation","translate");
			$i->MoveNext();
		}
		if($currency_aux!=$currency){
			echo $currency_aux."--".$currency;
			$CreatePnr->Pnr[$i]->Failure = "The currency doesn't match with location";
			$CreatePnr->Pnr[$i]->Failure->addAttribute("type","error");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("code","INTERNAL_ERROR");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("translation","translate");
			$i->MoveNext();
		}
		$pais_rq  ="select * from pais where id_pais = ".$paxs[$i][0][0]["countryid"];
		$pais_rs = $db1->Execute($pais_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		
		if($pais_rs->RecordCount()==0){
			$CreatePnr->Pnr[$i]->Failure = "First pax country is mandatory";
			$CreatePnr->Pnr[$i]->Failure->addAttribute("type","error");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("code","INTERNAL_ERROR");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("translation","translate");
			$i->MoveNext();
		}
		$aux = explode("/", $RoomSegmentRequest[$i]);
		$hotelid = $aux[0];
		$roomid = $aux[1];		
		$checkin = $aux[2];
		$checkout = $aux[3];
		$cityid = $aux[4];
		$habs['sgl']=$aux[5];
		$habs['twin']=$aux[6];
		$habs['mat']=$aux[7];
		$habs['tpl']=$aux[8];
		$total=$aux[10];
		//echo $cityid."/".$checkin."/".$checkout."/".$id_agencia."/".$habs."/".$hotelid."/".$roomid."/".$foreign;
		//print_r($habs);
		//die();
		//$cadena="".$hotel."/".$thab."/".CheckinDate/"CheckoutDate"/".$cityid."/".['sgl']."/".['twin']."/".['mat']."/".['tpl']."/".$countryid."/".['totValue'];
		$arr = $cot->matrizDisponibilidadG($db1, $cityid, $checkin, $checkout, $id_agencia, $habs, $hotelid, $roomid, $foreign);
		if(round($arr[$hotelid]['info']['habs'][$roomid]['totValue'], $cot->decimales)!=round($total, $cot->decimales)){
			$CreatePnr->Pnr[$i]->Failure = "There's no stock in the requested room";
			$CreatePnr->Pnr[$i]->Failure->addAttribute("type","error");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("code","INTERNAL_ERROR");
			$CreatePnr->Pnr[$i]->Failure->addAttribute("translation","translate");
			$i->MoveNext();
		}
		$result = $cot->cotCreator($db1, $arr, $habs, $paxs[$i][0],$id_usuario,$cityid,$extras[$i]);
		$now = getdate();
		$CreatePnr = simplexml_load_string("<?xml version='1.0' encoding='UTF-8'?><CreatePnrRS xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Success/>$string</CreatePnrRS>");
		$CreatePnr->Pnr[$i]->RecordLocator=$result['bookingcode'];
		$CreatePnr->Pnr[$i]->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		//$CreatePnr->Pnr[$i]->Status=$result['status'];
		$CreatePnr->Pnr[$i]->SegmentTotals->AirTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->AirTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->RoomTotal = $total;
		$CreatePnr->Pnr[$i]->SegmentTotals->RoomTaxTotal = $total*0.19; /// si ya viene con iva deberia ser $total/1.19*0.19
		$CreatePnr->Pnr[$i]->SegmentTotals->CarTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->CarTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->ActivityTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->ActivityTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->CruiseTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->CruiseTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->InsuranceTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->InsuranceTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->MerchandiseTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->MerchandiseTaxTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->PassThroughMarkupTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->BookingFeeClient = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->BookingFeeTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->ServiceChargeTotal = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->AgentMarkup = 0;
		$CreatePnr->Pnr[$i]->SegmentTotals->DiscountTotal = 0;
		$CreatePnr->Pnr[$i]->DepartureDate = "";
		//$CreatePnr->Pnr[$i]->PnrOwner = $formdata["PnrOwner"];
		//$CreatePnr->Pnr[$i]->LeadTraveler = $formdata["LeadTraveler"];
		$CreatePnr->Pnr[$i]->PnrOwner = null;
		$CreatePnr->Pnr[$i]->LeadTraveler = null;
		$CreatePnr->Pnr[$i]->CustomerPricingSummary->Currency = $currency  ; 
		$CreatePnr->Pnr[$i]->CustomerPricingSummary->PackageTotal =  $total * 1.19 ; 
		//$CreatePnr->Pnr[$i]->CustomerPricingSummary->PaymentTotal = $total  ; 
		//$CreatePnr->Pnr[$i]->CustomerPricingSummary->BalanceDueTotal =  0 ;
		$CreatePnr->Pnr[$i]->RoomSegment->BookingComponent = "Room";
		$CreatePnr->Pnr[$i]->RoomSegment->SegmentId  = substr($extras[$i]["clientcode"], 4, (strlen($extras[$i]["clientcode"])-1));
		$SegmentLocator .= base64_encode($result['bookingcode']."/");
		$sum +=$total;
	}
	/*$CreatePnr->Success = "Success";
	$CreatePnr->Segment->CorrelationId = $extras[$i]['clientcode'];
	$CreatePnr->Segment->SegmentLocator = $SegmentLocator;
	$CreatePnr->Segment->Currency = $currency;
	$CreatePnr->Segment->TotalPrice = $sum;*/
	$sql = 'update test.xmls set fecha_rs =now(), rs_procesado = "'.ereg_replace('"', "'",$CreatePnr->asXML()).'" where id_xml ='.$last_xml;
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $CreatePnr->asXML();
	//$domxml = dom_import_simplexml($CreatePnr);
	//return $domxml->ownerDocument->saveXML($domxml->ownerDocument->documentElement);
}

?>