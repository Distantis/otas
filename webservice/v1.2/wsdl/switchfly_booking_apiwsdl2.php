﻿<?php 

if (stristr($_SERVER['QUERY_STRING'], "wsdl")) {
    	// WSDL request - output raw XML
		header("Content-Type: application/soap+xml; charset=utf-8");
        echo DisplayXML();
    } else {
    	// Page accessed normally - output documentation
    	$cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1); // Current page
    	echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
    	echo '<html>';
    	echo '<head><title>' . $serviceName . '</title></head>';
    	echo '<body>';
    	echo '<h1>' . $serviceName . '</h1>';
        echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $cp . '?WSDL</code></p>';
    	
        // Document each function
        echo '<h2>Available Functionxs:</h2>';
        echo '<div style="margin-left:20px;">';
        for ($i=0;$i<count($functions);$i++) {
            echo '<h3>Function: ' . $functions[$i]['funcName'] . '</h3>';
            echo '<div style="margin-left:20px;">';
            echo '<p>';
            echo $functions[$i]['doc'];
            echo '<ul>';
            if (array_key_exists("inputParams", $functions[$i])) {
            	echo '<li>Input Parameterxs:<ul>';
            	for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
            	   	echo '<li>' . $functions[$i]['inputParams'][$j]['name'];
            	   	echo ' (' . $functions[$i]['inputParams'][$j]['type'];
            	   	echo ')</li>';
            	}
            	echo '</ul></li>';
            }
            if (array_key_exists("outputParams", $functions[$i])) {
                echo '<li>Output Parameterxs:<ul>';
                for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
                    echo '<li>' . $functions[$i]['outputParams'][$j]['name'];
                    echo ' (' . $functions[$i]['outputParams'][$j]['type'];
                    echo ')</li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
            echo '</p>';
            echo '</div>';
        }
        echo '</div>';
        	
    	echo '<h2>WSDL output:</h2>';
        echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
        echo DisplayXML(false);
        echo '</pre>';
        echo '</body></html>';
    }
                         
    exit; 
function DisplayXML($xmlformat=true) {
	

	
	$str =  '<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://www.darkerwhite.com/" targetNamespace="http://www.darkerwhite.com/" name="DistantisServices">
<wsdl:types>
<xs:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">
<xs:element name="AvailabilityRQ">
<xs:complexType>
<xs:sequence>
<xs:element name="CreateSession" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="UserId" type="xs:string"></xs:element>
<xs:element name="Password" type="xs:string"></xs:element>
<xs:element name="Cobrand" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="LanguageId" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="Currency" type="tns:CurrencyCodeType" minOccurs="0"></xs:element>
<xs:element name="Debug" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="CustomerIp" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="ClientMarker" type="xs:string" minOccurs="0"></xs:element>
<xs:element minOccurs="0" name="ClientExtensions">
<xs:complexType>
<xs:sequence>
<xs:any maxOccurs="unbounded" minOccurs="0" processContents="lax"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="StubScenario" type="xs:string" minOccurs="0"></xs:element>
<xs:choice>
<xs:sequence>
<xs:element name="RoomSearch" type="tns:RoomSearchType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:choice>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AvailabilityRS">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRSType">
<xs:sequence>
<xs:element name="RoomSearchResponse" type="tns:RoomSearchResponseType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="CancelPnrRQ">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRQType">
<xs:sequence>
<xs:element name="RecordLocator" type="xs:string"></xs:element>
<xs:element name="IsAutoCancel" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="RefundPayment" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="PushPnr" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="SendConfirmation" type="xs:boolean" minOccurs="0"></xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="CancelPnrRS">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRSType">
<xs:sequence>
<xs:element name="Pnr" type="tns:PnrType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="CreatePnrRQ">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyPaymentRQType">
<xs:sequence>
<xs:element name="AddToRecordLocator" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="PnrOwner" type="tns:TravelerType"></xs:element>
<xs:element name="LeadTraveler" type="tns:TravelerType"></xs:element>
<xs:element name="RoomSegmentRequest" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="CorrelationId" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="RoomSegment" type="xs:string"></xs:element>
<xs:element name="CustomFields" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="CustomField" type="tns:CustomFieldType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RoomingList" type="tns:GuestListType" minOccurs="0"/>
<xs:element name="SpecialRequests" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="RequestedService" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
<xs:element name="AgencyCreditCard" type="tns:CreditCardType" minOccurs="0"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="BookingHoldDays" type="xs:integer" minOccurs="0"/>
<xs:element name="BookingMemo" type="xs:string" minOccurs="0"/>
<xs:element name="ProcessPayment" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="ProcessPaymentCurrency" type="tns:CurrencyCodeType" minOccurs="0"></xs:element>
<xs:element name="PushPnr" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="SendConfirmation" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="ClientReservation" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Number" type="xs:string"></xs:element>
<xs:element name="LoyaltyNumber" type="xs:string" minOccurs="0"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="HoldSupplierConfirmation" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="DiscountCode" type="xs:string" minOccurs="0" maxOccurs="1"></xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="CreatePnrRS">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRSType">
<xs:choice>
<xs:sequence>
<xs:element name="UnavailableBookingComponent" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
<xs:element name="PriceChange" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string"/>
<xs:element name="Currency" type="xs:string"/>
<xs:element name="OriginalPrice" type="xs:double"/>
<xs:element name="NewPrice" type="xs:double"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
<xs:sequence>
<xs:element name="SpecialMarkup" type="xs:double"/>
<xs:element name="TicketingDeadline" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="Segment" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="CorrelationId" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="SegmentLocator" type="xs:string"></xs:element>
<xs:element name="CrsRecordLocator" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="SupplierLocator" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Currency" type="xs:string"/>
<xs:element name="TotalPrice" type="tns:CurrencyAmountType"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PaymentRedirectUrl" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="PaymentRedirectUrlParameters" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Parameter" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Name" type="xs:string"/>
<xs:element name="Value" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PrintUrlForOfflineBankTransfer" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Pnr" type="tns:PnrType"/>
</xs:sequence>
</xs:choice>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="HotelDetailsRQ">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRQType">
<xs:sequence>
<xs:element name="Hotel" type="xs:string"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="HotelDetailsRS">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRSType">
<xs:sequence>
<xs:element name="Hotel" type="tns:HotelType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:complexType name="AbstractActivityType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="Name" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Code" type="xs:string" minOccurs="0"/>
<xs:element name="SupplierName" type="xs:string"></xs:element>
<xs:element name="SupplierCode" type="xs:string" minOccurs="0"/>
<xs:element name="LongDescription" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="SupplierPhone" type="xs:string" minOccurs="0"/>
<xs:element name="ActivityAddress" type="tns:AddressType" minOccurs="0"/>
<xs:element name="IncludedServices" type="xs:string" minOccurs="0"/>
<xs:element name="AdditionalInfoQuestion" type="xs:string" minOccurs="0"/>
<xs:element name="FixedDailyPricing" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="LabelCountsSetFromTotalTravelers" minOccurs="0"></xs:element>
<xs:element name="VoucherInformation" type="xs:string" minOccurs="0"/>
<xs:element name="SortRanking" type="xs:decimal" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivityAnyDateEventType">
<xs:complexContent>
<xs:extension base="tns:ActivityEventType">
<xs:sequence>
<xs:element name="DateRangeBegin" type="tns:IsoDateType"/>
<xs:element name="DateRangeEnd" type="tns:IsoDateType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivityEventType">
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"/>
<xs:element name="AnyTime" type="xs:boolean"></xs:element>
<xs:element name="Available" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="FreeSell" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="OnRequest" type="xs:boolean" minOccurs="0"></xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivityFixedDateEventType">
<xs:complexContent>
<xs:extension base="tns:ActivityEventType">
<xs:sequence>
<xs:element name="Timestamp" type="tns:IsoTimestampType"></xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivityLabelType">
<xs:sequence>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
<xs:element name="BookingCode" type="xs:string" minOccurs="0"/>
<xs:element name="Label" type="xs:string"/>
<xs:element name="Count" type="xs:int"/>
<xs:element name="GuestList" type="tns:GuestListType" minOccurs="0"/>
<xs:element name="FamilyPlan" type="xs:boolean" minOccurs="0"/>
<xs:element name="FamilyPlanMaster" type="xs:boolean" minOccurs="0"/>
<xs:element name="Price" type="tns:PriceType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivityRateType">
<xs:sequence>
<xs:element name="RateName" type="xs:string" minOccurs="0"/>
<xs:element name="RateType" type="tns:ActivityRateApplicabilityType" minOccurs="0"/>
<xs:element name="Label" type="tns:ActivityLabelType" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivitySegmentType">
<xs:complexContent>
<xs:extension base="tns:AbstractActivityType">
<xs:sequence>
<xs:element name="Rate" type="tns:ActivityRateType" minOccurs="0"/>
<xs:choice>
<xs:element name="AnyDateEvent" type="tns:ActivityAnyDateEventType"/>
<xs:element name="FixedDateEvent" type="tns:ActivityFixedDateEventType"/>
</xs:choice>
<xs:element name="Price" type="tns:PriceType"/>
<xs:element name="VoucherUrl" type="xs:string" minOccurs="0"/>
<xs:element name="ActivityCancellationPolicies" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="ActivityCancellationPolicy" type="tns:ActivityCancellationPolicyType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="AccountingAdjustmentType">
<xs:sequence>
<xs:element name="EnteredDate" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="AgentName" type="xs:string" minOccurs="0"/>
<xs:element name="GlCode" type="xs:string" minOccurs="0"/>
<xs:element name="GlMemo" type="xs:string" minOccurs="0"/>
<xs:element name="AdjAmountSystemCurrency" type="xs:double" minOccurs="0"/>
<xs:element name="CustomerCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="AdjAmountCustomerCurrency" type="xs:double" minOccurs="0"/>
<xs:element name="RequiredApproval" type="xs:boolean" minOccurs="0"/>
<xs:element name="ApprovingAgentName" type="xs:string" minOccurs="0"/>
<xs:element name="IsApproved" type="xs:boolean" minOccurs="0"/>
<xs:element name="ApprovalTimestamp" type="tns:IsoTimestampType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AccountsPayableSupplierType">
<xs:sequence>
<xs:element name="SupplierId" type="xs:int" minOccurs="0"/>
<xs:element name="SupplierCode" type="xs:string" minOccurs="0"/>
<xs:element name="MasterSupplierCode" type="xs:string" minOccurs="0"/>
<xs:element name="OriginalAmount" type="xs:double" minOccurs="0"/>
<xs:element name="Owe" type="xs:double" minOccurs="0"/>
<xs:element name="SupplierCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="SupplierOriginalAmount" type="xs:double" minOccurs="0"/>
<xs:element name="SupplierOwe" type="xs:double" minOccurs="0"/>
<xs:element name="AirPnrId" type="xs:int" minOccurs="0"/>
<xs:element name="RoomId" type="xs:int" minOccurs="0"/>
<xs:element name="CarId" type="xs:int" minOccurs="0"/>
<xs:element name="ActivityId" type="xs:int" minOccurs="0"/>
<xs:element name="CruisePnrId" type="xs:int" minOccurs="0"/>
<xs:element name="CruiseSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="InsuranceId" type="xs:int" minOccurs="0"/>
<xs:element name="MerchandiseSegmentId" type="xs:int" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AccountingType">
<xs:sequence>
<xs:element name="GeneralLedger">
<xs:complexType>
<xs:sequence>
<xs:element name="GeneralLedgerDetails" type="tns:GeneralLedgerDetailsType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AccountsPayableSuppliers">
<xs:complexType>
<xs:sequence>
<xs:element name="AccountsPayableSupplier" type="tns:AccountsPayableSupplierType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PaymentSuppliers">
<xs:complexType>
<xs:sequence>
<xs:element name="PaymentSupplier" type="tns:PaymentSupplierType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AccountsPayableAgencies">
<xs:complexType>
<xs:sequence>
<xs:element name="AccountsPayableAgency" type="tns:AccountsPayableAgencyType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PaymentAgencies">
<xs:complexType>
<xs:sequence>
<xs:element name="PaymentAgency" type="tns:PaymentAgencyType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PaymentReceive">
<xs:complexType>
<xs:sequence>
<xs:element name="PaymentReceiveDetails" type="tns:PaymentReceiveDetailsType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AccountingAdjustments">
<xs:complexType>
<xs:sequence>
<xs:element name="AccountingAdjustment" type="tns:AccountingAdjustmentType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AccountsPayableAgencyType">
<xs:sequence>
<xs:element name="AgencyID" type="xs:string" minOccurs="0"/>
<xs:element name="OriginalAmount" type="xs:double" minOccurs="0"/>
<xs:element name="Owe" type="xs:double" minOccurs="0"/>
<xs:element name="AgencyCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="AgencyOriginalAmount" type="xs:double" minOccurs="0"/>
<xs:element name="AgencyOwe" type="xs:double" minOccurs="0"/>
<xs:element name="Valid" type="xs:boolean" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="ActivityRateApplicabilityType">
<xs:restriction base="xs:string">
<xs:enumeration value="activity_only"/>
<xs:enumeration value="addon_only"/>
<xs:enumeration value="activity/addon"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="AgencyType">
<xs:sequence>
<xs:element name="AgencyName" type="xs:string"/>
<xs:element name="AgencyCode" type="xs:string" minOccurs="0"/>
<xs:element name="IATA" type="xs:string" minOccurs="0"/>
<xs:element name="ARC" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AgentType">
<xs:sequence>
<xs:element name="FirstName" type="xs:string"/>
<xs:element name="LastName" type="xs:string"/>
<xs:element name="AgentId" type="xs:int"/>
<xs:element name="Agency" type="tns:AgencyType"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AirItineraryType">
<xs:sequence>
<xs:element name="RecordLocator" type="xs:string" minOccurs="0"/>
<xs:element name="AirPnrId" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="OriginalAirPnrId" type="xs:int" minOccurs="0" maxOccurs="1"></xs:element>
<xs:element name="Price" type="tns:PriceType"/>
<xs:element name="AgeClassPrices" type="tns:AgeClassPricesType"/>
<xs:element name="Traveler" type="tns:TravelerType" maxOccurs="unbounded"/>
<xs:element name="AirLeg" type="tns:AirLegType" maxOccurs="unbounded"/>
<xs:element name="TicketingDeadline" type="tns:IsoDateType" maxOccurs="1" minOccurs="0"/>
<xs:element name="SingleUseCard" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AirLegType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="AirLegId" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="Published" type="xs:boolean"></xs:element>
<xs:element name="Cabin" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="AirAllianceName" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"></xs:element>
<xs:sequence maxOccurs="unbounded">
<xs:element name="AirSegment" type="tns:AirSegmentType"/>
<xs:element name="LayoverTime" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Hours" type="xs:int"/>
<xs:element name="Minutes" type="xs:int"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="AirSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="AirSegmentType" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="AIR"/>
<xs:enumeration value="TRAIN"/>
<xs:enumeration value="BUS"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="DepartureTimestamp" type="tns:IsoTimestampType"></xs:element>
<xs:element name="ArrivalTimestamp" type="tns:IsoTimestampType"></xs:element>
<xs:element name="Duration" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Hours" type="xs:int"/>
<xs:element name="Minutes" type="xs:int"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="FromCity" type="xs:string"></xs:element>
<xs:element name="ToCity" type="xs:string"></xs:element>
<xs:element name="Airline" type="xs:string"></xs:element>
<xs:element name="CodeShareAirline" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Flight" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="FareClass" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Cabin" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Plane" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Stops" type="xs:int"></xs:element>
<xs:element name="Tickets" minOccurs="0" maxOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="Ticket" minOccurs="1" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Number" type="xs:string" maxOccurs="1" minOccurs="1"/>
<xs:element name="Traveler" maxOccurs="1" minOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="Title" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="FirstName" type="xs:string" maxOccurs="1" minOccurs="1"/>
<xs:element name="MiddleName" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="LastName" type="xs:string" maxOccurs="1" minOccurs="1"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="BookingFeeSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="ClientFeeType" type="xs:string"></xs:element>
<xs:element name="ContractName" type="xs:string"/>
<xs:element name="Price" type="tns:PriceType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="DiscountSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="DiscountType" type="xs:string"></xs:element>
<xs:element name="ContractName" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="DiscountCode" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="RecordLocator" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="ConfirmationCode" type="xs:string" maxOccurs="1" minOccurs="0"/>
<xs:element name="Price" type="tns:PriceType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="MerchandiseSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="MerchandiseItemName" type="xs:string"/>
<xs:element name="SupplierName" type="xs:string"/>
<xs:element name="SupplierId" type="xs:int"/>
<xs:element name="SupplierCode" type="xs:string" minOccurs="0"/>
<xs:element name="SKU" type="xs:string"/>
<xs:element name="MerchandiseItemQuantity" type="xs:int"/>
<xs:element name="Price" type="tns:PriceType"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ShippingDetailsType">
<xs:sequence>
<xs:element name="Title" type="xs:string" minOccurs="0"/>
<xs:element name="LastName" type="xs:string"/>
<xs:element name="FirstName" type="xs:string"/>
<xs:element name="Phone" type="xs:string"/>
<xs:element name="GiftNote" type="xs:string" minOccurs="0"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
<xs:element name="ShippingAdditionalInformation" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="BookingMemoType">
<xs:sequence>
<xs:element name="Timestamp" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="Agent" type="xs:string" minOccurs="0"/>
<xs:element name="MemoCode" type="xs:string" minOccurs="0"/>
<xs:element name="Memo" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="CarLocationType">
<xs:sequence>
<xs:element name="AirportCode" type="xs:string" minOccurs="0"/>
<xs:element name="CompanyCode" type="xs:string" minOccurs="0"/>
<xs:element name="LocationName" type="xs:string" minOccurs="0"/>
<xs:element name="PhoneNumber" type="xs:string" minOccurs="0"/>
<xs:element name="VicinityCode" type="xs:string" minOccurs="0"/>
<xs:element name="LocationNumber" type="xs:int" minOccurs="0"/>
<xs:element name="AtAirport" type="xs:boolean"/>
<xs:element name="InTerminal" type="xs:boolean"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="CarSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"/>
<xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="PickupCity" type="xs:string" minOccurs="0"/>
<xs:element name="DropOffCity" type="xs:string" minOccurs="0"/>
<xs:element name="PickupTimestamp" type="tns:IsoTimestampType"/>
<xs:element name="DropOffTimestamp" type="tns:IsoTimestampType"/>
<xs:element name="ChainCode" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="CompanyName" type="xs:string"/>
<xs:element name="Approved" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="CarCode">
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:string">
<xs:attribute name="Class" type="xs:string"></xs:attribute>
<xs:attribute name="Type" type="xs:string"></xs:attribute>
<xs:attribute name="Transmission" type="xs:string"></xs:attribute>
<xs:attribute name="Drive" type="xs:string"></xs:attribute>
<xs:attribute name="AirConditioning" type="xs:string"></xs:attribute>
<xs:attribute name="Fuel" type="xs:string"></xs:attribute>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
<xs:element name="CarName" type="xs:string" minOccurs="0"/>
<xs:element name="Model" type="xs:string" minOccurs="0"/>
<xs:element name="CarDescription" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="IncludedServices" type="xs:string" minOccurs="0"/>
<xs:element name="RateCode" type="xs:string" minOccurs="0"/>
<xs:element name="RateDescription" type="xs:string" minOccurs="0"/>
<xs:element name="PassengerCapacity" type="xs:int" minOccurs="0"/>
<xs:element name="BaggageCapacity" type="xs:int" minOccurs="0"/>
<xs:element name="LogoUrl" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="PictureUrl" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="HasUnlimitedMiles" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="FreeMilesPerDay" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="DistanceUnit" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="MILES"/>
<xs:enumeration value="KILOMETERS"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="Price" type="tns:PriceType"/>
<xs:element name="PickUpLocation" type="tns:CarLocationType"/>
<xs:element name="DropOffLocation" type="tns:CarLocationType"/>
<xs:element name="SpecialRequests" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="CertificateType">
<xs:sequence>
<xs:element name="CertificateId" type="xs:int" minOccurs="0"/>
<xs:element name="Amount" type="xs:double" minOccurs="0"/>
<xs:element name="Tracking" type="xs:string" minOccurs="0"/>
<xs:element name="Beneficiary" type="xs:string" minOccurs="0"/>
<xs:element name="BeginDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="EndDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="Void" type="xs:boolean" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ClientSearchLocationTypeV2">
<xs:sequence>
<xs:element name="ExplicitSearchString" type="xs:string"></xs:element>
<xs:choice>
<xs:element name="Airport">
<xs:complexType>
<xs:sequence>
<xs:element name="AirportCode" type="xs:string"/>
<xs:element name="AirportName" type="xs:string"/>
<xs:element name="StateCode" type="xs:string" minOccurs="0"/>
<xs:element name="StateName" type="xs:string" minOccurs="0"/>
<xs:element name="CountryCode" type="xs:string"/>
<xs:element name="CountryName" type="xs:string"/>
<xs:element name="Excluded" type="tns:ClientSearchLocationExcludedType" minOccurs="0"/>
<xs:element name="AirportCities">
<xs:complexType>
<xs:sequence>
<xs:element name="PrimaryCityId" type="xs:int"/>
<xs:element name="AlternateCityId" type="xs:int" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="City">
<xs:complexType>
<xs:sequence>
<xs:element name="CityId" type="xs:int"/>
<xs:element name="CityName" type="xs:string"/>
<xs:element name="StateCode" type="xs:string" minOccurs="0"/>
<xs:element name="StateName" type="xs:string" minOccurs="0"/>
<xs:element name="CountryCode" type="xs:string"/>
<xs:element name="CountryName" type="xs:string"/>
<xs:element name="Excluded" type="tns:ClientSearchLocationExcludedType" minOccurs="0"/>
<xs:element name="CityAirports">
<xs:complexType>
<xs:sequence>
<xs:element name="AirportCode" type="xs:string" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Region">
<xs:complexType>
<xs:sequence>
<xs:element name="RegionId" type="xs:string"/>
<xs:element name="RegionName" type="xs:string"/>
<xs:element name="Excluded" type="tns:ClientSearchLocationExcludedType" minOccurs="0"/>
<xs:element name="RegionCities">
<xs:complexType>
<xs:sequence>
<xs:element name="CityId" type="xs:int" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="LocationType">
<xs:restriction base="xs:string">
<xs:enumeration value="AIRPORT"/>
<xs:enumeration value="CITY"/>
<xs:enumeration value="REGION"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="ClientSearchLocationExcludedType">
<xs:sequence>
<xs:element name="Air" type="tns:ClientSearchLocationExclusionType" minOccurs="1"/>
<xs:element name="Room" type="tns:ClientSearchLocationExclusionType" minOccurs="1"/>
<xs:element name="Car" type="tns:ClientSearchLocationExclusionType" minOccurs="1"/>
<xs:element name="Activity" type="tns:ClientSearchLocationExclusionType" minOccurs="1"/>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="ClientSearchLocationExclusionType">
<xs:restriction base="xs:string">
<xs:enumeration value="NONE"/>
<xs:enumeration value="ORIGIN"/>
<xs:enumeration value="DESTINATION"/>
<xs:enumeration value="BOTH"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="CruiseSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"/>
<xs:element name="Departure" type="tns:DepartureType" minOccurs="0"/>
<xs:element name="Arrival" type="tns:DepartureType" minOccurs="0"/>
<xs:element name="CruiselineCode" type="xs:string"/>
<xs:element name="CruiseName" type="xs:string" minOccurs="0"/>
<xs:element name="ShipName" type="xs:string"/>
<xs:element name="ShipId" type="xs:string"/>
<xs:element name="CruiseDuration" type="xs:string" minOccurs="0"/>
<xs:element name="CabinNumber" type="xs:string" minOccurs="0"/>
<xs:element name="CabinCategoryLocation" type="xs:string" minOccurs="0"/>
<xs:element name="DinningOption" type="xs:string" minOccurs="0"/>
<xs:element name="CruiseFareCode" type="xs:string" minOccurs="0"/>
<xs:element name="Price" type="tns:PriceType" minOccurs="0"/>
<xs:element name="Travelers" type="tns:GuestListType" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ConnectorCustomInfoType">
<xs:sequence>
<xs:element name="Key" type="xs:string"/>
<xs:element name="Value" type="xs:string"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="DepartureType">
<xs:sequence>
<xs:element name="CityName" type="xs:string"/>
<xs:element name="CountryCode" type="xs:string"/>
<xs:element name="CountryName" type="xs:string"/>
<xs:element name="Timestamp" type="xs:string"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="GuestListType">
<xs:sequence>
<xs:element name="Guest" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="FirstName" type="xs:string" minOccurs="0"/>
<xs:element name="LastName" type="xs:string" minOccurs="0"/>
<xs:element name="Title" type="xs:string" minOccurs="0"/>
<xs:choice minOccurs="0">
<xs:element name="AgeInMonths" type="xs:int"/>
<xs:element name="Age" type="xs:int"/>
</xs:choice>
<xs:element name="AgeClass" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="ADULT"/>
<xs:enumeration value="CHILD"/>
<xs:enumeration value="SENIOR"/>
<xs:enumeration value="INFANT_REQUIRING_SEAT"/>
<xs:enumeration value="INFANT_IN_LAP"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="OfficialTravelerId" type="xs:string" minOccurs="0"/>
<xs:element name="OfficialTravelerIdLabel" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="OfficialTravelerIdCountry" type="xs:string" minOccurs="0"/>
<xs:element name="DateOfBirth" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="AirLoyaltyPrograms" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="LoyaltyProgram" minOccurs="0" type="tns:LoyaltyProgramType" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="GovernmentFinancialId" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="GeneralLedgerDetailsType">
<xs:sequence>
<xs:element name="GeneralLedgerId" type="xs:int"/>
<xs:element name="AccountId" type="xs:int"/>
<xs:element name="AgentId" type="xs:int"/>
<xs:element name="SupplierId" type="xs:int"/>
<xs:element name="SupplierCode" type="xs:string" minOccurs="0"/>
<xs:element name="Credit" type="xs:boolean"/>
<xs:element name="Debit" type="xs:boolean"/>
<xs:element name="Memo" type="xs:string" minOccurs="0"/>
<xs:element name="DateEntered" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="ActualAmount" type="xs:double"/>
<xs:element name="ActualCurrency" type="tns:CurrencyCodeType"/>
<xs:element name="AmountSystemCurrency" type="xs:double"/>
<xs:element name="CrsName" type="xs:string" minOccurs="0"/>
<xs:element name="Valid" type="xs:boolean"/>
<xs:element name="AirSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="RoomSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="CarSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="ActivitySegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="CruiseSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="InsuranceSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="ClientBookingFeeSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="DiscountSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="MerchandiseSegmentId" type="xs:int" minOccurs="0"/>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
<xs:element name="ClientGlCode" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="HistoryType">
<xs:sequence>
<xs:element name="Pnrs">
<xs:complexType>
<xs:sequence>
<xs:element name="Pnr" type="tns:PnrType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="HotelLandingPromotionType">
<xs:sequence>
<xs:element name="Crs" type="xs:string"/>
<xs:element name="HotelCode" type="xs:string"/>
<xs:element name="HotelName" type="xs:string"/>
<xs:element name="AirportCode" type="xs:string"/>
<xs:element name="CobrandName" type="xs:string" minOccurs="0"/>
<xs:element name="PromotionName" type="xs:string" minOccurs="0"/>
<xs:element name="PromotionType" type="xs:string"/>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
<xs:element name="StayRequirements" type="xs:string" minOccurs="0"/>
<xs:element name="BookDateRequirements" type="xs:string" minOccurs="0"/>
<xs:element name="TravelDateRequirements" type="xs:string" minOccurs="0"/>
<xs:element name="Inclusions" type="xs:string" minOccurs="0"/>
<xs:element name="BookingTips" type="xs:string" minOccurs="0"/>
<xs:element name="MetaData" type="xs:string" minOccurs="0"/>
<xs:element name="PromotionStartDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="PromotionEndDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="CheckinDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="CheckoutDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="ImageUrl" type="xs:string" minOccurs="0"/>
<xs:element name="MoreDetails" type="xs:string" minOccurs="0"/>
<xs:element name="TermsAndConditions" type="xs:string" minOccurs="0"/>
<xs:element name="PayNights" type="xs:int" minOccurs="0"/>
<xs:element name="StayNights" type="xs:int" minOccurs="0"/>
<xs:element name="AmountOff" type="xs:int" minOccurs="0"/>
<xs:element name="Currency" type="xs:string" minOccurs="0"/>
<xs:element name="PercentOff" type="xs:int" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="InsuranceSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="ContractName" type="xs:string" minOccurs="0"/>
<xs:element name="Price" type="tns:PriceType"/>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"/>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="MarketingDealType">
<xs:sequence>
<xs:element name="HotelName" type="xs:string"/>
<xs:element name="Currency" type="xs:string"/>
<xs:element name="StandaloneAvgNightlyPrice" type="xs:double" minOccurs="0"/>
<xs:element name="PackagePrice" type="xs:double" minOccurs="0"/>
<xs:element name="StarRating" type="xs:double"/>
<xs:element name="Crs" type="xs:string"/>
<xs:element name="HotelCode" type="xs:string"/>
<xs:element name="CheckinDate" type="tns:IsoDateType"/>
<xs:element name="CheckoutDate" type="tns:IsoDateType"/>
<xs:element name="AirportCode" type="xs:string"/>
<xs:element name="DepartureAirportCode" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ParcelPaymentSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="Currency" type="xs:string"></xs:element>
<xs:element name="PackagePrice" type="xs:double"></xs:element>
<xs:element name="ParcelCount" type="xs:int"></xs:element>
<xs:element name="InterestPercent" type="xs:double"></xs:element>
<xs:element name="InterestAmount" type="xs:double"></xs:element>
<xs:element name="FirstParcelAmount" type="xs:double"></xs:element>
<xs:element name="ParcelAmount" type="xs:double"></xs:element>
<xs:element name="TransactionTime" type="xs:string"></xs:element>
<xs:element name="CC4digits" type="xs:string"></xs:element>
<xs:element name="AuthNumber" type="xs:string"></xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="PaymentCorrectionType">
<xs:sequence>
<xs:element name="DateCorrected" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="CorrectedBefore" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="CorrectedAfter" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="AgentName" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PaymentSupplierType">
<xs:sequence>
<xs:element name="Amount" type="xs:double" minOccurs="0"/>
<xs:element name="InvoiceNumber" type="xs:string" minOccurs="0"/>
<xs:element name="AdjustAmount" type="xs:double" minOccurs="0"/>
<xs:element name="GlCode" type="xs:string" minOccurs="0"/>
<xs:element name="GlMemo" type="xs:string" minOccurs="0"/>
<xs:element name="SupplierCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="SupplierAmount" type="xs:double" minOccurs="0"/>
<xs:element name="SupplierAdjustAmount" type="xs:double" minOccurs="0"/>
<xs:element name="SupplierName" type="xs:string" minOccurs="0"/>
<xs:element name="CheckNumber" type="xs:int" minOccurs="0"/>
<xs:element name="Memo" type="xs:string" minOccurs="0"/>
<xs:element name="PaymentMethod" type="xs:string" minOccurs="0"/>
<xs:element name="Payee" type="xs:string" minOccurs="0"/>
<xs:element name="EnteredDate" type="tns:IsoTimestampType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PaymentAgencyType">
<xs:sequence>
<xs:element name="Amount" type="xs:double" minOccurs="0"/>
<xs:element name="AgencyCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="AgencyAmount" type="xs:double" minOccurs="0"/>
<xs:element name="SupplierName" type="xs:string" minOccurs="0"/>
<xs:element name="CheckNumber" type="xs:int" minOccurs="0"/>
<xs:element name="Payee" type="xs:string" minOccurs="0"/>
<xs:element name="AcountingId" type="xs:int" minOccurs="0"/>
<xs:element name="Memo" type="xs:string" minOccurs="0"/>
<xs:element name="PaymentMethod" type="xs:string" minOccurs="0"/>
<xs:element name="InvoiceNumber" type="xs:string" minOccurs="0"/>
<xs:element name="EnteredDate" type="tns:IsoTimestampType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PaymentReceiveDetailsType">
<xs:sequence>
<xs:element name="EnteredDate" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="Payor" type="xs:string" minOccurs="0"/>
<xs:element name="GovernmentId" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="SendGovernmentId" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
<xs:element name="Phone" type="xs:string" minOccurs="0"/>
<xs:element name="Fax" type="xs:string" minOccurs="0"/>
<xs:element name="Email" type="xs:string" minOccurs="0"/>
<xs:element name="Agency" type="xs:string" minOccurs="0"/>
<xs:element name="PaymentMethod" type="xs:string" minOccurs="0"/>
<xs:element name="CustomerCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="CustomerAmountReceivedSystemCurrency" type="xs:double" minOccurs="0"/>
<xs:element name="CustomerAmountReceivedCustomerCurrency" type="xs:double" minOccurs="0"/>
<xs:element name="Memo" type="xs:string" minOccurs="0"/>
<xs:element name="AgentName" type="xs:string" minOccurs="0"/>
<xs:element name="CreditCardAuthorizationCode" type="xs:string" minOccurs="0"/>
<xs:element name="CertificateId" type="xs:string" minOccurs="0"/>
<xs:element name="CreditCardTransId" type="xs:string" minOccurs="0"/>
<xs:element name="GlCode" type="xs:string" minOccurs="0"/>
<xs:element name="GlMemo" type="xs:string" minOccurs="0"/>
<xs:element name="MiscPaymentAdditionalData" type="tns:MiscPaymentAdditionalDataType" minOccurs="0"/>
<xs:element name="PaymentTotalPoints" type="xs:int" minOccurs="0"/>
<xs:element name="PaymentCorrections" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="PaymentCorrection" type="tns:PaymentCorrectionType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PaymentType">
<xs:sequence>
<xs:element name="PendingPaymentStatus" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="PENDING"/>
<xs:enumeration value="FAILURE"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="Timestamp" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="Method" type="xs:string" minOccurs="0"/>
<xs:element name="BankCode" type="xs:string" minOccurs="0"/>
<xs:element name="CreditCardNumber" type="xs:string" minOccurs="0"/>
<xs:element name="LastCCDigits" type="xs:string" minOccurs="0"/>
<xs:element name="AuthNumber" type="xs:string" minOccurs="0"/>
<xs:element name="TransactionID" type="xs:string" minOccurs="0"/>
<xs:element name="AcquirerId" type="xs:string" minOccurs="0"/>
<xs:element name="AuxiliaryNumber" type="xs:string" minOccurs="0"/>
<xs:element name="CustomerCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="CustomerAmount" type="xs:double" minOccurs="0"/>
<xs:element name="SystemCurrency" type="tns:CurrencyCodeType" minOccurs="0"/>
<xs:element name="SystemAmount" type="xs:double" minOccurs="0"/>
<xs:element name="AddressVerificationStatus" type="xs:string" minOccurs="0"/>
<xs:element name="SecurityCodeVerificationStatus" type="xs:string" minOccurs="0"/>
<xs:element name="GlMemo" type="xs:string" minOccurs="0"/>
<xs:element name="MiscPaymentAdditionalData" type="tns:MiscPaymentAdditionalDataType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="MiscPaymentAdditionalDataType">
<xs:sequence>
<xs:element name="MiscPaymentType" type="xs:string" minOccurs="0"/>
<xs:element name="GovernmentFinancialId" type="xs:string" minOccurs="0"/>
<xs:element name="DebitCardBrand" type="xs:string" minOccurs="0"/>
<xs:element name="LastFourDigits" type="xs:string" minOccurs="0"/>
<xs:element name="NumberOfInstallment" type="xs:string" minOccurs="0"/>
<xs:element name="AuthorizationNumber" type="xs:string" minOccurs="0"/>
<xs:element name="AuthorizationNus" type="xs:string" minOccurs="0"/>
<xs:element name="PayerInformation" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="FirstName" type="xs:string" minOccurs="0"/>
<xs:element name="LastName" type="xs:string" minOccurs="0"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
<xs:element name="Email" type="xs:string" minOccurs="0"/>
<xs:element name="PhoneNumber" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Bank" type="xs:string" minOccurs="0"/>
<xs:element name="AgencyBankCode" type="xs:string" minOccurs="0"/>
<xs:element name="CheckNumber" type="xs:string" minOccurs="0"/>
<xs:element name="Account" type="xs:string" minOccurs="0"/>
<xs:element name="BarCode" type="xs:string" minOccurs="0"/>
<xs:element name="ExpirationTime" type="xs:string" minOccurs="0"/>
<xs:element name="RegistrationNumber" type="xs:string" minOccurs="0"/>
<xs:element name="LoyaltyElectronicSign" type="xs:string" minOccurs="0"/>
<xs:element name="LoyaltyMembershipNumber" type="xs:string" minOccurs="0"/>
<xs:element name="LoyaltyPoints" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PnrType">
<xs:sequence>
<xs:element name="RecordLocator" type="xs:string"/>
<xs:element name="ClientTracking" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="ClientSubTrack" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="Cobrand" type="xs:string" minOccurs="0"/>
<xs:element name="LanguageId" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="BookingTimestamp" type="tns:IsoTimestampType"/>
<xs:element name="AutoCancelDate" type="tns:IsoDateType" minOccurs="0" maxOccurs="1"/>
<xs:element name="Status" type="xs:string"/>
<xs:element name="MastercardVoucher" type="xs:string" minOccurs="0" maxOccurs="1"/>
<xs:element name="FraudScreen" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Service" type="xs:string"/>
<xs:element name="Status" type="tns:FraudScreenStatusType"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ScheduleChangeStatus" type="xs:string" minOccurs="0"/>
<xs:element name="ClientReservation" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Number" type="xs:string"></xs:element>
<xs:element name="LoyaltyNumber" type="xs:string" minOccurs="0"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="SegmentTotals">
<xs:complexType>
<xs:sequence>
<xs:element name="AirTotal" type="xs:double"/>
<xs:element name="AirTaxTotal" type="xs:double"/>
<xs:element name="RoomTotal" type="xs:double"/>
<xs:element name="RoomTaxTotal" type="xs:double"/>
<xs:element name="CarTotal" type="xs:double"/>
<xs:element name="CarTaxTotal" type="xs:double"/>
<xs:element name="ActivityTotal" type="xs:double"/>
<xs:element name="ActivityTaxTotal" type="xs:double"/>
<xs:element name="CruiseTotal" type="xs:double"/>
<xs:element name="CruiseTaxTotal" type="xs:double"/>
<xs:element name="InsuranceTotal" type="xs:double"/>
<xs:element name="InsuranceTaxTotal" type="xs:double"/>
<xs:element name="MerchandiseTotal" type="xs:double"/>
<xs:element name="MerchandiseTaxTotal" type="xs:double"/>
<xs:element name="PassThroughMarkupTotal" type="xs:double"/>
<xs:element name="BookingFeeClient" type="xs:double"/>
<xs:element name="BookingFeeTotal" type="xs:double"/>
<xs:element name="ServiceChargeTotal" type="xs:double"/>
<xs:element name="AgentMarkup" type="xs:double"/>
<xs:element name="AgentMarkupCommission" type="xs:double"/>
<xs:element name="DiscountTotal" type="xs:double"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="CancelDate" type="tns:IsoTimestampType" minOccurs="0"/>
<xs:element name="DepartureDate" type="tns:IsoTimestampType"/>
<xs:element name="DepartureAirPortCode" type="xs:string" minOccurs="0"/>
<xs:element name="DestinationAirPortCode" type="xs:string" minOccurs="0"/>
<xs:element name="Agent" type="tns:AgentType" minOccurs="0"/>
<xs:element name="CallinAgent" type="tns:AgentType" minOccurs="0"/>
<xs:element name="PnrOwner" type="tns:TravelerType"/>
<xs:element name="LeadTraveler" type="tns:TravelerType"/>
<xs:element name="Subscribed" type="xs:boolean" minOccurs="0"/>
<xs:element name="PassiveCrs" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="BookingMemos" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="BookingMemo" type="tns:BookingMemoType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Certificates" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Certificate" type="tns:CertificateType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AirItinerary" type="tns:AirItineraryType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="RoomSegment" type="tns:RoomSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="CarSegment" type="tns:CarSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="ActivitySegment" type="tns:ActivitySegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="CruiseSegment" type="tns:CruiseSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="ParcelPaymentSegment" type="tns:ParcelPaymentSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="InsuranceSegment" type="tns:InsuranceSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="BookingFeeSegment" type="tns:BookingFeeSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="DiscountSegment" type="tns:DiscountSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="MerchandiseSegment" type="tns:MerchandiseSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="ShippingDetails" type="tns:ShippingDetailsType" minOccurs="0" maxOccurs="1"/>
<xs:element name="SystemPricingSummary" minOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="Currency" type="tns:CurrencyCodeType"/>
<xs:element name="PackageTotal" type="xs:double"/>
<xs:element name="CommissionTotal" type="xs:double"/>
<xs:element name="CommissionPaid" type="xs:double"/>
<xs:element name="PaymentTotal" type="xs:double"/>
<xs:element name="PaymentTotalPoints" type="xs:int"/>
<xs:element name="BalanceDueTotal" type="xs:double"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="CustomerPricingSummary" minOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="Currency" type="tns:CurrencyCodeType"/>
<xs:element name="PackageTotal" type="xs:double"/>
<xs:element name="PaymentTotal" type="xs:double"/>
<xs:element name="BalanceDueTotal" type="xs:double"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Payments" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Payment" type="tns:PaymentType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Redemption" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this booking specified a redemption contract, this node defines the details of the redemption.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="ConfirmationNumber" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the confirmation of the points redemption system that was used to make this payment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RefundConfirmationNumber" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>This is the Refund Confirmation Number</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierLocator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the supplier locator if the redemption system used a third party supplier.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ContractName" type="xs:string">
<xs:annotation>
<xs:documentation>
The name of the Switchfly redemption contract that was used to calculate the points values.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExternalProfileId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the id of the user in the points redemption system whose points were used for this payment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExternalProfileFirstName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The first name of the user as known in the points redemption system.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExternalProfileLastName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The last name of the user as known in the points redemption system.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TotalPointsEarned" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The total points earned by the booking. This may be less than the sum of the points earned by each traveler if some of the travelers are not loyalty members.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HouseholdAccountIndicator" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
Indicates whether the loyalty member is part of a household account or not.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Campaign" minOccurs="0">
<xs:annotation>
<xs:documentation>
Campaign applied if the member matched an entry in the client supplied audience file.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="Code" type="xs:string">
<xs:annotation>
<xs:documentation>Campaign code</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Cell" type="xs:string">
<xs:annotation>
<xs:documentation>Campaign cell code.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelPercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CarPercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AirPercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BookingFeePercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ActivityPercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InsurancePercentage" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The percent adjustment applied.</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RedemptionDetails" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Points in a payment can be allocated among various components in the booking. This section is repeated for each component to which points were applied.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="Product" type="tns:ProductType">
<xs:annotation>
<xs:documentation>
The type of product to which the points were allocations (air, room, car, activity, insurance, cruise, booking_fee)
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SegmentId" type="xs:int">
<xs:annotation>
<xs:documentation>
The id of the segment or itinerary to which this point allocation applies. The products themselves are described in a different branch of the XML tree and this id ties this point allocation to the specific product.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChildSegmentId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
For example, if SegmentId was an air pnr then this would be the air leg.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RuleName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The rule name that was used for points calculation. If no rule matched, this will be omitted.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Points" type="xs:int">
<xs:annotation>
<xs:documentation>The number of points allocated.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Currency" type="xs:string">
<xs:annotation>
<xs:documentation>
The currency in which the cash equivalent of points is expressed.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Cash" type="xs:double">
<xs:annotation>
<xs:documentation>
The amount of cash that was paid as part of this redemption.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AwardDesignator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The award designator assigned to the selected tier in the rule.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="LoyaltyEarnTravelers" minOccurs="0">
<xs:annotation>
<xs:documentation>
All eligible travelers that have earned points on this booking
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="LoyaltyEarnTraveler" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="FirstName" type="xs:string">
<xs:annotation>
<xs:documentation>Traveler first name</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MiddleName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Traveler middle name</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LastName" type="xs:string">
<xs:annotation>
<xs:documentation>Traveler last name</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LoyaltyMemberId" type="xs:string">
<xs:annotation>
<xs:documentation>Unique member ID</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PointsEarned" type="xs:int">
<xs:annotation>
<xs:documentation>Points that the traveler earned on this booking</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="IsPosted" type="xs:boolean">
<xs:annotation>
<xs:documentation>
Earned points have been posted to the external points service
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PostedTimestamp" type="tns:IsoTimestampType" minOccurs="0">
<xs:annotation>
<xs:documentation>Date that the earned points were posted</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ExternalDebitAccount" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this booking used an external debit account, this node defines the details of the redemption or refund.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="ExternalDebit" minOccurs="1" maxOccurs="2">
<xs:complexType>
<xs:sequence>
<xs:element name="Timestamp" type="tns:IsoTimestampType" minOccurs="1"/>
<xs:element name="RedeemMemberIdentificationNumber" type="xs:string" minOccurs="1"/>
<xs:element name="Points" type="xs:double" minOccurs="1"/>
<xs:element name="CustomerCurrency" type="tns:CurrencyCodeType" minOccurs="1"/>
<xs:element name="CustomerAmount" type="xs:double" minOccurs="1"/>
<xs:element name="SystemCurrency" type="tns:CurrencyCodeType" minOccurs="1"/>
<xs:element name="SystemAmount" type="xs:double" minOccurs="1"/>
<xs:element name="Cobrand" type="xs:string" minOccurs="1"/>
<xs:element name="ExchangeRate" type="xs:double" minOccurs="1"/>
<xs:element name="ConfirmationCode" type="xs:string" minOccurs="1"/>
<xs:element name="Refund" type="xs:boolean" minOccurs="1"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="History" type="tns:HistoryType" minOccurs="0"/>
<xs:element name="Accounting" type="tns:AccountingType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PnrElementType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="CorrelationId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If present, the correlation id is an echo of a correlation id passed in the request.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AgeClassPricesType">
<xs:annotation>
<xs:documentation>Prices being splitted by AgeClass.</xs:documentation>
</xs:annotation>
<xs:sequence>
<xs:element name="Price" minOccurs="1" maxOccurs="unbounded">
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:PriceType">
<xs:sequence>
<xs:element name="AgeClass" type="tns:AgeClassType">
<xs:annotation>
<xs:documentation>
Specifies that current price explains fares and taxes amounts for a given age range. We are not taking the values of that range. Instead we are populating the names of that ranges e.g. CHILD, ADULT, INFANT, etc.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Count" type="xs:integer">
<xs:annotation>
<xs:documentation>
The number of people for an AgeClass. This field must be present when AgeClass has been populated.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PriceType">
<xs:sequence>
<xs:element name="Total" type="xs:double">
<xs:annotation>
<xs:documentation>
The total price, including taxes. This amount includes taxes if they were available from the supplier of the product. I.e., if the CRS to which Switchfly connected included taxes in its response, then Switchfly passes those on. There can be the following conditions 1. taxes are known and broken out. TaxTotal and TaxDetails tags will be included. 2. Taxes are included in the price, the tax portion of the amount is known, but information about the kinds of taxes in not available. TaxTotal will be non-zero, but TaxDetails will be omitted. 3. The price includes taxes, but the distribution of the price between taxes and net is not known. TaxDetails will be zero. 4. Taxes are not included in the price. The tag IncludesTaxesAndExtraCharges is false.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TaxTotal" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The amount of the total that is taxes</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierCurrency" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The currency of the supplier</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TaxDetails" type="tns:TaxDetailsType" minOccurs="0"/>
<xs:element name="Commission" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>The amount of commission calculated.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CrsCommission" type="xs:double" minOccurs="0"/>
<xs:element name="CostOfGoods" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
The cost of goods. I.e, the cost of this product before markup. This is only sent for tightly coupled systems and its inclusion is controlled by an Switchfly configuration.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="IncludesTaxesAndExtraCharges" type="xs:boolean">
<xs:annotation>
<xs:documentation>
A boolean that indicates that the total includes all fees. If false, there may be some unknown taxes or other charges not included in the total. This is typical in car rentals where local taxes are often not included in the quote.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Currency" type="xs:string">
<xs:annotation>
<xs:documentation>
Three letter currency code. Prices are quoted relative to the system providing the quote, so the currency will be the system currency of the system generating this response.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PassThrough" type="xs:boolean">
<xs:annotation>
<xs:documentation>
This is true if the payment for this product will be "passed through" to the provider. For example, when selling a published hotel room, the client credit card is collected but not charged. Rather, it is passed to the hotel, which charges the customer on checkout and later sends a commission check. For "post-pay" products such as these, there is no way to collect payment up front.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierSoldAtRounding" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
The amount that the price was rounded to get to the nearest required decimal point. This is in supplier currency.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ClientAccountCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The accounting code for either a segment by CRS in the case of activity/room/car, or the account code by fee type (Booking, modification, etc...) and CRS in the case of client booking fees.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RedemptionPrice" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="ContractName" type="xs:string">
<xs:annotation>
<xs:documentation>
The name of the contract used to calculate the points.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RuleName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The name of the rule within the specific contract that was activated, if any.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MaxPoints" type="xs:int">
<xs:annotation>
<xs:documentation>
The number of points calculated that can be used in purchasing the product. Note that this is not necessarily the number of points that were redeemed, which is recorded in the redemptions section of a PNR. These points might not cover the full price of the product if, for example, points cannot be used towards taxes.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Currency" type="xs:string">
<xs:annotation>
<xs:documentation>
The currency in which the MaxPointsValue is expressed.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MaxPointsValue" type="xs:double">
<xs:annotation>
<xs:documentation>This is the cash value of the TotalPoints.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AwardDesignator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The award designator assigned to the selected tier in the rule.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LoyaltyPromotionContractName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The promotion contract name applied to the selected segment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LoyaltyPromotionPoints" type="xs:integer" minOccurs="0">
<xs:annotation>
<xs:documentation>
The promotion contract points applied to the selected segment.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="SystemCurrencyPrice" minOccurs="0">
<xs:annotation>
<xs:documentation>
This tag is present only when this price is part of a PushPnrRQ, ReadPnrRS or CreatPnrRS. It is used to present the price in the system currency in addition to the default customer currency.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="Total" type="xs:double"/>
<xs:element name="TaxTotal" type="xs:double"/>
<xs:element name="TaxDetails" type="tns:TaxDetailsType"/>
<xs:element name="Commission" type="xs:double" minOccurs="0"/>
<xs:element name="CrsCommission" type="xs:double" minOccurs="0"/>
<xs:element name="CostOfGoods" type="xs:double" minOccurs="0"/>
<xs:element name="Currency" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="RoomCancellationPolicyType">
<xs:sequence>
<xs:element name="Description" type="xs:string">
<xs:annotation>
<xs:documentation>A textual description of the cancellation policy.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:sequence>
<xs:element name="DeadlineTimestamp" minOccurs="0">
<xs:annotation>
<xs:documentation>
The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for the reserved hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:choice>
<xs:annotation>
<xs:documentation>
Cancellation policies can be specified in one of several ways.
</xs:documentation>
</xs:annotation>
<xs:element name="Percentage" type="xs:double">
<xs:annotation>
<xs:documentation>
This indicates that the cancellation penalty is a percentage of the total booking.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="NumberOfNights" type="xs:int">
<xs:annotation>
<xs:documentation>
This indicates the number of nights that cancellation will charge the specified number of nights.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="FlatFee">
<xs:annotation>
<xs:documentation>
This indicates that a flat fee will be charged in the specified currency.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:double">
<xs:attribute name="Currency" type="xs:string"/>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:sequence>
<xs:element name="CalculatedFeeAmount" minOccurs="0">
<xs:annotation>
<xs:documentation>
This indicates the calculated fee amount in customer currency.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:double">
<xs:attribute name="Currency" type="xs:string"/>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="RoomPriceType">
<xs:complexContent>
<xs:extension base="tns:PriceType">
<xs:sequence>
<xs:element name="DailyTotal" type="xs:double" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
An element exists for each day of the room stay. The sum of the DailyTotal elements equals the Total element.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DailyTotalNoTax" type="xs:double" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
A parallel array to the DailyTotal array. This holds the totals without tax, in case that kind of display to the customer is desired.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExchangeRateSnapshotId" type="xs:int" minOccurs="0"/>
<xs:element name="SupplierToCustomerExchangeRate" type="xs:double" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="RoomSegmentType">
<xs:complexContent>
<xs:extension base="tns:SegmentType">
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The booking descriptor is present when part of an availability request. It is not included when reading a segment after booking.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Checkin" type="tns:IsoTimestampType" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the same checkin as specified in the hotel. It is replicated here because after booking hotels are no longer retained and the segment must hold complete information.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Checkout" type="tns:IsoTimestampType" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the same checkout as specified in the hotel. It is replicated here because after booking hotels are no longer retained and the segment must hold complete information.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Nights" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
Duration of stay (Difference between Checkin and Checkout dates)
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Adults" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of adults associated with this room segment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Children" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of children associated with this room segment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Infants" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of infants in seat associated with this room segment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PackageName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this room is part of an Switchfly package, this field holds the name of that package.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChainCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The two letter code for the chain offering the room.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BrandCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The two letter code for the brand offering the room.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The CRS assigned code for the hotel. If specified only hotels with this hotel code will be returned. A collection of hotel codes can be specified using a comma separated list.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Ql2Code" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Not usable without prior configuration by Switchfly support.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The name of the hotel that contains this room.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelRating" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The rating of the hotel that contains this room</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelStarRatingDescription" type="xs:string" minOccurs="0"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
<xs:element name="CustomerSupportPhone" type="xs:string" minOccurs="0"/>
<xs:element name="HotelPhone" type="xs:string" minOccurs="0"/>
<xs:element name="HotelFax" type="xs:string" minOccurs="0"/>
<xs:element name="HotelEmail" type="xs:string" minOccurs="0"/>
<xs:element name="RoomName" type="xs:string" minOccurs="0"/>
<xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
<xs:element name="RateName" type="xs:string" minOccurs="0"/>
<xs:element name="RateMemo" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
After booking, memos might be added to a room segment. These are those memos.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AdditionalRoomDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
A list of supplementary information, such as promotional text, that is to be displayed with the room
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AdditionalHotelDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
A list of supplementary information, such as promotional text, that is to be displayed with the hotel
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="IncludedService" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
An optional list of services included with this room. These are unique to each room in the hotel. If there are hotel wide services, they will be repeated in each room segment.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="Code" type="xs:string"/>
<xs:element name="Name" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AvailableRequestService" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
This field holds a collection of services that can be requested for this room. Each service has a code, used to request the service, and a descriptive name.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="Code" type="xs:string"/>
<xs:element name="Name" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RequestedService" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
This field can be used to specify a desired service using the code specified in an AvailableRequestService.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SpecialRequests" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A string that is passed to the hotel during booking for any special requests. Such requests are not guaranteed.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="VoucherInformation" type="xs:string" minOccurs="0"/>
<xs:element name="RoomPrice" type="tns:RoomPriceType"/>
<xs:element name="StrikethroughPrice" type="tns:RoomPriceType" minOccurs="0"/>
<xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
This specifies the ages of the children associated with this room segment. If present, the number of ChildAge elements should match the value found in the Children element.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RateCode" type="xs:string" minOccurs="0"/>
<xs:element name="RoomCode" type="xs:string" minOccurs="0"/>
<xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
True if additional details about this room are available. These details can sometimes affect pricing.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierConfirmationSent" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>If true, supplier confirmation was sent.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MealType" type="xs:string" minOccurs="0"/>
<xs:element name="RoomTypeId" type="xs:integer" minOccurs="0"/>
<xs:element name="RoomContractId" type="xs:integer" minOccurs="0"/>
<xs:element name="FeeDueAtHotelList" type="tns:FeeDueAtHotelListType" minOccurs="0" maxOccurs="1">
<xs:annotation>
<xs:documentation>
Mandatory fees charged by the hotel property that is due at the time of checkin or checkout.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CustomFields" minOccurs="0">
<xs:annotation>
<xs:documentation>Custom fields for the contract</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="CustomField" type="tns:CustomFieldType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RoomingList" type="tns:GuestListType" minOccurs="0">
<xs:annotation>
<xs:documentation>
When booking, this holds the list of traveler names.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Promotion" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Promotional messages that can be added to the room during availability
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="IsPrimary" type="xs:boolean"/>
<xs:element name="Message" type="xs:string" minOccurs="0"/>
<xs:element name="LongPrimaryMessage" type="xs:string" minOccurs="0"/>
<xs:element name="IsClientExclusive" type="xs:boolean" minOccurs="0"/>
<xs:element name="PromoType" type="xs:string" minOccurs="0"/>
<xs:element name="TermsAndConditions" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RoomCancellationPolicies" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="RoomCancellationPolicy" type="tns:RoomCancellationPolicyType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="NonRefundable" type="xs:boolean" default="false" minOccurs="0">
<xs:annotation>
<xs:documentation>True if room is non refundable, false otherwise</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="RoomSegmentUpdateType">
<xs:sequence>
<xs:element name="SegmentLocator" type="xs:string"/>
<xs:element name="RequestedService" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>A comma separated list of service codes.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SpecialRequests" type="xs:string" minOccurs="0"/>
<xs:element name="Checkin" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="Checkout" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="RateCode" type="xs:string" minOccurs="0"/>
<xs:element name="RoomCode" type="xs:string" minOccurs="0"/>
<xs:element name="RoomName" type="xs:string" minOccurs="0"/>
<xs:element name="RoomingList" type="tns:GuestListType" minOccurs="0"/>
<xs:element name="RoomModificationType">
<xs:annotation>
<xs:documentation>
one of: DATES, PAX_NAMES, PAX_AGES, PAX_ADD, PAX_DELETE, ROOM_TYPE, SPECIAL_REQUESTS, REQUESTED_SERVICES
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="DATES"/>
<xs:enumeration value="PAX_NAMES"/>
<xs:enumeration value="PAX_AGES"/>
<xs:enumeration value="PAX_ADD"/>
<xs:enumeration value="PAX_DELETE"/>
<xs:enumeration value="ROOM_TYPE"/>
<xs:enumeration value="SPECIAL_REQUESTS"/>
<xs:enumeration value="REQUESTED_SERVICES"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="SegmentType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexContent>
<xs:extension base="tns:PnrElementType">
<xs:sequence>
<xs:element name="BookingComponent" type="tns:BookingComponentType" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Specifies the components included in package. The value must be one of: Activity,Air, Car, Cruise, or Room.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SegmentId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
A unique identifier for a segment of this type. I.e., no two room segments will have the same SegmentId, although a room segment and a car segment might have the same segment id.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="OriginalSegmentId" type="xs:int" minOccurs="0" maxOccurs="1">
<xs:annotation>
<xs:documentation>
An id that identifies the original segment of this type. The presence of this field indicates that this is a historical version of the object.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SwitchflyBookingNumber" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The Switchfly booking number identifies the booking in Switchfly that contains this segment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SegmentLocator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If the segment represents a booked segment, this overrides an identifier for that segment within Switchfly. This allows the segment to be identified in subsequent modification messages. A segment locator is not always available.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CrsName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The name of the CRS associated with this segment. The names are an internal Switchfly encoding.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CrsRecordLocator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The is record locator used in the crs where Switchfly made the booking. It represents the booking and would be used to read or cancel the booking. In the case of single CRS, this is the key identifier for the PNR. In the case where the booking was made through an intermediary CRS (such as a GDS), this is the GDS record locator. The record locator for the ultimate supplier is different and might be required if calling the supplier directly. See the SupplierLocator tag.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PassiveCrsRecordLocator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this segment has an associated passive segment in a separate CRS, this is the record locator for the PNR in which that passive segment was created. The name of the CRS is defined at a higher level in the PNR element.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierLocator" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If the segment represents a booked segment, this is the locator for the booking on he suppliers CRS when accessed through an intermediate CRS. For example, when booking a flight on Continental through Sabre, you get a record locator from Sabre that you use to modify or cancel the PNR. However, Continental has its own record locator that it assigns from its own CRS. When we are able, we capture this locator, too, and return it in this field. This is not intended for programmatic use, but to be printed on an itinerary for information purposes, e.g., for use when checking in at a hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Policies" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Any policies, especially regarding cancellation, applied by the Switchfly server. These policies are configured directly in the Switchfly server.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CrsPolicies" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Any policies, especially regarding cancellation, from the remote CRS. These policies are received from the CRS that provided the product.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AgencyName" type="xs:string" minOccurs="0"/>
<xs:element name="AgencyAddress" type="xs:string" minOccurs="0"/>
<xs:element name="AgentName" type="xs:string" minOccurs="0"/>
<xs:element name="ConfirmationStatus" type="xs:string" minOccurs="0"/>
<xs:element name="SpecialId" type="xs:int" minOccurs="0"/>
<xs:element name="Supplier" type="tns:InternalSupplierType" minOccurs="0">
<xs:annotation>
<xs:documentation>
Optional information about the supplier of this segment
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BookingMemos" minOccurs="0">
<xs:annotation>
<xs:documentation>
This field holds memos that can be optionally added to a booking in the remote Switchfly system.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="BookingMemo" type="tns:BookingMemoType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ConnectorCustomInfos" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
This field holds a collection of miscellaneous connector-specific details that are stored for post-booking inspections and operations.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="ConnectorCustomInfo" type="tns:ConnectorCustomInfoType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ArrivalAirConnectionInfo" type="tns:AirConnectionInfoType" minOccurs="0" maxOccurs="1">
<xs:annotation>
<xs:documentation>
This field holds arrival airline information that some connectors will use.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DepartureAirConnectionInfo" type="tns:AirConnectionInfoType" minOccurs="0" maxOccurs="1">
<xs:annotation>
<xs:documentation>
This field holds arrival airline information that some connectors will use.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SingleUseCard" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="SuperPnrType">
<xs:sequence>
<xs:element name="Traveler" type="tns:TravelerType" maxOccurs="unbounded"/>
<xs:element name="AirItinerary" type="tns:AirItineraryType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="RoomSegment" type="tns:RoomSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="CarSegment" type="tns:CarSegmentType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="ActivitySegment" type="tns:ActivitySegmentType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PnrListType">
<xs:sequence>
<xs:element name="BookingId" type="xs:string"/>
<xs:element name="ModifiedDate" type="tns:IsoTimestampType"/>
<xs:element name="ModificationType" type="xs:string"/>
<xs:element name="AgentId" type="xs:string"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="TaxDetailsType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="Tax" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:attribute name="Name" type="xs:string"/>
<xs:attribute name="Type" type="xs:string"/>
<xs:attribute name="Amount" type="xs:double"/>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="TravelerType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="InternalProfileId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The Switchfly id for the profile for this traveler.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExternalProfileId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this traveler was created based on the profile from an external system, this is the id in that external system.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LastName" type="xs:string" minOccurs="0"/>
<xs:element name="FirstName" type="xs:string" minOccurs="0"/>
<xs:element name="MiddleName" type="xs:string" minOccurs="0"/>
<xs:element name="Gender" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="M"/>
<xs:enumeration value="F"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="OfficialTravelerId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>An official id (e.g., passport number).</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="OfficialTravelerIdLabel" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Type of official traveler id. Possible label valuexs: PASSPORT, DRIVERS_LICENSE, NATIONAL_IDENTITY_CARD, LOCAL_ID
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="OfficialTravelerIdCountry" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Issuing country of official traveler id.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TsaRedressNumber" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The TSA assigned redress number.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DateOfBirth" type="tns:IsoDateType" minOccurs="0"/>
<xs:choice minOccurs="0">
<xs:element name="AgeInMonths" type="xs:int"/>
<xs:element name="Age" type="xs:int"/>
</xs:choice>
<xs:element name="AgeClass" type="tns:AgeClassType" minOccurs="0"/>
<xs:element name="Title" type="xs:string" minOccurs="0"/>
<xs:element name="AirLoyaltyPrograms" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="LoyaltyProgram" minOccurs="0" type="tns:LoyaltyProgramType" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RoomLoyaltyPrograms" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="LoyaltyProgram" type="tns:LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="CarLoyaltyPrograms" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="LoyaltyProgram" type="tns:LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="LoyaltyPrograms" type="tns:LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Obsolete. This may or may not work properly. Use the Air, Room, and Car loyalty program tags above.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Phone" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:string">
<xs:attribute name="Type" type="xs:string">
<xs:annotation>
<xs:documentation>
This attribute can be set to specify the type of phone number. It is free form, but specific values recognized by Switchfly are: home, work, and cell. Entering these values might allow better identification of a phone number in a CRS.
</xs:documentation>
</xs:annotation>
</xs:attribute>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
<xs:element name="Fax" type="xs:string" minOccurs="0"/>
<xs:element name="Company" type="xs:string" minOccurs="0"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
<xs:element name="Email" type="xs:string" minOccurs="0"/>
<xs:element name="BookingMemos" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="BookingMemo" type="tns:BookingMemoType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="DeleteTraveler" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
True/False. Defaults to false. Used during a modify to indicate that this traveler is deleted
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="GovernmentFinancialId" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="TravelerProfileType">
<xs:complexContent>
<xs:extension base="tns:TravelerType">
<xs:sequence>
<xs:element name="CreditCard" type="tns:CreditCardType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="PointRedemption" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Minimum" type="xs:int" minOccurs="0"/>
<xs:element name="Available" type="xs:int" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="UserReviewSummaryType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="UserReviewHotelCode" type="xs:string"/>
<xs:element name="LogoImageUrl" type="xs:string"/>
<xs:element name="RankScale" type="xs:int" minOccurs="0"/>
<xs:element name="Rank" type="xs:int" minOccurs="0"/>
<xs:element name="MedianUserRating" type="xs:double" minOccurs="0"/>
<xs:element name="RatingImageUrl" type="xs:string" minOccurs="0"/>
<xs:element name="WriteReviewUrl" type="xs:string" minOccurs="0"/>
<xs:element name="TotalUserReviewCount" type="xs:int" minOccurs="0"/>
<xs:element name="UserReview" type="tns:UserReviewType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="UserReviewType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="PublishedDate" type="tns:IsoDateType" minOccurs="0"/>
<xs:element name="Author" type="xs:string" minOccurs="0"/>
<xs:element name="AuthorLocation" type="xs:string" minOccurs="0"/>
<xs:element name="Title" type="xs:string" minOccurs="0"/>
<xs:element name="Content" type="xs:string" minOccurs="0"/>
<xs:element name="ContentSummary" type="xs:string" minOccurs="0"/>
<xs:element name="Rating" type="xs:int" minOccurs="0"/>
<xs:element name="RatingImage" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="FareBasisCodeType">
<xs:sequence>
<xs:element name="LocationInformation" minOccurs="1" maxOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="DepartureDate" type="tns:IsoDateType"/>
<xs:element name="Arrival" type="xs:string"/>
<xs:element name="Origin" type="xs:string"/>
<xs:element name="AirLineCode" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Code" type="xs:string"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="FeeDueAtHotelListType">
<xs:sequence>
<xs:element name="FeeDueAtHotel" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Currency" type="xs:string" minOccurs="0"/>
<xs:element name="Fee" type="xs:decimal" minOccurs="0"/>
<xs:element name="Description" type="xs:string" minOccurs="1"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AirConnectionInfoType">
<xs:sequence>
<xs:element name="AirlineCode" type="xs:string" minOccurs="0"/>
<xs:element name="AirlineName" type="xs:string" minOccurs="0"/>
<xs:element name="FlightNumber" type="xs:string" minOccurs="0"/>
<xs:element name="Time" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ItineraryType">
<xs:sequence>
<xs:element name="AirItineraryRequest" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="AirLeg" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
The BookingDescriptor string that was returned in the availability response.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="QuickAddAirItinerary" type="tns:AirItineraryType" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
If Quick Add Air Itinerary will be booked in the CreatePnrRQ, then it must be specified here in order to calculate the discount.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="RoomSegmentRequest" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="RoomSegment" type="xs:string">
<xs:annotation>
<xs:documentation>
The BookingDescriptor string that was returned in the availability response.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="CarSegmentRequest" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="CarSegment" type="xs:string">
<xs:annotation>
<xs:documentation>
The BookingDescriptor string that was returned in the availability response.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ActivitySegmentRequest" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="SegmentDescriptor" type="xs:string">
<xs:annotation>
<xs:documentation>
The BookingDescriptor string that was returned in the availability response.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="EventDescriptor" type="xs:string">
<xs:annotation>
<xs:documentation>
The Booking Event Descriptor string that was returned in the availability response.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="LoyaltyProgramType">
<xs:sequence>
<xs:element name="Company" type="xs:string">
<xs:annotation>
<xs:documentation>
The industry standard code for the company. E.g., UA for United Airlines.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Number" type="xs:string">
<xs:annotation>
<xs:documentation>The loyalty program membership number.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TierCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The Tier Code.</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivityChoiceType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexContent>
<xs:extension base="tns:ActivityDescriptionType">
<xs:sequence>
<xs:element name="BookingDescriptor" type="xs:string"/>
<xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
True if additional details about this activity are available. These details can sometimes affect pricing.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="EventGroup" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Rate">
<xs:complexType>
<xs:sequence>
<xs:element name="RateName" type="xs:string" minOccurs="0"/>
<xs:element name="RateType" type="tns:ActivityRateApplicabilityType" minOccurs="0">
<xs:annotation>
<xs:documentation>
This defines the allowed use of the activity. It has one of the valuexs: activity_only, activity/addon, or addon_only. Addon_only activities are returned only in a ActivityAddonRS, so addon_only activities should be ignored in any other context.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Label" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
<xs:element name="BookingCode" type="xs:string" minOccurs="0"/>
<xs:element name="Label" type="xs:string"/>
<xs:element name="AgeClass" type="tns:AgeClassType" minOccurs="0"/>
<xs:element name="Count" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
When booking, this holds the number of purchases for this Label. During availability, this field is not used.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="FamilyPlan" type="xs:boolean">
<xs:annotation>
<xs:documentation>
This is true if this label participates in family plan logic. If there is a family plan master label specified, up to the number of purchases in that label are free.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="FamilyPlanMaster" type="xs:boolean">
<xs:annotation>
<xs:documentation>
If true, this label specifies the number of free participants taken from the family plan labels. Only one label may be specified the master.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Price" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>
Price is included in labels only during shopping. Booked activities have a price on the activity rather than the label.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="StrikeThroughPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>Strike-through price of the label.</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="CrsPolicies" type="xs:string" minOccurs="0"/>
<xs:element name="ActivityQuestions" type="tns:ActivityQuestionsType" minOccurs="0"/>
<xs:element name="PassengersInfo" type="tns:PassengersInfoType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AnyDateEvent" type="tns:ActivityAnyDateEventType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="FixedDateEvent" type="tns:ActivityFixedDateEventType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="ActivityCancellationPolicies" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="ActivityCancellationPolicy" type="tns:ActivityCancellationPolicyType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivityCancellationPolicyType">
<xs:sequence>
<xs:element name="Description" type="xs:string">
<xs:annotation>
<xs:documentation>A textual description of the cancellation policy.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:sequence>
<xs:element name="DeadlineDateBeforeTimestamp" minOccurs="0">
<xs:annotation>
<xs:documentation>
The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for the reserved activity.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DeadlineDateAfterTimestamp" minOccurs="0">
<xs:annotation>
<xs:documentation>
The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for the reserved activity.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:choice>
<xs:annotation>
<xs:documentation>
Cancellation policies can be specified in one of several ways.
</xs:documentation>
</xs:annotation>
<xs:element name="Percentage" type="xs:double">
<xs:annotation>
<xs:documentation>
This indicates that the cancellation penalty is a percentage of the total booking.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="FlatFee">
<xs:annotation>
<xs:documentation>
This indicates that a flat fee will be charged in the specified currency.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:double">
<xs:attribute name="Currency" type="xs:string"/>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:sequence>
<xs:element name="CalculatedFeeAmount" minOccurs="0">
<xs:annotation>
<xs:documentation>
This indicates the calculated fee amount in customer currency.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:simpleContent>
<xs:extension base="xs:double">
<xs:attribute name="Currency" type="xs:string"/>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivityQuestionsType">
<xs:sequence>
<xs:element name="ActivityQuestion" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:attribute name="id" use="required">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:minLength value="1"/>
</xs:restriction>
</xs:simpleType>
</xs:attribute>
<xs:attribute name="type" type="tns:QuestionType" use="optional"/>
<xs:attribute name="title" type="xs:string" use="optional"/>
<xs:attribute name="value" type="xs:string" use="optional"/>
<xs:attribute name="min" type="xs:int" use="optional"/>
<xs:attribute name="max" type="xs:int" use="optional"/>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="QuestionType">
<xs:restriction base="xs:string">
<xs:enumeration value="TEXT_ADDITION"/>
<xs:enumeration value="BOOLEAN_ADDITION"/>
<xs:enumeration value="RANGE_ADDITION"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="PassengersInfoType">
<xs:sequence>
<xs:element name="PassengerInfo" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="ActivityQuestions" type="tns:ActivityQuestionsType" minOccurs="0" maxOccurs="1"/>
</xs:sequence>
<xs:attribute name="seqNumber" type="xs:integer" use="required"/>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ActivityDescriptionType">
<xs:complexContent>
<xs:extension base="tns:AbstractActivityType">
<xs:sequence>
<xs:element name="Category" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is a free format string intended for organizing groups of activities in a GUI.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CheckinText" type="xs:string" minOccurs="0"/>
<xs:element name="Description" type="xs:string" minOccurs="0"/>
<xs:element name="Features" type="xs:string" minOccurs="0"/>
<xs:element name="ImageUrl" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="ThumbnailUrl" type="xs:string" minOccurs="0"/>
<xs:element name="Notes" type="xs:string" minOccurs="0"/>
<xs:element name="Subcategory" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is a free format string intended for organizing groups of activities in a GUI.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ActivityPhoneNumber" type="xs:string" minOccurs="0"/>
<xs:element name="YoutubeMovieIds" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="YoutubeMovieId" type="xs:string" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivitySearchType">
<xs:complexContent>
<xs:extension base="tns:SearchType">
<xs:sequence>
<xs:choice>
<xs:element name="SearchLocation" type="tns:SearchLocationType" minOccurs="0"/>
<xs:element name="SearchActivities" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="SearchActivity" type="tns:SearchActivityType" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
<xs:element name="NumAdults" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of adults. The server may be able to return some activities without traveler counts being specified. Within the Switchfly server, internal activities do not require counts to be returned. However, most CRSs that the Switchfly server communicates with require these values and unless traveler counts are specified they will return no activities.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="NumChildren" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>The number of children.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
The ages of the children. If specified, the number of ChildAge elements must match the value entered in NumChildren.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="StartDate" type="tns:IsoDateType">
<xs:annotation>
<xs:documentation>
The date, in ISO format, on which to start searching for activities.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="EndDate" type="tns:IsoDateType">
<xs:annotation>
<xs:documentation>
The date, in ISO format, on which to stop searching for activities.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="ActivitySearchResponseType">
<xs:complexContent>
<xs:extension base="tns:SearchResponseType">
<xs:sequence>
<xs:element name="ActivityChoice" type="tns:ActivityChoiceType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="AirSearchLegType"/>
<xs:complexType name="AirSearchType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexContent>
<xs:extension base="tns:SearchType">
<xs:sequence>
<xs:element name="Airline" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Restrict search to the specified collection of airlines. Airlines are specified using their 2 letter code. (E.g., UA, AA, AQ, etc.) If no airlines are specified, than the search is unrestricted. If one or more airlines are specified, then only those airlines will be returned in the response.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Cabin" type="xs:string">
<xs:annotation>
<xs:documentation>
The cabin in which to search: "first", "business", or "coach". You can only search within a single cabin, although you may sometimes find that the results you get may be in a different cabin from what you requested.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MaxStops" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The maximum number of stops for any leg in the journey.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BulkOnly" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
If true, only private fares will be returned and published fares will be filtered out.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Adults" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>The number of adults requiring seats.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Children" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>The number of children requiring seats.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Seniors" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>The number of seniors requiring seats.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantsRequiringSeats" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of infants requiring seats. Do not include infants that will be in laps.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantsInLaps" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The number of infants that do not require seats and will be traveling in a lap.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Leg" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:AirSearchLegType">
<xs:sequence>
<xs:element name="CorrelationId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is an arbitrary string that will be echoed in the corresponding legs returned in the response.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="OriginLocation" type="tns:SearchLocationType" minOccurs="0"/>
<xs:element name="DestinationLocation" type="tns:SearchLocationType" minOccurs="0"/>
<xs:element name="DepartureTimestamp" type="tns:IsoTimestampType">
<xs:annotation>
<xs:documentation>
The ISO timestamp at which to start searching for availability. (E.g., 2008-01-02T15:30 for 2 Jan, 2008 at 3:30 PM) The search will be for the specified day and will search from the specified time forward.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="AirSearchResponseType">
<xs:complexContent>
<xs:extension base="tns:SearchResponseType">
<xs:sequence>
<xs:element name="Alternative" type="tns:AirPnrType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="CarSearchType">
<xs:complexContent>
<xs:extension base="tns:SearchType">
<xs:sequence>
<xs:element name="CountryOfResidency" minOccurs="0">
<xs:annotation>
<xs:documentation>
The two letter country code of the country in which the renter resides.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PickupLocation" type="tns:SearchLocationType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The location at which the car will be picked up. Currently, only airport pickup is supported and an ambiguous search error will be thrown if a city with multiple airports is specified.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DropOffLocation" type="tns:SearchLocationType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The location at which the car will be dropped off. Currently, Switchfly only supports dropping off at the same location as picking up. If omitted, drop off defaults to the same value as pickup.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PickupTimestamp" type="tns:IsoTimestampType">
<xs:annotation>
<xs:documentation>
The date and time that the car is desired, in ISO format.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DropOffTimestamp" type="tns:IsoTimestampType">
<xs:annotation>
<xs:documentation>
The date and time that the car will be returned, in ISO format.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChainCode" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
One or more two character company codes may be specified, in which case the search will be limited to those companies. E.g., AL or ZE.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CarCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A 4 character SIPP code can be specified (e.g., CDAR) in which case the search will be limited to the specified car type.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SortCriteria" minOccurs="0">
<xs:annotation>
<xs:documentation>
Sort car results by: car_price - sort by ascending price up to MaxResults and car_sipp_category_then_price - return cars in ascending price up to MaxResults for each car class (1st letter of SIPP code).
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="car_price"/>
<xs:enumeration value="car_sipp_category_then_price"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="MaxResults" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
Limit car results up to MaxResults or the limit car results up to MaxResults by car class depending on SortCriteria.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AtAirportOnly" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
If true, than only airport locations will be returned from the search. For most data sources, the currently version of Switchfly will only return airport locations. The exception is that internal Switchfly cars can be located at arbitrary locations.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="CarSearchResponseType">
<xs:complexContent>
<xs:extension base="tns:SearchResponseType">
<xs:sequence>
<xs:element name="CarSegment" type="tns:CarSegmentType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="DirectionType">
<xs:sequence>
<xs:element name="Origin" type="xs:string" minOccurs="0"/>
<xs:element name="Destination" type="xs:string" minOccurs="0"/>
<xs:element name="Directions" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="HotelType">
<xs:sequence>
<xs:element name="EzRezId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
Switchfly maintains a master list of hotels independent of the hotels returned from its sources. In order to avoid presenting duplicate hotels, it attempts to match each hotel with this master list. If successful, this element holds the id of that master hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Ql2Code" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Not usable without prior configuration by Switchfly support.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SearchHotelId" minOccurs="0">
<xs:annotation>
<xs:documentation>
If the search specified hotel ids, this element will identify the id that caused this hotel to be returned.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:sequence>
<xs:element name="IdSource" type="xs:string"/>
<xs:element name="Id" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="AirportCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>They airport code associated with this hotel.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BookingDescriptor" type="xs:string">
<xs:annotation>
<xs:documentation>
A string that will be used when booking a room in this hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChainCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Switchfly maintains a master list of hotel chains independent of the hotel chains returned from its sources.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="BrandCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Switchfly maintains a master list of hotel brands independent of the hotel brands returned from its sources.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The name of the hotel.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="HotelCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A code returned from the CRS to which Switchfly connected to search for rooms. It uniquely defines the hotel for a period of time defined by the remote CRS. This time can range from temporary (such as a small time period after the availability query) to permanent. THis value is not returned if the search is specified using a hotel id.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Approved" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
If omitted, this is false. If true, it indicates the Switchfly server has approved this hotel. This is usually used to display the hotel in special format.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ExclusiveDeal" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
If omitted, this is false. If true, it indicates that the hotel offer is flagged from the CRS as an exclusive deal and you may want to sort this early in your customer display.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Policies" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The policies of the remote system regarding payments and refunds.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PropertyDescription" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>A textual description of the property.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RoomType" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="RoomTypesDescriptions" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A textual description of the types of rooms available in the hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
<xs:element name="ShortDescription" type="xs:string" minOccurs="0"/>
<xs:element name="ExteriorDescription" type="xs:string" minOccurs="0"/>
<xs:element name="LobbyDescription" type="xs:string" minOccurs="0"/>
<xs:element name="PointOfInterest" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Description" type="xs:string"/>
<xs:sequence minOccurs="0">
<xs:element name="Distance" type="xs:float"/>
<xs:element name="Units" type="xs:string">
<xs:annotation>
<xs:documentation>miles or meters</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="PointsOfInterestDescription" type="xs:string" minOccurs="0"/>
<xs:element name="Address" type="tns:AddressType" minOccurs="0">
<xs:annotation>
<xs:documentation>The address of the hotel.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Phone" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The phone number for calls regarding the hotel. This may be a toll free number shared among many hotels.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LocalPhone" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A phone number that is associated specifically with the hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Fax" type="xs:string" minOccurs="0"/>
<xs:element name="LocalFax" type="xs:string" minOccurs="0"/>
<xs:element name="GeoLocation" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Latitude" type="xs:string"/>
<xs:element name="Longitude" type="xs:string"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="DistanceFromSearchLocation" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Distance" type="xs:double"/>
<xs:element name="Units" type="tns:DistanceUnitsType"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Amenity" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Any number of amenities may be provided identifying the amenities for the hotel. (E.g., pool, parking, etc.) These amenities are free-form and can vary in wording and spelling, even from a single CRS.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AmenitiesDescription" type="xs:string" minOccurs="0"/>
<xs:element name="AmenityFilter" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
An amenity filter is an amenity that has been "unified" by the Switchfly server and is one of a well defined set of values. It is intended for use in filtering hotels in GUI based on available amenities. It is not intended to replace the Amenity elements, which most likely include more information than the amenityFilter elements
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="AIRPORT_SHUTTLE"/>
<xs:enumeration value="ALL_INCLUSIVE"/>
<xs:enumeration value="BABYSITTING_CHILD_CARE"/>
<xs:enumeration value="BAR_LOUNGE_ONSITE"/>
<xs:enumeration value="BEACH"/>
<xs:enumeration value="BUSINESS_CENTER"/>
<xs:enumeration value="CASINO"/>
<xs:enumeration value="CHILDREN_TEEN_PROGRAMS"/>
<xs:enumeration value="CONTINENTAL_MEAL_PLAN"/>
<xs:enumeration value="FREE_PARKING"/>
<xs:enumeration value="GOLF"/>
<xs:enumeration value="GYM"/>
<xs:enumeration value="HANDICAP_FACILITIES"/>
<xs:enumeration value="INTERNET"/>
<xs:enumeration value="MEETING_ROOMS"/>
<xs:enumeration value="PETS_ALLOWED"/>
<xs:enumeration value="RESTAURANT_ONSITE"/>
<xs:enumeration value="ROOM_SERVICE"/>
<xs:enumeration value="SKI_IN_SKI_OUT"/>
<xs:enumeration value="SMALL_PETS_ALLOWED"/>
<xs:enumeration value="SPA"/>
<xs:enumeration value="SWIMMING_POOL"/>
<xs:enumeration value="SMOKE_FREE"/>
<xs:enumeration value="TENNIS"/>
<xs:enumeration value="WIRELESS_INTERNET"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="FamilyDescription" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This field is a free format text field that describes the hotel policy that defines a family. This will be absent unless the remote CRS provides this information. It is most likely present for hotels defined locally in the Switchfly that is serving this message.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Checkin" type="tns:IsoTimestampType" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the date and time of checkin. The date is taken from the original search. The time is added, if known, from the description of the hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Checkout" type="tns:IsoTimestampType" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is the date and time of checkout. The date is taken from the original search. The time is added, if known, from the description of the hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Rating" type="xs:double" minOccurs="0"/>
<xs:element name="SpecialNotices" type="xs:string" minOccurs="0"/>
<xs:element name="AdditionalInfo" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is a free format placeholder for additional information about the hotel. It is currently only populated for internally loaded Switchfly hotels.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AdditionalDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="Currency" type="xs:string"/>
<xs:element name="MinimumPrice" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
For some sources of availability, the first call to get availability does not return specific room information but does return a price range for the hotel. That information is provided here. In this case, the minimum and maximum may be only an estimate and may differ from the true minimum and maximum that will be determined when complete room information is obtained using the hotel details message. If room information *is* returned, these tags will simply reflect the min and max prices of the underlying rooms.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MinimumStandalonePrice" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
If the search included a flag to get package savings, then this elements will represent the minimum standalone rate for a hotel that was searched as part of a package.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MinimumStandalonePriceTaxTotal" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
If the search included a flag to get package savings, then this elements will represent the minimum standalone rate tax for a hotel that was searched as part of a package.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MinimumPriceTaxTotal" type="xs:double" minOccurs="0">
<xs:annotation>
<xs:documentation>
These tags will simply reflect the tax total for the minimum price of the underlying rooms.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Url" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A link to a page that provide additional information about the hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Html" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
HTML that describes the hotel. In general, only one of these or the Url element is specified.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MapUrl" type="xs:string" minOccurs="0"/>
<xs:element name="Thumbnail" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Images over the API are limited to leonardo.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="VirtualTourUrl" type="xs:string" minOccurs="0"/>
<xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="RoomSegment" type="tns:RoomSegmentType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
<xs:attribute name="adults" type="xs:int" use="required"/>
<xs:attribute name="children" type="xs:int"/>
</xs:complexType>
</xs:element>
<xs:element name="RoomAmenity" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
Any number of amenities may be present identifying the amenities for each room in the hotel. (E.g., safe, HBO, etc.)
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RoomAmenitiesDescription" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is a free format description of the amenities present in the rooms of the hotel. Whether a free format description as held here is provided, or a list of amenities suitable for a bullet list is provided using the RoomAmenity tag, depends on the ultimate source of the room.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Direction" type="tns:DirectionType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="Restaurant" type="tns:RestaurantType" minOccurs="0" maxOccurs="unbounded"/>
<xs:element name="RestaurantsDescription" type="xs:string" minOccurs="0"/>
<xs:element name="Image" type="tns:ImageType" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>Images over the API are limited to leonardo.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0">
<xs:annotation>
<xs:documentation>
Since the Switchfly system communicates with and aggregates products from a number of different CRSs, it must adapt to various styles of availability searches. Some systems return all they know about available properties in one call. Others might return just a hotel description and no information about rooms. Even others might return bare bones information about the hotel but details about available rooms. The DetailsAvailable boolean indicates whether additional information about the hotel is available. If so, the HotelDetails message may be used to get additional information about the property. The intent is that this second, potentially much more expensive, call is avoided until the customer indicates interest in the property. While the original room search translates into a single call to the remote CRS, the details call can translate into multiple calls for the single specified hotel.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Entertainment" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This is a free format field allowing a description of entertainment options at or around the hotel. It is currently only populated by internally loaded Switchfly hotels.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="UserReviewSummary" type="tns:UserReviewSummaryType" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="ImageType">
<xs:sequence>
<xs:element name="Height" type="xs:int" minOccurs="0"/>
<xs:element name="Width" type="xs:int" minOccurs="0"/>
<xs:element name="Caption" type="xs:string" minOccurs="0"/>
<xs:element name="Name" type="xs:string" minOccurs="0"/>
<xs:element name="ThumbnailUrl" type="xs:string" minOccurs="0"/>
<xs:element name="ImageId" type="xs:integer" minOccurs="0"/>
<xs:element name="Type" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
"lobby", "logo","map", "guest room", "suite", "restaurant", "beach", "spa", "pool", "exterior", "golf", "meeting_room", "ballroom", "bar_lounge", "public_area", "interior", "view", "unknown", "main";
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Url" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="AirPnrType">
<xs:sequence>
<xs:element name="Adults" type="xs:int">
<xs:annotation>
<xs:documentation>The number of adults specified in the search.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AdultPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>The unit price for each adult in the itinerary.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Children" type="xs:int">
<xs:annotation>
<xs:documentation>The number of children specified in the search.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChildPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>The unit price for each child in the itinerary.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
The ages of the children. If specified, the number of ChildAge elements must match the value entered in NumChildren.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Seniors" type="xs:int">
<xs:annotation>
<xs:documentation>The number of seniors specified in the search.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SeniorPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>The unit price for each senior in the itinerary.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantsRequiringSeats" type="xs:int">
<xs:annotation>
<xs:documentation>
The number of infants specified in the search that require their own seat.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantRequiringSeatPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The unit price for each seated infant in the itinerary.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantsInLaps" type="xs:int">
<xs:annotation>
<xs:documentation>
The number of infants specified in the search that will be sitting in a lap.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="InfantInLapPrice" type="tns:PriceType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The unit price for each infant in lap in the itinerary.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="IsPassThrough" type="xs:boolean">
<xs:annotation>
<xs:documentation>
This is a boolean that is true if air fare is to be charged to the customers credit card rather than settled between the Switchfly server and the airline. If true, the customers credit card should be passed with the booking and it should not be charged locally for the air fare.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TotalPrice" type="tns:PriceType">
<xs:annotation>
<xs:documentation>The total price for the itinerary.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="TicketingDeadline" type="tns:IsoDateType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The ticketing deadline is informational. When returned from the CRS, Switchfly captures it and passes it on. It is used by some clients to postpone ticketing. We recommend immediate ticketing, however, since prices are not guaranteed until ticketed.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="LegChoices" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="AirLeg" type="tns:AirLegType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="FareBasisCodes" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="FareBasisCode" type="tns:FareBasisCodeType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="RestaurantType">
<xs:sequence>
<xs:element name="Atmosphere" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>e.g., “casual”</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Cuisine" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>e.g., “american”</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Hours" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>e.g., “5pm – 1am”</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Name" type="xs:string"/>
<xs:element name="Setting" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>e.g., “ocean view”</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Description" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>e.g., “your description”</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="RoomSearchType">
<xs:sequence>
<xs:element name="CrsName" type="xs:string" minOccurs="0"/>
<xs:element name="SearchLocation" type="tns:SearchLocationType" minOccurs="0"/>
<xs:element name="RadiusSearch" type="tns:RadiusSearchType" minOccurs="0"/>
<xs:element name="ChainCode" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="BrandCode" type="xs:string" minOccurs="0"></xs:element>
<xs:choice minOccurs="0">
<xs:element name="UnifiedHotelIds" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="Id" type="xs:string" maxOccurs="unbounded"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="HotelIds" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="IdSource" type="xs:string"></xs:element>
<xs:element name="Id" type="xs:string" maxOccurs="unbounded"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
<xs:element name="Ql2HotelIds" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Id" type="xs:string" minOccurs="1" maxOccurs="unbounded"></xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="InternalRoom" minOccurs="0" maxOccurs="1">
<xs:complexType>
<xs:sequence>
<xs:element name="ContractId" type="xs:integer" minOccurs="0"/>
<xs:element name="RoomTypeId" type="xs:integer" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="HotelCode" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="ExcludedEzRezHotelIds" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="IncludedEzRezHotelIds" type="xs:string" minOccurs="0"></xs:element>
<xs:element name="SortCriteria" minOccurs="0">
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="hotel_price_ascending"/>
<xs:enumeration value="hotel_price_descending"/>
<xs:enumeration value="hotel_composite_score"/>
<xs:enumeration value="hotel_rating_then_price"/>
<xs:enumeration value="hotel_rating_then_composite_score"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="MaxResults" type="xs:integer" minOccurs="0"></xs:element>
<xs:element name="PreferredHotelRating" type="xs:double" minOccurs="0" maxOccurs="unbounded"></xs:element>
<xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
<xs:complexType>
<xs:sequence>
<xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
<xs:attribute name="adults" type="xs:int" use="required"/>
<xs:attribute name="children" type="xs:int"/>
</xs:complexType>
</xs:element>
<xs:element name="CheckinDate" type="tns:IsoDateType"></xs:element>
<xs:element name="CheckoutDate" type="tns:IsoDateType"></xs:element>
<xs:element name="Rating" type="xs:int" minOccurs="0"></xs:element>
<xs:element name="ShouldDoPackageSavingsSearch" type="xs:boolean" minOccurs="0"></xs:element>
<xs:element name="ResponseDetails" minOccurs="0">
<xs:complexType>
<xs:simpleContent>
<xs:extension base="tns:ResponseDetailsType">
<xs:attribute name="WithTripAdvisor" default="true" type="xs:boolean"></xs:attribute>
<xs:attribute name="ResultsPerHotel" type="tns:ResultsPerHotelType"></xs:attribute>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="ResponseDetailsType">
<xs:restriction base="xs:string">
<xs:enumeration value="FULL"/>
<xs:enumeration value="MINIMUM"/>
<xs:enumeration value="ROOM_DETAILS"/>
</xs:restriction>
</xs:simpleType>
<xs:simpleType name="ResultsPerHotelType">
<xs:restriction base="xs:string">
<xs:enumeration value="ALL"/>
<xs:enumeration value="LOWEST_PRICED_ROOM"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="RoomSearchResponseType">
<xs:complexContent>
<xs:extension base="tns:SearchResponseType">
<xs:sequence>
<xs:element name="Hotel" type="tns:HotelType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="RadiusSearchType">
<xs:annotation>
<xs:documentation>The radius search from given location</xs:documentation>
</xs:annotation>
<xs:sequence>
<xs:element name="GeoLocation" minOccurs="1">
<xs:complexType>
<xs:annotation>
<xs:documentation>Specify the Latitude and Longitude coordinates</xs:documentation>
</xs:annotation>
<xs:sequence>
<xs:element name="Latitude" minOccurs="1">
<xs:simpleType>
<xs:annotation>
<xs:documentation>
Specify the north-south position of the point on the Earths surface
</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:double">
<xs:minInclusive value="-90"/>
<xs:maxInclusive value="90"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="Longitude" minOccurs="1">
<xs:simpleType>
<xs:annotation>
<xs:documentation>
Specify the east-west position of a point on the Earths surface
</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:double">
<xs:minInclusive value="-180"/>
<xs:maxInclusive value="180"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="Radius" minOccurs="0" default="10">
<xs:simpleType>
<xs:annotation>
<xs:documentation>
The maximum distance to search from the specified point
</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:int">
<xs:minInclusive value="1"/>
<xs:maxInclusive value="50"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="Units" type="tns:DistanceUnitsType" minOccurs="0" default="m"/>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="DistanceUnitsType">
<xs:annotation>
<xs:documentation>Distance units</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:string">
<xs:enumeration value="m"/>
<xs:enumeration value="km"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="SearchLocationType">
<xs:annotation>
<xs:documentation>
A search location may be specified in one or more of three ways. At least one of the child elements must be specified. If more than one is specified, the server is free to choose which data to use.
</xs:documentation>
</xs:annotation>
<xs:sequence>
<xs:element name="Location" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
A location can be specified as a free form string. It will be parsed and an attempt made to match it against (1) an airport code; (2) an area or subarea as defined within the server; (3) A city, with an optional state or country qualifier, such as "Paris, TX" or "Paris, FR", o a region. If the string can be interpreted unambiguously, it will be used. If no match can be found, or the search is ambiguous, then an error will be returned.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="RegionId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
A region is a collection of cities within Switchfly. There is a small set of known regions defined in Switchfly that can be identified with an id.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CityId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
The Switchfly assigned id for the city being searched.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AirportCode" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
One or more airport codes may be specified to indicate the destination to search. It depends on the source of the inventory whether the search will benefit from multiple airport codes being specified. The Switchfly server will usually be able to use just one airport code if searching externally. No guarantee is made for which of a collection of codes will be used if only one can be used, so it is generally best to specify a single airport code.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
    <xs:complexType name="SearchType">
        <xs:annotation>
            <xs:documentation />
        </xs:annotation>
        <xs:sequence>
            <xs:element name="SearchId" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        If specified, this id will be returned with the associated search results.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="BookingComponent" type="tns:BookingComponentType" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                    <xs:documentation>
                        Specifies the components that will go into the ultimate booking. This field may be repeated. At least one
                        component (the type of the search) is assumed. Specifying additional components specifies that the ultimate sale will be
                        packaged with opaque pricing and may allow the search to find special fares. The value must be one of: Activity,Air, Car,
                        Cruise, or Room.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="SpecialId" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        If this element exists and is not zero, it represents a "special" in the client system for which this search is
                        being made. Most servers will ignore this field, but the server is free to make pricing decisions based on this value.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
<xs:complexType name="SearchResponseType">
<xs:sequence>
<xs:element name="SearchId" minOccurs="0">
<xs:annotation>
<xs:documentation>
If specified in the search requests, the SearchId is echoed here.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ErrorText" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
This holds error text that occurred while processing the request. Since availability might come from multiple sources, errors can occur on some sources and other sources may work correctly, so the existence of errors does not mean there are not usable results. However, it is an indicator of some issues on the server that should, perhaps, be investigated to get better availability.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="SeatMapType">
<xs:sequence>
<xs:element name="DepartureAirportCode" type="xs:string"/>
<xs:element name="ArrivalAirportCode" type="xs:string"/>
<xs:element name="DepartureDate" type="tns:IsoDateType"/>
<xs:element name="Airline" type="xs:string"/>
<xs:element name="Flight" type="xs:string"/>
<xs:element name="Cabin" type="xs:string"/>
<xs:element name="Columns">
<xs:complexType>
<xs:sequence>
<xs:element name="Column">
<xs:complexType>
<xs:sequence>
<xs:element name="Name" type="xs:string"/>
<xs:element name="Aisle" type="xs:boolean"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
<xs:element name="FirstRowOverWing" type="xs:int"/>
<xs:element name="LastRowOverWing" type="xs:int"/>
<xs:element name="Rows">
<xs:complexType>
<xs:sequence>
<xs:element name="Row">
<xs:complexType>
<xs:sequence>
<xs:element name="Number" type="xs:int"/>
<xs:element name="ExitRowLeft" type="xs:boolean"/>
<xs:element name="ExitRowRight" type="xs:boolean"/>
<xs:element name="Seats">
<xs:complexType>
<xs:sequence>
<xs:element name="Seat">
<xs:complexType>
<xs:sequence>
<xs:element name="Column" type="xs:string"/>
<xs:element name="IsAvailable" type="xs:boolean"/>
<xs:element name="IsPreferential" type="xs:boolean"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="SearchActivityType">
<xs:sequence>
<xs:element name="CrsName" type="tns:NonEmptyString"/>
<xs:element name="ActivityCodes">
<xs:complexType>
<xs:sequence>
<xs:element name="ActivityCode" type="tns:NonEmptyString" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:element name="EzRezGenericRS" type="tns:SwitchflyRSType"/>
<xs:element name="PingRQ">
<xs:annotation>
<xs:documentation>
This message can be used to test if the server is up with minimal processing.
</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRQType">
<xs:sequence>
<xs:element name="Token" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This element is optional and will be echoed if present.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:element name="PingRS">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexType>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRSType">
<xs:sequence>
<xs:element name="Token" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
This element echoes the token passed in the request.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
</xs:element>
<xs:simpleType name="AgeClassType">
<xs:restriction base="xs:string">
<xs:enumeration value="ADULT"/>
<xs:enumeration value="CHILD"/>
<xs:enumeration value="SENIOR"/>
<xs:enumeration value="INFANT_IN_LAP"/>
<xs:enumeration value="INFANT_REQUIRING_SEAT"/>
</xs:restriction>
</xs:simpleType>
<xs:simpleType name="BookingComponentType">
<xs:restriction base="xs:string">
<xs:enumeration value="Activity"/>
<xs:enumeration value="Air"/>
<xs:enumeration value="Car"/>
<xs:enumeration value="Cruise"/>
<xs:enumeration value="Room"/>
</xs:restriction>
</xs:simpleType>
<xs:simpleType name="CurrencyAmountType">
<xs:restriction base="xs:double"/>
</xs:simpleType>
<xs:simpleType name="CurrencyCodeType">
<xs:restriction base="xs:string">
<xs:length value="3"/>
</xs:restriction>
</xs:simpleType>
<xs:simpleType name="IsoDateType">
<xs:annotation>
<xs:documentation>yy-mm-dd</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:string"/>
</xs:simpleType>
<xs:simpleType name="IsoTimeType">
<xs:annotation>
<xs:documentation>hh:mm:ss</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:string"/>
</xs:simpleType>
<xs:simpleType name="ProductType">
<xs:restriction base="xs:string">
<xs:enumeration value="room"/>
<xs:enumeration value="car"/>
<xs:enumeration value="air"/>
<xs:enumeration value="activity"/>
<xs:enumeration value="booking fee"/>
<xs:enumeration value="discount"/>
<xs:enumeration value="insurance"/>
<xs:enumeration value="merchandise"/>
<xs:enumeration value="master_supplier"/>
<xs:enumeration value="api"/>
</xs:restriction>
</xs:simpleType>
<xs:simpleType name="IsoTimestampType">
<xs:annotation>
<xs:documentation>yyyy-mm-dd-hh:mm:ss</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:string"/>
</xs:simpleType>
<xs:complexType name="EzRezMessageType"/>
<xs:complexType name="SwitchflyPaymentRQType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexContent>
<xs:extension base="tns:SwitchflyRQType">
<xs:sequence>
<xs:element name="DeviceFingerprint" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
If this tag is specified, it defines a device fingerprint for the customer on whose behalf this API call is made. (see http://en.wikipedia.org/wiki/Device_fingerprint) This is sued during fraud screening.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PaymentOptions" minOccurs="0">
<xs:annotation>
<xs:documentation>You have to chose one PaymentOption element.</xs:documentation>
</xs:annotation>
<xs:complexType>
<xs:all>
<xs:element name="PaymentOption" type="tns:PaymentOptionType"/>
</xs:all>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="SwitchflyRQType">
        <xs:annotation>
            <xs:documentation/>
        </xs:annotation>
        <xs:complexContent>
            <xs:extension base="tns:EzRezMessageType">
                <xs:sequence>
                    <xs:element name="UserId" type="xs:string">
                        <xs:annotation>
                            <xs:documentation>A pre-assigned id used for accessing this Switchfly system.</xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="Password" type="xs:string">
                        <xs:annotation>
                            <xs:documentation>
                                The associated password. Note that the password is passed in clear text, so communication must be done
                                over a secure socket.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="Cobrand" type="xs:string" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                The cobrand. This value should be provided by the company operating the server to which this message is
                                sent. If no cobrand is specified the "default" cobrand will be used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="LanguageId" type="xs:int" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                This is the language id for a language defined within Switchfly. This value should be provided by the
                                company operating the server to which this message is sent. If not specified, the language for the active cobrand is
                                used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="Currency" type="tns:CurrencyCodeType" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                This tag specifies the currency of the prices returned in the response. An error will be thrown if the
                                currency is not a supported customer currency in the server.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="Debug" type="xs:boolean" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                If this tag is specified with "true" as its value, then debugging information about this message will be
                                saved on the server. For general use this value should be omitted or set to false. However when debugging certain
                                situations in conjunction with Switchfly they may ask you to set this value to true in order to capture details about
                                the message interactions.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="CustomerIp" type="xs:string" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                If this tag is specified, it defines the internet address of the customer on whose behalf this API call
                                is made. The Switchfly system detects, uses, and records the IP address during the search and booking processes and it
                                can affect pricing and fraud screening and is ultimately recorded with the booking. When processing an API call the
                                address detected by Switchfly is the address of the API client rather than that of the customer the API client is
                                serving, so this tag allows Switchfly to get the correct address for more accurate processing.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="ClientMarker" type="xs:string" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                If this tag is specified, it tags the specific request within Switchfly with this marker string for
                                later analysis. Recommend to use a GUID.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element minOccurs="0" name="ClientExtensions">
                        <xs:annotation>
                            <xs:documentation>
                                The element allows for custom, pre-agreed upon elements between Switchfly and their client-partners.
                            </xs:documentation>
                        </xs:annotation>
                        <xs:complexType>
                            <xs:sequence>
                                <xs:any maxOccurs="unbounded" minOccurs="0" processContents="lax"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="StubScenario" type="xs:string" minOccurs="0">
                        <xs:annotation>
                            <xs:documentation>
                                The Query String that should be used by the Stub Framework. Value should be formatted the same as the
                                Query String that is used to invoke the Stub Framework from a URL on Switchfly web pages.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                </xs:sequence>
                <xs:attribute name="Version" use="required">
                    <xs:simpleType>
                        <xs:restriction base="xs:string">
                            <xs:enumeration value="16.3"/>
                        </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
<xs:complexType name="FailureType">
<xs:simpleContent>
<xs:extension base="xs:string">
<xs:attribute name="type">
<xs:annotation>
<xs:documentation>
This defines the category of the error. It currently is little used and is either PAYMENT or missing.
</xs:documentation>
</xs:annotation>
</xs:attribute>
<xs:attribute name="code" type="xs:string">
<xs:annotation>
<xs:documentation>
This defines the error. To see the current list of error codes, look at the list of error translations in your Switchfly system. This code will always be present and be one of those predefined codes.
</xs:documentation>
</xs:annotation>
</xs:attribute>
<xs:attribute name="translation" type="xs:string">
<xs:annotation>
<xs:documentation>
This is the translation of the error using the translation using the client translation tables based on the language active during the api call.
</xs:documentation>
</xs:annotation>
</xs:attribute>
</xs:extension>
</xs:simpleContent>
</xs:complexType>
<xs:complexType name="SwitchflyRSType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:complexContent>
<xs:extension base="tns:EzRezMessageType">
<xs:sequence>
<xs:element name="LogName" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>
The name of a log file associated with this message. This can be used when corresponding with Switchfly about API questions. Multiple logs can be generated.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ClientMarker" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Echoed back client marker string passed in the request. Normally a GUID.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:choice>
<xs:element name="Success" type="xs:string">
<xs:annotation>
<xs:documentation>
Indicates successful message completion if tag is present. Tag has no contents.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Failure" type="tns:FailureType"/>
</xs:choice>
</xs:sequence>
</xs:extension>
</xs:complexContent>
</xs:complexType>
<xs:complexType name="AddressType">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
<xs:sequence>
<xs:element name="AddressLine" type="xs:string" minOccurs="0" maxOccurs="unbounded">
<xs:annotation>
<xs:documentation>Free format address line</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="City" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>The name of the city.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="State" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
The two letter state code for US and Canada. For other countries, this may be a state code or a spelled out state name.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Zip" type="xs:string" minOccurs="0"/>
<xs:element name="Country" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Where possible, a two letter country code is returned or expected for addresses.
</xs:documentation>
</xs:annotation>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="CreditCardType">
<xs:sequence>
<xs:element name="Type" minOccurs="0">
<xs:annotation>
<xs:documentation>
The type is determined from the number so the type need not be specified. If specified and it does not match the type determined from the number, an error will be generated. AMERICAN_EXPRESS = "AX", CARTE_BLANCHE = "CB", DINERS_CLUB = "DC", DISCOVER = "DS", ENROUTE = "EN", JCB = "JC", MASTERCARD = "CA", VISA = "VI"
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="AX"/>
<xs:enumeration value="CB"/>
<xs:enumeration value="DC"/>
<xs:enumeration value="DS"/>
<xs:enumeration value="EN"/>
<xs:enumeration value="JC"/>
<xs:enumeration value="LS"/>
<xs:enumeration value="CA"/>
<xs:enumeration value="VI"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="Number" type="xs:string"/>
<xs:element name="LastName" type="xs:string"/>
<xs:element name="FirstName" type="xs:string"/>
<xs:element name="Address" type="tns:AddressType"/>
<xs:element name="Expiration" type="xs:string">
<xs:annotation>
<xs:documentation>ISO date format. (2007-11)</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Email" type="xs:string" minOccurs="0"/>
<xs:element name="SecurityCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
To help guard against fraud, the major credit card companies have implemented a system to ensure the credit card used in a transaction is actually possessed by the user. This system may be variously called CVV2 (Visa), CVC2 (MasterCard), or CID (American Express). The verification number is a non-embossed number printed on the credit card.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="Phone" type="xs:string" minOccurs="0"/>
<xs:element name="ParcelId" type="xs:integer" minOccurs="0"/>
<xs:element name="GovernmentId" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Some clients collect a government id (e.g., CPF for Brazil, Social Security Number for US). The type of id collected can differ according to country or client.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ProcessPaymentAmount" type="xs:double" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="BankTransferType">
<xs:sequence>
<xs:element name="BankId" type="xs:integer"/>
<xs:element name="CallBackURLSuccess" type="xs:string" minOccurs="0"/>
<xs:element name="CallBackURLPending" type="xs:string" minOccurs="0"/>
<xs:element name="CallBackURLFailure" type="xs:string" minOccurs="0"/>
<xs:element name="ProcessPaymentAmount" type="xs:double" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="MiscType">
<xs:sequence>
<xs:element name="PaymentMemo" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Payment memo. Can be used to store a client reference to the payment.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="ProcessPaymentAmount" type="xs:double" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="PaymentOptionType">
<xs:choice>
<xs:element name="CreditCard" type="tns:CreditCardType"/>
<xs:element name="BankTransfer" type="tns:BankTransferType"/>
<xs:element name="Miscellaneous" type="tns:MiscType"/>
</xs:choice>
</xs:complexType>
<xs:simpleType name="FraudScreenStatusType">
<xs:annotation>
<xs:documentation>
The result of fraud screening in the server. It can be ACCEPT, DECLINE, or REVIEW if screening is still in progress. If fraud screening is not enabled in the server, it will default to ACCEPT.
</xs:documentation>
</xs:annotation>
<xs:restriction base="xs:string">
<xs:enumeration value="ACCEPT"/>
<xs:enumeration value="DECLINE"/>
<xs:enumeration value="REVIEW"/>
</xs:restriction>
</xs:simpleType>
<xs:complexType name="InternalRoomCustomFieldType">
<xs:sequence>
<xs:element name="Id" type="xs:int"/>
<xs:element name="Name" type="xs:string" minOccurs="0"/>
<xs:element name="Value" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="InternalSupplierType">
<xs:annotation>
<xs:documentation>
Base type for a supplier (SupplierId and SupplierName)
</xs:documentation>
</xs:annotation>
<xs:sequence>
<xs:element name="Product" type="tns:ProductType">
<xs:annotation>
<xs:documentation>The type of supplier (e.g. room)</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="PropertyType" minOccurs="0">
<xs:annotation>
<xs:documentation>
The type of property, currently only used for Hotel suppliers.
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="hotel"/>
<xs:enumeration value="bandb"/>
<xs:enumeration value="boutique"/>
<xs:enumeration value="condo"/>
<xs:enumeration value="vacation_rental"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="SupplierId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>Unique id of the supplier</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierName" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Name of the supplier specified in the Switchfly Admin Portal
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="NotificationMethod" minOccurs="0">
<xs:annotation>
<xs:documentation>email or fax (required)</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:enumeration value="email"/>
<xs:enumeration value="fax"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="NotificationEmail" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Email address of the supplier (one form of notification is required)
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="NotificationFax" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Fax number of the supplier (one form of notification is required)
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Unique identifier used by partners</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AgencyCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Used by supplier to track the booking channel</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CurrencyCode" minOccurs="0">
<xs:annotation>
<xs:documentation>
The currency of the rates (aka supplier currency). This should be a valid three letter code set up in the Admin Portal.
</xs:documentation>
</xs:annotation>
<xs:simpleType>
<xs:restriction base="xs:string">
<xs:maxLength value="3" fixed="true"/>
<xs:minLength value="3" fixed="true"/>
</xs:restriction>
</xs:simpleType>
</xs:element>
<xs:element name="SupplierOnlyBookingMemo" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>
Memo field sent to the supplier during booking in the notification message.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierAddress" type="tns:AddressType" minOccurs="0">
<xs:annotation>
<xs:documentation>The address of the supplier.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SupplierPhone" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation>Phone number for the supplier.</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SubAreaId" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
Search subarea id where the hotel can be found. This is configured through Admin Portal.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="SubAreaId2" type="xs:int" minOccurs="0">
<xs:annotation>
<xs:documentation>
Secondary search subarea id where the hotel can be found. This is configured through Admin Portal.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="MasterSupplier" type="tns:InternalSupplierType" minOccurs="0">
<xs:annotation>
<xs:documentation>
If a segment is associated to a Master Supplier, it will be populated at this element.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="AirportCode" type="xs:string" minOccurs="0">
<xs:annotation>
<xs:documentation/>
</xs:annotation>
</xs:element>
<xs:element name="ShouldSendNetsOnManifest" type="xs:boolean" default="false" minOccurs="0">
<xs:annotation>
<xs:documentation>
Flag to determine if net rates are included in the notification email or fax to the supplier.
</xs:documentation>
</xs:annotation>
</xs:element>
<xs:element name="CheckinTime" type="tns:IsoTimeType" minOccurs="0"/>
<xs:element name="CheckoutTime" type="tns:IsoTimeType" minOccurs="0"/>
<xs:element name="Translations" minOccurs="0">
<xs:complexType>
<xs:sequence>
<xs:element name="Translation" type="tns:HotelTranslationType" minOccurs="0" maxOccurs="unbounded"/>
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:sequence>
</xs:complexType>
<xs:complexType name="HotelTranslationType">
<xs:sequence>
<xs:element name="LanguageId" type="xs:int" minOccurs="1"/>
<xs:element name="HotelAmenities" type="xs:string" minOccurs="0"/>
<xs:element name="HotelDescription" type="xs:string" minOccurs="0"/>
<xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
<xs:element name="RoomAmenities" type="xs:string" minOccurs="0"/>
<xs:element name="Directions" type="xs:string" minOccurs="0"/>
<xs:element name="Restaurants" type="xs:string" minOccurs="0"/>
<xs:element name="Entertainment" type="xs:string" minOccurs="0"/>
<xs:element name="Policies" type="xs:string" minOccurs="0"/>
<xs:element name="AdditionalInfo" type="xs:string" minOccurs="0"/>
<xs:element name="SpecialNotes" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="RoomTypeTranslationType">
<xs:sequence>
<xs:element name="LanguageId" type="xs:int" minOccurs="1"/>
<xs:element name="RoomType" type="xs:string" minOccurs="1"/>
<xs:element name="RoomTypeDescription" type="xs:string" minOccurs="1"/>
</xs:sequence>
</xs:complexType>
<xs:complexType name="CustomFieldType">
<xs:sequence>
<xs:element name="Id" type="xs:int" minOccurs="0"/>
<xs:element name="Name" type="xs:string" minOccurs="0"/>
<xs:element name="Value" type="xs:string" minOccurs="0"/>
</xs:sequence>
</xs:complexType>
<xs:simpleType name="NonEmptyString">
<xs:restriction base="xs:string">
<xs:minLength value="1"/>
<xs:pattern value=".*[^\s].*"/>
</xs:restriction>
</xs:simpleType>
</xs:schema>
</wsdl:types>
<wsdl:message name="AvailabilityRQ">
<wsdl:part name="parameters" element="tns:AvailabilityRQ"/>
</wsdl:message>
<wsdl:message name="AvailabilityRS">
<wsdl:part name="parameters" element="tns:AvailabilityRS"/>
</wsdl:message>
<wsdl:message name="CreatePnrRQ">
<wsdl:part name="parameters" element="tns:CreatePnrRQ"/>
</wsdl:message>
<wsdl:message name="CreatePnrRS">
<wsdl:part name="parameters" element="tns:CreatePnrRS"/>
</wsdl:message>
<wsdl:message name="CancelPnrRQ">
<wsdl:part name="parameters" element="tns:CancelPnrRQ"/>
</wsdl:message>
<wsdl:message name="CancelPnrRS">
<wsdl:part name="parameters" element="tns:CancelPnrRS"/>
</wsdl:message>
<wsdl:portType name="switchflyportType">
<wsdl:operation name="Availability">
<wsdl:input message="tns:AvailabilityRQ"/>
<wsdl:output message="tns:AvailabilityRS"/>
</wsdl:operation>
<wsdl:operation name="CreatePnr">
<wsdl:input message="tns:CreatePnrRQ"/>
<wsdl:output message="tns:CreatePnrRS"/>
</wsdl:operation>
<wsdl:operation name="CancelPnr">
<wsdl:input message="tns:CancelPnrRQ"/>
<wsdl:output message="tns:CancelPnrRS"/>
</wsdl:operation>
</wsdl:portType>
<wsdl:binding name="switchflyBinding" type="tns:switchflyportType">
<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
<wsdl:operation name="Availability">
<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/switchfly_booking_api.php#Availability" style="document"/>
<wsdl:input>
<soap:body use="literal"/>
</wsdl:input>
<wsdl:output>
<soap:body use="literal"/>
</wsdl:output>
<wsdl:documentation>Availability</wsdl:documentation>
</wsdl:operation>
<wsdl:operation name="CreatePnr">
<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/switchfly_booking_api.php#CreatePnr" style="document"/>
<wsdl:input>
<soap:body use="literal"/>
</wsdl:input>
<wsdl:output>
<soap:body use="literal"/>
</wsdl:output>
<wsdl:documentation>CreatePnr</wsdl:documentation>
</wsdl:operation>
<wsdl:operation name="CancelPnr">
<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/switchfly_booking_api.php#CancelPnr" style="document"/>
<wsdl:input>
<soap:body use="literal"/>
</wsdl:input>
<wsdl:output>
<soap:body use="literal"/>
</wsdl:output>
<wsdl:documentation>CancelPnr</wsdl:documentation>
</wsdl:operation>
</wsdl:binding>
<wsdl:service name="DistantisServices">
<wsdl:port name="switchflyPort" binding="tns:switchflyBinding">
<soap:address location="http://otas.distantis.com/otas/webservice/v1.2/switchfly_booking_api.php"/>
</wsdl:port>
</wsdl:service>
</wsdl:definitions>';
	if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
    if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
    if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
    return $str;
}
?>