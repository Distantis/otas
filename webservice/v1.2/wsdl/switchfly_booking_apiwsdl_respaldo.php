﻿<?php 

if (stristr($_SERVER['QUERY_STRING'], "wsdl")) {
    	// WSDL request - output raw XML
		header("Content-Type: application/soap+xml; charset=utf-8");
        echo DisplayXML();
    } else {
    	// Page accessed normally - output documentation
    	$cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1); // Current page
    	echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
    	echo '<html>';
    	echo '<head><title>' . $serviceName . '</title></head>';
    	echo '<body>';
    	echo '<h1>' . $serviceName . '</h1>';
        echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $cp . '?WSDL</code></p>';
    	
        // Document each function
        echo '<h2>Available Functions:</h2>';
        echo '<div style="margin-left:20px;">';
        for ($i=0;$i<count($functions);$i++) {
            echo '<h3>Function: ' . $functions[$i]['funcName'] . '</h3>';
            echo '<div style="margin-left:20px;">';
            echo '<p>';
            echo $functions[$i]['doc'];
            echo '<ul>';
            if (array_key_exists("inputParams", $functions[$i])) {
            	echo '<li>Input Parameters:<ul>';
            	for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
            	   	echo '<li>' . $functions[$i]['inputParams'][$j]['name'];
            	   	echo ' (' . $functions[$i]['inputParams'][$j]['type'];
            	   	echo ')</li>';
            	}
            	echo '</ul></li>';
            }
            if (array_key_exists("outputParams", $functions[$i])) {
                echo '<li>Output Parameters:<ul>';
                for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
                    echo '<li>' . $functions[$i]['outputParams'][$j]['name'];
                    echo ' (' . $functions[$i]['outputParams'][$j]['type'];
                    echo ')</li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
            echo '</p>';
            echo '</div>';
        }
        echo '</div>';
        	
    	echo '<h2>WSDL output:</h2>';
        echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
        echo DisplayXML(false);
        echo '</pre>';
        echo '</body></html>';
    }
                         
    exit; 
function DisplayXML($xmlformat=true) {
	
	function cabeza(){
		$aux = '<?xml version="1.0" encoding="utf-8" ?>
			<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://www.darkerwhite.com/" targetNamespace="http://www.darkerwhite.com/" name="DistantisServices">
			<wsdl:types>
			<s:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">';
		return $aux;
	}
	function booking_api(){
		$aux = '<s:element name="AvailabilityRQ">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRQTypeAv">
								<s:sequence>
									<s:element name="CreateSession" type="s:boolean" minOccurs="0">
									</s:element>
									<s:choice>
										<s:sequence>
											<s:element name="RoomSearch" type="RoomSearchType" minOccurs="0" maxOccurs="unbounded" />
										</s:sequence>
									</s:choice>
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="AvailabilityRS">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRSType">
								<s:sequence>
									<s:element name="RoomSearchResponse" type="RoomSearchResponseType" minOccurs="0" maxOccurs="unbounded" />
									<s:element name="SessionToken" type="s:string" minOccurs="0">
									</s:element>
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="CancelPnrRQ">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRQType">
								<s:sequence>
									<s:element name="RecordLocator" type="s:string">
									</s:element>
									<s:element name="IsAutoCancel" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="RefundPayment" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="PushPnr" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="SendConfirmation" type="s:boolean" minOccurs="0">
									</s:element>
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="CancelPnrRS">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRSType">
								<s:sequence>
									<s:element name="Pnr" type="PnrType" />
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="CreatePnrRQ">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyPaymentRQType">
								<s:sequence>
									<s:element name="AddToRecordLocator" type="s:string" minOccurs="0">
									</s:element>
									<s:element name="PnrOwner" type="TravelerType">
									</s:element>
									<s:element name="LeadTraveler" type="TravelerType">
									</s:element>
									<s:element name="RoomSegmentRequest" minOccurs="0" maxOccurs="unbounded">
										<s:complexType>
											<s:sequence>
												<s:element name="CorrelationId" type="s:string" minOccurs="0">
												</s:element>
												<s:element name="RoomSegment" type="s:string">
												</s:element>
												<s:element name="CustomFields" minOccurs="0">
													<s:complexType>
														<s:sequence>
															<s:element name="CustomField" type="CustomFieldType" minOccurs="0" maxOccurs="unbounded" />
														</s:sequence>
													</s:complexType>
												</s:element>
												<s:element name="RoomingList" type="GuestListType" minOccurs="0" />
												<s:element name="SpecialRequests" type="s:string" minOccurs="0">
												</s:element>
												<s:element name="RequestedService" type="s:string" minOccurs="0" maxOccurs="unbounded">
												</s:element>
												<s:element name="AgencyCreditCard" type="CreditCardType" minOccurs="0">
												</s:element>
											</s:sequence>
										</s:complexType>
									</s:element>
									<s:element name="BookingHoldDays" type="s:integer" minOccurs="0" />
									<s:element name="BookingMemo" type="s:string" minOccurs="0" />
									<s:element name="ProcessPayment" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="ProcessPaymentCurrency" type="CurrencyCodeType" minOccurs="0">
									</s:element>
									<s:element name="PushPnr" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="SendConfirmation" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="ClientReservation" minOccurs="0" maxOccurs="unbounded">
										<s:complexType>
											<s:sequence>
												<s:element name="Number" type="s:string">
												</s:element>
												<s:element name="LoyaltyNumber" type="s:string" minOccurs="0">
												</s:element>
											</s:sequence>
										</s:complexType>
									</s:element>
									<s:element name="HoldSupplierConfirmation" type="s:boolean" minOccurs="0">
									</s:element>
									<s:element name="DiscountCode" type="s:string" minOccurs="0" maxOccurs="1">
									</s:element>
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="CreatePnrRS">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRSType">
								<s:choice>
									<s:sequence>
										<s:element name="UnavailableBookingComponent" type="s:string" minOccurs="0" maxOccurs="unbounded">
										</s:element>
										<s:element name="PriceChange" minOccurs="0" maxOccurs="unbounded">
											<s:complexType>
												<s:sequence>
													<s:element name="BookingDescriptor" type="s:string" />
													<s:element name="Currency" type="s:string" />
													<s:element name="OriginalPrice" type="s:double" />
													<s:element name="NewPrice" type="s:double" />
												</s:sequence>
											</s:complexType>
										</s:element>
									</s:sequence>
									<s:sequence>
										<s:element name="SpecialMarkup" type="s:double" />
										<s:element name="TicketingDeadline" type="IsoDateType" minOccurs="0" />
										<s:element name="Segment" minOccurs="0" maxOccurs="unbounded">
											<s:complexType>
												<s:sequence>
													<s:element name="CorrelationId" type="s:string" minOccurs="0">
													</s:element>
													<s:element name="SegmentLocator" type="s:string">
													</s:element>
													<s:element name="CrsRecordLocator" type="s:string" minOccurs="0">
													</s:element>
													<s:element name="SupplierLocator" type="s:string" minOccurs="0">
													</s:element>
													<s:element name="Currency" type="s:string" />
													<s:element name="TotalPrice" type="CurrencyAmountType">
													</s:element>
												</s:sequence>
											</s:complexType>
										</s:element>
										<s:element name="PaymentRedirectUrl" type="s:string" minOccurs="0">
										</s:element>
										<s:element name="PaymentRedirectUrlParameters" minOccurs="0">
											<s:complexType>
												<s:sequence>
													<s:element name="Parameter" maxOccurs="unbounded">
														<s:complexType>
															<s:sequence>
																<s:element name="Name" type="s:string" />
																<s:element name="Value" type="s:string" />
															</s:sequence>
														</s:complexType>
													</s:element>
												</s:sequence>
											</s:complexType>
										</s:element>
										<s:element name="PrintUrlForOfflineBankTransfer" type="s:string" minOccurs="0">
										</s:element>
										<s:element name="Pnr" type="PnrType" />
									</s:sequence>
								</s:choice>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="HotelDetailsRQ">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRQType">
								<s:sequence>
									<s:element name="Hotel" type="s:string" />
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>
				<s:element name="HotelDetailsRS">
					<s:complexType>
						<s:complexContent>
							<s:extension base="SwitchflyRSType">
								<s:sequence>
									<s:element name="Hotel" type="HotelType" />
								</s:sequence>
							</s:extension>
						</s:complexContent>
					</s:complexType>
				</s:element>';
		return $aux;
	}
	function booking_objects(){
		$aux = '<s:complexType name="AbstractActivityType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="Name" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="Code" type="s:string" minOccurs="0"/>
                    <s:element name="SupplierName" type="s:string">
                    </s:element>
                    <s:element name="SupplierCode" type="s:string" minOccurs="0"/>
                    <s:element name="LongDescription" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="SupplierPhone" type="s:string" minOccurs="0"/>
                    <s:element name="ActivityAddress" type="AddressType" minOccurs="0"/>
                    <s:element name="IncludedServices" type="s:string" minOccurs="0"/>
                    <s:element name="AdditionalInfoQuestion" type="s:string" minOccurs="0"/>
                    <s:element name="FixedDailyPricing" type="s:boolean" minOccurs="0">
                    </s:element>
                    <s:element name="LabelCountsSetFromTotalTravelers" minOccurs="0">
                    </s:element>
                    <s:element name="VoucherInformation" type="s:string" minOccurs="0"/>
                    <s:element name="SortRanking" type="s:decimal" minOccurs="0"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="ActivityAnyDateEventType">
        <s:complexContent>
            <s:extension base="ActivityEventType">
                <s:sequence>
                    <s:element name="DateRangeBegin" type="IsoDateType"/>
                    <s:element name="DateRangeEnd" type="IsoDateType"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="ActivityEventType">
        <s:sequence>
            <s:element name="BookingDescriptor" type="s:string" minOccurs="0"/>
            <s:element name="AnyTime" type="s:boolean">
            </s:element>
            <s:element name="Available" type="s:int" minOccurs="0">
            </s:element>
            <s:element name="FreeSell" type="s:boolean" minOccurs="0">
            </s:element>
            <s:element name="OnRequest" type="s:boolean" minOccurs="0">
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ActivityFixedDateEventType">
        <s:complexContent>
            <s:extension base="ActivityEventType">
                <s:sequence>
                    <s:element name="Timestamp" type="IsoTimestampType">
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="ActivityLabelType">
        <s:sequence>
            <s:element name="Description" type="s:string" minOccurs="0"/>
            <s:element name="BookingCode" type="s:string" minOccurs="0"/>
            <s:element name="Label" type="s:string"/>
            <s:element name="Count" type="s:int"/>
            <s:element name="GuestList" type="GuestListType" minOccurs="0"/>
            <s:element name="FamilyPlan" type="s:boolean" minOccurs="0"/>
            <s:element name="FamilyPlanMaster" type="s:boolean" minOccurs="0"/>
            <s:element name="Price" type="PriceType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ActivityRateType">
        <s:sequence>
            <s:element name="RateName" type="s:string" minOccurs="0"/>
            <s:element name="RateType" type="ActivityRateApplicabilityType" minOccurs="0"/>
            <s:element name="Label" type="ActivityLabelType" maxOccurs="unbounded"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ActivitySegmentType">
        <s:complexContent>
            <s:extension base="AbstractActivityType">
                <s:sequence>
                    <s:element name="Rate" type="ActivityRateType" minOccurs="0"/>
                    <s:choice>
                        <s:element name="AnyDateEvent" type="ActivityAnyDateEventType"/>
                        <s:element name="FixedDateEvent" type="ActivityFixedDateEventType"/>
                    </s:choice>
                    <s:element name="Price" type="PriceType"/>
                    <s:element name="VoucherUrl" type="s:string" minOccurs="0"/>
                    <s:element name="ActivityCancellationPolicies" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="ActivityCancellationPolicy"
                                    type="ActivityCancellationPolicyType" minOccurs="0"
                                    maxOccurs="unbounded"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="AccountingAdjustmentType">
        <s:sequence>
            <s:element name="EnteredDate" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="AgentName" type="s:string" minOccurs="0"/>
            <s:element name="GlCode" type="s:string" minOccurs="0"/>
            <s:element name="GlMemo" type="s:string" minOccurs="0"/>
            <s:element name="AdjAmountSystemCurrency" type="s:double" minOccurs="0"/>
            <s:element name="CustomerCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="AdjAmountCustomerCurrency" type="s:double" minOccurs="0"/>
            <s:element name="RequiredApproval" type="s:boolean" minOccurs="0"/>
            <s:element name="ApprovingAgentName" type="s:string" minOccurs="0"/>
            <s:element name="IsApproved" type="s:boolean" minOccurs="0"/>
            <s:element name="ApprovalTimestamp" type="IsoTimestampType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AccountsPayableSupplierType">
        <s:sequence>
            <s:element name="SupplierId" type="s:int" minOccurs="0"/>
            <s:element name="SupplierCode" type="s:string" minOccurs="0"/>
            <s:element name="MasterSupplierCode" type="s:string" minOccurs="0"/>
            <s:element name="OriginalAmount" type="s:double" minOccurs="0"/>
            <s:element name="Owe" type="s:double" minOccurs="0"/>
            <s:element name="SupplierCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="SupplierOriginalAmount" type="s:double" minOccurs="0"/>
            <s:element name="SupplierOwe" type="s:double" minOccurs="0"/>
            <s:element name="AirPnrId" type="s:int" minOccurs="0"/>
            <s:element name="RoomId" type="s:int" minOccurs="0"/>
            <s:element name="CarId" type="s:int" minOccurs="0"/>
            <s:element name="ActivityId" type="s:int" minOccurs="0"/>
            <s:element name="CruisePnrId" type="s:int" minOccurs="0"/>
            <s:element name="CruiseSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="InsuranceId" type="s:int" minOccurs="0"/>
            <s:element name="MerchandiseSegmentId" type="s:int" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AccountingType">
        <s:sequence>
            <s:element name="GeneralLedger">
                <s:complexType>
                    <s:sequence>
                        <s:element name="GeneralLedgerDetails" type="GeneralLedgerDetailsType"
                            minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="AccountsPayableSuppliers">
                <s:complexType>
                    <s:sequence>
                        <s:element name="AccountsPayableSupplier"
                            type="AccountsPayableSupplierType" minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="PaymentSuppliers">
                <s:complexType>
                    <s:sequence>
                        <s:element name="PaymentSupplier" type="PaymentSupplierType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="AccountsPayableAgencies">
                <s:complexType>
                    <s:sequence>
                        <s:element name="AccountsPayableAgency" type="AccountsPayableAgencyType"
                            minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="PaymentAgencies">
                <s:complexType>
                    <s:sequence>
                        <s:element name="PaymentAgency" type="PaymentAgencyType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="PaymentReceive">
                <s:complexType>
                    <s:sequence>
                        <s:element name="PaymentReceiveDetails" type="PaymentReceiveDetailsType"
                            minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="AccountingAdjustments">
                <s:complexType>
                    <s:sequence>
                        <s:element name="AccountingAdjustment" type="AccountingAdjustmentType"
                            minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AccountsPayableAgencyType">
        <s:sequence>
            <s:element name="AgencyID" type="s:string" minOccurs="0"/>
            <s:element name="OriginalAmount" type="s:double" minOccurs="0"/>
            <s:element name="Owe" type="s:double" minOccurs="0"/>
            <s:element name="AgencyCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="AgencyOriginalAmount" type="s:double" minOccurs="0"/>
            <s:element name="AgencyOwe" type="s:double" minOccurs="0"/>
            <s:element name="Valid" type="s:boolean" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:simpleType name="ActivityRateApplicabilityType">
        <s:restriction base="s:string">
            <s:enumeration value="activity_only"/>
            <s:enumeration value="addon_only"/>
            <s:enumeration value="activity/addon"/>
        </s:restriction>
    </s:simpleType>

    <s:complexType name="AgencyType">
        <s:sequence>
            <s:element name="AgencyName" type="s:string"/>
            <s:element name="AgencyCode" type="s:string" minOccurs="0"/>
            <s:element name="IATA" type="s:string" minOccurs="0"/>
            <s:element name="ARC" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AgentType">
        <s:sequence>
            <s:element name="FirstName" type="s:string"/>
            <s:element name="LastName" type="s:string"/>
            <s:element name="AgentId" type="s:int"/>
            <s:element name="Agency" type="AgencyType"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AirItineraryType">
        <s:sequence>
            <s:element name="RecordLocator" type="s:string" minOccurs="0"/>
            <s:element name="AirPnrId" type="s:int" minOccurs="0">
            
            </s:element>
            <s:element name="OriginalAirPnrId" type="s:int" minOccurs="0" maxOccurs="1">
            </s:element>
            <s:element name="Price" type="PriceType"/>
            <s:element name="AgeClassPrices" type="AgeClassPricesType"/>
            <s:element name="Traveler" type="TravelerType" maxOccurs="unbounded"/>
            <s:element name="AirLeg" type="AirLegType" maxOccurs="unbounded"/>
            <s:element name="TicketingDeadline" type="IsoDateType" maxOccurs="1" minOccurs="0"/>
            <s:element name="SingleUseCard" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AirLegType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="AirLegId" type="s:int" minOccurs="0">
                    </s:element>
                    <s:element name="Published" type="s:boolean">
                    </s:element>
                    <s:element name="Cabin" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="AirAllianceName" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="BookingDescriptor" type="s:string" minOccurs="0">
                    </s:element>
                    <s:sequence maxOccurs="unbounded">
                        <s:element name="AirSegment" type="AirSegmentType"/>
                        <s:element name="LayoverTime" minOccurs="0">
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="Hours" type="s:int"/>
                                    <s:element name="Minutes" type="s:int"/>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                    </s:sequence>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="AirSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="AirSegmentType" minOccurs="0">
                        <s:simpleType>
                            <s:restriction base="s:string">
                                <s:enumeration value="AIR"/>
                                <s:enumeration value="TRAIN"/>
                                <s:enumeration value="BUS"/>
                            </s:restriction>
                        </s:simpleType>
                    </s:element>
                    <s:element name="DepartureTimestamp" type="IsoTimestampType">
                    </s:element>
                    <s:element name="ArrivalTimestamp" type="IsoTimestampType">
                    </s:element>
                    <s:element name="Duration" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Hours" type="s:int"/>
                                <s:element name="Minutes" type="s:int"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="FromCity" type="s:string">
                    </s:element>
                    <s:element name="ToCity" type="s:string">
                    </s:element>
                    <s:element name="Airline" type="s:string">
                    </s:element>
                    <s:element name="CodeShareAirline" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="Flight" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="FareClass" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="Cabin" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="Plane" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="Stops" type="s:int">
                    </s:element>
                    <s:element name="Tickets" minOccurs="0" maxOccurs="1">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Ticket" minOccurs="1" maxOccurs="unbounded">
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="Number" type="s:string" maxOccurs="1"
                                                minOccurs="1"/>
                                            <s:element name="Traveler" maxOccurs="1" minOccurs="1">
                                                <s:complexType>
                                                  <s:sequence>
                                                  <s:element name="Title" type="s:string"
                                                  maxOccurs="1" minOccurs="0"/>
                                                  <s:element name="FirstName" type="s:string"
                                                  maxOccurs="1" minOccurs="1"/>
                                                  <s:element name="MiddleName" type="s:string"
                                                  maxOccurs="1" minOccurs="0"/>
                                                  <s:element name="LastName" type="s:string"
                                                  maxOccurs="1" minOccurs="1"/>
                                                  </s:sequence>
                                                </s:complexType>
                                            </s:element>
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="BookingFeeSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="ClientFeeType" type="s:string">
                    </s:element>
                    <s:element name="ContractName" type="s:string"/>
                    <s:element name="Price" type="PriceType"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="DiscountSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="DiscountType" type="s:string">
                    </s:element>
                    <s:element name="ContractName" type="s:string" maxOccurs="1" minOccurs="0"/>
                    <s:element name="DiscountCode" type="s:string" maxOccurs="1" minOccurs="0"/>
                    <s:element name="RecordLocator" type="s:string" maxOccurs="1" minOccurs="0"/>
                    <s:element name="ConfirmationCode" type="s:string" maxOccurs="1" minOccurs="0"/>
                    <s:element name="Price" type="PriceType"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="MerchandiseSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="MerchandiseItemName" type="s:string"/>
                    <s:element name="SupplierName" type="s:string"/>
                    <s:element name="SupplierId" type="s:int"/>
                    <s:element name="SupplierCode" type="s:string" minOccurs="0"/>
                    <s:element name="SKU" type="s:string"/>
                    <s:element name="MerchandiseItemQuantity" type="s:int"/>
                    <s:element name="Price" type="PriceType"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="ShippingDetailsType">
        <s:sequence>
            <s:element name="Title" type="s:string" minOccurs="0"/>
            <s:element name="LastName" type="s:string"/>
            <s:element name="FirstName" type="s:string"/>
            <s:element name="Phone" type="s:string"/>
            <s:element name="GiftNote" type="s:string" minOccurs="0"/>
            <s:element name="Address" type="AddressType" minOccurs="0"/>
            <s:element name="ShippingAdditionalInformation" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="BookingMemoType">
        <s:sequence>
            <s:element name="Timestamp" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="Agent" type="s:string" minOccurs="0"/>
            <s:element name="MemoCode" type="s:string" minOccurs="0"/>
            <s:element name="Memo" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="CarLocationType">
        <s:sequence>
            <s:element name="AirportCode" type="s:string" minOccurs="0"/>
            <s:element name="CompanyCode" type="s:string" minOccurs="0"/>
            <s:element name="LocationName" type="s:string" minOccurs="0"/>
            <s:element name="PhoneNumber" type="s:string" minOccurs="0"/>
            <s:element name="VicinityCode" type="s:string" minOccurs="0"/>
            <s:element name="LocationNumber" type="s:int" minOccurs="0"/>
            <s:element name="AtAirport" type="s:boolean"/>
            <s:element name="InTerminal" type="s:boolean"/>
            <s:element name="Address" type="AddressType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="CarSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="BookingDescriptor" type="s:string" minOccurs="0"/>
                    <s:element name="DetailsAvailable" type="s:boolean" minOccurs="0">
                    </s:element>
                    <s:element name="PickupCity" type="s:string" minOccurs="0"/>
                    <s:element name="DropOffCity" type="s:string" minOccurs="0"/>
                    <s:element name="PickupTimestamp" type="IsoTimestampType"/>
                    <s:element name="DropOffTimestamp" type="IsoTimestampType"/>
                    <s:element name="ChainCode" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="CompanyName" type="s:string"/>
                    <s:element name="Approved" type="s:boolean" minOccurs="0">
                    </s:element>
                    <s:element name="CarCode">
                        <s:complexType>
                            <s:simpleContent>
                                <s:extension base="s:string">
                                    <s:attribute name="Class" type="s:string">
                                    </s:attribute>
                                    <s:attribute name="Type" type="s:string">
                                    </s:attribute>
                                    <s:attribute name="Transmission" type="s:string">
                                    </s:attribute>
                                    <s:attribute name="Drive" type="s:string">
                                    </s:attribute>
                                    <s:attribute name="AirConditioning" type="s:string">
                                    </s:attribute>
                                    <s:attribute name="Fuel" type="s:string">
                                    </s:attribute>
                                </s:extension>
                            </s:simpleContent>
                        </s:complexType>
                    </s:element>
                    <s:element name="CarName" type="s:string" minOccurs="0"/>
                    <s:element name="Model" type="s:string" minOccurs="0"/>
                    <s:element name="CarDescription" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="IncludedServices" type="s:string" minOccurs="0"/>
                    <s:element name="RateCode" type="s:string" minOccurs="0"/>
                    <s:element name="RateDescription" type="s:string" minOccurs="0"/>
                    <s:element name="PassengerCapacity" type="s:int" minOccurs="0"/>
                    <s:element name="BaggageCapacity" type="s:int" minOccurs="0"/>
                    <s:element name="LogoUrl" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="PictureUrl" type="s:string" minOccurs="0">
                    </s:element>
                    <s:element name="HasUnlimitedMiles" type="s:boolean" minOccurs="0">
                    </s:element>
                    <s:element name="FreeMilesPerDay" type="s:int" minOccurs="0">
                    </s:element>
                    <s:element name="DistanceUnit" minOccurs="0">
                        <s:simpleType>
                            <s:restriction base="s:string">
                                <s:enumeration value="MILES"/>
                                <s:enumeration value="KILOMETERS"/>
                            </s:restriction>
                        </s:simpleType>
                    </s:element>
                    <s:element name="Price" type="PriceType"/>
                    <s:element name="PickUpLocation" type="CarLocationType"/>
                    <s:element name="DropOffLocation" type="CarLocationType"/>
                    <s:element name="SpecialRequests" type="s:string" minOccurs="0"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="CertificateType">
        <s:sequence>
            <s:element name="CertificateId" type="s:int" minOccurs="0"/>
            <s:element name="Amount" type="s:double" minOccurs="0"/>
            <s:element name="Tracking" type="s:string" minOccurs="0"/>
            <s:element name="Beneficiary" type="s:string" minOccurs="0"/>
            <s:element name="BeginDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="EndDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="Void" type="s:boolean" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ClientSearchLocationTypeV2">
        <s:sequence>
            <s:element name="ExplicitSearchString" type="s:string">
            </s:element>
            <s:choice>
                <s:element name="Airport">
                    <s:complexType>
                        <s:sequence>
                            <s:element name="AirportCode" type="s:string"/>
                            <s:element name="AirportName" type="s:string"/>
                            <s:element name="StateCode" type="s:string" minOccurs="0"/>
                            <s:element name="StateName" type="s:string" minOccurs="0"/>
                            <s:element name="CountryCode" type="s:string"/>
                            <s:element name="CountryName" type="s:string"/>
                            <s:element name="Excluded" type="ClientSearchLocationExcludedType"
                                minOccurs="0"/>
                            <s:element name="AirportCities">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="PrimaryCityId" type="s:int"/>
                                        <s:element name="AlternateCityId" type="s:int"
                                            minOccurs="0"/>
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:complexType>
                </s:element>
                <s:element name="City">
                    <s:complexType>
                        <s:sequence>
                            <s:element name="CityId" type="s:int"/>
                            <s:element name="CityName" type="s:string"/>
                            <s:element name="StateCode" type="s:string" minOccurs="0"/>
                            <s:element name="StateName" type="s:string" minOccurs="0"/>
                            <s:element name="CountryCode" type="s:string"/>
                            <s:element name="CountryName" type="s:string"/>
                            <s:element name="Excluded" type="ClientSearchLocationExcludedType"
                                minOccurs="0"/>
                            <s:element name="CityAirports">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="AirportCode" type="s:string"
                                            maxOccurs="unbounded"/>
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:complexType>
                </s:element>
                <s:element name="Region">
                    <s:complexType>
                        <s:sequence>
                            <s:element name="RegionId" type="s:string"/>
                            <s:element name="RegionName" type="s:string"/>
                            <s:element name="Excluded" type="ClientSearchLocationExcludedType"
                                minOccurs="0"/>
                            <s:element name="RegionCities">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="CityId" type="s:int"
                                            maxOccurs="unbounded"/>
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:complexType>
                </s:element>
            </s:choice>
        </s:sequence>
    </s:complexType>
    <s:simpleType name="LocationType">
        <s:restriction base="s:string">
            <s:enumeration value="AIRPORT"/>
            <s:enumeration value="CITY"/>
            <s:enumeration value="REGION"/>
        </s:restriction>
    </s:simpleType>
    <s:complexType name="ClientSearchLocationExcludedType">
        <s:sequence>
            <s:element name="Air" type="ClientSearchLocationExclusionType" minOccurs="1"/>
            <s:element name="Room" type="ClientSearchLocationExclusionType" minOccurs="1"/>
            <s:element name="Car" type="ClientSearchLocationExclusionType" minOccurs="1"/>
            <s:element name="Activity" type="ClientSearchLocationExclusionType" minOccurs="1"/>
        </s:sequence>
    </s:complexType>
    <s:simpleType name="ClientSearchLocationExclusionType">
        <s:restriction base="s:string">
            <s:enumeration value="NONE"/>
            <s:enumeration value="ORIGIN"/>
            <s:enumeration value="DESTINATION"/>
            <s:enumeration value="BOTH"/>
        </s:restriction>
    </s:simpleType>
    <s:complexType name="CruiseSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="BookingDescriptor" type="s:string" minOccurs="0"/>
                    <s:element name="Departure" type="DepartureType" minOccurs="0"/>
                    <s:element name="Arrival" type="DepartureType" minOccurs="0"/>
                    <s:element name="CruiselineCode" type="s:string"/>
                    <s:element name="CruiseName" type="s:string" minOccurs="0"/>
                    <s:element name="ShipName" type="s:string"/>
                    <s:element name="ShipId" type="s:string"/>
                    <s:element name="CruiseDuration" type="s:string" minOccurs="0"/>
                    <s:element name="CabinNumber" type="s:string" minOccurs="0"/>
                    <s:element name="CabinCategoryLocation" type="s:string" minOccurs="0"/>
                    <s:element name="DinningOption" type="s:string" minOccurs="0"/>
                    <s:element name="CruiseFareCode" type="s:string" minOccurs="0"/>
                    <s:element name="Price" type="PriceType" minOccurs="0"/>
                    <s:element name="Travelers" type="GuestListType" minOccurs="0"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="ConnectorCustomInfoType">
        <s:sequence>
            <s:element name="Key" type="s:string"/>
            <s:element name="Value" type="s:string"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="DepartureType">
        <s:sequence>
            <s:element name="CityName" type="s:string"/>
            <s:element name="CountryCode" type="s:string"/>
            <s:element name="CountryName" type="s:string"/>
            <s:element name="Timestamp" type="s:string"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="GuestListType">
        <s:sequence>
            <s:element name="Guest" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="FirstName" type="s:string" minOccurs="0"/>
                        <s:element name="LastName" type="s:string" minOccurs="0"/>
                        <s:element name="Title" type="s:string" minOccurs="0"/>
                        <s:choice minOccurs="0">
                            <s:element name="AgeInMonths" type="s:int"/>
                            <s:element name="Age" type="s:int"/>
                        </s:choice>
                        <s:element name="AgeClass" minOccurs="0">
                            <s:simpleType>
                                <s:restriction base="s:string">
                                    <s:enumeration value="ADULT"/>
                                    <s:enumeration value="CHILD"/>
                                    <s:enumeration value="SENIOR"/>
                                    <s:enumeration value="INFANT_REQUIRING_SEAT"/>
                                    <s:enumeration value="INFANT_IN_LAP"/>
                                </s:restriction>
                            </s:simpleType>
                        </s:element>
                        <s:element name="OfficialTravelerId" type="s:string" minOccurs="0"/>
                        <s:element name="OfficialTravelerIdLabel" type="s:string" minOccurs="0">
                        </s:element>
                        <s:element name="OfficialTravelerIdCountry" type="s:string" minOccurs="0"/>
                        <s:element name="DateOfBirth" type="IsoDateType" minOccurs="0"/>
                        <s:element name="AirLoyaltyPrograms" minOccurs="0">
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="LoyaltyProgram" minOccurs="0" type="LoyaltyProgramType"
                                        maxOccurs="unbounded"/>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                        <s:element name="GovernmentFinancialId" type="s:string" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="GeneralLedgerDetailsType">
        <s:sequence>
            <s:element name="GeneralLedgerId" type="s:int"/>
            <s:element name="AccountId" type="s:int"/>
            <s:element name="AgentId" type="s:int"/>
            <s:element name="SupplierId" type="s:int"/>
            <s:element name="SupplierCode" type="s:string" minOccurs="0"/>
            <s:element name="Credit" type="s:boolean"/>
            <s:element name="Debit" type="s:boolean"/>
            <s:element name="Memo" type="s:string" minOccurs="0"/>
            <s:element name="DateEntered" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="ActualAmount" type="s:double"/>
            <s:element name="ActualCurrency" type="CurrencyCodeType"/>
            <s:element name="AmountSystemCurrency" type="s:double"/>
            <s:element name="CrsName" type="s:string" minOccurs="0"/>
            <s:element name="Valid" type="s:boolean"/>
            <s:element name="AirSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="RoomSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="CarSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="ActivitySegmentId" type="s:int" minOccurs="0"/>
            <s:element name="CruiseSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="InsuranceSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="ClientBookingFeeSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="DiscountSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="MerchandiseSegmentId" type="s:int" minOccurs="0"/>
            <s:element name="Description" type="s:string" minOccurs="0"/>
            <s:element name="ClientGlCode" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="HistoryType">
        <s:sequence>
            <s:element name="Pnrs">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Pnr" type="PnrType" minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="HotelLandingPromotionType">
        <s:sequence>
            <s:element name="Crs" type="s:string"/>
            <s:element name="HotelCode" type="s:string"/>
            <s:element name="HotelName" type="s:string"/>
            <s:element name="AirportCode" type="s:string"/>
            <s:element name="CobrandName" type="s:string" minOccurs="0"/>
            <s:element name="PromotionName" type="s:string" minOccurs="0"/>
            <s:element name="PromotionType" type="s:string"/>
            <s:element name="Description" type="s:string" minOccurs="0"/>
            <s:element name="StayRequirements" type="s:string" minOccurs="0"/>
            <s:element name="BookDateRequirements" type="s:string" minOccurs="0"/>
            <s:element name="TravelDateRequirements" type="s:string" minOccurs="0"/>
            <s:element name="Inclusions" type="s:string" minOccurs="0"/>
            <s:element name="BookingTips" type="s:string" minOccurs="0"/>
            <s:element name="MetaData" type="s:string" minOccurs="0"/>
            <s:element name="PromotionStartDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="PromotionEndDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="CheckinDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="CheckoutDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="ImageUrl" type="s:string" minOccurs="0"/>
            <s:element name="MoreDetails" type="s:string" minOccurs="0"/>
            <s:element name="TermsAndConditions" type="s:string" minOccurs="0"/>
            <s:element name="PayNights" type="s:int" minOccurs="0"/>
            <s:element name="StayNights" type="s:int" minOccurs="0"/>
            <s:element name="AmountOff" type="s:int" minOccurs="0"/>
            <s:element name="Currency" type="s:string" minOccurs="0"/>
            <s:element name="PercentOff" type="s:int" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="InsuranceSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="ContractName" type="s:string" minOccurs="0"/>
                    <s:element name="Price" type="PriceType"/>
                    <s:element name="BookingDescriptor" type="s:string" minOccurs="0" />
                    <s:element name="Description" type="s:string" minOccurs="0"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="MarketingDealType">
        <s:sequence>
            <s:element name="HotelName" type="s:string"/>
            <s:element name="Currency" type="s:string"/>
            <s:element name="StandaloneAvgNightlyPrice" type="s:double" minOccurs="0"/>
            <s:element name="PackagePrice" type="s:double" minOccurs="0"/>
            <s:element name="StarRating" type="s:double"/>
            <s:element name="Crs" type="s:string"/>
            <s:element name="HotelCode" type="s:string"/>
            <s:element name="CheckinDate" type="IsoDateType"/>
            <s:element name="CheckoutDate" type="IsoDateType"/>
            <s:element name="AirportCode" type="s:string"/>
            <s:element name="DepartureAirportCode" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ParcelPaymentSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="Currency" type="s:string">
                    </s:element>
                    <s:element name="PackagePrice" type="s:double">
                    </s:element>
                    <s:element name="ParcelCount" type="s:int">
                    </s:element>
                    <s:element name="InterestPercent" type="s:double">
                    </s:element>
                    <s:element name="InterestAmount" type="s:double">
                    </s:element>
                    <s:element name="FirstParcelAmount" type="s:double">
                    </s:element>
                    <s:element name="ParcelAmount" type="s:double">
                    </s:element>
                    <s:element name="TransactionTime" type="s:string">
                    </s:element>
                    <s:element name="CC4digits" type="s:string">
                    </s:element>
                    <s:element name="AuthNumber" type="s:string">
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="PaymentCorrectionType">
        <s:sequence>
            <s:element name="DateCorrected" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="CorrectedBefore" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="CorrectedAfter" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="AgentName" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PaymentSupplierType">
        <s:sequence>
            <s:element name="Amount" type="s:double" minOccurs="0"/>
            <s:element name="InvoiceNumber" type="s:string" minOccurs="0"/>
            <s:element name="AdjustAmount" type="s:double" minOccurs="0"/>
            <s:element name="GlCode" type="s:string" minOccurs="0"/>
            <s:element name="GlMemo" type="s:string" minOccurs="0"/>
            <s:element name="SupplierCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="SupplierAmount" type="s:double" minOccurs="0"/>
            <s:element name="SupplierAdjustAmount" type="s:double" minOccurs="0"/>
            <s:element name="SupplierName" type="s:string" minOccurs="0"/>
            <s:element name="CheckNumber" type="s:int" minOccurs="0"/>
            <s:element name="Memo" type="s:string" minOccurs="0"/>
            <s:element name="PaymentMethod" type="s:string" minOccurs="0"/>
            <s:element name="Payee" type="s:string" minOccurs="0"/>
            <s:element name="EnteredDate" type="IsoTimestampType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PaymentAgencyType">
        <s:sequence>
            <s:element name="Amount" type="s:double" minOccurs="0"/>
            <s:element name="AgencyCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="AgencyAmount" type="s:double" minOccurs="0"/>
            <s:element name="SupplierName" type="s:string" minOccurs="0"/>
            <s:element name="CheckNumber" type="s:int" minOccurs="0"/>
            <s:element name="Payee" type="s:string" minOccurs="0"/>
            <s:element name="AcountingId" type="s:int" minOccurs="0"/>
            <s:element name="Memo" type="s:string" minOccurs="0"/>
            <s:element name="PaymentMethod" type="s:string" minOccurs="0"/>
            <s:element name="InvoiceNumber" type="s:string" minOccurs="0"/>
            <s:element name="EnteredDate" type="IsoTimestampType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PaymentReceiveDetailsType">
        <s:sequence>
            <s:element name="EnteredDate" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="Payor" type="s:string" minOccurs="0"/>
            <s:element name="GovernmentId" type="s:string" minOccurs="0">
            </s:element>
            <s:element name="SendGovernmentId" type="s:boolean" minOccurs="0">
            </s:element>
            <s:element name="Address" type="AddressType" minOccurs="0"/>
            <s:element name="Phone" type="s:string" minOccurs="0"/>
            <s:element name="Fax" type="s:string" minOccurs="0"/>
            <s:element name="Email" type="s:string" minOccurs="0"/>
            <s:element name="Agency" type="s:string" minOccurs="0"/>
            <s:element name="PaymentMethod" type="s:string" minOccurs="0"/>
            <s:element name="CustomerCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="CustomerAmountReceivedSystemCurrency" type="s:double" minOccurs="0"/>
            <s:element name="CustomerAmountReceivedCustomerCurrency" type="s:double" minOccurs="0"/>
            <s:element name="Memo" type="s:string" minOccurs="0"/>
            <s:element name="AgentName" type="s:string" minOccurs="0"/>
            <s:element name="CreditCardAuthorizationCode" type="s:string" minOccurs="0"/>
            <s:element name="CertificateId" type="s:string" minOccurs="0"/>
            <s:element name="CreditCardTransId" type="s:string" minOccurs="0"/>
            <s:element name="GlCode" type="s:string" minOccurs="0"/>
            <s:element name="GlMemo" type="s:string" minOccurs="0"/>
            <s:element name="MiscPaymentAdditionalData" type="MiscPaymentAdditionalDataType"
                minOccurs="0"/>
            <s:element name="PaymentTotalPoints" type="s:int" minOccurs="0"/>
            <s:element name="PaymentCorrections" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="PaymentCorrection" type="PaymentCorrectionType"
                            minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PaymentType">
        <s:sequence>
            <s:element name="PendingPaymentStatus" minOccurs="0">
                <s:simpleType>
                    <s:restriction base="s:string">
                        <s:enumeration value="PENDING"/>
                        <s:enumeration value="FAILURE"/>
                    </s:restriction>
                </s:simpleType>
            </s:element>
            <s:element name="Timestamp" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="Method" type="s:string" minOccurs="0"/>
            <s:element name="BankCode" type="s:string" minOccurs="0"/>
            <s:element name="CreditCardNumber" type="s:string" minOccurs="0"/>
            <s:element name="LastCCDigits" type="s:string" minOccurs="0"/>
            <s:element name="AuthNumber" type="s:string" minOccurs="0"/>
            <s:element name="TransactionID" type="s:string" minOccurs="0"/>
            <s:element name="AcquirerId" type="s:string" minOccurs="0"/>
            <s:element name="AuxiliaryNumber" type="s:string" minOccurs="0"/>
            <s:element name="CustomerCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="CustomerAmount" type="s:double" minOccurs="0"/>
            <s:element name="SystemCurrency" type="CurrencyCodeType" minOccurs="0"/>
            <s:element name="SystemAmount" type="s:double" minOccurs="0"/>
            <s:element name="AddressVerificationStatus" type="s:string" minOccurs="0"/>
            <s:element name="SecurityCodeVerificationStatus" type="s:string" minOccurs="0"/>
            <s:element name="GlMemo" type="s:string" minOccurs="0"/>
            <s:element name="MiscPaymentAdditionalData" type="MiscPaymentAdditionalDataType"
                minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="MiscPaymentAdditionalDataType">
        <s:sequence>
            <s:element name="MiscPaymentType" type="s:string" minOccurs="0"/>
            <s:element name="GovernmentFinancialId" type="s:string" minOccurs="0"/>
            <s:element name="DebitCardBrand" type="s:string" minOccurs="0"/>
            <s:element name="LastFourDigits" type="s:string" minOccurs="0"/>
            <s:element name="NumberOfInstallment" type="s:string" minOccurs="0"/>
            <s:element name="AuthorizationNumber" type="s:string" minOccurs="0"/>
            <s:element name="AuthorizationNus" type="s:string" minOccurs="0"/>
            <s:element name="PayerInformation" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="FirstName" type="s:string" minOccurs="0"/>
                        <s:element name="LastName" type="s:string" minOccurs="0"/>
                        <s:element name="Address" type="AddressType" minOccurs="0"/>
                        <s:element name="Email" type="s:string" minOccurs="0"/>
                        <s:element name="PhoneNumber" type="s:string" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="Bank" type="s:string" minOccurs="0"/>
            <s:element name="AgencyBankCode" type="s:string" minOccurs="0"/>
            <s:element name="CheckNumber" type="s:string" minOccurs="0"/>
            <s:element name="Account" type="s:string" minOccurs="0"/>
            <s:element name="BarCode" type="s:string" minOccurs="0"/>
            <s:element name="ExpirationTime" type="s:string" minOccurs="0"/>
            <s:element name="RegistrationNumber" type="s:string" minOccurs="0"/>
            <s:element name="LoyaltyElectronicSign" type="s:string" minOccurs="0"/>
            <s:element name="LoyaltyMembershipNumber" type="s:string" minOccurs="0"/>
            <s:element name="LoyaltyPoints" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PnrType">
        <s:sequence>
            <s:element name="RecordLocator" type="s:string"/>
            <s:element name="ClientTracking" type="s:string" minOccurs="0">
            </s:element>
            <s:element name="ClientSubTrack" type="s:string" minOccurs="0">
            </s:element>
            <s:element name="Cobrand" type="s:string" minOccurs="0"/>
            <s:element name="LanguageId" type="s:int" minOccurs="0">
            </s:element>
            <s:element name="BookingTimestamp" type="IsoTimestampType"/>
            <s:element name="AutoCancelDate" type="IsoDateType" minOccurs="0" maxOccurs="1"/>
            <s:element name="Status" type="s:string"/>
            <s:element name="MastercardVoucher" type="s:string" minOccurs="0" maxOccurs="1"/>
            <s:element name="FraudScreen" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Service" type="s:string"/>
                        <s:element name="Status" type="FraudScreenStatusType"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="ScheduleChangeStatus" type="s:string" minOccurs="0"/>
            <s:element name="ClientReservation" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Number" type="s:string">
                        </s:element>
                        <s:element name="LoyaltyNumber" type="s:string" minOccurs="0">
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="SegmentTotals">
                <s:complexType>
                    <s:sequence>
                        <s:element name="AirTotal" type="s:double"/>
                        <s:element name="AirTaxTotal" type="s:double"/>
                        <s:element name="RoomTotal" type="s:double"/>
                        <s:element name="RoomTaxTotal" type="s:double"/>
                        <s:element name="CarTotal" type="s:double"/>
                        <s:element name="CarTaxTotal" type="s:double"/>
                        <s:element name="ActivityTotal" type="s:double"/>
                        <s:element name="ActivityTaxTotal" type="s:double"/>
                        <s:element name="CruiseTotal" type="s:double"/>
                        <s:element name="CruiseTaxTotal" type="s:double"/>
                        <s:element name="InsuranceTotal" type="s:double"/>
                        <s:element name="InsuranceTaxTotal" type="s:double"/>
                        <s:element name="MerchandiseTotal" type="s:double"/>
                        <s:element name="MerchandiseTaxTotal" type="s:double"/>
                        <s:element name="PassThroughMarkupTotal" type="s:double"/>
                        <s:element name="BookingFeeClient" type="s:double"/>
                        <s:element name="BookingFeeTotal" type="s:double"/>
                        <s:element name="ServiceChargeTotal" type="s:double"/>
                        <s:element name="AgentMarkup" type="s:double"/>
                        <s:element name="AgentMarkupCommission" type="s:double"/>
                        <s:element name="DiscountTotal" type="s:double"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="CancelDate" type="IsoTimestampType" minOccurs="0"/>
            <s:element name="DepartureDate" type="IsoTimestampType"/>
            <s:element name="DepartureAirPortCode" type="s:string" minOccurs="0"/>
            <s:element name="DestinationAirPortCode" type="s:string" minOccurs="0"/>
            <s:element name="Agent" type="AgentType" minOccurs="0"/>
            <s:element name="CallinAgent" type="AgentType" minOccurs="0"/>
            <s:element name="PnrOwner" type="TravelerType"/>
            <s:element name="LeadTraveler" type="TravelerType"/>
            <s:element name="Subscribed" type="s:boolean" minOccurs="0"/>
            <s:element name="PassiveCrs" type="s:string" minOccurs="0">
            </s:element>
            <s:element name="BookingMemos" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="BookingMemo" type="BookingMemoType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="Certificates" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Certificate" type="CertificateType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="AirItinerary" type="AirItineraryType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="RoomSegment" type="RoomSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="CarSegment" type="CarSegmentType" minOccurs="0" maxOccurs="unbounded"/>
            <s:element name="ActivitySegment" type="ActivitySegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="CruiseSegment" type="CruiseSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="ParcelPaymentSegment" type="ParcelPaymentSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="InsuranceSegment" type="InsuranceSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="BookingFeeSegment" type="BookingFeeSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="DiscountSegment" type="DiscountSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="MerchandiseSegment" type="MerchandiseSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="ShippingDetails" type="ShippingDetailsType" minOccurs="0"
                maxOccurs="1"/>
            <s:element name="SystemPricingSummary" minOccurs="1">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Currency" type="CurrencyCodeType"/>
                        <s:element name="PackageTotal" type="s:double"/>
                        <s:element name="CommissionTotal" type="s:double"/>
                        <s:element name="CommissionPaid" type="s:double"/>
                        <s:element name="PaymentTotal" type="s:double"/>
                        <s:element name="PaymentTotalPoints" type="s:int"/>
                        <s:element name="BalanceDueTotal" type="s:double"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="CustomerPricingSummary" minOccurs="1">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Currency" type="CurrencyCodeType"/>
                        <s:element name="PackageTotal" type="s:double"/>
                        <s:element name="PaymentTotal" type="s:double"/>
                        <s:element name="BalanceDueTotal" type="s:double"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="Payments" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Payment" type="PaymentType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="Redemption" minOccurs="0">
                <s:annotation>
                    <s:documentation>If this booking specified a redemption contract, this node defines the details of the redemption.
                    </s:documentation>
                </s:annotation>
                <s:complexType>
                    <s:sequence>
                        <s:element name="ConfirmationNumber" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>This is the confirmation of the points redemption system that was used to make this payment.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="RefundConfirmationNumber" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>This is the Refund Confirmation Number
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SupplierLocator" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>This is the supplier locator if the redemption system used a third party supplier.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ContractName" type="s:string">
                            <s:annotation>
                                <s:documentation>The name of the Switchfly redemption contract that was used to calculate the points values.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ExternalProfileId" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>This is the id of the user in the points redemption system whose points were used for this payment.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ExternalProfileFirstName" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The first name of the user as known in the points redemption system.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ExternalProfileLastName" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The last name of the user as known in the points redemption system.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="TotalPointsEarned" type="s:int" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The total points earned by the booking. This may be less than the sum of the points earned by each
                                    traveler if some of the travelers are not loyalty members.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="HouseholdAccountIndicator" type="s:boolean" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Indicates whether the loyalty member is part of a household account or not.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="Campaign" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Campaign applied if the member matched an entry in the client supplied audience file.
                                </s:documentation>
                            </s:annotation>
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="Code" type="s:string">
                                        <s:annotation>
                                            <s:documentation>Campaign code</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="Cell" type="s:string">
                                        <s:annotation>
                                            <s:documentation>Campaign cell code.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="HotelPercentage" type="s:double"
                                        minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="CarPercentage" type="s:double" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="AirPercentage" type="s:double" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="BookingFeePercentage" type="s:double"
                                        minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="ActivityPercentage" type="s:double"
                                        minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="InsurancePercentage" type="s:double"
                                        minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The percent adjustment applied.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                        <s:element name="RedemptionDetails" minOccurs="0" maxOccurs="unbounded">
                            <s:annotation>
                                <s:documentation>Points in a payment can be allocated among various components in the booking. This section is
                                    repeated for each component to which points were applied.
                                </s:documentation>
                            </s:annotation>
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="Product" type="ProductType">
                                        <s:annotation>
                                            <s:documentation>The type of product to which the points were allocations (air, room, car, activity,
                                                insurance, cruise, booking_fee)
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="SegmentId" type="s:int">
                                        <s:annotation>
                                            <s:documentation>The id of the segment or itinerary to which this point allocation applies. The products
                                                themselves are described in a different branch of the XML tree and this id ties this point allocation
                                                to the specific product.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="ChildSegmentId" type="s:int" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>
                                                For example, if SegmentId was an air pnr then this would be the air leg.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="RuleName" type="s:string" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The rule name that was used for points calculation. If no rule matched, this will be
                                                omitted.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="Points" type="s:int">
                                        <s:annotation>
                                            <s:documentation>The number of points allocated.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="Currency" type="s:string">
                                        <s:annotation>
                                            <s:documentation>The currency in which the cash equivalent of points is expressed.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="Cash" type="s:double">
                                        <s:annotation>
                                            <s:documentation>The amount of cash that was paid as part of this redemption.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="AwardDesignator" type="s:string"
                                        minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>The award designator assigned to the selected tier in the rule.
                                            </s:documentation>
                                        </s:annotation>
                                    </s:element>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                        <s:element name="LoyaltyEarnTravelers" minOccurs="0">
                            <s:annotation>
                                <s:documentation>All eligible travelers that have earned points on this booking
                                </s:documentation>
                            </s:annotation>
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="LoyaltyEarnTraveler" maxOccurs="unbounded">
                                        <s:complexType>
                                            <s:sequence>
                                                <s:element name="FirstName" type="s:string">
                                                  <s:annotation>
                                                  <s:documentation>Traveler first name</s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="MiddleName" type="s:string"
                                                  minOccurs="0">
                                                  <s:annotation>
                                                  <s:documentation>Traveler middle name</s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="LastName" type="s:string">
                                                  <s:annotation>
                                                  <s:documentation>Traveler last name</s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="LoyaltyMemberId" type="s:string">
                                                  <s:annotation>
                                                  <s:documentation>Unique member ID</s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="PointsEarned" type="s:int">
                                                  <s:annotation>
                                                  <s:documentation>Points that the traveler earned on this booking
                                                        </s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="IsPosted" type="s:boolean">
                                                  <s:annotation>
                                                  <s:documentation>Earned points have been posted to the external points service
                                                        </s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                                <s:element name="PostedTimestamp"
                                                  type="IsoTimestampType" minOccurs="0">
                                                  <s:annotation>
                                                  <s:documentation>Date that the earned points were posted
                                                        </s:documentation>
                                                  </s:annotation>
                                                </s:element>
                                            </s:sequence>
                                        </s:complexType>
                                    </s:element>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="ExternalDebitAccount" minOccurs="0">
                <s:annotation>
                    <s:documentation>If this booking used an external debit account, this node defines the details of the redemption or refund.
                    </s:documentation>
                </s:annotation>
                <s:complexType>
                    <s:sequence>
                        <s:element name="ExternalDebit" minOccurs="1" maxOccurs="2">
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="Timestamp" type="IsoTimestampType" minOccurs="1"/>
                                    <s:element name="RedeemMemberIdentificationNumber" type="s:string" minOccurs="1"/>
                                    <s:element name="Points" type="s:double" minOccurs="1"/>
                                    <s:element name="CustomerCurrency" type="CurrencyCodeType" minOccurs="1"/>
                                    <s:element name="CustomerAmount" type="s:double" minOccurs="1"/>
                                    <s:element name="SystemCurrency" type="CurrencyCodeType" minOccurs="1"/>
                                    <s:element name="SystemAmount" type="s:double" minOccurs="1"/>
                                    <s:element name="Cobrand" type="s:string" minOccurs="1"/>
                                    <s:element name="ExchangeRate" type="s:double" minOccurs="1"/>
                                    <s:element name="ConfirmationCode" type="s:string" minOccurs="1"/>
                                    <s:element name="Refund" type="s:boolean" minOccurs="1"/>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="History" type="HistoryType" minOccurs="0"/>
            <s:element name="Accounting" type="AccountingType" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PnrElementType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:sequence>
            <s:element name="CorrelationId" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>If present, the correlation id is an echo of a correlation id passed in the request.
                    </s:documentation>
                </s:annotation>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AgeClassPricesType">
        <s:annotation>
            <s:documentation>Prices being splitted by AgeClass.
            </s:documentation>
        </s:annotation>
        <s:sequence>
            <s:element name="Price" minOccurs="1" maxOccurs="unbounded">
                <s:complexType>
                    <s:complexContent>
                        <s:extension base="PriceType">
                            <s:sequence>
                                <s:element name="AgeClass" type="AgeClassType">
                                    <s:annotation>
                                        <s:documentation>Specifies that current price explains fares and taxes amounts for a given age range. We are
                                            not taking the values of that range. Instead we are populating the names of that ranges e.g. CHILD, ADULT,
                                            INFANT, etc.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="Count" type="s:integer">
                                    <s:annotation>
                                        <s:documentation>The number of people for an AgeClass. This field must be present when AgeClass has been
                                            populated.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                            </s:sequence>
                        </s:extension>
                    </s:complexContent>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PriceType">
        <s:sequence>
            <s:element name="Total" type="s:double">
                <s:annotation>
                    <s:documentation>The total price, including taxes. This amount includes taxes if they were available from the supplier of the
                        product. I.e., if the CRS to which Switchfly connected included taxes in its response, then Switchfly passes those on. There
                        can be the following conditions 1. taxes are known and broken out. TaxTotal and TaxDetails tags will be included. 2. Taxes are
                        included in the price, the tax portion of the amount is known, but information about the kinds of taxes in not available.
                        TaxTotal will be non-zero, but TaxDetails will be omitted. 3. The price includes taxes, but the distribution of the price
                        between taxes and net is not known. TaxDetails will be zero. 4. Taxes are not included in the price. The tag
                        IncludesTaxesAndExtraCharges is false.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="TaxTotal" type="s:double" minOccurs="0">
                <s:annotation>
                    <s:documentation>The amount of the total that is taxes</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="SupplierCurrency" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>The currency of the supplier</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="TaxDetails" type="TaxDetailsType" minOccurs="0"/>
            <s:element name="Commission" type="s:double" minOccurs="0">
                <s:annotation>
                    <s:documentation>The amount of commission calculated.</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="CrsCommission" type="s:double" minOccurs="0"/>
            <s:element name="CostOfGoods" type="s:double" minOccurs="0">
                <s:annotation>
                    <s:documentation>The cost of goods. I.e, the cost of this product before markup. This is only sent for tightly coupled systems
                        and its inclusion is controlled by an Switchfly configuration.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="IncludesTaxesAndExtraCharges" type="s:boolean">
                <s:annotation>
                    <s:documentation>A boolean that indicates that the total includes all fees. If false, there may be some unknown taxes or other
                        charges not included in the total. This is typical in car rentals where local taxes are often not included in the quote.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="Currency" type="s:string">
                <s:annotation>
                    <s:documentation>Three letter currency code. Prices are quoted relative to the system providing the quote, so the currency will
                        be the system currency of the system generating this response.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="PassThrough" type="s:boolean">
                <s:annotation>
                    <s:documentation>This is true if the payment for this product will be "passed through" to the provider. For example, when selling
                        a published hotel room, the client credit card is collected but not charged. Rather, it is passed to the hotel, which charges
                        the customer on checkout and later sends a commission check. For "post-pay" products such as these, there is no way to collect
                        payment up front.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="SupplierSoldAtRounding" type="s:double" minOccurs="0">
                <s:annotation>
                    <s:documentation>The amount that the price was rounded to get to the nearest required decimal point. This is in supplier
                        currency.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="ClientAccountCode" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>The accounting code for either a segment by CRS in the case of activity/room/car, or the account code by fee
                        type (Booking, modification, etc...) and CRS in the case of client booking fees.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="RedemptionPrice" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="ContractName" type="s:string">
                            <s:annotation>
                                <s:documentation>The name of the contract used to calculate the points.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="RuleName" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The name of the rule within the specific contract that was activated, if any.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="MaxPoints" type="s:int">
                            <s:annotation>
                                <s:documentation>The number of points calculated that can be used in purchasing the product. Note that this is not
                                    necessarily the number of points that were redeemed, which is recorded in the redemptions section of a PNR. These
                                    points might not cover the full price of the product if, for example, points cannot be used towards taxes.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="Currency" type="s:string">
                            <s:annotation>
                                <s:documentation>The currency in which the MaxPointsValue is expressed.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="MaxPointsValue" type="s:double">
                            <s:annotation>
                                <s:documentation>This is the cash value of the TotalPoints.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="AwardDesignator" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The award designator assigned to the selected tier in the rule.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="LoyaltyPromotionContractName" type="s:string"
                            minOccurs="0">
                            <s:annotation>
                                <s:documentation>The promotion contract name applied to the selected segment.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="LoyaltyPromotionPoints" type="s:integer" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The promotion contract points applied to the selected segment.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="SystemCurrencyPrice" minOccurs="0">
                <s:annotation>
                    <s:documentation>This tag is present only when this price is part of a PushPnrRQ, ReadPnrRS or CreatPnrRS. It is used to present
                        the price in the system currency in addition to the default customer currency.
                    </s:documentation>
                </s:annotation>
                <s:complexType>
                    <s:sequence>
                        <s:element name="Total" type="s:double"/>
                        <s:element name="TaxTotal" type="s:double"/>
                        <s:element name="TaxDetails" type="TaxDetailsType"/>
                        <s:element name="Commission" type="s:double" minOccurs="0"/>
                        <s:element name="CrsCommission" type="s:double" minOccurs="0"/>
                        <s:element name="CostOfGoods" type="s:double" minOccurs="0"/>
                        <s:element name="Currency" type="s:string"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="RoomCancellationPolicyType">
        <s:sequence>
            <s:element name="Description" type="s:string">
                <s:annotation>
                    <s:documentation>A textual description of the cancellation policy.</s:documentation>
                </s:annotation>
            </s:element>
            <s:sequence>
                <s:element name="DeadlineTimestamp" minOccurs="0">
                    <s:annotation>
                        <s:documentation>The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for
                            the reserved hotel.
                        </s:documentation>
                    </s:annotation>
                </s:element>
                <s:choice>
                    <s:annotation>
                        <s:documentation>Cancellation policies can be specified in one of several ways.
                        </s:documentation>
                    </s:annotation>
                    <s:element name="Percentage" type="s:double">
                        <s:annotation>
                            <s:documentation>This indicates that the cancellation penalty is a percentage of the total booking.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="NumberOfNights" type="s:int">
                        <s:annotation>
                            <s:documentation>This indicates the number of nights that cancellation will charge the specified number of nights.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="FlatFee">
                        <s:annotation>
                            <s:documentation>This indicates that a flat fee will be charged in the specified currency.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:simpleContent>
                                <s:extension base="s:double">
                                    <s:attribute name="Currency" type="s:string"/>
                                </s:extension>
                            </s:simpleContent>
                        </s:complexType>
                    </s:element>
                </s:choice>
            </s:sequence>
            <s:element name="CalculatedFeeAmount" minOccurs="0">
                <s:annotation>
                    <s:documentation>This indicates the calculated fee amount in customer currency.
                    </s:documentation>
                </s:annotation>
                <s:complexType>
                    <s:simpleContent>
                        <s:extension base="s:double">
                            <s:attribute name="Currency" type="s:string"/>
                        </s:extension>
                    </s:simpleContent>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="RoomPriceType">
        <s:complexContent>
            <s:extension base="PriceType">
                <s:sequence>
                    <s:element name="DailyTotal" type="s:double" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>An element exists for each day of the room stay. The sum of the DailyTotal elements equals the Total
                                element.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="DailyTotalNoTax" type="s:double" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>A parallel array to the DailyTotal array. This holds the totals without tax, in case that kind of
                                display to the customer is desired.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ExchangeRateSnapshotId" type="s:int" minOccurs="0"/>
                    <s:element name="SupplierToCustomerExchangeRate" type="s:double" minOccurs="0"
                    />
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="RoomSegmentType">
        <s:complexContent>
            <s:extension base="SegmentType">
                <s:sequence>
                    <s:element name="BookingDescriptor" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The booking descriptor is present when part of an availability request. It is not included when reading
                                a segment after booking.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Checkin" type="IsoTimestampType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is the same checkin as specified in the hotel. It is replicated here because after booking hotels
                                are no longer retained and the segment must hold complete information.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Checkout" type="IsoTimestampType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is the same checkout as specified in the hotel. It is replicated here because after booking hotels
                                are no longer retained and the segment must hold complete information.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Nights" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Duration of stay (Difference between Checkin and Checkout dates)</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Adults" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The number of adults associated with this room segment.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Children" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The number of children associated with this room segment.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Infants" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The number of infants in seat associated with this room segment.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="PackageName" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If this room is part of an Switchfly package, this field holds the name of that package.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ChainCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The two letter code for the chain offering the room.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="BrandCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The two letter code for the brand offering the room.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The CRS assigned code for the hotel. If specified only hotels with this hotel code will be returned. A
                                collection of hotel codes can be specified using a comma separated list.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Ql2Code" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Not usable without prior configuration by Switchfly support.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelName" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The name of the hotel that contains this room.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelRating" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The rating of the hotel that contains this room</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelStarRatingDescription" type="s:string" minOccurs="0"/>
                    <s:element name="Address" type="AddressType" minOccurs="0"/>
                    <s:element name="CustomerSupportPhone" type="s:string" minOccurs="0"/>
                    <s:element name="HotelPhone" type="s:string" minOccurs="0"/>
                    <s:element name="HotelFax" type="s:string" minOccurs="0"/>
                    <s:element name="HotelEmail" type="s:string" minOccurs="0"/>
                    <s:element name="RoomName" type="s:string" minOccurs="0"/>
                    <s:element name="RoomDescription" type="s:string" minOccurs="0"/>
                    <s:element name="RateName" type="s:string" minOccurs="0"/>
                    <s:element name="RateMemo" type="s:string" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>After booking, memos might be added to a room segment. These are those memos.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AdditionalRoomDisplayInfo" type="s:string" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>A list of supplementary information, such as promotional text, that is to be displayed with the room
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AdditionalHotelDisplayInfo" type="s:string" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>A list of supplementary information, such as promotional text, that is to be displayed with the hotel
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="IncludedService" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>An optional list of services included with this room. These are unique to each room in the hotel. If
                                there are hotel wide services, they will be repeated in each room segment.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Code" type="s:string"/>
                                <s:element name="Name" type="s:string"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="AvailableRequestService" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>This field holds a collection of services that can be requested for this room. Each service has a code,
                                used to request the service, and a descriptive name.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Code" type="s:string"/>
                                <s:element name="Name" type="s:string"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="RequestedService" type="s:string" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>This field can be used to specify a desired service using the code specified in an
                                AvailableRequestService.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SpecialRequests" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A string that is passed to the hotel during booking for any special requests. Such requests are not
                                guaranteed.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="VoucherInformation" type="s:string" minOccurs="0"/>
                    <s:element name="RoomPrice" type="RoomPriceType"/>
                    <s:element name="StrikethroughPrice" type="RoomPriceType" minOccurs="0"/>
                    <s:element name="ChildAge" type="s:int" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>This specifies the ages of the children associated with this room segment. If present, the number of
                                ChildAge elements should match the value found in the Children element.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="RateCode" type="s:string" minOccurs="0"/>
                    <s:element name="RoomCode" type="s:string" minOccurs="0"/>
                    <s:element name="DetailsAvailable" type="s:boolean" minOccurs="0">
                        <s:annotation>
                            <s:documentation>True if additional details about this room are available. These details can sometimes affect pricing.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SupplierConfirmationSent" type="s:boolean" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If true, supplier confirmation was sent.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="MealType" type="s:string" minOccurs="0"/>
                    <s:element name="RoomTypeId" type="s:integer" minOccurs="0"/>
                    <s:element name="RoomContractId" type="s:integer" minOccurs="0"/>
                    <s:element name="FeeDueAtHotelList" type="FeeDueAtHotelListType" minOccurs="0"
                        maxOccurs="1">
                        <s:annotation>
                            <s:documentation>Mandatory fees charged by the hotel property that is due at the time of checkin or checkout.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="CustomFields" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Custom fields for the contract</s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="CustomField" type="CustomFieldType" minOccurs="0"
                                    maxOccurs="unbounded"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="RoomingList" type="GuestListType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>When booking, this holds the list of traveler names.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Promotion" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>
                                Promotional messages that can be added to the room during availability
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="IsPrimary" type="s:boolean"/>
                                <s:element name="Message" type="s:string" minOccurs="0"/>
                                <s:element name="LongPrimaryMessage" type="s:string" minOccurs="0"/>
                                <s:element name="IsClientExclusive" type="s:boolean" minOccurs="0"/>
                                <s:element name="PromoType" type="s:string" minOccurs="0"/>
                                <s:element name="TermsAndConditions" type="s:string" minOccurs="0"
                                />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="RoomCancellationPolicies" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="RoomCancellationPolicy"
                                    type="RoomCancellationPolicyType" minOccurs="0"
                                    maxOccurs="unbounded"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="NonRefundable" type="s:boolean" default="false" minOccurs="0">
                        <s:annotation>
                            <s:documentation>True if room is non refundable, false otherwise
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="RoomSegmentUpdateType">
        <s:sequence>
            <s:element name="SegmentLocator" type="s:string"/>
            <s:element name="RequestedService" type="s:string" minOccurs="0" maxOccurs="unbounded">
                <s:annotation>
                    <s:documentation>A comma separated list of service codes.</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="SpecialRequests" type="s:string" minOccurs="0"/>
            <s:element name="Checkin" type="IsoDateType" minOccurs="0"/>
            <s:element name="Checkout" type="IsoDateType" minOccurs="0"/>
            <s:element name="RateCode" type="s:string" minOccurs="0"/>
            <s:element name="RoomCode" type="s:string" minOccurs="0"/>
            <s:element name="RoomName" type="s:string" minOccurs="0"/>
            <s:element name="RoomingList" type="GuestListType" minOccurs="0"/>
            <s:element name="RoomModificationType">
                <s:annotation>
                    <s:documentation>one of: DATES, PAX_NAMES, PAX_AGES, PAX_ADD, PAX_DELETE, ROOM_TYPE, SPECIAL_REQUESTS, REQUESTED_SERVICES
                    </s:documentation>
                </s:annotation>
                <s:simpleType>
                    <s:restriction base="s:string">
                        <s:enumeration value="DATES"/>
                        <s:enumeration value="PAX_NAMES"/>
                        <s:enumeration value="PAX_AGES"/>
                        <s:enumeration value="PAX_ADD"/>
                        <s:enumeration value="PAX_DELETE"/>
                        <s:enumeration value="ROOM_TYPE"/>
                        <s:enumeration value="SPECIAL_REQUESTS"/>
                        <s:enumeration value="REQUESTED_SERVICES"/>
                    </s:restriction>
                </s:simpleType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="SegmentType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:complexContent>
            <s:extension base="PnrElementType">
                <s:sequence>
                    <s:element name="BookingComponent" type="BookingComponentType" minOccurs="0"
                        maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>Specifies the components included in package. The value must be one of: Activity,Air, Car, Cruise, or
                                Room.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SegmentId" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A unique identifier for a segment of this type. I.e., no two room segments will have the same SegmentId,
                                although a room segment and a car segment might have the same segment id.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="OriginalSegmentId" type="s:int" minOccurs="0" maxOccurs="1">
                        <s:annotation>
                            <s:documentation>An id that identifies the original segment of this type. The presence of this field indicates that this is a historical version of the object.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SwitchflyBookingNumber" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The Switchfly booking number identifies the booking in Switchfly that contains this segment.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SegmentLocator" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If the segment represents a booked segment, this overrides an identifier for that segment within
                                Switchfly. This allows the segment to be identified in subsequent modification messages. A segment locator is not
                                always available.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="CrsName" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The name of the CRS associated with this segment. The names are an internal Switchfly encoding.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="CrsRecordLocator" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The is record locator used in the crs where Switchfly made the booking. It represents the booking and
                                would be used to read or cancel the booking. In the case of single CRS, this is the key identifier for the PNR. In the
                                case where the booking was made through an intermediary CRS (such as a GDS), this is the GDS record locator. The
                                record locator for the ultimate supplier is different and might be required if calling the supplier directly. See the
                                SupplierLocator tag.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="PassiveCrsRecordLocator" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If this segment has an associated passive segment in a separate CRS, this is the record locator for the
                                PNR in which that passive segment was created. The name of the CRS is defined at a higher level in the PNR element.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SupplierLocator" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If the segment represents a booked segment, this is the locator for the booking on he suppliers CRS when
                                accessed through an intermediate CRS. For example, when booking a flight on Continental through Sabre, you get a
                                record locator from Sabre that you use to modify or cancel the PNR. However, Continental has its own record locator
                                that it assigns from its own CRS. When we are able, we capture this locator, too, and return it in this field. This is
                                not intended for programmatic use, but to be printed on an itinerary for information purposes, e.g., for use when
                                checking in at a hotel.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Policies" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Any policies, especially regarding cancellation, applied by the Switchfly server. These policies are
                                configured directly in the Switchfly server.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="CrsPolicies" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Any policies, especially regarding cancellation, from the remote CRS. These policies are received from
                                the CRS that provided the product.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AgencyName" type="s:string" minOccurs="0"/>
                    <s:element name="AgencyAddress" type="s:string" minOccurs="0"/>
                    <s:element name="AgentName" type="s:string" minOccurs="0"/>
                    <s:element name="ConfirmationStatus" type="s:string" minOccurs="0"/>
                    <s:element name="SpecialId" type="s:int" minOccurs="0"/>
                    <s:element name="Supplier" type="InternalSupplierType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Optional information about the supplier of this segment</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="BookingMemos" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This field holds memos that can be optionally added to a booking in the remote Switchfly system.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="BookingMemo" type="BookingMemoType" minOccurs="0"
                                    maxOccurs="unbounded"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="ConnectorCustomInfos" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>This field holds a collection of miscellaneous connector-specific details that are stored for
                                post-booking inspections and operations.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="ConnectorCustomInfo"
                                    type="ConnectorCustomInfoType" minOccurs="0"
                                    maxOccurs="unbounded"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="ArrivalAirConnectionInfo" type="AirConnectionInfoType"
                        minOccurs="0" maxOccurs="1">
                        <s:annotation>
                            <s:documentation>This field holds arrival airline information that some connectors will use.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="DepartureAirConnectionInfo" type="AirConnectionInfoType"
                        minOccurs="0" maxOccurs="1">
                        <s:annotation>
                            <s:documentation>This field holds arrival airline information that some connectors will use.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SingleUseCard" type="s:string" minOccurs="0"/>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="SuperPnrType">
        <s:sequence>
            <s:element name="Traveler" type="TravelerType" maxOccurs="unbounded"/>
            <s:element name="AirItinerary" type="AirItineraryType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="RoomSegment" type="RoomSegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
            <s:element name="CarSegment" type="CarSegmentType" minOccurs="0" maxOccurs="unbounded"/>
            <s:element name="ActivitySegment" type="ActivitySegmentType" minOccurs="0"
                maxOccurs="unbounded"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="PnrListType">
        <s:sequence>
            <s:element name="BookingId" type="s:string"/>
            <s:element name="ModifiedDate" type="IsoTimestampType"/>
            <s:element name="ModificationType" type="s:string"/>
            <s:element name="AgentId" type="s:string"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="TaxDetailsType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:sequence>
            <s:element name="Tax" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:attribute name="Name" type="s:string"/>
                    <s:attribute name="Type" type="s:string"/>
                    <s:attribute name="Amount" type="s:double"/>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="TravelerType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:sequence>
            <s:element name="InternalProfileId" type="s:int" minOccurs="0">
                <s:annotation>
                    <s:documentation>The Switchfly id for the profile for this traveler.</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="ExternalProfileId" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>If this traveler was created based on the profile from an external system, this is the id in that external
                        system.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="LastName" type="s:string" minOccurs="0"/>
            <s:element name="FirstName" type="s:string" minOccurs="0"/>
            <s:element name="MiddleName" type="s:string" minOccurs="0"/>
            <s:element name="Gender" minOccurs="0">
                <s:simpleType>
                    <s:restriction base="s:string">
                        <s:enumeration value="M"/>
                        <s:enumeration value="F"/>
                    </s:restriction>
                </s:simpleType>
            </s:element>
            <s:element name="OfficialTravelerId" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>An official id (e.g., passport number).</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="OfficialTravelerIdLabel" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>Type of official traveler id. Possible label values: PASSPORT, DRIVERS_LICENSE, NATIONAL_IDENTITY_CARD, LOCAL_ID
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="OfficialTravelerIdCountry" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>Issuing country of official traveler id.</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="TsaRedressNumber" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>The TSA assigned redress number.</s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="DateOfBirth" type="IsoDateType" minOccurs="0"/>
            <s:choice minOccurs="0">
                <s:element name="AgeInMonths" type="s:int"/>
                <s:element name="Age" type="s:int"/>
            </s:choice>
            <s:element name="AgeClass" type="AgeClassType" minOccurs="0"/>
            <s:element name="Title" type="s:string" minOccurs="0"/>
            <s:element name="AirLoyaltyPrograms" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="LoyaltyProgram" minOccurs="0" type="LoyaltyProgramType" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="RoomLoyaltyPrograms" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="LoyaltyProgram" type="LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="CarLoyaltyPrograms" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="LoyaltyProgram" type="LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="LoyaltyPrograms" type="LoyaltyProgramType" minOccurs="0" maxOccurs="unbounded">
                 <s:annotation>
                    <s:documentation>Obsolete. This may or may not work properly. Use the Air, Room, and Car loyalty program tags above.
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="Phone" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:simpleContent>
                        <s:extension base="s:string">
                            <s:attribute name="Type" type="s:string">
                                <s:annotation>
                                    <s:documentation>This attribute can be set to specify the type of phone number. It is free form, but specific
                                        values recognized by Switchfly are: home, work, and cell. Entering these values might allow better
                                        identification of a phone number in a CRS.
                                    </s:documentation>
                                </s:annotation>
                            </s:attribute>
                        </s:extension>
                    </s:simpleContent>
                </s:complexType>
            </s:element>
            <s:element name="Fax" type="s:string" minOccurs="0"/>
            <s:element name="Company" type="s:string" minOccurs="0"/>
            <s:element name="Address" type="AddressType" minOccurs="0"/>
            <s:element name="Email" type="s:string" minOccurs="0"/>
            <s:element name="BookingMemos" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="BookingMemo" type="BookingMemoType" minOccurs="0"
                            maxOccurs="unbounded"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="DeleteTraveler" type="s:string" minOccurs="0">
                <s:annotation>
                    <s:documentation>True/False. Defaults to false. Used during a modify to indicate that this traveler is deleted
                    </s:documentation>
                </s:annotation>
            </s:element>
            <s:element name="GovernmentFinancialId" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="TravelerProfileType">
        <s:complexContent>
            <s:extension base="TravelerType">
                <s:sequence>
                    <s:element name="CreditCard" type="CreditCardType" minOccurs="0"
                        maxOccurs="unbounded"/>
                    <s:element name="PointRedemption" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Minimum" type="s:int" minOccurs="0"/>
                                <s:element name="Available" type="s:int" minOccurs="0"/>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:extension>
        </s:complexContent>
    </s:complexType>
    <s:complexType name="UserReviewSummaryType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:sequence>
            <s:element name="UserReviewHotelCode" type="s:string"/>
            <s:element name="LogoImageUrl" type="s:string"/>
            <s:element name="RankScale" type="s:int" minOccurs="0"/>
            <s:element name="Rank" type="s:int" minOccurs="0"/>
            <s:element name="MedianUserRating" type="s:double" minOccurs="0"/>
            <s:element name="RatingImageUrl" type="s:string" minOccurs="0"/>
            <s:element name="WriteReviewUrl" type="s:string" minOccurs="0"/>
            <s:element name="TotalUserReviewCount" type="s:int" minOccurs="0"/>
            <s:element name="UserReview" type="UserReviewType" minOccurs="0" maxOccurs="unbounded"
            />
        </s:sequence>
    </s:complexType>
    <s:complexType name="UserReviewType">
        <s:annotation>
            <s:documentation/>
        </s:annotation>
        <s:sequence>
            <s:element name="PublishedDate" type="IsoDateType" minOccurs="0"/>
            <s:element name="Author" type="s:string" minOccurs="0"/>
            <s:element name="AuthorLocation" type="s:string" minOccurs="0"/>
            <s:element name="Title" type="s:string" minOccurs="0"/>
            <s:element name="Content" type="s:string" minOccurs="0"/>
            <s:element name="ContentSummary" type="s:string" minOccurs="0"/>
            <s:element name="Rating" type="s:int" minOccurs="0"/>
            <s:element name="RatingImage" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="FareBasisCodeType">
        <s:sequence>
            <s:element name="LocationInformation" minOccurs="1" maxOccurs="1">
                <s:complexType>
                    <s:sequence>
                        <s:element name="DepartureDate" type="IsoDateType"/>
                        <s:element name="Arrival" type="s:string"/>
                        <s:element name="Origin" type="s:string"/>
                        <s:element name="AirLineCode" type="s:string"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="Code" type="s:string"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="FeeDueAtHotelListType">
        <s:sequence>
            <s:element name="FeeDueAtHotel" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="Currency" type="s:string" minOccurs="0"/>
                        <s:element name="Fee" type="s:decimal" minOccurs="0"/>
                        <s:element name="Description" type="s:string" minOccurs="1"/>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
    </s:complexType>
    <s:complexType name="AirConnectionInfoType">
        <s:sequence>
            <s:element name="AirlineCode" type="s:string" minOccurs="0"/>
            <s:element name="AirlineName" type="s:string" minOccurs="0"/>
            <s:element name="FlightNumber" type="s:string" minOccurs="0"/>
            <s:element name="Time" type="s:string" minOccurs="0"/>
        </s:sequence>
    </s:complexType>
    <s:complexType name="ItineraryType">
        <s:sequence>
            <s:element name="AirItineraryRequest" minOccurs="0">
                <s:complexType>
                    <s:sequence>
                        <s:element name="AirLeg" type="s:string" minOccurs="0" maxOccurs="unbounded">
                            <s:annotation>
                                <s:documentation>The BookingDescriptor string that was returned in the availability response.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="QuickAddAirItinerary" type="AirItineraryType" minOccurs="0" maxOccurs="unbounded">
                            <s:annotation>
                                <s:documentation>If Quick Add Air Itinerary will be booked in the CreatePnrRQ, then it must be specified here in order to calculate the discount.</s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="RoomSegmentRequest" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="RoomSegment" type="s:string">
                            <s:annotation>
                                <s:documentation>The BookingDescriptor string that was returned in the availability response.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="CarSegmentRequest" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="CarSegment" type="s:string">
                            <s:annotation>
                                <s:documentation>The BookingDescriptor string that was returned in the availability response.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
            <s:element name="ActivitySegmentRequest" minOccurs="0" maxOccurs="unbounded">
                <s:complexType>
                    <s:sequence>
                        <s:element name="SegmentDescriptor" type="s:string">
                            <s:annotation>
                                <s:documentation>The BookingDescriptor string that was returned in the availability response.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="EventDescriptor" type="s:string">
                            <s:annotation>
                                <s:documentation>The Booking Event Descriptor string that was returned in the availability response.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
            </s:element>
        </s:sequence>
		</s:complexType>
		<s:complexType name="LoyaltyProgramType">
			<s:sequence>
				<s:element name="Company" type="s:string">
					<s:annotation>
						<s:documentation>The industry standard code for the company. E.g., UA for United Airlines.
						</s:documentation>
					</s:annotation>
				</s:element>
				<s:element name="Number" type="s:string">
					<s:annotation>
						<s:documentation>The loyalty program membership number.</s:documentation>
					</s:annotation>
				</s:element>
				<s:element name="TierCode" type="s:string" minOccurs="0">
					<s:annotation>
						<s:documentation>The Tier Code.</s:documentation>
					</s:annotation>
				</s:element>
			</s:sequence>
		</s:complexType>';
		return $aux;
	}
	function search_objects(){
		$aux = '
            <s:complexType name="ActivityChoiceType">
                <s:annotation>
                    <s:documentation />
                </s:annotation>
                <s:complexContent>
                    <s:extension base="ActivityDescriptionType">
                        <s:sequence>
                            <s:element name="BookingDescriptor" type="s:string" />
                            <s:element name="DetailsAvailable" type="s:boolean" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>True if additional details about this activity are available. These details can sometimes affect
                                        pricing.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="EventGroup" minOccurs="0" maxOccurs="unbounded">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="Rate">
                                            <s:complexType>
                                                <s:sequence>
                                                    <s:element name="RateName" type="s:string" minOccurs="0" />
                                                    <s:element name="RateType" type="ActivityRateApplicabilityType" minOccurs="0">
                                                        <s:annotation>
                                                            <s:documentation>This defines the allowed use of the activity. It has one of the values:
                                                                activity_only, activity/addon, or addon_only. Addon_only activities are returned only
                                                                in a ActivityAddonRS, so addon_only activities should be ignored in any other context.
                                                            </s:documentation>
                                                        </s:annotation>
                                                    </s:element>
                                                    <s:element name="Label" maxOccurs="unbounded">
                                                        <s:complexType>
                                                            <s:sequence>
                                                                <s:element name="Description" type="s:string" minOccurs="0" />
                                                                <s:element name="BookingCode" type="s:string" minOccurs="0" />
                                                                <s:element name="Label" type="s:string" />
                                                                <s:element name="AgeClass" type="AgeClassType" minOccurs="0" />
                                                                <s:element name="Count" type="s:int" minOccurs="0">
                                                                    <s:annotation>
                                                                        <s:documentation>When booking, this holds the number of purchases for this Label.
                                                                            During availability, this field is not used.
                                                                        </s:documentation>
                                                                    </s:annotation>
                                                                </s:element>
                                                                <s:element name="FamilyPlan" type="s:boolean">
                                                                    <s:annotation>
                                                                        <s:documentation>This is true if this label participates in family plan logic. If
                                                                            there is a family plan master label specified, up to the number of purchases in
                                                                            that label are free.
                                                                        </s:documentation>
                                                                    </s:annotation>
                                                                </s:element>
                                                                <s:element name="FamilyPlanMaster" type="s:boolean">
                                                                    <s:annotation>
                                                                        <s:documentation>If true, this label specifies the number of free participants taken
                                                                            from the family plan labels. Only one label may be specified the master.
                                                                        </s:documentation>
                                                                    </s:annotation>
                                                                </s:element>
                                                                <s:element name="Price" type="PriceType" minOccurs="0">
                                                                    <s:annotation>
                                                                        <s:documentation>Price is included in labels only during shopping. Booked activities
                                                                            have a price on the activity rather than the label.
                                                                        </s:documentation>
                                                                    </s:annotation>
                                                                </s:element>
                                                                <s:element name="StrikeThroughPrice" type="PriceType" minOccurs="0">
                                                                    <s:annotation>
                                                                        <s:documentation>Strike-through price of the label.
                                                                        </s:documentation>
                                                                    </s:annotation>
                                                                </s:element>
                                                            </s:sequence>
                                                        </s:complexType>
                                                    </s:element>
                                                    <s:element name="CrsPolicies" type="s:string" minOccurs="0" />
                                                    <s:element name="ActivityQuestions" type="ActivityQuestionsType" minOccurs="0" />
                                                    <s:element name="PassengersInfo" type="PassengersInfoType" minOccurs="0" />
                                                </s:sequence>
                                            </s:complexType>
                                        </s:element>
                                        <s:element name="AnyDateEvent" type="ActivityAnyDateEventType" minOccurs="0" maxOccurs="unbounded" />
                                        <s:element name="FixedDateEvent" type="ActivityFixedDateEventType" minOccurs="0" maxOccurs="unbounded" />
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                            <s:element name="ActivityCancellationPolicies" minOccurs="0">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="ActivityCancellationPolicy" type="ActivityCancellationPolicyType" minOccurs="0" maxOccurs="unbounded" />
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="ActivityCancellationPolicyType">
                <s:sequence>
                    <s:element name="Description" type="s:string">
                        <s:annotation>
                            <s:documentation>A textual description of the cancellation policy.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:sequence>
                        <s:element name="DeadlineDateBeforeTimestamp" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for
                                    the reserved activity.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="DeadlineDateAfterTimestamp" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The timestamp after which this penalty cancellation policy applies. This timestamp is in the local time for
                                    the reserved activity.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:choice>
                            <s:annotation>
                                <s:documentation>Cancellation policies can be specified in one of several ways.
                                </s:documentation>
                            </s:annotation>
                            <s:element name="Percentage" type="s:double">
                                <s:annotation>
                                    <s:documentation>This indicates that the cancellation penalty is a percentage of the total booking.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="FlatFee">
                                <s:annotation>
                                    <s:documentation>This indicates that a flat fee will be charged in the specified currency.
                                    </s:documentation>
                                </s:annotation>
                                <s:complexType>
                                    <s:simpleContent>
                                        <s:extension base="s:double">
                                            <s:attribute name="Currency" type="s:string" />
                                        </s:extension>
                                    </s:simpleContent>
                                </s:complexType>
                            </s:element>
                        </s:choice>
                    </s:sequence>
                    <s:element name="CalculatedFeeAmount" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This indicates the calculated fee amount in customer currency.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:simpleContent>
                                <s:extension base="s:double">
                                    <s:attribute name="Currency" type="s:string" />
                                </s:extension>
                            </s:simpleContent>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="ActivityQuestionsType">
                <s:sequence>
                    <s:element name="ActivityQuestion" minOccurs="0" maxOccurs="unbounded">
                        <s:complexType>
                            <s:attribute name="id" use="required">
                                <s:simpleType>
                                    <s:restriction base="s:string">
                                        <s:minLength value="1" />
                                    </s:restriction>
                                </s:simpleType>
                            </s:attribute>
                            <s:attribute name="type" type="QuestionType" use="optional" />
                            <s:attribute name="title" type="s:string" use="optional" />
                            <s:attribute name="value" type="s:string" use="optional" />
                            <s:attribute name="min" type="s:int" use="optional" />
                            <s:attribute name="max" type="s:int" use="optional" />
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:simpleType name="QuestionType">
                <s:restriction base="s:string">
                    <s:enumeration value="TEXT_ADDITION" />
                    <s:enumeration value="BOOLEAN_ADDITION" />
                    <s:enumeration value="RANGE_ADDITION" />
                </s:restriction>
            </s:simpleType>
            <s:complexType name="PassengersInfoType">
                <s:sequence>
                    <s:element name="PassengerInfo" minOccurs="0" maxOccurs="unbounded">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="ActivityQuestions" type="ActivityQuestionsType" minOccurs="0" maxOccurs="1" />
                            </s:sequence>
                            <s:attribute name="seqNumber" type="s:integer" use="required" />
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="ActivityDescriptionType">
                <s:complexContent>
                    <s:extension base="AbstractActivityType">
                        <s:sequence>
                            <s:element name="Category" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>This is a free format string intended for organizing groups of activities in a GUI.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="CheckinText" type="s:string" minOccurs="0" />
                            <s:element name="Description" type="s:string" minOccurs="0" />
                            <s:element name="Features" type="s:string" minOccurs="0" />
                            <s:element name="ImageUrl" type="s:string" minOccurs="0" maxOccurs="unbounded" />
                            <s:element name="ThumbnailUrl" type="s:string" minOccurs="0" />
                            <s:element name="Notes" type="s:string" minOccurs="0" />
                            <s:element name="Subcategory" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>This is a free format string intended for organizing groups of activities in a GUI.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ActivityPhoneNumber" type="s:string" minOccurs="0" />
                            <s:element name="YoutubeMovieIds" minOccurs="0">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="YoutubeMovieId" type="s:string" maxOccurs="unbounded" />
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="ActivitySearchType">
                <s:complexContent>
                    <s:extension base="SearchType">
                        <s:sequence>
                            <s:choice>
                                <s:element name="SearchLocation" type="SearchLocationType" minOccurs="0" />
                                <s:element name="SearchActivities" minOccurs="0">
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="SearchActivity" type="SearchActivityType" maxOccurs="unbounded" />
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                            </s:choice>
                            <s:element name="NumAdults" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of adults. The server may be able to return some activities without traveler counts being
                                        specified. Within the Switchfly server, internal activities do not require counts to be returned. However, most CRSs
                                        that the Switchfly server communicates with require these values and unless traveler counts are specified they will
                                        return no activities.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="NumChildren" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of children.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ChildAge" type="s:int" minOccurs="0" maxOccurs="unbounded">
                                <s:annotation>
                                    <s:documentation>The ages of the children. If specified, the number of ChildAge elements must match the value entered in
                                        NumChildren.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="StartDate" type="IsoDateType">
                                <s:annotation>
                                    <s:documentation>The date, in ISO format, on which to start searching for activities.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="EndDate" type="IsoDateType">
                                <s:annotation>
                                    <s:documentation>The date, in ISO format, on which to stop searching for activities.</s:documentation>
                                </s:annotation>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="ActivitySearchResponseType">
                <s:complexContent>
                    <s:extension base="SearchResponseType">
                        <s:sequence>
                            <s:element name="ActivityChoice" type="ActivityChoiceType" minOccurs="0" maxOccurs="unbounded" />
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="AirSearchLegType" />
            <s:complexType name="AirSearchType">
                <s:annotation>
                    <s:documentation />
                </s:annotation>
                <s:complexContent>
                    <s:extension base="SearchType">
                        <s:sequence>
                            <s:element name="Airline" type="s:string" minOccurs="0" maxOccurs="unbounded">
                                <s:annotation>
                                    <s:documentation>Restrict search to the specified collection of airlines. Airlines are specified using their 2 letter
                                        code. (E.g., UA, AA, AQ, etc.) If no airlines are specified, than the search is unrestricted. If one or more airlines
                                        are specified, then only those airlines will be returned in the response.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Cabin" type="s:string">
                                <s:annotation>
                                    <s:documentation>The cabin in which to search: "first", "business", or "coach". You can only search within a single
                                        cabin, although you may sometimes find that the results you get may be in a different cabin from what you requested.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="MaxStops" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The maximum number of stops for any leg in the journey.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="BulkOnly" type="s:boolean" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>If true, only private fares will be returned and published fares will be filtered out.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Adults" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of adults requiring seats.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Children" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of children requiring seats.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Seniors" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of seniors requiring seats.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="InfantsRequiringSeats" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of infants requiring seats. Do not include infants that will be in laps.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="InfantsInLaps" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The number of infants that do not require seats and will be traveling in a lap.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Leg" maxOccurs="unbounded">
                                <s:annotation>
                                    <s:documentation />
                                </s:annotation>
                                <s:complexType>
                                    <s:complexContent>
                                        <s:extension base="AirSearchLegType">
                                            <s:sequence>
                                                <s:element name="CorrelationId" type="s:string" minOccurs="0">
                                                    <s:annotation>
                                                        <s:documentation>This is an arbitrary string that will be echoed in the corresponding legs returned
                                                            in the response.
                                                        </s:documentation>
                                                    </s:annotation>
                                                </s:element>
                                                <s:element name="OriginLocation" type="SearchLocationType" minOccurs="0" />
                                                <s:element name="DestinationLocation" type="SearchLocationType" minOccurs="0" />
                                                <s:element name="DepartureTimestamp" type="IsoTimestampType">
                                                    <s:annotation>
                                                        <s:documentation>The ISO timestamp at which to start searching for availability. (E.g.,
                                                            2008-01-02T15:30 for 2 Jan, 2008 at 3:30 PM) The search will be for the specified day and will
                                                            search from the specified time forward.
                                                        </s:documentation>
                                                    </s:annotation>
                                                </s:element>
                                            </s:sequence>
                                        </s:extension>
                                    </s:complexContent>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="AirSearchResponseType">
                <s:complexContent>
                    <s:extension base="SearchResponseType">
                        <s:sequence>
                            <s:element name="Alternative" type="AirPnrType" minOccurs="0" maxOccurs="unbounded" />
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="CarSearchType">
                <s:complexContent>
                    <s:extension base="SearchType">
                        <s:sequence>
                            <s:element name="CountryOfResidency" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The two letter country code of the country in which the renter resides.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="PickupLocation" type="SearchLocationType" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The location at which the car will be picked up. Currently, only airport pickup is supported and an
                                        ambiguous search error will be thrown if a city with multiple airports is specified.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="DropOffLocation" type="SearchLocationType" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The location at which the car will be dropped off. Currently, Switchfly only supports dropping off at
                                        the same location as picking up. If omitted, drop off defaults to the same value as pickup.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="PickupTimestamp" type="IsoTimestampType">
                                <s:annotation>
                                    <s:documentation>The date and time that the car is desired, in ISO format.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="DropOffTimestamp" type="IsoTimestampType">
                                <s:annotation>
                                    <s:documentation>The date and time that the car will be returned, in ISO format.</s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ChainCode" type="s:string" minOccurs="0" maxOccurs="unbounded">
                                <s:annotation>
                                    <s:documentation>One or more two character company codes may be specified, in which case the search will be limited to
                                        those companies. E.g., AL or ZE.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="CarCode" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>A 4 character SIPP code can be specified (e.g., CDAR) in which case the search will be limited to the
                                        specified car type.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="SortCriteria" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Sort car results by: car_price - sort by ascending price up to MaxResults and
                                        car_sipp_category_then_price - return cars in ascending price up to MaxResults for each car class (1st letter of SIPP
                                        code).
                                    </s:documentation>
                                </s:annotation>
                                <s:simpleType>
                                    <s:restriction base="s:string">
                                        <s:enumeration value="car_price" />
                                        <s:enumeration value="car_sipp_category_then_price" />
                                    </s:restriction>
                                </s:simpleType>
                            </s:element>
                            <s:element name="MaxResults" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Limit car results up to MaxResults or the limit car results up to MaxResults by car class depending on
                                        SortCriteria.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="AtAirportOnly" type="s:boolean" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>If true, than only airport locations will be returned from the search. For most data sources, the
                                        currently version of Switchfly will only return airport locations. The exception is that internal Switchfly cars can
                                        be located at arbitrary locations.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="CarSearchResponseType">
                <s:complexContent>
                    <s:extension base="SearchResponseType">
                        <s:sequence>
                            <s:element name="CarSegment" type="CarSegmentType" minOccurs="0" maxOccurs="unbounded" />
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="DirectionType">
                <s:sequence>
                    <s:element name="Origin" type="s:string" minOccurs="0" />
                    <s:element name="Destination" type="s:string" minOccurs="0" />
                    <s:element name="Directions" type="s:string" minOccurs="0" />
                </s:sequence>
            </s:complexType>
            <s:complexType name="HotelType">
                <s:sequence>
                    <s:element name="EzRezId" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Switchfly maintains a master list of hotels independent of the hotels returned from its sources. In order to
                                avoid presenting duplicate hotels, it attempts to match each hotel with this master list. If successful, this element holds
                                the id of that master hotel.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Ql2Code" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Not usable without prior configuration by Switchfly support.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SearchHotelId" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If the search specified hotel ids, this element will identify the id that caused this hotel to be returned.
                            </s:documentation>
                        </s:annotation>
                        <s:complexType>
                            <s:sequence>
                                <s:element name="IdSource" type="s:string" />
                                <s:element name="Id" type="s:string" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="AirportCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>They airport code associated with this hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="BookingDescriptor" type="s:string">
                        <s:annotation>
                            <s:documentation>A string that will be used when booking a room in this hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ChainCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Switchfly maintains a master list of hotel chains independent of the hotel chains returned from its sources.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="BrandCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Switchfly maintains a master list of hotel brands independent of the hotel brands returned from its sources.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelName" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The name of the hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="HotelCode" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A code returned from the CRS to which Switchfly connected to search for rooms. It uniquely defines the hotel for
                                a period of time defined by the remote CRS. This time can range from temporary (such as a small time period after the
                                availability query) to permanent. THis value is not returned if the search is specified using a hotel id.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Approved" type="s:boolean" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If omitted, this is false. If true, it indicates the Switchfly server has approved this hotel. This is usually
                                used to display the hotel in special format.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ExclusiveDeal" type="s:boolean" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If omitted, this is false. If true, it indicates that the hotel offer is flagged from the CRS as an exclusive
                                deal and you may want to sort this early in your customer display.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Policies" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The policies of the remote system regarding payments and refunds.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="PropertyDescription" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A textual description of the property.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="RoomType" type="s:string" minOccurs="0" maxOccurs="unbounded" />
                    <s:element name="RoomTypesDescriptions" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A textual description of the types of rooms available in the hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="RoomDescription" type="s:string" minOccurs="0" />
                    <s:element name="ShortDescription" type="s:string" minOccurs="0" />
                    <s:element name="ExteriorDescription" type="s:string" minOccurs="0" />
                    <s:element name="LobbyDescription" type="s:string" minOccurs="0" />
                    <s:element name="PointOfInterest" minOccurs="0" maxOccurs="unbounded">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Description" type="s:string" />
                                <s:sequence minOccurs="0">
                                    <s:element name="Distance" type="s:float" />
                                    <s:element name="Units" type="s:string">
                                        <s:annotation>
                                            <s:documentation>miles or meters</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                </s:sequence>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="PointsOfInterestDescription" type="s:string" minOccurs="0" />
                    <s:element name="Address" type="AddressType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The address of the hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Phone" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The phone number for calls regarding the hotel. This may be a toll free number shared among many hotels.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="LocalPhone" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A phone number that is associated specifically with the hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Fax" type="s:string" minOccurs="0" />
                    <s:element name="LocalFax" type="s:string" minOccurs="0" />
                    <s:element name="GeoLocation" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Latitude" type="s:string" />
                                <s:element name="Longitude" type="s:string" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="DistanceFromSearchLocation" minOccurs="0">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Distance" type="s:double" />
                                <s:element name="Units" type="DistanceUnitsType" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="Amenity" type="s:string" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>Any number of amenities may be provided identifying the amenities for the hotel. (E.g., pool, parking, etc.)
                                These amenities are free-form and can vary in wording and spelling, even from a single CRS.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AmenitiesDescription" type="s:string" minOccurs="0" />
                    <s:element name="AmenityFilter" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>An amenity filter is an amenity that has been "unified" by the Switchfly server and is one of a well defined set
                                of values. It is intended for use in filtering hotels in GUI based on available amenities. It is not intended to replace the
                                Amenity elements, which most likely include more information than the amenityFilter elements
                            </s:documentation>
                        </s:annotation>
                        <s:simpleType>
                            <s:restriction base="s:string">
                                <s:enumeration value="AIRPORT_SHUTTLE" />
                                <s:enumeration value="ALL_INCLUSIVE" />
                                <s:enumeration value="BABYSITTING_CHILD_CARE" />
                                <s:enumeration value="BAR_LOUNGE_ONSITE" />
                                <s:enumeration value="BEACH" />
                                <s:enumeration value="BUSINESS_CENTER" />
                                <s:enumeration value="CASINO" />
                                <s:enumeration value="CHILDREN_TEEN_PROGRAMS" />
                                <s:enumeration value="CONTINENTAL_MEAL_PLAN" />
                                <s:enumeration value="FREE_PARKING" />
                                <s:enumeration value="GOLF" />
                                <s:enumeration value="GYM" />
                                <s:enumeration value="HANDICAP_FACILITIES" />
                                <s:enumeration value="INTERNET" />
                                <s:enumeration value="MEETING_ROOMS" />
                                <s:enumeration value="PETS_ALLOWED" />
                                <s:enumeration value="RESTAURANT_ONSITE" />
                                <s:enumeration value="ROOM_SERVICE" />
                                <s:enumeration value="SKI_IN_SKI_OUT" />
                                <s:enumeration value="SMALL_PETS_ALLOWED" />
                                <s:enumeration value="SPA" />
                                <s:enumeration value="SWIMMING_POOL" />
                                <s:enumeration value="SMOKE_FREE" />
                                <s:enumeration value="TENNIS" />
                                <s:enumeration value="WIRELESS_INTERNET" />
                            </s:restriction>
                        </s:simpleType>
                    </s:element>
                    <s:element name="FamilyDescription" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This field is a free format text field that describes the hotel policy that defines a family. This will be
                                absent unless the remote CRS provides this information. It is most likely present for hotels defined locally in the Switchfly
                                that is serving this message.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Checkin" type="IsoTimestampType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is the date and time of checkin. The date is taken from the original search. The time is added, if known,
                                from the description of the hotel.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Checkout" type="IsoTimestampType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is the date and time of checkout. The date is taken from the original search. The time is added, if known,
                                from the description of the hotel.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Rating" type="s:double" minOccurs="0" />
                    <s:element name="SpecialNotices" type="s:string" minOccurs="0" />
                    <s:element name="AdditionalInfo" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is a free format placeholder for additional information about the hotel. It is currently only populated for
                                internally loaded Switchfly hotels.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AdditionalDisplayInfo" type="s:string" minOccurs="0" maxOccurs="unbounded" />
                    <s:element name="Currency" type="s:string" />
                    <s:element name="MinimumPrice" type="s:double" minOccurs="0">
                        <s:annotation>
                            <s:documentation>For some sources of availability, the first call to get availability does not return specific room information
                                but does return a price range for the hotel. That information is provided here. In this case, the minimum and maximum may be
                                only an estimate and may differ from the true minimum and maximum that will be determined when complete room information is
                                obtained using the hotel details message. If room information *is* returned, these tags will simply reflect the min and max
                                prices of the underlying rooms.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="MinimumStandalonePrice" type="s:double" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If the search included a flag to get package savings, then this elements will represent the minimum standalone
                                rate for a hotel that was searched as part of a package.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="MinimumStandalonePriceTaxTotal" type="s:double" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If the search included a flag to get package savings, then this elements will represent the minimum standalone
                                rate tax for a hotel that was searched as part of a package.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="MinimumPriceTaxTotal" type="s:double" minOccurs="0">
                        <s:annotation>
                            <s:documentation>These tags will simply reflect the tax total for the minimum price of the underlying rooms.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Url" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A link to a page that provide additional information about the hotel.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Html" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>HTML that describes the hotel. In general, only one of these or the Url element is specified.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="MapUrl" type="s:string" minOccurs="0" />
                    <s:element name="Thumbnail" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Images over the API are limited to leonardo.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="VirtualTourUrl" type="s:string" minOccurs="0" />
                    <s:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="RoomSegment" type="RoomSegmentType" minOccurs="0" maxOccurs="unbounded" />
                            </s:sequence>
                            <s:attribute name="adults" type="s:int" use="required" />
                            <s:attribute name="children" type="s:int" />
                        </s:complexType>
                    </s:element>
                    <s:element name="RoomAmenity" type="s:string" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>Any number of amenities may be present identifying the amenities for each room in the hotel. (E.g., safe, HBO,
                                etc.)
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="RoomAmenitiesDescription" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is a free format description of the amenities present in the rooms of the hotel. Whether a free format
                                description as held here is provided, or a list of amenities suitable for a bullet list is provided using the RoomAmenity tag,
                                depends on the ultimate source of the room.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Direction" type="DirectionType" minOccurs="0" maxOccurs="unbounded" />
                    <s:element name="Restaurant" type="RestaurantType" minOccurs="0" maxOccurs="unbounded" />
                    <s:element name="RestaurantsDescription" type="s:string" minOccurs="0" />
                    <s:element name="Image" type="ImageType" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>Images over the API are limited to leonardo.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="DetailsAvailable" type="s:boolean" minOccurs="0">
                        <s:annotation>
                            <s:documentation>Since the Switchfly system communicates with and aggregates products from a number of different CRSs, it must
                                adapt to various styles of availability searches. Some systems return all they know about available properties in one call.
                                Others might return just a hotel description and no information about rooms. Even others might return bare bones information
                                about the hotel but details about available rooms. The DetailsAvailable boolean indicates whether additional information about
                                the hotel is available. If so, the HotelDetails message may be used to get additional information about the property. The
                                intent is that this second, potentially much more expensive, call is avoided until the customer indicates interest in the
                                property. While the original room search translates into a single call to the remote CRS, the details call can translate into
                                multiple calls for the single specified hotel.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Entertainment" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>This is a free format field allowing a description of entertainment options at or around the hotel. It is
                                currently only populated by internally loaded Switchfly hotels.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="UserReviewSummary" type="UserReviewSummaryType" minOccurs="0" />
                </s:sequence>
            </s:complexType>
            <s:complexType name="ImageType">
                <s:sequence>
                    <s:element name="Height" type="s:int" minOccurs="0" />
                    <s:element name="Width" type="s:int" minOccurs="0" />
                    <s:element name="Caption" type="s:string" minOccurs="0" />
                    <s:element name="Name" type="s:string" minOccurs="0" />
                    <s:element name="ThumbnailUrl" type="s:string" minOccurs="0" />
                    <s:element name="ImageId" type="s:integer" minOccurs="0" />
                    <s:element name="Type" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>"lobby", "logo","map", "guest room", "suite", "restaurant", "beach", "spa", "pool", "exterior", "golf",
                                "meeting_room", "ballroom", "bar_lounge", "public_area", "interior", "view", "unknown", "main";
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Url" type="s:string" minOccurs="0" />
                </s:sequence>
            </s:complexType>
            <s:complexType name="AirPnrType">
                <s:sequence>
                    <s:element name="Adults" type="s:int">
                        <s:annotation>
                            <s:documentation>The number of adults specified in the search.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AdultPrice" type="PriceType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The unit price for each adult in the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Children" type="s:int">
                        <s:annotation>
                            <s:documentation>The number of children specified in the search.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ChildPrice" type="PriceType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The unit price for each child in the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ChildAge" type="s:int" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>The ages of the children. If specified, the number of ChildAge elements must match the value entered in
                                NumChildren.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Seniors" type="s:int">
                        <s:annotation>
                            <s:documentation>The number of seniors specified in the search.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SeniorPrice" type="PriceType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The unit price for each senior in the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="InfantsRequiringSeats" type="s:int">
                        <s:annotation>
                            <s:documentation>The number of infants specified in the search that require their own seat.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="InfantRequiringSeatPrice" type="PriceType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The unit price for each seated infant in the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="InfantsInLaps" type="s:int">
                        <s:annotation>
                            <s:documentation>The number of infants specified in the search that will be sitting in a lap.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="InfantInLapPrice" type="PriceType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The unit price for each infant in lap in the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="IsPassThrough" type="s:boolean">
                        <s:annotation>
                            <s:documentation>This is a boolean that is true if air fare is to be charged to the customers credit card rather than settled
                                between the Switchfly server and the airline. If true, the customers credit card should be passed with the booking and it
                                should not be charged locally for the air fare.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="TotalPrice" type="PriceType">
                        <s:annotation>
                            <s:documentation>The total price for the itinerary.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="TicketingDeadline" type="IsoDateType" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The ticketing deadline is informational. When returned from the CRS, Switchfly captures it and passes it on. It
                                is used by some clients to postpone ticketing. We recommend immediate ticketing, however, since prices are not guaranteed
                                until ticketed.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="LegChoices" maxOccurs="unbounded">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="AirLeg" type="AirLegType" minOccurs="0" maxOccurs="unbounded" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="FareBasisCodes" maxOccurs="unbounded">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="FareBasisCode" type="FareBasisCodeType" minOccurs="0" maxOccurs="unbounded" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="RestaurantType">
                <s:sequence>
                    <s:element name="Atmosphere" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>e.g., “casual”</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Cuisine" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>e.g., “american”</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Hours" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>e.g., “5pm – 1am”</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Name" type="s:string" />
                    <s:element name="Setting" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>e.g., “ocean view”</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="Description" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>e.g., “your description”</s:documentation>
                        </s:annotation>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="RoomSearchType">
                <s:complexContent>
                    <s:extension base="SearchType">
                        <s:sequence>
                            <s:element name="CrsName" type="s:string" minOccurs="0" />
                            <s:element name="SearchLocation" type="SearchLocationType" minOccurs="0" />
                            <s:element name="RadiusSearch" type="RadiusSearchType" minOccurs="0" />
                            <s:element name="ChainCode" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>If the chain code is specified, only hotels in the specified chain will be returned.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="BrandCode" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>If the brand code is specified, only hotels in the specified brand will be returned.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:choice minOccurs="0">
                                <s:element name="UnifiedHotelIds" minOccurs="0" maxOccurs="unbounded">
                                    <s:annotation>
                                        <s:documentation>A list of hotels for which to search. If hotel ids are specified, the search location can be omitted
                                            and will be derived. Only "unified" hotels will work.
                                        </s:documentation>
                                    </s:annotation>
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="Id" type="s:string" maxOccurs="unbounded">
                                                <s:annotation>
                                                    <s:documentation>The unified hotel id. Contact Switchfly for valid values.</s:documentation>
                                                </s:annotation>
                                            </s:element>
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                                <s:element name="HotelIds" minOccurs="0" maxOccurs="unbounded">
                                    <s:annotation>
                                        <s:documentation>A list of hotels for which to search. If hotel ids are specified, the search location can be omitted
                                            and will be derived. Only "unified" hotels will work.
                                        </s:documentation>
                                    </s:annotation>
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="IdSource" type="s:string">
                                                <s:annotation>
                                                    <s:documentation>The system that defines the ID. Currently only supported for internal and northstar.
                                                    </s:documentation>
                                                </s:annotation>
                                            </s:element>
                                            <s:element name="Id" type="s:string" maxOccurs="unbounded">
                                                <s:annotation>
                                                    <s:documentation>The id as defined by the id source.</s:documentation>
                                                </s:annotation>
                                            </s:element>
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                            </s:choice>
                            <s:element name="Ql2HotelIds" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Not usable without prior configuration by Switchfly support.</s:documentation>
                                </s:annotation>
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="Id" type="s:string" minOccurs="1" maxOccurs="unbounded">
                                            <s:annotation>
                                                <s:documentation>Not usable without prior configuration by Switchfly support.</s:documentation>
                                            </s:annotation>
                                        </s:element>
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                            <s:element name="InternalRoom" minOccurs="0" maxOccurs="1">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="ContractId" type="s:integer" minOccurs="0" />
                                        <s:element name="RoomTypeId" type="s:integer" minOccurs="0" />
                                    </s:sequence>
                                </s:complexType>
                            </s:element>
                            <s:element name="HotelCode" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>If the hotel code is specified, only the specified hotel will be returned.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ExcludedEzRezHotelIds" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>A comma separated list of ids for the Switchfly unified hotel database. If the server is able to unify
                                        the hotels received from its sources, it will exclude any hotels that map to one of these unified hotel entries. If
                                        the list is empty or not specified, no hotels are excluded.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="IncludedEzRezHotelIds" type="s:string" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>A comma separated list of ids for the Switchfly unified hotel database. The server will only return
                                        hotels that can be mapped to the unified hotel database and that map to one of the specified entries. If the list is
                                        empty or not specified, no hotels are excluded.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="SortCriteria" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Return the hotels sorted by: hotel_price_ascending - ascending order, hotel_price_descending -
                                        descending order, score up to MaxResults, hotel_composite_score - composite score in descending order up to
                                        MaxResults, hotel_rating_then_price - sort by star rating (high to low) then by price in ascending order up to
                                        MaxResults for each star rating, and hotel_rating_then_composite_score - sort by star rating (high to low) then by
                                        composite score in descending score up to MaxResults for each star rating.
                                    </s:documentation>
                                </s:annotation>
                                <s:simpleType>
                                    <s:restriction base="s:string">
                                        <s:enumeration value="hotel_price_ascending" />
                                        <s:enumeration value="hotel_price_descending" />
                                        <s:enumeration value="hotel_composite_score" />
                                        <s:enumeration value="hotel_rating_then_price" />
                                        <s:enumeration value="hotel_rating_then_composite_score" />
                                    </s:restriction>
                                </s:simpleType>
                            </s:element>
                            <s:element name="MaxResults" type="s:integer" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Maximum number of hotels to return or the maximum number of hotels returned per star rating group
                                        depending on the value in SortCriteria.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="PreferredHotelRating" type="s:double" minOccurs="0" maxOccurs="unbounded">
                                <s:annotation>
                                    <s:documentation>Hotels that match the star rating defined in this element can be weighed higher in the sort order when
                                        composite score sorting is enabled.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
                                <s:complexType>
                                    <s:sequence>
                                        <s:element name="ChildAge" type="s:int" minOccurs="0" maxOccurs="unbounded" />
                                    </s:sequence>
                                    <s:attribute name="adults" type="s:int" use="required" />
                                    <s:attribute name="children" type="s:int" />
                                </s:complexType>
                            </s:element>
                            <s:element name="CheckinDate" type="IsoDateType">
                                <s:annotation>
                                    <s:documentation>The date on which the guest will check in, in ISO format. E.g., 2008-1-28.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="CheckoutDate" type="IsoDateType">
                                <s:annotation>
                                    <s:documentation>The date on which the guest will check out, in ISO format. E.g., 2008-1-31.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="Rating" type="s:int" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>The desired rating from 1 to 5. Only hotels with a rating equal or greater than specified rating will be
                                        returned.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ShouldDoPackageSavingsSearch" type="s:boolean" minOccurs="0">
                                <s:annotation>
                                    <s:documentation>Flag designates whether or not we will do an additional room search to get the minimum standalone rate.
                                    </s:documentation>
                                </s:annotation>
                            </s:element>
                            <s:element name="ResponseDetails" minOccurs="0">
                                <s:complexType>
                                    <s:simpleContent>
                                        <s:extension base="ResponseDetailsType">
                                            <s:attribute name="WithTripAdvisor" default="true" type="s:boolean">
                                                <s:annotation>
                                                    <s:documentation>
                                                        This attribute specifies whether to show user review data or not.
                                                    </s:documentation>
                                                </s:annotation>
                                            </s:attribute>
                                            <s:attribute name="ResultsPerHotel" type="ResultsPerHotelType">
                                                <s:annotation>
                                                    <s:documentation>
                                                        This tag restricts number of results returned in a particular response. Possible values are
                                                        LOWEST_PRICED_HOTEL - returns the hotel with lowest price and ALL returns all hotels matching the
                                                        search criteria. Refer to ResponseDetailsType and ResultsPerHotelType below for all the possible
                                                        values for the respective types.
                                                    </s:documentation>
                                                </s:annotation>
                                            </s:attribute>
                                        </s:extension>
                                    </s:simpleContent>
                                </s:complexType>
                            </s:element>
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:simpleType name="ResponseDetailsType">
                <s:restriction base="s:string">
                    <s:enumeration value="FULL" />
                    <s:enumeration value="MINIMUM" />
                    <s:enumeration value="ROOM_DETAILS" />
                </s:restriction>
            </s:simpleType>
            <s:simpleType name="ResultsPerHotelType">
                <s:restriction base="s:string">
                    <s:enumeration value="ALL" />
                    <s:enumeration value="LOWEST_PRICED_ROOM" />
                </s:restriction>
            </s:simpleType>
            <s:complexType name="RoomSearchResponseType">
                <s:complexContent>
                    <s:extension base="SearchResponseType">
                        <s:sequence>
                            <s:element name="Hotel" type="HotelType" minOccurs="0" maxOccurs="unbounded" />
                        </s:sequence>
                    </s:extension>
                </s:complexContent>
            </s:complexType>
            <s:complexType name="RadiusSearchType">
                <s:annotation>
                    <s:documentation>The radius search from given location
                    </s:documentation>
                </s:annotation>
                <s:sequence>
                    <s:element name="GeoLocation" minOccurs="1">
                        <s:complexType>
                            <s:annotation>
                                <s:documentation>Specify the Latitude and Longitude coordinates
                                </s:documentation>
                            </s:annotation>
                            <s:sequence>
                                <s:element name="Latitude" minOccurs="1">
                                    <s:simpleType>
                                        <s:annotation>
                                            <s:documentation>Specify the north-south position of the point on the Earths surface
                                            </s:documentation>
                                        </s:annotation>
                                        <s:restriction base="s:double">
                                            <s:minInclusive value="-90" />
                                            <s:maxInclusive value="90" />
                                        </s:restriction>
                                    </s:simpleType>
                                </s:element>
                                <s:element name="Longitude" minOccurs="1">
                                    <s:simpleType>
                                        <s:annotation>
                                            <s:documentation>Specify the east-west position of a point on the Earths surface
                                            </s:documentation>
                                        </s:annotation>
                                        <s:restriction base="s:double">
                                            <s:minInclusive value="-180" />
                                            <s:maxInclusive value="180" />
                                        </s:restriction>
                                    </s:simpleType>
                                </s:element>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="Radius" minOccurs="0" default="10">
                        <s:simpleType>
                            <s:annotation>
                                <s:documentation>The maximum distance to search from the specified point
                                </s:documentation>
                            </s:annotation>
                            <s:restriction base="s:int">
                                <s:minInclusive value="1" />
                                <s:maxInclusive value="50" />
                            </s:restriction>
                        </s:simpleType>
                    </s:element>
                    <s:element name="Units" type="DistanceUnitsType" minOccurs="0" default="m" />
                </s:sequence>
            </s:complexType>
            <s:simpleType name="DistanceUnitsType">
                <s:annotation>
                    <s:documentation>Distance units
                    </s:documentation>
                </s:annotation>
                <s:restriction base="s:string">
                    <s:enumeration value="m" />
                    <s:enumeration value="km" />
                </s:restriction>
            </s:simpleType>
            <s:complexType name="SearchLocationType">
                <s:annotation>
                    <s:documentation>A search location may be specified in one or more of three ways. At least one of the child elements must be specified.
                        If more than one is specified, the server is free to choose which data to use.
                    </s:documentation>
                </s:annotation>
                <s:sequence>
                    <s:element name="Location" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A location can be specified as a free form string. It will be parsed and an attempt made to match it against (1)
                                an airport code; (2) an area or subarea as defined within the server; (3) A city, with an optional state or country qualifier,
                                such as "Paris, TX" or "Paris, FR", o a region. If the string can be interpreted unambiguously, it will be used. If no match
                                can be found, or the search is ambiguous, then an error will be returned.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="RegionId" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>A region is a collection of cities within Switchfly. There is a small set of known regions defined in Switchfly
                                that can be identified with an id.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="CityId" type="s:int" minOccurs="0">
                        <s:annotation>
                            <s:documentation>The Switchfly assigned id for the city being searched.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="AirportCode" type="s:string" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>One or more airport codes may be specified to indicate the destination to search. It depends on the source of
                                the inventory whether the search will benefit from multiple airport codes being specified. The Switchfly server will usually
                                be able to use just one airport code if searching externally. No guarantee is made for which of a collection of codes will be
                                used if only one can be used, so it is generally best to specify a single airport code.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="SearchType">
                <s:annotation>
                    <s:documentation />
                </s:annotation>
                <s:sequence>
                    <s:element name="SearchId" type="s:string" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If specified, this id will be returned with the associated search results.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="BookingComponent" type="BookingComponentType" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>Specifies the components that will go into the ultimate booking. This field may be repeated. At least one
                                component (the type of the search) is assumed. Specifying additional components specifies that the ultimate sale will be
                                packaged with opaque pricing and may allow the search to find special fares. The value must be one of: Activity,Air, Car,
                                Cruise, or Room.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="SpecialId" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If this element exists and is not zero, it represents a "special" in the client system for which this search is
                                being made. Most servers will ignore this field, but the server is free to make pricing decisions based on this value.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="SearchResponseType">
                <s:sequence>
                    <s:element name="SearchId" minOccurs="0">
                        <s:annotation>
                            <s:documentation>If specified in the search requests, the SearchId is echoed here.</s:documentation>
                        </s:annotation>
                    </s:element>
                    <s:element name="ErrorText" type="s:string" minOccurs="0" maxOccurs="unbounded">
                        <s:annotation>
                            <s:documentation>This holds error text that occurred while processing the request. Since availability might come from multiple
                                sources, errors can occur on some sources and other sources may work correctly, so the existence of errors does not mean there
                                are not usable results. However, it is an indicator of some issues on the server that should, perhaps, be investigated to get
                                better availability.
                            </s:documentation>
                        </s:annotation>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="SeatMapType">
                <s:sequence>
                    <s:element name="DepartureAirportCode" type="s:string" />
                    <s:element name="ArrivalAirportCode" type="s:string" />
                    <s:element name="DepartureDate" type="IsoDateType" />
                    <s:element name="Airline" type="s:string" />
                    <s:element name="Flight" type="s:string" />
                    <s:element name="Cabin" type="s:string" />
                    <s:element name="Columns">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Column">
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="Name" type="s:string" />
                                            <s:element name="Aisle" type="s:boolean" />
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                    <s:element name="FirstRowOverWing" type="s:int" />
                    <s:element name="LastRowOverWing" type="s:int" />
                    <s:element name="Rows">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="Row">
                                    <s:complexType>
                                        <s:sequence>
                                            <s:element name="Number" type="s:int" />
                                            <s:element name="ExitRowLeft" type="s:boolean" />
                                            <s:element name="ExitRowRight" type="s:boolean" />
                                            <s:element name="Seats">
                                                <s:complexType>
                                                    <s:sequence>
                                                        <s:element name="Seat">
                                                            <s:complexType>
                                                                <s:sequence>
                                                                    <s:element name="Column" type="s:string" />
                                                                    <s:element name="IsAvailable" type="s:boolean" />
                                                                    <s:element name="IsPreferential" type="s:boolean" />
                                                                </s:sequence>
                                                            </s:complexType>
                                                        </s:element>
                                                    </s:sequence>
                                                </s:complexType>
                                            </s:element>
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>
            <s:complexType name="SearchActivityType">
                <s:sequence>
                    <s:element name="CrsName" type="NonEmptyString" />
                    <s:element name="ActivityCodes">
                        <s:complexType>
                            <s:sequence>
                                <s:element name="ActivityCode" type="NonEmptyString" maxOccurs="unbounded" />
                            </s:sequence>
                        </s:complexType>
                    </s:element>
                </s:sequence>
            </s:complexType>';
		return $aux;
	}
	function common_objects(){
		$aux = '
                <s:element name="EzRezGenericRS" type="SwitchflyRSType"/>
                <s:element name="PingRQ">
                    <s:annotation>
                        <s:documentation>This message can be used to test if the server is up with minimal processing.</s:documentation>
                    </s:annotation>
                    <s:complexType>
                        <s:complexContent>
                            <s:extension base="SwitchflyRQType">
                                <s:sequence>
                                    <s:element name="Token" type="s:string" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>This element is optional and will be echoed if present.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                </s:sequence>
                            </s:extension>
                        </s:complexContent>
                    </s:complexType>
                </s:element>
                <s:element name="PingRS">
                    <s:annotation>
                        <s:documentation/>
                    </s:annotation>
                    <s:complexType>
                        <s:complexContent>
                            <s:extension base="SwitchflyRSType">
                                <s:sequence>
                                    <s:element name="Token" type="s:string" minOccurs="0">
                                        <s:annotation>
                                            <s:documentation>This element echoes the token passed in the request.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                </s:sequence>
                            </s:extension>
                        </s:complexContent>
                    </s:complexType>
                </s:element>
                <s:simpleType name="AgeClassType">
                    <s:restriction base="s:string">
                        <s:enumeration value="ADULT"/>
                        <s:enumeration value="CHILD"/>
                        <s:enumeration value="SENIOR"/>
                        <s:enumeration value="INFANT_IN_LAP"/>
                        <s:enumeration value="INFANT_REQUIRING_SEAT"/>
                    </s:restriction>
                </s:simpleType>
                <s:simpleType name="BookingComponentType">
                    <s:restriction base="s:string">
                        <s:enumeration value="Activity"/>
                        <s:enumeration value="Air"/>
                        <s:enumeration value="Car"/>
                        <s:enumeration value="Cruise"/>
                        <s:enumeration value="Room"/>
                    </s:restriction>
                </s:simpleType>
                <s:simpleType name="CurrencyAmountType">
                    <s:restriction base="s:double"/>
                </s:simpleType>
                <s:simpleType name="CurrencyCodeType">
                    <s:restriction base="s:string">
                        <s:length value="3"/>
                    </s:restriction>
                </s:simpleType>
                <s:simpleType name="IsoDateType">
                    <s:annotation>
                        <s:documentation>yy-mm-dd</s:documentation>
                    </s:annotation>
                    <s:restriction base="s:string"/>
                </s:simpleType>
                <s:simpleType name="IsoTimeType">
                    <s:annotation>
                        <s:documentation>hh:mm:ss</s:documentation>
                    </s:annotation>
                    <s:restriction base="s:string"/>
                </s:simpleType>
                <s:simpleType name="ProductType">
                    <s:restriction base="s:string">
                        <s:enumeration value="room"/>
                        <s:enumeration value="car"/>
                        <s:enumeration value="air"/>
                        <s:enumeration value="activity"/>
                        <s:enumeration value="booking fee"/>
                        <s:enumeration value="discount"/>
                        <s:enumeration value="insurance"/>
                        <s:enumeration value="merchandise"/>
                        <s:enumeration value="master_supplier"/>
                        <s:enumeration value="api"/>
                    </s:restriction>
                </s:simpleType>
                <s:simpleType name="IsoTimestampType">
                    <s:annotation>
                        <s:documentation>yyyy-mm-dd-hh:mm:ss</s:documentation>
                    </s:annotation>
                    <s:restriction base="s:string"/>
                </s:simpleType>
                <s:complexType name="EzRezMessageType"/>
                <s:complexType name="SwitchflyPaymentRQType">
                    <s:annotation>
                        <s:documentation/>
                    </s:annotation>
                    <s:complexContent>
                        <s:extension base="SwitchflyRQType">
                            <s:sequence>
                                <s:element name="DeviceFingerprint" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>If this tag is specified, it defines a device fingerprint for the customer on whose behalf this API call
                                            is made. (see http://en.wikipedia.org/wiki/Device_fingerprint) This is sued during fraud screening.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="PaymentOptions" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>You have to chose one PaymentOption element.
                                        </s:documentation>
                                    </s:annotation>
                                    <s:complexType>
                                        <s:all>
                                            <s:element name="PaymentOption" type="PaymentOptionType"/>
                                        </s:all>
                                    </s:complexType>
                                </s:element>
                            </s:sequence>
                        </s:extension>
                    </s:complexContent>
                </s:complexType>

                <s:complexType name="SwitchflyRQTypeAv">
                    <s:annotation>
                        <s:documentation/>
                    </s:annotation>
                    <s:complexContent>
                        <s:extension base="EzRezMessageType">
                            <s:sequence>
                                <s:element name="UserId" type="s:string">
                                    <s:annotation>
                                        <s:documentation>A pre-assigned id used for accessing this Switchfly system.</s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="Password" type="s:string">
                                    <s:annotation>
                                        <s:documentation>The associated password. Note that the password is passed in clear text, so communication must be done
                                            over a secure socket.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="Cobrand" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>The cobrand. This value should be provided by the company operating the server to which this message is
                                            sent. If no cobrand is specified the "default" cobrand will be used.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="LanguageId" type="s:int" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>This is the language id for a language defined within Switchfly. This value should be provided by the
                                            company operating the server to which this message is sent. If not specified, the language for the active cobrand is
                                            used.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="Currency" type="CurrencyCodeType" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>This tag specifies the currency of the prices returned in the response. An error will be thrown if the
                                            currency is not a supported customer currency in the server.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="Debug" type="s:boolean" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>If this tag is specified with true as its value, then debugging information about this message will be
                                            saved on the server. For general use this value should be omitted or set to false. However when debugging certain
                                            situations in conjunction with Switchfly they may ask you to set this value to true in order to capture details about
                                            the message interactions.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="CustomerIp" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>If this tag is specified, it defines the internet address of the customer on whose behalf this API call
                                            is made. The Switchfly system detects, uses, and records the IP address during the search and booking processes and it
                                            can affect pricing and fraud screening and is ultimately recorded with the booking. When processing an API call the
                                            address detected by Switchfly is the address of the API client rather than that of the customer the API client is
                                            serving, so this tag allows Switchfly to get the correct address for more accurate processing.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="ClientMarker" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>If this tag is specified, it tags the specific request within Switchfly with this marker string for
                                            later analysis. Recommend to use a GUID.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element minOccurs="0" name="ClientExtensions">
                                    <s:annotation>
                                        <s:documentation>The element allows for custom, pre-agreed upon elements between Switchfly and their client-partners.
                                        </s:documentation>
                                    </s:annotation>
                                    <s:complexType>
                                        <s:sequence>
                                            <s:any maxOccurs="unbounded" minOccurs="0" processContents="lax"/>
                                        </s:sequence>
                                    </s:complexType>
                                </s:element>
                                <s:element name="StubScenario" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>The Query String that should be used by the Stub Framework. Value should be formatted the same as the
                                            Query String that is used to invoke the Stub Framework from a URL on Switchfly web pages.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                            </s:sequence>
                            <s:attribute name="Version" use="required">
                                <s:simpleType>
                                    <s:restriction base="s:string">
                                        <s:enumeration value="16.3"/>
                                    </s:restriction>
                                </s:simpleType>
                            </s:attribute>
                        </s:extension>
                    </s:complexContent>
                </s:complexType>
                <s:complexType name="FailureType">
                    <s:simpleContent>
                        <s:extension base="s:string">
                            <s:attribute name="type">
                                <s:annotation>
                                    <s:documentation>This defines the category of the error. It currently is little used and is either PAYMENT or missing.
                                    </s:documentation>
                                </s:annotation>
                            </s:attribute>
                            <s:attribute name="code" type="s:string">
                                <s:annotation>
                                    <s:documentation>This defines the error. To see the current list of error codes, look at the list of error translations in
                                        your Switchfly system. This code will always be present and be one of those predefined codes.
                                    </s:documentation>
                                </s:annotation>
                            </s:attribute>
                            <s:attribute name="translation" type="s:string">
                                <s:annotation>
                                    <s:documentation>This is the translation of the error using the translation using the client translation tables based on the
                                        language active during the api call.
                                    </s:documentation>
                                </s:annotation>
                            </s:attribute>
                        </s:extension>
                    </s:simpleContent>
                </s:complexType>
                <s:complexType name="SwitchflyRSType">
                    <s:annotation>
                        <s:documentation/>
                    </s:annotation>
                    <s:complexContent>
                        <s:extension base="EzRezMessageType">
                            <s:sequence>
                                <s:element name="LogName" type="s:string" minOccurs="0" maxOccurs="unbounded">
                                    <s:annotation>
                                        <s:documentation>The name of a log file associated with this message. This can be used when corresponding with Switchfly
                                            about API questions. Multiple logs can be generated.
                                        </s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:element name="ClientMarker" type="s:string" minOccurs="0">
                                    <s:annotation>
                                        <s:documentation>Echoed back client marker string passed in the request. Normally a GUID.</s:documentation>
                                    </s:annotation>
                                </s:element>
                                <s:choice>
                                    <s:element name="Success" type="s:string">
                                        <s:annotation>
                                            <s:documentation>Indicates successful message completion if tag is present. Tag has no contents.</s:documentation>
                                        </s:annotation>
                                    </s:element>
                                    <s:element name="Failure" type="FailureType"/>
                                </s:choice>
                            </s:sequence>
                        </s:extension>
                    </s:complexContent>
                </s:complexType>
                <s:complexType name="AddressType">
                    <s:annotation>
                        <s:documentation/>
                    </s:annotation>
                    <s:sequence>
                        <s:element name="AddressLine" type="s:string" minOccurs="0" maxOccurs="unbounded">
                            <s:annotation>
                                <s:documentation>Free format address line</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="City" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The name of the city.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="State" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The two letter state code for US and Canada. For other countries, this may be a state code or a spelled out
                                    state name.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="Zip" type="s:string" minOccurs="0"/>
                        <s:element name="Country" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Where possible, a two letter country code is returned or expected for addresses.</s:documentation>
                            </s:annotation>
                        </s:element>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="CreditCardType">
                    <s:sequence>
                        <s:element name="Type" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The type is determined from the number so the type need not be specified. If specified and it does not match the
                                    type determined from the number, an error will be generated. AMERICAN_EXPRESS = "AX", CARTE_BLANCHE = "CB", DINERS_CLUB =
                                    "DC", DISCOVER = "DS", ENROUTE = "EN", JCB = "JC", MASTERCARD = "CA", VISA = "VI"
                                </s:documentation>
                            </s:annotation>
                            <s:simpleType>
                                <s:restriction base="s:string">
                                    <s:enumeration value="AX"/>
                                    <s:enumeration value="CB"/>
                                    <s:enumeration value="DC"/>
                                    <s:enumeration value="DS"/>
                                    <s:enumeration value="EN"/>
                                    <s:enumeration value="JC"/>
                                    <s:enumeration value="LS"/>
                                    <s:enumeration value="CA"/>
                                    <s:enumeration value="VI"/>
                                </s:restriction>
                            </s:simpleType>
                        </s:element>
                        <s:element name="Number" type="s:string"/>
                        <s:element name="LastName" type="s:string"/>
                        <s:element name="FirstName" type="s:string"/>
                        <s:element name="Address" type="AddressType"/>
                        <s:element name="Expiration" type="s:string">
                            <s:annotation>
                                <s:documentation>ISO date format. (2007-11)</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="Email" type="s:string" minOccurs="0"/>
                        <s:element name="SecurityCode" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>To help guard against fraud, the major credit card companies have implemented a system to ensure the credit card
                                    used in a transaction is actually possessed by the user. This system may be variously called CVV2 (Visa), CVC2 (MasterCard),
                                    or CID (American Express). The verification number is a non-embossed number printed on the credit card.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="Phone" type="s:string" minOccurs="0"/>
                        <s:element name="ParcelId" type="s:integer" minOccurs="0"/>
                        <s:element name="GovernmentId" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Some clients collect a government id (e.g., CPF for Brazil, Social Security Number for US). The type of id
                                    collected can differ according to country or client.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ProcessPaymentAmount" type="s:double" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="BankTransferType">
                    <s:sequence>
                        <s:element name="BankId" type="s:integer"/>
                        <s:element name="CallBackURLSuccess" type="s:string" minOccurs="0"/>
                        <s:element name="CallBackURLPending" type="s:string" minOccurs="0"/>
                        <s:element name="CallBackURLFailure" type="s:string" minOccurs="0"/>
                        <s:element name="ProcessPaymentAmount" type="s:double" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="MiscType">
                    <s:sequence>
                        <s:element name="PaymentMemo" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Payment memo. Can be used to store a client reference to the payment.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="ProcessPaymentAmount" type="s:double" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="PaymentOptionType">
                    <s:choice>
                        <s:element name="CreditCard" type="CreditCardType"/>
                        <s:element name="BankTransfer" type="BankTransferType"/>
                        <s:element name="Miscellaneous" type="MiscType"/>
                    </s:choice>
                </s:complexType>
                <s:simpleType name="FraudScreenStatusType">
                    <s:annotation>
                        <s:documentation>The result of fraud screening in the server. It can be ACCEPT, DECLINE, or REVIEW if screening is still in progress. If
                            fraud screening is not enabled in the server, it will default to ACCEPT.
                        </s:documentation>
                    </s:annotation>
                    <s:restriction base="s:string">
                        <s:enumeration value="ACCEPT"/>
                        <s:enumeration value="DECLINE"/>
                        <s:enumeration value="REVIEW"/>
                    </s:restriction>
                </s:simpleType>
                <s:complexType name="InternalRoomCustomFieldType">
                    <s:sequence>
                        <s:element name="Id" type="s:int"/>
                        <s:element name="Name" type="s:string" minOccurs="0"/>
                        <s:element name="Value" type="s:string" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="InternalSupplierType">
                    <s:annotation>
                        <s:documentation>Base type for a supplier (SupplierId and SupplierName)</s:documentation>
                    </s:annotation>
                    <s:sequence>
                        <s:element name="Product" type="ProductType">
                            <s:annotation>
                                <s:documentation>The type of supplier (e.g. room)</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="PropertyType" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The type of property, currently only used for Hotel suppliers.</s:documentation>
                            </s:annotation>
                            <s:simpleType>
                                <s:restriction base="s:string">
                                    <s:enumeration value="hotel"/>
                                    <s:enumeration value="bandb"/>
                                    <s:enumeration value="boutique"/>
                                    <s:enumeration value="condo"/>
                                    <s:enumeration value="vacation_rental"/>
                                </s:restriction>
                            </s:simpleType>
                        </s:element>
                        <s:element name="SupplierId" type="s:int" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Unique id of the supplier</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SupplierName" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Name of the supplier specified in the Switchfly Admin Portal</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="NotificationMethod" minOccurs="0">
                            <s:annotation>
                                <s:documentation>email or fax (required)</s:documentation>
                            </s:annotation>
                            <s:simpleType>
                                <s:restriction base="s:string">
                                    <s:enumeration value="email"/>
                                    <s:enumeration value="fax"/>
                                </s:restriction>
                            </s:simpleType>
                        </s:element>
                        <s:element name="NotificationEmail" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Email address of the supplier (one form of notification is required)</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="NotificationFax" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Fax number of the supplier (one form of notification is required)</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SupplierCode" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Unique identifier used by partners</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="AgencyCode" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Used by supplier to track the booking channel</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="CurrencyCode" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The currency of the rates (aka supplier currency). This should be a valid three letter code set up in the Admin
                                    Portal.
                                </s:documentation>
                            </s:annotation>
                            <s:simpleType>
                                <s:restriction base="s:string">
                                    <s:maxLength value="3" fixed="true"/>
                                    <s:minLength value="3" fixed="true"/>
                                </s:restriction>
                            </s:simpleType>
                        </s:element>
                        <s:element name="SupplierOnlyBookingMemo" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Memo field sent to the supplier during booking in the notification message.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SupplierAddress" type="AddressType" minOccurs="0">
                            <s:annotation>
                                <s:documentation>The address of the supplier.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SupplierPhone" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Phone number for the supplier.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SubAreaId" type="s:int" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Search subarea id where the hotel can be found. This is configured through Admin Portal.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="SubAreaId2" type="s:int" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Secondary search subarea id where the hotel can be found. This is configured through Admin Portal.
                                </s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="MasterSupplier" type="InternalSupplierType" minOccurs="0">
                            <s:annotation>
                                <s:documentation>If a segment is associated to a Master Supplier, it will be populated at this element.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="AirportCode" type="s:string" minOccurs="0">
                            <s:annotation>
                                <s:documentation/>
                            </s:annotation>
                        </s:element>
                        <s:element name="ShouldSendNetsOnManifest" type="s:boolean" default="false" minOccurs="0">
                            <s:annotation>
                                <s:documentation>Flag to determine if net rates are included in the notification email or fax to the supplier.</s:documentation>
                            </s:annotation>
                        </s:element>
                        <s:element name="CheckinTime" type="IsoTimeType" minOccurs="0"/>
                        <s:element name="CheckoutTime" type="IsoTimeType" minOccurs="0"/>
                        <s:element name="Translations" minOccurs="0">
                            <s:complexType>
                                <s:sequence>
                                    <s:element name="Translation" type="HotelTranslationType" minOccurs="0" maxOccurs="unbounded"/>
                                </s:sequence>
                            </s:complexType>
                        </s:element>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="HotelTranslationType">
                    <s:sequence>
                        <s:element name="LanguageId" type="s:int" minOccurs="1"/>
                        <s:element name="HotelAmenities" type="s:string" minOccurs="0"/>
                        <s:element name="HotelDescription" type="s:string" minOccurs="0"/>
                        <s:element name="RoomDescription" type="s:string" minOccurs="0"/>
                        <s:element name="RoomAmenities" type="s:string" minOccurs="0"/>
                        <s:element name="Directions" type="s:string" minOccurs="0"/>
                        <s:element name="Restaurants" type="s:string" minOccurs="0"/>
                        <s:element name="Entertainment" type="s:string" minOccurs="0"/>
                        <s:element name="Policies" type="s:string" minOccurs="0"/>
                        <s:element name="AdditionalInfo" type="s:string" minOccurs="0"/>
                        <s:element name="SpecialNotes" type="s:string" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="RoomTypeTranslationType">
                    <s:sequence>
                        <s:element name="LanguageId" type="s:int" minOccurs="1"/>
                        <s:element name="RoomType" type="s:string" minOccurs="1"/>
                        <s:element name="RoomTypeDescription" type="s:string" minOccurs="1"/>
                    </s:sequence>
                </s:complexType>
                <s:complexType name="CustomFieldType">
                    <s:sequence>
                        <s:element name="Id" type="s:int" minOccurs="0"/>
                        <s:element name="Name" type="s:string" minOccurs="0"/>
                        <s:element name="Value" type="s:string" minOccurs="0"/>
                    </s:sequence>
                </s:complexType>
                <s:simpleType name="NonEmptyString">
                    <s:restriction base="s:string">
                        <s:minLength value="1"/>
                        <s:pattern value=".*[^\s].*"/>
                    </s:restriction>
                </s:simpleType>';
		return $aux;
	}
	function definitions(){
		$aux = '</s:schema>
			</wsdl:types>
			<wsdl:message name="AvailabilityRQ">
				<wsdl:part name="parameters" element="tns:AvailabilityRQ"/>
			</wsdl:message>
			<wsdl:message name="AvailabilityRS">
				<wsdl:part name="parameters" element="tns:AvailabilityRS"/>
			</wsdl:message>
			<wsdl:message name="CreatePnrRQ">
				<wsdl:part name="parameters" element="tns:CreatePnrRQ"/>
			</wsdl:message>
			<wsdl:message name="CreatePnrRS">
				<wsdl:part name="parameters" element="tns:CreatePnrRS"/>
			</wsdl:message>
			<wsdl:message name="CancelPnrRQ">
				<wsdl:part name="parameters" element="tns:CancelPnrRQ"/>
			</wsdl:message>
			<wsdl:message name="CancelPnrRS">
				<wsdl:part name="parameters" element="tns:CancelPnrRS"/>
			</wsdl:message>
			<wsdl:portType name="switchflyportType">
				<wsdl:operation name="Availability">
					<wsdl:input message="tns:AvailabilityRQ"/>
					<wsdl:output message="tns:AvailabilityRS"/>
				</wsdl:operation>
				<wsdl:operation name="CreatePnr">
					<wsdl:input message="tns:CreatePnrRQ"/>
					<wsdl:output message="tns:CreatePnrRS"/>
				</wsdl:operation>
				<wsdl:operation name="CancelPnr">
					<wsdl:input message="tns:CancelPnrRQ"/>
					<wsdl:output message="tns:CancelPnrRS"/>
				</wsdl:operation>
			</wsdl:portType>
			<wsdl:binding name="switchflyBinding" type="tns:switchflyportType">
				<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
				<wsdl:operation name="Availability">
						<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/test.php#Availability" style="document"/>
						<wsdl:input>
							<soap:body use="literal"/>
						</wsdl:input>
						<wsdl:output>
							<soap:body use="literal"/>
						</wsdl:output>
						<wsdl:documentation>Availability</wsdl:documentation>
				</wsdl:operation>
				<wsdl:operation name="CreatePnr">
						<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/test.php#CreatePnr" style="document"/>
						<wsdl:input>
							<soap:body use="literal"/>
						</wsdl:input>
						<wsdl:output>
							<soap:body use="literal"/>
						</wsdl:output>
						<wsdl:documentation>CreatePnr</wsdl:documentation>
				</wsdl:operation>
				<wsdl:operation name="CancelPnr">
						<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/test.php#CancelPnr" style="document"/>
						<wsdl:input>
							<soap:body use="literal"/>
						</wsdl:input>
						<wsdl:output>
							<soap:body use="literal"/>
						</wsdl:output>
						<wsdl:documentation>CancelPnr</wsdl:documentation>
				</wsdl:operation>
			</wsdl:binding>
			<wsdl:service name="DistantisServices">
				<wsdl:port name="switchflyPort" binding="tns:switchflyBinding">
					<soap:address location="http://otas.distantis.com/otas/webservice/v1.2/test.php"/>
				</wsdl:port>
			</wsdl:service>
			</wsdl:definitions>';
		return $aux;
	}
	
	$str =  cabeza();
	$str .=  booking_api();
	$str .=  booking_objects();
	$str .=  search_objects();
	$str .=  common_objects();
	$str .=  definitions();
	if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
    if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
    if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
    return $str;
}
?>