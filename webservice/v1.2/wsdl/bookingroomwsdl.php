<?php
$functions = array();

    $serviceName = "Distantis Services";
	$functions[] = array("funcName" => "CreatePnr",
                        "doc" => "CreatePnr",
                        "inputParams" => array(array("name" => "UserId", "type" => "string"),
                                                array("name" => "Password", "type" => "string"),
												array("name" => "Currency", "type" => "string"),
												array("name" => "PnrOwner", "type" => "PnrOwner"),
												array("name" => "LeadTraveler", "type" => "LeadTraveler"),
												array("name" => "RoomSegmentRequest", "type" => "RoomSegmentRequest"),
												array("name" => "ClientReservation", "type" => "string")),
						"PnrOwner" => array(array("name" => "LastName", "type" => "string"),
										array("name" => "FirstName", "type" => "string"),
										array("name" => "Age", "type" => "string"),
										array("name" => "AgeClass", "type" => "string"),
										array("name" => "Phone", "type" => "string"),
										array("name" => "Address", "type" => "string"),
										array("name" => "Email", "type" => "string"),
										array("name" => "BookingMemos", "type" => "string")),
						"LeadTraveler" => array(array("name" => "LastName", "type" => "string"),
										array("name" => "FirstName", "type" => "string"),
										array("name" => "Gender", "type" => "string"),
										array("name" => "OfficialTravelerId", "type" => "string"),
										array("name" => "OfficialTravelerIdLabel", "type" => "string"),
										array("name" => "OfficialTravelerIdCountry", "type" => "string"),
										array("name" => "Age", "type" => "string"),
										array("name" => "AgeClass", "type" => "string"),
										array("name" => "Title", "type" => "string"),
										array("name" => "Phone", "type" => "string"),
										array("name" => "Address", "type" => "string"),
										array("name" => "Email", "type" => "string"),
										array("name" => "BookingMemos", "type" => "string")),
						"PhoneAttributes" => array(
									array("name" => "Type", "type"=> "string")),
						"RoomSegmentRequest" => array(array("name" => "CorrelationId", "type" => "string"),
										array("name" => "RoomSegment", "type" => "string"),
										array("name" => "RoomingList", "type" => "RoomingList")),
						"RoomingList" => array(array("name" => "Guest", "type" => "Guest")),
						"Guest" => array(array("name" => "FirstName", "type" => "string"),
										array("name" => "LastName", "type" => "string"),
										array("name" => "Title", "type" => "string"),
										array("name" => "Age", "type" => "string"),
										array("name" => "OfficialTravelerId", "type" => "string"),
										array("name" => "OfficialTravelerIdLabel", "type" => "string"),
										array("name" => "OfficialTravelerIdCountry", "type" => "string")),
                        "outputParams" => array(
									array("name" => "Success", "type"=> "string"),
									array("name"=>"Segment","type"=>"Segment"),
									array("name"=>"Pnr","type"=>"Pnr")),
						"Segment" => array(array("name" => "SegmentLocator", "type" => "string"),
										array("name" => "Currency", "type" => "string"),
										array("name" => "TotalPrice", "type" => "string")),
						"Pnr" => array(array("name" => "RecordLocator", "type" => "string"),
										array("name" => "BookingTimestamp", "type" => "string"),
										array("name" => "Status", "type" => "string"),
										array("name" => "SegmentTotals", "type" => "SegmentTotals"),
										array("name" => "DepartureDate", "type" => "string"),
										array("name" => "PnrOwner", "type" => "PnrOwner"),
										array("name" => "LeadTraveler", "type" => "LeadTraveler"),
										array("name" => "CustomerPricingSummary", "type" => "CustomerPricingSummary")),
						"SegmentTotals" => array(array("name" => "AirTotal", "type" => "string"),
										array("name" => "AirTaxTotal", "type" => "string"),
										array("name" => "RoomTotal", "type" => "string"),
										array("name" => "RoomTaxTotal", "type" => "string"),
										array("name" => "CarTotal", "type" => "string"),
										array("name" => "CarTaxTotal", "type" => "string"),
										array("name" => "ActivityTotal", "type" => "string"),
										array("name" => "ActivityTaxTotal", "type" => "string"),
										array("name" => "CruiseTotal", "type" => "string"),
										array("name" => "CruiseTaxTotal", "type" => "string"),
										array("name" => "InsuranceTotal", "type" => "string"),
										array("name" => "InsuranceTaxTotal", "type" => "string"),
										array("name" => "MerchandiseTotal", "type" => "string"),
										array("name" => "MerchandiseTaxTotal", "type" => "string"),
										array("name" => "PassThroughMarkupTotal", "type" => "string"),
										array("name" => "BookingFeeClient", "type" => "string"),
										array("name" => "BookingFeeTotal", "type" => "string"),
										array("name" => "ServiceChargeTotal", "type" => "string"),
										array("name" => "AgentMarkup", "type" => "string"),
										array("name" => "DiscountTotal", "type" => "string")),
						"CustomerPricingSummary" => array(array("name" => "Currency", "type" => "string"),
										array("name" => "PackageTotal", "type" => "string"),
										array("name" => "PaymentTotal", "type" => "string"),
										array("name" => "BalanceDueTotal", "type" => "string")),
                        "soapAddress" => "http://otas.distantis.com/otas/webservice/v1.2/bookingroom.php"
                         );

    if (stristr($_SERVER['QUERY_STRING'], "wsdl")) {
    	// WSDL request - output raw XML
		header("Content-Type: application/soap+xml; charset=utf-8");
        echo DisplayXML();
    } else {
    	// Page accessed normally - output documentation
    	$cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1); // Current page
    	echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
    	echo '<html>';
    	echo '<head><title>' . $serviceName . '</title></head>';
    	echo '<body>';
    	echo '<h1>' . $serviceName . '</h1>';
        echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $cp . '?WSDL</code></p>';
    	
        // Document each function
        echo '<h2>Available Functions:</h2>';
        echo '<div style="margin-left:20px;">';
        for ($i=0;$i<count($functions);$i++) {
            echo '<h3>Function: ' . $functions[$i]['funcName'] . '</h3>';
            echo '<div style="margin-left:20px;">';
            echo '<p>';
            echo $functions[$i]['doc'];
            echo '<ul>';
            if (array_key_exists("inputParams", $functions[$i])) {
            	echo '<li>Input Parameters:<ul>';
            	for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
            	   	echo '<li>' . $functions[$i]['inputParams'][$j]['name'];
            	   	echo ' (' . $functions[$i]['inputParams'][$j]['type'];
            	   	echo ')</li>';
            	}
            	echo '</ul></li>';
            }
            if (array_key_exists("outputParams", $functions[$i])) {
                echo '<li>Output Parameters:<ul>';
                for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
                    echo '<li>' . $functions[$i]['outputParams'][$j]['name'];
                    echo ' (' . $functions[$i]['outputParams'][$j]['type'];
                    echo ')</li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
            echo '</p>';
            echo '</div>';
        }
        echo '</div>';
        	
    	echo '<h2>WSDL output:</h2>';
        echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
        echo DisplayXML(false);
        echo '</pre>';
        echo '</body></html>';
    }
                         
    exit; 
    
/*****************************************************************************
 * Create WSDL XML 
 * @PARAM xmlformat=true - Display output in HTML friendly format if set false
 *****************************************************************************/
function DisplayXML($xmlformat=true) {
	global $functions;         // Functions that this web service supports
	global $serviceName;       // Web Service ID
	$i = 0;                    // For traversing functions array
	$j = 0;                    // For traversing parameters arrays
	$str = '';                 // XML String to output
	
	// Tab spacings
	$t1 = '    ';
	if (!$xmlformat) $t1 = '&nbsp;&nbsp;&nbsp;&nbsp;';
	$t2 = $t1 . $t1;
	$t3 = $t2 . $t1;
	$t4 = $t3 . $t1;
	$t5 = $t4 . $t1;
	
	$serviceID = str_replace(" ", "", $serviceName);
	
	// Declare XML format
    $str .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n\n";
    
    // Declare definitions / namespaces
    $str .= '<wsdl:definitions ' . "\n"; 
    $str .= $t1 . 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" ' . "\n";
    $str .= $t1 . 'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' . "\n";
    $str .= $t1 . 'xmlns:s="http://www.w3.org/2001/XMLSchema" ' . "\n"; 
    $str .= $t1 . 'targetNamespace="http://www.darkerwhite.com/" ' . "\n";
    $str .= $t1 . 'xmlns:tns="http://www.darkerwhite.com/" ' . "\n"; 
    $str .= $t1 . 'name="' . $serviceID . '" ' . "\n";
    $str .= '>' . "\n\n";
	
	// Declare Types / Schema
    $str .= '<wsdl:types>' . "\n";
    $str .= $t1 . '<s:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">' . "\n";
    for ($i=0;$i<count($functions);$i++) {
    	// Define Request Types
    	if (array_key_exists("inputParams", $functions[$i])) {
    	   $str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'RQ">' . "\n";
    	   $str .= $t3 . '<s:complexType><s:sequence>' . "\n";		    
    	   for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
			   $formato="s";
			   $maxoccur="1";
			   if($functions[$i]['inputParams'][$j]['type']<>"string" && $functions[$i]['inputParams'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
    	       $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
    	       $str .= 'name="' . $functions[$i]['inputParams'][$j]['name'] . '" ';
    	       $str .= 'type="'.$formato.':' . $functions[$i]['inputParams'][$j]['type'] . '" />' . "\n";	
    	   }
    	   $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
           $str .= $t2 . '</s:element>' . "\n";
    	}
		if (array_key_exists("PnrOwner", $functions[$i])) {
			$str .= $t2 . '<s:complexType  name="PnrOwner"><s:sequence>'."\n";
			for ($j=0;$j<count($functions[$i]['PnrOwner']);$j++) {
				$auxiliar = $functions[$i]['PnrOwner'][$j]['name'].'Attributes';
				if(array_key_exists($auxiliar, $functions[$i])){
					$str .= $t3. '<s:element minOccurs="1" maxOccurs="unbounded" name="'.$functions[$i]['PnrOwner'][$j]['name'].'" >'."\n";
					$str .= $t4. '<s:complexType><s:simpleContent><s:extension base="s:string">'."\n";
					for($xx=0;$xx<count($functions[$i][$auxiliar]);$xx++){
						$str .= $t5. '<s:attribute name="'.$functions[$i][$auxiliar][$xx]['name'].'" type="'.$functions[$i][$auxiliar][$xx]['type'].'" /> '."\n";
					}					
					$str .= $t4. '</s:extension></s:simpleContent></s:complexType>'."\n";
					$str .= $t3. '</s:element>'."\n";
				}else{
					$str .= $t3 . '<s:element minOccurs="1" maxOccurs="1" ';
					$str .= 'name="'.$functions[$i]['PnrOwner'][$j]['name'].'" ';
					$str .= 'type="s:'.$functions[$i]['PnrOwner'][$j]['type'].'" />' . "\n";
				}
			}
			$str .= $t2.'</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("LeadTraveler", $functions[$i])) {
			$str .= $t2 . '<s:complexType  name="LeadTraveler"><s:sequence>'."\n";
			for ($j=0;$j<count($functions[$i]['LeadTraveler']);$j++) {
				$auxiliar = $functions[$i]['LeadTraveler'][$j]['name'].'Attributes';
				if(array_key_exists($auxiliar, $functions[$i])){
					$str .= $t3. '<s:element minOccurs="1" maxOccurs="unbounded" name="'.$functions[$i]['LeadTraveler'][$j]['name'].'" >'."\n";
					$str .= $t4. '<s:complexType><s:simpleContent><s:extension base="s:string">'."\n";
					for($xx=0;$xx<count($functions[$i][$auxiliar]);$xx++){
						$str .= $t5. '<s:attribute name="'.$functions[$i][$auxiliar][$xx]['name'].'" type="'.$functions[$i][$auxiliar][$xx]['type'].'" /> '."\n";
					}					
					$str .= $t4. '</s:extension></s:simpleContent></s:complexType>'."\n";
					$str .= $t3. '</s:element>'."\n";
				}else{
					$str .= $t3 . '<s:element minOccurs="1" maxOccurs="1" ';
					$str .= 'name="'.$functions[$i]['LeadTraveler'][$j]['name'].'" ';
					$str .= 'type="s:'.$functions[$i]['LeadTraveler'][$j]['type'].'" />' . "\n";
				}
			}
			$str .= $t2.'</s:sequence></s:complexType>' . "\n";
		}

		if (array_key_exists("RoomSegmentRequest", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomSegmentRequest"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomSegmentRequest']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomSegmentRequest'][$j]['type']<>"string" && $functions[$i]['RoomSegmentRequest'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomSegmentRequest'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomSegmentRequest'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomingList", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomingList"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomingList']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomingList'][$j]['type']<>"string" && $functions[$i]['RoomingList'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomingList'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomingList'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("Guest", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="Guest"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['Guest']);$j++) {
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="'.$functions[$i]['Guest'][$j]['name'].'" ';
				$str .= 'type="s:'.$functions[$i]['Guest'][$j]['type'].'" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("outputParams", $functions[$i])) {
			$str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'RS">' . "\n";
			$str .= $t3 . '<s:complexType ><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
				$formato="s";
				$maxoccur="1";
				if($functions[$i]['outputParams'][$j]['type']<>"string" 
					&& $functions[$i]['outputParams'][$j]['type']<>"integer")
						{$formato="tns"; $maxoccur="unbounded";}
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
				$str .= 'name="' . $functions[$i]['outputParams'][$j]['name'] . '" ';
				$str .= 'type="'.$formato.':' . $functions[$i]['outputParams'][$j]['type'] . '" />' . "\n";
           }
           $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
           $str .= $t2 . '</s:element>' . "\n";
		
        }
    
		if (array_key_exists("Segment", $functions[$i])) { // define los complextype anexos
			//$str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'Request">' . "\n";
			$str .= $t3 . '<s:complexType  name="Segment"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['Segment']);$j++) {				
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="' . $functions[$i]['Segment'][$j]['name'] . '" ';
				$str .= 'type="s:' . $functions[$i]['Segment'][$j]['type'] . '" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
			//$str .= $t2 . '</s:element>' . "\n";
		}
		if (array_key_exists("Pnr", $functions[$i])) { // define los complextype anexos
			//$str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'Request">' . "\n";
			$str .= $t3 . '<s:complexType  name="Pnr"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['Pnr']);$j++) {
				$formato="s";
				$maxoccur="1";
				if($functions[$i]['Pnr'][$j]['type']<>"string" 
					&& $functions[$i]['Pnr'][$j]['type']<>"integer")
						{$formato="tns"; $maxoccur="unbounded";}
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
				$str .= 'name="' . $functions[$i]['Pnr'][$j]['name'] . '" ';
				$str .= 'type="'.$formato.':' . $functions[$i]['Pnr'][$j]['type'] . '" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
			//$str .= $t2 . '</s:element>' . "\n";
		}
		if (array_key_exists("SegmentTotals", $functions[$i])) { // define los complextype anexos
			//$str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'Request">' . "\n";
			$str .= $t3 . '<s:complexType  name="SegmentTotals"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['SegmentTotals']);$j++) {				
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="' . $functions[$i]['SegmentTotals'][$j]['name'] . '" ';
				$str .= 'type="s:' . $functions[$i]['SegmentTotals'][$j]['type'] . '" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
			//$str .= $t2 . '</s:element>' . "\n";
		}
		if (array_key_exists("CustomerPricingSummary", $functions[$i])) { // define los complextype anexos
			//$str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'Request">' . "\n";
			$str .= $t3 . '<s:complexType  name="CustomerPricingSummary"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['CustomerPricingSummary']);$j++) {				
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="' . $functions[$i]['CustomerPricingSummary'][$j]['name'] . '" ';
				$str .= 'type="s:' . $functions[$i]['CustomerPricingSummary'][$j]['type'] . '" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
			//$str .= $t2 . '</s:element>' . "\n";
		}		
    }
    $str .= $t1 . '</s:schema>' . "\n";
    $str .= '</wsdl:types>' . "\n\n";

    // Declare Messages
    for ($i=0;$i<count($functions);$i++) {
    	// Define Request Messages
        if (array_key_exists("inputParams", $functions[$i])) {
        	$str .= '<wsdl:message name="' . $functions[$i]['funcName'] . 'RQ">' . "\n";
            $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['funcName'] . 'RQ" />' . "\n";
            $str .= '</wsdl:message>' . "\n";
        }
        // Define Response Messages
        if (array_key_exists("outputParams", $functions[$i])) {
            $str .= '<wsdl:message name="' . $functions[$i]['funcName'] . 'RS">' . "\n";
            $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['funcName'] . 'RS" />' . "\n";
            $str .= '</wsdl:message>' . "\n\n";
        }
    }

    // Declare Port Types
    for ($i=0;$i<count($functions);$i++) {
    	$str .= '<wsdl:portType name="' . $functions[$i]['funcName'] . 'PortType">' . "\n";
    	$str .= $t1 . '<wsdl:operation name="' . $functions[$i]['funcName'] . '">' . "\n";
    	if (array_key_exists("inputParams", $functions[$i])) 
    	   $str .= $t2 . '<wsdl:input message="tns:' . $functions[$i]['funcName'] . 'RQ" />' . "\n";
    	if (array_key_exists("outputParams", $functions[$i])) 
           $str .= $t2 . '<wsdl:output message="tns:' . $functions[$i]['funcName'] . 'RS" />' . "\n";
        $str .= $t1 . '</wsdl:operation>' . "\n";
    	$str .= '</wsdl:portType>' . "\n\n";
    }
    
    // Declare Bindings
    for ($i=0;$i<count($functions);$i++) {
        $str .= '<wsdl:binding name="' . $functions[$i]['funcName'] . 'Binding" type="tns:' . $functions[$i]['funcName'] . 'PortType">' . "\n";
        $str .= $t1 . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />' . "\n";
        $str .= $t1 . '<wsdl:operation name="' . $functions[$i]['funcName'] . '">' . "\n";
        $str .= $t2 . '<soap:operation soapAction="' . $functions[$i]['soapAddress'] . '#' . $functions[$i]['funcName'] . '" style="document" />' . "\n";
        if (array_key_exists("inputParams", $functions[$i]))
            $str .= $t2 . '<wsdl:input><soap:body use="literal" /></wsdl:input>' . "\n";
        if (array_key_exists("outputParams", $functions[$i]))
            $str .= $t2 . '<wsdl:output><soap:body use="literal" /></wsdl:output>' . "\n";
    	$str .= $t2 . '<wsdl:documentation>' . $functions[$i]['doc'] . '</wsdl:documentation>' . "\n";
        $str .= $t1 . '</wsdl:operation>' . "\n";
    	$str .= '</wsdl:binding>' . "\n\n";
    }
    
    // Declare Service
    $str .= '<wsdl:service name="' . $serviceID . '">' . "\n";
    for ($i=0;$i<count($functions);$i++) {
        $str .= $t1 . '<wsdl:port name="' . $functions[$i]['funcName'] . 'Port" binding="tns:' . $functions[$i]['funcName'] . 'Binding">' . "\n";
        $str .= $t2 . '<soap:address location="' . $functions[$i]['soapAddress'] . '" />' . "\n";
        $str .= $t1 . '</wsdl:port>' . "\n";
    }
    $str .= '</wsdl:service>' . "\n\n";
    
    // End Document
    $str .= '</wsdl:definitions>' . "\n";
    
    if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
    if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
    if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
    return $str;
}

?>