﻿<?php 

if (stristr($_SERVER['QUERY_STRING'], "wsdl")) {
    	// WSDL request - output raw XML
		header("Content-Type: application/soap+xml; charset=utf-8");
        echo DisplayXML();
    } else {
    	// Page accessed normally - output documentation
    	$cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1); // Current page
    	echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
    	echo '<html>';
    	echo '<head><title>' . $serviceName . '</title></head>';
    	echo '<body>';
    	echo '<h1>' . $serviceName . '</h1>';
        echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $cp . '?WSDL</code></p>';
    	
        // Document each function
        echo '<h2>Available Functions:</h2>';
        echo '<div style="margin-left:20px;">';
        for ($i=0;$i<count($functions);$i++) {
            echo '<h3>Function: ' . $functions[$i]['funcName'] . '</h3>';
            echo '<div style="margin-left:20px;">';
            echo '<p>';
            echo $functions[$i]['doc'];
            echo '<ul>';
            if (array_key_exists("inputParams", $functions[$i])) {
            	echo '<li>Input Parameters:<ul>';
            	for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
            	   	echo '<li>' . $functions[$i]['inputParams'][$j]['name'];
            	   	echo ' (' . $functions[$i]['inputParams'][$j]['type'];
            	   	echo ')</li>';
            	}
            	echo '</ul></li>';
            }
            if (array_key_exists("outputParams", $functions[$i])) {
                echo '<li>Output Parameters:<ul>';
                for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
                    echo '<li>' . $functions[$i]['outputParams'][$j]['name'];
                    echo ' (' . $functions[$i]['outputParams'][$j]['type'];
                    echo ')</li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
            echo '</p>';
            echo '</div>';
        }
        echo '</div>';
        	
    	echo '<h2>WSDL output:</h2>';
        echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
        echo DisplayXML(false);
        echo '</pre>';
        echo '</body></html>';
    }
                         
    exit; 
function DisplayXML($xmlformat=true) {
	
	function cabeza(){
		$aux = '<?xml version="1.0" encoding="utf-8" ?>
			<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://www.darkerwhite.com/" targetNamespace="http://www.darkerwhite.com/" name="DistantisServices">
			<wsdl:types>
			<xs:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">
		';
		return $aux;
	}
	function booking_api(){
		$aux = '<xs:element name="AvailabilityRQ">
					<xs:complexType>
						<xs:complexContent>
							<xs:extension base="SwitchflyRQType">
								<xs:sequence>
									<xs:element name="CreateSession" type="xs:boolean" minOccurs="0">
									</xs:element>
									<xs:choice>
										<xs:sequence>
											<xs:element name="RoomSearch" type="xs:RoomSearchType" minOccurs="0" maxOccurs="unbounded" />
										</xs:sequence>
									</xs:choice>
								</xs:sequence>
							</xs:extension>
						</xs:complexContent>
					</xs:complexType>
				</xs:element>
				<xs:element name="AvailabilityRS">
					<xs:complexType>
						<xs:complexContent>
							<xs:extension base="SwitchflyRSType">
								<xs:sequence>
									<xs:element name="RoomSearchResponse" type="xs:RoomSearchResponseType" minOccurs="0" maxOccurs="unbounded" />
									<xs:element name="SessionToken" type="xs:string" minOccurs="0">
									</xs:element>
								</xs:sequence>
							</xs:extension>
						</xs:complexContent>
					</xs:complexType>
				</xs:element>';
		return $aux;
	}
	function aditionals(){

		$aux='
		<xs:complexType name="RoomSearchType">
	        <xs:complexContent>
	            <xs:extension base="SearchType">
	                <xs:sequence>
	                    <xs:element name="CrsName" type="xs:string" minOccurs="0" />
	                    <xs:element name="SearchLocation" type="SearchLocationType" minOccurs="0" />
	                    <xs:element name="RadiusSearch" type="RadiusSearchType" minOccurs="0" />
	                    <xs:element name="ChainCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="BrandCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:choice minOccurs="0">
	                        <xs:element name="UnifiedHotelIds" minOccurs="0" maxOccurs="unbounded">
	                            <xs:complexType>
	                                <xs:sequence>
	                                    <xs:element name="Id" type="xs:string" maxOccurs="unbounded">
	                                    </xs:element>
	                                </xs:sequence>
	                            </xs:complexType>
	                        </xs:element>
	                        <xs:element name="HotelIds" minOccurs="0" maxOccurs="unbounded">
	                            <xs:complexType>
	                                <xs:sequence>
	                                    <xs:element name="IdSource" type="xs:string">
	                                    </xs:element>
	                                    <xs:element name="Id" type="xs:string" maxOccurs="unbounded">
	                                    </xs:element>
	                                </xs:sequence>
	                            </xs:complexType>
	                        </xs:element>
	                    </xs:choice>
	                    <xs:element name="Ql2HotelIds" minOccurs="0">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="Id" type="xs:string" minOccurs="1" maxOccurs="unbounded">
	                                </xs:element>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="InternalRoom" minOccurs="0" maxOccurs="1">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="ContractId" type="xs:integer" minOccurs="0" />
	                                <xs:element name="RoomTypeId" type="xs:integer" minOccurs="0" />
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="HotelCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="ExcludedEzRezHotelIds" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="IncludedEzRezHotelIds" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="SortCriteria" minOccurs="0">
	                        <xs:simpleType>
	                            <xs:restriction base="xs:string">
	                                <xs:enumeration value="hotel_price_ascending" />
	                                <xs:enumeration value="hotel_price_descending" />
	                                <xs:enumeration value="hotel_composite_score" />
	                                <xs:enumeration value="hotel_rating_then_price" />
	                                <xs:enumeration value="hotel_rating_then_composite_score" />
	                            </xs:restriction>
	                        </xs:simpleType>
	                    </xs:element>
	                    <xs:element name="MaxResults" type="xs:integer" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="PreferredHotelRating" type="xs:double" minOccurs="0" maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded" />
	                            </xs:sequence>
	                            <xs:attribute name="adults" type="xs:int" use="required" />
	                            <xs:attribute name="children" type="xs:int" />
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="CheckinDate" type="IsoDateType">
	                    </xs:element>
	                    <xs:element name="CheckoutDate" type="IsoDateType">
	                    </xs:element>
	                    <xs:element name="Rating" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="ShouldDoPackageSavingsSearch" type="xs:boolean" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="ResponseDetails" minOccurs="0">
	                        <xs:complexType>
	                            <xs:simpleContent>
	                                <xs:extension base="ResponseDetailsType">
	                                    <xs:attribute name="WithTripAdvisor" default="true" type="xs:boolean">
	                                    </xs:attribute>
	                                    <xs:attribute name="ResultsPerHotel" type="ResultsPerHotelType">
	                                    </xs:attribute>
	                                </xs:extension>
	                            </xs:simpleContent>
	                        </xs:complexType>
	                    </xs:element>
	                </xs:sequence>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>
	    <xs:simpleType name="ResponseDetailsType">
	        <xs:restriction base="xs:string">
	            <xs:enumeration value="FULL" />
	            <xs:enumeration value="MINIMUM" />
	            <xs:enumeration value="ROOM_DETAILS" />
	        </xs:restriction>
	    </xs:simpleType>
	    <xs:simpleType name="ResultsPerHotelType">
	        <xs:restriction base="xs:string">
	            <xs:enumeration value="ALL" />
	            <xs:enumeration value="LOWEST_PRICED_ROOM" />
	        </xs:restriction>
	    </xs:simpleType>
	    <xs:complexType name="RoomSearchResponseType">
	        <xs:complexContent>
	            <xs:extension base="SearchResponseType">
	                <xs:sequence>
	                    <xs:element name="Hotel" type="HotelType" minOccurs="0" maxOccurs="unbounded" />
	                </xs:sequence>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>
	    <xs:complexType name="RadiusSearchType">
	        <xs:sequence>
	            <xs:element name="GeoLocation" minOccurs="1">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="Latitude" minOccurs="1">
	                            <xs:simpleType>
	                                <xs:restriction base="xs:double">
	                                    <xs:minInclusive value="-90" />
	                                    <xs:maxInclusive value="90" />
	                                </xs:restriction>
	                            </xs:simpleType>
	                        </xs:element>
	                        <xs:element name="Longitude" minOccurs="1">
	                            <xs:simpleType>
	                                <xs:restriction base="xs:double">
	                                    <xs:minInclusive value="-180" />
	                                    <xs:maxInclusive value="180" />
	                                </xs:restriction>
	                            </xs:simpleType>
	                        </xs:element>
	                    </xs:sequence>
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="Radius" minOccurs="0" default="10">
	                <xs:simpleType>
	                    <xs:restriction base="xs:int">
	                        <xs:minInclusive value="1" />
	                        <xs:maxInclusive value="50" />
	                    </xs:restriction>
	                </xs:simpleType>
	            </xs:element>
	            <xs:element name="Units" type="DistanceUnitsType" minOccurs="0" default="m" />
	        </xs:sequence>
	    </xs:complexType>
	    <xs:simpleType name="DistanceUnitsType">
	        <xs:restriction base="xs:string">
	            <xs:enumeration value="m" />
	            <xs:enumeration value="km" />
	        </xs:restriction>
	    </xs:simpleType>
	    <xs:complexType name="SearchLocationType">
	        <xs:sequence>
	            <xs:element name="Location" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="RegionId" type="xs:int" minOccurs="0">
	            </xs:element>
	            <xs:element name="CityId" type="xs:int" minOccurs="0">
	            </xs:element>
	            <xs:element name="AirportCode" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="SearchType">
		        <xs:sequence>
		            <xs:element name="SearchId" type="xs:string" minOccurs="0">
		            </xs:element>
		            <xs:element name="BookingComponent" type="BookingComponentType" minOccurs="0" maxOccurs="unbounded">
		            </xs:element>
		            <xs:element name="SpecialId" minOccurs="0">
		            </xs:element>
		        </xs:sequence>

		</xs:complexType>
	    <xs:complexType name="SearchResponseType">
	        <xs:sequence>
	            <xs:element name="SearchId" minOccurs="0">
	            </xs:element>
	            <xs:element name="ErrorText" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:simpleType name="BookingComponentType">
	        <xs:restriction base="xs:string">
	            <xs:enumeration value="Activity"/>
	            <xs:enumeration value="Air"/>
	            <xs:enumeration value="Car"/>
	            <xs:enumeration value="Cruise"/>
	            <xs:enumeration value="Room"/>
	        </xs:restriction>
	    </xs:simpleType>
	    <xs:simpleType name="IsoDateType">
	        <xs:restriction base="xs:string"/>
	    </xs:simpleType>
	    <xs:complexType name="SwitchflyRSType">
	        <xs:complexContent>
	            <xs:extension base="EzRezMessageType">
	                <xs:sequence>
	                    <xs:element name="LogName" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="ClientMarker" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:choice>
	                        <xs:element name="Success" type="xs:string">
	                        </xs:element>
	                        <xs:element name="Failure" type="FailureType"/>
	                    </xs:choice>
	                </xs:sequence>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>
	    <xs:complexType name="EzRezMessageType"/>
	    <xs:complexType name="FailureType">
	        <xs:simpleContent>
	            <xs:extension base="xs:string">
	                <xs:attribute name="type">
	                </xs:attribute>
	                <xs:attribute name="code" type="xs:string">
	                </xs:attribute>
	                <xs:attribute name="translation" type="xs:string">
	                </xs:attribute>
	            </xs:extension>
	        </xs:simpleContent>
	    </xs:complexType>
	    <xs:complexType name="SwitchflyRQType">
	        <xs:complexContent>
	            <xs:extension base="EzRezMessageType">
	                <xs:sequence>
	                    <xs:element name="UserId" type="xs:string">
	                    </xs:element>
	                    <xs:element name="Password" type="xs:string">
	                    </xs:element>
	                    <xs:element name="Cobrand" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="LanguageId" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Currency" type="CurrencyCodeType" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Debug" type="xs:boolean" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="CustomerIp" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="ClientMarker" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element minOccurs="0" name="ClientExtensions">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:any maxOccurs="unbounded" minOccurs="0" processContents="lax"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="StubScenario" type="xs:string" minOccurs="0">
	                    </xs:element>
	                </xs:sequence>
	                <xs:attribute name="Version" use="required">
	                    <xs:simpleType>
	                        <xs:restriction base="xs:string">
	                            <xs:enumeration value="16.3"/>
	                        </xs:restriction>
	                    </xs:simpleType>
	                </xs:attribute>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>
	    <xs:simpleType name="CurrencyCodeType">
		    <xs:restriction base="xs:string">
		        <xs:length value="3"/>
		    </xs:restriction>
		</xs:simpleType>
		<xs:complexType name="HotelType">
	        <xs:sequence>
	            <xs:element name="EzRezId" type="xs:int" minOccurs="0">
	            </xs:element>
	            <xs:element name="Ql2Code" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="SearchHotelId" minOccurs="0">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="IdSource" type="xs:string" />
	                        <xs:element name="Id" type="xs:string" />
	                    </xs:sequence>
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="AirportCode" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="BookingDescriptor" type="xs:string">
	            </xs:element>
	            <xs:element name="ChainCode" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="BrandCode" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="HotelName" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="HotelCode" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Approved" type="xs:boolean" minOccurs="0">
	            </xs:element>
	            <xs:element name="ExclusiveDeal" type="xs:boolean" minOccurs="0">
	            </xs:element>
	            <xs:element name="Policies" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="PropertyDescription" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="RoomType" type="xs:string" minOccurs="0" maxOccurs="unbounded" />
	            <xs:element name="RoomTypesDescriptions" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="RoomDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="ShortDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="ExteriorDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="LobbyDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="PointOfInterest" minOccurs="0" maxOccurs="unbounded">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="Description" type="xs:string" />
	                        <xs:sequence minOccurs="0">
	                            <xs:element name="Distance" type="xs:float" />
	                            <xs:element name="Units" type="xs:string">
	                            </xs:element>
	                        </xs:sequence>
	                    </xs:sequence>
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="PointsOfInterestDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="Address" type="AddressType" minOccurs="0">
	            </xs:element>
	            <xs:element name="Phone" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="LocalPhone" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Fax" type="xs:string" minOccurs="0" />
	            <xs:element name="LocalFax" type="xs:string" minOccurs="0" />
	            <xs:element name="GeoLocation" minOccurs="0">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="Latitude" type="xs:string" />
	                        <xs:element name="Longitude" type="xs:string" />
	                    </xs:sequence>
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="DistanceFromSearchLocation" minOccurs="0">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="Distance" type="xs:double" />
	                        <xs:element name="Units" type="DistanceUnitsType" />
	                    </xs:sequence>
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="Amenity" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	            <xs:element name="AmenitiesDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="AmenityFilter" minOccurs="0" maxOccurs="unbounded">
	                <xs:simpleType>
	                    <xs:restriction base="xs:string">
	                        <xs:enumeration value="AIRPORT_SHUTTLE" />
	                        <xs:enumeration value="ALL_INCLUSIVE" />
	                        <xs:enumeration value="BABYSITTING_CHILD_CARE" />
	                        <xs:enumeration value="BAR_LOUNGE_ONSITE" />
	                        <xs:enumeration value="BEACH" />
	                        <xs:enumeration value="BUSINESS_CENTER" />
	                        <xs:enumeration value="CASINO" />
	                        <xs:enumeration value="CHILDREN_TEEN_PROGRAMS" />
	                        <xs:enumeration value="CONTINENTAL_MEAL_PLAN" />
	                        <xs:enumeration value="FREE_PARKING" />
	                        <xs:enumeration value="GOLF" />
	                        <xs:enumeration value="GYM" />
	                        <xs:enumeration value="HANDICAP_FACILITIES" />
	                        <xs:enumeration value="INTERNET" />
	                        <xs:enumeration value="MEETING_ROOMS" />
	                        <xs:enumeration value="PETS_ALLOWED" />
	                        <xs:enumeration value="RESTAURANT_ONSITE" />
	                        <xs:enumeration value="ROOM_SERVICE" />
	                        <xs:enumeration value="SKI_IN_SKI_OUT" />
	                        <xs:enumeration value="SMALL_PETS_ALLOWED" />
	                        <xs:enumeration value="SPA" />
	                        <xs:enumeration value="SWIMMING_POOL" />
	                        <xs:enumeration value="SMOKE_FREE" />
	                        <xs:enumeration value="TENNIS" />
	                        <xs:enumeration value="WIRELESS_INTERNET" />
	                    </xs:restriction>
	                </xs:simpleType>
	            </xs:element>
	            <xs:element name="FamilyDescription" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Checkin" type="IsoTimestampType" minOccurs="0">
	            </xs:element>
	            <xs:element name="Checkout" type="IsoTimestampType" minOccurs="0">
	            </xs:element>
	            <xs:element name="Rating" type="xs:double" minOccurs="0" />
	            <xs:element name="SpecialNotices" type="xs:string" minOccurs="0" />
	            <xs:element name="AdditionalInfo" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="AdditionalDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded" />
	            <xs:element name="Currency" type="xs:string" />
	            <xs:element name="MinimumPrice" type="xs:double" minOccurs="0">
	            </xs:element>
	            <xs:element name="MinimumStandalonePrice" type="xs:double" minOccurs="0">
	            </xs:element>
	            <xs:element name="MinimumStandalonePriceTaxTotal" type="xs:double" minOccurs="0">
	            </xs:element>
	            <xs:element name="MinimumPriceTaxTotal" type="xs:double" minOccurs="0">
	            </xs:element>
	            <xs:element name="Url" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Html" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="MapUrl" type="xs:string" minOccurs="0" />
	            <xs:element name="Thumbnail" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="VirtualTourUrl" type="xs:string" minOccurs="0" />
	            <xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
	                <xs:complexType>
	                    <xs:sequence>
	                        <xs:element name="RoomSegment" type="RoomSegmentType" minOccurs="0" maxOccurs="unbounded" />
	                    </xs:sequence>
	                    <xs:attribute name="adults" type="xs:int" use="required" />
	                    <xs:attribute name="children" type="xs:int" />
	                </xs:complexType>
	            </xs:element>
	            <xs:element name="RoomAmenity" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	            <xs:element name="RoomAmenitiesDescription" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Direction" type="DirectionType" minOccurs="0" maxOccurs="unbounded" />
	            <xs:element name="Restaurant" type="RestaurantType" minOccurs="0" maxOccurs="unbounded" />
	            <xs:element name="RestaurantsDescription" type="xs:string" minOccurs="0" />
	            <xs:element name="Image" type="ImageType" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	            <xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0">
	            </xs:element>
	            <xs:element name="Entertainment" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="UserReviewSummary" type="UserReviewSummaryType" minOccurs="0" />
	        </xs:sequence>
	    </xs:complexType>
	    <xs:simpleType name="IsoTimestampType">
	        <xs:restriction base="xs:string"/>
	    </xs:simpleType>
	    <xs:complexType name="AddressType">
	        <xs:sequence>
	            <xs:element name="AddressLine" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	            </xs:element>
	            <xs:element name="City" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="State" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Zip" type="xs:string" minOccurs="0"/>
	            <xs:element name="Country" type="xs:string" minOccurs="0">
	            </xs:element>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="DirectionType">
	        <xs:sequence>
	            <xs:element name="Origin" type="xs:string" minOccurs="0" />
	            <xs:element name="Destination" type="xs:string" minOccurs="0" />
	            <xs:element name="Directions" type="xs:string" minOccurs="0" />
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="RestaurantType">
	        <xs:sequence>
	            <xs:element name="Atmosphere" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Cuisine" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Hours" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Name" type="xs:string" />
	            <xs:element name="Setting" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Description" type="xs:string" minOccurs="0">
	            </xs:element>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="ImageType">
	        <xs:sequence>
	            <xs:element name="Height" type="xs:int" minOccurs="0" />
	            <xs:element name="Width" type="xs:int" minOccurs="0" />
	            <xs:element name="Caption" type="xs:string" minOccurs="0" />
	            <xs:element name="Name" type="xs:string" minOccurs="0" />
	            <xs:element name="ThumbnailUrl" type="xs:string" minOccurs="0" />
	            <xs:element name="ImageId" type="xs:integer" minOccurs="0" />
	            <xs:element name="Type" type="xs:string" minOccurs="0">
	            </xs:element>
	            <xs:element name="Url" type="xs:string" minOccurs="0" />
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="UserReviewSummaryType">
	        <xs:sequence>
	            <xs:element name="UserReviewHotelCode" type="xs:string"/>
	            <xs:element name="LogoImageUrl" type="xs:string"/>
	            <xs:element name="RankScale" type="xs:int" minOccurs="0"/>
	            <xs:element name="Rank" type="xs:int" minOccurs="0"/>
	            <xs:element name="MedianUserRating" type="xs:double" minOccurs="0"/>
	            <xs:element name="RatingImageUrl" type="xs:string" minOccurs="0"/>
	            <xs:element name="WriteReviewUrl" type="xs:string" minOccurs="0"/>
	            <xs:element name="TotalUserReviewCount" type="xs:int" minOccurs="0"/>
	            <xs:element name="UserReview" type="UserReviewType" minOccurs="0" maxOccurs="unbounded"/>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="UserReviewType">
	        <xs:sequence>
	            <xs:element name="PublishedDate" type="IsoDateType" minOccurs="0"/>
	            <xs:element name="Author" type="xs:string" minOccurs="0"/>
	            <xs:element name="AuthorLocation" type="xs:string" minOccurs="0"/>
	            <xs:element name="Title" type="xs:string" minOccurs="0"/>
	            <xs:element name="Content" type="xs:string" minOccurs="0"/>
	            <xs:element name="ContentSummary" type="xs:string" minOccurs="0"/>
	            <xs:element name="Rating" type="xs:int" minOccurs="0"/>
	            <xs:element name="RatingImage" type="xs:string" minOccurs="0"/>
	        </xs:sequence>
	    </xs:complexType>
	    <xs:complexType name="RoomSegmentType">
	        <xs:complexContent>
	            <xs:extension base="SegmentType">
	                <xs:sequence>
	                    <xs:element name="BookingDescriptor" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Checkin" type="IsoTimestampType" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Checkout" type="IsoTimestampType" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Nights" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Adults" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Children" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Infants" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="PackageName" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="ChainCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="BrandCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="HotelCode" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Ql2Code" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="HotelName" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="HotelRating" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="HotelStarRatingDescription" type="xs:string" minOccurs="0"/>
	                    <xs:element name="Address" type="AddressType" minOccurs="0"/>
	                    <xs:element name="CustomerSupportPhone" type="xs:string" minOccurs="0"/>
	                    <xs:element name="HotelPhone" type="xs:string" minOccurs="0"/>
	                    <xs:element name="HotelFax" type="xs:string" minOccurs="0"/>
	                    <xs:element name="HotelEmail" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RoomName" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RateName" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RateMemo" type="xs:string" minOccurs="0" maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="AdditionalRoomDisplayInfo" type="xs:string" minOccurs="0"
	                        maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="AdditionalHotelDisplayInfo" type="xs:string" minOccurs="0"
	                        maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="IncludedService" minOccurs="0" maxOccurs="unbounded">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="Code" type="xs:string"/>
	                                <xs:element name="Name" type="xs:string"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="AvailableRequestService" minOccurs="0" maxOccurs="unbounded">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="Code" type="xs:string"/>
	                                <xs:element name="Name" type="xs:string"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="RequestedService" type="xs:string" minOccurs="0"
	                        maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="SpecialRequests" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="VoucherInformation" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RoomPrice" type="RoomPriceType"/>
	                    <xs:element name="StrikethroughPrice" type="RoomPriceType" minOccurs="0"/>
	                    <xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="RateCode" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RoomCode" type="xs:string" minOccurs="0"/>
	                    <xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="SupplierConfirmationSent" type="xs:boolean" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="MealType" type="xs:string" minOccurs="0"/>
	                    <xs:element name="RoomTypeId" type="xs:integer" minOccurs="0"/>
	                    <xs:element name="RoomContractId" type="xs:integer" minOccurs="0"/>
	                    <xs:element name="FeeDueAtHotelList" type="FeeDueAtHotelListType" minOccurs="0" maxOccurs="1">
	                    </xs:element>
	                    <xs:element name="CustomFields" minOccurs="0">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="CustomField" type="CustomFieldType" minOccurs="0" maxOccurs="unbounded"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="RoomingList" type="GuestListType" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Promotion" minOccurs="0" maxOccurs="unbounded">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="IsPrimary" type="xs:boolean"/>
	                                <xs:element name="Message" type="xs:string" minOccurs="0"/>
	                                <xs:element name="LongPrimaryMessage" type="xs:string" minOccurs="0"/>
	                                <xs:element name="IsClientExclusive" type="xs:boolean" minOccurs="0"/>
	                                <xs:element name="PromoType" type="xs:string" minOccurs="0"/>
	                                <xs:element name="TermsAndConditions" type="xs:string" minOccurs="0"
	                                />
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="RoomCancellationPolicies" minOccurs="0">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="RoomCancellationPolicy" type="RoomCancellationPolicyType" minOccurs="0"
	                                    maxOccurs="unbounded"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="NonRefundable" type="xs:boolean" default="false" minOccurs="0">
	                    </xs:element>
	                </xs:sequence>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>
	    <xs:complexType name="SegmentType">
	        <xs:complexContent>
	            <xs:extension base="PnrElementType">
	                <xs:sequence>
	                    <xs:element name="BookingComponent" type="BookingComponentType" minOccurs="0"
	                        maxOccurs="unbounded">
	                    </xs:element>
	                    <xs:element name="SegmentId" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="OriginalSegmentId" type="xs:int" minOccurs="0" maxOccurs="1">
	                    </xs:element>
	                    <xs:element name="SwitchflyBookingNumber" type="xs:int" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="SegmentLocator" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="CrsName" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="CrsRecordLocator" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="PassiveCrsRecordLocator" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="SupplierLocator" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="Policies" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="CrsPolicies" type="xs:string" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="AgencyName" type="xs:string" minOccurs="0"/>
	                    <xs:element name="AgencyAddress" type="xs:string" minOccurs="0"/>
	                    <xs:element name="AgentName" type="xs:string" minOccurs="0"/>
	                    <xs:element name="ConfirmationStatus" type="xs:string" minOccurs="0"/>
	                    <xs:element name="SpecialId" type="xs:int" minOccurs="0"/>
	                    <xs:element name="Supplier" type="InternalSupplierType" minOccurs="0">
	                    </xs:element>
	                    <xs:element name="BookingMemos" minOccurs="0">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="BookingMemo" type="BookingMemoType" minOccurs="0"
	                                    maxOccurs="unbounded"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="ConnectorCustomInfos" minOccurs="0" maxOccurs="unbounded">
	                        <xs:complexType>
	                            <xs:sequence>
	                                <xs:element name="ConnectorCustomInfo"
	                                    type="ConnectorCustomInfoType" minOccurs="0"
	                                    maxOccurs="unbounded"/>
	                            </xs:sequence>
	                        </xs:complexType>
	                    </xs:element>
	                    <xs:element name="ArrivalAirConnectionInfo" type="AirConnectionInfoType"
	                        minOccurs="0" maxOccurs="1">
	                    </xs:element>
	                    <xs:element name="DepartureAirConnectionInfo" type="AirConnectionInfoType"
	                        minOccurs="0" maxOccurs="1">
	                    </xs:element>
	                    <xs:element name="SingleUseCard" type="xs:string" minOccurs="0"/>
	                </xs:sequence>
	            </xs:extension>
	        </xs:complexContent>
	    </xs:complexType>



	    ';

		return $aux;
	}
	
	function definitions(){
		$aux = '</xs:schema>
			</wsdl:types>
			<wsdl:message name="AvailabilityRQ">
				<wsdl:part name="parameters" element="tns:AvailabilityRQ"/>
			</wsdl:message>
			<wsdl:message name="AvailabilityRS">
				<wsdl:part name="parameters" element="tns:AvailabilityRS"/>
			</wsdl:message>
			<wsdl:portType name="switchflyportType">
				<wsdl:operation name="Availability">
					<wsdl:input message="tns:AvailabilityRQ"/>
					<wsdl:output message="tns:AvailabilityRS"/>
				</wsdl:operation>
			</wsdl:portType>
			<wsdl:binding name="switchflyBinding" type="tns:switchflyportType">
				<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
				<wsdl:operation name="Availability">
						<soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/test.php#Availability" style="document"/>
						<wsdl:input>
							<soap:body use="literal"/>
						</wsdl:input>
						<wsdl:output>
							<soap:body use="literal"/>
						</wsdl:output>
						<wsdl:documentation>Availability</wsdl:documentation>
				</wsdl:operation>
				
			</wsdl:binding>
			<wsdl:service name="DistantisServices">
				<wsdl:port name="switchflyPort" binding="tns:switchflyBinding">
					<soap:address location="http://otas.distantis.com/otas/webservice/v1.2/test.php"/>
				</wsdl:port>
			</wsdl:service>
			</wsdl:definitions>';
		return $aux;
	}


	function fuckthisshit(){
		$aux='<!-- Created with Liquid Studio 2018 (https://www.liquid-technologies.com) -->
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://www.darkerwhite.com/" targetNamespace="http://www.darkerwhite.com/" name="DistantisServices">
    <wsdl:types>
        <xs:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">
            <xs:element name="AvailabilityRQ">
                <xs:complexType>
                    <xs:complexContent>
                        <xs:extension base="tns:SwitchflyRQType">
                            <xs:sequence>
                                <xs:element name="CreateSession" type="xs:boolean" minOccurs="0"></xs:element>
                                <xs:choice>
                                    <xs:sequence>
                                        <xs:element name="RoomSearch" type="tns:RoomSearchType" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                </xs:choice>
                            </xs:sequence>
                        </xs:extension>
                    </xs:complexContent>
                </xs:complexType>
            </xs:element>
            <xs:element name="AvailabilityRS">
                <xs:complexType>
                    <xs:complexContent>
                        <xs:extension base="tns:SwitchflyRSType">
                            <xs:sequence>
                                <xs:element name="RoomSearchResponse" type="tns:RoomSearchResponseType" minOccurs="0" maxOccurs="unbounded"/>
                                <xs:element name="SessionToken" type="xs:string" minOccurs="0"></xs:element>
                            </xs:sequence>
                        </xs:extension>
                    </xs:complexContent>
                </xs:complexType>
            </xs:element>
            <xs:complexType name="RoomSearchType">
                <xs:complexContent>
                    <xs:extension base="tns:SearchType">
                        <xs:sequence>
                            <xs:element name="CrsName" type="xs:string" minOccurs="0"/>
                            <xs:element name="SearchLocation" type="tns:SearchLocationType" minOccurs="0"/>
                            <xs:element name="RadiusSearch" type="tns:RadiusSearchType" minOccurs="0"/>
                            <xs:element name="ChainCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="BrandCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:choice minOccurs="0">
                                <xs:element name="UnifiedHotelIds" minOccurs="0" maxOccurs="unbounded">
                                    <xs:complexType>
                                        <xs:sequence>
                                            <xs:element name="Id" type="xs:string" maxOccurs="unbounded"></xs:element>
                                        </xs:sequence>
                                    </xs:complexType>
                                </xs:element>
                                <xs:element name="HotelIds" minOccurs="0" maxOccurs="unbounded">
                                    <xs:complexType>
                                        <xs:sequence>
                                            <xs:element name="IdSource" type="xs:string"></xs:element>
                                            <xs:element name="Id" type="xs:string" maxOccurs="unbounded"></xs:element>
                                        </xs:sequence>
                                    </xs:complexType>
                                </xs:element>
                            </xs:choice>
                            <xs:element name="Ql2HotelIds" minOccurs="0">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="Id" type="xs:string" minOccurs="1" maxOccurs="unbounded"></xs:element>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="InternalRoom" minOccurs="0" maxOccurs="1">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="ContractId" type="xs:integer" minOccurs="0"/>
                                        <xs:element name="RoomTypeId" type="xs:integer" minOccurs="0"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="HotelCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="ExcludedEzRezHotelIds" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="IncludedEzRezHotelIds" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="SortCriteria" minOccurs="0">
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:enumeration value="hotel_price_ascending"/>
                                        <xs:enumeration value="hotel_price_descending"/>
                                        <xs:enumeration value="hotel_composite_score"/>
                                        <xs:enumeration value="hotel_rating_then_price"/>
                                        <xs:enumeration value="hotel_rating_then_composite_score"/>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:element>
                            <xs:element name="MaxResults" type="xs:integer" minOccurs="0"></xs:element>
                            <xs:element name="PreferredHotelRating" type="xs:double" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                    <xs:attribute name="adults" type="xs:int" use="required"/>
                                    <xs:attribute name="children" type="xs:int"/>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="CheckinDate" type="tns:IsoDateType"></xs:element>
                            <xs:element name="CheckoutDate" type="tns:IsoDateType"></xs:element>
                            <xs:element name="Rating" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="ShouldDoPackageSavingsSearch" type="xs:boolean" minOccurs="0"></xs:element>
                            <xs:element name="ResponseDetails" minOccurs="0">
                                <xs:complexType>
                                    <xs:simpleContent>
                                        <xs:extension base="tns:ResponseDetailsType">
                                            <xs:attribute name="WithTripAdvisor" default="true" type="xs:boolean"></xs:attribute>
                                            <xs:attribute name="ResultsPerHotel" type="tns:ResultsPerHotelType"></xs:attribute>
                                        </xs:extension>
                                    </xs:simpleContent>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>
            <xs:simpleType name="ResponseDetailsType">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="FULL"/>
                    <xs:enumeration value="MINIMUM"/>
                    <xs:enumeration value="ROOM_DETAILS"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:simpleType name="ResultsPerHotelType">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="ALL"/>
                    <xs:enumeration value="LOWEST_PRICED_ROOM"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:complexType name="RoomSearchResponseType">
                <xs:complexContent>
                    <xs:extension base="tns:SearchResponseType">
                        <xs:sequence>
                            <xs:element name="Hotel" type="tns:HotelType" minOccurs="0" maxOccurs="unbounded"/>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>
            <xs:complexType name="RadiusSearchType">
                <xs:sequence>
                    <xs:element name="GeoLocation" minOccurs="1">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Latitude" minOccurs="1">
                                    <xs:simpleType>
                                        <xs:restriction base="xs:double">
                                            <xs:minInclusive value="-90"/>
                                            <xs:maxInclusive value="90"/>
                                        </xs:restriction>
                                    </xs:simpleType>
                                </xs:element>
                                <xs:element name="Longitude" minOccurs="1">
                                    <xs:simpleType>
                                        <xs:restriction base="xs:double">
                                            <xs:minInclusive value="-180"/>
                                            <xs:maxInclusive value="180"/>
                                        </xs:restriction>
                                    </xs:simpleType>
                                </xs:element>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="Radius" minOccurs="0" default="10">
                        <xs:simpleType>
                            <xs:restriction base="xs:int">
                                <xs:minInclusive value="1"/>
                                <xs:maxInclusive value="50"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="Units" type="tns:DistanceUnitsType" minOccurs="0" default="m"/>
                </xs:sequence>
            </xs:complexType>
            <xs:simpleType name="DistanceUnitsType">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="m"/>
                    <xs:enumeration value="km"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:complexType name="SearchLocationType">
                <xs:sequence>
                    <xs:element name="Location" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="RegionId" type="xs:int" minOccurs="0"></xs:element>
                    <xs:element name="CityId" type="xs:int" minOccurs="0"></xs:element>
                    <xs:element name="AirportCode" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="SearchType">
                <xs:sequence>
                    <xs:element name="SearchId" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="BookingComponent" type="tns:BookingComponentType" minOccurs="0" maxOccurs="unbounded"></xs:element>
                    <xs:element name="SpecialId" minOccurs="0"></xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="SearchResponseType">
                <xs:sequence>
                    <xs:element name="SearchId" minOccurs="0"></xs:element>
                    <xs:element name="ErrorText" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:simpleType name="BookingComponentType">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="Activity"/>
                    <xs:enumeration value="Air"/>
                    <xs:enumeration value="Car"/>
                    <xs:enumeration value="Cruise"/>
                    <xs:enumeration value="Room"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:simpleType name="IsoDateType">
                <xs:restriction base="xs:string"/>
            </xs:simpleType>
            <xs:complexType name="SwitchflyRSType">
                <xs:complexContent>
                    <xs:extension base="tns:EzRezMessageType">
                        <xs:sequence>
                            <xs:element name="LogName" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="ClientMarker" type="xs:string" minOccurs="0"></xs:element>
                            <xs:choice>
                                <xs:element name="Success" type="xs:string"></xs:element>
                                <xs:element name="Failure" type="tns:FailureType"/>
                            </xs:choice>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>
            <xs:complexType name="EzRezMessageType"/>
            <xs:complexType name="FailureType">
                <xs:simpleContent>
                    <xs:extension base="xs:string">
                        <xs:attribute name="type"></xs:attribute>
                        <xs:attribute name="code" type="xs:string"></xs:attribute>
                        <xs:attribute name="translation" type="xs:string"></xs:attribute>
                    </xs:extension>
                </xs:simpleContent>
            </xs:complexType>
            <xs:complexType name="SwitchflyRQType">
                <xs:complexContent>
                    <xs:extension base="tns:EzRezMessageType">
                        <xs:sequence>
                            <xs:element name="UserId" type="xs:string"></xs:element>
                            <xs:element name="Password" type="xs:string"></xs:element>
                            <xs:element name="Cobrand" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="LanguageId" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="Currency" type="tns:CurrencyCodeType" minOccurs="0"></xs:element>
                            <xs:element name="Debug" type="xs:boolean" minOccurs="0"></xs:element>
                            <xs:element name="CustomerIp" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="ClientMarker" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element minOccurs="0" name="ClientExtensions">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:any maxOccurs="unbounded" minOccurs="0" processContents="lax"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="StubScenario" type="xs:string" minOccurs="0"></xs:element>
                        </xs:sequence>
                        <xs:attribute name="Version" use="required">
                            <xs:simpleType>
                                <xs:restriction base="xs:string">
                                    <xs:enumeration value="16.3"/>
                                </xs:restriction>
                            </xs:simpleType>
                        </xs:attribute>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>
            <xs:simpleType name="CurrencyCodeType">
                <xs:restriction base="xs:string">
                    <xs:length value="3"/>
                </xs:restriction>
            </xs:simpleType>
            <xs:complexType name="HotelType">
                <xs:sequence>
                    <xs:element name="EzRezId" type="xs:int" minOccurs="0"></xs:element>
                    <xs:element name="Ql2Code" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="SearchHotelId" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="IdSource" type="xs:string"/>
                                <xs:element name="Id" type="xs:string"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="AirportCode" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="BookingDescriptor" type="xs:string"></xs:element>
                    <xs:element name="ChainCode" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="BrandCode" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="HotelName" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="HotelCode" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Approved" type="xs:boolean" minOccurs="0"></xs:element>
                    <xs:element name="ExclusiveDeal" type="xs:boolean" minOccurs="0"></xs:element>
                    <xs:element name="Policies" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="PropertyDescription" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="RoomType" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="RoomTypesDescriptions" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="ShortDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="ExteriorDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="LobbyDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="PointOfInterest" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Description" type="xs:string"/>
                                <xs:sequence minOccurs="0">
                                    <xs:element name="Distance" type="xs:float"/>
                                    <xs:element name="Units" type="xs:string"></xs:element>
                                </xs:sequence>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="PointsOfInterestDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="Address" type="tns:AddressType" minOccurs="0"></xs:element>
                    <xs:element name="Phone" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="LocalPhone" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Fax" type="xs:string" minOccurs="0"/>
                    <xs:element name="LocalFax" type="xs:string" minOccurs="0"/>
                    <xs:element name="GeoLocation" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Latitude" type="xs:string"/>
                                <xs:element name="Longitude" type="xs:string"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="DistanceFromSearchLocation" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Distance" type="xs:double"/>
                                <xs:element name="Units" type="tns:DistanceUnitsType"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="Amenity" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                    <xs:element name="AmenitiesDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="AmenityFilter" minOccurs="0" maxOccurs="unbounded">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="AIRPORT_SHUTTLE"/>
                                <xs:enumeration value="ALL_INCLUSIVE"/>
                                <xs:enumeration value="BABYSITTING_CHILD_CARE"/>
                                <xs:enumeration value="BAR_LOUNGE_ONSITE"/>
                                <xs:enumeration value="BEACH"/>
                                <xs:enumeration value="BUSINESS_CENTER"/>
                                <xs:enumeration value="CASINO"/>
                                <xs:enumeration value="CHILDREN_TEEN_PROGRAMS"/>
                                <xs:enumeration value="CONTINENTAL_MEAL_PLAN"/>
                                <xs:enumeration value="FREE_PARKING"/>
                                <xs:enumeration value="GOLF"/>
                                <xs:enumeration value="GYM"/>
                                <xs:enumeration value="HANDICAP_FACILITIES"/>
                                <xs:enumeration value="INTERNET"/>
                                <xs:enumeration value="MEETING_ROOMS"/>
                                <xs:enumeration value="PETS_ALLOWED"/>
                                <xs:enumeration value="RESTAURANT_ONSITE"/>
                                <xs:enumeration value="ROOM_SERVICE"/>
                                <xs:enumeration value="SKI_IN_SKI_OUT"/>
                                <xs:enumeration value="SMALL_PETS_ALLOWED"/>
                                <xs:enumeration value="SPA"/>
                                <xs:enumeration value="SWIMMING_POOL"/>
                                <xs:enumeration value="SMOKE_FREE"/>
                                <xs:enumeration value="TENNIS"/>
                                <xs:enumeration value="WIRELESS_INTERNET"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="FamilyDescription" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Checkin" type="tns:IsoTimestampType" minOccurs="0"></xs:element>
                    <xs:element name="Checkout" type="tns:IsoTimestampType" minOccurs="0"></xs:element>
                    <xs:element name="Rating" type="xs:double" minOccurs="0"/>
                    <xs:element name="SpecialNotices" type="xs:string" minOccurs="0"/>
                    <xs:element name="AdditionalInfo" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="AdditionalDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Currency" type="xs:string"/>
                    <xs:element name="MinimumPrice" type="xs:double" minOccurs="0"></xs:element>
                    <xs:element name="MinimumStandalonePrice" type="xs:double" minOccurs="0"></xs:element>
                    <xs:element name="MinimumStandalonePriceTaxTotal" type="xs:double" minOccurs="0"></xs:element>
                    <xs:element name="MinimumPriceTaxTotal" type="xs:double" minOccurs="0"></xs:element>
                    <xs:element name="Url" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Html" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="MapUrl" type="xs:string" minOccurs="0"/>
                    <xs:element name="Thumbnail" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="VirtualTourUrl" type="xs:string" minOccurs="0"/>
                    <xs:element name="RoomOccupancy" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="RoomSegment" type="tns:RoomSegmentType" minOccurs="0" maxOccurs="unbounded"/>
                            </xs:sequence>
                            <xs:attribute name="adults" type="xs:int" use="required"/>
                            <xs:attribute name="children" type="xs:int"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="RoomAmenity" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                    <xs:element name="RoomAmenitiesDescription" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Direction" type="tns:DirectionType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="Restaurant" type="tns:RestaurantType" minOccurs="0" maxOccurs="unbounded"/>
                    <xs:element name="RestaurantsDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="Image" type="tns:ImageType" minOccurs="0" maxOccurs="unbounded"></xs:element>
                    <xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0"></xs:element>
                    <xs:element name="Entertainment" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="UserReviewSummary" type="tns:UserReviewSummaryType" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>
            <xs:simpleType name="IsoTimestampType">
                <xs:restriction base="xs:string"/>
            </xs:simpleType>
            <xs:complexType name="AddressType">
                <xs:sequence>
                    <xs:element name="AddressLine" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                    <xs:element name="City" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="State" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Zip" type="xs:string" minOccurs="0"/>
                    <xs:element name="Country" type="xs:string" minOccurs="0"></xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="DirectionType">
                <xs:sequence>
                    <xs:element name="Origin" type="xs:string" minOccurs="0"/>
                    <xs:element name="Destination" type="xs:string" minOccurs="0"/>
                    <xs:element name="Directions" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="RestaurantType">
                <xs:sequence>
                    <xs:element name="Atmosphere" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Cuisine" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Hours" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Name" type="xs:string"/>
                    <xs:element name="Setting" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Description" type="xs:string" minOccurs="0"></xs:element>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="ImageType">
                <xs:sequence>
                    <xs:element name="Height" type="xs:int" minOccurs="0"/>
                    <xs:element name="Width" type="xs:int" minOccurs="0"/>
                    <xs:element name="Caption" type="xs:string" minOccurs="0"/>
                    <xs:element name="Name" type="xs:string" minOccurs="0"/>
                    <xs:element name="ThumbnailUrl" type="xs:string" minOccurs="0"/>
                    <xs:element name="ImageId" type="xs:integer" minOccurs="0"/>
                    <xs:element name="Type" type="xs:string" minOccurs="0"></xs:element>
                    <xs:element name="Url" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="UserReviewSummaryType">
                <xs:sequence>
                    <xs:element name="UserReviewHotelCode" type="xs:string"/>
                    <xs:element name="LogoImageUrl" type="xs:string"/>
                    <xs:element name="RankScale" type="xs:int" minOccurs="0"/>
                    <xs:element name="Rank" type="xs:int" minOccurs="0"/>
                    <xs:element name="MedianUserRating" type="xs:double" minOccurs="0"/>
                    <xs:element name="RatingImageUrl" type="xs:string" minOccurs="0"/>
                    <xs:element name="WriteReviewUrl" type="xs:string" minOccurs="0"/>
                    <xs:element name="TotalUserReviewCount" type="xs:int" minOccurs="0"/>
                    <xs:element name="UserReview" type="tns:UserReviewType" minOccurs="0" maxOccurs="unbounded"/>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="UserReviewType">
                <xs:sequence>
                    <xs:element name="PublishedDate" type="tns:IsoDateType" minOccurs="0"/>
                    <xs:element name="Author" type="xs:string" minOccurs="0"/>
                    <xs:element name="AuthorLocation" type="xs:string" minOccurs="0"/>
                    <xs:element name="Title" type="xs:string" minOccurs="0"/>
                    <xs:element name="Content" type="xs:string" minOccurs="0"/>
                    <xs:element name="ContentSummary" type="xs:string" minOccurs="0"/>
                    <xs:element name="Rating" type="xs:int" minOccurs="0"/>
                    <xs:element name="RatingImage" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="RoomSegmentType">
                <xs:complexContent>
                    <xs:extension base="tns:SegmentType">
                        <xs:sequence>
                            <xs:element name="BookingDescriptor" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="Checkin" type="tns:IsoTimestampType" minOccurs="0"></xs:element>
                            <xs:element name="Checkout" type="tns:IsoTimestampType" minOccurs="0"></xs:element>
                            <xs:element name="Nights" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="Adults" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="Children" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="Infants" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="PackageName" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="ChainCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="BrandCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="HotelCode" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="Ql2Code" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="HotelName" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="HotelRating" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="HotelStarRatingDescription" type="xs:string" minOccurs="0"/>
                            <xs:element name="Address" type="tns:AddressType" minOccurs="0"/>
                            <xs:element name="CustomerSupportPhone" type="xs:string" minOccurs="0"/>
                            <xs:element name="HotelPhone" type="xs:string" minOccurs="0"/>
                            <xs:element name="HotelFax" type="xs:string" minOccurs="0"/>
                            <xs:element name="HotelEmail" type="xs:string" minOccurs="0"/>
                            <xs:element name="RoomName" type="xs:string" minOccurs="0"/>
                            <xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
                            <xs:element name="RateName" type="xs:string" minOccurs="0"/>
                            <xs:element name="RateMemo" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="AdditionalRoomDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="AdditionalHotelDisplayInfo" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="IncludedService" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="Code" type="xs:string"/>
                                        <xs:element name="Name" type="xs:string"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="AvailableRequestService" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="Code" type="xs:string"/>
                                        <xs:element name="Name" type="xs:string"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="RequestedService" type="xs:string" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="SpecialRequests" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="VoucherInformation" type="xs:string" minOccurs="0"/>
                            <xs:element name="RoomPrice" type="tns:RoomPriceType"/>
                            <xs:element name="StrikethroughPrice" type="tns:RoomPriceType" minOccurs="0"/>
                            <xs:element name="ChildAge" type="xs:int" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="RateCode" type="xs:string" minOccurs="0"/>
                            <xs:element name="RoomCode" type="xs:string" minOccurs="0"/>
                            <xs:element name="DetailsAvailable" type="xs:boolean" minOccurs="0"></xs:element>
                            <xs:element name="SupplierConfirmationSent" type="xs:boolean" minOccurs="0"></xs:element>
                            <xs:element name="MealType" type="xs:string" minOccurs="0"/>
                            <xs:element name="RoomTypeId" type="xs:integer" minOccurs="0"/>
                            <xs:element name="RoomContractId" type="xs:integer" minOccurs="0"/>
                            <xs:element name="FeeDueAtHotelList" type="tns:FeeDueAtHotelListType" minOccurs="0" maxOccurs="1"></xs:element>
                            <xs:element name="CustomFields" minOccurs="0">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="CustomField" type="tns:CustomFieldType" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="RoomingList" type="tns:GuestListType" minOccurs="0"></xs:element>
                            <xs:element name="Promotion" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="IsPrimary" type="xs:boolean"/>
                                        <xs:element name="Message" type="xs:string" minOccurs="0"/>
                                        <xs:element name="LongPrimaryMessage" type="xs:string" minOccurs="0"/>
                                        <xs:element name="IsClientExclusive" type="xs:boolean" minOccurs="0"/>
                                        <xs:element name="PromoType" type="xs:string" minOccurs="0"/>
                                        <xs:element name="TermsAndConditions" type="xs:string" minOccurs="0"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="RoomCancellationPolicies" minOccurs="0">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="RoomCancellationPolicy" type="tns:RoomCancellationPolicyType" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="NonRefundable" type="xs:boolean" default="false" minOccurs="0"></xs:element>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>
            <xs:complexType name="SegmentType">
                <xs:complexContent>
                    <xs:extension base="tns:PnrElementType">
                        <xs:sequence>
                            <xs:element name="BookingComponent" type="tns:BookingComponentType" minOccurs="0" maxOccurs="unbounded"></xs:element>
                            <xs:element name="SegmentId" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="OriginalSegmentId" type="xs:int" minOccurs="0" maxOccurs="1"></xs:element>
                            <xs:element name="SwitchflyBookingNumber" type="xs:int" minOccurs="0"></xs:element>
                            <xs:element name="SegmentLocator" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="CrsName" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="CrsRecordLocator" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="PassiveCrsRecordLocator" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="SupplierLocator" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="Policies" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="CrsPolicies" type="xs:string" minOccurs="0"></xs:element>
                            <xs:element name="AgencyName" type="xs:string" minOccurs="0"/>
                            <xs:element name="AgencyAddress" type="xs:string" minOccurs="0"/>
                            <xs:element name="AgentName" type="xs:string" minOccurs="0"/>
                            <xs:element name="ConfirmationStatus" type="xs:string" minOccurs="0"/>
                            <xs:element name="SpecialId" type="xs:int" minOccurs="0"/>
                            <xs:element name="Supplier" type="tns:InternalSupplierType" minOccurs="0"></xs:element>
                            <xs:element name="BookingMemos" minOccurs="0">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="BookingMemo" type="tns:BookingMemoType" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="ConnectorCustomInfos" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="ConnectorCustomInfo" type="tns:ConnectorCustomInfoType" minOccurs="0" maxOccurs="unbounded"/>
                                    </xs:sequence>
                                </xs:complexType>
                            </xs:element>
                            <xs:element name="ArrivalAirConnectionInfo" type="tns:AirConnectionInfoType" minOccurs="0" maxOccurs="1"></xs:element>
                            <xs:element name="DepartureAirConnectionInfo" type="tns:AirConnectionInfoType" minOccurs="0" maxOccurs="1"></xs:element>
                            <xs:element name="SingleUseCard" type="xs:string" minOccurs="0"/>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>

            <xs:complexType name="PnrElementType">
                <xs:annotation>
                    <xs:documentation/>
                </xs:annotation>
                <xs:sequence>
                    <xs:element name="CorrelationId" type="xs:string" minOccurs="0">
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="InternalSupplierType">
                <xs:sequence>
                    <xs:element name="Product" type="tns:ProductType">
                    </xs:element>
                    <xs:element name="PropertyType" minOccurs="0">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="hotel"/>
                                <xs:enumeration value="bandb"/>
                                <xs:enumeration value="boutique"/>
                                <xs:enumeration value="condo"/>
                                <xs:enumeration value="vacation_rental"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="SupplierId" type="xs:int" minOccurs="0">
                    </xs:element>
                    <xs:element name="SupplierName" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="NotificationMethod" minOccurs="0">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:enumeration value="email"/>
                                <xs:enumeration value="fax"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="NotificationEmail" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="NotificationFax" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="SupplierCode" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="AgencyCode" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="CurrencyCode" minOccurs="0">
                        <xs:simpleType>
                            <xs:restriction base="xs:string">
                                <xs:maxLength value="3" fixed="true"/>
                                <xs:minLength value="3" fixed="true"/>
                            </xs:restriction>
                        </xs:simpleType>
                    </xs:element>
                    <xs:element name="SupplierOnlyBookingMemo" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="SupplierAddress" type="tns:AddressType" minOccurs="0">
                    </xs:element>
                    <xs:element name="SupplierPhone" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="SubAreaId" type="xs:int" minOccurs="0">
                    </xs:element>
                    <xs:element name="SubAreaId2" type="xs:int" minOccurs="0">
                    </xs:element>
                    <xs:element name="MasterSupplier" type="tns:InternalSupplierType" minOccurs="0">
                    </xs:element>
                    <xs:element name="AirportCode" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="ShouldSendNetsOnManifest" type="xs:boolean" default="false" minOccurs="0">
                    </xs:element>
                    <xs:element name="CheckinTime" type="tns:IsoTimeType" minOccurs="0"/>
                    <xs:element name="CheckoutTime" type="tns:IsoTimeType" minOccurs="0"/>
                    <xs:element name="Translations" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Translation" type="tns:HotelTranslationType" minOccurs="0" maxOccurs="unbounded"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:simpleType name="ProductType">
                <xs:restriction base="xs:string">
                    <xs:enumeration value="room"/>
                    <xs:enumeration value="car"/>
                    <xs:enumeration value="air"/>
                    <xs:enumeration value="activity"/>
                    <xs:enumeration value="booking fee"/>
                    <xs:enumeration value="discount"/>
                    <xs:enumeration value="insurance"/>
                    <xs:enumeration value="merchandise"/>
                    <xs:enumeration value="master_supplier"/>
                    <xs:enumeration value="api"/>
                </xs:restriction>
            </xs:simpleType>

            <xs:simpleType name="IsoTimeType">
                <xs:restriction base="xs:string"/>
            </xs:simpleType>


            <xs:complexType name="HotelTranslationType">
                <xs:sequence>
                    <xs:element name="LanguageId" type="xs:int" minOccurs="1"/>
                    <xs:element name="HotelAmenities" type="xs:string" minOccurs="0"/>
                    <xs:element name="HotelDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="RoomDescription" type="xs:string" minOccurs="0"/>
                    <xs:element name="RoomAmenities" type="xs:string" minOccurs="0"/>
                    <xs:element name="Directions" type="xs:string" minOccurs="0"/>
                    <xs:element name="Restaurants" type="xs:string" minOccurs="0"/>
                    <xs:element name="Entertainment" type="xs:string" minOccurs="0"/>
                    <xs:element name="Policies" type="xs:string" minOccurs="0"/>
                    <xs:element name="AdditionalInfo" type="xs:string" minOccurs="0"/>
                    <xs:element name="SpecialNotes" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="AirConnectionInfoType">
                <xs:sequence>
                    <xs:element name="AirlineCode" type="xs:string" minOccurs="0"/>
                    <xs:element name="AirlineName" type="xs:string" minOccurs="0"/>
                    <xs:element name="FlightNumber" type="xs:string" minOccurs="0"/>
                    <xs:element name="Time" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="RoomPriceType">
                <xs:complexContent>
                    <xs:extension base="tns:PriceType">
                        <xs:sequence>
                            <xs:element name="DailyTotal" type="xs:double" minOccurs="0" maxOccurs="unbounded">
                            </xs:element>
                            <xs:element name="DailyTotalNoTax" type="xs:double" minOccurs="0" maxOccurs="unbounded">
                            </xs:element>
                            <xs:element name="ExchangeRateSnapshotId" type="xs:int" minOccurs="0"/>
                            <xs:element name="SupplierToCustomerExchangeRate" type="xs:double" minOccurs="0"/>
                        </xs:sequence>
                    </xs:extension>
                </xs:complexContent>
            </xs:complexType>

            <xs:complexType name="PriceType">
                <xs:sequence>
                    <xs:element name="Total" type="xs:double">
                    </xs:element>
                    <xs:element name="TaxTotal" type="xs:double" minOccurs="0">
                    </xs:element>
                    <xs:element name="SupplierCurrency" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="TaxDetails" type="tns:TaxDetailsType" minOccurs="0"/>
                    <xs:element name="Commission" type="xs:double" minOccurs="0">
                    </xs:element>
                    <xs:element name="CrsCommission" type="xs:double" minOccurs="0"/>
                    <xs:element name="CostOfGoods" type="xs:double" minOccurs="0">
                    </xs:element>
                    <xs:element name="IncludesTaxesAndExtraCharges" type="xs:boolean">
                    </xs:element>
                    <xs:element name="Currency" type="xs:string">
                    </xs:element>
                    <xs:element name="PassThrough" type="xs:boolean">
                    </xs:element>
                    <xs:element name="SupplierSoldAtRounding" type="xs:double" minOccurs="0">
                    </xs:element>
                    <xs:element name="ClientAccountCode" type="xs:string" minOccurs="0">
                    </xs:element>
                    <xs:element name="RedemptionPrice" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="ContractName" type="xs:string">
                                </xs:element>
                                <xs:element name="RuleName" type="xs:string" minOccurs="0">
                                </xs:element>
                                <xs:element name="MaxPoints" type="xs:int">
                                </xs:element>
                                <xs:element name="Currency" type="xs:string">
                                </xs:element>
                                <xs:element name="MaxPointsValue" type="xs:double">
                                </xs:element>
                                <xs:element name="AwardDesignator" type="xs:string" minOccurs="0">
                                </xs:element>
                                <xs:element name="LoyaltyPromotionContractName" type="xs:string"
                                    minOccurs="0">
                                </xs:element>
                                <xs:element name="LoyaltyPromotionPoints" type="xs:integer" minOccurs="0">
                                </xs:element>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="SystemCurrencyPrice" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Total" type="xs:double"/>
                                <xs:element name="TaxTotal" type="xs:double"/>
                                <xs:element name="TaxDetails" type="tns:TaxDetailsType"/>
                                <xs:element name="Commission" type="xs:double" minOccurs="0"/>
                                <xs:element name="CrsCommission" type="xs:double" minOccurs="0"/>
                                <xs:element name="CostOfGoods" type="xs:double" minOccurs="0"/>
                                <xs:element name="Currency" type="xs:string"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="TaxDetailsType">
                <xs:annotation>
                    <xs:documentation/>
                </xs:annotation>
                <xs:sequence>
                    <xs:element name="Tax" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:attribute name="Name" type="xs:string"/>
                            <xs:attribute name="Type" type="xs:string"/>
                            <xs:attribute name="Amount" type="xs:double"/>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>
            
            <xs:complexType name="FeeDueAtHotelListType">
                <xs:sequence>
                    <xs:element name="FeeDueAtHotel" minOccurs="0" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="Currency" type="xs:string" minOccurs="0"/>
                                <xs:element name="Fee" type="xs:decimal" minOccurs="0"/>
                                <xs:element name="Description" type="xs:string" minOccurs="1"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="GuestListType">
                <xs:sequence>
                    <xs:element name="Guest" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="FirstName" type="xs:string" minOccurs="0"/>
                                <xs:element name="LastName" type="xs:string" minOccurs="0"/>
                                <xs:element name="Title" type="xs:string" minOccurs="0"/>
                                <xs:choice minOccurs="0">
                                    <xs:element name="AgeInMonths" type="xs:int"/>
                                    <xs:element name="Age" type="xs:int"/>
                                </xs:choice>
                                <xs:element name="AgeClass" minOccurs="0">
                                    <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                            <xs:enumeration value="ADULT"/>
                                            <xs:enumeration value="CHILD"/>
                                            <xs:enumeration value="SENIOR"/>
                                            <xs:enumeration value="INFANT_REQUIRING_SEAT"/>
                                            <xs:enumeration value="INFANT_IN_LAP"/>
                                        </xs:restriction>
                                    </xs:simpleType>
                                </xs:element>
                                <xs:element name="OfficialTravelerId" type="xs:string" minOccurs="0"/>
                                <xs:element name="OfficialTravelerIdLabel" type="xs:string" minOccurs="0">
                                </xs:element>
                                <xs:element name="OfficialTravelerIdCountry" type="xs:string" minOccurs="0"/>
                                <xs:element name="DateOfBirth" type="tns:IsoDateType" minOccurs="0"/>
                                <xs:element name="AirLoyaltyPrograms" minOccurs="0">
                                    <xs:complexType>
                                        <xs:sequence>
                                            <xs:element name="LoyaltyProgram" minOccurs="0" type="tns:LoyaltyProgramType"
                                                maxOccurs="unbounded"/>
                                        </xs:sequence>
                                    </xs:complexType>
                                </xs:element>
                                <xs:element name="GovernmentFinancialId" type="xs:string" minOccurs="0"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="LoyaltyProgramType">
                <xs:sequence>
                    <xs:element name="Company" type="xs:string">
                    </xs:element>
                    <xs:element name="Number" type="xs:string">
                    </xs:element>
                    <xs:element name="TierCode" type="xs:string" minOccurs="0">
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="RoomCancellationPolicyType">
                <xs:sequence>
                    <xs:element name="Description" type="xs:string">
                    </xs:element>
                    <xs:sequence>
                        <xs:element name="DeadlineTimestamp" minOccurs="0">
                        </xs:element>
                        <xs:choice>
                            <xs:element name="Percentage" type="xs:double">
                            </xs:element>
                            <xs:element name="NumberOfNights" type="xs:int">
                            </xs:element>
                            <xs:element name="FlatFee">
                                <xs:complexType>
                                    <xs:simpleContent>
                                        <xs:extension base="xs:double">
                                            <xs:attribute name="Currency" type="xs:string"/>
                                        </xs:extension>
                                    </xs:simpleContent>
                                </xs:complexType>
                            </xs:element>
                        </xs:choice>
                    </xs:sequence>
                    <xs:element name="CalculatedFeeAmount" minOccurs="0">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="xs:double">
                                    <xs:attribute name="Currency" type="xs:string"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="CustomFieldType">
                <xs:sequence>
                    <xs:element name="Id" type="xs:int" minOccurs="0"/>
                    <xs:element name="Name" type="xs:string" minOccurs="0"/>
                    <xs:element name="Value" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>

            <xs:complexType name="ConnectorCustomInfoType">
                <xs:sequence>
                    <xs:element name="Key" type="xs:string"/>
                    <xs:element name="Value" type="xs:string"/>
                </xs:sequence>
            </xs:complexType>
            
            <xs:complexType name="BookingMemoType">
                <xs:sequence>
                    <xs:element name="Timestamp" type="tns:IsoTimestampType" minOccurs="0"/>
                    <xs:element name="Agent" type="xs:string" minOccurs="0"/>
                    <xs:element name="MemoCode" type="xs:string" minOccurs="0"/>
                    <xs:element name="Memo" type="xs:string" minOccurs="0"/>
                </xs:sequence>
            </xs:complexType>














        </xs:schema>
    </wsdl:types>
    <wsdl:message name="AvailabilityRQ">
        <wsdl:part name="parameters" element="tns:AvailabilityRQ"/>
    </wsdl:message>
    <wsdl:message name="AvailabilityRS">
        <wsdl:part name="parameters" element="tns:AvailabilityRS"/>
    </wsdl:message>
    <wsdl:portType name="switchflyportType">
        <wsdl:operation name="Availability">
            <wsdl:input message="tns:AvailabilityRQ"/>
            <wsdl:output message="tns:AvailabilityRS"/>
        </wsdl:operation>
    </wsdl:portType>
    <wsdl:binding name="switchflyBinding" type="tns:switchflyportType">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="Availability">
            <soap:operation soapAction="http://otas.distantis.com/otas/webservice/v1.2/test.php#Availability" style="document"/>
            <wsdl:input>
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal"/>
            </wsdl:output>
            <wsdl:documentation>Availability</wsdl:documentation>
        </wsdl:operation>
    </wsdl:binding>
    <wsdl:service name="DistantisServices">
        <wsdl:port name="switchflyPort" binding="tns:switchflyBinding">
            <soap:address location="http://otas.distantis.com/otas/webservice/v1.2/test.php"/>
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>';
return $aux;
	}
	
	//$str =  cabeza();
	//$str .=  booking_api();
	//$str .=  aditionals();
	//$str .=  search_objects();
	//$str .=  common_objects();
	//$str .=  definitions();
	$str=fuckthisshit();
	if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
    if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
    if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
    return $str;
}
?>