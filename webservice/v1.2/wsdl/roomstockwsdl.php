<?php
$functions = array();
	//$variablectm = '<s:complexType name="RoomOccupancy"><s:sequence><s:element minOccurs="1" maxOccurs="unbounded" name="RoomSegment" type="tns:RoomSegmentType"/></s:sequence></s:complexType>';
    $serviceName = "Distantis Services";
	$functions[] = array("funcName" => "Availability",
                        "doc" => "Availability",
                        "inputParams" => array(array("name" => "UserId", "type" => "string"),
										array("name" => "Password", "type" => "string"),
										array("name" => "Currency", "type" => "string"),
										array("name" => "RoomSearch", "type" => "RoomSearch")),
						"RoomSearch" => array(array("name" => "SearchLocation", "type" => "SearchLocation"),
										array("name" => "MaxResults", "type" => "string"),
										array("name" => "RoomOccupancy", "type" => "RoomOccupancy"),
										array("name" => "CheckinDate", "type" => "string"),
										array("name" => "CheckoutDate", "type" => "string")),
						"SearchLocation" => array(array("name" => "Location", "type" => "string")),
						"RoomOccupancy" => array(array("name" => "ChildAge", "type" => "string")),
						"RoomOccupancyAttributes" => array(
										array("name" => "adults", "type" => "string", "use" =>"required"),
										array("name" => "children", "type" => "string")),
                        "outputParams" => array(
									array("name" => "RoomSearchResponse", "type"=> "RoomSearchResponseType")),
						"RoomSearchResponseType" => array(
									array("name" => "Hotel", "type"=> "HotelType")),
						"HotelType" => array(
									array("name" => "BookingDescriptor", "type"=> "string"),
									array("name" => "HotelName", "type"=> "string"),
									array("name" => "RoomType", "type"=> "string"),
									array("name" => "RoomTypesDescriptions", "type"=> "string"),
									array("name" => "Adress", "type"=> "AdressType"),
									array("name" => "Amenity", "type"=> "string"),
									array("name" => "Rating", "type"=> "string"),
									array("name" => "Currency", "type"=> "string"),
									array("name" => "MinimumPrice", "type"=> "string"),
									array("name" => "RoomOccupancy", "type"=> "RoomOccupancyRS"),
									array("name" => "RoomAmenity ", "type"=> "string"),
									array("name" => "Image", "type"=> "ImageType")),
						"AdressType" => array(
									array("name" => "AddressLine", "type"=> "string"),
									array("name" => "City", "type"=> "string"),
									array("name" => "State", "type"=> "string"),
									array("name" => "Country", "type"=> "string")),
						"RoomOccupancyRS" => array(
								array("name" => "RoomSegment", "type" => "RoomSegmentType")),
						"ImageType" => array(
									array("name" => "ThumbnailUrl ", "type"=> "string"),
									array("name" => "Type", "type"=> "string")),
						"RoomSegmentType" => array(
									array("name" => "RateName", "type"=> "string"),
									array("name" => "RoomPrice", "type"=> "RoomPrice"),
									array("name" => "RoomCancellationPolicies", "type"=> "RoomCancellationPolicies")),
						"RoomPrice" => array(
									array("name" => "Total", "type"=> "string"),
									array("name" => "TaxTotal", "type"=> "string"),
									array("name" => "TaxDetails", "type"=> "TaxDetailsType"),
									array("name" => "Commission", "type"=> "string"),
									array("name" => "IncludesTaxesAndExtraCharges", "type"=> "string"),
									array("name" => "Currency", "type"=> "string"),
									array("name" => "PassThrough", "type"=> "string"),
									array("name" => "DailyTotal", "type"=> "string"),
									array("name" => "DailyTotalNoTax", "type"=> "string")),
						"TaxDetailsType" => array(
									array("name" => "Tax", "type"=> "string")),
						"TaxAttributes"	=> array(
									array("name" => "Name", "type"=> "string"),
									array("name" => "Type", "type"=> "string"),
									array("name" => "Amount", "type"=> "double")),
						"RoomCancellationPolicies" => array(
									array("name" => "RoomCancellationPolicy", "type"=> "RoomCancellationPolicy")),
						"RoomCancellationPolicy" => array(
									array("name" => "Description", "type"=> "string"),
									array(array("name" => "DeadlineTimestamp", "type"=> "string")),
									array("name" => "CalculatedFeeAmount", "type"=> "string")),
						"RoomCancellationPolicyChoices" => array(
									array("name" => "Percentage", "type"=> "string"),
									array("name" => "NumberOfNights", "type"=> "string"),
									array("name" => "FlatFee", "type"=> "string")),
						"FlatFeeAttributes"=> array(
									array("name" => "Currency", "type"=> "string")),
						"CalculatedFeeAmountAttributes"=> array(
									array("name" => "Currency", "type"=> "string")),
                        "soapAddress" => "http://otas.distantis.com/otas/webservice/v1.2/roomstock.php"
                         );
						 
    if (stristr($_SERVER['QUERY_STRING'], "wsdl")){
		header("Content-Type: application/soap+xml; charset=utf-8");
        echo DisplayXML();
    }else{
    	$cp = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
    	echo '<!-- Attention: To access via a SOAP client use ' . $cp . '?WSDL -->';
    	echo '<html>';
    	echo '<head><title>' . $serviceName . '</title></head>';
    	echo '<body>';
    	echo '<h1>' . $serviceName . '</h1>';
        echo '<p style="margin-left:20px;">To access via a SOAP client use <code>' . $cp . '?WSDL</code></p>';
        echo '<h2>Available Functions:</h2>';
        echo '<div style="margin-left:20px;">';
        for ($i=0;$i<count($functions);$i++) {
            echo '<h3>Function: ' . $functions[$i]['funcName'] . '</h3>';
            echo '<div style="margin-left:20px;">';
            echo '<p>';
            echo $functions[$i]['doc'];
            echo '<ul>';
            if (array_key_exists("inputParams", $functions[$i])) {
            	echo '<li>Input Parameters:<ul>';
            	for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
            	   	echo '<li>' . $functions[$i]['inputParams'][$j]['name'];
            	   	echo ' (' . $functions[$i]['inputParams'][$j]['type'];
            	   	echo ')</li>';
            	}
            	echo '</ul></li>';
            }
            if (array_key_exists("outputParams", $functions[$i])) {
                echo '<li>Output Parameters:<ul>';
                for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
                    echo '<li>' . $functions[$i]['outputParams'][$j]['name'];
                    echo ' (' . $functions[$i]['outputParams'][$j]['type'];
                    echo ')</li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
            echo '</p>';
            echo '</div>';
        }
        echo '</div>';
        	
    	echo '<h2>WSDL output:</h2>';
        echo '<pre style="margin-left:20px;width:800px;overflow-x:scroll;border:1px solid black;padding:10px;background-color:#D3D3D3;">';
        echo DisplayXML(false);
        echo '</pre>';
        echo '</body></html>';
    }                         
    exit;
	
function DisplayXML($xmlformat=true) {
	global $functions;         // Functions that this web service supports
	global $serviceName;       // Web Service ID
	$i = 0;                    // For traversing functions array
	$j = 0;                    // For traversing parameters arrays
	$str = '';                 // XML String to output
	
	// Tab spacings
	$t1 = '    ';
	if (!$xmlformat) $t1 = '&nbsp;&nbsp;&nbsp;&nbsp;';
	$t2 = $t1 . $t1;
	$t3 = $t2 . $t1;
	$t4 = $t3 . $t1;
	$t5 = $t4 . $t1;
	
	$serviceID = str_replace(" ", "", $serviceName);
	
	// Declare XML format
    $str .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\n\n";
    
    // Declare definitions / namespaces
    $str .= '<wsdl:definitions ' . "\n"; 
    $str .= $t1 . 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" ' . "\n";
    $str .= $t1 . 'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' . "\n";
    $str .= $t1 . 'xmlns:s="http://www.w3.org/2001/XMLSchema" ' . "\n"; 
    $str .= $t1 . 'targetNamespace="http://www.darkerwhite.com/" ' . "\n";
    $str .= $t1 . 'xmlns:tns="http://www.darkerwhite.com/" ' . "\n"; 
    $str .= $t1 . 'name="' . $serviceID . '" ' . "\n";
    $str .= '>' . "\n\n";
	
	// Declare Types / Schema
    $str .= '<wsdl:types>' . "\n";
    $str .= $t1 . '<s:schema elementFormDefault="qualified" targetNamespace="http://www.darkerwhite.com/">' . "\n";
    for ($i=0;$i<count($functions);$i++) {
    	// Define Request Types
    	if (array_key_exists("inputParams", $functions[$i])) {
    	   $str .= $t2 . '<s:element name="' . $functions[$i]['funcName'] . 'RQ">' . "\n";
    	   $str .= $t3 . '<s:complexType><s:sequence>' . "\n";		    
    	   for ($j=0;$j<count($functions[$i]['inputParams']);$j++) {
			   $formato="s";
			   $maxoccur="1";
			   if($functions[$i]['inputParams'][$j]['type']<>"string" && $functions[$i]['inputParams'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
    	       $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
    	       $str .= 'name="' . $functions[$i]['inputParams'][$j]['name'] . '" ';
    	       $str .= 'type="'.$formato.':' . $functions[$i]['inputParams'][$j]['type'] . '" />' . "\n";	
    	   }
    	   $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
           $str .= $t2 . '</s:element>' . "\n";
    	}

		if (array_key_exists("RoomSearch", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomSearch"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomSearch']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomSearch'][$j]['type']<>"string" && $functions[$i]['RoomSearch'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomSearch'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomSearch'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("SearchLocation", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="SearchLocation"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['SearchLocation']);$j++) {
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="'.$functions[$i]['SearchLocation'][$j]['name'].'" ';
				$str .= 'type="s:'.$functions[$i]['SearchLocation'][$j]['type'].'" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomOccupancy", $functions[$i])) {		   
			$str .= $t3 . '<s:complexType  name="RoomOccupancy">'."\n";
			$str .= $t4 . '<s:sequence>'."\n";			
			for ($j=0;$j<count($functions[$i]['RoomOccupancy']);$j++) {
				$formato="s";
				$maxoccur="unbounded";
				if($functions[$i]['RoomOccupancy'][$j]['type']<>"string" 
					&& $functions[$i]['RoomOccupancy'][$j]['type']<>"integer")
						{$formato="tns"; $maxoccur="unbounded";}
				$str .= $t5 . '<s:element minOccurs="0" maxOccurs="'.$maxoccur.'" ';
				$str .= 'name="'.$functions[$i]['RoomOccupancy'][$j]['name'].'" ';
				$str .= 'type="'.$formato.':'.$functions[$i]['RoomOccupancy'][$j]['type'].'" />'."\n";	
			}
			$str .= $t4 . '</s:sequence>'."\n";
			if(array_key_exists('RoomOccupancyAttributes', $functions[$i])){
			   for($j=0;$j<count($functions[$i]['RoomOccupancyAttributes']);$j++){
				   $use='';
				   if(isset($functions[$i]['RoomOccupancyAttributes'][$j]['use'])){
					   $use='use="'.$functions[$i]['RoomOccupancyAttributes'][$j]['use'].'"';
				   }
					$str .= $t4 . '<s:attribute name="'.$functions[$i]['RoomOccupancyAttributes'][$j]['name'].'" '.$use.' /> '."\n";
			   }
		   }
			$str .= $t3.'</s:complexType>' . "\n";
		}
		if (array_key_exists("outputParams", $functions[$i])) {
			$str .= $t2 . '<s:element name="'.$functions[$i]['funcName'].'RS">' . "\n";
			$str .= $t3 . '<s:complexType ><s:sequence>' . "\n";			
			for ($j=0;$j<count($functions[$i]['outputParams']);$j++) {
				$formato="s";
				$maxoccur="1";
				if($functions[$i]['outputParams'][$j]['type']<>"string" 
					&& $functions[$i]['outputParams'][$j]['type']<>"integer")
						{$formato="tns"; $maxoccur="unbounded";}
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
				$str .= 'name="' . $functions[$i]['outputParams'][$j]['name'] . '" ';
				$str .= 'type="'.$formato.':'.$functions[$i]['outputParams'][$j]['type'].'" />' . "\n";
           }
           $str .= $t3 . '</s:sequence></s:complexType>' . "\n";
           $str .= $t2 . '</s:element>' . "\n";
        }
		if (array_key_exists("RoomSearchResponseType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomSearchResponseType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomSearchResponseType']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomSearchResponseType'][$j]['type']<>"string" && $functions[$i]['RoomSearchResponseType'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomSearchResponseType'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomSearchResponseType'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("HotelType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="HotelType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['HotelType']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['HotelType'][$j]['type']<>"string" && $functions[$i]['HotelType'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="0" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['HotelType'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['HotelType'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("AdressType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="AdressType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['AdressType']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['AdressType'][$j]['type']<>"string" && $functions[$i]['AdressType'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['AdressType'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['AdressType'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomOccupancyRS", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomOccupancyRS"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomOccupancyRS']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomOccupancyRS'][$j]['type']<>"string" && $functions[$i]['RoomOccupancyRS'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomOccupancyRS'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomOccupancyRS'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("ImageType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="ImageType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['ImageType']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['ImageType'][$j]['type']<>"string" && $functions[$i]['ImageType'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="0" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['ImageType'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['ImageType'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomSegmentType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomSegmentType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomSegmentType']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomSegmentType'][$j]['type']<>"string" && $functions[$i]['RoomSegmentType'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="0" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomSegmentType'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomSegmentType'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomPrice", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomPrice"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomPrice']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomPrice'][$j]['type']<>"string" && $functions[$i]['RoomPrice'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomPrice'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomPrice'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("TaxDetailsType", $functions[$i])) {
			$str .= $t2 . '<s:complexType  name="TaxDetailsType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['TaxDetailsType']);$j++) {
				$formato="s";
				$maxoccur="unbounded";
				$str .= $t3 . '<s:element minOccurs="0" maxOccurs="'.$maxoccur.'" ';
				$str .= 'name="' . $functions[$i]['TaxDetailsType'][$j]['name'] . '" >' . "\n";
				$auxiliar = $functions[$i]['TaxDetailsType'][$j]['name']."Attributes";
				if(array_key_exists($auxiliar, $functions[$i])) {
					$str .= $t3. '<s:complexType>';
					for($xx=0;$xx<count($functions[$i][$auxiliar]);$xx++){
						$str .= $t4. '<s:attribute name="'.$functions[$i][$auxiliar][$xx]['name'].'" type="s:'.$functions[$i][$auxiliar][$xx]['type'].'" />';
					}
					$str .= $t3. '</s:complexType>';
				}
				$str .= $t3 . '</s:element>';
			}
			$str .= $t2 . '</s:sequence></s:complexType>' . "\n";
		}		
		if (array_key_exists("RoomCancellationPolicies", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomCancellationPolicies"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomCancellationPolicies']);$j++) {
				$formato="s";
			   $maxoccur="1";
			   if($functions[$i]['RoomCancellationPolicies'][$j]['type']<>"string" && $functions[$i]['RoomCancellationPolicies'][$j]['type']<>"integer"){
				   $formato="tns"; $maxoccur="unbounded";
				}
			   $str .= $t4 . '<s:element minOccurs="1" maxOccurs="'.$maxoccur.'" ';
			   $str .= 'name="' . $functions[$i]['RoomCancellationPolicies'][$j]['name'] . '" ';
			   $str .= 'type="'.$formato.':' . $functions[$i]['RoomCancellationPolicies'][$j]['type'] . '" />' . "\n";
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		if (array_key_exists("RoomCancellationPolicy", $functions[$i])) {
			$str .= $t1 . '<s:complexType  name="RoomCancellationPolicy"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomCancellationPolicy']);$j++) {
				if(isset($functions[$i]['RoomCancellationPolicy'][$j]['name'])){
					$auxiliar = $functions[$i]['RoomCancellationPolicy'][$j]['name'].'Attributes';
					if(array_key_exists($auxiliar, $functions[$i])){
						$str .= $t2. '<s:element  name="'.$functions[$i]['RoomCancellationPolicy'][$j]['name'].'" >'."\n";
						$str .= $t3. '<s:complexType><s:simpleContent><s:extension base="s:string">'."\n";
						for($zz=0;$zz<count($functions[$i][$auxiliar]);$zz++){
							$str .= $t5.$t1. '<s:attribute name="'.$functions[$i][$auxiliar][$zz]['name'].'" type= "'.$functions[$i][$auxiliar][$zz]['type'].'" /> '."\n";
						}                   
						$str .= $t3. '</s:extension></s:simpleContent></s:complexType>'."\n";
						$str .= $t2. '</s:element>'."\n";
					}else{
						$str .= $t2 . '<s:element minOccurs="1" maxOccurs="1" ';
						$str .= 'name="'.$functions[$i]['RoomCancellationPolicy'][$j]['name'].'" ';
						$str .= 'type="s:'.$functions[$i]['RoomCancellationPolicy'][$j]['type'].'" />' . "\n";
					}
				}else{
					$str .= $t2 .'<s:sequence>' . "\n";
					for ($y=0;$y<count($functions[$i]['RoomCancellationPolicy'][$j]);$y++){
						$str .= $t3 . '<s:element minOccurs="0"  ';
						$str .= 'name="'.$functions[$i]['RoomCancellationPolicy'][$j][$y]['name'].'" ';
						$str .= 'type="s:'.$functions[$i]['RoomCancellationPolicy'][$j][$y]['type'].'" />' . "\n";
						if (array_key_exists("RoomCancellationPolicyChoices", $functions[$i])){
							$str .= $t3 .'<s:choice>'."\n";
							for ($jj=0;$jj<count($functions[$i]['RoomCancellationPolicyChoices']);$jj++){
                                $auxiliar = $functions[$i]['RoomCancellationPolicyChoices'][$jj]['name'].'Attributes';
                                if(array_key_exists($auxiliar, $functions[$i])){
                                    $str .= $t4. '<s:element  name="'.$functions[$i]['RoomCancellationPolicyChoices'][$jj]['name'].'" >'."\n";
                                    $str .= $t5. '<s:complexType><s:simpleContent><s:extension base="s:string">'."\n";
                                    for($xx=0;$xx<count($functions[$i][$auxiliar]);$xx++){
                                        $str .= $t5.$t1. '<s:attribute name="'.$functions[$i][$auxiliar][$xx]['name'].'" type= "'.$functions[$i][$auxiliar][$xx]['type'].'" /> '."\n";
                                    }                   
                                    $str .= $t5. '</s:extension></s:simpleContent></s:complexType>'."\n";
                                    $str .= $t4. '</s:element>'."\n";
                                }else{
                                    $str .= $t4 . '<s:element ';
                                    $str .= 'name="'.$functions[$i]['RoomCancellationPolicyChoices'][$jj]['name'].'" ';
                                    $str .= 'type="s:'.$functions[$i]['RoomCancellationPolicyChoices'][$jj]['type'].'" />' . "\n";
                                }                               
                            }
							$str .= $t3 .'</s:choice>'."\n";
						}
					}
					$str .= $t2 .'</s:sequence>' . "\n";
				}
			}
			$str .= $t1 . '</s:sequence></s:complexType>' . "\n";
		}
		/*if (array_key_exists("RoomSegmentType", $functions[$i])) {
			$str .= $t3 . '<s:complexType  name="RoomSegmentType"><s:sequence>' . "\n";
			for ($j=0;$j<count($functions[$i]['RoomSegmentType']);$j++) {
				$str .= $t4 . '<s:element minOccurs="1" maxOccurs="1" ';
				$str .= 'name="'.$functions[$i]['RoomSegmentType'][$j]['name'].'" ';
				$str .= 'type="s:'.$functions[$i]['RoomSegmentType'][$j]['type'].'" />' . "\n";	
			}
			$str .= $t3 . '</s:sequence></s:complexType>' . "\n";
		}
		*/
    }
    $str .= $t1 . '</s:schema>' . "\n";
    $str .= '</wsdl:types>' . "\n\n";

    // Declare Messages
    for ($i=0;$i<count($functions);$i++) {
    	// Define Request Messages
        if (array_key_exists("inputParams", $functions[$i])) {
        	$str .= '<wsdl:message name="' . $functions[$i]['funcName'] . 'RQ">' . "\n";
            $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['funcName'] . 'RQ" />' . "\n";
            $str .= '</wsdl:message>' . "\n";
        }
        // Define Response Messages
        if (array_key_exists("outputParams", $functions[$i])) {
            $str .= '<wsdl:message name="' . $functions[$i]['funcName'] . 'RS">' . "\n";
            $str .= $t1 . '<wsdl:part name="parameters" element="tns:' . $functions[$i]['funcName'] . 'RS" />' . "\n";
            $str .= '</wsdl:message>' . "\n\n";
        }
    }

    // Declare Port Types
    for ($i=0;$i<count($functions);$i++) {
    	$str .= '<wsdl:portType name="' . $functions[$i]['funcName'] . 'PortType">' . "\n";
    	$str .= $t1 . '<wsdl:operation name="' . $functions[$i]['funcName'] . '">' . "\n";
    	if (array_key_exists("inputParams", $functions[$i])) 
    	   $str .= $t2 . '<wsdl:input message="tns:' . $functions[$i]['funcName'] . 'RQ" />' . "\n";
    	if (array_key_exists("outputParams", $functions[$i])) 
           $str .= $t2 . '<wsdl:output message="tns:' . $functions[$i]['funcName'] . 'RS" />' . "\n";
        $str .= $t1 . '</wsdl:operation>' . "\n";
    	$str .= '</wsdl:portType>' . "\n\n";
    }
    
    // Declare Bindings
    for ($i=0;$i<count($functions);$i++) {
        $str .= '<wsdl:binding name="' . $functions[$i]['funcName'] . 'Binding" type="tns:' . $functions[$i]['funcName'] . 'PortType">' . "\n";
        $str .= $t1 . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />' . "\n";
        $str .= $t1 . '<wsdl:operation name="' . $functions[$i]['funcName'] . '">' . "\n";
        $str .= $t2 . '<soap:operation soapAction="' . $functions[$i]['soapAddress'] . '#' . $functions[$i]['funcName'] . '" style="document" />' . "\n";
        if (array_key_exists("inputParams", $functions[$i]))
            $str .= $t2 . '<wsdl:input><soap:body use="literal" /></wsdl:input>' . "\n";
        if (array_key_exists("outputParams", $functions[$i]))
            $str .= $t2 . '<wsdl:output><soap:body use="literal" /></wsdl:output>' . "\n";
    	$str .= $t2 . '<wsdl:documentation>' . $functions[$i]['doc'] . '</wsdl:documentation>' . "\n";
        $str .= $t1 . '</wsdl:operation>' . "\n";
    	$str .= '</wsdl:binding>' . "\n\n";
    }
    
    // Declare Service
    $str .= '<wsdl:service name="' . $serviceID . '">' . "\n";
    for ($i=0;$i<count($functions);$i++) {
        $str .= $t1 . '<wsdl:port name="' . $functions[$i]['funcName'] . 'Port" binding="tns:' . $functions[$i]['funcName'] . 'Binding">' . "\n";
        $str .= $t2 . '<soap:address location="' . $functions[$i]['soapAddress'] . '" />' . "\n";
        $str .= $t1 . '</wsdl:port>' . "\n";
    }
    $str .= '</wsdl:service>' . "\n\n";
    
    // End Document
    $str .= '</wsdl:definitions>' . "\n";
    
    if (!$xmlformat) $str = str_replace("<", "&lt;", $str); 
    if (!$xmlformat) $str = str_replace(">", "&gt;", $str);
    if (!$xmlformat) $str = str_replace("\n", "<br />", $str);
    return $str;
}

?>