<?php 
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/v1.2/wsdl/bookingroomwsdl.php?WSDL");
$server->addFunction("CreatePnr");
$server->handle();
function CreatePnr($formdata){
	
	$Pnr = new stdClass();
        $SegmentTotals = new stdClass();
        $PnrOwner = new stdClass();
        $LeadTraveler = new stdClass();
        $CreatePnrRS = new stdClass();
        $Segment = new stdClass();
        $CreatePnrRS->Success = "string";
        $Segment->SegmentLocator = "string";
        $Segment->Currency = "string";        
        $Segment->TotalPrice ="5584.4149058039";
        $CreatePnrRS->Segment[] = $Segment;
        $Pnr->RecordLocator = "string";
        $Pnr->BookingTimestamp = "string";
        $Pnr->Status = "string";
        $SegmentTotals->AirTotal =0.00;
        $SegmentTotals->AirTaxTotal =0.00;
        $SegmentTotals->RoomTotal =5584.4149058039;
        $SegmentTotals->RoomTaxTotal =0.00;
        $SegmentTotals->CarTotal =0.00;
        $SegmentTotals->CarTaxTotal =0.00;
        $SegmentTotals->ActivityTotal =0.00;
        $SegmentTotals->ActivityTaxTotal =0.00;
        $SegmentTotals->CruiseTotal =0.00;
        $SegmentTotals->CruiseTaxTotal =0.00;
        $SegmentTotals->InsuranceTotal =0.00;
        $SegmentTotals->InsuranceTaxTotal =0.00;
        $SegmentTotals->MerchandiseTotal =0.00;
        $SegmentTotals->MerchandiseTaxTotal =0.00;
        $SegmentTotals->PassThroughMarkupTotal =0.00;
        $SegmentTotals->BookingFeeClient =0.00;
        $SegmentTotals->BookingFeeTotal =0.00;
        $SegmentTotals->ServiceChargeTotal =0.00;
        $SegmentTotals->AgentMarkup =0.00;
        $SegmentTotals->DiscountTotal =0.00;
    	$Pnr->SegmentTotals[] = $SegmentTotals;
        $Pnr->DepartureDate = "string";
        $PnrOwner->LastName = "Reznick";
		$PnrOwner->FirstName = "Tina";
		$PnrOwner->Age = "35";
		$PnrOwner->AgeClass = "ADULT";
		$PnrOwner->Phone->Type = "cell";
		$PnrOwner->Phone->_ =  "8087357777";
		$PnrOwner->Address = "";
		$PnrOwner->Email = "info@switchfly.com";
		$PnrOwner->BookingMemos = "";
		$LeadTraveler->LastName = "Reznick" ;
		$LeadTraveler->FirstName = "Tina" ;
		$LeadTraveler->Gender = "F" ;
		$LeadTraveler->OfficialTravelerId = "123324324" ;
		$LeadTraveler->OfficialTravelerIdLabel = "PASSPORT" ;
		$LeadTraveler->OfficialTravelerIdCountry = "CL" ;
		$LeadTraveler->Age = "35" ;
		$LeadTraveler->AgeClass = "ADULT" ;
		$LeadTraveler->Title = "Mrs" ;
		$LeadTraveler->Phone->Type = "home";
		$LeadTraveler->Phone->_ = "7357777" ;
		$LeadTraveler->Address = "" ;
		$LeadTraveler->Email = "info@switchfly.com" ;
		$LeadTraveler->BookingMemos = "BookingMemos" ;
        $Pnr->PnrOwner [] = $PnrOwner;
        $Pnr->LeadTraveler[] = $LeadTraveler;        
        $CustomerPricingSummary->Currency = "ABC";
        $CustomerPricingSummary->PackageTotal = "7025.9149058039";
        $CustomerPricingSummary->PaymentTotal = "-5625.0650941961";
        $CustomerPricingSummary->BalanceDueTotal = "1367.2349058039";
        $Pnr->CustomerPricingSummary[] = $CustomerPricingSummary;
        $CreatePnrRS->Pnr[] = $Pnr;
		return $CreatePnrRS;
	$id_webservice=3;
	require_once('../Connections/db1.php');
	require_once('../clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = get_object_vars($formdata);
	$user = $formdata["user"];
	$password = $formdata["password"];
	$extras['clientcode'] = $formdata["clientcode"];
	$foreign = $formdata["foreign"];
	$checkin = $formdata["checkin"];
	$checkout = $formdata["checkout"];
	$hotelid = $formdata["hotelid"];
	$roomid = $formdata["roomid"];
	$currency= $formdata["currency"];
	$total= $formdata["total"];
	$extras['notes']= $formdata["notes"];
	if(isset($formdata["roomdetails"]->sgl)){
		$sgl[0] = $formdata["roomdetails"]->sgl;
		$twin[0] = $formdata["roomdetails"]->twin;
		$mat[0] = $formdata["roomdetails"]->mat;
		$tpl[0] = $formdata["roomdetails"]->tpl;
		if(isset($formdata["roomdetails"]->pax->name)){	
			$paxs[0][0]["name"] = $formdata["roomdetails"]->pax->name;
			$paxs[0][0]["lastname"] = $formdata["roomdetails"]->pax->lastname;
			$paxs[0][0]["countryid"] = $formdata["roomdetails"]->pax->countryid;
			$paxs[0][0]["passport"] = $formdata["roomdetails"]->pax->passport;
		}else{	
			foreach($formdata["roomdetails"]->pax as $pasajero=>$pax_data){
				$paxs[0][$pasajero]["name"] = $pax_data->name;
				$paxs[0][$pasajero]["lastname"] = $pax_data->lastname;
				$paxs[0][$pasajero]["countryid"] = $pax_data->countryid;
				$paxs[0][$pasajero]["passport"] = $pax_data->passport;
			}
		}
	}else{
		foreach($formdata["roomdetails"] as $roomIndex=>$roomData){
			$sgl[$roomIndex] = $roomData->sgl;
			$twin[$roomIndex] = $roomData->twin;
			$mat[$roomIndex] = $roomData->mat;
			$tpl[$roomIndex] = $roomData->tpl;
			if(isset($roomData->pax->name)){
				$paxs[$roomIndex][0]["name"] = $roomData->pax->name;
				$paxs[$roomIndex][0]["lastname"] = $roomData->pax->lastname;
				$paxs[$roomIndex][0]["countryid"] = $roomData->pax->countryid;
				$paxs[$roomIndex][0]["passport"] = $roomData->pax->passport;
			}else{
				foreach($roomData->pax as $pasajero=>$pax_data){
					$paxs[$roomIndex][$pasajero]["name"] = $pax_data->name;
					$paxs[$roomIndex][$pasajero]["lastname"] = $pax_data->lastname;
					$paxs[$roomIndex][$pasajero]["countryid"] = $pax_data->countryid;
					$paxs[$roomIndex][$pasajero]["passport"] = $pax_data->passport;
				}
			}
		}
	}
	if(strtoupper($foreign)=="FALSE" || $foreign=='0'){
		$currency_aux="CLP";
		$foreign=false;
	}else{
		$currency_aux="USD";
		$foreign=true;
	}
	$res=new stdClass();
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($usuario_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->errorcode='105';
		$res->description="Invalid user";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->errorcode='106';
		$res->description='Not allowed user';
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	if($currency_aux!=$currency){
		$res->status='ERROR';
		$res->errorcode='107';
		$res->description="The currency doesn't match with the tag foreign";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	$pais_rq  ="select * from pais where id_pais = ".$paxs[0][0]["countryid"];
	$pais_rs = $db1->Execute($pais_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($pais_rs->RecordCount()==0){
		$res->status='ERROR';
		$res->errorcode='108';
		$res->description="First pax country is mandatory.";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	if(($foreign && $paxs[0][0]["countryid"] == $cot->nationalCountryID) || (!$foreign && $paxs[0][0]["countryid"] != $cot->nationalCountryID)){
		$res->status='ERROR';
		$res->errorcode='109';
		$res->description="First pax country doesn't match with foreign tag";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	$cant_room = count($paxs);
	$habs['sgl']=0;
	$habs['twin']=0;
	$habs['mat']=0;
	$habs['tpl']=0;
	for($i=0;$i<=$cant_room;$i++){
		$total_paxs=0;
		if(is_numeric($sgl[$i])){
			$total_paxs+=$sgl[$i];
			$habs['sgl']+=$sgl[$i];			
		}
		if(is_numeric($twin[$i])){
			$total_paxs+=$twin[$i]*2;
			$habs['twin']+=$twin[$i];
		}
		if(is_numeric($mat[$i])){
			$total_paxs+=$mat[$i]*2;
			$habs['mat']+=$mat[$i];
		}
		if(is_numeric($tpl[$i])){
			$total_paxs+=$tpl[$i]*3;
			$habs['tpl']+=$tpl[$i];
		}
		if($total_paxs<count($paxs[$i])){
			$res->status='ERROR';
			$res->errorcode='110';
			$res->description="Number of paxs doesnt match with rooms($i)";
			$res->bookingcode="";
			$res->refundable='';
			$lista[]=$res;
			return $lista;
		}
		foreach($paxs[$i] as $index=>$pasajero){
			$paxes[]=$paxs[$i][$index];
		}
	}	
	if(is_numeric($hotelid) && is_numeric($roomid)){
		$hotel_rq  ="select * from hotel where id_hotel =".$hotelid;
		$hotel_rs = $db1->Execute($hotel_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($hotel_rs->RecordCount()>=0){
			$cityid=$hotel_rs->Fields('id_ciudad');
		}
	}else{
		$res->status='ERROR';
		$res->errorcode='111';
		$res->description="Invalid hotel or room";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	$arr = $cot->matrizDisponibilidadG($db1, $cityid, $checkin, $checkout, $id_agencia, $habs, $hotelid, $roomid, $foreign);
	if(round($arr[$hotelid]['info']['habs'][$roomid]['totValue'], $cot->decimales)!=round($total, $cot->decimales)){
		$res->status='ERROR';
		$res->errorcode='112';
		$res->description="There's no stock in the requested room";
		$res->bookingcode="";
		$res->refundable='';
		$lista[]=$res;
		return $lista;
	}
	if(trim($extras['notes'])=='' || trim($extras['notes'])=='?' || !isset($extras['notes'])){
		$extras['notes']='';
	}
	if(trim($extras['clientcode'])=='' || trim($extras['clientcode'])=='?' || !isset($extras['clientcode'])){
		$extras['clientcode']='';
	}
	$result = $cot->cotCreator($db1, $arr, $habs, $paxes,$id_usuario,$cityid,$extras);
	$res->status=$result['status'];
	$res->errorcode=$result['errorcode'];
	$res->description=$result['description'];
	$res->bookingcode=$result['bookingcode'];
	if(!isset($result['refundable'])){
		$res->refundable='';
	}else{
		$res->refundable=($result['refundable']==0)?"TRUE":"FALSE";
	}	
	$lista[]=$res;
	return $lista;
}
?>