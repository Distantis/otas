<?php
ini_set("soap.wsdl_cache_enabled", "0");
set_time_limit(0);
$server = new SoapServer("http://otas.distantis.com/otas/webservice/v1.2/wsdl/switchfly_booking_apiwsdl2.php?WSDL");
$server->addFunction("Availability");
$server->addFunction("CreatePnr");
$server->addFunction("CancelPnr");
$server->handle();
function Availability($formdata){
    $id_webservice=13;
    require_once('/var/www/otas/Connections/db1.php');
    require_once('/var/www/otas/clases/cotizacion.php');
    $cot = new Cotizacion();
    $formdata = get_object_vars($formdata);
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
    $currency = $formdata["Currency"];
    if(isset($formdata["RoomSearch"]->CheckinDate)){
		if(isset($formdata["RoomSearch"]->SearchLocation->Location)){
            $Location[0][0] = $formdata["RoomSearch"]->SearchLocation->Location;
        }else{
            foreach($formdata["RoomSearch"]->SearchLocation as $location_index=>$location_data){
                $Location[0][$location_index]["Nombre"] = $location_data->Location;
            }
        }
        $RoomSearch[0]["MaxResults"] = $formdata["RoomSearch"]->MaxResults;
		if(isset($formdata["RoomSearch"]->RoomOccupancy->adults)){
            $adults[0][0] = $formdata["RoomSearch"]->RoomOccupancy->adults;
            $children[0][0] = $formdata["RoomSearch"]->RoomOccupancy->children;
            if($children[0][0]==1){
				$ChildAge[0][0][0]=$formdata["RoomSearch"]->RoomOccupancy->ChildAge;
            }
            if($children[0][0]>1){
				foreach($formdata["RoomSearch"]->RoomOccupancy->ChildAge as $childindex => $childdata){
                    $ChildAge[0][0][$childindex] = $childdata->ChildAge;
                }
            }
        }else{
            foreach($formdata["RoomSearch"]->RoomOccupancy as $roomocup_index=>$roomocu_data){
                $adults[0][$roomocup_index] = $roomocu_data->adults;
                $children[0][$roomocup_index] = $roomocu_data->children;
                if($children[0][$roomocup_index]==1){
					$ChildAge[0][$roomocup_index][0]= $roomocu_data->ChildAge;
                }
                if($children[0][$roomocup_index]>1){
					foreach($roomocu_data->ChildAge as $childindex => $childdata){
                        $ChildAge[0][$roomocup_index][$childindex] = $childdata->ChildAge;
                    }
                }
            }
        }
        $RoomSearch[0]["CheckinDate"] = $formdata["RoomSearch"]->CheckinDate;
        $RoomSearch[0]["CheckoutDate"] = $formdata["RoomSearch"]->CheckoutDate;
    }else{
		foreach($formdata["RoomSearch"] as $roomindex=>$roomdata){
			if(isset($roomdata->SearchLocation->Location)){
				$Location[$roomindex][0] = $roomdata->SearchLocation->Location;
			}else{
				foreach($roomdata->SearchLocation as $location_index=>$location_data){
					$Location[$roomindex][$location_index]["Nombre"] = $location_data->Location;
				}
			}
			$RoomSearch[$roomindex]["MaxResults"] = $roomdata->MaxResults;
			if(isset($roomdata->RoomOccupancy->adults)){
				$adults[$roomindex][0] = $roomdata->RoomOccupancy->adults;
				$children[$roomindex][0] = $roomdata->RoomOccupancy->children;
				if($children[$roomindex][0]==1){
					$ChildAge[$roomindex][0][0]=$roomdata->RoomOccupancy->ChildAge;
				}
				if($children[$roomindex][0]>1){
					foreach($roomdata->RoomOccupancy->ChildAge as $childindex => $childdata){
						$ChildAge[$roomindex][0][$childindex] = $childdata->ChildAge;
					}
				}
			}else{
				foreach($roomdata->RoomOccupancy as $roomocup_index=>$roomocu_data){
					$adults[$roomindex][$roomocup_index] = $roomocu_data->adults;
					$children[$roomindex][$roomocup_index] = $roomocu_data->children;
					if($children[$roomindex][$roomocup_index]==1){
						$ChildAge[$roomindex][$roomocup_index][0]=$roomocu_data->ChildAge;
					}
					if($children[$roomindex][$roomocup_index]>1){
						foreach($roomocu_data->ChildAge as $childindex => $childdata){
							$ChildAge[$roomindex][$roomocup_index][$childindex] = $childdata->ChildAge;
						}
					}
				}
			}
			$RoomSearch[$roomindex]["CheckinDate"] = $roomdata->CheckinDate;
			$RoomSearch[$roomindex]["CheckoutDate"] = $roomdata->CheckoutDate;
		}
	}	
	if($currency=="CLP"){
		$foreign = false;
	}elseif($currency=="USD"){
		$foreign = true;
	}else{
		return (0);
	}
	$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$res2=new stdClass();
	for($i=0;$i<count($RoomSearch);$i++){
		$habs[0]['sgl']=0;
		$habs[0]['twin']=0;
		$habs[0]['mat']=0;
		$habs[0]['tpl']=0;
		//print_r($adults);die();
		foreach($adults[$i] as $adult_index => $adult_data){
			//print_r($adult_data);die();
			if($adult_data==1){
				$habs[0]['sgl']+=1;
			}elseif($adult_data==2){
				$haydbbl=true;
				$habs[0]['twin']+=1;
			}elseif($adult_data==3){
				$habs[0]['tpl']+=1;
			}
		}
		if($haydbbl){
			$habs[1]['sgl']=$habs[0]['sgl'];
			$habs[1]['twin']=0;
			$habs[1]['mat']=$habs[0]['twin'];
			$habs[1]['tpl']=$habs[0]['tpl'];
		}
		for($o=0;$o<count($Location[$i]);$o++){
			$descompone = explode(",", $Location[$i][$o]);		
			$ciudad = $descompone[0];
			$pais = $descompone[1];
			$sql = "select c.* from ciudad c inner join pais p on c.id_pais = p.id_pais where c.ciu_nombre like '%".trim($ciudad)."%' and p.pai_nombre like '%".trim($pais)."%'";
			$get_ciudad = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$cityid = $get_ciudad->Fields("id_ciudad");
			$countryid = $get_ciudad->Fields("id_pais");
			
			for($u=0;$u<count($habs);$u++){
				$childs=null;
				//echo "$cityid, ".$RoomSearch[$i]["CheckinDate"].", ".$RoomSearch[$i]["CheckoutDate"].", $id_agencia, ".$habs[$u].",  $foreign";
				//echo "<br>";
				//die();
				$arr[$u] = $cot->matrizDisponibilidadG($db1, $cityid, $RoomSearch[$i]["CheckinDate"], $RoomSearch[$i]["CheckoutDate"], $id_agencia, $habs[$u], null,null, $foreign,$childs);
				$RoomSearchResponse=new stdClass();
				foreach($arr[$u] as $key=>$ways){
					foreach ($ways['info']['habs'] as $thab => $infohab){
						$res=new stdClass();						
						$cadena="".$key."/".$thab."/".$RoomSearch[$i]["CheckinDate"]."/".$RoomSearch[$i]["CheckoutDate"]."/".$cityid."/".$habs[$u]['sgl']."/".$habs[$u]['twin']."/".$habs[$u]['mat']."/".$habs[$u]['tpl']."/".$countryid."/".$infohab['totValue'];
						$res->BookingDescriptor = base64_encode($cadena);
						//$res->BookingDescriptor = utf8_encode(base64_decode($cadena));
						$res->HotelName=utf8_encode($ways['info']['name']);
						$res->RoomType=utf8_encode($infohab['thname']);
						$res->Adress->AddressLine= $ways['info']['AddressLine'];
						$res->Adress->City= $ways['info']['City'];
						$res->Adress->State= $ways['info']['State'];
						$res->Adress->Country= $ways['info']['Country'];
						$amenities = $ways['info']['amenities'];
						$res->Amenity = implode(',',$amenities);
						$res->Rating = $ways['info']['rating'];
						$res->Currency = $currency;
						$res->MinimumPrice = $infohab['totValue'];
						$iva = ($infohab["taxes"]>0)?1.19:1;
						
						if($habs[$u]['sgl']>0){
							for($x=0;$x<$habs[$u]['sgl'];$x++){
								$RoomOccupancy = new stdClass();								
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$RoomOccupancy->RoomSegment->RateName= "SGL";
									$RoomOccupancy->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$RoomOccupancy->RoomSegment->RoomPrice->Currency = $currency;
									$RoomOccupancy->RoomSegment->RoomPrice->PassThrough = false;
									$RoomOccupancy->RoomSegment->RoomPrice->Total =round($infoday['sgl']/$infoday['markup']['sgl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->RoomPrice->TaxTotal =round($infoday['sgl']/$infoday['markup']['sgl']*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotal[]=round($infoday['sgl']/$infoday['markup']['sgl']*$iva,$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotalNoTax[]=round($infoday['sgl']/$infoday['markup']['sgl'],$cot->decimales);
								}
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "";
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->Currency = $currency;
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->Currency = $currency;
								$res->RoomOccupancy[] = $RoomOccupancy;
							}
						}
						if($habs[$u]['twin']>0){
							for($x=0;$x<$habs[$u]['twin'];$x++){
								$RoomOccupancy = new stdClass();								
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$RoomOccupancy->RoomSegment->RateName= "TWIN";
									$RoomOccupancy->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$RoomOccupancy->RoomSegment->RoomPrice->Currency = $currency;
									$RoomOccupancy->RoomSegment->RoomPrice->PassThrough = false;
									$RoomOccupancy->RoomSegment->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup']['twin']*$iva*count($ways['habs'][$thab]),$cot->decimales)."/".$infoday['hotdet_cli'];
									//$RoomOccupancy->RoomSegment->RoomPrice->Total =$infoday['hotdet_cli'];
									$RoomOccupancy->RoomSegment->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup']['twin']*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotal[]=round($infoday['dbl']*2/$infoday['markup']['twin']*$iva,$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotalNoTax[]=round($infoday['dbl']*2/$infoday['markup']['twin'],$cot->decimales);
								}
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "";
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->Currency = $currency;
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->Currency = $currency;
								$res->RoomOccupancy[] = $RoomOccupancy;
							}
						}
						if($habs[$u]['mat']>0){
							for($x=0;$x<$habs[$u]['mat'];$x++){
								$RoomOccupancy = new stdClass();								
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$RoomOccupancy->RoomSegment->RateName= "MATRIMONIAL";
									$RoomOccupancy->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$RoomOccupancy->RoomSegment->RoomPrice->Currency = $currency;
									$RoomOccupancy->RoomSegment->RoomPrice->PassThrough = false;
									$RoomOccupancy->RoomSegment->RoomPrice->Total =round($infoday['dbl']*2/$infoday['markup']['mat']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->RoomPrice->TaxTotal =round($infoday['dbl']*2/$infoday['markup']['mat']*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotal[]=round($infoday['dbl']*2/$infoday['markup']['mat']*$iva,$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotalNoTax[]=round($infoday['dbl']*2/$infoday['markup']['mat'],$cot->decimales);
								}
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "";
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->Currency = $currency;
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->Currency = $currency;
								$res->RoomOccupancy[] = $RoomOccupancy;
							}
						}
						if($habs[$u]['tpl']>0){
							for($x=0;$x<$habs[$u]['tpl'];$x++){
								$RoomOccupancy = new stdClass();								
								foreach($ways['habs'][$thab] as $fecha => $infoday) {
									$RoomOccupancy->RoomSegment->RateName= "TPL";
									$RoomOccupancy->RoomSegment->RoomPrice->IncludesTaxesAndExtraCharges = true;
									$RoomOccupancy->RoomSegment->RoomPrice->Currency = $currency;
									$RoomOccupancy->RoomSegment->RoomPrice->PassThrough = false;
									$RoomOccupancy->RoomSegment->RoomPrice->Total =round($infoday['tpl']*3/$infoday['markup']['tpl']*$iva*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->RoomPrice->TaxTotal =round($infoday['tpl']*3/$infoday['markup']['tpl']*count($ways['habs'][$thab]),$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotal[]=round($infoday['tpl']*3/$infoday['markup']['tpl']*$iva,$cot->decimales);
									$RoomOccupancy->RoomSegment->DailyTotalNoTax[]=round($infoday['tpl']*3/$infoday['markup']['tpl'],$cot->decimales);
								}
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->Description = "";
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->DeadlineTimestamp = $ways['info']['deadline'];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->FlatFee->Currency = $currency;
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->_ = $infohab["penalty"];
								$RoomOccupancy->RoomSegment->RoomCancellationPolicies->RoomCancellationPolicy->CalculatedFeeAmount->Currency = $currency;
								$res->RoomOccupancy[] = $RoomOccupancy;
							}
						}
						
					}
					$RoomSearchResponse->Hotel[] = $res;
					$res2->RoomSearchResponse[]=$RoomSearchResponse;
					
					//print_r($res);die();
					//$lista[]=$res2;
				}
			}
		}		
	}
	//print_r($res2);die();
	return $res2;
}
function CreatePnr($formdata){
    $id_webservice=13;
    require_once('/var/www/otas/Connections/db1.php');
    require_once('/var/www/otas/clases/cotizacion.php');
    $cot = new Cotizacion();
    $formdata = get_object_vars($formdata);
    $user = $formdata["UserId"];
    $password = $formdata["Password"];
    $currency = $formdata["Currency"];
	if(isset($formdata["RoomSegmentRequest"]->RoomSegment)){
		$extras[0]['clientcode'] = $formdata["RoomSegmentRequest"]->CorrelationId;
		$RoomSegmentRequest[0] = base64_decode($formdata["RoomSegmentRequest"]->RoomSegment);
		$aux[0] = explode("/", $RoomSegmentRequest[0]);
		if(isset($formdata["RoomSegmentRequest"]->RoomingList->Guest)){
			//cuando es un romminglist en un roomsegment
			if(isset($formdata["RoomSegmentRequest"]->RoomingList->Guest->FirstName)){
				//cuando es un pasajero en un rominglist
				$paxs[0][0][0]["name"] = $formdata["RoomSegmentRequest"]->RoomingList->Guest->FirstName;
				$paxs[0][0][0]["lastname"] = $formdata["RoomSegmentRequest"]->RoomingList->Guest->LastName;
				$paxs[0][0][0]["passport"] = $formdata["RoomSegmentRequest"]->RoomingList->Guest->OfficialTravelerId;
				$paxs[0][0][0]["countryid"] = $aux[0][9];
			}else{
				foreach($formdata["RoomSegmentRequest"]->RoomingList->Guest as $paxindex => $paxdata){
					//cuando es mas de un pasajero en un roominglist
					$paxs[0][0][$paxindex]["name"] = $paxdata->FirstName;
					$paxs[0][0][$paxindex]["lastname"] = $paxdata->LastName;
					$paxs[0][0][$paxindex]["passport"] = $paxdata->OfficialTravelerId;
					$paxs[0][0][$paxindex]["countryid"] = $aux[0][9];
				}
			}				
		}else{
			//cuando es mas de un roomingList en un roomsegment
			foreach($formdata["RoomSegmentRequest"]->RoomingList as $roomlindex => $roomldata){
				if(isset($roomldata->Guest->FirstName)){
					//cuando es un pasajero en mas de un roomminglist
					$paxs[0][$roomlindex][0]["name"] = $roomldata->Guest->FirstName;
					$paxs[0][$roomlindex][0]["lastname"] = $roomldata->Guest->LastName;
					$paxs[0][$roomlindex][0]["passport"] = $roomldata->Guest->OfficialTravelerId;
					$paxs[0][$roomlindex][0]["countryid"] = $aux[0][9];				
				}else{
					foreach($roomldata->Guest as $paxindex => $paxdata){
						//cuando es mas de un pasajero en mas de un roomingList
						$paxs[0][$roomlindex][$paxindex]["name"] = $paxdata->FirstName;
						$paxs[0][$roomlindex][$paxindex]["lastname"] = $paxdata->LastName;
						$paxs[0][$roomlindex][$paxindex]["passport"] = $paxdata->OfficialTravelerId;
						$paxs[0][$roomlindex][$paxindex]["countryid"] = $aux[0][9];	
					}
				}
			}
		}
	}else{
		//cuando es mas de un roomsegment
		foreach($formdata["RoomSegmentRequest"] as $roomsindex => $roomsdata){
			$extras[$roomsindex]['clientcode'] = $roomsdata->CorrelationId;
			$RoomSegmentRequest[$roomsindex] = base64_decode($roomsdata->RoomSegment);
			$aux[$roomsindex] = explode("/", $RoomSegmentRequest[$roomsindex]);
			if(isset($roomsdata->RoomingList->Guest)){
				//cuando es un romminglist en mas de un roomsegment
				if(isset($roomsdata->RoomingList->Guest->FirstName)){
					//cuando es un pasajero en un rominglist
					$paxs[$roomsindex][0][0]["name"] = $roomsdata->RoomingList->Guest->FirstName;
					$paxs[$roomsindex][0][0]["lastname"] = $roomsdata->RoomingList->Guest->LastName;
					$paxs[$roomsindex][0][0]["passport"] = $roomsdata->RoomingList->Guest->OfficialTravelerId;
					$paxs[$roomsindex][0][0]["countryid"] = $aux[$roomsindex][9];
				}else{
					foreach($roomsdata->RoomingList->Guest as $paxindex => $paxdata){
						//cuando es mas de un pasajero en un roominglist
						$paxs[$roomsindex][0][$paxindex]["name"] = $paxdata->FirstName;
						$paxs[$roomsindex][0][$paxindex]["lastname"] = $paxdata->LastName;
						$paxs[$roomsindex][0][$paxindex]["passport"] = $paxdata->OfficialTravelerId;
						$paxs[$roomsindex][0][$paxindex]["countryid"] = $aux[$roomsindex][9];
					}
				}				
			}else{
				//cuando es mas de un roomingList en mas de un roomsegment
				foreach($roomsdata->RoomingList as $roomlindex => $roomldata){
					if(isset($roomldata->Guest->FirstName)){
						//cuando es un pasajero en mas de un roomminglist
						$paxs[$roomsindex][$roomlindex][0]["name"] = $roomldata->Guest->FirstName;
						$paxs[$roomsindex][$roomlindex][0]["lastname"] = $roomldata->Guest->LastName;
						$paxs[$roomsindex][$roomlindex][0]["passport"] = $roomldata->Guest->OfficialTravelerId;
						$paxs[$roomsindex][$roomlindex][0]["countryid"] = $aux[$roomsindex][9];				
					}else{
						foreach($roomldata->Guest as $paxindex => $paxdata){
							//cuando es mas de un pasajero en mas de un roomingList
							$paxs[$roomsindex][$roomlindex][$paxindex]["name"] = $paxdata->FirstName;
							$paxs[$roomsindex][$roomlindex][$paxindex]["lastname"] = $paxdata->LastName;
							$paxs[$roomsindex][$roomlindex][$paxindex]["passport"] = $paxdata->OfficialTravelerId;
							$paxs[$roomsindex][$roomlindex][$paxindex]["countryid"] = $aux[$roomsindex][9];	
						}
					}
				}
			}
		}
	}
	$lista = new stdClass();
	for($i=0;$i<count($RoomSegmentRequest);$i++){
		$foreign = ($paxs[$i][0][0]["countryid"]==4)?"FALSE":"TRUE";
		if(strtoupper($foreign)=="FALSE" || $foreign=='0'){
			$currency_aux="CLP";
			$foreign=false;
		}else{
			$currency_aux="USD";
			$foreign=true;
		}
		$Pnr=new stdClass();
		$usuario_rq  ="select * from usuarios where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
		$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($usuario_rs->RecordCount()==0){
			$Failure->type = "error";
			$Failure->code = "INTERNAL_ERROR";
			$Failure->translation = "translate";
			$Failure->_ = "Invalid user";
			$Pnr->Failure = $Failure;
			return $Pnr;
		}
		$id_agencia = $usuario_rs->Fields('id_agencia');
		$id_usuario = $usuario_rs->Fields('id_usuario');
		$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
		$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if($agws_rs->RecordCount()==0){
			$Failure->type = "error";
			$Failure->code = "INTERNAL_ERROR";
			$Failure->translation = "translate";
			$Failure->_ = "Not allowed user";
			$Pnr->Failure = $Failure;
			return $Pnr;
		}
		if($currency_aux!=$currency){
			$Failure->type = "error";
			$Failure->code = "INTERNAL_ERROR";
			$Failure->translation = "translate";
			$Failure->_ = "The currency doesn't match with location";
			$Pnr->Failure = $Failure;
			return $Pnr;
		}
		$pais_rq  ="select * from pais where id_pais = ".$paxs[$i][0][0]["countryid"];
		$pais_rs = $db1->Execute($pais_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		
		if($pais_rs->RecordCount()==0){
			$Failure->type = "error";
			$Failure->code = "INTERNAL_ERROR";
			$Failure->translation = "translate";
			$Failure->_ = "First pax country is mandatory";
			$Pnr->Failure = $Failure;
			return $Pnr;
		}
		$aux = explode("/", $RoomSegmentRequest[$i]);
		$hotelid = $aux[0];
		$roomid = $aux[1];		
		$checkin = $aux[2];
		$checkout = $aux[3];
		$cityid = $aux[4];
		$habs['sgl']=$aux[5];
		$habs['twin']=$aux[6];
		$habs['mat']=$aux[7];
		$habs['tpl']=$aux[8];
		$total=$aux[10];
		//echo $cityid."/".$checkin."/".$checkout."/".$id_agencia."/".$habs."/".$hotelid."/".$roomid."/".$foreign;
		//print_r($habs);
		//die();
		//$cadena="".$hotel."/".$thab."/".CheckinDate/"CheckoutDate"/".$cityid."/".['sgl']."/".['twin']."/".['mat']."/".['tpl']."/".$countryid."/".['totValue'];
		$arr = $cot->matrizDisponibilidadG($db1, $cityid, $checkin, $checkout, $id_agencia, $habs, $hotelid, $roomid, $foreign);
		if(round($arr[$hotelid]['info']['habs'][$roomid]['totValue'], $cot->decimales)!=round($total, $cot->decimales)){
			$Failure->type = "error";
			$Failure->code = "INTERNAL_ERROR";
			$Failure->translation = "translate";
			$Failure->_ = "There's no stock in the requested room";
			$Pnr->Failure = $Failure;
			return $Pnr;
		}
		$result = $cot->cotCreator($db1, $arr, $habs, $paxs[$i][0],$id_usuario,$cityid,$extras);
		$now = getdate();
		$Pnr->RecordLocator=$result['bookingcode'];
		$Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$Pnr->Status=$result['status'];
		$SegmentTotals = new stdClass();
		$SegmentTotals->AirTotal = 0;
		$SegmentTotals->AirTaxTotal = 0;
		$SegmentTotals->RoomTotal = $total;
		$SegmentTotals->RoomTaxTotal = $total*0.19; /// si ya viene con iva deberia ser $total/1.19*0.19
		$SegmentTotals->CarTotal = 0;
		$SegmentTotals->CarTaxTotal = 0;
		$SegmentTotals->ActivityTotal = 0;
		$SegmentTotals->ActivityTaxTotal = 0;
		$SegmentTotals->CruiseTotal = 0;
		$SegmentTotals->CruiseTaxTotal = 0;
		$SegmentTotals->InsuranceTotal = 0;
		$SegmentTotals->InsuranceTaxTotal = 0;
		$SegmentTotals->MerchandiseTotal = 0;
		$SegmentTotals->MerchandiseTaxTotal = 0;
		$SegmentTotals->PassThroughMarkupTotal = 0;
		$SegmentTotals->BookingFeeClient = 0;
		$SegmentTotals->BookingFeeTotal = 0;
		$SegmentTotals->ServiceChargeTotal = 0;
		$SegmentTotals->AgentMarkup = 0;
		$SegmentTotals->DiscountTotal = 0;
		$Pnr->SegmentTotals[] = $SegmentTotals;
		$Pnr->DepartureDate = "";
		$Pnr->PnrOwner = $formdata["PnrOwner"];
		$Pnr->LeadTraveler = $formdata["LeadTraveler"];
		$CustomerPricingSummary = new stdClass();
		$CustomerPricingSummary->Currency = $currency  ; 
		$CustomerPricingSummary->PackageTotal =  $total ; 
		$CustomerPricingSummary->PaymentTotal = $total  ; 
		$CustomerPricingSummary->BalanceDueTotal =  0 ; 
		$Pnr->CustomerPricingSummary[] = $CustomerPricingSummary;
		$Segmentaux = new stdClass();
		$Segmentaux->SegmentLocator = $result['bookingcode'];
		$Segmentaux->CorrelationId = $extras[$i]['clientcode'];
		$Segmentaux->Currency = $currency;
		$Segmentaux->TotalPrice= $total;
		$lista->Success  ="Success";
		//$lista->Segment[] = $Segmentaux;
		$lista->_->_->Segment = "";
		$Segmentarr[]=$Segmentaux;
		//$lista->Pnr[]=$Pnr;
	}
	//print_r($lista);die();
	return $lista;
}
function CancelPnr($formdata){
    $res = new stdClass();
	$Failure = new stdClass();
	$id_webservice=13;
	require_once('/var/www/otas/Connections/db1.php');
	require_once('/var/www/otas/clases/cotizacion.php');
	$cot = new Cotizacion();
	$formdata = get_object_vars($formdata);
	$user = $formdata["UserId"];
	$password = $formdata["Password"];
	$bookingcode = $formdata["RecordLocator"];
	$usuario_rq  ="select u.*, a.ag_espanula from usuarios u join agencia a 
		on u.id_agencia = a.id_agencia where usu_login like '$user' and usu_password like '$password' and usu_estado = 0";
	$usuario_rs = $db1->Execute($usuario_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$res= new stdClass();
	if($usuario_rs->RecordCount()==0){
		$Failure->type = "error";
		$Failure->code = "INTERNAL_ERROR";
		$Failure->translation = "translate";
		$Failure->_ = "Invalid user";
		$res->Failure = $Failure;
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['description'];
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
		return $res;
	}
	$id_agencia = $usuario_rs->Fields('id_agencia');
	$id_usuario = $usuario_rs->Fields('id_usuario');
	$agws_rq  ="select * from agencia_webservice where id_agencia = $id_agencia and id_webservice = $id_webservice and agws_estado = 0";
	$agws_rs = $db1->Execute($agws_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($agws_rs->RecordCount()==0){
		$Failure->type = "error";
		$Failure->code = "INTERNAL_ERROR";
		$Failure->translation = "translate";
		$Failure->_ = "Not allowed user";
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['description'];
		$SegmentTotals = new stdClass();
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
		$res->Failure = $Failure;
		return $res;
	}
	$cot_rq  ="select * from cot where id_cot = $bookingcode and id_seg = 7 and cot_estado = 0";
	$cot_rs = $db1->Execute($cot_rq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($cot_rs->RecordCount()==0){
		$Failure->type = "error";
		$Failure->code = "INTERNAL_ERROR";
		$Failure->translation = "translate";
		$Failure->_ = "Booking must be confirmed";
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['description'];
		$SegmentTotals = new stdClass();
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
		$res->Failure = $Failure;
		return $res;
	}
	$sql= "SELECT TIME_TO_SEC(TIMEDIFF(NOW(), ha_hotanula)) AS diff FROM cot WHERE id_cot = $bookingcode";
	$rs= $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($rs->Fields("diff")>=0 && $usuario_rs->Fields("ag_espanula")==1){
		$Failure->type = "error";
		$Failure->code = "INTERNAL_ERROR";
		$Failure->translation = "translate";
		$Failure->_ = "User can not cancel out of deadline";
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['description'];
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
		$res->Failure = $Failure;
		return $res;
	}
	$result = $cot->cotAnulator($db1, $bookingcode,$id_usuario);
	if($result['penalty']!=0 || $result['status']!='SUCCESS'){
		$Failure->type= "error";
		$Failure->code="INTERNAL_ERROR";
		$Failure->translation = "translate";
		$Failure->_ = $result['description'];
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['description'];
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
		$res->Failure = $Failure;
	}else{
		
		$res->Success = $result['status'];
		$res->Pnr->RecordLocator = $formdata["RecordLocator"];
		$now = getdate();
		$res->Pnr->BookingTimestamp = $now['year']."-".$now['mon']."-".$now['mday'].":".$now['hours'].":".$now['minutes'].":".$now['seconds'];
		$res->Pnr->Status =$result['status'];
		$res->Pnr->SegmentTotals->AirTotal = 0;
		$res->Pnr->SegmentTotals->AirTaxTotal = 0;
		$res->Pnr->SegmentTotals->RoomTotal = 0;
		$res->Pnr->SegmentTotals->RoomTaxTotal = 0; /// si ya viene con iva deberia ser $total/1.19*0.19
		$res->Pnr->SegmentTotals->CarTotal = 0;
		$res->Pnr->SegmentTotals->CarTaxTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTotal = 0;
		$res->Pnr->SegmentTotals->ActivityTaxTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTotal = 0;
		$res->Pnr->SegmentTotals->CruiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTotal = 0;
		$res->Pnr->SegmentTotals->InsuranceTaxTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTotal = 0;
		$res->Pnr->SegmentTotals->MerchandiseTaxTotal = 0;
		$res->Pnr->SegmentTotals->PassThroughMarkupTotal = 0;
		$res->Pnr->SegmentTotals->BookingFeeClient = 0;
		$res->Pnr->SegmentTotals->BookingFeeTotal = 0;
		$res->Pnr->SegmentTotals->ServiceChargeTotal = 0;
		$res->Pnr->SegmentTotals->AgentMarkup = 0;
		$res->Pnr->SegmentTotals->AgentMarkupCommission = 0;
		$res->Pnr->SegmentTotals->DiscountTotal = 0;
		$res->Pnr->SegmentTotals = "";
		$res->Pnr->DepartureDate = "";
		$res->Pnr->PnrOwner  = "";
		$res->Pnr->LeadTraveler  = "";
		$res->Pnr->SystemPricingSummary  = "";
		$res->Pnr->CustomerPricingSummary = "";
	}
	return $res;
}
?>