<?php

require_once('clases/usuario.php');
require_once('Connections/db1.php');
$usuario = $_SESSION['usuario'];

$distantis = new distantis();

require_once('includes/functions.inc.php');

$permiso=209;
require('secure.php');



$distantis->anular_admin($db1,$_GET['id_cot']);
//Connection statemente
$observa = "";
$resumen = "Resumen Destino ";
$val_total = 0;
$val_total_habmark = 0;
$query_cot = "SELECT 	*,c.id_area as area_d,
				
				
				if(cot_estado = 1,'- ANULADO','&nbsp;') as anulado,
				DATE_FORMAT(c.cot_fec, '%d-%m-%Y %h:%m:%i') as cot_fec,
				DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
				DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
				DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y') as ha_hotanula1,
				o.ag_nombre as op1,
				h.ag_nombre as op2,
				c.id_opcts as id_opcts
			  FROM		cot c
				INNER JOIN	usuarios u ON c.id_usuario = u.id_usuario
				LEFT JOIN	tipopack i ON c.id_tipopack = i.id_tipopack
				INNER JOIN	seg s ON s.id_seg = c.id_seg
				INNER JOIN	agencia o ON o.id_agencia = c.id_agencia
				LEFT JOIN	agencia h ON h.id_agencia = c.id_opcts
				
				
				
			  WHERE	c.id_cot = ".$_GET['id_cot'];
//echo $query_cot."<hr>";
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_logeo = "SELECT
	log.id_accion,
	usuarios.usu_login,
	agencia.ag_nombre,
	DATE_FORMAT(log.fechaaccion, '%d-%m-%Y %H:%i:%s') as fechaaccion1,
	permisos.per_descripcion
	FROM
	log
	INNER JOIN usuarios ON usuarios.id_usuario = log.id_user
	INNER JOIN permisos ON log.id_accion = permisos.per_codigo
	INNER JOIN agencia ON agencia.id_agencia = usuarios.id_agencia
	WHERE
	log.id_cambio = ".$_GET['id_cot']." ORDER BY log.fechaaccion ASC";
$logeo = $db1->SelectLimit($query_logeo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_destinos = "SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			i.id_ciudad as id_ciudad,
			IF(c.cd_estado = 1,'- ANULADO','&nbsp;') AS anulado
	FROM cotdes c 
	INNER JOIN ".$distantis->dbhot.".tipohabitacion th
	ON c.`id_tipohabitacion` = th.`id_tipohabitacion`
	LEFT JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad
	INNER JOIN seg s ON s.id_seg = c.id_seg
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0";
//echo $query_destinos."<hr>";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");





?>
<html>
	<head>
		<title><?=$distantis->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="css/test.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-3.2.1.min.js"></script>
<script>
function modCot(){
	if(document.getElementById('modificarCot').style.display=='none'){
		document.getElementById('modificarCot').style.display="inline";	
	}else{
		document.getElementById('modificarCot').style.display="none";
	}

}

function submit_OR(){
	var texto=document.getElementById("ap_onreq_just").value.replace(/(\n\n|\r|\n)/g, "<br />");
	document.getElementById("text_justificacion").value=escape(texto);
	document.forms['form_ap_onrequest'].submit();
}

function Anula(tipo,conciliado,facturado){
	var alerta = '';
	var desea = ' Desea Anular?';
	if(conciliado == 1 || facturado == 1){
	
		if(conciliado == 1){
			alerta = alerta+'--Esta Reserva Posee Pago Involucrado\n';
		}
		if(facturado == 1){
			alerta = alerta+'---Esta Reserva Tiene Factura Asociada\n';
		}
		
		if(confirm(alerta+desea)){
			if(tipo == 1){
				window.location='cot_fenix.php?id_cot=<?=$_GET['id_cot'];?>';
			}
			if(tipo == 2){
				window.location='cot_anula.php?id_cot=<?=$_GET['id_cot'];?>';
			}
			if(tipo == 3){
				<?php
				if($cot->Fields('id_tipopack') == 1){$dir_anula="cot_anula_pack";}
				if($cot->Fields('id_tipopack') == 2 or $cot->Fields('id_tipopack') == 5){$dir_anula="cot_anula_dest";}
				if($cot->Fields('id_tipopack') == 3){$dir_anula="cot_anula_hotel";}
				if($cot->Fields('id_tipopack') == 4){$dir_anula="cot_anula_trans";}
				?>
				window.location='<?=$dir_anula;?>.php?id_cot=<?=$_GET['id_cot'];?>';
			}
			if(tipo ==4){
				window.location='cot_anula_sop.php?id_cot=<?=$_GET['id_cot'];?>';
			}
		}
	}else{
		if(tipo == 1){
			window.location='cot_fenix.php?id_cot=<?=$_GET['id_cot'];?>';
		}
		if(tipo == 2){
			window.location='cot_anula.php?id_cot=<?=$_GET['id_cot'];?>';
		}
		if(tipo == 3){
			<?php
				if($cot->Fields('id_tipopack') == 1){$dir_anula="cot_anula_pack";}
				if($cot->Fields('id_tipopack') == 2 or $cot->Fields('id_tipopack') == 5){$dir_anula="cot_anula_dest";}
				if($cot->Fields('id_tipopack') == 3){$dir_anula="cot_anula_hotel";}
				if($cot->Fields('id_tipopack') == 4){$dir_anula="cot_anula_trans";}
			?>
			window.location='<?=$dir_anula;?>.php?id_cot=<?=$_GET['id_cot'];?>';
		}
		if(tipo ==4){
			window.location='cot_anula_sop.php?id_cot=<?=$_GET['id_cot'];?>';
		}
	}

}

</script>
<body OnLoad="document.form.txt_obs.focus();">
<div align="center" style="font-family:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#0000A4;"><h4><?=$cot->Fields('tp_nombre')?> - N�<? echo $_GET['id_cot'];?> / FF <? echo $cot->Fields('cot_numfile');?></h4>
	<!-- ESTADO GENERAL COT FMEJIAS 04/04/2012  -->
	<h4>ESTADO GENERAL COTIZACI&Oacute;N :</h4><h3> <? echo $cot->Fields('seg_nombre');?> <? echo $cot->Fields('anulado');?></h3></br>
	<?php if($cot->Fields('cot_conciliado') == 1){ echo "<h3><font color='red'>Reserva Concilada</font></h3></br> "; }?>
	<?php if($cot->Fields('cot_facturado') == 1){ echo "<h3><font color='red'>Factura emitida al cliente</font></h3></br> "; }?>
	</div>
<center>

	

    
	<button name="anula2" type="button" onClick="javascript:Anula(3,<?=$cot->Fields('cot_conciliado'); ?>,<?=$cot->Fields('cot_facturado'); ?>);" style="width:125px; height:27px">Anula Con Correo</button>

	
    <button name="volver" type="button" onClick="window.location='cot_search.php';" style="width:100px; height:27px">Volver</button>
	</center>



<br>
<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
  <tbody>
    <tr valign="baseline">
      <td width="88" align="left" bgcolor="#D5D5FF">Creador :</td>
      <td width="358" class="tdedita"><? echo $cot->Fields('usu_login')." (".$cot->Fields('op1').")";?></td>
      <td width="101" bgcolor="#D5D5FF">Fecha Creacion :</td>
      <td width="227" class="tdedita"><? echo $cot->Fields('cot_fec');?></td>
      <td width="146" align="left" bgcolor="#D5D5FF">Fecha Anulacion Sin Costo:</td>
      <td width="152" class="tdedita"><?= $cot->Fields('ha_hotanula1') ?></td>
    </tr>
    <tr valign="baseline">
      <td bgcolor="#D5D5FF">Operador :</td>
      <td class="tdedita"><? echo $cot->Fields('op2')." (ID:".$cot->Fields('id_opcts').")";?></td>
      <td bgcolor="#D5D5FF">N&deg; Correlativo :</td>
      <td class="tdedita"><? echo $cot->Fields('cot_correlativo');?></td>
      <td bgcolor="#D5D5FF">Valor Total Reserva:</td>
      <td class="tdedita"><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <? echo $cot->Fields('cot_valor');?></td>
    </tr>
    <tr valign="baseline">
      <td valign="top" bgcolor="#D5D5FF"><? echo $observa;?> :</td>
      <td colspan="5" class="tdedita"><? echo $cot->Fields('cot_obs');?></td>
    </tr>
  </tbody>
</table>



<ul><li>Resumen Cotizaci&oacute;n <?=$cot->Fields('nom_pac');?></li></ul>
<? 

if($cot->Fields('id_tipopack') == 1 or $cot->Fields('id_tipopack') == 3){

	$d=1;
	  while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		  
	
?>
              <table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <th colspan="8"><? echo $resumen;?> (<?=$d;?>) <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="66" align="left" bgcolor="#D5D5FF">Hotel:</td>
                    <td width="300" class="tdedita"><? echo $destinos->Fields('hot_nombre')." (ID:".$destinos->Fields('id_hotel').")";?></td>
                    <td width="101" align="left" bgcolor="#D5D5FF">tipo Hotel :</td>
                    <td width="122" class="tdedita"><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td width="91" bgcolor="#D5D5FF">Valor Destino :</td>
                    
					<td width="99" class="tdedita"><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <? echo $destinos->Fields('cd_valor');?></td>
                    
					<td width="104" bgcolor="#D5D5FF">Estado :</td>
                    <td width="181" class="tdedita"><? echo $destinos->Fields('seg_nombre');?> <? echo $destinos->Fields('anulado');?></td>
				
				  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF">N&deg; Reserva :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_numreserva');?></td>
                    <td bgcolor="#D5D5FF">Sector Hotel :</td>
                    <td class="tdedita"><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                    <td align="left" bgcolor="#D5D5FF">Fecha desde :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td bgcolor="#D5D5FF">Fecha Hasta :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"  bgcolor="#D5D5FF">Single</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab1');?></td>
                    <td bgcolor="#D5D5FF">Doble Twin :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab2');?></td>
                    <td bgcolor="#D5D5FF">Doble Matrimonial :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab3');?></td>
                    <td bgcolor="#D5D5FF">Triple :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab4');?></td>
                  </tr>
				   <tr valign="baseline">
                    <td align="left"  bgcolor="#D5D5FF">Tipo Habitacion </td>
                    <td class="tdedita"><? echo $destinos->Fields('th_nombre');?></td>
                  </tr>
                </tbody>
              </table>
              <br />
              <table align="center" width="90%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tr>
                  <th colspan="2" align="center" bgcolor="#D5D5FF">Detalle del Pasajero</th>
                </tr>
                <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes');
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="11%" align="left" bgcolor="#D5D5FF">&nbsp;Pasajero  <? echo $z;?>.</td>
                  <td width="89%" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('pai_numvuelo');?></td>
                </tr>
                <? 
		
		
	$total_pas=0;$total_pas2=0;
		
          	$z++;
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst(); ?>
              </table><br />
                <? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
				$d++;
                }
            //}
}


if($cot->Fields('id_tipopack') == 2 or $cot->Fields('id_tipopack') == 5){
	if($totalRows_destinos > 0){
	$d=1;
	  while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		  
	
?>
              <table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <th colspan="8"><? echo $resumen;?> (<?=$d;?>) <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="66" align="left" bgcolor="#D5D5FF"><? echo $hotel_nom;?> :</td>
                    <td width="300" class="tdedita"><? echo $destinos->Fields('hot_nombre')." (ID:".$destinos->Fields('id_hotel').")";?></td>
                    <td width="101" align="left" bgcolor="#D5D5FF"><? echo $tipohotel;?> :</td>
                    <td width="122" class="tdedita"><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td width="91" bgcolor="#D5D5FF"><? echo $valdes;?> :</td>
                    <td width="99" class="tdedita"><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <? echo $destinos->Fields('cd_valor');?></td>
                   
					<td width="104" bgcolor="#D5D5FF">Estado :</td>
                    <td width="181" class="tdedita"><? echo $destinos->Fields('seg_nombre');?> <? echo $destinos->Fields('anulado');?></td>
					
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF">N&deg; Reserva :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_numreserva');?></td>
                    <td bgcolor="#D5D5FF"><? echo $sector;?> :</td>
                    <td class="tdedita"><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                    <td align="left" bgcolor="#D5D5FF"><? echo $fecha1;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td bgcolor="#D5D5FF"><? echo $fecha2;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"  bgcolor="#D5D5FF"><? echo $sin;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab1');?></td>
                    <td bgcolor="#D5D5FF"><? echo $dob;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab2');?></td>
                    <td bgcolor="#D5D5FF"><? echo $tri;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab3');?></td>
                    <td bgcolor="#D5D5FF"><? echo $cua;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab4');?></td>
                  </tr>
                </tbody>
              </table>
              <br />
                <? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
				$d++;
                }
            //}
?>
              <table align="center" width="90%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tr>
                  <th colspan="2" align="center" bgcolor="#D5D5FF"><? echo $detpas;?></th>
                </tr>
                <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="11%" align="left" bgcolor="#D5D5FF">&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="89%" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('pai_numvuelo');?></td>
                </tr>
<?
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst(); 
	?>                
              </table>


<?
}
?>
<br>
<table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
          <tr>
            <th colspan="2"><? echo $servinc;?></th>
          </tr>
          <tr>
            <th width="98">N&ordm;</th>
            <th width="704"><? echo $nomserv;?></th>
          </tr>
            <?php	 	
$cc = 1;
  while (!$cot->EOF) {
	  if($cot->Fields('pd_terrestre') == 1){
?>
          <tr>
            <td bgcolor="#D5D5FF"><center>
              <?php	 	 echo $cc; ?>&nbsp;
            </center></td>
            <td align="center"><?php	 	 echo $cot->Fields('pd_nombre'); ?>&nbsp;</td>
          </tr>
          <?php	 	 $cc++;
	  }
	$cot->MoveNext(); 
	}$cot->MoveFirst(); 
	
?>
</table>    
<? }
if($cot->Fields('id_tipopack') == 4){?>
        <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
		
  	while (!$pasajeros->EOF) {?>
<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <tr>
      <td>
    <table align="center" width="100%">
            <tr valign="baseline">
              <td width="98" align="left" bgcolor="#D5D5FF"><? echo $nombre;?> Pax N&deg;<? echo $z;?>:</td>
              <td width="690" class="tdedita"><? echo $pasajeros->Fields('cp_nombres');?> <? echo $pasajeros->Fields('cp_apellidos');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?></td>
            </tr>
        </table>
           </td></tr></table>         
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}
  }
  echo "<font size='1'>* Los Filas en rojo no se suman para el valor final.</font>";


if($cot->Fields('id_tipopack') != 4){

$query_valhab = "
	SELECT 
		o.id_hotocu, o.hc_hab1, o.hc_hab2, o.hc_hab3, o.hc_hab4, h.hd_sgl, h.hd_dbl, h.hd_tpl,
		if(o.hc_hab1 > 0,(o.hc_hab1*h.hd_sgl),0) as hab1, 
		if(o.hc_hab2 > 0,(o.hc_hab2*h.hd_dbl*2),0) as hab2, 
		if(o.hc_hab3 > 0,(o.hc_hab3*h.hd_dbl*2),0) as hab3, 
		h.hd_dbl*2 as hab33,
		if(o.hc_hab4 > 0,(o.hc_hab4*h.hd_tpl*3),0) as hab4,
		o.hc_valor1,o.hc_valor2,o.hc_valor3,o.hc_valor4,
		DATE_FORMAT(o.hc_fecha, '%d-%m-%Y') as hc_fecha, t.tt_nombre, h.id_hotdet, o.hc_estado
	FROM hotocu o
	INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
	INNER JOIN ".$distantis->dbhot.".tipotarifa t ON h.idpk_tipotarifa = t.id_tipotarifa
	WHERE o.id_cot =".$_GET['id_cot']." ORDER BY o.id_hotocu, o.hc_fecha";
	//echo $query_valhab;
$valhab = $db1->SelectLimit($query_valhab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<ul><li>Valores x Habitaci&oacute;n reservadas.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="3%" rowspan="2">N&ordm;</th>
    <th width="5%" rowspan="2">ID</th>
    <th width="14%" rowspan="2">Dia</th>
    <th width="25%" rowspan="2">Tipo Tarifa (ID)</th>
    <th colspan="4">Valor x Habitaci&oacute;n</th>
    <th width="13%" rowspan="2">Total</th>
    <th width="13%" rowspan="2">Total/Mark/Com</th>
  <tr>
    <th width="8%">SIN</th>
    <th width="8%">TWIN</th>
    <th width="8%">MATRI</th>
    <th width="8%">TPL</th>
<? 
$c = 1; $mark = 0; $total_procom = 0; $habmark = 0;
  while (!$valhab->EOF) {

	  $valxhabxdia = ($valhab->Fields('hab1')+$valhab->Fields('hab2')+$valhab->Fields('hab3')+$valhab->Fields('hab4'));
?>
  <tr title='N&deg;<?php echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'"  onMouseOut="style.background='<? if($valhab->Fields('hc_estado')==1){echo "#FF0000";} ?>', style.color='#000'" <? if($valhab->Fields('hc_estado')==1){echo "bgcolor='#FF0000'";} ?>>
    <td background="images/th.jpg"><center>
      <?php echo $c; ?>&nbsp;
    </center></td>
    <td align="center"><?php echo $valhab->Fields('id_hotocu'); ?></td>
    <td align="center"><?php echo $valhab->Fields('hc_fecha'); ?>&nbsp;</td>
    <td align="center"><?php echo $valhab->Fields('tt_nombre')." (".$valhab->Fields('id_hotdet').")"; ?></td>
    <td align="center"><?php echo $valhab->Fields('hc_hab1')."x".str_replace(".0","",number_format($valhab->Fields('hd_sgl'),1,'.',',')); ?></td>
    <td align="center"><?php echo $valhab->Fields('hc_hab2')."x".str_replace(".0","",number_format($valhab->Fields('hd_dbl')*2,1,'.',',')); ?></td>
    <td align="center"><?php echo $valhab->Fields('hc_hab3')."x".str_replace(".0","",number_format($valhab->Fields('hd_dbl')*2,1,'.',',')); ?></td>
    <td align="center"><?php echo $valhab->Fields('hc_hab4')."x".str_replace(".0","",number_format($valhab->Fields('hd_tpl')*3,1,'.',',')); ?></td>
    <td align="right"><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <?php echo str_replace(".0","",number_format($valxhabxdia,1,'.',',')); ?>.-</td>
    <td align="right"><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <?php 
		$habmark = $valhab->Fields('hc_valor1')+$valhab->Fields('hc_valor2')+$valhab->Fields('hc_valor3')+$valhab->Fields('hc_valor4');
		echo str_replace(".0","",number_format($habmark,1,'.',','));?>.-</td>
  </tr>
  <?php $c++;
	if($valhab->Fields('hc_estado')==0){
		$val_total+=$valxhabxdia;
		$val_total_habmark+=$habmark;
	}
	$valhab->MoveNext();
  }
?>
  <tr>
    <th colspan="8" align="right">TOTAL :</th>
    <th align="right"><b><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <?php echo str_replace(".0","",number_format($val_total,1,'.',',')); ?>.-</b></th>
    <th align="right"><b><? if($cot->Fields('area_d') == 2){ echo "CLP$";}elseif($cot->Fields('area_d') == 1){ echo "US$";} ?> <?php echo str_replace(".0","",number_format($val_total_habmark,1,'.',',')); ?>.-</b></th>
  </tr>
</table>
<? } ?>
<ul><li>Logeo de la Cotizaci&oacute;n.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="4%">N&ordm;</th>
    <th width="13%">Usuario</th>
    <th width="20%">Operador/Hotel</th>
    <th width="17%">Fecha</th>
    <th width="46%">Accion</th>
    <?php
$c = 1;
  while (!$logeo->EOF) {
?>
  <tr title='N&deg;<?php echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <td background="images/th.jpg"><center>
      <?php echo $c; ?>&nbsp;
    </center></td>
    <td align="center"><?php echo $logeo->Fields('usu_login'); ?>&nbsp;</td>
    <td align="center"><?php echo $logeo->Fields('hot_nombre'); ?>&nbsp;</td>
    <td align="center"><?php echo $logeo->Fields('fechaaccion1'); ?>&nbsp;</td>
    <td align="left"><?php echo $logeo->Fields('id_accion')." - ".$logeo->Fields('per_descripcion'); ?>&nbsp;</td>
  </tr>
  <?php $c++;
	$logeo->MoveNext(); 
	}
	
?>
</table><br />

<ul><li>Logeo de la Cotizaci&oacute;n <b>Por Destino</b>.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th>Usuario</th>
    <th>Destino</th>
    <th>Fecha</th>
    <th>Accion</th>
  </tr>

<?php

	$sqlLogPorDestino="
	SELECT 	cd.id_cotdes, h.hot_nombre, 
			p.per_descripcion,
			concat(ifnull(u.usu_nombre, ''), ' ', ifnull(u.usu_pat, ''), ' <b>(',tu.tu_nombre,')</b>') as usuario, 
			date_format(l.fechaaccion, '%d-%m-%Y %H:%m:%s') as fecha
			FROM cotdes cd
			inner join hotel h on h.id_hotel = cd.id_hotel
			inner join log l on l.id_cambio = cd.id_cotdes
			inner join permisos p on p.per_codigo = l.id_accion
			inner join usuarios u on u.id_usuario = l.id_user
			inner join tipousuario tu on tu.id_tipousuario = u.id_tipo
			WHERE	id_cot = ".$cot->Fields('id_cot')."
			order by l.fechaaccion";
	
	$logPorDestino = $db1->SelectLimit($sqlLogPorDestino);
	
	while (!$logPorDestino->EOF) {
		?>
		<tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
		<?php
		echo "<td>".$logPorDestino->Fields("usuario")."</td>
		    <td>".$logPorDestino->Fields("hot_nombre")."</td>
		    <td>".$logPorDestino->Fields("fecha")."</td>
		    <td>".$logPorDestino->Fields("per_descripcion")."</td>
		  </tr>";

		$logPorDestino->MoveNext(); 
	}

?>
</table>


<center>
    <button name="permisos" type="button" onClick="window.location='cot_search.php';" style="width:100px; height:27px">&nbsp;Volver</button>
</center>
</body>
</html>