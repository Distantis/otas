<?php

require_once('clases/usuario.php');
require_once('Connections/db1.php');
$usuario = $_SESSION['usuario'];

$distantis = new distantis();

$permiso = 1207;
require('secure.php');
require_once('includes/functions.inc.php');




if(isset($_POST['mk_area'])){
	foreach ($_POST['mk_area'] as $key => $value) {
		$usuario->markup_area($db1,$value,$key);
	}
}

if(isset($_POST['mk_cont'])){
	foreach ($_POST['mk_cont'] as $key => $value) {
		$usuario->markup_cont($db1,$value,$key);
	}
}

$resarea = $distantis->getAreas($db1,true); 
$resciudad = $distantis->getCities($db1,true);
$respais = $distantis->getCountrys($db1,true);

$respaishots = $distantis->paishots($db1);
$respaisopes = $distantis->paisopes($db1);

if(isset($_POST['finder'])){
	switch ($_POST['finder']){
		//busca markup general de hotel x area
		case '1':
		
			$resmkhot = $distantis->trae_markup_hot($db1,$_POST['area'],$_POST['ciudad'],$_POST['hotname']);

			$htmlreturn = "<tr>
			<td>Ciudad</td>
			<td>Hotel</td>
			<td>Area</td>
			<td>Markup</td>
			<td>Estado</td>
			<td>&nbsp;</td>
			</tr>";
			if($resmkhot->RecordCount()>0){
				$i=1;
				while(!$resmkhot->EOF){
					$htmlreturn.="<tr id='tr_$i'>";
					$htmlreturn.="<td>".$resmkhot->Fields('ciu_nombre')."</td>";
					$htmlreturn.="<td>".$resmkhot->Fields('hot_nombre')."</td>";
					$htmlreturn.="<td>".$resmkhot->Fields('area_nombre')."</td>";
					$htmlreturn.="<td><input type='text' name='mk_h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."' id='mk_h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."' value='".$resmkhot->Fields('markdin_valor')."'></td>";
					if($resmkhot->Fields('markdin_estado')==""){
						$htmlreturn.="<td>No Creado</td>";
						$htmlreturn.="<td>";
						$htmlreturn.="<input type='button' name='btncreamk' id='btncreamk' onClick='creamkhot(\"h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."\")' value='crear markup' />";
						$htmlreturn.="</td>";
					}else{
						$htmlreturn.="<td>";
						$htmlreturn.="<select name='estado_h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."' id='estado_h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."'>";
						$htmlreturn.="<option value='1' ";
						if($resmkhot->Fields('markdin_estado')==1){
							$htmlreturn.= " SELECTED";
						}
						$htmlreturn.=">Inactivo</option>";
						$htmlreturn.="<option value='0'";
						if($resmkhot->Fields('markdin_estado')==0){
							$htmlreturn.=" SELECTED";
						}

						$htmlreturn.=">Activo</option>";
						$htmlreturn.="</select>";
						$htmlreturn.="</td>";
						$htmlreturn.="<td><input type='button' id='btnupmk' onClick='upmkhot(\"h".$resmkhot->Fields('id_hotel')."_a".$resmkhot->Fields('id_area')."\")' value='Actualizar'/></td>";
					}
					$htmlreturn.="</tr>";
					$resmkhot->MoveNext();
					$i++;
				}

			}else{
				$htmlreturn.="<tr><td colspan='6'>No se encontraron resultados</td></tr>";
			}	
			die($htmlreturn);
		break;

		//BUSCA MARKUP GENERAL DE OPERADOR
		case '2':
			
			$resfindopes = $distantis->find_opes($db1,$_POST['pais'],$_POST['opename']);
			
			if($resfindopes->RecordCount()>1){
				$htmlbtn = "<input type='text' name='mkopemulti' id='mkopemulti' value='0.0000'><input type='button' name='btnmkallopes' id='btnmkallopes' onClick='copyMktoAllOpes()' value='replicar'>";
				$htmlimp = "<select name='impforall' id='impforall'><option value='0'>Si</option><option value='1' SELECTED>No</option></select>";
				$htmlbtn2 = "<input type='button' name='mkopemultiup' id='mkopemultiup' onClick='updateallmkopes()' value='Actualizar Todos'>";
			}else{
				$htmlbtn ="";
				$htmlimp = "";
				$htmlbtn2 = "";
			}

			$htmlreturn="<tr><td>nombre</td><td>markup $htmlbtn</td><td>imperativo $htmlimp</td><td>$htmlbtn2</td></tr>";

			while(!$resfindopes->EOF){
				$htmlreturn.="<tr>";
				$htmlreturn.="<td>".$resfindopes->Fields('ag_nombre')."</td>";
				$htmlreturn.="<td><input type='text' name='mkope_".$resfindopes->Fields('id_agencia')."' id='mkope_".$resfindopes->Fields('id_agencia')."' value='".$resfindopes->Fields('ag_markup')."'></td>";
				$htmlreturn.="<td><select name='opeimp_".$resfindopes->Fields('id_agencia')."' id='opeimp_".$resfindopes->Fields('id_agencia')."'>";

				$htmlreturn.="<option value='0'";
				if($resfindopes->Fields('ag_mark_imp')==0){
					$htmlreturn.=" SELECTED";
				}
				$htmlreturn.=">Si</option>";

				$htmlreturn.="<option value='1'";
				if($resfindopes->Fields('ag_mark_imp')==1){
					$htmlreturn.=" SELECTED";
				}
				$htmlreturn.=">No</option>";
				$htmlreturn.="</select></td>";
				$htmlreturn.="<td><input type='button' id='btnupope' value='Actualizar' onClick='upmkope(".$resfindopes->Fields('id_agencia').")'></td>";
				$htmlreturn.="</tr>";
				$resfindopes->MoveNext();
			}

			die($htmlreturn);
		break;
		//BUSCA MARKUP DE GRUPOS DE OPERADORES X HOTEL O TARIFA.
		case '3':
			die($distantis->loadhtmlgroups($db1,null,$_POST['usafec'],$_POST['fec1'],$_POST['fec2'],$_POST['nomgroup']));
		break;

		//formulario de nuevo grupo
		case '4':
			$htmlreturn=$distantis->loadhtmlgrupo($db1); 
			$htmlreturn.="<td><input type='button' name='btninsgrp' id='btninsgrp' value='ingresar'></td>";
			$htmlreturn.="</tr>";
			die($htmlreturn);
		break;

		//carga la lista de operadores asignados a un grupo.
		case '5':
			die($distantis->loadopesxgroup($db1,$_POST['id_grupo']));
		break;

		//carga el formulario de busqueda de operadores para asignarlos a un grupo.
		case '6':
			$htmlreturn="<table>
				<tr>
					<th colspan='8'>Asignar operadores</th>
				</tr>
				<tr>
					<td>Area operador:</td>
					<td>
						<select id='asigopearea'>
							<option value='0'>-=Todas=-</option>";
						
							$resarea->MoveFirst();
							while(!$resarea->EOF){
									$htmlreturn.= "<option value='".$resarea->Fields('id_area')."'>".$resarea->Fields('area_nombre')."</option>";
								$resarea->MoveNext();
							}
						
			$htmlreturn.="			</select>
					</td>
					<td>Pais operador:</td>
					<td>
						<select name='asigopepais' id='asigopepais'>
							<option Value='0'>-=Todos=-</option>";
							
							while (!$respais->EOF) {
								$htmlreturn.= "<option value='".$respais->Fields('id_pais')."'>".$respais->Fields('pai_nombre')."</option>";
								$respais->MoveNext();
							}
							
			$htmlreturn.="			</select>
					</td>
					<td>Nombre Operador:</td>
					<td><input type='text' name='asigopenomdef' id='asigopenomdef'></td>
					<td><input type='button' id='btnasigfindopes' name='btnasigfindopes' value='buscar'></td>
					<td><input type='button' id='btnasigcleanopes' name='btnasigcleanopes' value='limpiar'></td>
				</tr>
			</table><br><table id='tablasigopes' border='1'></table>";
			die($htmlreturn);
		break;

		//carga lista de operadores asignables a un grupo (que no se encuentren ya en el)
		case '7':
				
			$resfindopes = $distantis->find_opes_dis($db1,$_POST['area'],$_POST['pais'],$_POST['nombre'],$_POST['id_grupo']);
			
			if($resfindopes->RecordCount()>0){

				$htmlreturn="<tr><th colspan='3'><input type='button' name='btnallopes' id='btnallopes' value='Marcar Todos'>";
				$htmlreturn.="<input type='button' name='btnunallopes' id='btnunallopes' value='Desmarcar Todos'>";
				$htmlreturn.="<input type='button' name='btnaddtogroup' id='btnaddtogroup' value='Agregar al grupo'>";
				$htmlreturn.="</th></tr>";

				$htmlreturn.="<tr><th>Nombre</th><th>ciudad</th><th>Agregar</th></tr>";

				while(!$resfindopes->EOF){
					$htmlreturn.="<tr>";
					$htmlreturn.="<td>".$resfindopes->Fields('hot_nombre')."</td>";
					$htmlreturn.="<td>".$resfindopes->Fields('ciu_nombre')."</td>";
					$htmlreturn.="<td><input type='checkbox' name='optogroup[]' id='optogroup[]' value='".$resfindopes->Fields('id_hotel')."'></td>";
					$htmlreturn.="</tr>";
					$resfindopes->MoveNext();
				}
				$htmlreturn.="<tr><td colspan='3' align='center'>---</td></tr>";
				$htmlreturn.="<tr><th colspan='3'><input type='button' name='btnallopes' id='btnallopes' value='Marcar Todos'>";
				$htmlreturn.="<input type='button' name='btnunallopes' id='btnunallopes' value='Desmarcar Todos' >";
				$htmlreturn.="<input type='button' name='btnaddtogroup' id='btnaddtogroup' value='Agregar al grupo'>";
				$htmlreturn.="</th></tr>";
			}else{
				$htmlreturn="<tr><td>No se encontraron resultados</td></tr>";
			}
			die($htmlreturn);
		break;

		//carga formulario para modificar un grupo de operadores 
		case '8':
			$htmlreturn = $distantis->loadhtmlgrupo($db1,$_POST['id_grupo']);
			$htmlreturn.="<td><input type='button' name='btnupgrp' id='btnupgrp' onClick ='upgroup(".$_POST['id_grupo'].")'value='Actualizar'></td>";
			$htmlreturn.="</tr>";
			die($htmlreturn);
		break;

		//carga los operadores pertenecientes a un pais especifico.
		case '9':
						
			$resfindopes = $distantis->find_opes($db1,$_POST['id_pais']);
			$htmlreturn="<option value='0'>-=Todos=-</option>";
			while(!$resfindopes->EOF){
				$htmlreturn.="<option value='".$resfindopes->Fields('id_agencia')."'>".$resfindopes->Fields('ag_nombre')."</option>";
				$resfindopes->MoveNext();
			}
			die($htmlreturn);
		break;

		//carga los hoteles pertenecientes a un pais o ciudad especificos.
		case '10':
						
			$resfindopes = $distantis-> hoteles_paisociudad($db1,$_POST['id_pais'],$_POST['id_ciudad']);
			
			$default="-=Todos=-";
			if(isset($_POST['default'])){
				$default = $_POST['default'];
			}
			$htmlreturn="<option value='0'>$default</option>";
			while(!$resfindopes->EOF){
				$htmlreturn.="<option value='".$resfindopes->Fields('id_hotel')."'>".$resfindopes->Fields('hot_nombre')."</option>";
				$resfindopes->MoveNext();
			}
			die($htmlreturn);
		break;

		//carga el formulario de nuevo markup especifico por operador.
		case '11':
						
			$resopes = $distantis->find_opes($db1);

			$reshots = $distantis->hoteles_paisociudad($db1);
		
			$htmlreturn="<table>";
			$htmlreturn.="<tr><td>Operador:</td>";
			$htmlreturn.="<td><select name='newopeesp' id='newopeesp'><option value='0'>-=Seleccione=-</option>";
			while(!$resopes->EOF){
				$htmlreturn.="<option value='".$resopes->Fields('id_agencia')."'>".$resopes->Fields('ag_nombre')."</option>";
				$resopes->MoveNext();
			}
			$htmlreturn.="</select></td>"; 
			
			$htmlreturn.="<td>Pais hotel:</td>";
			$htmlreturn.="<td><select name='paihotesp' id='paihotesp'><option value='0'>-=Seleccione=-</option>";
			while(!$respaishots->EOF){
				$htmlreturn.="<option value='".$respaishots->Fields('id_pais')."'>".$respaishots->Fields('paises')."</option>";
				$respaishots->MoveNext();
			}
			$respaishots->MoveFirst();
			$htmlreturn.="</td>";

			$resciudad = $distantis->getCities($db1,true);

			$htmlreturn.="<td>Ciudad hotel</td>";

			$htmlreturn.="<td><select name='ciuhotesp' id='ciuhotesp'><option value='0'>-=Seleccione=-</option>";
			while(!$resciudad->EOF){
				$htmlreturn.="<option value='".$resciudad->Fields('id_ciudad')."'>".$resciudad->Fields('ciu_nombre')."</option>";
				$resciudad->MoveNext();
			}
			$htmlreturn.="</select>";
			$htmlreturn.="</td>";

			$resmon = $distantis->traer_mon($db1);
			
			$htmlreturn.="<td>Moneda:</td>";
			$htmlreturn.="<td><select name='monhotesp' id='monhotesp'><option value='0'>-=Todas=-</option>";
			while(!$resmon->EOF){
				$htmlreturn.="<option value='".$resmon->Fields('id_mon')."'>".$resmon->Fields('mon_nombre')."</option>";
				$resmon->MoveNext();
			}
			$htmlreturn.="</select>";
			$htmlreturn.="</td>";

			$htmlreturn.="<td>Hotel:</td>";
			$htmlreturn.="<td><select name='newhotesp' id='newhotesp'><option value='0'>-=Seleccione=-</option>";
			while(!$reshots->EOF){
				$htmlreturn.="<option value='".$reshots->Fields('id_hotel')."'>".$reshots->Fields('hot_nombre')."</option>";
				$reshots->MoveNext();	
			}
			$htmlreturn.="</td>";
			$htmlreturn.="</table><br><br>";

			$htmlreturn.="<table id='tabtarsnewmkesp' class='table table-bordered'></table>";

			die($htmlreturn);
		break;

		//Devuelve un arreglo con los precios de costo, markup y venta por defecto de cada habitacion en una tarifa 
		case '12':
						
			$reshotdet = $distantis->trae_arreglocosto($db1,$_POST['idtarifa']);
			
			if($reshotdet->RecordCount()==0){
				die("<?xml version='1.0' encoding='utf-8'?><error>No se pudo rescatar la tarifa, contacte a soporte</error>");
			}

			$htmlreturn="<?xml version='1.0' encoding='utf-8'?>";
			$htmlreturn.="<mainnode>";
				$htmlreturn.="<sgl>";
					$htmlreturn.="<sglcost>".$reshotdet->Fields('hd_sgl')."</sglcost>";
					$htmlreturn.="<sglmk>".$reshotdet->Fields('hd_markup')."</sglmk>";
					$htmlreturn.="<sglventa>".round(($reshotdet->Fields('hd_sgl') / $reshotdet->Fields('hd_markup')),1)."</sglventa>";
				$htmlreturn.="</sgl>";

				$htmlreturn.="<dbt>";
					$htmlreturn.="<dbtcost>".$reshotdet->Fields('hd_dbl')."</dbtcost>";
					$htmlreturn.="<dbtmk>".$reshotdet->Fields('hd_markup')."</dbtmk>";
					$htmlreturn.="<dbtventa>".round(($reshotdet->Fields('hd_dbl') / $reshotdet->Fields('hd_markup')),1)."</dbtventa>";
				$htmlreturn.="</dbt>";

				$htmlreturn.="<dbm>";
					$htmlreturn.="<dbmcost>".$reshotdet->Fields('hd_dbl')."</dbmcost>";
					$htmlreturn.="<dbmmk>".$reshotdet->Fields('hd_markup')."</dbmmk>";
					$htmlreturn.="<dbmventa>".round(($reshotdet->Fields('hd_dbl') / $reshotdet->Fields('hd_markup')),1)."</dbmventa>";
				$htmlreturn.="</dbm>";
				$htmlreturn.="<tpl>";
					$htmlreturn.="<tplcost>".$reshotdet->Fields('hd_tpl')."</tplcost>";
					$htmlreturn.="<tplmk>".$reshotdet->Fields('hd_markup')."</tplmk>";
					$htmlreturn.="<tplventa>".round(($reshotdet->Fields('hd_tpl') / $reshotdet->Fields('hd_markup')),1)."</tplventa>";
				$htmlreturn.="</tpl>";
				$htmlreturn.="<fechas>";
					$htmlreturn.="<fec1>".$reshotdet->Fields('fec1')."</fec1>";
					$htmlreturn.="<fec2>".$reshotdet->Fields('fec2')."</fec2>";
					$htmlreturn.="<desde>".$reshotdet->Fields('desde')."</desde>";
					$htmlreturn.="<hasta>".$reshotdet->Fields('hasta')."</hasta>";
				$htmlreturn.="</fechas>";
			$htmlreturn.="</mainnode>";	
			die($htmlreturn);
		break;

		//Busca markups especificos de operadores x Hotel/tarifa
		case '13':
			if($_POST['usafec']=='true'){
				$usafec = true;
			}else{ 
				$usafec = false;
			}

			$htmlreturn = $distantis->loadopexesp($db1, $_POST['paiope'] , $_POST['paihot'] , $_POST['idope'] , $_POST['idhot'] , $_POST['nomope'] , $_POST['nomhot'] , $usafec,$_POST['fec1'],$_POST['fec2']);
			

			die($htmlreturn);
		break;
		//carga el formulario de modificacion de markup especifico de operador x hotel/tarifa
		case '14':
			die($distantis->loadhtmlmkesp($db1, $_POST['idmkesp']));
		break;
		//carga tarifas con markup general 
		case '15':
						
			$ressearch = $distantis->trae_tarifas_mkgeneral($db1,$_POST['idhothd']);

			$htmlreturn="<thead>";
			$htmlreturn.="<tr>";
			$htmlreturn.="<th>Hotel</th>";
			$htmlreturn.="<th>Tarifa</th>"; 
			$htmlreturn.="<th>SGL</th>";
			$htmlreturn.="<th>DBL</th>";
			$htmlreturn.="<th>TPL</th>";
			$htmlreturn.="<th>Markup</th>";
			$htmlreturn.="<th></th>";
			$htmlreturn.="</tr>";
			$htmlreturn.="</thead><tbody>";

			while(!$ressearch->EOF){
				$htmlreturn.="<tr>";
				$htmlreturn.="<td>".$ressearch->Fields('hot_nombre')."</td>";
				$htmlreturn.="<td>".$ressearch->Fields('tarifa')."</td>";
				$htmlreturn.="<td>".$ressearch->Fields('hd_sgl')."</td>";
				$htmlreturn.="<td>".$ressearch->Fields('hd_dbl')."</td>";
				$htmlreturn.="<td>".$ressearch->Fields('hd_tpl')."</td>";
				$htmlreturn.="<td><input type='text' name='hdmkgral_".$ressearch->Fields('id_hotdet')."' id='hdmkgral_".$ressearch->Fields('id_hotdet')."' value='".$ressearch->Fields('hd_markup')."'></td>";
				$htmlreturn.="<td><input type='button' name='btnhdmkgral' id='btnhdmkgral' value='actualizar' onClick='updatehdmkgral(".$ressearch->Fields('id_hotdet').")'></td>";
				$htmlreturn.="</tr>";
				$ressearch->MoveNext();
			}
			$htmlreturn.="</tbody>";
			die($htmlreturn);
		break;
		//carga las ciudades pertenecientes a un pais específico o todas
		case '16':
			
			$resfindciu = $distantis->getCities($db1, true,$_POST['id_pais']);
			
			$default="-=Todas=-";
			if(isset($_POST['default'])){
				$default = $_POST['default'];
			}
			$htmlreturn="<option value='0'>$default</option>";
			while(!$resfindciu->EOF){
				$htmlreturn.="<option value='".$resfindciu->Fields('id_ciudad')."'>".$resfindciu->Fields('ciu_nombre')."</option>";
				$resfindciu->MoveNext();
			}
			die($htmlreturn);
		break;
		//carga grilla con tarifas de hotel para ingreso de markup especifico de operador x hotel/tarifa
		case '17':
		//	echo "1";
			$reshds = $distantis->trae_tarifas_mkgeneral($db1,0,$_POST['id_mon']);
//echo "1";
			$reshot = $distantis->hoteles_paisociudad($db1,0,0,$_POST['id_hotel']);
		//echo "1";
//die(); 		
			$style= "style='width:60px'";
			$htmlreturn="<tr><th>Tarifa</th>";
			$htmlreturn.="<th>C. Sgl</th>";			
			$htmlreturn.="<th>Mkp Sgl</th>";
			$htmlreturn.="<th>Vta. Sgl</th>";
			$htmlreturn.="<th>C. Dbt</th>";
			$htmlreturn.="<th>Mkp Dbt</th>";
			$htmlreturn.="<th>Vta. Dbt</th>";
			$htmlreturn.="<th>C. Ddm</th>";
			$htmlreturn.="<th>Mkp Dbm</th>";
			$htmlreturn.="<th>Vta. Dbm</th>";
			$htmlreturn.="<th>C. Tpl</th>";
			$htmlreturn.="<th>Mkp Tpl</th>";
			$htmlreturn.="<th>Vta. Tpl</th>";
			$htmlreturn.="<th>Vig. Fec1</th>";
			$htmlreturn.="<th>Vig. Fec2</th>";
			$htmlreturn.="<th><input type='button' name='btnprocnewmkesp' id='btnprocnewmkesp' value='Procesar' onClick='procnewmkespxope()'></th>";
			$htmlreturn.="</tr>";

			$htmlreturn.="<tr>";
			$htmlreturn.="<td>".$reshot->Fields('hot_nombre')." markup especifico de operador x hotel</td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td><input type='text' $style name='mkespsglhot_".$_POST['id_hotel']."' id='mkespsglhot_".$_POST['id_hotel']."' value='0.8000'></td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td><input type='text' $style name='mkespdbthot_".$_POST['id_hotel']."' id='mkespdbthot_".$_POST['id_hotel']."' value='0.8000'></td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td><input type='text' $style name='mkespdbmhot_".$_POST['id_hotel']."' id='mkespdbmhot_".$_POST['id_hotel']."' value='0.8000'></td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td><input type='text' $style name='mkesptplhot_".$_POST['id_hotel']."' id='mkesptplhot_".$_POST['id_hotel']."' value='0.8000'></td>";
			$htmlreturn.="<td>X</td>";
			$htmlreturn.="<td><input type='text' name='mkespfec1hot_".$_POST['id_hotel']."' id='mkespfec1hot_".$_POST['id_hotel']."' value=''></td>";
			$htmlreturn.="<td><input type='text' name='mkespfec2hot_".$_POST['id_hotel']."' id='mkespfec2hot_".$_POST['id_hotel']."' value=''></td>";
			$htmlreturn.="<td><input type='checkbox' name='chkmkesphot_".$_POST['id_hotel']."' id='chkmkesphot_".$_POST['id_hotel']."' value='1'> crear</td>";
			$htmlreturn.="</tr>";
			
			while(!$reshds->EOF){
				echo"1"; 

				$rescheckmk = $distantis->trae_mkophd($db1,$reshds->Fields('id_hotdet'));
				
				if($rescheckmk->RecordCount()>0){
					$trstyle = "style='background:#e83a4a;'";
				}else{
					$trstyle = "";
				}

				$htmlreturn.="<tr $trstyle>";
				$htmlreturn.="<td>".$reshds->Fields('tarifa')."</td>";
				$htmlreturn.="<td><input type='text' $style name='costnewsgl_".$reshds->Fields('id_hotdet')."' id='costnewsgl_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('hd_sgl')."' disabled></td>";
				$htmlreturn.="<td><input type='text' $style name='mknewsgl_".$reshds->Fields('id_hotdet')."' id='mknewsgl_".$reshds->Fields('id_hotdet')."' value='0.8000'></td>";
				$htmlreturn.="<td><input type='text' $style name='vtanewsgl_".$reshds->Fields('id_hotdet')."' id='vtanewsgl_".$reshds->Fields('id_hotdet')."' value='".($reshds->Fields('hd_sgl')/0.8)."'></td>";

				$htmlreturn.="<td><input type='text' $style name='costnewdbt_".$reshds->Fields('id_hotdet')."' id='costnewdbt_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('hd_dbl')."' disabled></td>";
				$htmlreturn.="<td><input type='text' $style name='mknewdbt_".$reshds->Fields('id_hotdet')."' id='mknewdbt_".$reshds->Fields('id_hotdet')."' value='0.8000'></td>";
				$htmlreturn.="<td><input type='text' $style name='vtanewdbt_".$reshds->Fields('id_hotdet')."' id='vtanewdbt_".$reshds->Fields('id_hotdet')."' value='".($reshds->Fields('hd_dbl')/0.8)."'></td>";

				$htmlreturn.="<td><input type='text' $style name='costnewdbm_".$reshds->Fields('id_hotdet')."' id='costnewdbm_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('hd_dbl')."' disabled></td>";
				$htmlreturn.="<td><input type='text' $style name='mknewdbm_".$reshds->Fields('id_hotdet')."' id='mknewdbm_".$reshds->Fields('id_hotdet')."' value='0.8000'></td>";
				$htmlreturn.="<td><input type='text' $style name='vtanewdbm_".$reshds->Fields('id_hotdet')."' id='vtanewdbm_".$reshds->Fields('id_hotdet')."' value='".($reshds->Fields('hd_dbl')/0.8)."'></td>";

				$htmlreturn.="<td><input type='text' $style name='costnewtpl_".$reshds->Fields('id_hotdet')."' id='costnewtpl_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('hd_tpl')."' disabled></td>";
				$htmlreturn.="<td><input type='text' $style name='mknewtpl_".$reshds->Fields('id_hotdet')."' id='mknewtpl_".$reshds->Fields('id_hotdet')."' value='0.8000'></td>";
				$htmlreturn.="<td><input type='text' $style name='vtanewtpl_".$reshds->Fields('id_hotdet')."' id='vtanewtpl_".$reshds->Fields('id_hotdet')."' value='".($reshds->Fields('hd_tpl')/0.8)."'></td>";

				$htmlreturn.="<td><input type='text' name='mkespfec1hd_".$reshds->Fields('id_hotdet')."' id='mkespfec1hd_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('fec1')."'></td>";
				$htmlreturn.="<td><input type='text' name='mkespfec2hd_".$reshds->Fields('id_hotdet')."' id='mkespfec2hd_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('fec2')."'></td>";
				$htmlreturn.="<td><input type='checkbox' name='chkmkesphd_".$reshds->Fields('id_hotdet')."' id='chkmkesphd_".$reshds->Fields('id_hotdet')."' value='".$reshds->Fields('id_hotdet')."'> crear</td>";
				$htmlreturn.="</tr>";
				$reshds->MoveNext();
			}
			die($htmlreturn);
		break;


		default:
			
		break;
	}

}

if(isset($_POST['action'])){
	switch ($_POST['action']) {
		//insertar markup general por hotel-area	
		case '1':
						
			$distantis->insertamkhotelarea($db1,$_POST['id_hotel'],$_POST['id_area'],$_POST['mkhot'],$usuario->id_usuario);

			die($distantis->loadhtmlhot($db1, $_POST['id_hotel'],$_POST['id_area']));
		break;

		//updatea el markup dinamico general de hotel-area
		case '2':
			
			$distantis->updatemkhotelarea($db1,$_POST['id_hotel'],$_POST['id_area'],$_POST['mkhot'],$usuario->id_usuario,$_POST['mkestado']);
			
			die($distantis->loadhtmlhot($db1, $_POST['id_hotel'],$_POST['id_area']));
		break;

		//Updatea el markup general de un operador
		case '3':

			if(isset($_POST['impes'])){
				die("entro1");
				$aux = str_replace("mkope_", "", $_POST['opes']);
				$oparray = explode("&", $aux);
				foreach ($oparray as $nodo => $idandvalue){
					echo "<br>$nodo: $idandvalue";
					$splitter = explode("=", $idandvalue);
					$aropes[$splitter[0]]= $splitter[1];
				}

				$aux = str_replace("opeimp_","",$_POST['impes']);
				$oparray = explode("&",$aux);
				foreach ($oparray as $nodo => $idandimp) {
					$splitter = explode("=",$idandimp);
					
					$distantis->updatemkagencia($db1,$aropes[$splitter[0]],$splitter[1],$splitter[0]);
					
				}
			}else{
				
				$distantis->updatemkagencia($db1,$_POST['mkope'],$_POST['imperativo'],$_POST['id_hotel']);
				
				
				$rescheckope = $distantis->find_opes($db1,$pais=0,$opename="",$_POST['id_hotel']);
				
				if($rescheckope->Fields('ag_mark_imp') == $_POST['imperativo'] && $rescheckope->Fields('ag_markup') == $_POST['mkope']){
					die("Cambios realizados con exito");
				}else{
					//die("paso2");
					die("Error al intentar actualizar operador");
				}
			}
		break;

		//ingreso de grupo nuevo
		case '4':
			
		break;
		
		//devuelve las tarifas de un hotel en fomato <option> para insertarlos en un select
		case '5':
			
			die($distantis->traetarifasoption($db1,$_POST['id_hotel']));
		break;
		//agrega operadores a un grupo de markup diferenciado
		case '6':
		/*	$aux = str_replace("optogroup%5B%5D=", "", $_POST['opes']);
			$oparray = explode("&", $aux);

			$sqlgroup = "SELECT * FROM mk_grupo WHERE id_mkgrupo = ".$_POST['id_grupo'];
			$resgroup= $db1->SelectLimit($sqlgroup) or die(__LINE__." ".$db1->ErrorMsg());

			$qhotdet ="";
			$qhotdet2="";
			$loghd = "";
			$desclog = "operador agregado a grupo - Hotel";
			$jerlog = 4;
			if($resgroup->Fields('id_hotdet')!=''){
				$loghd = "id_tarifa,";
				$qhotdet = "id_hotdet,";
				$qhotdet2= $resgroup->Fields('id_hotdet').",";
				$desclog = "operador agregado a grupo - tarifa";
				$jerlog = 3;
			}

			foreach ($oparray as $key => $value) {
				$sqlinsgorup = "INSERT INTO mk_ope_hotdet 
					($qhotdet
					id_agencia,
					id_hotel,
					mk_fecdesde,
					mk_fechasta,
					mk_markup_sgl,
					mk_markup_dbt,
					mk_markup_dbm,
					mk_markup_tpl,
					mk_estado,
					id_mkgrupo) 
					VALUES 
					($qhotdet2
					$value,
					".$resgroup->Fields('id_hotel').",
					'".$resgroup->Fields('mkg_fecdesde')."',
					'".$resgroup->Fields('mkg_fechasta')."',
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').",
					0,
					".$resgroup->Fields('id_mkgrupo').")";
				$db1->Execute($sqlinsgorup) or die(__LINE__." ".$db1->ErrorMsg());

				$idlog =$db1->Insert_ID();

				$sqllog = "INSERT INTO log_markup ($loghd id_hotel, id_operador, fec_vig1, fec_vig2, mk_sgl, mk_twin, mk_mat, mk_tpl, jerarquia, fecha, id_usuario, id_grupo, estado, descripcion, pk_target)
				VALUES ($qhotdet2 ".$resgroup->Fields('id_hotel').",  $value, '".$resgroup->Fields('mkg_fecdesde')."',
					'".$resgroup->Fields('mkg_fechasta')."',
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').",
					".$resgroup->Fields('mkg_markup').", $jerlog, NOW(), ".$usuario->id_usuario.",".$resgroup->Fields('id_mkgrupo')."0,'".$desclog."', $idlog)";
				$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
			}
			die();*/
		break;
		//updatea grupo de operadores con markup diferenciado.
		case '7':
		/*	$sqlins="";
			if($_POST['hotdet']==0){
				$sqlins="NULL";
				$loghd = "";
				$desclog = "Modifica markup de grupo - Hotel";
				$jerlog = 4;
				$qhotdet2="";
			}else{
				$qhotdet2= $_POST['hotdet'].",";
				$sqlins=$_POST['hotdet'];
				$loghd = "id_tarifa,";
				$desclog = "Modifica markup de grupo - tarifa";
				$jerlog = 3;
			}


			$sqlupgrp = "UPDATE mk_grupo SET mkg_nombre = '".$_POST['gname']."', mkg_fecdesde = '".dar_vuelta_fec($_POST['gfec1'])."', mkg_fechasta = '".dar_vuelta_fec($_POST['gfec2'])."', 
				mkg_markup = ".$_POST['gmarkup'].", id_hotel = ".$_POST['hotel'].", id_hotdet = $sqlins , mkg_estado = ".$_POST['gstate']." WHERE id_mkgrupo = ".$_POST['id_grupo'];
			$db1->Execute($sqlupgrp) or die(__LINE__." ".$db1->ErrorMsg());


			$sqllog = "INSERT INTO log_markup ($loghd id_hotel, id_grupo, fecha, tabla_target, mk_sgl, mk_twin, mk_mat, mk_tpl, estado, id_usuario, fec_vig1, fec_vig2, jerarquia, descripcion, pk_target)
				VALUES ($logidhd ".$_POST['hotel'].", $idgrupo, NOW(), 'mk_grupo', ".$_POST['gmarkup'].",".$_POST['gmarkup'].",".$_POST['gmarkup'].",".$_POST['gmarkup'].",".$_POST['gstate'].", ".$usuario->id_usuario.", '".dar_vuelta_fec($_POST['gfec1'])."','".dar_vuelta_fec($_POST['gfec2'])."', $jerlog, '".$desc."',".$_POST['id_grupo'].")";
 			$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());

			$sqlupopes = "UPDATE mk_ope_hotdet SET 
				id_hotel = ".$_POST['hotel'].",
				mk_fecdesde = '".dar_vuelta_fec($_POST['gfec1'])."',
				mk_fechasta = '".dar_vuelta_fec($_POST['gfec2'])."',
				mk_markup_sgl = ".$_POST['gmarkup'].",
				mk_markup_dbt = ".$_POST['gmarkup'].",
				mk_markup_dbm = ".$_POST['gmarkup'].",
				mk_markup_tpl = ".$_POST['gmarkup'].",
				mk_estado = ".$_POST['gstate'].",
				id_hotdet = $sqlins
				WHERE id_mkgrupo = ".$_POST['id_grupo']."
				AND mk_estado = 0";
			$db1->Execute($sqlupopes) or die(__LINE__." ".$db1->ErrorMsg());
			
			$sqlgetopes = "SELECT * FROM mk_ope_hotdet id_mkgrupo = ".$_POST['id_grupo']." AND mk_estado = 0";
			$resgetopes = $db1->SelectLimit($sqlgetopes) or die(__LINE__." ".$db1->ErrorMsg());

			while(!$resgetopes->EOF){
				$sqllog = "INSERT INTO log_markup ($loghd id_hotel, id_operador, fec_vig1, fec_vig2, mk_sgl, mk_twin, mk_mat, mk_tpl, jerarquia, fecha, id_usuario, id_grupo, estado, descripcion, pk_target)
					VALUES ($qhotdet2 ".$resgroup->Fields('id_hotel').",  $value, '".dar_vuelta_fec($_POST['gfec1'])."',
					'".dar_vuelta_fec($_POST['gfec2'])."',
					".$_POST['gmarkup'].",
					".$_POST['gmarkup'].",
					".$_POST['gmarkup'].",
					".$_POST['gmarkup'].", $jerlog, NOW(), ".$usuario->id_usuario.",".$_POST['id_grupo']."0,'".$desclog."', ".$resgetopes->Fields('id_mk').")";
				$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
				$resgetopes->MoveNext();
			}

			die($distantis->loadhtmlgroups($db1,$_POST['id_grupo']));*/
		break;
		//Quita un operador de un grupo con markup diferenciado.
		case '8':
			/*$sqlup = "UPDATE mk_ope_hotdet SET mk_estado = 1 WHERE mk_id = ".$_POST['mkid'];
			$db1->Execute($sqlup) or die(__LINE__." ".$db1->ErrorMsg());

			$sqlgetgroup = "SELECT id_mkgrupo, 
				id_hotdet, 
				mk_markup_sgl as mk_sgl, 
				id_agencia as ope, 
				id_hotel as id_hot ,
				DATE_FORMAT(mk_fecdesde, '%Y-%m-%d') AS fec1,
				DATE_FORMAT(mk_fechasta, '%Y-%m-%d') AS fec2,
				mk_estado
				FROM mk_ope_hotdet WHERE mk_id =".$_POST['mkid'];
			$resgetgroup = $db1->SelectLimit($sqlgetgroup) or die(__LINE__." ".$db1->ErrorMsg());

			$loghd = "";
			$logidhd ="";
			$jerar = 4;
			$desclog = 'Quita operador de grupo - hotel';
			if($resgetgroup->Fields('id_hotdet')!=""){
				$loghd = "id_tarifa, ";
				$logidhd = $resgetgroup->Fields('id_hotdet').", ";
				$jerar = 3;
				$desclog = 'Quita operador de grupo - tarifa';
			}
			$mklog = $resgetgroup->Fields('mk_sgl');

			$sqllog = "INSERT INTO log_markup ($loghd, id_hotel, id_operador, fec_vig1, fec_vig2, mk_sgl, mk_twin, mk_mat, mk_tpl, tabla_target,jerarquia, fecha, id_usuario, id_grupo, estado, descripcion, pk_target)
			VALUES ($logidhd ".$resgetgroup->Fields('id_hot').", ".$resgetgroup->Fields('ope').", '".$resgetgroup->Fields('fec1')."', '".$resgetgroup->Fields('fec2')."', $mklog, $mklog, $mklog, $mklog, 'mk_ope_hotdet',$jerar, NOW(), ".$usuario->id_usuario.", ".$resgetgroup->Fields('id_mkgrupo').", ".$resgetgroup->Fields('mk_estado').", '".$desclog."', ".$_POST['mkid'].")";
			$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
			
			die($distantis->loadopesxgroup($db1,$resgetgroup->Fields('id_mkgrupo')));*/
		break;

		//inserta markup especifico de operador
		case '9':
			            
			$distantis->ingresamkespecifico($db1,$_POST['idtar'],$_POST['mksgl'],$_POST['mkdbt'],$_POST['mkdbm'],$_POST['mktpl'],$_POST['idhot'],$_POST['idope'],$_POST['fec1'],$_POST['fec2'],$usuario->id_usuario);

			$htmlreturn = $distantis->loadopexesp($db1, 0 , 0 , $_POST['idope'] , $_POST['idhot'] , '' , '' , true,$_POST['fec1'],$_POST['fec2']);
			die($htmlreturn);
		break;

		//actualiza markup especifico de operador
		case '10':
			
			$distantis->updatemkespecifico($db1,$_POST['idtar'],$_POST['mksgl'],$_POST['mkdbt'],$_POST['mkdbm'],$_POST['mktpl'],$_POST['idhot'],$_POST['idope'],$_POST['fec1'],$_POST['fec2'],$usuario->id_usuario,$_POST['newest'],$_POST['imkesp']);
			
			$htmlreturn = $distantis->loadopexesp($db1, 0 , 0 , $_POST['idope'] , $_POST['idhot'] , '' , '' , true,$_POST['fec1'],$_POST['fec2']);
			die($htmlreturn);
		break;
		//actualiza markup general de tarifa
		case '11':
			 $sqlupdate = "UPDATE hotdet SET hd_markup = ".$_POST['mkhd']." WHERE id_hotdet = ".$_POST['idhd'];
			
			
			$db1->Execute($sqlupdate) or die(__LINE__." ".$db1->ErrorMsg());
		//	echo $sqlupdate."<br>";
		//	die();
			$sqllog = "INSERT INTO log_markup (id_tarifa, fecha, tabla_target, mk_sgl, mk_twin, mk_mat, mk_tpl, estado, id_usuario, jerarquia, descripcion, pk_target) VALUES
			(".$_POST['idhd'].", NOW(), 'hotdet', ".$_POST['mkhd'].",".$_POST['mkhd'].",".$_POST['mkhd'].",".$_POST['mkhd'].",0, ".$usuario->id_usuario.", 5, 'Actualiza mk gral tarifa', ".$_POST['idhd'].")";
			$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());

			die("ok");
		break;

		default:
			# code...
		break;
	}


}

header("Content-Type: text/html; charset=ISO-8859-1");
?>


<!DOCTYPE html>
<html>
	<head>
		<!--title>Markup Dinamico</title!-->
		<title><?=$usuario->nombre_plataforma;?></title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<script src="js/jquery-3.2.1.min.js"></script>
  		<script src="js/jquery-ui/jquery-ui.js"></script>
    	<link href="css/test.css" rel="stylesheet" type="text/css"/>
    	<link rel="stylesheet" href="js/jquery-ui/jquery-ui.css">

		
		<style type="text/css">
		input.tbl  {
			width: 150px;
			height: 20px;
		}
		</style>
		<script type="text/javascript">

		
			$(function(){



				//setea los calendarios del formulario de grupos de operadores.
				var dates = $("#dategroup1, #dategroup2" ).datepicker({
			   		changeMonth: true,
			   		changeYear:true,
			     	numberOfMonths: 2,
			     	dateFormat: 'dd-mm-yy',
			     	showOn: "button",
			      	buttonText: '...' ,
		 			onSelect: function( selectedDate ){
			      		var option = this.id == "dategroup1" ? "minDate" : "maxDate",
			       		instance = $( this ).data( "datepicker" ),
			       		date = $.datepicker.parseDate(instance.settings.dateFormat ||$.datepicker._defaults.dateFormat,selectedDate, instance.settings);
			      		dates.not(this).datepicker( "option", option, date );
			     	}
		  		});
		  		//setea los calendarios del formulario de markup especifico.
		  		var dates2 = $("#espfecdesde, #espfechasta" ).datepicker({
			   		changeMonth: true,
			   		changeYear:true,
			     	numberOfMonths: 2,
			     	dateFormat: 'dd-mm-yy',
			     	showOn: "button",
			      	buttonText: '...' ,
		 			onSelect: function( selectedDate ){
			      		var option = this.id == "espfecdesde" ? "minDate" : "maxDate",
			       		instance = $( this ).data( "datepicker" ),
			       		date = $.datepicker.parseDate(instance.settings.dateFormat ||$.datepicker._defaults.dateFormat,selectedDate, instance.settings);
			      		dates.not(this).datepicker( "option", option, date );
			     	}
		  		});




		  		//busca los hoteles con markup diferenciado de acuerdo a los parametros en el formulario.
				$('#btnfindhots').click(function(){
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 1,
							area: $("#hotarea").val(),
							ciudad:$("#hotciudad").val(),
							hotname:$("#hotname").val()
						},
						success:function(result){
							$("#mkgralhot").html(result);	
						},
						error:function(){
							alert("btnfindhots - Error al cargar la lista");
						}
					});
				});
				//limpia el contenido de la sección donde se muestran los hoteles con markup diferenciado.
				$("#btncleanhots").click(function(){
					$("#mkgralhot").html("");
				});
				//busca los operadores con markup diferenciado de acuerdo a los parametros del formulario.
				$("#btnfindopes").click(function(){
					
				//	alert($("#opepais").val());
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 2,
							area: $("#opearea").val(),
							pais: $("#opepais").val(),
							opename:$("#openomdef").val()
						},
						success:function(result){
							$("#mkgralopes").html(result);

							$("#impforall").change(function(){
								var confirmacion = confirm("¿Desea cambiar el valor imperativo de todos los operadores en la lista?");

								if(confirmacion){
									$("[name^=opeimp_]").val($("#impforall").val());
								}else{
									if($("#impforall").val()==0){
										$("#impforall").val(1);
									}else{
										$("#impforall").val(0)
									}
								}
							});
						},
						error:function(){
							alert("btnfindopes - Error al cargar la lista");
						}
					});
				});
				//limpia el contenido de la sección donde se muestran los operadores con markup diferenciado.
				$("#btncleanopes").click(function(){
					$("#mkgralopes").html("");
				});
				//busca los grupos de operadores con markup diferenciado de acuerdo a los parametros del formulario.
				$("#btnfindgroups").click(function(){
					clearasigopes();
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 3,
							nomgroup: $("#groupname").val(),
							fec1: $("#dategroup1").val(),
							fec2:$("#dategroup2").val(),
							usafec: $("#fecsgroups").is(':checked')
						},
						success:function(result){
							$("#mkgroups").html(result);	
						},
						error:function(){
							alert("btnfindgroups - Error al cargar la lista");
						}
					});
				});
				//limpia el contenido de la sección donde se muestran los hoteles con markup diferenciado.
				$("#btncleangroups").click(function(){
					$("#mkgroups").html("");
					clearasigopes();
				});
				//carga el formulario de creacion de grupos de operadores con markup diferenciado.
				$("#btnnewgroup").click(function(){
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 4
						},
						success:function(result){
							$("#mkgroups").html(result);
							$("#btninsgrp").click(function(){
								insnewgroup();
							});
							$("#hotgroup").change(function(){
								cargahotdet($(this).val(), "hdgrupo");
							});

							cargahotdet($("#hotgroup").val(), "hdgrupo");

							var dates = $("#newfec1, #newfec2" ).datepicker({
						     	//defaultDate: "+1w",
						   		changeMonth: true,
						   		changeYear:true,
						     	numberOfMonths: 2,
						     	dateFormat: 'dd-mm-yy',
						     	showOn: "button",
						      	buttonText: '...' ,
					 			onSelect: function( selectedDate ){
						      		var option = this.id == "newfec1" ? "minDate" : "maxDate",
						       		instance = $( this ).data( "datepicker" ),
						       		date = $.datepicker.parseDate(instance.settings.dateFormat ||$.datepicker._defaults.dateFormat,selectedDate, instance.settings);
						      		dates.not(this).datepicker( "option", option, date );
						     	}
					  		});
						},
						error:function(){
							alert("btnnewgroup - Error al cargar la lista");
						}
					});
				});
				//carga los operadores por defecto en el formulario de markup especifico
				loadselope(0,"espidope");
				//carga los hoteles por defecto en el formulario de markup especifico
				loadselhot(0,"espidhot");
				//carga los hoteles por defecto en el formulario de markup por tarifa
				loadselhot(0, "hotmkhd");
				//limipa el contenido de la sección donde se muestran los markup especificos de operador.
				$("#btnclearesp").click(function(){
					$("#tabesp").html("");
				});

				//carga el formulario de ingreso de nuevo markup especifico por operador
				$("#btnnewesp").click(function(){
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 11
						},
						success:function(result){
							$("#tabesp").html(result);
							$("#paihotesp").change(function(){
								loadselhot($(this).val(),'newhotesp');
								loadselciu($(this).val(),'ciuhotesp');
							});
							$("#ciuhotesp").change(function(){
								loadselhot(0,'newhotesp',this.value);
							});

							$("#newhotesp").change(function(){
								cargagrillamkesp(this.value, $("#monhotesp").val());
							});

						},
						error:function(){
							alert("btnnewesp - Error al cargar la lista");
						}
					});
				});
				//busca markup estecifico de operador.
				$("#btnfindesp").click(function(){
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 13,
							paiope: $("#esppaisope").val(),
							paihot: $("#esppaishot").val(),
							idope: $("#espidope").val(),
							idhot: $("#espidhot").val(),
							fec1: $("#espfecdesde").val(),
							fec2: $("#espfechasta").val(),
							nomope: $("#espnomope").val(),
							nomhot: $("#espnomhot").val(),
							usafec: $("#espactfec").is(':checked')

						},
						success:function(result){
							$("#tabesp").html(result);
							$(".buscar").css("display","block");

							/*$("#tabesp").DataTable({
				     			"scrollY":        "200px",
						        "scrollCollapse": true,
						        "paging":         false,
				                "aaSorting": [[0, 'asc']],
				                "oLanguage": {
				                    "sProcessing": "Procesando...",
				                    "sLengthMenu": "Mostrar _MENU_ ",
				                    "sZeroRecords": "No se han encontrado datos disponibles",
				                    "sEmptyTable": "No se han encontrado datos disponibles",
				                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				                    "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
				                    "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
				                    "sInfoPostFix": "",
				                    "sSearch": "Buscar: ",
				                    "sUrl": "",
				                    "sInfoThousands": ",",
				                    "sLoadingRecords": "Cargando...",
				                    "oPaginate": {
				                        "sFirst": "Primero",
				                        "sLast": "Último",
				                        "sNext": "Siguiente",
				                        "sPrevious": "Anterior"
				                    },
				                    "oAria": {
				                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
				                    }
				                }

				         		});*/

						},
						error:function(){
							alert("insnewmkesp - Error al cargar la lista");
						}
					});
				});
				//busca lista de tarifas con markup general.
				$("#btnfindhdmk").click(function(){
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							finder: 15,
							idhothd: $("#hotmkhd").val()
						},
						success:function(result){
							$("#mkgralhds").html(result);	
						},
						error:function(){
							alert("btnfindhdmk - Error al cargar la lista");
						}
					});
				});
				//limpia lista de tarifas con markup general.
				$("#btnclearhdmk").click(function(){
					$("#mkgralhds").html("");
				});
			});
			//carga grilla de markup especifico de operador x hotel/tarifa
			function cargagrillamkesp(idhotel, idmon){
				moneda = idmon || 0;
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 17,
						id_hotel: idhotel,
						id_mon: moneda
					},
					success:function(result){
						$("#tabtarsnewmkesp").html(result);
						//setea datepicker de campos de hotel
						$("[name^=mkespfec1hot_],[name^=mkespfec2hot_]").datepicker({
					     	//defaultDate: "+1w",
					   		changeMonth: true,
					   		changeYear:true,
					     	numberOfMonths: 2,
					     	dateFormat: 'dd-mm-yy',
					     	showOn: "button",
					      	buttonText: '...'
				  		});

						//trae cada input fec1 (fecha desde) correspondiente a cada tarifa y saca el id de tarifa 
						$("[name^=mkespfec1hd]").each(function(){
							var aux = $(this).attr("id").split("_");
							//setea datepickers de tarifa.
							var dates = $("#mkespfec1hd_"+aux[1]+", #mkespfec2hd_"+aux[1]).datepicker({
						     	//defaultDate: "+1w",
						   		changeMonth: true,
						   		changeYear:true,
						     	numberOfMonths: 2,
						     	dateFormat: 'dd-mm-yy',
						     	showOn: "button",
						      	buttonText: '...' ,
						      	minDate: $.datepicker.parseDate('dd-mm-yy', $("#mkespfec1hd_"+aux[1]).val()),
						      	maxDate: $.datepicker.parseDate('dd-mm-yy', $("#mkespfec2hd_"+aux[1]).val())

					  		});
						});
						$("#tabtarsnewmkesp [name^=mknew]").change(function(){
							var auxid = $(this).attr("id").replace("mknew", "");
							var mark = parseFloat($("#mknew"+auxid).val());
							mark = Math.round((mark + 0.0000000001) * 10000) / 10000;
							if($("#mknew"+auxid).val() < 0.0001 || $("#mknew"+auxid).val() > 1 || $("#mknew"+auxid).val().length > 6){
								if(mark<0.0001){
									mark = 1.0;
								}
								if(mark>1){
									mark = 1.0;
								}
								alert("Markup debe ser menor o igual a 1, mayor que 0.0000 y puede tener maximo 4 decimales");
								$("#mknew"+auxid).val(mark);
							}
							var venta = $("#costnew"+auxid).val() / $("#mknew"+auxid).val();
							venta = Math.round((venta + 0.0000000001) * 100) / 100;
							$("#vtanew"+auxid).val(venta);
						});


						$("#tabtarsnewmkesp [name^=vtanew]").change(function(){
							var auxid = $(this).attr("id").replace("vtanew", "");

							var mark = $("#costnew"+auxid).val() / $("#vtanew"+auxid).val();
							mark = Math.round((mark + 0.0000000001) * 10000) / 10000;
							if(mark < 0.0001 || mark > 1 || $("#mknew"+auxid).val().length > 6){
								if(mark<0.0001){
									mark = 1.0;
								}
								if(mark>1){
									mark = 1.0;
								}
								alert("Markup debe ser menor o igual a 1, mayor que 0.0000 y puede tener maximo 4 decimales");
							}
							$("#mknew"+auxid).val(mark);
							var venta = $("#costnew"+auxid).val() / $("#mknew"+auxid).val();
							venta = Math.round((venta + 0.0000000001) * 100) / 100;
							$("#vtanew"+auxid).val(venta);

						});


						
					},
					error:function(){
						alert("cargagrillamkesp - Error al cargar la lista");
					}
				});
			}

			//actualiza markup gral de tarifa;
			function updatehdmkgral(idtar){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 11,
						mkhd: $("#hdmkgral_"+idtar).val(),
						idhd: idtar
					},
					success:function(result){
						
						alert("markup actualizado");	
					},
					error:function(){
						alert("updatehdmkgral - Error al actualizar la tarifa");
					}
				});
			}





			//inserta nuevo markup especifico por operador.
			function insnewmkesp(){
				var stoper = false;
				if($("#newtaresp").val()==0){
					$("[id ^=mknew").each(function(){
						if($(this).val().trim()==""){
							alert("Debe ingresar todos los markups.");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
					if(stoper){
						return false;
					}

					$("[id^= newespfec").each(function(){
						if($(this).val().trim()==""){
							alert("Debe indicar ambas fechas de vigencia");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
					if(stoper){
						return false;
					}
				}else{
					$("#tabesp input[type=text]").each(function(){
						if($(this).val().trim()==""){
							alert("Debe llenar todos los campos");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
				}
				if(stoper){
					return false;
				}


				$("#cargar").html("<center><i class='fa fa-spinner fa-spin fa-3x'></i></center>");

				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 9,
						mksgl: $("#mknewsgl").val(),
						mkdbt: $("#mknewdbt").val(),
						mkdbm: $("#mknewdbm").val(),
						mktpl: $("#mknewtpl").val(),
						fec1: $("#newespfec1").val(),
						fec2: $("#newespfec2").val(),
						idope: $("#newopeesp").val(),
						idhot: $("#newhotesp").val(),
						idtar: $("#newtaresp").val()
					},
					success:function(result){
						$("#tabesp").html(result);
						$("#cargar").html("");	
					},
					error:function(){
						alert("insnewmkesp - Error al cargar la lista");
					}
				});
			}






			//carga el formulario de modificación de markup especifico operador x Htoel/tarifa
			function modmkesp(idmkes){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 14,
						idmkesp : idmkes
					},
					success:function(result){
						
						$("#tabesp").html(result);
						var dates = $("#newespfec1, #newespfec2" ).datepicker({
					   		changeMonth: true,
					   		changeYear:true,
					     	numberOfMonths: 2,
					     	dateFormat: 'dd-mm-yy',
					     	showOn: "button",
					      	buttonText: '...' 
				  		});
				  		$("#newhotesp").change(function(){
				  			cargahotdet(this.value, "newtaresp");
				  			$("[id ^= costnew]").val("");
							$("[id ^= mknew]").val("");
							$("[id ^= ventanew]").val("");
							$("[id ^= lbl]").html("");
							$("[id ^= ventanew]").attr("disabled","true");
							
				  		});
				  		//cargahotdet($("#newhotesp").val(),"newtaresp");
				  		$("#newtaresp").change(function(){
				  			loadmktar(this.value);
				  		});
				  		$("#mknewsgl").change(function(){
							calcventa("sgl");
						});
						$("#ventanewsgl").change(function(){
							calcmk("sgl");
						});
						$("#mknewdbt").change(function(){
							calcventa("dbt");
						});
						$("#ventanewdbt").change(function(){
							calcmk("dbt");
						});
						$("#mknewdbm").change(function(){
							calcventa("dbm");
						});
						$("#ventanewdbm").change(function(){
							calcmk("dbm");
						});
						$("#mknewtpl").change(function(){
							calcventa("tpl");
						});
						$("#ventanewtpl").change(function(){
							calcmk("tpl");
						});

					},
					error:function(){
						alert("modmkesp - Error al cargar la lista");
					}
				});
			}
			//modifica el markup especifico de un operador x Hotel/Tarfa
			function updatemkesp(idmk){
				var stoper = false;
				if($("#newtaresp").val()==0){
					$("[id ^=mknew").each(function(){
						if($(this).val().trim()==""){
							alert("Debe ingresar todos los markups.");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
					if(stoper){
						return false;
					}

					$("[id^= newespfec").each(function(){
						if($(this).val().trim()==""){
							alert("Debe indicar ambas fechas de vigencia");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
					if(stoper){
						return false;
					}
				}else{
					$("#tabesp input[type=text]").each(function(){
						if($(this).val().trim()==""){
							alert("Debe llenar todos los campos");
							$(this).focus();
							stoper = true;
							return false;
						}
					});
				}
				if(stoper){
					return false;
				}

				$("#cargar").html("<center><i class='fa fa-spinner fa-spin fa-3x'></i></center>");


				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 10,
						mksgl: $("#mknewsgl").val(),
						mkdbt: $("#mknewdbt").val(),
						mkdbm: $("#mknewdbm").val(),
						mktpl: $("#mknewtpl").val(),
						fec1: $("#newespfec1").val(),
						fec2: $("#newespfec2").val(),
						idope: $("#newopeesp").val(),
						idhot: $("#newhotesp").val(),
						idtar: $("#newtaresp").val(),
						newest: $("#estmkesp").val(),
						imkesp: idmk
					},
					success:function(result){
						
						
						$("#tabesp").html(result);
						$("#cargar").html("");
						
					},
					error:function(){
						alert("insnewmkesp - Error al cargar la lista");
					}
				});
			}


			function procnewmkespxope(){
				if($("#newopeesp").val()==0){
					alert("operador invalido");
					return false;
				}
				var finaltab = "";
				//verificamos si markup especifico de hotel esta marcado y enviamos la informacion por separado de las tarifas (flaite pero no habia de otra)
				if($("#tabtarsnewmkesp [mame^=chkmkesphot_]").is(':checked')){
					var stoper = false;
				
					if($("#tabtarsnewmkesp [name^=mkespfec1hot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkespfec2hot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkespsglhot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkespdbthot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkespdbmhot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkesptplhot_]").val().replace(" ", "")==""){
						stoper = true;
					}
					if($("#tabtarsnewmkesp [name^=mkespfec1hot_]").val() > $("#tabtarsnewmkesp [name^=mkespfec2hot_]").val()){
						stoper = true
					}


					if(stoper){
						alert("valores ingresados para markup especifico x hotel son incorrectos");
						return false;
					}

					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							action: 9,
							mksgl: $("#tabtarsnewmkesp [name^=mkespsglhot_]").val(),
							mkdbt: $("#tabtarsnewmkesp [name^=mkespdbthot_]").val(),
							mkdbm: $("#tabtarsnewmkesp [name^=mkespdbmhot_]").val(),
							mktpl: $("#tabtarsnewmkesp [name^=mkesptplhot_]").val(),
							fec1: $("#tabtarsnewmkesp [name^=mkespfec1hot_]").val(),
							fec2: $("#tabtarsnewmkesp [name^=mkespfec2hot_]").val(),
							idope: $("#newopeesp").val(),
							idhot: $("#newhotesp").val(),
							idtar: 0
						},
						success:function(result){
							finaltab+= result;	
						},
						error:function(){
							alert("procnewmkespxope - Error al cargar la lista");
						}
					});
				}

				$("[name^=chkmkesphd_]:checked").each(function(){
					var id = $(this).val();
					$.ajax({
						type: 'POST',
						url: 'conf_markup.php',
						data: {
							action: 9,
							mksgl: $("#tabtarsnewmkesp #mknewsgl_"+id).val(),
							mkdbt: $("#tabtarsnewmkesp #mknewdbt_"+id).val(),
							mkdbm: $("#tabtarsnewmkesp #mknewdbm_"+id).val(),
							mktpl: $("#tabtarsnewmkesp #mknewtpl_"+id).val(),
							fec1: $("#tabtarsnewmkesp #mkespfec1hd_"+id).val(),
							fec2: $("#tabtarsnewmkesp #mkespfec2hd_"+id).val(),
							idope: $("#newopeesp").val(),
							idhot: $("#newhotesp").val(),
							idtar: id
						},
						success:function(result){
							finaltab+= result;	
						},
						error:function(){
							alert("procnewmkespxope - Error al cargar la lista");
						}
					});
				});
				alert("fin de carga");
			}

			//carga el costo, y markup en el formulario de modificacion de markup especifico, setea listeners y gatilla funciones de calculo dentro del mismo formulario
			function loadmktar(tarifa){
				if(tarifa==0){
					$("[id ^= costnew]").val("");
					$("[id ^= mknew]").val("");
					$("[id ^= ventanew]").val("");
					$("[id ^= lbl]").html("");
					$("[id ^= ventanew]").attr("disabled","true");
					$("#newespfec1").val("");
					$("#newespfec2").val("");
					$("#newespfec1,#newespfec2").datepicker('option', 'minDate', 0);
					$("#newespfec1,#newespfec2").datepicker('option', 'maxDate', 0);
					return false;
				}else{
					$("[id ^= ventanew]").removeAttr("disabled");
				}
			
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 12,
						idtarifa: tarifa
					},
					success:function(xml){
						$(xml).find('sgl').each(function(){
							$("#costnewsgl").val($(this).find('sglcost').text());
							$("#mknewsgl").val($(this).find('sglmk').text());
							$("#ventanewsgl").val($(this).find('sglventa').text());
							$("#mknewsgl").change(function(){
								calcventa("sgl");
							});
							$("#ventanewsgl").change(function(){
								calcmk("sgl");
							});
						});
						$(xml).find('dbt').each(function(){
							$("#costnewdbt").val($(this).find('dbtcost').text());
							$("#mknewdbt").val($(this).find('dbtmk').text());
							$("#ventanewdbt").val($(this).find('dbtventa').text());
							$("#mknewdbt").change(function(){
								calcventa("dbt");
							});
							$("#ventanewdbt").change(function(){
								calcmk("dbt");
							});
						});
						$(xml).find('dbm').each(function(){
							$("#costnewdbm").val($(this).find('dbmcost').text());
							$("#mknewdbm").val($(this).find('dbmmk').text());
							$("#ventanewdbm").val($(this).find('dbmventa').text());
							$("#mknewdbm").change(function(){
								calcventa("dbm");
							});
							$("#ventanewdbm").change(function(){
								calcmk("dbm");
							});
						});
						$(xml).find('tpl').each(function(){
							$("#costnewtpl").val($(this).find('tplcost').text());
							$("#mknewtpl").val($(this).find('tplmk').text());
							$("#ventanewtpl").val($(this).find('tplventa').text());
							$("#mknewtpl").change(function(){
								calcventa("tpl");
							});
							$("#ventanewtpl").change(function(){
								calcmk("tpl");
							});
						});
			
						$(xml).find('fechas').each(function(){
							$("#newespfec1").val($(this).find('fec1').text());
							$("#newespfec2").val($(this).find('fec2').text());
							$("#newespfec1,#newespfec2").datepicker('option', 'minDate', new Date($(this).find('desde').text()));
							$("#newespfec1,#newespfec2").datepicker('option', 'maxDate', new Date($(this).find('hasta').text()));
						});
						$("[id ^= lbl]").html("");
					},
					error:function(){
						alert("loadmktar - Error al cargar la lista");
					}
				});
			}
			//calcula el precio de venta aplicado a una habitacion de una tarifa en base al markup ingresado
			function calcventa(hab){
				var mark = parseFloat($("#mknew"+hab).val());
				mark = Math.round((mark + 0.0000000001) * 10000) / 10000;
				if($("#mknew"+hab).val() < 0.0001 || $("#mknew"+hab).val() > 1 || $("#mknew"+hab).val().length > 6){
					if(mark<0.0001){
						mark = 1.0;
					}
					if(mark>1){
						mark = 1.0;
					}
					alert("Markup debe ser menor o igual a 1, mayor que 0.0000 y puede tener maximo 4 decimales");
					$("#mknew"+hab).val(mark);
					calcventa(hab);
					return false;
				}
				var venta = $("#costnew"+hab).val() / $("#mknew"+hab).val();
				venta = Math.round((venta + 0.0000000001) * 100) / 100;
				$("#ventanew"+hab).val(venta);
				$("#lbl"+hab).html(venta);
			}
			//calcula el markup especifico aplicado a la habitacion de una tarifa en base al precio de venta ingresado
			function calcmk(hab){
				var mark = $("#costnew"+hab).val() / $("#ventanew"+hab).val();
				mark = Math.round((mark + 0.0000000001) * 10000) / 10000;
				if(mark < 0.0001 || mark > 1 || $("#mknew"+hab).val().length > 6){
					if(mark<0.0001){
						mark = 1.0;
					}
					if(mark>1){
						mark = 1.0;
					}
					alert("Markup debe ser menor o igual a 1, mayor que 0.0000 y puede tener maximo 4 decimales");
					$("#mknew"+hab).val(mark);
					calcventa(hab);
					return false;
				}
				var ventareal = $("#costnew"+hab).val() / mark;
				ventareal = Math.round((ventareal) * 100) / 100;
				$("#lbl"+hab).html(ventareal);
				$("#mknew"+hab).val(mark);
			}
			//crea el markup general que aplica un hotel en sus tarifas.
			function creamkhot(codehot){
				var splitter = codehot.split("_");
				idhotel = splitter[0].replace("h","");
				idarea = splitter[1].replace("a","");
				markup = $("#mk_"+codehot).val();
				if(markup.indexOf(".")=== -1 || markup>1){
					alert("Formato ingresao incorrecto.");
					$("#mk_"+codehot).focus();
					return false;
				}
				idtr = $("#mk_"+codehot).closest('tr').attr('id');
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 1,
						id_hotel: idhotel,
						id_area: idarea,
						mkhot: markup
					},
					success:function(result){
						$("#"+idtr).html(result);	
					},
					error:function(){
						alert("creamkhot - Error al crear markup");
					}
				});
			}
			//actualiza el markup general que aplica un hotel en sus tarifas.
			function upmkhot(codehot){
				var splitter = codehot.split("_");
				idhotel = splitter[0].replace("h","");
				idarea = splitter[1].replace("a","");
				markup = $("#mk_"+codehot).val();
				estado = $("#estado_"+codehot).val();
				if(markup.indexOf(".")=== -1 || markup>1){
					alert("Formato ingresao incorrecto.");
					$("#mk_"+codehot).focus();
					return false;
				}
				idtr = $("#mk_"+codehot).closest('tr').attr('id');
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 2,
						id_hotel: idhotel,
						id_area: idarea,
						mkhot: markup,
						mkestado: estado
					},
					success:function(result){
						$("#"+idtr).html(result);	
					},
					error:function(){
						alert("creamkhot - Error al crear markup");
					}
				});
			}
			//actualiza el markup diferenciado que ocupa un operador.
			function upmkope(id_ope){
				markup = $("#mkope_"+id_ope).val();
				
				if(markup.indexOf(".")=== -1 || markup>1){
					alert("Formato ingresao incorrecto.");
					$("#mkope_"+id_ope).focus();
					return false;
				}
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 3,
						id_hotel: id_ope,
						mkope: markup,
						imperativo: $("#opeimp_"+id_ope).val()
					},
					success:function(result){
						alert(result);	
					},
					error:function(){
						alert("creamkhot - Error al crear markup");
					}
				});
			}
			//copia el mismo markup para toda la lista de operadores (markup general de operador)
			function copyMktoAllOpes(){
				$("[name^=mkope_]:text").val($("#mkopemulti").val())
			}
			//actualiza todos los markups generales de operador en la lista
			function updateallmkopes(){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 3,
						opes: $("#mkgralopes input[type=text][name^=mkope_]").serialize(),
						impes: $("#mkgralopes select[name^=opeimp_]").serialize()
					},
					success:function(result){
						$("#btnfindopes").click();

					},
					error:function(){
						alert("updateallmkopes - Error al cargar la lista");
					}
				});
			}
			//crea un nuevo grupo de operadores con markup diferenciado.
			function insnewgroup(){
				markup = $("#markgrupo").val();
				if(markup.indexOf(".")=== -1 || markup>1){
					alert("Formato ingresado incorrecto.");
					$("#mk_"+markgrupo).focus();
					return false;
				}
				$("#mkgroups").find('input[type=text]').each(function(){
					if($(this).val().trim()==''){
						alert('Campo no puede estar en blanco');
						$(this).focus();
						return false;
					}
				});
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 4,
						gname: $("#ngroupname").val() ,
						gmarkup: markup,
						gfec1: $("#newfec1").val(),
						gfec2: $("#newfec2").val(),
						gstate: $("#groupstate").val(),
						hotel: $("#hotgroup").val(),
						hotdet: $("#hdgrupo").val()
					},
					success:function(result){
						$("#mkgroups").html(result);
					},
					error:function(){
						alert("creamkhot - Error al crear markup");
					}
				});
			}
			//carga las tarifas del hotel escogido en el formulario en un combobox especifico.
			function cargahotdet(idhot, targetcombo){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action:5,
						id_hotel: idhot
					},
					success:function(result){
						$("#"+targetcombo).html(result);	
					},
					error:function(){
						alert("cargahotdet - Error al cargar la lista");
					}
				});
			}
			//borra el contenido de la sección donde se asignan operadores a un grupo con markup diferenciado.
			function clearasigopes(){
				$("#asigopes").html("<div id='opeson' style='float:left;width:39%;' align='center'></div><div id='opestoadd' style='float:left;width:60%;margin-left:1%;' align='center'></div>");
			}
			//carga los formularios para quitar o agregar operadores a un grupo con markup diferenciado.
			function editopegroup(idgrupo){
				$("#mkgroups").html("");
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 5,
						id_grupo: idgrupo
					},
					success:function(result){
						$("#opeson").html(result);
					},
					error:function(){
						alert("editopegroup - Error al cargar la lista");
					}
				});
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 6
					},
					success:function(result){
						$("#opestoadd").html(result);
						$("#btnasigcleanopes").click(function(){
							$("#tablasigopes").html("");
						});

						$("#btnasigfindopes").click(function(){
							$.ajax({
								type: 'POST',
								url: 'conf_markup.php',
								data: {
									finder: 7,
									id_grupo: idgrupo,
									area: $("#asigopearea").val(),
									pais: $("#asigopepais").val(),
									nombre: $("#asigopenomdef").val()
								},
								success:function(result){
									$("#tablasigopes").html(result);
									$("#btnallopes").click(function(){
										checkallopes();
									});
									$("#btnunallopes").click(function(){
										uncheckallopes();
									});
									$("#btnaddtogroup").click(function(){
										addopestogroup(idgrupo);
									});
								},
								error:function(){
									alert("btnasigfindopes - Error al cargar la lista");
								}
							});
						});
					},
					error:function(){
						alert("editopegroup - Error al cargar la lista");
					}
				});
			}
			//carga el formulario para modificar los datos de un grupo de operadores.
			function editgroup(idgrupo){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 8,
						id_grupo: idgrupo
					},
					success:function(result){
						$("#mkgroups").html(result);
						var dates = $("#newfec1, #newfec2" ).datepicker({
					     	//defaultDate: "+1w",
					   		changeMonth: true,
					   		changeYear:true,
					     	numberOfMonths: 2,
					     	dateFormat: 'dd-mm-yy',
					     	showOn: "button",
					      	buttonText: '...' ,
				 			onSelect: function( selectedDate ){
					      		var option = this.id == "newfec1" ? "minDate" : "maxDate",
					       		instance = $( this ).data( "datepicker" ),
					       		date = $.datepicker.parseDate(instance.settings.dateFormat ||$.datepicker._defaults.dateFormat,selectedDate, instance.settings);
					      		dates.not(this).datepicker( "option", option, date );
					     	}
				  		});
					},
					error:function(){
						alert("editopegroup - Error al cargar la lista");
					}
				});
			}
			//actualiza los datos de un grupo de operadores.
			function upgroup(idgrupo){
				clearasigopes();
				markup = $("#markgrupo").val();
				if(markup.indexOf(".")=== -1 || markup>1){
					alert("Formato ingresado incorrecto.");
					$("#mk_"+markgrupo).focus();
					return false;
				}
				$("#mkgroups").find('input[type=text]').each(function(){
					if($(this).val().trim()==''){
						alert('Campo no puede estar en blanco');
						$(this).focus();
						return false;
					}
				});
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 7,
						gname: $("#ngroupname").val() ,
						gmarkup: markup,
						gfec1: $("#newfec1").val(),
						gfec2: $("#newfec2").val(),
						gstate: $("#groupstate").val(),
						hotel: $("#hotgroup").val(),
						hotdet: $("#hdgrupo").val(),
						id_grupo: idgrupo
					},
					success:function(result){
						$("#mkgroups").html(result);
					},
					error:function(){
						alert("upgroup - Error al crear markup");
					}
				});
			}
			//marca todos los checkbox en la tabla de asignacion de operadores a un grupo.
			function checkallopes(){
				$("#tablasigopes").find('input[type=checkbox]').each(function(){
					$(this).attr('checked',true);
				});
			}
			//desmarcar todos los checkbox en la tabla de asignacion de operadores a un grupo.
			function uncheckallopes(){
				$("#tablasigopes").find('input[type=checkbox]').each(function(){
					$(this).attr('checked',false);
				});
			}
			//agrega los operadores marcados en la tabla de asignacion a un grupo.
			function addopestogroup(id_grupo){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 6,
						id_grupo: id_grupo,
						opes: $("#tablasigopes input[type=checkbox]:checked").serialize()
					},
					success:function(result){
						$("#tablasigopes").html(result);
						editopegroup(id_grupo);

					},
					error:function(){
						alert("btnasigfindopes - Error al cargar la lista");
					}
				});
			}
			//remueve a un operador del grupo con markup diferenciado.
			function quitopefromgroup(id_mk){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						action: 8,
						mkid: id_mk
					},
					success:function(result){
						$("#opeson").html(result);
					},
					error:function(){
						alert("btnasigfindopes - Error al cargar la lista");
					}
				});
			}
			//agrega lista de operadores segun pais a un combobox indicado por parametro.
			function loadselope(paisope, targetcombo){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 9,
						id_pais: paisope
					},
					success:function(result){
						$("#"+targetcombo).html(result);
					},
					error:function(){
						alert("loadselope - Error al cargar la lista");
					}
				});
			}
			//agrega lista de hoteles segun pais a un combobox indicado por parametro.
			function loadselhot(paisope,targetcombo, ciudad){
				ciudad = ciudad || 0;
				paisope= paisope || 0;
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 10,
						id_pais: paisope,
						id_ciudad: ciudad
					},
					success:function(result){
						$("#"+targetcombo).html(result);
					},
					error:function(){
						alert("loadselhot - Error al cargar la lista");
					}
				});
			}
			//agrega lista de ciudades segun pais a un combobox indicado por parametro.
			function loadselciu(pais,targetcombo){
				$.ajax({
					type: 'POST',
					url: 'conf_markup.php',
					data: {
						finder: 16,
						id_pais: pais
					},
					success:function(result){
						$("#"+targetcombo).html(result);
					},
					error:function(){
						alert("loadselciu - Error al cargar la lista");
					}
				});
			}

			jQuery(function($) {
				$(".kwd_search").keyup(function(){
				

					if($(this).val() != ""){

						$("#tabesp tbody>tr").hide();
						$("#tabesp td:contains-ci('" + $(this).val() + "')").parent("tr").show();
					}else
						$("#tabesp tbody>tr").show();


				});


				$.extend($.expr[":"], 
					{
					    "contains-ci": function(elem, i, match, array) 
						{
							return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
						}
					});
			});

		</script>
	</head>
	<body>
		<h2>Markup Dinamico</h2>
		<form name='mkareas' method='POST'>
			<table>
				<tr>
					<th colspan='2'>Markup gral de area</th>
				</tr>
				<tr>
					<td>Area</td>
					<td>Markup</td>
				</tr>
				<?
				while(!$resarea->EOF){
					echo "<tr>";
					echo "<td>".$resarea->Fields('area_nombre')."</td>";
					echo "<td><input type='text' name='mk_area[".$resarea->Fields('id_area')."]' value='".$resarea->Fields('area_markup')."'></td>";
					echo "</tr>";
					$resarea->MoveNext();
				}
				$resarea->MoveFirst();
				?>
				<tr><td colspan='2'><input type='submit' name='subarea' value='Actualizar'></td></tr>
			</table>
		</form>



		
		<br><br><br>



		<table hidden>
			<tr>
				<th colspan='8'>Markup gral de hotel X area</th>
			</tr>
			<tr>
				<td>Area markup:</td>
				<td>
					<select id='hotarea'>
						<option value='0'>-=Todas=-</option>
					<?
						while(!$resarea->EOF){
								echo "<option value='".$resarea->Fields('id_area')."'>".$resarea->Fields('area_nombre')."</option>";
							$resarea->MoveNext();
						}
					?>
					</select>
				</td>
				<td>Ciudad:</td>
				<td>
					<select id='hotciudad'>
						<option value='0'>-=Todos=-</option>
						<?
						while(!$resciudad->EOF){
							echo "<option value='".$resciudad->Fields('id_ciudad')."'>".$resciudad->Fields('ciu_nombre')."</option>";
							$resciudad->MoveNext();
						}
						?>	
					</select>
				</td>
				<td>Hotel:</td>
				<td><input type='text' id='hotname' ></td>
				<td><input type='button' id='btnfindhots' name='btnfindhots' value='buscar'></td>
				<td><input type='button' id='btncleanhots' name='btncleanhots' value='limpiar'></td>
			</tr>
		</table>
		<table id='mkgralhot' border='1'>
		</table>

		<br><br><br>

		<table>
			<tr>
				<th colspan="8">Markup gral de tarifa</th>
			</tr>
			<tr>
				<td>Hotel:</td>
				<td>
					<select name='hotmkhd' id='hotmkhd'></select>
				</td>
				<td><input type='button' name='btnfindhdmk' id='btnfindhdmk' value='buscar'></td>
				<td><input type='button' name='btnclearhdmk' id='btnclearhdmk' value='limpiar'></td>
			</tr>
		</table>
		<table id='mkgralhds' border='1'></table>


		<br><br><br>

		<table>
			<tr>
				<th colspan='8'>Markup gral de operador(cliente)</th>
			</tr>
			<tr>
				<td>Area operador:</td>
				<td>
					<select id='opearea'>
						<option value='0'>-=Todas=-</option>
					<?
						$resarea->MoveFirst();
						while(!$resarea->EOF){
								echo "<option value='".$resarea->Fields('id_area')."'>".$resarea->Fields('area_nombre')."</option>";
							$resarea->MoveNext();
						}
					?>
					</select>
				</td>
				<td>Pais operador:</td>
				<td>
					<select name='opepais' id='opepais'>
						<option Value='0'>-=Todos=-</option>
						<?
						while (!$respais->EOF) {
							echo "<option value='".$respais->Fields('id_pais')."'>".$respais->Fields('pai_nombre')."</option>";
							$respais->MoveNext();
						}
						?>
					</select>
				</td>
				<td>Nombre Operador:</td>
				<td><input type='text' name='openomdef' id='openomdef'></td>
				<td><input type='button' id='btnfindopes' name='btnfindopes' value='buscar'></td>
				<td><input type='button' id='btncleanopes' name='btncleanopes' value='limpiar'></td>
			</tr>
		</table>
		<table id='mkgralopes' border='1'>
		</table>


		<br><br><br>


		

		<div id='asigopes' style='overflow:auto;'>
		</div>


		<br><br><br>


		<table>
			<tr>
				<th colspan='6'>Markup especifico de operador(cliente) x hotel/tarifa</th>
			</tr>

			<tr>
				<td>Pais operador:</td>
				<td><select name='esppaisope' id='esppaisope' onChange='loadselope(this.value, "espidope")'><option value='0'>-=Todos=-</option>
					<?
					while(!$respaisopes->EOF){
						echo "<option value='".$respaisopes->Fields('id_pais')."'>".$respaisopes->Fields('paises')."</option>";
						$respaisopes->MoveNext();
					}
					?>
				</select></td>
				<td>Operador:</td>
				<td><select name='espidope' id='espidope'></select></td>
				<td>Nombre Operador:</td>
				<td><input type='text' name='espnomope' id='espnomope'></td>
			</tr>
			<tr>
				<td>Pais Hotel:</td>
				<td><select name='esppaishot' id='esppaishot' onChange='loadselhot(this.value, "espidhot")'>
						<option value='0'>-=Todos=-</option>
						<?
						while(!$respaishots->EOF){
							echo "<option value='".$respaishots->Fields('id_pais')."'>".$respaishots->Fields('paises')."</option>";
							$respaishots->MoveNext();
						}
						?>
				</select></td>
				<td>Hotel:</td>
				<td><select name='espidhot' id='espidhot'></select></td>
				<td>Nombre hotel:</td>
				<td><input type='text' name='espnomhot' id='espnomhot'></td>
			</tr>
			<tr>
				<td>Fecha desde:</td>
				<td><input type='text' name='espfecdesde' id='espfecdesde'></td>
				<td>Fecha hasta:</td>
				<td><input type='text' name='espfechasta' id='espfechasta'></td>
				<td>Activar Fechas <input type='checkbox' name='espactfec' id='espactfec' value='1'></td>
				<td>
					<input type='button' name='btnfindesp' id='btnfindesp' value='Buscar'>
					&nbsp;
					<input type='button' name='btnclearesp' id='btnclearesp' value='Limpiar'>
					&nbsp;
					<input type='button' name='btnnewesp' id='btnnewesp' value='Nuevo'>
				</td>
			</tr>
		</table>

		<br>
		<div class="col-md-2 pull-right buscar" style="display:none;"><input type="text" class="form-control kwd_search " placeholder="BUSCAR"></div>
		<table id='tabesp' class="table table-bordered datatable"></table>
		<br><br>
		<div id="cargar"></div>



		<br><br><br>

	</body>


</html>