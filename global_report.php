<?php

include("Connections/db1.php");
include("/var/www/otas/clases/cotizacion.php");
include("/var/www/otas/clases/language.php");
include("/var/www/otas/clases/usuario.php");

$cotClass = new Cotizacion();
$userClass = new Usuario();
$lanClass = new Language();
//include('/var/www/otas/secure_opaco.php');





$id_pk = null;
if(isset($_POST['id_pk'])){
	if($_POST['id_pk']!=0){
		$id_pk = $_POST['id_pk'];
	}
}

$hoteles = $cotClass->getHotFichas($db1, null, $id_pk);

$closedColor = "w3-light-red";
$warningColor = "w3-yellow";
if(isset($_POST['getDispo'])){
	$period = new DatePeriod(new DateTime($_POST['fechad']), new DateInterval('P1D'), new DateTime($_POST['fechah']));
	$dispo = $cotClass->getGlobalDispo($db1, $_POST['fechad'], $_POST['fechah'], $id_pk, $_POST['ciudad']);
	$htmlresult.= "<thead class='w3-grey'>";
	$htmlresult.="<tr><th rowspan='2'>N&#176;</th><th rowspan='2'>ID PK</th><th rowspan='2'>Hotel</th><th rowspan='2'>T. Habitacion</th>";
	$head2 = "<tr>";
	foreach ($period as $key => $value) {
		$htmlresult.="<th colspan='4' class='w3-center'>".$value->format('Y-m-d')."</th>";
		$head2.= "<th>Sgl</th><th>Twin</th><th>Mat</th><th>Tpl</th>";
	}
	$htmlresult.="</tr>".$head2."</tr>";
	$htmlresult .="</thead><tbody>";


	$hotcount = 0;
	while(!$hoteles->EOF){
		if(isset($dispo[$hoteles->Fields('id_pk')]) && $hoteles->Fields('es_global')==0){
			$hotcount++;
			$habcount = count($dispo[$hoteles->Fields('id_pk')]);
			$counter = 0;
			$htmlresult.="<tr>
			<td rowspan='$habcount'>".$hotcount."</td>
			<td rowspan='$habcount'>".$hoteles->Fields('id_pk')."</td>
			<td rowspan='$habcount'>".$hoteles->Fields('hot_nombre')."</td>";

			foreach($dispo[$hoteles->Fields('id_pk')] as $id_tipohabitacion => $noches){
				$htmlresult.="<td>".$dispo[$hoteles->Fields('id_pk')][$id_tipohabitacion]['th_nombre']."</td>";
				$thnombre = $dispo[$hoteles->Fields('id_pk')][$id_tipohabitacion]['th_nombre'];
				unset($dispo[$hoteles->Fields('id_pk')][$id_tipohabitacion]['th_nombre']);
				$daycount = 0;
				foreach($period as $key => $value){
					if($daycount % 2 == 0){
						$color = "w3-light-blue";
					}else{
						$color = "";
					}

					if(isset($noches[$value->format('Y-m-d')])){
						$title = array();						
						$closed = false;
						for($i=1; $i <=4 ; $i++){
							$varname = "color$i";
							$$varname = $color;
							if($noches[$value->format('Y-m-d')]["cerrado$i"]==1){
								$$varname = $closedColor;
								$closed = true;
								switch($i){
									case 1:
										array_push($title, "Sgl Cl");
									break;
									case 2:
										array_push($title, "Twin Cl");
									break;
									case 3:
										array_push($title, "Mat Cl");
									break;
									case 4:
										array_push($title, "Tpl Cl");
									break;
								}
							}

							if($hoteles->Fields('modo_global')==1 && $i == 4){
								$colorEnjoy = $color;
								if($closed){
									$colorEnjoy = $warningColor;
								}
								if($noches[$value->format('Y-m-d')]['minnoc']>1){
									array_push($title, "MN: ".$noches[$value->format('Y-m-d')]['minnoc']);
								}
								$htmlresult.="<td colspan='4' class='w3-center $colorEnjoy' title='".$hoteles->Fields('hot_nombre')." ".$thnombre." ".$value->format('Y-m-d')." ".implode("-", $title)."'>".$noches[$value->format('Y-m-d')]['dhab1']."</td>";
							}elseif ($hoteles->Fields('modo_global')==0){
								if($noches[$value->format('Y-m-d')]['minnoc']>1 && !$closed){
									$$varname = $warningColor;
									if(count($title)<1){
										array_push($title, "MN: ".$noches[$value->format('Y-m-d')]['minnoc']);
									}
								}
								$htmlresult.="<td class='w3-center ".$$varname."' title='".$hoteles->Fields('hot_nombre')." ".$thnombre." ".$value->format('Y-m-d')." ".$title[0]."'>".$noches[$value->format('Y-m-d')]["dhab$i"]."</td>";
							}
						}
					}else{
						$htmlresult.="<td colspan='4' class='w3-center $closedColor' title='".$hoteles->Fields('hot_nombre')." ".$thnombre." ".$value->format('Y-m-d')."'>------</td>";
					}
					$daycount++;
				}
				
				$htmlresult.="</tr>";
				$counter++;
				if($counter!=$habcount){
					$htmlresult.="<tr>";
				}
			}
		}

		$hoteles->MoveNext();
	}
	$htmlresult.="</tbody>";
	die(utf8_encode($htmlresult));

}

//include("superior.php");
?>


<html>
	<head>
		<title><?=$cotClass->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../../otas/css/w3.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../otas/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
		<script src="../../otas/js/jquery-3.2.1.min.js"></script>
		<script src="../../otas/js/jquery-ui/jquery-ui.js"></script>
		<script src="../../otas/js/MainJs.js"></script>
		<style>
			html, body {
			    height: 100%;
			    font-family: "Inconsolata", sans-serif;
			}
			input[type=date]::-webkit-inner-spin-button {
    			-webkit-appearance: none;
    			display: none;
			}
			.modal {
			    display:    none;
			    position:   fixed;
			    z-index:    1000;
			    top:        0;
			    left:       0;
			    height:     100%;
			    width:      100%;
			    background: rgba( 157, 248, 252, 0.3 ) 
			    url('http://i.stack.imgur.com/FhHRx.gif') 
			    50% 50% 
			    no-repeat;
			}
			body.loading {
			    overflow: hidden;   
			}
			body.loading .modal {
			    display: block;
			}



		</style>
		<script type="text/javascript">
			function getDispo(){
				$.ajax({
					type: 'POST',
					url: 'global_report.php',
					data:{
						getDispo:'true',
						id_pk: $("#id_pk").val(),
						fechad: $("#fechad").val(),
						fechah: $("#fechah").val(),
						ciudad: $("#ciudad").val()
					},
					beforeSend:function(){
						$body = $("body");
						$body.addClass("loading");
					},
					complete:function(){
						$body.removeClass("loading");
					},
					success:function(result){
						$("#lista").html(result);
						$body.removeClass("loading");	


					},
					error:function(){
						alert("Error al traer los datos.");
					}
				});
			}
			function hotlist(){
				$("#id_pk [city!="+$("#ciudad").val()+"]").hide();
				$("#id_pk [city="+$("#ciudad").val()+"]").show();
				$("#id_pk [value=0]").show();
				
			}
			$(document).ready(function(){
				hotlist();
				$("#ciudad").change(function(){
					hotlist();
				});
			});



		</script>
	</head>
	<body> 
		<div id='container' class="w3-sand w3-grayscale" style='height:100%'>
			<div class='w3-content'>
				<table class='w3-table w3-bordered w3-small'>
					<td>
						<label>Hotel</label>
						<select name='id_pk' id='id_pk' class='w3-select'>
							<option value='0'>-=Todos=-</option>
							<?
							if($hoteles->RecordCount()>0){
								while(!$hoteles->EOF){
									if($hoteles->Fields("es_global")==0){
										echo "<option city='".$hoteles->Fields('id_ciudad')."' value='".$hoteles->Fields('id_pk')."'>".utf8_encode($hoteles->Fields('hot_nombre'))."</option>";
									}
									$hoteles->MoveNext();
								}
							}

							?>
						</select>
					</td>
					<td>
						<label><?=ucfirst($lanClass->ciudad);?></label>
						<select name='ciudad' id='ciudad' class="w3-select">
							<?
								$cities = $cotClass->getCities($db1, true,$cotClass->nationalCountryID);
								while(!$cities->EOF){
									echo "<option value='".$cities->Fields('id_ciudad')."' ".(($cities->Fields('id_ciudad')==102)?'SELECTED':'').">".utf8_encode($cities->Fields('ciu_nombre'))."</option>";
									$cities->MoveNext();
								}
							?>
						</select>


					</td>
					<td>
						<label><?=ucfirst($lanClass->fecha_desde);?></label>
						<input id='fechad' name='fechad' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d'); ?>"/>
					</td>
					<td>
						<label><?=ucfirst($lanClass->fecha_hasta);?></label>
						<input id='fechah' name='fechah ' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d'); ?>"/>
					</td>
					<td><button id='btn_search' name='btn_search' onClick='getDispo()'>Buscar</button></td>
					
				</table>
			</div>
			<div class="w3-sand">
				<div class="w3-content">
					<table class='w3-table w3-small'>
						<tr>
							<td class='<?=$closedColor;?>'>Cerrado</td>
							<td class='<?=$warningColor;?>'>Con restricciones | *Cl = Hab cerrada | MN = Min. Noches</td>
						</tr>
					</table>
					<table class='w3-table w3-small w3-responsive' id='lista' border='1' style="height: 80%"></table>
				</div>
			</div>
		</div>
		<div class="modal"><!-- div para el loading --></div>
	</body>
</html>