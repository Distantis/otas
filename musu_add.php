<?php	 	  
require_once("clases/usuario.php");
require_once("clases/agencia.php");
$usuario = new Usuario();
$agencia = new Agencia();
//Connection statemente
require_once('Connections/db1.php');

$permiso=106;
require('secure.php');

require_once('fun_select.php');


if(isset($_GET['id_usuario'])){
	$findUser = $usuario->getMeThatUser($db1,$_GET['id_usuario']);
	if($_SESSION['Usuario']->id_tipo!=1 && $_SESSION['Usuario']->id_agencia != $findUser->Fields('id_agencia')){
		die("Las agencias no coinciden");
	}
}
if(isset($_POST['inserta']) || isset($_POST['modifica'])) {
	if(isset($_POST['correo_send'])){
		$usu_enviar_correo= $_POST['correo_send'];
	}else{
		$usu_enviar_correo= 0;	
	}
	if(isset($_POST['usu_portal'])){
		$usuario_portal = 0;
	}else{
		$usuario_portal = 1;
	}
	
	$dat = array(
		array(
			"usu_rut"	=>		$_POST['rut'],
			"usu_login"	=>		$_POST['login'],
			"usu_password"	=>		$usuario->codigo_rand(),
			"usu_nombre"	=>		$_POST['nombre'],
			"usu_pat"	=>		$_POST['paterno'],
			"usu_mat"	=>		$_POST['materno'],
			"usu_codigo" => $_POST['usucode'],
			"usu_estado"		=>		0,
			"usu_mail"	=>		$_POST['mail'],
			"usu_enviar_correo"		=>		$usu_enviar_correo,
			"ve_password"		=>		0,
			"usu_portal"		=>		$usuario_portal,
			"id_idioma"		=>		$_POST['idioma'],
			"id_tipo"		=>		$_POST['id_tipousuario'],
			"id_hotel"		=>		0,
			"id_agencia"		=>	$_POST['id_agencia'],
			"id_area"		=>		$_POST['id_area']
			)
		);
	if(isset($_POST['inserta'])){
		$id_usuario = $usuario->AgregarUsuarios($db1,$dat);
	}else{
		$data = $dat[0];
		$data['usu_password'] = $findUser->Fields('usu_password');
		$id_usuario = $_POST['id_usuario'];
		$usuario->modificaUsuario($db1,$data, $id_usuario);
	}
	$vars['txt_id'] = $id_usuario;
	$agencia->redirectByPost('musu_search.php', $vars=null,$msgalert='');
}

$tipo = $agencia->GetTipoUsuario($db1,false);
$areas = $agencia->getAreas($db1,false);
$idiomas = $agencia->getLanguages($db1,false);
$agencias =  $agencia->getMeAgencies($db1,true);

?>
<html>

<head>
	<title><?php echo $agencia->nombre_plataforma;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/MainJs.js"></script>
	<link href="css/test.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form method="post" id="form" name="form">
	<table class='mainstream'>
		<th colspan="2" id="thtitulo"><div align="center"><?=((isset($findUser)?'Modifica ':'Nuevo '));?>Usuario</div></th>
		<?if(isset($_GET['id_usuario'])){ echo "<input type='hidden' name='id_usuario' value='".$_GET['id_usuario']."'>";}?>
		<tr>
	  		<th>Area: </th>
	  		<td>
			  	<select name="id_area" id="id_area">
				  	<?php
				  	while(!$areas->EOF){
				  		echo "<option value='".$areas->Fields('id_area')."'";
				  		if(isset($findUser)){
					  		if($areas->Fields('id_area')==$findUser->Fields('id_area')){
					  			echo " SELECTED";
					  		}
				  		}
				  		echo ">".$areas->Fields('area_nombre')."</option>";
						$areas->MoveNext();  	
				  	}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Nombre Usuario :</th>
			<td><input type="text" name="nombre" onChange="M(this)" value='<?if(isset($findUser)){echo $findUser->Fields('usu_nombre');}?>' required /></td>
		</tr>
		<tr>
			<th>Apellido Paterno :</th>
			<td><input type="text" name="paterno"  size="30" onChange="M(this)" value='<?if(isset($findUser)){echo $findUser->Fields('usu_pat');}?>' required /></td>
		</tr>
		<tr>
			<th>Apellido Materno :</th>
			<td><input type="text" name="materno"  size="30" onChange="M(this)" value='<?if(isset($findUser)){echo $findUser->Fields('usu_mat');}?>' /></td>
		</tr>
		<tr>
			<th>Rut :</th>
			<td><input type="text" name="rut"  size="30" onChange="M(this)" value='<?if(isset($findUser)){echo $findUser->Fields('usu_rut');}?>' /></td>
		</tr>
		<tr>
			<th>Codigo Interno:</th>
			<td><input type='text' name='usucode' size='11' onChange='M(this)' value='<?if(isset($findUser)){echo $findUser->Fields('usu_codigo');}?>'/></td>
		</tr>
		<tr>
			<th>Idioma :</th>
			<td>
				<select name="idioma"  id="idioma" >
					<?php
					while(!$idiomas->EOF) {
						echo "<option value='".$idiomas->Fields('id_idioma')."'";
						if(isset($findUser)){
					  		if($idiomas->Fields('id_idioma')==$findUser->Fields('id_idioma')){
					  			echo " SELECTED";
					  		}
				  		}
						echo ">".utf8_encode($idiomas->Fields('idioma'))."</option>";
						$idiomas->MoveNext();
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<th>E-Mail :</th>
			<td>
				<input type="text" name="mail" id="mail" value='<?if(isset($findUser)){echo $findUser->Fields('usu_mail');}?>' required/>&nbsp;
				<input type="checkbox" name="chk_si" value="1"> &nbsp;Enviar copia mail de usuario nuevo. | 
				<input name="correo_send" type="checkbox" value="1" checked> &nbsp;Recibir correos.
			</td>
		</tr>

		<tr>
			<th>Usuario :</th>
			<td><input type="text" name="login"  size="20" onChange="M(this)" value='<?if(isset($findUser)){echo $findUser->Fields('usu_login');}?>' required /></td>
		</tr>

		<tr>
			<th>Tipo de Usuario :</th>
			<td>
				<select name="id_tipousuario">
					<?php	 	  
					while(!$tipo->EOF){

						if($_SESSION['Usuario']->id_tipo==1 ||($_SESSION['Usuario']->id_tipo!=1 && $tipo->Fields('id_tipousuario')!=1)){
							echo "<option value='".$tipo->Fields('id_tipousuario')."'";
							if(isset($findUser)){
						  		if($tipo->Fields('id_tipousuario')==$findUser->Fields('id_tipo')){
						  			echo " SELECTED";
						  		}
					  		}
							echo ">".$tipo->Fields('tu_nombre')."</option>";
						}
						$tipo->MoveNext();
					}
					?>
				</select>
				<input type="checkbox" name="usu_portal" value="1" 
					<?
					if(isset($findUser)){
						if($findUser->Fields('usu_portal')==0){
							echo "checked";
						}
					}else{
						echo "checked";
					}
					?>
				>Usuario Portal
			</td>
		</tr>
		<tr>
			<th>Empresa Principal :</th>
			<td>
				<span class="nombreusuario">
					<select id="id_agencia" name="id_agencia">
						<?
						while(!$agencias->EOF){
							echo "<option value='".$agencias->Fields('id_agencia')."'";
							if(isset($findUser)){
						  		if($agencias->Fields('id_agencia')==$findUser->Fields('id_agencia')){
						  			echo " SELECTED";
						  		}
					  		}
							echo ">".$agencias->Fields('ag_nombre')."</option>";
							$agencias->MoveNext();
						}

						?>
					</select>
				</span>
			</td>
		</tr>
		<tr>
			<th colspan="2" align="right" nowrap>&nbsp;</th>
		</tr>
	</table>
	<br>
	<center>
		<button name="<?=((isset($_GET['id_usuario']))?'modifica':'inserta');?>" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
		<button name="cancela" type="button" style="width:100px; height:27px" onClick="window.location.href='musu_search.php';">&nbsp;Cancelar</button>
	</center>
</form>

<p>&nbsp;</p>
</body>
</html>