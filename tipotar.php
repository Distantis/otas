<?
require_once('clases/distantis.php');
require_once('Connections/db1.php');
$usuario = $_SESSION['Usuario'];
$permiso = 401;
require('secure.php');
//require_once('includes/functions.inc.php');

$distantis = new distantis();
function stateClassChooser($state){
	if(!$state){
		return 'activo';
	}else{
		return 'inactivo';
	}
}

if(isset($_POST['inserta'])){
	$id = $distantis->SetTipoTarifa($db1,$_POST['newname'],$_POST['newcode'], $_POST['newstate'],$_POST['newsell'],$_POST['newpromo'],$_POST['newmax'],$_POST['newmin']);
	$distantis->insLog($db1,$usuario->id_usuario, $permiso, $id);
}
if(isset($_POST['mod'])){ 

	$distantis->updateTipoTarHotel($db1,$_POST['newval']['tname'],$_POST['newval']['tcode'],$_POST['newval']['tstate'],$_POST['newval']['tsell'],$_POST['newval']['tpromo'],$_POST['newval']['tmax'],$_POST['newval']['tmin'],$_POST['ttid']);
	
	$newTr ="<th>".$_POST['ttid']."</th>
		<td>".$_POST['newval']['tname']."</td>
		<td>".$_POST['newval']['tcode']."</td>
		<td class='".stateClassChooser($_POST['newval']['tstate'])."'>".($_POST['newval']['tstate'] ==0 ? 'Activa':'Inactiva')."</td>
	    <td class='".stateClassChooser($_POST['newval']['tsell'])."'>".($_POST['newval']['tstate'] ==0 ? 'Si':'No')."</td>
        <td class='".stateClassChooser(!$_POST['newval']['tpromo'])."'>".($_POST['newval']['tpromo'] ==1 ? 'Si':'No')."</td>
        <td>".$_POST['newval']['tmin']."</td>
        <td>".$_POST['newval']['tmax']."</td>
		<td>
			<img src='images/editar.png' alt='Editar Información' class='imgBtn' onclick='editaTipoTar(".$_POST['ttid'].")'>
			&nbsp;
			<a href='tipotar_mod.php?id=".$_POST['ttid']."'>
				<img src='images/lupa.png' border='0' alt='Editar Asignadas'>
			</a>
		</td>";
		die($newTr);
}


$datos = $distantis->GetTipoTarifa($db1,false);
?>
<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8">
		<link href="css/test.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/MainJs.js"></script>
		<script type="text/javascript">
			var artt = {};
			<?
			$tableHtml="";
			while(!$datos->EOF){
				?>
				artt[<?=$datos->Fields('id_tipotarifa')?>] = {};
				artt[<?=$datos->Fields('id_tipotarifa')?>]['name'] = '<?=$datos->Fields('tt_nombre')?>';
				artt[<?=$datos->Fields('id_tipotarifa')?>]['code'] = '<?=$datos->Fields('tt_codcts')?>';
				artt[<?=$datos->Fields('id_tipotarifa')?>]['state'] = <?=$datos->Fields('tt_estado')?>;
				artt[<?=$datos->Fields('id_tipotarifa')?>]['sell'] = <?=$datos->Fields('tt_sellotas')?>;
				artt[<?=$datos->Fields('id_tipotarifa')?>]['promo'] = <?=$datos->Fields('tt_espromo')?>;
				artt[<?=$datos->Fields('id_tipotarifa')?>]['min'] = <?=$datos->Fields('tt_minnoc')?>;
				artt[<?=$datos->Fields('id_tipotarifa')?>]['max'] = <?=$datos->Fields('tt_maxnoc')?>;
				<?


				$tableHtml.="<tr id='tr".$datos->Fields('id_tipotarifa')."'>
					<th>".$datos->Fields('id_tipotarifa')."</th>
					<td>".$datos->Fields('tt_nombre')."</td>
					<td>".$datos->Fields('tt_codcts')."</td>
					<td class='".stateClassChooser($datos->Fields('tt_estado'))."'>".$datos->Fields('tt_estadot')."</td>
				    <td class='".stateClassChooser($datos->Fields('tt_sellotas'))."'>".$datos->Fields('tt_sellotast')."</td>
			        <td class='".stateClassChooser(!$datos->Fields('tt_espromo'))."'>".$datos->Fields('tt_espromot')."</td>
			        <td>".$datos->Fields('tt_minnoc')."</td>
			        <td>".$datos->Fields('tt_maxnoc')."</td>
					<td>
						<img src='images/editar.png' alt='Editar Información' class='imgBtn' onclick='editaTipoTar(".$datos->Fields('id_tipotarifa').")'>
						&nbsp;
						<a href='tipotar_mod.php?id=".$datos->Fields('id_tipotarifa')."'>
							<img src='images/lupa.png' border='0' alt='Editar Asignadas'>
						</a>
						</td>
					</tr>";
				$datos->MoveNext(); 
			}?>

			function propSwitcher(idtt, way, btn){
				var aux = $("#t"+way).val()==0 ? 1 : 0;
				$("#t"+way).val(aux) ;
				if(btn.className.indexOf('btnActivo')!= -1){
					btn.className= btn.className.replace('btnActivo', 'btnInactivo');
				}else if(btn.className.indexOf('btnInactivo')){
					btn.className= btn.className.replace('btnInactivo', 'btnActivo');
				}
				if(way=='state'){
					if(btn.value == 'Activa'){
						btn.value = 'Inactiva';
					}else{
						btn.value='Activa';
					}
				}else{
					if(btn.value=='Si'){
						btn.value='No';
					}else{
						btn.value='Si';
					}
				}
			}

			function cancelar(){
				if($("#tredit").isset){
					if(confirm("¿Desea descartar los cambios en el tipo de tarifa?")){
						$("#tredit").remove();
					}
				}
			} 
			function guardar(ttid){
				var conf = confirm("¿Desea proceder con los cambios?");
				if(!conf){
					return false;
				}
				var values = {};
				$("#tredit input:not([type='button'])").each(function(){
					values[this.id] = this.value;
				});
				
				$.ajax({
					type: 'POST',
					url: 'tipotar.php',
					data: {
						mod: true,
						newval: values,
						ttid: ttid
					},
					success:function(result){
						
						alert("Cambios realizados con exito");
						$.each(values, function(index, valor){
							artt[ttid][index.substring(1,index.length)] = valor;
						});
						$("#tredit").remove();
						$("#tr"+ttid).html(result);
							
						
					},
					error:function(){
						alert("Error al actualizar los datos");
					}
				});
			}

			function editaTipoTar(varid){
				
				if($("#tredit").isset()){
					var ask4sure = confirm("¿Desea descartar los cambios en el otro tipo de tarifa?");
					if(ask4sure){
						$("#tredit").remove();
					}
				}

				var bodytr = "<tr id='tredit'><th>--</th>";
				bodytr+="<td><input type='text' id='tname' onChange='M(this)' value='"+artt[varid]['name']+"'></td>";
				bodytr+="<td><input type='text' id='tcode' value='"+artt[varid]['code']+"'></td>";
				bodytr+="<td><input type='button' onClick='propSwitcher("+varid+",\"state\", this)' class=";
				if(artt[varid]['state']==0){
					bodytr+="'btnEstados btnActivo' value='Activa'";
				}else{
					bodytr+="'btnEstados btnInactivo' value='Inactiva'";
				}
				bodytr+="></td>";
				bodytr+="<td><input type='button' id='btnstt' onClick='propSwitcher("+varid+",\"sell\", this)' class="+detBtnClass(artt[varid]['sell'])+"></td>";
				bodytr+="<td><input type='button' onClick='propSwitcher("+varid+",\"promo\", this)' class="+detBtnClass(!artt[varid]['promo'])+"></td>";

				bodytr+="<td><input type='text' id='tmin' value='"+artt[varid]['min']+"'></td>";
				bodytr+="<td><input type='text' id='tmax' value='"+artt[varid]['max']+"'></td>";
				bodytr+="<td><img src='images/save.gif' alt='Guardar' class='imgBtn' onclick='guardar("+varid+")'>&nbsp;";
				bodytr+="<img src='images/Delete.png' alt='Cancelar' class='imgBtn' onclick='cancelar()'>";
				bodytr+="<input type='hidden' id='tstate' value='"+artt[varid]['state']+"'>";
				bodytr+="<input type='hidden' id='tsell' value='"+artt[varid]['sell']+"'>";
				bodytr+="<input type='hidden' id='tpromo' value='"+artt[varid]['promo']+"'></td></tr>";
				$("#tr"+varid).after(bodytr);
			}

			function detBtnClass(state){
				if(state==0){
					return "'btnEstados btnActivo' value='Si'";
				}else{
					return "'btnEstados btnInactivo' value='No'";
				}
			}
		</script>
	</head>
	<body>
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
			<br><br><br>
			<table class='mainstream'>
				<tr>
					<th  id='thtitulo' colspan="4"><div align="center">Nuevo Tipo Tarifa</div></th>
				</tr>
				<tr>
					<th>Nombre Tipo Tarifa:</th>
					<td><input type="text" name="newname" onChange="M(this)" required/></td>
					<th>Codigo Interno</th>
					<td><input type="text" name="newcode" onChange="M(this)"/></td>
				</tr>
				<tr>
					<th>Estado</th>
					<td><select name='newstate'><option value='0'>Activa</option><option value='1'>Inactiva</option></select></td>
					<th>Vender</th>
					<td><select name='newsell'><option value='0'>Si</option><option value='1'>No</option></select></td>
				</tr>
				<tr>
					<th>Es Promoción</th>
					<td><select name='newpromo'><option value='1'>Si</option><option value='0'>No</option></select></td>
					<th>Minimo Noches</th>
					<td><input type="text" name="newmin" value='0' onChange="M(this)" required/></td>
				</tr>
				<tr>
					<th>Máximo Noches</th>
					<td><input type="text" name="newmax" value='0'required/></td>
				</tr>
				<tr><th colspan="4"></th></tr>
				<tr><td colspan="4"></td></tr>
			</table>
			<br>
		 		<center>
		 			<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
					<button name="buscar" type="button" onClick="window.location='tipotar.php'" style="width:100px; height:27px">Cancelar</button>
		 		</center>
 		</form>
		<br><br><br>
		<table class='listaBoni'>
			<tr>
				<th id='thtitulo' colspan="9"><div align="center">Lista Tipo Tarifas</div></th>
			</tr>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Codigo</th>
				<th>Estado</th>
				<th>Vender</th>
				<th>Es Promo</th>
				<th>Min Noc</th>
				<th>Max Noc</th>
				<th>Accion</th>
			</tr>
			<?=$tableHtml;?>
		</table>
	</body>
</html>




<?/*<td><input type='button' onClick='stateSwitcher(<?=$datos->Fields('id_tipotarifa');?>, this)' class=
			            <?
			            if($datos->Fields('tt_estado') == 0){
			              echo "'btnEstados btnActivo' value='Activa'";
			            }else{
			              echo "'btnEstados btnInactivo' value='Inactiva'";
			            }?>
			          ></td>
			        <td><input type='button' onClick='sellingSwitcher(<?=$datos->Fields('id_tipotarifa');?>, this)' class=
			            <?
			            if($datos->Fields('tt_sellotas') == 0){
			              echo "'btnEstados btnActivo' value='Si'";
			            }else{
			              echo "'btnEstados btnInactivo' value='No'";
			            }?>
			          >
			        </td>
			        <td>
			        	<input type='button' onClick='promoSwitcher(<?=$datos->Fields('id_tipotarifa');?>, this)' class=
				            <?
				            if($datos->Fields('tt_espromo') == 1){
				              echo "'btnEstados btnActivo' value='Si'";
				            }else{
				              echo "'btnEstados btnInactivo' value='No'";
				            }?>
			         	 >
			        </td>*/?>