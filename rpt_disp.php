<?php
//require_once('clases/distantis.php');
require_once('clases/agencia.php');
require_once('clases/usuario.php');
require_once('Connections/db1.php');

$userClass= new Usuario();
$agencia = new Agencia();
require('secure_opaco.php');
$id_agencia = $_SESSION["idagencia"];
$tipousu = $_SESSION["idtipo"];
$areausu = $_SESSION["idarea"];
if($tipousu==1){
	$hoteles = $agencia->hoteles_paisociudad($db1);
}else{
	$hoteles = $agencia->getAllHotsXagency($db1,$id_agencia, true, null, true, false);
}
$areas = $agencia->getAreas($db1);
$tipotars = $agencia->GetTipoTarifa($db1);
$ciudades = $agencia->getCities($db1);

$tipousu = (isset($_GET["tipo"]))?$_GET["tipo"]:$tipousu;
//$rpt = $agencias->rptMatrix($db1, $ciudad, $fechad, $fechah, $tipousu, $id_agencia, $habs, $byhotel, $id_hotel=null, $id_tipotarifa=null, $extranjero=null);
echo "tipo $tipousu --- area: $areausu";
?>
<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8">
		<link href="css/test.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="js/jquery-ui/jquery-ui.css">
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/jquery-ui/jquery-ui.js"></script>
		<script src="js/MainJs.js"></script>
		<script type="text/javascript">			
			var tipousu = <?php echo json_encode($tipousu); ?>;
			if(tipousu==1){
				var minfecha = "";
			}else{
				var minfecha = new Date(<? echo date('Y');?>, <? echo date('m');?> - 1, <? echo date('d');?>);
			}			
			$(function(){
			    var dates = $("#txt_desde").datepicker({
			   		changeMonth: true,
			   		changeYear:true,
			     	numberOfMonths: 1,
			     	dateFormat: 'dd-mm-yy',
			     	showOn: "button",
					minDate:minfecha,
			      	buttonText: '...'
			    });	
			});
			$(function(){
			    var dates = $("#txt_hasta").datepicker({
			   		changeMonth: true,
			   		changeYear:true,
			     	numberOfMonths: 1,
			     	dateFormat: 'dd-mm-yy',
			     	showOn: "button",
					minDate:minfecha,
			      	buttonText: '...'
			    });	
			});
			function addcity(id, nombre){
				var select = document.getElementById("cb_multp_ciudad[]");
				$("#cb_multp_ciudad[] option").each(function(){
					
				});
				var option = document.createElement('option');
				option.value = id;
				option.text = nombre;				
				select.add(option,0);
			}
		</script>
	</head>
	<body>
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
			<br><br><br>
			<table class='mainstream'>
				<tr>
					<th  id='thtitulo' colspan="4"><div align="center">Filtrar reporte</div></th>
				</tr>
				<tr>
					<th style="width: 90px">Hotel:</th>
					<td>
						<select id="cb_hotel" name="cb_hotel" style="width: 150px">
							<option value="0"> Seleccione </option>
							<?php 
								while(!$hoteles->EOF){
									$selected =($_POST["id_hotel"]==$hoteles->Fields("id_hotel"))?"selected":"";
									echo "<option value=".$hoteles->Fields("id_hotel")." $selected>".utf8_encode($hoteles->Fields("hot_nombre"))."</option>";
									$hoteles->MoveNext();
								}
							?>
						</select>
					</td>
					<th style="width: 90px">Area:</th>
					<td>
						<?php 
							$bloqueo = ($tipousu!=1)?"disabled":"";
							$elcombo= '<select id="cb_area" name="cb_area" style="width: 150px" '.$bloqueo.'>
								<option value="0"> Seleccione </option>';
							while(!$areas->EOF){
								$selected =($areausu==$areas->Fields("id_area"))?"selected":"";
								$elcombo.= "<option value=".$areas->Fields("id_area")." $selected>".$areas->Fields("area_nombre")."</option>";
								$areas->MoveNext();
							}
							$elcombo.='</select>';
							echo $elcombo;
						?>						
					</td>
				</tr>
				<tr> 
					<th>Desde:</th> 
					<td><input type="text" id="txt_desde" name="txt_desde" style="width:150px;" value="<?php if(isset($_POST['txt_desde']))echo $_POST['txt_desde'];?>"></td> 
					<th>Hasta:</th>
					<td><input type="text" id="txt_hasta" name="txt_hasta" style="width:150px;" value="<?php if(isset($_POST['txt_hasta']))echo $_POST['txt_hasta']; ?>"></td>
				</tr>
				<tr  >
					<th <?php if($tipousu!=1) echo "hidden"; ?>>Tipotarifa:</th>
					<td <?php if($tipousu!=1) echo "hidden"; ?>>
						<select id="cb_tipotar" name="cb_tipotar" style="width: 150px">
							<option value="0"> Seleccione </option>
							<?php 
								while(!$tipotars->EOF){
									$selected =($_POST["cb_tipotar"]==$tipotars->Fields("id_tipotarifa"))?"selected":"";
									echo "<option value=".$tipotars->Fields("id_tipotarifa")." $selected>".$tipotars->Fields("tt_nombre")."</option>";
									$tipotars->MoveNext();
								}
							?>
						</select>
					</td>
					<th >Ciudad:</th>
					<td >
						<select id="cb_ciudad" name="cb_ciudad" onchange=addcity(this.value,this.options[this.selectedIndex].text) style="width: 150px">
							<option value="0"> Seleccione </option>
							<?php 
								while(!$ciudades->EOF){
									$selected =($_POST["cb_ciudad"]==$ciudades->Fields("id_ciudad"))?"selected":"";
									echo "<option value=".$ciudades->Fields("id_ciudad")." $selected >".utf8_encode($ciudades->Fields("ciu_nombre"))."</option>";
									$ciudades->MoveNext();
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<th>Multiples ciudades</th>
					<td>
						<select multiple id="cb_multp_ciudad[]" ondblclick="quitar_ciudad(this.value)" name="cb_multp_ciudad[]" style="width:150px;">
						<?php for($i=0; $i<count($_POST['cb_multp_ciudad']); $i++) {
							$trae_nombre_city_sql ="SELECT * FROM ciudad WHERE id_ciudad =".$_POST['cb_multp_ciudad'][$i];
							$trae_nombre_city = $db1->SelectLimit($trae_nombre_city_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							echo "<option value=".$_POST['cb_multp_ciudad'][$i].">".$trae_nombre_city->Fields('ciu_nombre')."</option>";
						 } ?>  
						</select>
					</td>
				</tr>
			</table>
			<br>
		 		<center>
		 			<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
					<button name="buscar" type="button" onClick="window.location='rpt_disp.php'" style="width:100px; height:27px">Cancelar</button>
		 		</center>
 		</form>
		<br><br><br>
	</body>
</html>