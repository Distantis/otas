<?php
require_once('Connections/db1.php');
require_once('secure.php');
require_once('clases/distantis.php');

$general = new Distantis();

?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<META HTTP-EQUIV="Expires" CONTENT="0">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="js/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" href="css/font-awesome.min.css">
<title><?=$general->nombre_plataforma?></title>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="3%" align="center">
    	<i class="fa fa-user-o" style="font-size:20px"></i>
    </td>
    <td colspan="2">
    	<span class="bienvenido">Bienvenido </span>
    	<?
    	if(isset($_SESSION['Usuario'])){
    		echo "<span class='nombreusuario'><b>".$_SESSION['Usuario']->usu_nombre." </b></span>";
    		if($_SESSION['Usuario']->id_tipo == 1){
				  echo "<a href='manager.htm' target='_parent'>Administrador</a>&nbsp;";
    		}
    	}
		  ?>
		  <a href='compra_opaca.php' target='_parent'>Reservar</a>&nbsp;
			<a href='historial_reservas.php' target='_parent'>Reservas Realizadas</a>&nbsp;
			<a href='global_report.php' target='blank'>Dispo global</a>&nbsp;
      <a href="logout.php">Salir</a>
    </td>
  </tr>
  <tr>
    <td colspan="4"><img src="images/separa.png" width="5" height="5"></td>
  </tr>
</table>
</body></html>


