<?php
require_once('Connections/db1.php');
require_once('clases/agencia.php');
$agencia = new Agencia();

$permiso = 105;


if(isset($_POST['id_agencia'])){
  $id_agencia = $_POST['id_agencia'];
  $permiso = 305;
}
if(isset($_GET['id_agencia'])){
  $id_agencia = $_GET['id_agencia'];
  $permiso = 305;
}
require_once('secure.php');
if(isset($_POST['inserta'])){
$db1->debug=true; 


  if(isset($id_agencia)){
    $agencia->updateAgen($db1,$id_agencia,$_POST);
    $agencia->insAgencyLog($db1,$_SESSION['Usuario']->id_usuario, $id_agencia, 'Actualiza agencia');
  }else{
    $id_agencia = $agencia->insertAgen($db1,$_POST);
    $agencia->insAgencyLog($db1,$_SESSION['Usuario']->id_usuario, $id_agencia, 'Ingresa nueva agencia');
  }

$db1->debug=true;
  if($_FILES["img"]['name']!=""){
    $ruta = $agencia->uploadImage($db1,$_FILES, $id_agencia, "logos");
    $agencia->updateAgencyImage($db1, $id_agencia, $ruta);  
  }
  if($_POST['chk_push']==0){
    $estadoinfo = 0;
  }else{
    $estadoinfo = 1;
  }
  $agencia->managePushInfo($db1, $_POST['push_user'],$_POST['push_pass'], $_POST['push_receptor'], $_POST['push_inyector'], $id_agencia,$estadoinfo);
	KT_redir("mope_search.php");	
}
$pais = $agencia->getCountrys($db1);
$grupo = $agencia->getGroups($db1);
if(isset($id_agencia)){
  $data_agencia = $agencia->GetMeThatAgency($db1, $id_agencia);
  if($data_agencia->RecordCount()==0){
    unset($data_agencia);
  }
}



?>

<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="css/test.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/MainJs.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        switchPushView();
        $("[name=chk_push]").change(function(){
          switchPushView();
        });
      });

      function switchPushView(){
        if($("[name=chk_push]:checked").val()==1){
          $("#infopushtab").hide(500);
          $("#infopushtab input").prop('required',false);
        }else{
          $("#infopushtab").show(500);
          $("#infopushtab input").prop('required',true);
        }
      }

      

    </script>
		
	</head>
	<body>
		
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
  			<table class='mainstream'>
          <?if(isset($data_agencia)){
            echo "<input type='hidden' name='id_agencia' id='id_agencia' value='".$id_agencia."'>";
            $title="Edita Comprador";
          }else{
            $title="Nuevo Comprador";
          }?>
    			<th colspan="4" id='thtitulo'><div align="center"><?=$title?></div></th>
          
    			<tr>
    				<th>Nombre:</th>
    				<td><input type="text" name="txt_nombre" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_nombre');}?>" size="20" onChange="M(this)" required/></td>
    				<th>Codigo Comprador:</th>
    				<td><input type="text" name="txt_codcts" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_code');}?>" size="20" onChange="M(this)"/></td>
    			</tr>
    			<tr>
    				<th>Pais:</th>
    				<td>
              <select name="id_pais" id="id_pais">
    				    <?
						    while(!$pais->EOF){
						    ?>
    					    <option value="<?=$pais->Fields('id_pais')?>"<?if(isset($data_agencia)){if($pais->Fields('id_pais') == $data_agencia->Fields('id_pais')){echo "SELECTED";}}?>><?=$pais->Fields('pai_nombre')?></option>
        					<?
    						  $pais->MoveNext();
  						  }
  						  $pais->MoveFirst();
						    ?>
      				</select>
      			</td>
			      <th>Fono:</th>
			      <td><input type="text" name="txt_fono" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_fono');}?>" size="20" onChange="M(this)" /></td>
			    </tr>
    			<tr>
    				<th>Rut:</th>
    				<td><input type="text" name="txt_rut" value="<?if(isset($data_agencia)){ echo $data_agencia->Fields('ag_rut');}?>" size="20" onChange="M(this)" /></td>
    	      <th>Categoria Comisión:</th>
      			<td>
      				<select name="id_grupo">
        				<? 
                while(!$grupo->EOF){ ?>
          			  <option value="<?= $grupo->Fields('id_grupo') ?>"<?if(isset($data_agencia)){if($data_agencia->Fields('id_grupo')==$grupo->Fields('id_grupo')){?> selected<? }}?>><?=$grupo->Fields('gru_nombre') ?></option>
        					<? $grupo->MoveNext();
        				} ?>
            	</select>
          	</td>
   				</tr>
          <tr>
            <th>Razón:</th>
            <td><input type='text' name='txt_razon' value='<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_razon');}?>' onChange='M(this)'/></td>
            <th>Giro:</th>
            <td><input type='text' name='txt_giro' value='<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_giro');}?>' onChange='M(this)'/></td>
          </tr>
          <tr>
            <th>Dirección:</th>
            <td><input type='text' name='txt_direccion' value='<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_direccion');}?>' onChange='M(this)'/></td>
            <th>Email:</th>
            <td><input type='text' name='txt_email' value='<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_email');}?>' onChange='M(this)'/></td>
          </tr>
			    <tr>
			      	<th>Antelación de compra para Hotel:</th>
			      	<td><input type="number" name="txt_reshot" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_diasrestriccion_hot');}?>" size="10" onChange="M(this)" required/></td>
			      	<th>Antelación de compra para servicios:</th>
			      	<td><input type="number" name="txt_resserv" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_diasrestriccion_serv');}?>" size="10" onChange="M(this)" required/></td>
			    </tr>
			    <tr>
			      	<th>Markup Comprador:</th>
			      	<td><input type="text" name="txt_markup" value="<?if(isset($data_agencia)){echo $data_agencia->Fields('ag_markup');}else{echo "1";}?>" size="10" onChange="M(this)" required/></td>
			      	<th>Markup Imperativo:</th>
			      	<td colspan="3">
                <input type="radio" name="chk_mkimp" value="1" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_mark_imp')==1){echo "checked";}}else{echo checked;}?>>NO
                <input type="radio" name="chk_mkimp" value="0" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_mark_imp')==0){echo "checked";}}?>>SI</td>
			    </tr>
    			<tr>
    			  	<th>Anula Post Fecha Anulación:</th>
    			  	<td>
                <input type="radio" name="chk_anuesp" value="1" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_espanula')==1){echo "checked";}}else{echo checked;}?>>NO
                <input type="radio" name="chk_anuesp" value="0" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_espanula')==0){echo "checked";}}?>>SI</td>
    			  <th>Cobra Noshow en anulación post fecha limite:</th>
    			  <td>
              <input type="radio" name="chk_chargeNoShow" value="1" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_cobrarnoshow')==1){echo "checked";}}else{echo checked;}?>>NO
              <input type="radio" name="chk_chargeNoShow" value="0" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_cobrarnoshow')==0){echo "checked";}}?>>SI</td>
    			</tr>
    			<tr>
  			    <th>Es push:</th>
    			  <td>
              <input type="radio" name="chk_push" value="1" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_espush')==1){echo "checked";}}else{echo checked;}?>>NO
              <input type="radio" name="chk_push" value="0" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_espush')==0){echo "checked";}}?>>SI
            </td>
            <th>Aplica markup en contratación directa:</th>
            <td>
              <input type="radio" name="chk_applymkdir" value="1" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_applymkdir')==1){echo "checked";}}else{echo checked;}?>>NO
              <input type="radio" name="chk_applymkdir" value="0" <?if(isset($data_agencia)){if($data_agencia->Fields('ag_applymkdir')==0){echo "checked";}}?>>SI
            </td>
          </tr>

          <tr>  
            <td colspan="4">
              <input type="file" name="img" id="img"></br>
              <?
              if(isset($data_agencia)){ 
               if(trim($data_agencia->Fields('ag_logo')) != ""){ ?>
                <img src="images/<?=$data_agencia->Fields('ag_logo')?>" alt="" width="150" height="100"/>
              <? } }?>
            </td>

          </tr>
          <tr>
            <td colspan='4'><?
              if(isset($data_agencia)){
                ?>
                <button name="buscar" type="button" onClick="window.location='hotxmope.php?id_agencia=<?=$id_agencia?>'">hoteles autorizados</button>
                <button name="buscar" type="button" onClick="window.location='wsxmope.php?id_agencia=<?=$id_agencia?>'" >Webservices</button>
                <button name="buscar" type="button" onClick="window.location='agecredit.php?id_agencia=<?=$id_agencia?>'" >Credito</button>
                <?
              }
              ?>

              
              </td>
    			</tr>
  			</table>
  			<br>

        <table id='infopushtab' class='mainstream' visible='false'>
          <th colspan="4" id='thtitulo'><div align="center">Información Push</div></th>
          <tr>
            <th>User:</th>
            <td><input type='text' name='push_user' id='push_user' value='<?if(isset($data_agencia)){ echo $data_agencia->Fields('user');}?>'></td>
            <th>Pass:</th>
            <td><input type='text' name='push_pass' id='push_pass' value='<?if(isset($data_agencia)){ echo $data_agencia->Fields('pass');}?>'></td>
          </tr>
          <tr>
            <th>Url Inyector: </th>
            <td><input type='text' name='push_inyector' id='push_inyector' value='<?if(isset($data_agencia)){ echo $data_agencia->Fields('url_inyector');}?>'></td>
            <th>Url Receptor: </th>
            <td><input type='text' name='push_receptor' id='push_receptor' value='<?if(isset($data_agencia)){ echo $data_agencia->Fields('url_receptor');}?>'></td>
          </tr>
        </table>
        <br>
	 		<center>
	 			<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
	    	<button name="buscar" type="button" onClick="window.location='mope_search.php'" style="width:100px; height:27px">Cancelar</button>&nbsp;
	 		</center>
		</form>
	</body>
</html>
