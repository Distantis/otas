<?php
require_once(dirname(__FILE__) . '/distantis_desarrollo.php');


class Tarifa extends distantis{
	//esta shit nos dice si admitimos que se carguen tarifas que tienen el IVA incluido (wnes como turavion cargan venta con iva incluido)
	private $allowIvaIncluded = false;
	//con esta shit le decimos si valida o no que el valor de la tarifa en cualquier habitación sea mayor a $minValueAllowed
	private $validateMinValue = true;
	//si $validateMinValue esta activo, solo permite tarifas cuyo valor final en cualquiera de sus habitaciones sea mayor a lo indicado aqui (usd)
	private $minValueAllowed = 30;
	private $minClpValueAllowed = 10000;
	//esta shit decide si se checkea la dispo en tarHandler
	public $checkDispo = true;
	//le dice a tarhandler si debe imprimir alguna wea o no.
	public $talkToME = false;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//constructor... aqui se inicializan todas las shits que no se puedan declarar como constantes arriba... (arrays y otros inventos weones).
	function __construct(){   
    	$this->clientes = array();
    	
	}
	function setMinValueAllowed($val){
		$this->minValueAllowed = $val;
	}
	function setClpMinValueAllowed($val){
		$this->minClpValueAllowed = $val;
	}
	function setValidateMinValue($val){
		$this->validateMinValue = $val;
	}
	function setAllowIvaIncluded($val){
		$this->allowIvaIncluded = $val;
	}

	function getMinValueAllowed(){
		return $this->minValueAllowed;
	}
	function getClpMinValueAllowed(){
		return $this->minClpValueAllowed;
	}
	function getValidateMinValue(){
		return $this->validateMinValue;
	}
	function getAllowIvaIncluded(){
		return $this->allowIvaIncluded;
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	



	//inserta el log de la tarifa
	public function InsHotdetLog($db1, $idHotdetGandhi, $descripcion = ''){
		
		try {
			$this->opDebug(__FUNCTION__);
	    	$INSlogTars = "INSERT INTO ".$this->dbMain.".log_hotdet
	    		(id_hotdet,hd_fecdesde, hd_fechasta, hd_sgl, hd_dbl, hd_tpl, hd_estado, idpk_tipotarifa, idpk_tipohabitacion, hd_iva, hd_markup, hd_usamarkup, id_cliente, max_sgl, max_dbl, max_tpl, id_hotel, id_regimen, id_area, id_mon, id_hotdet_cliente, hd_sgl_cli, hd_dbl_cli, hd_tpl_cli, log_descripcion, hd_prepago, hdlog_fecha)
				SELECT 
				id_hotdet,hd_fecdesde, hd_fechasta, hd_sgl, hd_dbl, hd_tpl, hd_estado, idpk_tipotarifa, idpk_tipohabitacion, hd_iva, hd_markup, hd_usamarkup, id_cliente, max_sgl, max_dbl, max_tpl, id_hotel, id_regimen, id_area, id_mon, id_hotdet_cliente, hd_sgl_cli, hd_dbl_cli, hd_tpl_cli, '".$descripcion."', hd_prepago, NOW()
				FROM ".$this->dbMain.".hotdet WHERE id_hotdet = $idHotdetGandhi";
	    	$db1->Execute($INSlogTars) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	    	$this->clDebug();
	    	return true;
		} catch (Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//desactiva una tarifa
	public function TarDeactivator($db1, $hotdetgandhi = null, $lockDispo = false, $hotdetcli=null, $id_cliente=null){
		
		try{
			$this->opDebug(__FUNCTION__);
			$arConds = array();
			if($hotdetgandhi!=null){
				array_push($arConds, "id_hotdet = ".$hotdetgandhi);
			}
			if($hotdetcli!=null){
				array_push($arConds, "id_hotdet_cliente = ".$hotdetcli);	
			}
			if($id_cliente!=null){
				array_push($arConds, "id_cliente = ".$id_cliente);
			}

			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}

			$SQLupHotdet = "UPDATE ".$this->dbMain.".hotdet SET hd_estado = 1 ".$cond;
			$db1->Execute($SQLupHotdet) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->InsHotdetLog($db1,$hotdetgandhi, "Desactiva Tarifa");
			if($lockDispo){
				$this->dispoDeactivator($db1, $hotdetgandhi);
			}
			$this->clDebug();
		}catch (Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//desactiva la dipos de una tarifa
	public function dispoDeactivator($db1, $hotdetgandhi, $hd_cliente = null, $id_cliente = null){
		
		try{
			$this->opDebug(__FUNCTION__);
			$join = "";
			$cond = "";
			if($hotdetgandhi!=null){
				$cond = "id_hotdet = ".$hotdetgandhi;
			}else{
				if($hdCliente!=null && $id_cliente!=null){
					$join = " INNER JOIN ".$this->dbMain.".hotdet hd ON hd.id_hotdet = ds.id_hotdet";
					$cond = " id_hotdet_cliente = ".$hd_cliente." AND id_cliente = ".$id_cliente;
				}else{
					echo "<br>No se ingresaron parametros correctos. - dispoDeactivator";
					$this->clDebug();
					return false;
				}
			}
			$SQLupDispo = "UPDATE ".$this->dbMain.".dispo ds $join SET ds_estado = 1 WHERE ".$cond;
			$db1->Execute($SQLupDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//busca tarifas en el cliente que sea indicado. A excepción del id_cliente, todos los campos son opcionales, si las fechas no son indicadas, trae solo aquellas cuya vigencia no haya expirado.
	public function TarHandler($db1, $id_cliente, $hotdet_cli = null, $pkhot= null, $hdsgl= null, $hddbl= null, $hdtpl= null, $fdesde= null, $fhasta = null, $ttpk= null, $thpk = null, $mon=null, $hdestado=null){
		ob_start();
		try {
			$this->opDebug(__FUNCTION__);
			$hdsgl =($hdsgl==null)?null:$this->intvalidator($hdsgl);
			$hddbl =($hddbl==null)?null:$this->intvalidator($hddbl);
			$hdtpl =($hdtpl==null)?null:$this->intvalidator($hdtpl);
			$cliente = $this->getMeThatClient($db1, $id_cliente);
			if($cliente['estado']==1){
				$SQLgetTars = "SELECT * FROM ".$this->dbMain.".hotdet WHERE id_cliente = $id_cliente AND hd_estado = 0";
				$RESgetTars = $db1->Execute($SQLgetTars) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

				while(!$RESgetTars->EOF){
					$UPDtars = "UPDATE ".$this->dbMain.".hotdet SET hd_estado = 1 WHERE id_hotdet = ".$RESgetTars->Fields('id_hotdet');
					$db1->Execute($UPDtars) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					$this->InsHotdetLog($db1, $RESgetTars->Fields('id_hotdet'),'Desactiva tarifa - Cliente desactivado');
					$RESgetTars->MoveNext();
				}
			}else{
				$bdcli = $cliente['bd'];
				$namecli = $cliente['name'];
				$strExtraConds = "";
				//columnas especiales que deben ser consideradas por cliente... nemo y esas shits...
				if(isset($cliente['esColls']['hotdet'])){
					$extraConds = array();
					foreach ($cliente['esColls']['hotdet'] as $col => $val){
						$arConds = array();
						$strAux = "(";
						$nums = array();
						foreach ($val as $index=> $valorValido){
							if($valorValido==''){
								array_push($arConds, "IFNULL(".$col.",'') = ''");
							}else{
								if(is_numeric($valorValido)){
									array_push($nums, $valorValido);
								}else{
									array_push($arConds, $col." = '".$valorValido."'");
								}
							}
						}
						if(count($nums)>0){
							array_push($arConds, $col." in (".implode(",", $nums).")");
						}
						$strAux.= implode(" OR ",$arConds);
						$strAux.= ") ";
						array_push($extraConds, $strAux);
					}
					$strExtraConds="if(".implode(" AND ", $extraConds).", 'S','N') as hd_valida,";
				}else{
					$strExtraConds="'S' as hd_valida,";
				}

				$calclines = $this->getCalcLines($db1,$id_cliente);
				$strExtraConds = str_replace("id_cliente", "hd.id_cliente", $strExtraConds);
				//traemos las tarifas dependiendo de los parametros que se le pasen a la función					
				$SQLgetTars = "SELECT 
					$strExtraConds
					hd.id_hotdet, 
					hd.hd_fecdesde,
					hd.hd_fechasta,
					hd.hd_mon,
					IFNULL(hd.hd_sgl, 0) as hd_sgl,
					IFNULL(hd.hd_dbl,0) as hd_dbl,
					IFNULL(hd.hd_tpl,0) as hd_tpl,
					hd.hd_estado,
					hd.id_area,
					hg.id_hotel as hot_gandhi,
					hdg.id_hotel as hot_targ,
					hg.id_ciudad as ciu_gandhi,
					ciu.aplica_iva,
					hg.hot_estado as hot_estadog,
					hm.id_pk,
					tt.id_tipotar as tt_gandhi,
					th.id_tipohab as th_gandhi,
					tt.tt_estado, 
					h.hot_estado,
					h.hot_activo,
					h.hot_sellota,
					hd.hd_iva,
					hdg.id_hotdet as id_hotdetg,
					h.prepago,
					htt.tt_aplicamkotas
				FROM $bdcli.hotdet hd 
					INNER JOIN $bdcli.tipohabitacion th ON hd.id_tipohabitacion = th.id_tipohabitacion
					INNER JOIN $bdcli.hotel h ON hd.id_hotel = h.id_hotel
					INNER JOIN ".$this->dbhot.".hotelesmerge hm ON hd.id_hotel = hm.id_hotel_$namecli
					INNER JOIN $bdcli.tipotarifa tt ON hd.id_tipotarifa = tt.id_tipotarifa
					INNER JOIN ".$this->dbhot.".tipotarifa htt ON tt.id_tipotar = htt.id_tipotarifa
					LEFT JOIN ".$this->dbMain.".hotel hg ON hm.id_pk = hg.id_pk
					LEFT JOIN ".$this->dbMain.".ciudad ciu ON hg.id_ciudad = ciu.id_ciudad
					LEFT JOIN ".$this->dbMain.".hotdet hdg ON hd.id_hotdet = hdg.id_hotdet_cliente AND hdg.id_cliente = $id_cliente
					WHERE hd.id_area IS NOT NULL AND hd.hd_mon IS NOT NULL";
					
				
				if($pkhot!=null){
					$SQLgetTars.=" AND hm.id_pk = $pkhot";	
				}
				if($hdsgl!=null){
					$SQLgetTars.=" AND hd.hd_sgl = $hdsgl";
				}
				if($hddbl!=null){
					$SQLgetTars.=" AND hd.hd_dbl = $hddbl";
				}
				if($hdtpl!=null){
					$SQLgetTars.=" AND hd.hd_tpl = $hdtpl";
				}
				$fecsGiven = false;
				if($fdesde!=null){
					$SQLgetTars.=" AND hd.hd_fecdesde = '$fdesde'";
					$fecsGiven=true;
				}
				if($fhasta!=null){
					$SQLgetTars.=" AND hd.hd_fechasta = '$hasta'";
					$fecsGiven=true;	
				}
				if(!$fecsGiven){
					//hice el date_format y me importo una raja la opinion publica (ups).F.G.
					$SQLgetTars.=" AND (hd.hd_fechasta >= DATE_FORMAT(NOW(), '%Y-%m-%d') OR (hdg.hd_fechasta > NOW() AND hdg.hd_fechasta <> hd.hd_fechasta))";
				}
				if($ttpk!=null){
					$SQLgetTars.=" AND tt.id_tipotar = $ttpk";
				}
				if($thpk!=null){
					$SQLgetTars.=" AND th.id_tipohab = $thpk";
				}
				if($mon!=null){
					$SQLgetTars.=" AND hd.hd_mon = $mon";
				}
				if($hdestado!=null){
					$SQLgetTars.=" AND hd.hd_estado = $hdestado";
				}
				$SQLgetTars.=" ";

				if($hotdet_cli!=null){
					$SQLgetTars.="AND hd.id_hotdet = $hotdet_cli";
				}

				$SQLgetTars.=" ORDER BY hg.id_hotel ASC, hd.hd_mon ASC";
				$RESgetTars = $db1->Execute($SQLgetTars) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


				while(!$RESgetTars->EOF){
					$desactivarTar = false;
					//validamos que respete las reglas basicas
					if($RESgetTars->Fields('tt_gandhi')!='' AND $RESgetTars->Fields('th_gandhi')!='' AND $RESgetTars->Fields('hot_gandhi')!='' AND $RESgetTars->Fields('hot_estado')==0 AND $RESgetTars->Fields('hot_activo')==0 AND $RESgetTars->Fields('tt_estado')==0 AND $RESgetTars->Fields('hot_estadog')==0 AND $RESgetTars->Fields('hot_sellota')==0){
						//solo si esta activa hacemos los calculos de valor que correspondan.
						
						if($RESgetTars->Fields('hd_estado')==0 && $RESgetTars->Fields('hd_valida')=='S'){
							$apply = true;
							if($RESgetTars->Fields('hd_mon')==2){
								if($RESgetTars->Fields('hd_iva')==1 && !$this->allowIvaIncluded){
									$apply = false;
								}
							}

							$minValue = ($RESgetTars->Fields('hd_mon')==1)?$this->minValueAllowed : $this->minClpValueAllowed;
							///////////////////////////////////////////////////////////////////////////////////////////////////
							//////esto es para los imbeciles que ponen 1 o similares en triple cuando no tienen tarifa...//////
							///////////////////////////////////////////////////////////////////////////////////////////////////

							if($this->validateMinValue){
								$hd_sgl = (($RESgetTars->Fields('hd_sgl') < $minValue)?0:$RESgetTars->Fields('hd_sgl'));
								$hd_dbl = (($RESgetTars->Fields('hd_dbl') < ($minValue/2))?0:$RESgetTars->Fields('hd_dbl'));
								$hd_tpl = (($RESgetTars->Fields('hd_tpl') < ($minValue/3))?0:$RESgetTars->Fields('hd_tpl'));
								//validacion de valor minimo permitido
								if(($hd_sgl < $minValue && $hd_dbl < ($minValue/2) && $hd_tpl < ($minValue/3))){
									$apply = false;
								}
							}else{
								$hd_sgl = $RESgetTars->Fields('hd_sgl');
								$hd_dbl = $RESgetTars->Fields('hd_dbl');
								$hd_tpl = $RESgetTars->Fields('hd_tpl');
							}
							///////////////////////////////////////////////////////////////////////////////////////////////////
							///////////////////////////////////////////////////////////////////////////////////////////////////

							//validacion de lineas de calculo 
							if(!isset($calclines[$id_cliente]['hotocu'][$RESgetTars->Fields('hd_mon')]) || !isset($calclines[$id_cliente]['cotdes'][$RESgetTars->Fields('hd_mon')])){
								$apply = false;
							}
							//si la tarifa aun es candidata a venderse en Gandhi, se calcula el resto, de lo contrario se marca para eliminación/ignorar
							if($apply){
								if($RESgetTars->Fields('hd_mon')==2 && $RESgetTars->Fields('hd_iva')==0 && $RESgetTars->Fields('aplica_iva')==0){
									$aplicaiva = 0;
								}else{
									$aplicaiva = 1;
								}
								if($RESgetTars->Fields('tt_aplicamkotas')==0){
									$markup = $cliente['mk'][$RESgetTars->Fields('hd_mon')];
								}else{
									$markup = 1;
								}

								if($hd_sgl>0){
									$hd_sgl = $this->evalCalcLines($markup,$hd_sgl, 1, $aplicaiva, 1, $calclines[$id_cliente]['hotocu'][$RESgetTars->Fields('hd_mon')]);
									$hd_sgl = $this->evalCalcLines($markup,$hd_sgl, 1, $aplicaiva, 1, $calclines[$id_cliente]['cotdes'][$RESgetTars->Fields('hd_mon')]);
								}
								if($hd_dbl>0){
									$hd_dbl = $this->evalCalcLines($markup,$hd_dbl, 1, $aplicaiva, 1, $calclines[$id_cliente]['hotocu'][$RESgetTars->Fields('hd_mon')]);
									$hd_dbl = $this->evalCalcLines($markup,$hd_dbl, 1, $aplicaiva, 1, $calclines[$id_cliente]['cotdes'][$RESgetTars->Fields('hd_mon')]);
								}
								if($hd_tpl>0){
									$hd_tpl = $this->evalCalcLines($markup,$hd_tpl, 1, $aplicaiva, 1, $calclines[$id_cliente]['hotocu'][$RESgetTars->Fields('hd_mon')]);
									$hd_tpl = $this->evalCalcLines($markup,$hd_tpl, 1, $aplicaiva, 1, $calclines[$id_cliente]['cotdes'][$RESgetTars->Fields('hd_mon')]);
								}
								$insertar = false;
								
								if($RESgetTars->Fields('id_hotdetg')!=''){
									if($RESgetTars->Fields('hot_gandhi')!=$RESgetTars->Fields('hot_targ')){
										$this->TarDeactivator($db1, $RESgetTars->Fields('id_hotdetg'));
										$insertar = true;
									}else{
										if($this->talkToME){
											echo "<br>UPD->  hdg:".$RESgetTars->Fields('id_hotdetg');
										}
										$SQLupTar = "UPDATE ".$this->dbMain.".hotdet SET 
											idpk_tipotarifa = ".$RESgetTars->Fields('tt_gandhi').",
											idpk_tipohabitacion = ".$RESgetTars->Fields('th_gandhi').",
											hd_fecdesde = '".$RESgetTars->Fields('hd_fecdesde')."',
											hd_fechasta = '".$RESgetTars->Fields('hd_fechasta')."',
											hd_sgl = $hd_sgl,
											hd_dbl = $hd_dbl,
											hd_tpl = $hd_tpl,
											hd_sgl_cli = ".$RESgetTars->Fields('hd_sgl').",
											hd_dbl_cli = ".$RESgetTars->Fields('hd_dbl').",
											hd_tpl_cli = ".$RESgetTars->Fields('hd_tpl').",
											id_mon = ".$RESgetTars->Fields('hd_mon').",
											hd_iva = ".$RESgetTars->Fields('hd_iva').",
											hd_estado = 0,
											hd_prepago = ".$RESgetTars->Fields('prepago');
										$SQLupTar.=" WHERE id_hotdet = ".$RESgetTars->Fields('id_hotdetg');			
										$db1->Execute($SQLupTar) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
										$this->InsHotdetLog($db1, $RESgetTars->Fields('id_hotdetg'),'Actualiza Tarifa - TarHandler');
										if($this->checkDispo){
											$this->dispoChecker($db1, $id_cliente, $RESgetTars->Fields('id_pk'), $RESgetTars->Fields('id_hotdet'));
										}else{
											$sqlupdis ="UPDATE ".$this->dbMain.".dispo SET ds_estado = 1 WHERE (ds_fecha < '".$RESgetTars->Fields('hd_fecdesde')."' OR ds_fecha > '".$RESgetTars->Fields('hd_fechasta')."') AND id_hotdet = ".$RESgetTars->Fields('prepago');
											$db1->Execute($sqlupdis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
										}
									}
								}else{
									$insertar = true;
								}
								
								if($insertar){
									if($this->talkToME){
										echo "<br>NEW-> client: $id_cliente -> hd: ".$RESgetTars->Fields('id_hotdet');
									}
									$SQLinsTar = "INSERT INTO ".$this->dbMain.".hotdet 
										(hd_fecdesde, hd_fechasta, hd_sgl, hd_dbl, hd_tpl, hd_estado, idpk_tipotarifa, idpk_tipohabitacion,  id_cliente, id_hotel,  id_area, id_mon, id_hotdet_cliente, hd_sgl_cli, hd_dbl_cli, hd_tpl_cli, hd_iva, hd_prepago)
										VALUES ( '".$RESgetTars->Fields('hd_fecdesde')."','".$RESgetTars->Fields('hd_fechasta')."',".$hd_sgl.", ".$hd_dbl.", ".$hd_tpl.", 0, ".$RESgetTars->Fields('tt_gandhi').",".$RESgetTars->Fields('th_gandhi').", $id_cliente, ".$RESgetTars->Fields('hot_gandhi').",".$RESgetTars->Fields('id_area').",".$RESgetTars->Fields('hd_mon').",".$RESgetTars->Fields('id_hotdet').", ".$RESgetTars->Fields('hd_sgl').", ".$RESgetTars->Fields('hd_dbl').", ".$RESgetTars->Fields('hd_tpl').",".$RESgetTars->Fields('hd_iva').", ".$RESgetTars->Fields('prepago').")";
									$db1->Execute($SQLinsTar) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
									$this->InsHotdetLog($db1, $db1->Insert_ID() ,'Ingresa Tarifa - TarHandler');
									$this->dispoCreator($db1, $id_cliente, $RESgetTars->Fields('id_hotdet'));
								}
							}else{
								$desactivarTar = true;
							}
						}else{
							$desactivarTar = true;
						}
					}else{
						$desactivarTar = true;
					}
					if($desactivarTar && $RESgetTars->Fields('id_hotdetg')!=''){
						if($this->talkToME){
							echo "<br>DEL-> hdg:".$RESgetTars->Fields('id_hotdetg');
						}
						$this->TarDeactivator($db1, $RESgetTars->Fields('id_hotdetg'),false);
					}
					$RESgetTars->MoveNext();
				}
			}
			$this->clDebug();
			ob_end_clean();
			return true;
		} catch (Exception $e) {
			var_dump($e);
			$this->clDebug();
		}
	}
	//setea la dispo de una tarifa indicada para un cliente entre las fechas dadas.
	public function dispoUpdater($db1,$hotdetCliente, $idcliente, $fechaDesde, $fechaHasta = null, $sgl = null, $twin =null, $mat = null, $tpl=null, $minimoNoches=null, $estado=null, $cerrado=null, $bloqueado=null, $cerradosgl=null, $cerradotwn=null, $cerradomat=null,$cerradotpl=null){
		ob_start();
		try{
			$this->opDebug(__FUNCTION__);
			$sgl =$this->intvalidator($sgl);
			$twin =$this->intvalidator($twin);
			$mat =$this->intvalidator($mat);
			$tpl =$this->intvalidator($tpl);
			if($sgl!==null && $twin!==null && $mat!==null && $tpl!==null && $estado!==null && $cerrado!==null){ 
				throw new Exception("Debe indicar disponibilidad para al menos una habitación o el estado");
			}
			$SQLupDispo = "UPDATE ".$this->dbMain.".dispo ds INNER JOIN ".$this->dbMain.".hotdet hd ON ds.id_hotdet = hd.id_hotdet SET ";
			$setconds = array();
			if($sgl!==null){
				array_push($setconds, " ds_hab1 = ".$sgl);
			}
			if($twin!==null){
				array_push($setconds," ds_hab2 = ".$twin);
			}
			if($mat!==null){
				array_push($setconds," ds_hab3 = ".$mat);	
			}
			if($tpl!==null){
				array_push($setconds," ds_hab4 = ".$tpl);
			}
			if($minimoNoches!==null){
				array_push($setconds," ds_minnoche = ".$minimoNoches);
			}
			if($estado!==null){
				array_push($setconds," ds_estado = ".$estado);
			}
			if($cerrado!==null){
				array_push($setconds," ds_cerrado = ".$cerrado);
			}
			if($bloqueado!==null){
				array_push($setconds," ds_tarifa = ".$bloqueado);
			}
			if($cerradosgl!==null){
				array_push($setconds," ds_cerrado_sgl = ".$cerradosgl);
			}
			if($cerradotwn!==null){
				array_push($setconds," ds_cerrado_twn = ".$cerradotwn);
			}
			if($cerradomat!==null){
				array_push($setconds," ds_cerrado_mat = ".$cerradomat);
			}
			if($cerradotpl!==null){
				array_push($setconds," ds_cerrado_tpl = ".$cerradotpl);
			}
			$finalCond = implode(",", $setconds);
			$SQLupDispo.= $finalCond;
			$SQLupDispo.=" WHERE hd.id_cliente = ".$idcliente." AND hd.id_hotdet_cliente = ".$hotdetCliente;
			if($fechaHasta!=null){
				$SQLupDispo.=" AND ds_fecha >= '".$fechaDesde."' AND ds_fecha <= '".$fechaHasta."'";
			}else{
				$SQLupDispo.=" AND ds_fecha = '".$fechaDesde."'";
			}
			
			
			$db1->Execute($SQLupDispo);
			$this->clDebug();
			ob_end_clean();
		}catch (Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//valida si existe el stock y lo inserta o updatea segun corresponda, según lo que hay en la BBDD
	public function dispoChecker($db1, $id_cliente, $pk_hotel, $hotdetCliente=null){
		try{
			$this->opDebug(__FUNCTION__);

			$cliente = $this->getMeThatClient($db1,$id_cliente);

			$SQLgetDispo="SELECT 
				hd.id_hotdet, 
				sc_hab1 - (IFNULL(SUM(hc_hab1),0)) AS dispo1,
				sc_hab2 - (IFNULL(SUM(hc_hab2),0)) AS dispo2,
				sc_hab3 - (IFNULL(SUM(hc_hab3),0)) AS dispo3,
				sc_hab4 - (IFNULL(SUM(hc_hab4),0)) AS dispo4,
				sc_fecha,
				sc_estado,
				sc_cerrado,
				sc_minnoche,
				sc_tarifa,
				WEEKDAY(sc_fecha) as dia,
				hd_diadesde,
				hd_diahasta,
				id_stock,
				hm.ver as es_global,
				hm.mira as mixto,
				sc_cerrado_sgl,
				sc_cerrado_twn,
				sc_cerrado_dbl,
				sc_cerrado_tpl
				FROM ".$cliente['bd'].".hotdet hd 
				INNER JOIN ".$cliente['bd'].".stock s ON hd.id_hotdet = s.id_hotdet
				INNER JOIN ".$this->dbhot.".hotelesmerge hm ON hd.id_hotel = hm.id_hotel_".$cliente['name']."
				LEFT JOIN ".$cliente['bd'].".hotocu hc ON s.id_hotdet = hc.id_hotdet AND hc.hc_fecha = s.sc_fecha and hc_estado = 0
				WHERE hm.id_pk = ".$pk_hotel;
			if($hotdetCliente!=null){
				$SQLgetDispo.=" AND hd.id_hotdet = ".$hotdetCliente;
			}
			$SQLgetDispo.=" GROUP BY hd.id_hotdet, sc_fecha";
			$RESgetDispo = $db1->Execute($SQLgetDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			while(!$RESgetDispo->EOF){
				$SQLchkExist = "SELECT * FROM ".$this->dbMain.".hotdet hd
					LEFT JOIN ".$this->dbMain.".dispo ds ON hd.id_hotdet = ds.id_hotdet 
					WHERE id_cliente = ".$id_cliente." AND id_hotdet_cliente = ".$RESgetDispo->Fields('id_hotdet')." AND ds_fecha = '".$RESgetDispo->Fields('sc_fecha')."'";
				$RESchkExist = $db1->Execute($SQLchkExist) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				if($RESgetDispo->Fields('dispo1')<0 || $RESgetDispo->Fields('es_global')==0){
					$dispo1 = 0;
				}else{
					$dispo1 =$RESgetDispo->Fields('dispo1');
				}
				if($RESgetDispo->Fields('dispo2')<0 || $RESgetDispo->Fields('es_global')==0){
					$dispo2 = 0;
				}else{
					$dispo2 =$RESgetDispo->Fields('dispo2');
				}
				if($RESgetDispo->Fields('dispo3')<0 || $RESgetDispo->Fields('es_global')==0){
					$dispo3 = 0;
				}else{
					$dispo3 =$RESgetDispo->Fields('dispo3');
				}
				if($RESgetDispo->Fields('dispo4')<0 || $RESgetDispo->Fields('es_global')==0){
					$dispo4 = 0;
				}else{
					$dispo4 =$RESgetDispo->Fields('dispo4');
				}
				$estado = $RESgetDispo->Fields('sc_estado');
				if($RESgetDispo->Fields('dia')< ($RESgetDispo->Fields('hd_diadesde') -1) || $RESgetDispo->Fields('dia')> ($RESgetDispo->Fields('hd_diahasta') -1)){
					$estado = 1;
				}

				if($RESchkExist->Fields('id_dispo')!=null){
					$SQLupDispo = "UPDATE ".$this->dbMain.".dispo SET ds_hab1 = $dispo1, ds_hab2 = $dispo2, ds_hab3 = $dispo3, ds_hab4 = $dispo4, 
						ds_estado = ".$estado.", ds_cerrado = ".$RESgetDispo->Fields('sc_cerrado').", ds_minnoche = ".$RESgetDispo->Fields('sc_minnoche').",
						id_stock_cli = ".$RESgetDispo->Fields('id_stock')."
						WHERE id_dispo = ".$RESchkExist->Fields('id_dispo');
					$db1->Execute($SQLupDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
				}else{
					$SQLupDispo="INSERT INTO ".$this->dbMain.".dispo (ds_hab1, ds_hab2, ds_hab3, ds_hab4, ds_estado, ds_cerrado, ds_tarifa, ds_minnoche, ds_fecha, id_hotdet, id_stock_cli, ds_cerrado_sgl, ds_cerrado_twn, ds_cerrado_mat, ds_cerrado_tpl) VALUES(
						$dispo1,
						$dispo2,
						$dispo3,
						$dispo4,
						".$estado.",
						".$RESgetDispo->Fields('sc_cerrado').",
						".$RESgetDispo->Fields('sc_tarifa').",
						".$RESgetDispo->Fields('sc_minnoche').",
						'".$RESgetDispo->Fields('sc_fecha')."',
						".$RESgetDispo->Fields('id_hotdet').",
						".$RESgetDispo->Fields('id_stock').",
						".$RESgetDispo->Fields('sc_cerrado_sgl').",
						".$RESgetDispo->Fields('sc_cerrado_twn').",
						".$RESgetDispo->Fields('sc_cerrado_dbl').",
						".$RESgetDispo->Fields('sc_cerrado_tpl').")";
					$db1->Execute($SQLupDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
				$RESgetDispo->MoveNext();
			}
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	//elimina la dispo de la base de datos.
	public function dispoDeleter($db1, $id_cliente, $hot_pk, $fecd, $fech=null, $hd_cliente=null){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLdelDispo = "DELETE d FROM ".$this->dbMain.".dispo d 
				INNER JOIN ".$this->dbMain.".hotdet hd ON d.id_hotdet = hd.id_hotdet
				INNER JOIN ".$this->dbMain.".hotel h ON hd.id_hotel = h.id_hotel
				WHERE h.id_pk = ".$hot_pk." AND hd.id_cliente = ".$id_cliente;
			if($hd_cliente!=null){
				$SQLdelDispo.=" AND hd.id_hotdet_cliente = ".$hd_cliente;
			}
			if($fech!=null){
				$SQLdelDispo.=" AND ds_fecha >= '".$fecd."' AND ds_fecha <= '".$fech."'";
			}else{
				$SQLdelDispo.=" AND ds_fecha = '".$fecd."'";
			}
			$db1->Execute($SQLdelDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch (Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//crea la dispo sin validar ni mierda segun lo que haya en la BBDD
	public function dispoCreator($db1, $id_cliente, $hd_cliente){
		try{
			$this->opDebug(__FUNCTION__);
			$cliente = $this->getMeThatClient($db1,$id_cliente);
			$SQLcreateDispo = "INSERT INTO ".$this->dbMain.".dispo (ds_fecha, ds_hab1, ds_hab2, ds_hab3, ds_hab4, ds_estado, ds_cerrado, ds_tarifa, id_hotdet, id_stock_cli, ds_cerrado_sgl, ds_cerrado_twn, ds_cerrado_mat, ds_cerrado_tpl)
				SELECT 
					sc_fecha, 
					if((((sc_hab1 - (IFNULL(SUM(hc_hab1),0)))<0) || hm.ver = 0),0, sc_hab1 - (IFNULL(SUM(hc_hab1),0))) AS dispo1,
					if((((sc_hab2 - (IFNULL(SUM(hc_hab2),0)))<0) || hm.ver = 0),0, sc_hab2 - (IFNULL(SUM(hc_hab2),0))) AS dispo2,
					if((((sc_hab3 - (IFNULL(SUM(hc_hab3),0)))<0) || hm.ver = 0),0, sc_hab3 - (IFNULL(SUM(hc_hab3),0))) AS dispo3,
					if((((sc_hab4 - (IFNULL(SUM(hc_hab4),0)))<0) || hm.ver = 0),0, sc_hab4 - (IFNULL(SUM(hc_hab4),0))) AS dispo4,
					sc_estado,
					sc_cerrado,
					sc_tarifa,
					hd.id_hotdet,
					id_stock,
					sc_cerrado_sgl,
					sc_cerrado_twn,
					sc_cerrado_dbl,
					sc_cerrado_tpl
				FROM 
				".$this->dbMain.".hotdet hd 
				INNER JOIN ".$cliente['bd'].".stock s ON hd.id_hotdet_cliente = s.id_hotdet
				LEFT JOIN ".$cliente['bd'].".hotocu hc ON hd.id_hotdet_cliente = hc.id_hotdet AND s.sc_fecha = hc.hc_fecha and hc_estado = 0
				LEFT JOIN ".$this->dbMain.".hotel h ON hd.id_hotel = h.id_hotel 
				LEFT JOIN ".$this->dbhot.".hotelesmerge hm ON h.id_pk = hm.id_pk
				WHERE hd.id_hotdet_cliente = ".$hd_cliente." AND id_cliente = ".$id_cliente." GROUP BY s.id_hotdet, sc_fecha";

			$db1->Execute($SQLcreateDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//reduce la dispo para la fecha o las fechas indicadas.
	public function dispoReducer($db1, $id_cliente, $sgl=0, $twin=0, $mat=0, $tpl=0, $fechaDesde, $fechaHasta=null, $hd_cliente=null, $hdGandhi=null){
		try{
			$this->opDebug(__FUNCTION__);
			$sgl =$this->intvalidator($sgl);
			$twin =$this->intvalidator($twin);
			$mat =$this->intvalidator($mat);
			$tpl =$this->intvalidator($tpl);
			if($hd_cliente==null && $hdGandhi==null){
				die("dispoReducer debe recibir algún id de tarifa");
			}

			$SQLupDispo = "UPDATE ".$this->dbMain.".dispo d 
				INNER JOIN ".$this->dbMain.".hotdet hd ON d.id_hotdet = hd.id_hotdet
				INNER JOIN ".$this->dbMain.".hotel h ON hd.id_hotel = h.id_hotel
				INNER JOIN ".$this->dbhot.".hotelesmerge hm ON h.id_pk = hm.id_pk";
			if($hd_cliente!=null && $hdGandhi==null){
				$condHD = " AND hd.id_hotdet_cliente = ".$hd_cliente." AND id_cliente = ".$id_cliente;
			}else{
				$condHD = " AND d.id_hotdet = ".$hdGandhi;	
			}
			$SQLupDispo.=" SET 
				d.ds_hab1 = IF(((ds_hab1 - $sgl < 0) || hm.ver = 0), 0, ds_hab1 - $sgl),
				d.ds_hab2 = IF(((ds_hab2 - $twin < 0) || hm.ver = 0), 0, ds_hab2 - $twin),
				d.ds_hab3 = IF(((ds_hab3 - $mat < 0) || hm.ver = 0), 0, ds_hab3 - $mat),
				d.ds_hab4 = IF(((ds_hab4 - $tpl < 0) || hm.ver = 0), 0, ds_hab4 - $tpl)
				WHERE ";
			if($fechaHasta!=null){
				$SQLupDispo.=" ds_fecha >= '".$fechaDesde."' AND ds_fecha <= '".$fechaHasta."'";
			}else{
				$SQLupDispo.=" ds_fecha = '".$fechaDesde."'";
			}
			$SQLupDispo.=$condHD;
			$db1->Execute($SQLupDispo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	public function globalCheckerConf($db1,$cotdes_cliente,$id_cliente,$sgl=0,$twin=0,$mat=0,$tpl = 0){
		try{
			$this->opDebug(__FUNCTION__);
				$sgl =$this->intvalidator($sgl);
				$twin =$this->intvalidator($twin);
				$mat =$this->intvalidator($mat);
				$tpl =$this->intvalidator($tpl);
				$cliente = $this->getMeThatClient($db1, $id_cliente);
				
				$select = "SELECT 
							  ho.* 
							FROM
							  ".$cliente['bd'].".cotdes cd 
							  INNER JOIN ".$cliente['bd'].".hotocu ho 
								ON cd.id_cotdes = ho.id_cotdes 
							WHERE cd.`usa_stock_dist` = 'n' 
							AND hc_estado = 0 
							  AND cd.id_cotdes = ".$cotdes_cliente;
							
				$respuesta = $db1->Execute($select) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		  
				
				while(!$respuesta->EOF){
					//mandamos a llamar a disporeducer
					$this->dispoReducer($db1, $id_cliente, $sgl, $twin, $mat, $tpl, $respuesta->Fields('hc_fecha'), $respuesta->Fields('hc_fecha'), $respuesta->Fields('id_hotdet'));
					$respuesta->MoveNext();
				}
		$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	public function intvalidator($var){
		try{
			$this->opDebug(__FUNCTION__);
			if(!is_numeric($var)){
				return 0;
			}else{
				return round($var,0);
			}
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}



}
?>