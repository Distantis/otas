<?php
require_once("distantis.php");

class Usuario extends distantis{
	
	//Atributos
	public $mensaje;
	public $id_usuario;
	public $usu_rut;
	private $usu_login;
	public $usu_password;
	public $usu_nombre;
	public $usu_pat;
	public $usu_mat;
	public $usu_estado;
	public $usu_mail;
	public $usu_enviar_correo;
	public $ve_password;
	public $usu_portal;
	public $id_idioma;
	public $id_tipo;
	public $id_hotel;
	public $id_agencia;
	public $id_area;

	public $estado = false;
	
	
	
	public function __construct(){
				
	}
	
	public function __destruct(){
		
	}
	
	public function CambiarPass($db1,$datos){
		try{
			$ids = "";
			$userData = array();
			foreach($datos as $dato){
				$userData[] = " WHEN id_usuario = ".$dato['id_usuario']." THEN '".$dato['usu_password']."'";
				$ids = $ids.$dato['id_usuario'].",";
			}
			
		$consulta = "Update ".$this->dbMain.".usuarios set usu_password = case ". implode(',', $userData) ." ELSE usuarios END where id_usuario in (".substr($ids, 0, -1).")";
		return $consulta;
			
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase usuarios --CambiarPass");
		}
	}


	public function getMeThatUser($db1, $id_user){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlget = "SELECT * FROM ".$this->dbMain.".usuarios WHERE id_usuario = $id_user";
			$resuser = $db1->Execute($sqlget) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $resuser;

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}


	public function validateExternalUser($db1, $cliente, $user, $iduser, $id_empresa){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlValidate= "SELECT * FROM ".$cliente['bd'].".usuarios WHERE id_usuario = ".$iduser." AND usu_login = '".$user."' AND id_empresa = ".$id_empresa." AND usu_estado = 0";
			$resValidate = $db1->Execute($sqlValidate) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($resValidate->RecordCount()>0){
				$this->clDebug();
				return true;
			}else{
				$this->clDebug();
				return false;
			}
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function lowValidateInternalUser($db1,$id_agencia,$id_userota,$usu_portal=true){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlValidate = "SELECT * FROM ".$this->dbMain.".usuarios WHERE id_agencia = $id_agencia AND id_usuario = $id_userota AND usu_estado = 0";
			if($usu_portal!=null){
				$sqlValidate.=" AND usu_portal = ".(($usu_portal)?0:1);
			}
			$resValidate = $db1->Execute($sqlValidate) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($resValidate->RecordCount()>0){
				return true;
			}else{
				return false;
			}

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function AgregarUsuarios($db1,$datos){
		try{
			$userData = array();
			foreach($datos as $dato){
				$userData[] = "(
					'" . $dato['usu_rut'] . "',
					'" . $dato['usu_login'] . "',
					'" . $dato['usu_password'] . "',
					'" . $dato['usu_nombre'] . "',
					'" . $dato['usu_pat'] . "',
					'" . $dato['usu_mat'] . "',
					". $dato['usu_estado'] .",
					'" . $dato['usu_mail'] . "',
					". $dato['usu_enviar_correo'] .",
					". $dato['ve_password'] .",
					". $dato['usu_portal'] .",
					". $dato['id_idioma'] .",
					". $dato['id_tipo'] .",
					". $dato['id_hotel'] .",
					". $dato['id_agencia'] .",
					". $dato['id_area'] .")";
			  
				$tipo = $dato['id_tipo'];
			}
			
			$consulta = 'INSERT INTO '.$this->dbMain.'.usuarios (usu_rut,
												usu_login,
												usu_password,
												usu_nombre,
												usu_pat,
												usu_mat,
												usu_estado,
												usu_mail,
												usu_enviar_correo,
												ve_password,
												usu_portal,
												id_idioma,
												id_tipo,
												id_hotel,
												id_agencia,
												id_area
												) VALUES' . implode(',', $userData);
			$db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$id_usuario = $db1->Insert_ID();

			$this->insLog($db1,$this->id_usuario, 106, $id_usuario);
			
			$this->CrearPermisos($db1,$id_usuario,$tipo);
			return $id_usuario;
			
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase usuarios --AgregarUsuarios");
		}
		
	}

	public function modificaUsuario($db1,$dato, $id_usuario){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlMod = "UPDATE ".$this->dbMain.".usuarios SET

				usu_rut = '".$dato['usu_rut']."',
				usu_login = '".$dato['usu_login']."',
				usu_nombre = '".$dato['usu_nombre']."',
				usu_password = '".$dato['usu_password']."',
				usu_pat = '".$dato['usu_pat']."',
				usu_mat = '".$dato['usu_mat']."',
				usu_estado = ".$dato['usu_estado'].",
				usu_mail = '".$dato['usu_mail']."',
				usu_enviar_correo = ".$dato['usu_enviar_correo'].",
				ve_password = ".$dato['ve_password'].",
				usu_portal = ".$dato['usu_portal'].",
				id_idioma = ".$dato['id_idioma'].",
				id_tipo = ".$dato['id_tipo'].",
				id_hotel = ".$dato['id_hotel'].",
				id_agencia = ".$dato['id_agencia'].",
				id_area = ".$dato['id_area']."
				WHERE id_usuario = ".$id_usuario;
			$db1->Execute($sqlMod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$this->clDebug();
			return true;
		}catch (Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("userClass -> Modifica usuarios");
		}
	}
	 
	public function checkLogin($db1,$usuario,$password){
		try{
			//	$distantis = new distantis();
			$this->opDebug(__FUNCTION__);
		
			if($usuario=='' or $password==''){
				$this->mensaje = "Usuario o Password incompleto";
				$this->clDebug();
				return $this;
			}else{
				$consulta = "select * from ".$this->dbMain.".usuarios where usu_login = '$usuario' and usu_password = '$password' and usu_estado = 0 ";
				$login = $db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				if(!$login->EOF){
					
					$this->mensaje = "Login Correcto";
					$this->id_usuario = $login->Fields('id_usuario');
					$this->usu_rut = $login->Fields('usu_rut');
					$this->usu_login = $login->Fields('usu_login');
					$this->usu_password = $login->Fields('usu_password');
					$this->usu_nombre = $login->Fields('usu_nombre');
					$this->usu_pat = $login->Fields('usu_pat');
					$this->usu_mat =$login->Fields('usu_mat') ;
					$this->usu_estado =$login->Fields('usu_estado') ;
					$this->usu_mail = $login->Fields('usu_mail');
					$this->usu_enviar_correo = $login->Fields('usu_enviar_correo');
					$this->ve_password = $login->Fields('ve_password');
					$this->usu_portal = $login->Fields('usu_portal');
					$this->id_idioma = $login->Fields('id_idioma');
					$this->id_tipo = $login->Fields('id_tipo');
					$this->id_hotel = $login->Fields('id_hotel');
					$this->id_agencia = $login->Fields('id_agencia');
					$this->id_area = $login->Fields('id_area');
					$this->estado = true;
					
					//$this->pico();
					$this->clDebug();
					return $this;	
				}else{
					$this->mensaje = "Usuario o Contraseña Incorrecto";
					$this->clDebug();
					return $this;
				}
			}
			$this->clDebug();
		}catch (Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase usuarios --loguear");
		}
	}

	//valida si el rut existe en la base de datos o no, retorna true o false
	public function RutExiste($db1,$rut){
		try{
			$this->opDebug(__FUNCTION__);
			$consulta = "SELECT * FROM ".$this->dbMain.".usuarios where usu_rut = '".$rut."'";
			$rut = $db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($rut->RecordCount()>0){
				$this->clDebug();
				return true;
			}else{
				$this->clDebug();
				return false;
			}
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase usuarios --RutExiste");
		}
	}

	//busca en el registro si un permiso especifico esta asociado a un usuario especifico y devuelve el resulset.
	public function checkPerUser($db1,$user,$permiso){
		try{
			$this->opDebug(__FUNCTION__);
			$con1="SELECT * FROM ".$this->dbMain.".permisosusuario WHERE id_usuario = ".$user. " and per_codigo =".$permiso;
			$perm=$db1->Execute($con1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $perm;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}
	
	public function CrearPermisos($db1,$idusuario,$tipo){
		try{
			$permisos_sql = "select per_codigo from ".$this->dbMain.".pertipousuario where id_tipousu=".$tipo;
			$perm=$db1->Execute($permisos_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$dat = array();
			$cont = 0;
			while(!$perm->EOF){
				$dat[$cont]['per_codigo'] = $perm->Fields('per_codigo');
				$cont++;
			$perm->MoveNext();
			}
			
			$userData = array();
			foreach($dat as $dato){
				$userData[] = 	"(".$idusuario. ",
								" . $dato['per_codigo'] . ")"
							;
			}
			if(count($userData)>0){
				$consulta = "INSERT INTO ".$this->dbMain.".permisosusuario (id_usuario,per_codigo) VALUES ".implode(',', $userData);
				$db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			return true;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase usuarios --CrearPermisos");	
		}
	}


	public function getUsers($db1,$data){
		try{
			$this->opDebug(__FUNCTION__);
			$arconds = array();
			$sqlusers = "SELECT id_usuario,
				usu_nombre, 
				usu_pat, 
				usu_mat, 
				usu_login, 
				usu_password, 
				usu_mail, 
				u.id_agencia, 
				ag_nombre FROM ".$this->dbMain.".usuarios u INNER JOIN ".$this->dbMain.".agencia ag ON u.id_agencia = ag.id_agencia";
			foreach($data as $colname => $colval){
				if(is_numeric($colval)){
					$auxstr = "$colname = $colval";
				}else{
					$auxstr = "$colname like '%$colval%'";
				}
				array_push($arconds, $auxstr);
			}
			if(count($arconds)>0){
				$sqlusers.= " WHERE ".implode(" AND ", $arconds);
			}
			$resusers = $db1->Execute($sqlusers) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$this->clDebug();
			return $resusers;

		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}




}
?>