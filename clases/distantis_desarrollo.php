<?php
if(!function_exists('KT_replaceParam') || !function_exists('kt_replaceparam')){
	require_once(dirname(__FILE__) . '/../includes/functions.inc.php');	
}
class distantis{
	public $nombre_plataforma = "Distantis";
	public $correos = array(
		0 => array("nombre" => "Gonzalo Cadiz", "mail" => "gonzalo@distantis.com"),
		0 => array("nombre" => "Gonzalo Cadiz", "mail" => "gonzalo@distantis.com"),
	);
	public $dbhot = "hoteles";
	public $dbMain = "otas";
	public $decimales = 2;
	public $clientes;
	public $debug = false;
	public $debColors = array("chocolate","Blue","Fuchsia","Gray","Green","#607D8B","Maroon","Navy","Olive","Purple","Red","Teal","goldenrod");
	//define el pais considerado como nacional
	public $nationalCountryID = 4;
	//constructor... aqui se inicializan todas las shits que no se puedan declarar como constantes arriba... (arrays y otros inventos weones).
	function __construct(){     
    	$this->clientes = array();

	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//le pone color a todo lo que pase dbentro de una funcion (se pone al principio del try en todas las funciones);(FG)
	public function opDebug($funcName){
		if($this->debug){
			$debColor = array_shift($this->debColors);
			array_push($this->debColors, $debColor);
			echo "<span style='color:".$debColor."'><br>-----FUNCION: $funcName --------------<br>";
		}
	}
	//esto cierra el coloreo dentro de las funciones (FG)
	public function clDebug(){
		if($this->debug){
			echo "</span>";
		}
	}
	public function debugVar($var){
		echo "<pre>";
		var_dump($var);
		echo "</pre>";
	}
	//trae todos los clientes, independiente de su estado.(FG)
	public function getClientes($db1, $reload=false,$id_agencia = null){
		try {
			$this->opDebug(__FUNCTION__);
			if(count($this->clientes)==0 || $reload){
				$SQLclientes = "SELECT c.*, mkc.mk_valor, mkc.id_area FROM ".$this->dbhot.".clientes c LEFT JOIN ".$this->dbMain.".mk_cliente mkc ON c.id_cliente = mkc.id_cliente";
				if(isset($id_agencia)){
					$SQLclientes.=" AND c.id_agencia = $id_agencia";
				}
				$resClientes = $db1->Execute($SQLclientes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$clientes = array();

				while(!$resClientes->EOF){
					$clientes[$resClientes->Fields('id_cliente')]['name'] = $resClientes->Fields('nombre');
					$clientes[$resClientes->Fields('id_cliente')]['bd'] = $resClientes->Fields('bd'); 
					$clientes[$resClientes->Fields('id_cliente')]['estado'] = $resClientes->Fields('estado');
					$clientes[$resClientes->Fields('id_cliente')]['mk'][$resClientes->Fields('id_area')] = $resClientes->Fields('mk_valor');
					$clientes[$resClientes->Fields('id_cliente')]['tghandi'] = $resClientes->Fields('trabaja_gandhi');
					$clientes[$resClientes->Fields('id_cliente')]['id_user'] = $resClientes->Fields('id_usuario_integracion');
					$clientes[$resClientes->Fields('id_cliente')]['usachild'] = $resClientes->Fields('usachild');
					$clientes[$resClientes->Fields('id_cliente')]['opebuyer'] = $resClientes->Fields('idope_comprador');
					$clientes[$resClientes->Fields('id_cliente')]['raiz'] = $resClientes->Fields('raiz');
					$clientes[$resClientes->Fields('id_cliente')]['id_agencia'] = $resClientes->Fields('id_agencia');
					
					$SQLEspColls = "SELECT * FROM ".$this->dbhot.".dcep_cliente WHERE dcepc_estado = 0 AND id_cliente = ".$resClientes->Fields('id_cliente');
					$RESEspColls = $db1->Execute($SQLEspColls) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$arEsp = array();
					while(!$RESEspColls->EOF){
						if($RESEspColls->Fields('dcepc_valido') == null || $RESEspColls->Fields('dcepc_valido') == ''){
							$auxval = '';
						}else{
							$auxval = $RESEspColls->Fields('dcepc_valido');
						}

						if(isset($arEsp[$RESEspColls->Fields('dcepc_tabname')][$RESEspColls->Fields('dcepc_colname')])){
							$index = count($arEsp[$RESEspColls->Fields('dcepc_tabname')][$RESEspColls->Fields('dcepc_colname')]);
						}else{
							$index = 0;
						}
						$arEsp[$RESEspColls->Fields('dcepc_tabname')][$RESEspColls->Fields('dcepc_colname')][$index]  = $auxval;
						$RESEspColls->MoveNext();
					}
					$clientes[$resClientes->Fields('id_cliente')]['esColls'] = $arEsp;
					$resClientes->MoveNext();
				}
				$this->clientes = $clientes;
			}
			$this->clDebug();
			return $this->clientes;
		} catch (Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>No se pudieron cargar los clientes - getClientes");
		}
	}



	//trae un cliente en especifico.(FG)
	public function getMeThatClient($db1, $idcliente){
		try{
			$this->opDebug(__FUNCTION__);
			if(!isset($this->clientes[$idcliente])){
				$this->getClientes($db1, true);
				if(!isset($this->clientes[$idcliente])){
					die("ID Cliente no existe ($idcliente) - getMeThatClient");
				}else{
					$this->clDebug();
					return $this->clientes[$idcliente];
				}
			}else{
				$this->clDebug();
				return $this->clientes[$idcliente];
			}
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getCalcLines($db1,$id_cliente, $area=null){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLgetCalcLines = "SELECT * FROM ".$this->dbMain.".calc_cli cc 
				INNER JOIN ".$this->dbMain.".calc_lines cl ON cc.id_calcline = cl.id_calcline 
				WHERE id_cliente = ".$id_cliente;
			if($area!=null){
				$SQLgetCalcLines.=" AND id_area = ".$area;
			}
			
			$resCalcLines = $db1->Execute($SQLgetCalcLines) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($resCalcLines->RecordCount()<2){
				return null;
			}else{
				while(!$resCalcLines->EOF){
					$calclines[$id_cliente][$resCalcLines->Fields('table')][$resCalcLines->Fields('id_area')] = $resCalcLines->Fields('cl_calcline');
					$resCalcLines->MoveNext();
				}
			}
			$this->clDebug();
			return $calclines;
		}catch(Exception $e){
			echo $e->getMesagge();
			var_dump($e);
			$this->clDebug();
		}
	}

	public function evalCalcLines($mk, $costo, $factorHab, $aplicaIva, $ocupacion, $calcLine){
		try{
			$this->opDebug(__FUNCTION__);
			$calcLine = str_replace("__markup__", $mk, $calcLine);
			$calcLine = str_replace("__iva__", (($aplicaIva==0)?1.19 : 1), $calcLine);
			$habValStr = str_replace("__costo__", $costo * $factorHab, str_replace("__ocu__", $ocupacion, $calcLine));
			eval('$habVal = round('.$habValStr.', $this->decimales);');
			$this->clDebug();
			return $habVal;
		}catch(Exception $e){
			echo $e->getMesagge();
			var_dump($e);
			$this->clDebug();
		}
	}

	public function strangerPax($id_pais){
		return ($id_pais == $this->nationalCountryID)?false:true;
	}

	//devuelve un resultset con los paises(FG)
	public function getCountrys($db1, $onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			if($onlyActive){
				$cond = " WHERE pai_estado = 0";
			}
			$SQLgetCountrys = "SELECT * FROM ".$this->dbMain.".pais $cond ORDER BY pai_nombre ASC";
			$RESgetCountrys = $db1->Execute($SQLgetCountrys) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetCountrys;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//devuelve un resultset con las ciudades.(FG)
	public function getCities($db1, $onlyActive=true,$id_pais = 0, $id_ciudad = null){
		try{
			$this->opDebug(__FUNCTION__);
			$arConds = array();
			$cond="";
			if($onlyActive){
				array_push($arConds, " ciu_estado = 0");
			}
			if($id_ciudad!=null){
				array_push($arConds, " id_ciudad = ".$id_ciudad);	
			}
			if($id_pais!=0){
				//$sqlreq = " and id_pais = ".$id_pais;
				array_push($arConds,  " id_pais=".$id_pais);			
			}
			
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}
			
			
			$SQLgetCities = "SELECT * FROM ".$this->dbMain.".ciudad $cond ORDER BY ciu_nombre ASC";
			$RESgetCities = $db1->Execute($SQLgetCities) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetCities;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//devuelve un resulset con los grupos de agencias.(FG)
	public function getGroups($db1, $onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			if($onlyActive){
				$cond = " WHERE gru_estado = 0";
			}
			$SQLgetGroup = "SELECT * FROM ".$this->dbMain.".grupo $cond ORDER BY gru_nombre ASC";
			$RESgetGroup = $db1->Execute($SQLgetGroup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetGroup;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	//inserta log en el log general.(FG)
	public function insLog($db1,$user, $action, $idAffected){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLinsLog = sprintf("INSERT INTO ".$this->dbMain.".log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
				GetSQLValueString($user, "int"), 
				GetSQLValueString($action, "int"), 
				GetSQLValueString($idAffected, "int") );
				$db1->Execute($SQLinsLog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$this->clDebug();
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//valida el formato de markup y devuleve 1 si no es valido.(FG)
	public function valMk($markup){
		try{
			$this->opDebug(__FUNCTION__);
			if($markup!='' && $markup!=0){
				if($markup>1){
					$this->clDebug();
					return 1-($markup/100);
				}else{
					$this->clDebug();
					return $markup;
				}
			}else{
				$this->clDebug();
				return 1;
			}
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
			
	}
	
	//devuelve la hora del servidor de BBDD.
	public function FechaHoraServer($db1){
		try{
		$consulta = "SELECT 
			DATE_FORMAT(NOW(), '%d %M %Y %H:%i:%s') AS fecha";
		$fecha = $db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $fecha->Fields('fecha');
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase distantis --FechaHoraServer");
		}
	}
	
	public function FechaHoraServerRespuesta($db1){
		try{
		$consulta = "SELECT 
			DATE_FORMAT(NOW(), '%d %M %Y %H:%i:%s') AS fecha,
			DATE_FORMAT(NOW(), '%Y%m%d000000') AS fecha2,
			DATE_FORMAT(NOW(), '%Y%m%d235959') AS fecha3,
		DATE_FORMAT(NOW(), '%d-%m-%Y') AS fecha4";
		$fecha = $db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		return $fecha;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			die("<br>Error en clase distantis --FechaHoraServer");
		}
	}
	//obtiene el permiso asociado al codigo enviado por parametro(FG)
	public function getThatPerm($db1, $permiso){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLpermiso = "SELECT * FROM permisos WHERE per_codigo =".$permiso;
			$RESpermiso = $db1->Execute($SQLpermiso) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESpermiso;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//trae los tipos de usuario
	public function GetTipoUsuario($db1, $onlyActive = true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			if($onlyActive){
				$cond= "WHERE tu_estado = 0";
			}
			$query_Recordset1 = "SELECT * FROM ".$this->dbMain.".tipousuario $cond ORDER BY tu_nombre ASC";
			$tipousuario = $db1->Execute($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $tipousuario;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetTipoUsuario");	
		}
	}
	//devuelve las area usadas en la plataforma(FG)
	public function getAreas($db1,$onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			if($onlyActive){
				$cond= "WHERE area_estado = 0";
			}
			$SQLgetAreas="SELECT * FROM ".$this->dbMain.".area $cond ORDER BY area_nombre ASC";
			$RESgetAreas = $db1->Execute($SQLgetAreas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetAreas;

		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	//obitene los idiomas(FG)
	public function getLanguages($db1,$onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			if($onlyActive){
				$cond= "WHERE estado_idioma = 0";
			}
			$SQLgetLan="SELECT * FROM ".$this->dbMain.".idioma $cond ORDER BY idioma ASC";
			$RESgetLan = $db1->Execute($SQLgetLan) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetLan;

		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getHotFichas($db1){
		try{
			$this->opDebug(__FUNCTION__);
			$queryhoteles = "SELECT h.*, IFNULL(id_hotficha, '-1') AS id_hotficha 
				FROM ".$this->dbMain.".hotel h 
				LEFT JOIN ".$this->dbMain.".hotficha hf ON h.id_hotel = hf.id_hotel 
				ORDER BY hot_nombre";
			$hoteles = $db1->SelectLimit($queryhoteles) or die(__LINE__." - ".$db1->ErrorMsg());
			$this->clDebug();
			return $hoteles;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getFichaHotel($db1, $id_hotel){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlgetficha = "SELECT * FROM ".$this->dbMain.".hotficha WHERE id_hotel = $id_hotel";
			$resgetficha = $db1->Execute($sqlgetficha) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $resgetficha;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}

	public function createFichaVacia($db1,$id_hotel){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlinsficha = "INSERT INTO ".$this->dbMain.".hotficha (id_hotel) VALUES ($id_hotel)";
			$db1->Execute($sqlinsficha) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
 
	public function updateFichaHot($db1, $data, $id_hotel){
		//pa que esta wea funcione, hay que mandarle en los nodos el nombre de la columna en la bbdd ej: $data['id_regimen']=1 
		try{
			$this->opDebug(__FUNCTION__);
			end($data);
			$lastindex = key($data);
			reset($data);
			$sqlup = "UPDATE ".$this->dbMain.".hotficha SET ";
			foreach ($data as $columna => $valor) {
				$sqlup.=" $columna = $valor";
				if($columna!=$lastindex){
					$sqlup.=", ";
				}
			}
			$sqlup.=" WHERE id_hotel = ".$id_hotel;
			$db1->Execute($sqlup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}		
	}

	public function codigo_rand(){
		$keychars = "ABCDEFGHIJKMNOPQRSTUVWXYZ0123456789";
		$length = 7;
		$randkey = "";
		$max = strlen($keychars) - 1;
		for ($i = 0; $i < $length; $i++) {
			$randkey .= substr($keychars, rand(0, $max), 1);
		}
		return($randkey);
	}
	
	public function GetTipoHab($db1,$onlyActive=true,$sellOtas=true,$id=null){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
		$arConds = array();
			
			if($onlyActive){
				array_push($arConds, "th_estado = 0");
			}
			if($sellOtas){
				array_push($arConds, "th_sellotas= 0");
			}
			if($id!=null){
				array_push($arConds,  "id_tipotarifa=".$id);			
			}
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}			
			$condicion_sql = "SELECT * 	FROM ".$this->dbhot.".tipohabitacion $cond ORDER BY th_nombre ASC";
			$respuesta =  $db1->Execute($condicion_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());					
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetTipoHab");	
		}
	}
	
	public function GetBookings($db1,$data,$limit=null){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
			$arConds = array();			
			if(isset($data['id_agencia'])){
				array_push($arConds, "a.id_agencia =".$data['id_agencia']);
			}
			if(isset($data['id_usuario'])){
				array_push($arConds, "id_usuario=".$data['id_usuario']);
			}
			if(isset($data['checkin'])){
				array_push($arConds, "cot_fecdesde >='".$data['checkin']."'");
			}
			if(isset($data['checkout'])){
				array_push($arConds, "cot_fechasta <='".$data['checkout']."'");
			}
			if(isset($data['betcheckin'])){
				array_push($arConds, "cot_fecdesde between '".$data['betcheckin']['fec1']."' AND '".$data['betcheckin']['fec2']."'");	
			}

			if(isset($data['id_ciudad'])){
				array_push($arConds, "cot_pridestino =".$data['id_ciudad']);
			}
			if(isset($data['id_hotel'])){
				array_push($arConds, "cot_prihotel =".$data['id_hotel']);
			}
			if(isset($data['cot_fecfrom'])){
				$str = " DATE_FORMAT(cot_fec, '%Y-%m-%d') ".(($data['cot_fecto']!='')?" >= ":" = ")."'".$data['cot_fecfrom']."'";
				array_push($arConds, $str);
			}
			if(isset($data['cot_fecto'])){
				array_push($arConds, " DATE_FORMAT(cot_fec, '%Y-%m-%d') <= '".$data['cot_fecto']."'");
			}
			if(isset($data['clientcode'])){
				array_push($arConds, " cot_correlativo =".$data['clientcode']);
			}
			if(isset($data['include_noshow'])){	
				array_push($arConds, "cot_noshow = 1");
			}
			if(isset($data['includenonref'])){
				array_push($arConds, "cot_refundable = 0");
			}
			if(isset($data['id_cot'])){
				array_push($arConds, "c.id_cot = ".$data['id_cot']);
			}
			if(isset($data['any_estado'])){
				array_push($arConds, "cot_estado = 0 and id_seg = 7");
			}else{
				array_push($arConds, "id_seg = 7");
			}
			if(isset($data['idcliente'])){
				array_push($arConds, "id_cliente = ".$data['idcliente']);	
			}
			if(isset($data['id_area'])){
				array_push($arConds, "id_area = ".$data['id_area']);
			}	
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}
			$condicion_sql = "SELECT *, ag_nombre as ope_namebuyer FROM ".$this->dbMain.".cot c
				INNER JOIN ".$this->dbMain.".ciudad ciu ON c.cot_pridestino = ciu.id_ciudad
				INNER JOIN ".$this->dbMain.".hotel h ON c.cot_prihotel = h.id_hotel
				INNER JOIN ".$this->dbMain.".agencia a ON c.id_agencia = a.id_agencia
				INNER JOIN ".$this->dbMain.".hotocu hc ON c.id_cot = hc.id_cot
				$cond GROUP BY c.id_cot ORDER BY cot_fec ASC";			
			$respuesta =  $db1->SelectLimit($condicion_sql,((isset($limit)?$limit:-1))) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());					
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetBookings");	
		}
	}
	public function getCotBuyer($db1, $idcotbuyer = null, $idagencia ,$cliente , $extras = null){
		try{
			$this->opDebug(__FUNCTION__);
			$arconds = array();
			if(isset($idcotbuyer)){
				array_push($arConds, "cb.id_cot = ".$idcotbuyer);
			}
			if(isset($extras)){
				foreach ($extras as $colname => $colvalue){
					if(is_array($colvalue)){
						array_push($arConds, "$colname ".$colvalue['cond']." ".$colvalue['val']);	
					}
					array_push($arConds, "$colname = ".$colvalue);
				}

			}
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}

			$sqlgetcot = "SELECT cb.id_cot AS id_cotbuyer,  
					cg.id_cot AS id_cotgand,
					cg.cot_fecdesde,
					cg.cot_fechasta,
					cg.cot_correlativo AS corrgand,
					cb.cot_valor AS cot_valorbuyer,
					cg.cot_valor AS cot_valorgand,
					cb.cot_venta AS cot_ventabuyer,
					cg.cot_venta AS cot_ventagand,
					cg.cot_fecconf AS fecconf_gand,
					cg.cot_pripas,
					cg.cot_pripas2,
					CONCAT(u1.usu_nombre,' ',u1.usu_pat) AS conf_userbuyer,
					CONCAT(u2.usu_nombre,' ',u2.usu_pat) AS anul_userbuyer,
					cg.cot_estado,
					cg.cot_obs AS cot_obs,
					cg.id_agencia,
					cg.id_mon,
					cg.id_area,
					cg.ha_hotanula,
					h.hot_nombre AS ope_namebuyer,
					hg.hot_nombre,
					TRIM(ciu_nombre) as ciu_nombre,
					cg.cot_noshow,
					cg.cot_outdate, 
					cg.cot_estado
				FROM ".$cliente['bd'].".cot cb 
				INNER JOIN ".$this->dbMain.".cot cg ON cb.id_cot = cg.cot_correlativo AND cb.id_tipocot = 3 AND id_agencia = $idagencia
				INNER JOIN ".$this->dbMain.".ciudad ciu ON cg.cot_pridestino = ciu.id_ciudad
				INNER JOIN ".$this->dbMain.".hotel hg ON cg.cot_prihotel = hg.id_hotel
				LEFT JOIN ".$cliente['bd'].".usuarios u1 ON cb.id_usuconf = u1.id_usuario
				LEFT JOIN ".$cliente['bd'].".usuarios u2 ON cb.id_usuanula = u2.id_usuario
				LEFT JOIN ".$cliente['bd'].".hotel h ON cb.id_operador = h.id_hotel
				$cond";
			$rescot = $db1->Execute($sqlgetcot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $rescot;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function getDestinations($db1,$id_cot,$buyer=false,$idagencia=null,$cliente=null){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlGetDestinations =" SELECT cd.id_cotdes,
					hot_nombre,
					ciu_nombre,
					DATE_FORMAT(cd.cd_fecdesde, '%Y-%m-%d') AS cd_fecdesde, 
					DATE_FORMAT(cd.cd_fechasta, '%Y-%m-%d') AS cd_fechasta, 
					cd.cd_hab1,
					cd.cd_hab2, 
					cd.cd_hab3, 
					cd.cd_hab4, 
					cd.cd_valor as cd_valorgand,
					cd.cd_venta as cd_ventagand,
					h.hot_direccion, ";
					if($buyer){
						$sqlGetDestinations.="cd2.cd_valor as cd_valorbuyer, cd2.cd_venta as cd_ventabuyer,";
					}
					$sqlGetDestinations.="th_nombre, 
					cd.cd_numpas, 
					cd.usa_stock_dist
				FROM ".$this->dbMain.".cotdes cd INNER JOIN hotel h ON cd.id_hotel = h.id_hotel
				INNER JOIN ".$this->dbMain.".ciudad ciu ON h.id_ciudad = ciu.id_ciudad 
				INNER JOIN ".$this->dbhot.".tipohabitacion th ON cd.id_tipohabitacion = th.id_tipohabitacion";
				if($buyer){
					$sqlGetDestinations.=" INNER JOIN ".$this->dbMain.".cot c ON cd.id_cot = c.id_cot 
					INNER JOIN ".$cliente['bd'].".cotdes cd2 ON c.cot_correlativo = cd2.id_cot
					WHERE c.cot_correlativo = $id_cot AND id_agencia = $idagencia";
				}else{
					$sqlGetDestinations.=" WHERE id_cot = $id_cot";
				}
				
				$resdestinations = $db1->Execute($sqlGetDestinations) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$this->clDebug();
				return $resdestinations;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	public function getPaxes($db1, $id_cotdes){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlGetPaxes = "SELECT cp_nombres, cp_apellidos, p.pai_nombre 
				FROM ".$this->dbMain.".cotpas cp 
				INNER JOIN ".$this->dbMain.".pais p ON cp.id_pais = p.id_pais
				WHERE id_cotdes = $id_cotdes";
			$resPaxes = $db1->Execute($sqlGetPaxes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $resPaxes;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}	
	}
	public function GetMealPlan($db1,$hotel=null){
		try{
			$this->opDebug(__FUNCTION__);
			$condicion_sql = "SELECT r.* FROM ".$this->dbMain.".regimen r";
			if(isset($hotel)){
				$condicion_sql.=" INNER JOIN ".$this->dbMain.".hotdet hd ON r.id_regimen = hd.id_regimen
					WHERE hd.id_hotel =".$hotel." GROUP BY r.id_regimen"; 
			}
			$respuesta =  $db1->Execute($condicion_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());					
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetMealPlan");	
		}
	}
	public function getBreakfasts($db1, $onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlgetbf = "SELECT * FROM ".$this->dbMain.".tipodesayuno";
			if($onlyActive){
				$sqlgetbf.=" WHERE tipodes_estado = 0";
			}
			$respuesta =  $db1->Execute($sqlgetbf) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --getBreakfasts");	
		}
	}

	public function getPoolTypes($db1, $onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlgetpools="SELECT * FROM tipopiscina";
			if($onlyActive){
				$sqlgetpools.=" WHERE tpisc_estado = 0";
			}
			$respuesta =  $db1->Execute($sqlgetpools) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --getPoolTypes");	
		}
	}
	public function getWifiTypes($db1, $onlyActive=true){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlgetwifis="SELECT * FROM tipowifi";
			if($onlyActive){
				$sqlgetwifis.=" WHERE twifi_estado = 0";
			}
			$respuesta =  $db1->Execute($sqlgetwifis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --getWifiTypes");	
		}
	}


	public function redirectByPost($url, $vars=null,$msgalert=''){
		$html="<form name='tempForm' id='tempForm' method='POST' action='$url'>";
		if(isset($vars)){
			foreach($vars as $key => $value){
				$html.="<input type='hidden' name='$key' id='$key' value='$value'>";
			}
			
		}
		$html.="</form><script>";
		if($msgalert!=''){
			$html.="alert('$msgalert');";
		}
		$html.="document.getElementById('tempForm').submit();</script>";
		die($html);
	}

	//estado debe ser 0 o 1
	//id corresponde a id de bd hoteleria
	public function GetTipoTarifa($db1,$onlyActive=true,$id=null){
		try{
			$this->opDebug(__FUNCTION__);
			$cond = "";
		$arConds = array();
			
			if($onlyActive){
				array_push($arConds, "tt_estado = 0");
			}
			if($id!=null){
				array_push($arConds,  "id_tipotarifa=".$id);			
			}
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}
			
			
			$condicion_sql = "SELECT 
							   id_tipotarifa,
							   tt_nombre,
							   tt_codcts,
							   if(tt_estado=0,'Activo','Desactivado') as tt_estadot,
							   if(tt_sellotas=0,'Si','No') as tt_sellotast,
							   if(tt_espromo=1,'Si','No') as tt_espromot, 
							   tt_estado,
							 tt_sellotas,
							 tt_espromo,
							 tt_maxnoc,
							 tt_minnoc
							FROM
							   ".$this->dbhot.".tipotarifa 
							 $cond ORDER BY tt_nombre ASC";
			$respuesta =  $db1->Execute($condicion_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());					
			$this->clDebug(); 
			return $respuesta;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetTipoTarifa");	
		}
	}

	//estado debe ser 1 o 0
	//ID corresponde al id especifico de la tarifa
	public function GetTipoTarCliente($db1,$cliente,$onlyActive=true,$id_ttcli=null,$id_ttesp=null){
		try{
			$this->opDebug(__FUNCTION__);
			$arConds = array();
			$cond="";
			if($onlyActive){
				array_push($arConds, "tt_estado = 0");
			}
			if($id_ttcli!=null){
				array_push($arConds,  "id_tipotarifa=".$id_ttcli);			
			}
			if($id_ttesp!=null){
				array_push($arConds,  "id_tipotar=".$id_ttesp);			
			}
			if(count($arConds)>0){
				$cond="WHERE ".implode(" AND ", $arConds);
			}
			$consulta = "select * from $cliente.tipotarifa $cond ORDER BY tt_nombre ASC";
			$datos = $db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		
			$this->clDebug(); 
			return $datos;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --GetTipoTarCliente");	
		}
	}
	
	public function updateTipoTarHotel($db1,$nombre,$codigo,$estado, $vende,$promo,$max,$min,$id){
		try{
			$this->opDebug(__FUNCTION__);
			
				
				$consulta = "UPDATE 
							  ".$this->dbhot.".tipotarifa tt 
							SET
							  tt_nombre = '".$nombre."',
							  tt_codcts = '".$codigo."',
							  tt_estado = ".$estado.",
							  tt_sellotas =".$vende.",
							  tt_espromo =".$promo.",
							  tt_maxnoc =".$max.",
							  tt_minnoc =".$min."
							WHERE id_tipotarifa =".$id;
				//die ($consulta);
				$db1->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
			$this->clDebug();
			return true;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --updateTipoTarHotel");	
		}
	}

	//****^* esta wea supongo que ya no la vas a ocupar... o la vas a reapuntar... trata de cambiarle el nombre a Instipotarifa o newTipoTarifa... o weas asi... los set son para las variables... 
	public function SetTipoTarifa($db1,$nombre,$codigo,$estado,$vende,$promo,$max,$min){
		try{
			$this->opDebug(__FUNCTION__);
			
			$Inserta = "INSERT INTO ".$this->dbhot.".tipotarifa (tt_nombre, tt_codcts, tt_estado, tt_sellotas, tt_espromo, tt_maxnoc, tt_minnoc) 
				VALUES ('".$nombre."', '".$codigo."', ".$estado.",".$vende.", ".$promo.", ".$max.", ".$min.")";
			$db1->Execute($Inserta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug(); 
			return $db1->Insert_ID();
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --SetTipoTarifa");	
		}
	}
	
	public function resetTipoTarCliente($db1,$cliente,$id){
		try{
			$this->opDebug(__FUNCTION__);
			
				$update = "UPDATE 
				  	".$cliente.".tipotarifa tt 
					SET
				  	tt.`id_tipotar` = 0 
					WHERE id_tipotar = ".$id;
				$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$this->clDebug();
				return true;
			}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --UpdTipoTarCliente");	
		}
	}

	//idtt: variable que guarda el id interno de la bd de hoteles
	//idtte: variable que guarda el id interno del operador
	public function UpdTipoTarClienteEsp($db1,$cliente,$idtt,$idtte){
		try{
			$this->opDebug(__FUNCTION__);
				$update = "UPDATE 
					".$cliente.".tipotarifa tt 
					SET
					tt.`id_tipotar` = ".$idtt." 
					WHERE id_tipotarifa = ".$idtte;
				$db1->Execute($update) or die($update);
				$this->clDebug();
				return true;
			}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
			die("<br>Error en clase distantis --UpdTipoTarCliente");	
		}
	}
	
	//trae el markup general del sistema(FG)
	public function getMkGral($db1,$onlyVigente=true,$extranjero=true){
		try{
			$this->opDebug(__FUNCTION__);
			
			/////Se comenta este codigo por que pasamos a usar markup por area default, Mantengo el codigo por si se arrepienten. NB///////
			/*if($onlyVigente){
				$cond = " WHERE mg_vigdesde <=NOW()
					ORDER BY mg_vigdesde DESC ,mg_fecha DESC LIMIT 1";
			}else{
				$cond =" ORDER BY mg_fecha DESC";

			}
			$SQLgetMk ="SELECT mg_markup, DATE_FORMAT(mg_fecha, '%d-%m-%Y %H:%i') AS fecha_creacion_f,
			    mg_fecha,
			    DATE_FORMAT(mg_vigdesde,'%d-%m-%Y') AS vig1_f,
			    mg_vigdesde,
			    CONCAT(usu_nombre, ' ',usu_pat) AS creador
			    FROM markup_general mg
			    LEFT JOIN 
			    usuarios u ON 
			    mg.id_usuario = u.id_usuario".$cond;
				
				
				*/
				
				//echo $extranjero;
			if($extranjero){
				$area = 1;
			}else{
				$area = 2;
			}	 
			
			
			$SQLgetMKarea = "SELECT 
						  area_markup as mg_markup
						FROM
						  area
						WHERE id_area = ".$area;
			$RESgetMk = $db1->Execute($SQLgetMKarea) or die(__LINE__." ".$db1->ErrorMsg());
			
	
			$this->clDebug();
			return $RESgetMk;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function ModCliente($db1,$id,$tgandhi,$markup, $id_user,$opebuyer,$id_agencia){
		try{
			$this->opDebug(__FUNCTION__);
			
			$consulta = "update ".$this->dbhot.".clientes set 
				trabaja_gandhi = $tgandhi,
				id_usuario_integracion = ".(($id_user!='')?$id_user:'null').",
				idope_comprador = ".(($opebuyer!='')?$opebuyer:'null').",
				id_agencia = ".(($id_agencia!='')?$id_agencia:'null')."
				where id_cliente = ".$id;
			//return $consulta;
			$db1->Execute($consulta) or die(__LINE__." ".$db1->ErrorMsg());	

			foreach ($markup as $idarea => $valmk){
				$upmkcli = "UPDATE ".$this->dbMain.".mk_cliente SET mk_valor = ".$valmk." WHERE id_area = $idarea AND id_cliente = $id";
				$db1->Execute($upmkcli) or die(__LINE__." ".$db1->ErrorMsg());
			}
			
			$this->clDebug();
			return true;
		
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function markup_area($db1,$markup,$area){
		try{
			
			$this->opDebug(__FUNCTION__);
			$upstring = "UPDATE ".$this->dbMain.".area SET area_markup = ".$markup." WHERE id_area = ".$area;
			$db1->Execute($upstring) or die(__LINE__." ".$db1->ErrorMsg());		
			$this->clDebug();
			return true;
			
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function markup_cont($db1,$markup,$cont){
		try{
			$this->opDebug(__FUNCTION__);
			
			$upstring = "UPDATE ".$this->dbMain.".cont SET cont_markuphtl = ".$markup." WHERE id_cont = ".$cont;
			$db1->Execute($upstring) or die(__LINE__." ".$db1->ErrorMsg());		
			
			$this->clDebug();
			return true;
		
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function paishots($db1){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlpaishots = "SELECT 
							DISTINCT(pai_nombre) AS paises, p.id_pais
							FROM
							".$this->dbMain.".hotel h 
							INNER JOIN ".$this->dbMain.".ciudad ciu 
							ON h.id_ciudad = ciu.id_ciudad 
							INNER JOIN ".$this->dbMain.".pais p 
							ON ciu.id_pais = p.id_pais 
							WHERE  hot_estado = 0 ORDER BY paises ASC"; 
			$respaishots = $db1->Execute($sqlpaishots) or die(__LINE__." ".$db1->ErrorMsg());	
			
			$this->clDebug();
			return $respaishots;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function paisopes($db1){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlpaisopes = "SELECT 
							DISTINCT(pai_nombre) AS paises, p.id_pais
							FROM
							".$this->dbMain.".agencia h 
							
							INNER JOIN ".$this->dbMain.".pais p 
							ON h.id_pais = p.id_pais 
							WHERE ag_estado = 0 ORDER BY paises ASC";
			$respaisopes = $db1->Execute($sqlpaisopes) or die(__LINE__." ".$db1->ErrorMsg());
			
			$this->clDebug();
			return $respaisopes;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function loadhtmlhot($db1, $id_hotel, $id_area){
		try{
			$this->opDebug(__FUNCTION__);
				
				$sqlmk = "SELECT h.id_hotel, hot_nombre, a.id_area, a.area_nombre, id_markdin, markdin_valor, markdin_estado, ciu_nombre, h.id_ciudad FROM hotel h 
					INNER JOIN ".$this->dbMain.".area a 
					LEFT JOIN ".$this->dbMain.".markdin_gral mg ON 
					h.id_hotel = mg.id_hotel AND a.id_area = mg.id_area
					INNER JOIN ".$this->dbMain.".ciudad ciu ON
					h.id_ciudad = ciu.id_ciudad
					WHERE hot_estado = 0
					AND ciu_estado = 0
					AND mg.id_hotel = ".$id_hotel." AND mg.id_area = ".$id_area;
				$resmk = $db1->Execute($sqlmk) or die(__LINE__." ".$db1->ErrorMsg());

				$htmlreturn = "<td>".$resmk->Fields('ciu_nombre')."</td>";
				$htmlreturn.="<td>".$resmk->Fields('hot_nombre')."</td>";
				$htmlreturn.="<td>".$resmk->Fields('area_nombre')."</td>";
				$htmlreturn.="<td><input type='text' name='mk_h".$resmk->Fields('id_hotel')."_a".$resmk->Fields('id_area')."' id='mk_h".$resmk->Fields('id_hotel')."_a".$resmk->Fields('id_area')."' value='".$resmk->Fields('markdin_valor')."'></td>";
				$htmlreturn.="<td>";
				$htmlreturn.="<select name='estado_h".$resmk->Fields('id_hotel')."_a".$resmk->Fields('id_area')."' id='estado_h".$resmk->Fields('id_hotel')."_a".$resmk->Fields('id_area')."'>";
				$htmlreturn.="<option value='1' ";
				if($resmk->Fields('markdin_estado')==1){
					$htmlreturn.= " SELECTED";
				}
				$htmlreturn.=">Inactivo</option>";
				$htmlreturn.="<option value='0'";
				if($resmk->Fields('markdin_estado')==0){
					$htmlreturn.=" SELECTED";
				}
				$htmlreturn.=">Activo</option>";
				$htmlreturn.="</select>";
				$htmlreturn.="</td>";
				$htmlreturn.="<td><input type='button' id='btnupmk' onClick='upmkhot(\"h".$resmk->Fields('id_hotel')."_a".$resmk->Fields('id_area')."\")' value='Actualizar'/></td>";
				
				$this->opDebug(__FUNCTION__);
				return $htmlreturn;
		
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function dar_vuelta_fec($fec){
		$funcAuxFec = explode("-", $fec);
		return($funcAuxFec[2]."-".$funcAuxFec[1]."-".$funcAuxFec[0]);
	}

	public function checkDateFormat($fec){
		$auxvar = explode("-", $fec);
		if(strlen($auxvar[0])==4){
			return $fec;
		}else{
			return $this->dar_vuelta_fec($fec);
		}
	}
	
	
	
	
	public function loadhtmlgroups($db1, $idgrupo = null, $usafec=false,$fec1=null,$fec2=null, $nombre=''){
		try{
			$this->opDebug(__FUNCTION__);
			$condsql="";
			if(str_replace(" ", "", $nombre)!=""){
				$condsql.=" AND mkg_nombre like '%".$nombre."%'";
			}

			if($usafec==1){
				$condsql.=" AND mkg_fecdesde >= '".dar_vuelta_fec($fec1)."' AND mkg_fechasta <= '".dar_vuelta_fec($fec2)."'";
			}

			if($idgrupo!=''){
				$condsql.=" AND id_mkgrupo = ".$idgrupo;
			}
			$sqlgroups = "SELECT 
							id_mkgrupo, 
							mkg_nombre, 
							date_format(mkg_fecdesde, '%d-%m-%Y') as fec1, 
							date_format(mkg_fechasta, '%d-%m-%Y') as fec2, 
							mkg_markup, 
							mkg_estado,
							hot_nombre,
							IFNULL(CONCAT(hd.id_hotdet, ' - ', th_nombre, ' ', tt_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')', ' sgl: ', hd_sgl),'NO ASIGNADA')	 AS nom_hd
							FROM 
							".$this->dbMain.".mk_grupo mkg 
							INNER JOIN ".$this->dbMain.".hotel h ON mkg.id_hotel = h.id_hotel
							LEFT JOIN ".$this->dbMain.".hotdet hd ON mkg.id_hotdet = hd.id_hotdet
							LEFT JOIN ".$this->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
							LEFT JOIN ".$this->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion 
							WHERE 1 = 1 
							$condsql 
							ORDER BY mkg_nombre ASC";
			//echo $sqlgroups;
			$resgroups = $db1->Execute($sqlgroups) or die(__LINE__." ".$db1->ErrorMsg());
			$htmlreturn="<tr><td>Nombre</td><td>Fecha desde</td><td>Fecha Hasta</td><td>Hotel</td><td>Tarifa</td><td>markup</td><td>N Opes</td><td>Estado</td><td>&nbsp;</td><td>&nbsp;</td></tr>";

			if($resgroups->RecordCount()>0){
				while (!$resgroups->EOF){

					$sqlconteo = "SELECT COUNT(mk_id) AS conteo FROM ".$this->dbMain.".mk_ope_hotdet WHERE mk_estado = 0 AND id_mkgrupo =".$resgroups->Fields('id_mkgrupo');
					$resconteo = $db1->Execute($sqlconteo) or die(__LINE__." ".$db1->ErrorMsg());


					$htmlreturn.="<tr>";
					$htmlreturn.="<td>".$resgroups->Fields('mkg_nombre')."</td>";
					$htmlreturn.="<td>".$resgroups->Fields('fec1')."</td>";
					$htmlreturn.="<td>".$resgroups->Fields('fec2')."</td>";
					$htmlreturn.="<td>".$resgroups->Fields('hot_nombre')."</td>";
					$htmlreturn.="<td>".$resgroups->Fields('nom_hd')."</td>";
					$htmlreturn.="<td>".$resgroups->Fields('mkg_markup')."</td>";
					$htmlreturn.="<td>".$resconteo->Fields('conteo')."</td>";
					$htmlreturn.="<td>";
					if($resgroups->Fields('mkg_estado')==0){
						$htmlreturn.="Activo";
					}
					if($resgroups->Fields('mkg_estado')==1){
						$htmlreturn.="Inactivo";
					}
					$htmlreturn.="</td>";
					$htmlreturn.="<td><input type='button' name='btnupgroup' id='btnupgroup' onClick='editgroup(".$resgroups->Fields('id_mkgrupo').")' value='Editar grupo'></td>";
					$htmlreturn.="<td><input type='button' name='btneditopegroup' id='btneditopegroup' onClick='editopegroup(".$resgroups->Fields('id_mkgrupo').")' value='Editar Operadores'></td>";
					$htmlreturn.="</tr>";
					$resgroups->MoveNext();
				}
			}else{
				$htmlreturn.="<tr><td colspan='9'> No se encontraron resultados</td></tr>";
			}
			$this->clDebug();
			return $htmlreturn;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function loadhtmlgrupo($db1, $idgrupo=null){
		try{
			$this->opDebug(__FUNCTION__);
		
			$sqlhots = "SELECT * FROM ".$this->dbMain.".hotel WHERE hot_estado = 0 ORDER BY hot_nombre ASC";
			$reshots = $db1->Execute($sqlhots) or die(__LINE__." ".$db1->ErrorMsg());
			$flag = false;
			if($idgrupo!=null){
				$sqlgroup = "SELECT * FROM ".$this->dbMain.".mk_grupo WHERE id_mkgrupo = ".$idgrupo;
				$resgroup = $db1->Execute($sqlgroup) or die(__LINE__." ".$db1->ErrorMsg());
				$flag = true;
			}


			$htmlreturn="<tr><th colspan='4'>Grupo</th></tr>";
			$htmlreturn.="<tr>";
			$htmlreturn.="<td>Nombre:</td>";
			$htmlreturn.="<td><input type='text' name='ngroupname' id='ngroupname' ";
			if($flag){
				$htmlreturn.=" value='".$resgroup->Fields('mkg_nombre')."'";
			}
			$htmlreturn.="></td>";
			$htmlreturn.="<td>Markup grupo:</td>";
			$htmlreturn.="<td><input type='text' name='markgrupo' id='markgrupo' ";
			if($flag){
				$htmlreturn.=" value='".$resgroup->Fields('mkg_markup')."'";
			}
			$htmlreturn.="></td>";
			$htmlreturn.="</tr>";
			$htmlreturn.="<tr>";
			$htmlreturn.="<td>Fecha desde:</td>";
			$htmlreturn.="<td><input type='text' name='newfec1' id='newfec1' ";
			if($flag){
				$htmlreturn.=" value='".dar_vuelta_fec($resgroup->Fields('mkg_fecdesde'))."'";
			}
			$htmlreturn.="></td>";
			$htmlreturn.="<td>Fecha hasta:</td>";
			$htmlreturn.="<td><input type='text' name='newfec2' id='newfec2' ";
			if($flag){
				$htmlreturn.=" value='".dar_vuelta_fec($resgroup->Fields('mkg_fechasta'))."'";
			}
			$htmlreturn.="></td>";
			$htmlreturn.="</tr>";
			$htmlreturn.="<tr>";
			$htmlreturn.="<td>Hotel</td>";
			$htmlreturn.="<td>";
			$htmlreturn.="<select name='hotgroup' id='hotgroup'>";
			while (!$reshots->EOF) {
				$htmlreturn.="<option value='".$reshots->Fields('id_hotel')."'";
				if($flag){
					if($reshots->Fields('id_hotel')==$resgroup->Fields('id_hotel')){
						$htmlreturn.=" SELECTED";
					}
				}

				$htmlreturn.=">".$reshots->Fields('hot_nombre')."</option>";
				$reshots->MoveNext();
			}
			$htmlreturn.="</select>";
			$htmlreturn.="</td>";
			$htmlreturn.="<td>Tarifa</td>";
			$htmlreturn.="<td>";
			$htmlreturn.="<select name='hdgrupo' id='hdgrupo'>";
			if($flag){
				$sqlhotdet = "SELECT id_hotdet, CONCAT(th_nombre, ' ', tt_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')', 'sgl: ', hd_sgl) AS tarifa FROM ".$this->dbMain.".hotdet hd
						LEFT JOIN ".$this->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
						LEFT JOIN ".$this->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion 
						WHERE hd_estado = 0
						AND hd_fechasta > NOW() AND id_hotel = ".$resgroup->Fields('id_hotel');
				$reshotdet = $db1->Execute($sqlhotdet) or die(__LINE__." ".$db1->ErrorMsg());
				$htmlreturn.="<option value='0'>Sin Asignar</option>";
				while(!$reshotdet->EOF){
					$htmlreturn.="<option value='".$reshotdet->Fields('id_hotdet')."'";
					if($reshotdet->Fields('id_hotdet')==$resgroup->Fields('id_hotdet')){
						$htmlreturn.=" SELECTED";
					}
					$htmlreturn.=">".$reshotdet->Fields('tarifa')."</option>";
					$reshotdet->MoveNext();
				}
			}
			$htmlreturn.="</select>";
			$htmlreturn.="</td>";
			$htmlreturn.="</tr>";
			$htmlreturn.="<tr>";
			$htmlreturn.="<td>Estado:</td>";
			$htmlreturn.="<td><select name='groupstate' id='groupstate'>
			<option value='1'";
				if($flag){
					if($resgroup->Fields('mkg_estado')==0){
						$htmlreturn.=" SELECTED";
					}
				}
				$htmlreturn.=">Inactivo</option>
			<option value='0'";
				if($flag){
					if($resgroup->Fields('mkg_estado')==0){
						$htmlreturn.=" SELECTED";
					}
				}
				$htmlreturn.=">Activo</option>
			</select></td>";
			$this->clDebug();
			return $htmlreturn;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
		
		
	}
	
	public function loadopesxgroup($db1, $idgrupo){
		try{
			$this->opDebug(__FUNCTION__);
	
					$sqlopesgroup = "SELECT * FROM ".$this->dbMain.".mk_ope_hotdet moh INNER JOIN ".$this->dbMain.".hotel h on moh.id_hotel = h.id_hotel WHERE mk_estado = 0 AND moh.id_mkgrupo = ".$idgrupo." ORDER BY hot_nombre ASC";
					$resopesgroup = $db1->Execute($sqlopesgroup) or die(__LINE__." ".$db1->ErrorMsg());

					$sqlgrupo = "SELECT 
						id_mkgrupo, 
						mkg_nombre, 
						date_format(mkg_fecdesde, '%d-%m-%Y') as fec1, 
						date_format(mkg_fechasta, '%d-%m-%Y') as fec2, 
						mkg_markup, 
						mkg_estado,
						hot_nombre,
						IFNULL(CONCAT(hd.id_hotdet, ' - ', th_nombre, ' ', tt_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')'),'NO ASIGNADA')	 AS nom_hd
						FROM 
						".$this->dbMain.".mk_grupo mkg 
						INNER JOIN ".$this->dbMain.".hotel h ON mkg.id_hotel = h.id_hotel
						LEFT JOIN ".$this->dbMain.".hotdet hd ON mkg.id_hotdet = hd.id_hotdet
						LEFT JOIN ".$this->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
						LEFT JOIN ".$this->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion 
						WHERE id_mkgrupo = ".$idgrupo;
					$resgrupo = $db1->Execute($sqlgrupo) or die(__LINE__." ".$db1->ErrorMsg());



					$htmlreturn="<table border='1'><tr><th>Operadores en el Grupo '".$resgrupo->Fields('mkg_nombre')."'</th><th>mk: ".$resgrupo->Fields('mkg_markup')."</th>";
					$htmlreturn.="<th>".$resgrupo->Fields('hot_nombre')."</th><th>tar: ".$resgrupo->Fields('nom_hd')."</th>";
					$htmlreturn.="<th>".$resgrupo->Fields('fec1')." al ".$resgrupo->Fields('fec2')."</th><tr>";
					$htmlreturn.="</table>";
					$htmlreturn.="<br><table border='1'>";

					while (!$resopesgroup->EOF){
						$htmlreturn.="<tr>";
						$htmlreturn.="<td colspan='4'>".$resopesgroup->Fields('hot_nombre')."</td>";
						$htmlreturn.="<td><input type='button' name='btnquitope' id='btnquitope' value='Quitar' onClick='quitopefromgroup(".$resopesgroup->Fields('mk_id').")'></td>";
						$htmlreturn.="</tr>";
						$resopesgroup->MoveNext();
					}
					$htmlreturn.="</table>";
					$this->clDebug();
					return $htmlreturn;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
			
			
	}
	
	public function loadhtmlmkesp($db1, $idmkesp = null){					
		try{
			$this->opDebug(__FUNCTION__);
			

					if($idmkesp!=NULL){
						$sqlmkesp = "SELECT *, date_format(mk_fecdesde, '%d-%m-%Y') as fec1, 
								date_format(mk_fechasta, '%d-%m-%Y') as fec2 FROM ".$this->dbMain.".mk_ope_hotdet WHERE mk_id = ".$idmkesp;
						$resmkesp = $db1->Execute($sqlmkesp) or die(__LINE__." ".$db1->ErrorMsg());


						$sqlhotdet = "SELECT id_hotdet, CONCAT(th_nombre, ' ', tt_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')', 'sgl: ', hd_sgl) AS tarifa FROM ".$this->dbMain.".hotdet hd
							INNER JOIN ".$this->dbhot.".tipotarifa tt ON
							hd.idpk_tipotarifa = tt.id_tipotarifa
							INNER JOIN ".$this->dbhot.".tipohabitacion th ON
							hd.idpk_tipohabitacion = th.id_tipohabitacion
							WHERE hd_estado = 0
							AND hd_fechasta > NOW() AND id_hotel = ".$resmkesp->Fields('id_hotel');
						$reshotdet = $db1->Execute($sqlhotdet) or die(__LINE__." ".$db1->ErrorMsg());			
					} 

					$condhot = "";
					if($idmkesp!=null){
						$condhot = " AND id_hotel = ".$resmkesp->Fields('id_hotel');
					}
					$sqlhots = "SELECT 
						id_hotel, hot_nombre 
						FROM
						".$this->dbMain.".hotel h 
						INNER JOIN ".$this->dbMain.".ciudad ciu 
						ON h.id_ciudad = ciu.id_ciudad 
						INNER JOIN ".$this->dbMain.".pais p 
						ON ciu.id_pais = p.id_pais 
						WHERE hot_estado = 0 $condhot ORDER BY hot_nombre ASC"; 
					$reshots = $db1->Execute($sqlhots) or die(__LINE__." ".$db1->ErrorMsg());

					$condope="";
					if($idmkesp!=null){
						$condope = " AND id_agencia = ".$resmkesp->Fields('id_agencia');
					}
					$sqlopes = "SELECT 
						id_agencia, ag_nombre 
						FROM
						".$this->dbMain.".agencia h 
						
						INNER JOIN ".$this->dbMain.".pais p 
						ON h.id_pais = p.id_pais 
						WHERE ag_estado = 0 $condope ORDER BY ag_nombre ASC";
					$resopes = $db1->Execute($sqlopes) or die(__LINE__." ".$db1->ErrorMsg());
					


					$htmlreturn="";

					$htmlreturn.="<tr><th colspan='6'>Markup especifico</th></tr>";

					$htmlreturn.="<tr>";
					$htmlreturn.="<td>Operador</td>";
					$htmlreturn.="<td><select id='newopeesp' name='newopeesp'>";
					while(!$resopes->EOF){
						$htmlreturn.="<option value='".$resopes->Fields('id_agencia')."'";
						$htmlreturn.=">".$resopes->Fields('ag_nombre')."</option>";
						$resopes->MoveNext();
					}
					$htmlreturn.="</select></td>";
					$htmlreturn.="<td>Hotel</td>";
					$htmlreturn.="<td><select id='newhotesp' name='newhotesp'>";
					while(!$reshots->EOF) {
						$htmlreturn.="<option value='".$reshots->Fields('id_hotel')."'";
						
						$htmlreturn.=">".$reshots->Fields('hot_nombre')."</option>";
						$reshots->MoveNext();
					}
					$htmlreturn.="</select></td>";
					$htmlreturn.="<td>Tarifa</td>";
					$htmlreturn.="<td><select id='newtaresp'>";
					if($idmkesp!=null){
						while(!$reshotdet->EOF){
							$htmlreturn.="<option value='".$reshotdet->Fields('id_hotdet')."'";
							if($reshotdet->Fields('id_hotdet')==$resmkesp->Fields('id_hotdet')){
								$htmlreturn.=" SELECTED";
							}
							$htmlreturn.=" >".$reshotdet->Fields('tarifa')."</option>";
							$reshotdet->MoveNext();
						}
					}
					$htmlreturn.="</select></td>";
					$htmlreturn.="</tr>";
					$htmlreturn.="<tr>";
					$htmlreturn.="<td>Costo SGL</td>";
					$htmlreturn.="<td><input type='text' name='costnewsgl' id='costnewsgl' ";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_costo_sgl')."'";
					}
					$htmlreturn.= " disabled></td>";
					$htmlreturn.="<td>Markup SGL</td>";
					$htmlreturn.="<td><input type='text' name='mknewsgl' id='mknewsgl'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_markup_sgl')."'";
					}
					$htmlreturn.="></td>";
					$htmlreturn.="<td>Venta SGL</td>";
					$htmlreturn.="<td><input type='text' name='ventanewsgl' id='ventanewsgl'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".($resmkesp->Fields('mk_costo_sgl')/$resmkesp->Fields('mk_markup_sgl'))."'";
					}
					$htmlreturn.=">&nbsp;<label id='lblsgl'></label></td>";
					$htmlreturn.="</tr>";
					$htmlreturn.="<tr>";
					$htmlreturn.="<td>Costo DBT</td>";
					$htmlreturn.="<td><input type='text' name='costnewdbt' id='costnewdbt'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_costo_dbt')."'";
					}
					$htmlreturn.=" disabled></td>";
					$htmlreturn.="<td>Markup DBT</td>";
					$htmlreturn.="<td><input type='text' name='mknewdbt' id='mknewdbt'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_markup_dbt')."'";
					}
					$htmlreturn.="></td>";
					$htmlreturn.="<td>Venta DBT</td>";
					$htmlreturn.="<td><input type='text' name='ventanewdbt' id='ventanewdbt'";
					if($idmkesp!=null){
						$htmlreturn.="value='".($resmkesp->Fields('mk_costo_dbt')/$resmkesp->Fields('mk_markup_dbt'))."'";
					}
					$htmlreturn.=">&nbsp;<label id='lbldbt'></label></td>";
					$htmlreturn.="</tr>";
					$htmlreturn.="<tr>";
					$htmlreturn.="<td>Costo DBM</td>";
					$htmlreturn.="<td><input type='text' name='costnewdbm' id='costnewdbm'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_costo_dbm')."'";
					}
					$htmlreturn.=" disabled></td>";
					$htmlreturn.="<td>Markup DBM</td>";
					$htmlreturn.="<td><input type='text' name='mknewdbm' id='mknewdbm'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_markup_dbm')."'";
					}
					$htmlreturn.="></td>";
					$htmlreturn.="<td>Venta DBM</td>";
					$htmlreturn.="<td><input type='text' name='ventanewdbm' id='ventanewdbm'";
					if($idmkesp!=null){
						$htmlreturn.="value='".($resmkesp->Fields('mk_costo_dbm')/$resmkesp->Fields('mk_markup_dbm'))."'";
					}
					$htmlreturn.=">&nbsp;<label id='lbldbm'></label></td>";
					$htmlreturn.="</tr>";
					$htmlreturn.="<td>Costo TPL</td>";
					$htmlreturn.="<td><input type='text' name='costnewtpl' id='costnewtpl'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_costo_tpl')."'";
					}
					$htmlreturn.=" disabled></td>";
					$htmlreturn.="<td>Markup TPL</td>";
					$htmlreturn.="<td><input type='text' name='mknewtpl' id='mknewtpl'";
					if($idmkesp!=null){
						$htmlreturn.=" value='".$resmkesp->Fields('mk_markup_tpl')."'";
					}
					$htmlreturn.="></td>";
					$htmlreturn.="<td>Venta TPL</td>";
					$htmlreturn.="<td><input type='text' name='ventanewtpl' id='ventanewtpl'";
					if($idmkesp!=null){
						$htmlreturn.="value='".($resmkesp->Fields('mk_costo_tpl')/$resmkesp->Fields('mk_markup_tpl'))."'";
					}
					$htmlreturn.=">&nbsp;<label id='lbltpl'></label></td>";
					$htmlreturn.="</tr>";
					$htmlreturn.="<tr>";
					$htmlreturn.="<td>Fecha desde</td>";
					$htmlreturn.="<td><input type='text' name='newespfec1' id='newespfec1'";
					if($idmkesp!=null){
						$htmlreturn.= "value='".$resmkesp->Fields('fec1')."'";
					}
					$htmlreturn.="></td>";
					$htmlreturn.="<td>Fecha hasta</td>";
					$htmlreturn.="<td><input type='text' name='newespfec2' id='newespfec2'";
					if($idmkesp!=null){
						$htmlreturn.= "value='".$resmkesp->Fields('fec2')."'";
					}
					$htmlreturn.="></td>";
					if($idmkesp!=null){
						$htmlreturn.="<td colspan='2'>
							<table border='1' width='100%'>
							<tr>
							<td align='center'><select name='estmkesp' id='estmkesp'>


							<option value='0'";
							if($resmkesp->Fields('mk_estado')==0){
								$htmlreturn.=" SELECTED";
							}
							$htmlreturn.=">Activo</option>
							<option value='1'";
							if($resmkesp->Fields('mk_estado')==1){
								$htmlreturn.=" SELECTED";
							}
							$htmlreturn.=">Inactivo</option>
							</select></td>
							<td align='center'><input type='button' name='btnupdatemkesp' id='btnupdatemkesp' onClick='updatemkesp(".$idmkesp.")' value='Actualizar'></td>
							</tr>
							</table></td>";
						//$htmlreturn.="<td colspan='2' align='center'><input type='button' name='btnupdatemkesp' id='btnupdatemkesp' onClick='updatemkesp(".$idmkesp.")' value='Actualizar'></td>";
					}else{
						$htmlreturn.="<td colspan='2' align='center'><input type='button' name='btninsnewmkesp' id='btninsnewmkesp' onClick='insnewmkesp()' value='Insertar'></td>";
					}
					$htmlreturn.="</tr>";
					$this->clDebug();
					return $htmlreturn;
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function loadopexesp($db1, $paiope = 0, $paihot = 0, $idope = 0, $idhot = 0, $nomope = '', $nomhot = '', $usafec = false, $fec1 = '', $fec2 = ''){
				
		try{
			$this->opDebug(__FUNCTION__);		
				
				$sqlfin = "SELECT 
					  mk_id,
					  ope.ag_nombre AS ope_nombre,
					  hot.hot_nombre AS hot_nombre,
					  DATE_FORMAT(mk_fecdesde, '%d-%m-%Y') AS fec1,
					  DATE_FORMAT(mk_fechasta, '%d-%m-%Y') AS fec2,
					  mk_markup_sgl AS mksgl,
					  mk_markup_dbt AS mkdbt,
					  mk_markup_dbm AS mkdbm,
					  mk_markup_tpl AS mktpl,
					  tt_nombre,
					  th_nombre,
					  hd_sgl,
					  hd_estado,
					  hd.id_hotdet 
					FROM
					 ".$this->dbMain.".mk_ope_hotdet mk 
					  INNER JOIN ".$this->dbMain.".agencia ope 
						ON mk.id_agencia = ope.id_agencia 
					  INNER JOIN ".$this->dbMain.".hotel hot 
						ON mk.id_hotel = hot.id_hotel 
					  INNER JOIN ".$this->dbMain.".ciudad ciuhot 
						ON hot.id_ciudad = ciuhot.id_ciudad 
					  LEFT JOIN ".$this->dbMain.".hotdet hd 
						ON mk.id_hotdet = hd.id_hotdet 
					  LEFT JOIN ".$this->dbhot.".tipotarifa tt ON
						hd.idpk_tipotarifa = tt.id_tipotarifa
					  LEFT JOIN ".$this->dbhot.".tipohabitacion th ON
						hd.idpk_tipohabitacion = th.id_tipohabitacion
					  WHERE  mk_estado = 0";
				if($paiope!=0){
					$sqlfin.=" AND ope.id_pais = ".$paiope;
				}
				if($paihot!=0){
					$sqlfin.=" AND ciuhot.id_pais = ".$paihot;
				}
				if($idope!=0){
					$sqlfin.=" AND ope.id_agencia = ".$idope;
				}
				if($idhot!=0){
					$sqlfin.=" AND hot.id_hotel = ".$idhot;
				}
				if(trim($nomope)!=""){ 
					$sqlfin.=" AND ope.ag_nombre like '%".$nomope."%'";
				}
				if(trim($nomhot)!=""){
					$sqlfin.=" AND hot.hot_nombre like '%".$nomhot."%'";
				}
				if($usafec=='true'){
					$sqlfin.=" AND (mk_fecdesde BETWEEN '".dar_vuelta_fec($fec1)."' AND '".dar_vuelta_fec($fec2)."'";
					$sqlfin.=" OR  mk_fechasta BETWEEN '".dar_vuelta_fec($fec1)."' AND '".dar_vuelta_fec($fec2)."')";
				}else{
					$sqlfin.= " AND mk_fechasta >= NOW()";
				}
				
				//echo $sqlfin."<br><br>";
				$resfin = $db1->Execute($sqlfin) or die(__LINE__." ".$db1->ErrorMsg());

				$htmlreturn="";
				$htmlreturn="<thead><tr><th>Hotel</th><th>Operador</th><th>Tarifa</th><th>Vigencia</th><th>mksgl</th><th>mkdbt</th><th>mkdbm</th><th>mktpl</th><th>&nbsp;</th></tr></thead><tbody>";
				while(!$resfin->EOF){
					if($resfin->Fields('hd_estado')==1){
						$htmlreturn.="<tr style='background:#e83a4a;'>";
					}else{
						$htmlreturn.="<tr>";
					}
					$htmlreturn.="<td>".$resfin->Fields('hot_nombre')."</td>";
					$htmlreturn.="<td>".$resfin->Fields('ope_nombre')."</td>";
					$htmlreturn.="<td>";
					if($resfin->Fields('tt_nombre')!=""){
						$htmlreturn.= "(".$resfin->Fields('id_hotdet').") ".$resfin->Fields('tt_nombre')." - ".$resfin->Fields('th_nombre')." SGL: ".$resfin->Fields('hd_sgl');
					}else{
						$htmlreturn.="No Asignada";
					}
					$htmlreturn.="</td>";
					$htmlreturn.="<td>".$resfin->Fields('fec1')." al ".$resfin->Fields('fec2')."</td>";
					$htmlreturn.="<td>".$resfin->Fields('mksgl')."</td>";
					$htmlreturn.="<td>".$resfin->Fields('mkdbt')."</td>";
					$htmlreturn.="<td>".$resfin->Fields('mkdbm')."</td>";
					$htmlreturn.="<td>".$resfin->Fields('mktpl')."</td>";
					$htmlreturn.="<td><input type='button' name='modmkespbtn' id='modmkespbtn' onClick='modmkesp(".$resfin->Fields('mk_id').")' value='Modificar'></td>";
					$htmlreturn.="</tr>";
					$resfin->MoveNext();
				}

				$htmlreturn.="</tbody>";
				$this->clDebug();
				return $htmlreturn;
				
		}catch(Exception $e) {
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function trae_markup_hot($db1,$area,$ciudad,$hotname){ 
		
		try{
			$this->opDebug(__FUNCTION__);
		
				$condsql = "";

					if($area!=0){
						$condsql.=" AND a.id_area = ".$area;
					}
					if($ciudad!=0){
						$condsql.= " AND h.id_ciudad = ".$ciudad;
					}
					if(str_replace(" ", "", $hotname)!=""){
						$condsql.=" AND hot_nombre like '%".$hotname."%' ";
					}
					
					
					$sqlmkhot = "SELECT h.id_hotel, hot_nombre, a.id_area, a.area_nombre, id_markdin, markdin_valor, markdin_estado, ciu_nombre, h.id_ciudad FROM ".$this->dbMain.".hotel h 
						INNER JOIN ".$this->dbMain.".area a 
						LEFT JOIN ".$this->dbMain.".markdin_gral mg ON 
						h.id_hotel = mg.id_hotel AND a.id_area = mg.id_area
						INNER JOIN ".$this->dbMain.".ciudad ciu ON
						h.id_ciudad = ciu.id_ciudad
						WHERE  hot_estado = 0 
						AND ciu_estado = 0
						$condsql
						ORDER BY ciu_nombre ASC, hot_nombre ASC";
					$resmkhot = $db1->Execute($sqlmkhot) or die(__LINE__." ".$db1->ErrorMsg());
					
				$this->clDebug();
				return $resmkhot;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getImpuestoPais($db1,$id_pais, $fec = null){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlimpuesto = "SELECT * FROM impuestoxpais  WHERE impuesto_vigentedesde <= ".(($fec==null)?$fec:"NOW()")." AND id_pais = $id_pais ORDER BY impuesto_vigentedesde DESC ,impuesto_fecha DESC LIMIT 1";
			$resimpuesto = $db1->Execute($sqlimpuesto) or die(__LINE__." ".$db1->ErrorMsg());
			if($resimpuesto->RecordCount()>0){
				$impuesto = 1+($resimpuesto->Fields('impuesto_valor')/100);
			}else{
				$impuesto = 1;
			}
			$this->clDebug();
			return $impuesto;

		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function find_opes($db1,$pais=0,$opename="",$id_agencia=0){
		try{
			$this->opDebug(__FUNCTION__);	
			
			//die($pais);
				$condsql="";
				
				if($pais!=0){
					$condsql.=" AND h.id_pais = ".$pais;
				}
				if($id_agencia!=0){
					$condsql.=" AND h.id_agencia = ".$id_agencia;
				}
				if(str_replace(" ", "", $opename)!=""){
					$condsql.=" AND ag_nombre like '%".$opename."%'";
				}
				$sqlfinopes = "SELECT 
					id_agencia, ag_nombre, ag_markup, ag_mark_imp
					FROM
					".$this->dbMain.".agencia h
					
					
					WHERE ag_estado = 0 $condsql ORDER BY ag_nombre ASC";
			//	die($sqlfinopes);
				$resfindopes = $db1->Execute($sqlfinopes) or die(__LINE__." ".$db1->ErrorMsg());
				$this->clDebug();
				return $resfindopes;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function find_opes_dis($db1,$area=0,$pais=0,$nombre="",$grupo){
		try{
			$this->opDebug(__FUNCTION__);
		
			$condsql="";
			if($area!=0){
				$condsql.=" AND h.id_area = ".$area;
			}
			if($pais!=0){
				$condsql.=" AND ciu.id_pais = ".$pais;
			}
			if(str_replace(" ", "", $nombre)!=""){
				$condsql.=" AND hot_nombre like '%".$nombre."%'";
			}
			
			$sqlfinopes = "SELECT 
				id_agencia, ag_nombre, area_nombre, ciu_nombre
				FROM
				".$this->dbMain.".agencia h
				INNER JOIN ".$this->dbMain.".area a ON
				h.id_area = a.id_area
				INNER JOIN ".$this->dbMain.".ciudad ciu ON
				h.id_ciudad = ciu.id_ciudad
				WHERE ag_estado = 0 $condsql AND id_agencia NOT IN (SELECT id_agencia FROM mk_ope_hotdet WHERE id_mkgrupo = ".$grupo.")ORDER BY ag_nombre ASC";
			$resfindopes = $db1->Execute($sqlfinopes) or die(__LINE__." ".$db1->ErrorMsg());
			
			
			$this->clDebug();
			return $resfindopes;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	
	public function hoteles_paisociudad($db1,$id_pais=0,$id_ciudad=0,$id_hotel=0){
		try{
			$this->opDebug(__FUNCTION__);
				$sqlreq = "";
					
						if($id_pais!=0){
							$sqlreq = " AND p.id_pais = ".$id_pais;
						}
					
					
						if($id_ciudad!=0){
							$sqlreq.=" AND ciu.id_ciudad = ".$id_ciudad;
						}
						if($id_hotel!=0){
							$sqlreq.=" AND id_hotel = ".$id_hotel;
						}
						

					$sqlfindopes = "SELECT 
						hot_nombre, id_hotel
						FROM
						".$this->dbMain.".hotel h 
						INNER JOIN ".$this->dbMain.".ciudad ciu 
						ON h.id_ciudad = ciu.id_ciudad 
						INNER JOIN ".$this->dbMain.".pais p 
						ON ciu.id_pais = p.id_pais 
						WHERE  hot_estado = 0 $sqlreq ORDER BY hot_nombre ASC";
					$resfindopes = $db1->Execute($sqlfindopes) or die(__LINE__." ".$db1->ErrorMsg());
			
				$this->clDebug();
				return $resfindopes;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}
	
	public function traer_mon($db1){
		try{
			$this->opDebug(__FUNCTION__);
				$sqlmon = "SELECT * FROM ".$this->dbMain.".mon";
				$resmon = $db1->Execute($sqlmon) or die(__LINE__." ".$db1->ErrorMsg());
			$this->clDebug();
				return $resmon;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function trae_arreglocosto($db1,$id_tarifa){
		try{
			$this->opDebug(__FUNCTION__);
			
				$sqlhotdet = "SELECT *,DATE_FORMAT(hd_fecdesde, '%d-%m-%Y') AS fec1, DATE_FORMAT(hd_fechasta, '%d-%m-%Y') AS fec2,
				DATE_FORMAT(DATE_ADD(hd_fecdesde, INTERVAL 1 DAY), '%Y-%m-%d') AS desde,
				DATE_FORMAT(DATE_ADD(hd_fechasta, INTERVAL 1 DAY), '%Y-%m-%d') AS hasta
				 FROM ".$this->dbMain.".hotdet WHERE id_hotdet = ".$id_tarifa;
				$reshotdet = $db1->Execute($sqlhotdet) or die(__LINE__." ".$db1->ErrorMsg());
			
			$this->clDebug();
			return $reshotdet;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	
	public function trae_tarifas_mkgeneral($db1,$idhothd=0,$id_mon=0){
		try{
			$this->opDebug(__FUNCTION__);
				$condhot="";
				if($idhothd!=0){
					$condhot.= " AND hd.id_hotel = ".$idhothd;
				}
				if($id_mon!=0){
					$condmon.= " AND id_mon = ".$id_mon;
				}
				$sqlsearch = "SELECT id_hotdet,hot_nombre, CONCAT(tt_nombre, ' ', th_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')') AS tarifa,
					hd_sgl,
					hd_dbl,
					hd_tpl,
					hd_markup
					FROM ".$this->dbMain.".hotdet hd
					INNER JOIN ".$this->dbhot.".tipotarifa tt ON
					hd.idpk_tipotarifa = tt.id_tipotarifa
					INNER JOIN ".$this->dbhot.".tipohabitacion th ON
					hd.idpk_tipohabitacion = th.id_tipohabitacion
					INNER JOIN ".$this->dbMain.".hotel h ON
					hd.id_hotel = h.id_hotel
					WHERE hd_estado = 0
					AND hd_fechasta > NOW() ".$condhot." ORDER BY hot_nombre ASC, hd_fecdesde ASC";
					
				$ressearch = $db1->Execute($sqlsearch) or die(__LINE__." ".$db1->ErrorMsg());
			$this->clDebug();
			return $ressearch;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function trae_mkophd($db1,$id_hotdet){
		try{
			$this->opDebug(__FUNCTION__);
				$checkmk = "SELECT * FROM ".$this->dbMain.".mk_ope_hotdet WHERE mk_estado = 0 AND id_hotdet = ".$id_hotdet;
				$rescheckmk= $db1->Execute($checkmk) or die(__LINE__." ".$db1->ErrorMsg());
			$this->clDebug();
			return $rescheckmk;
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function insertamkhotelarea($db1,$id_hotel,$id_area,$mkhot,$id_usuario){
		try{
			$this->opDebug(__FUNCTION__);
			
				$sqlinsmk = "INSERT INTO ".$this->dbMain.".markdin_gral (id_hotel, id_area, markdin_valor, markdin_imperativo, markdin_estado)
							 VALUES (".$id_hotel.", ".$id_area.", ".$mkhot.",1,0)";
				$db1->Execute($sqlinsmk) or die(__LINE__." ".$db1->ErrorMsg());
				
				
				$id_log = $db1->Insert_ID();

				$sqllog = "INSERT INTO ".$this->dbMain.".log_markup (id_hotel, id_area, mk_sgl, mk_twin, mk_mat, mk_tpl, estado, tabla_target, fecha, jerarquia, descripcion, id_usuario, pk_target)
				VALUES (".$id_hotel.", ".$id_area.", ".$_POST['mkhot'].", ".$mkhot.", ".$mkhot.", ".$mkhot.", 0, 'markdin_gral', NOW(), 6, 'Inserta markup gral hotel x area', ".$id_usuario.",$id_log)";
				$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
				
		
			$this->clDebug();
			return true;
			
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}
	
	public function updatemkhotelarea($db1,$id_hotel,$id_area,$mkhot,$id_usuario,$estado){
		try{
			$this->opDebug(__FUNCTION__);
			
				$sqlupmk = "UPDATE ".$this->dbMain.".markdin_gral SET markdin_valor =".$mkhot.", markdin_estado = ".$_POST['mkestado']." WHERE id_area = ".$id_area." AND id_hotel = ".$id_hotel;
				$db1->Execute($sqlupmk) or die(__LINE__." ".$db1->ErrorMsg());

				$sqlgetpk = "SELECT id_markdin FROM ".$this->dbMain.".markdin_gral WHERE id_area = ".$id_area." AND id_hotel = ".$id_hotel;
				$resgetpk = $db1->SelectLimit($sqlgetpk) or die(__LINE__." ".$db1->ErrorMsg());			
				$id_log = $resgetpk->Fields('id_markdin');


				$sqllog = "INSERT INTO ".$this->dbMain.".log_markup (id_hotel, id_area, mk_sgl, mk_twin, mk_mat, mk_tpl, estado, tabla_target, fecha, jerarquia, descripcion, id_usuario, pk_target)
				VALUES (".$id_hotel.",".$id_area.", ".$mkhot.", ".$mkhot.", ".$mkhot.", ".$mkhot.", ".$estado.", 'markdin_gral', NOW(), 6, 'Modifica markup gral hotel x area', ".$id_usuario.", $id_log)";
				$db1->Execute($sqllog) or die($sqllog);
				
		
			$this->clDebug();
			return true;
			
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}
	
	public function updatemkagencia($db1,$agmarkup,$imperativo,$id_hotel){
		try{
			$this->opDebug(__FUNCTION__);
			
				$sqlupope = "UPDATE ".$this->dbMain.".agencia SET ag_markup = ".$agmarkup.", ag_mark_imp = ".$imperativo." WHERE id_agencia = ".$id_hotel;
					
				//	echo $sqlupope;
				//	die();
				$db1->Execute($sqlupope) or die(__LINE__." ".$db1->ErrorMsg());
				
				$sqllog = "INSERT INTO ".$this->dbMain.".log_markup (id_agencia, fecha, tabla_target, mk_sgl, mk_twin, mk_mat, mk_tpl, estado, jerarquia, descripcion, imperativo, pk_target)
				VALUES (".$id_hotel.", NOW(), 'hotel', ".$agmarkup.", ".$agmarkup.", ".$agmarkup.", ".$agmarkup.", 0,7,'Modifica markup gral de operador', ".$imperativo.",".$id_hotel.")";
				$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
			$this->clDebug();
			return true; 
		
		}catch(Exception $e){
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	

	public function getMkCompraOpaca($db1, $cliente, $ciudad, $id_area, $id_hotel = null){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlGetThatShit = "SELECT h.id_pk, mkco_markup FROM ".$cliente['bd'].".mk_compra_opaca mco INNER JOIN ".$this->dbMain.".hotel h ON mco.id_pk = h.id_pk WHERE id_ciudad = $ciudad AND id_area = $id_area";
			if($id_hotel!=null){
				$sqlGetThatShit.=" AND h.id_hotel = ".$id_hotel;
			}
			$resGetThatShit = $db1->Execute($sqlGetThatShit) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $resGetThatShit;


		}catch(Exception $e){ 
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
	}
	
	public function traetarifasoption($db1,$id_hotel){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlhotdet = "SELECT id_hotdet, CONCAT(th_nombre, ' ', tt_nombre, ' (', DATE_FORMAT(hd.hd_fecdesde, '%d-%m-%Y'), ' al ',DATE_FORMAT(hd.hd_fechasta, '%d-%m-%Y'),')', 'sgl: ', hd_sgl) AS tarifa FROM hotdet hd
					INNER JOIN tipotarifa tt ON
					hd.id_tipotarifa = tt.id_tipotarifa
					INNER JOIN tipohabitacion th ON
					hd.id_tipohabitacion = th.id_tipohabitacion
					WHERE hd_estado = 0
					AND hd_fechasta > NOW() AND id_hotel = ".$id_hotel;
				$reshotdet = $db1->SelectLimit($sqlhotdet) or die(__LINE__." ".$db1->ErrorMsg());
				$htmlreturn="<option value='0'>No Asignar</option>";
				while(!$reshotdet->EOF){
					$htmlreturn.="<option value='".$reshotdet->Fields('id_hotdet')."'>".$reshotdet->Fields('tarifa')."</option>";
					$reshotdet->MoveNext();
				}
			$this->clDebug();	
			return $htmlreturn;
		}catch(Exception $e){ 
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
		
	}
	
	public function ingresamkespecifico($db1,$idtar,$mksgl,$mkdbt,$mkdbm,$mktpl,$idhot,$idope,$fec1,$fec2,$id_usuario){
		try{
			$this->opDebug(__FUNCTION__);
		
			
				$csgl = 0;
			$cdbt = 0;
			$cdbm = 0;
			$ctpl = 0;
			$vsgl = 0;
			$vdbt = 0;
			$vdbm = 0;
			$vtpl = 0;
			$roundfactor = 0;
			$condtar="";
			if($idtar!=0){
				$jerarlog = 1;
				$condtar = $idtar;
				$desclog = 'Ingresa markup especifico operador - tarifa';
				$star = "SELECT id_hotdet, hd_sgl, hd_dbl, hd_tpl, hd_mon FROM hotdet where id_hotdet = ".$condtar;
				$rtar = $db1->SelectLimit($star) or die(__LINE__." ".$db1->ErrorMsg());

				if($rtar->Fields('hd_mon')==1){
					$roundfactor = 2;
				}

				$csgl = $rtar->Fields('hd_sgl');
				$cdbt = $rtar->Fields('hd_dbl');
				$cdbm = $rtar->Fields('hd_dbl');
				$ctpl = $rtar->Fields('hd_tpl');

				$vsgl = round($rtar->Fields('hd_sgl') / $mksgl, $roundfactor);
				$vdbt = round($rtar->Fields('hd_dbl') / $mkdbt, $roundfactor);
				$vdbm = round($rtar->Fields('hd_dbl') / $mkdbm, $roundfactor);
				$vtpl = round($rtar->Fields('hd_tpl') / $mktpl, $roundfactor);


			}else{
				$condtar="NULL";
				$jerarlog = 2;
				$desclog = 'Ingresa markup especifico operador - hotel';
			}
			
			$sqlvalida = "SELECT * FROM mk_ope_hotdet 
				WHERE id_hotdet = $condtar 
				AND id_hotel = ".$idhot." 
				AND id_agencia = ".$idope." 
				AND mk_estado = 0 AND 
					('".dar_vuelta_fec($fec1)."' BETWEEN mk_fecdesde AND mk_fechasta 
					OR '".dar_vuelta_fec($_POST['fec2'])."' BETWEEN mk_fecdesde AND mk_fechasta)";
			$resvalida = $db1->SelectLimit($sqlvalida) or die(__LINE__." ".$db1->ErrorMsg());

			if($resvalida->RecordCount() > 0){
				die("<?xml version='1.0' encoding='utf-8'?><response>no</response>");
			}



			$sqlins = "INSERT INTO mk_ope_hotdet 
				(id_hotdet,
				id_agencia, 
				id_hotel, 
				mk_fecdesde, 
				mk_fechasta, 
				mk_costo_sgl,
				mk_markup_sgl,
				mk_venta_sgl,
				mk_costo_dbt,
				mk_markup_dbt,
				mk_venta_dbt,
				mk_costo_dbm,
				mk_markup_dbm,
				mk_venta_dbm,
				mk_costo_tpl,
				mk_markup_tpl,
				mk_venta_tpl,
				mk_estado,
				id_mkgrupo)
				VALUES
				($condtar,
				".$idope.",
				".$idhot.",
				'".dar_vuelta_fec($fec1)."',
				'".dar_vuelta_fec($fec2)."',
				".$csgl.",
				".$mksgl.",
				".$vsgl.",
				".$cdbt.",
				".$mkdbt.",
				".$vdbt.",
				".$cdbm.",
				".$mkdbm.",
				".$vdbm.",
				".$ctpl.",
				".$mktpl.",
				".$vtpl.",
				0,
				NULL)";
			$db1->Execute($sqlins) or die(__LINE__." ".$db1->ErrorMsg());

			$idlog =$db1->Insert_ID();

			$sqllog = "INSERT INTO log_markup (id_tarifa, id_hotel, id_operador, fec_vig1, fec_vig2, mk_sgl, mk_twin, mk_mat, mk_tpl, tabla_target, jerarquia, fecha, id_usuario, estado, descripcion, pk_target)
			VALUES ($condtar, ".$idhot.", ".$idope.", '".dar_vuelta_fec($fec1)."','".dar_vuelta_fec($fec2)."', ".$mksgl.",".$mkdbt.",".$mkdbm.",".$mktpl.", 'mk_ope_hotdet', $jerarlog, NOW(), ".$id_usuario.",0,'".$desclog."', $idlog)";
            $db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
		
			$this->clDebug();	
			return true;
		
		
		}catch(Exception $e){ 
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}
		
		
	}
	
	
	public function updatemkespecifico($db1,$idtar,$mksgl,$mkdbt,$mkdbm,$mktpl,$idhot,$idope,$fec1,$fec2,$id_usuario,$newest,$imkesp){
		try{
			$this->opDebug(__FUNCTION__);
				$csgl = 0;
				$cdbt = 0;
				$cdbm = 0;
				$ctpl = 0;
				$vsgl = 0;
				$vdbt = 0;
				$vdbm = 0;
				$vtpl = 0;
				$roundfactor = 0;
				$condtar="";
				if($idtar!=0){
					$condtar = $idtar;
					$star = "SELECT id_hotdet, hd_sgl, hd_dbl, hd_tpl, id_mon FROM hotdet where id_hotdet = ".$condtar;
					$rtar = $db1->SelectLimit($star) or die(__LINE__." ".$db1->ErrorMsg());
					$jerarlog = 1;
					$desclog = 'Actualiza markup especifico operador - tarifa';

					if($rtar->Fields('id_mon')==1){
						$roundfactor = 2;
					}

					$csgl = $rtar->Fields('hd_sgl');
					$cdbt = $rtar->Fields('hd_dbl');
					$cdbm = $rtar->Fields('hd_dbl');
					$ctpl = $rtar->Fields('hd_tpl');

					$vsgl = round($rtar->Fields('hd_sgl') / $mksgl, $roundfactor);
					$vdbt = round($rtar->Fields('hd_dbl') / $mkdbt, $roundfactor);
					$vdbm = round($rtar->Fields('hd_dbl') / $mkdbm, $roundfactor);
					$vtpl = round($rtar->Fields('hd_tpl') / $mktpl, $roundfactor);
				}else{
					$condtar="NULL";
					$jerarlog = 2;
					$desclog = 'Actualiza markup especifico operador - hotel';
				}

				if($newest==0){
					$sqlvalida = "SELECT * FROM mk_ope_hotdet  
						WHERE id_hotdet = $condtar 
						AND id_hotel = ".$idhot." 
						AND id_agencia = ".$idope." 
						AND mk_estado = 0 AND 
							('".$this->dar_vuelta_fec($fec1)."' BETWEEN mk_fecdesde AND mk_fechasta 
						OR '".$this->dar_vuelta_fec($fec2)."' BETWEEN mk_fecdesde AND mk_fechasta)";
					die($sqlvalida);
					$resvalida = $db1->SelectLimit($sqlvalida) or die(__LINE__." ".$db1->ErrorMsg());

					if($resvalida->RecordCount() == 0){
						die("<?xml version='1.0' encoding='utf-8'?><response>no</response>");
					}
				}


				$sqlupdate = "UPDATE mk_ope_hotdet SET 
				id_agencia = ".$idope.", 
				id_hotel = ".$idhot.",
				mk_fecdesde = '".dar_vuelta_fec($fec1)."',
				mk_fechasta = '".dar_vuelta_fec($fec2)."',
				mk_costo_sgl = ".$csgl.",
				mk_markup_sgl = ".$mksgl.",
				mk_venta_sgl = ".$vsgl.",
				mk_costo_dbt = ".$cdbt.",
				mk_markup_dbt = ".$mkdbt.",
				mk_venta_dbt = ".$vdbt.",
				mk_costo_dbm = ".$cdbm.",
				mk_markup_dbm = ".$mkdbm.",
				mk_venta_dbm = ".$vdbm.",
				mk_costo_tpl = ".$ctpl.",
				mk_markup_tpl = ".$mktpl.",
				mk_venta_tpl = ".$vtpl.",
				mk_estado = ".$newest.",
				id_hotdet = $condtar,
				id_mkgrupo = NULL
				WHERE mk_id = ".$imkesp;
				
				$db1->Execute($sqlupdate) or die(__LINE__." ".$db1->ErrorMsg());

				$sqllog = "INSERT INTO log_markup (id_tarifa, id_hotel, id_operador, fec_vig1, fec_vig2, mk_sgl, mk_twin, mk_mat, mk_tpl, tabla_target, jerarquia, fecha, id_usuario, estado, descripcion, pk_target)
				VALUES ($condtar, ".$idhot.", ".$idope.", '".dar_vuelta_fec($fec1)."','".dar_vuelta_fec($fec2)."', ".$mksgl.",".$_mkdbt.",".$mkdbm.",".$mktpl.", 'mk_ope_hotdet', $jerarlog, NOW(), ".$id_usuario.",0,'$desclog', ".$imkesp.")";
				$db1->Execute($sqllog) or die(__LINE__." ".$db1->ErrorMsg());
			
			
			$this->clDebug();	
			return true;	
		}catch(Exception $e){ 
			echo $e->getMesagge()."<br>";
			var_dump($e);
			$this->clDebug();
		}	
	}
	
	//matriz reportes vendedor y admin
	public function rptMatrix($db1, $ciudad, $fechad, $fechah, $tipousu, $id_agencia, $habs, $byhotel=false, $id_hotel=null, $id_tipotarifa=null, $extranjero=null){
		try{
			$this->opDebug(__FUNCTION__);
			require_once("/var/www/otas/clases/agencia.php");
			$agencyClass = new Agencia();
			
			$agencyClass->debug = $this->debug;
			$arMk = array();
			$lookForMk = true;
			$agencia = $agencyClass->getMeThatAgency($db1,$id_agencia);

			//////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////
			$sglRequest=0;
			$twinRequest=0;
			$matRequest=0;
			$tplRequest=0;
			$fechad = $this->checkDateFormat($fechad);
			if(!is_numeric($fechah)){
				$fechah = $this->checkDateFormat($fechah);
				$fechaHasta = "ADDDATE('".$fechah."',-1)";
				$nightDiff="DATEDIFF('$fechah','$fechad')";
			}else{
				$fechaHasta = "ADDDATE('".$fechad."', ".($fechah-1).")";
				$nightDiff = $fechah;
			}
			
			$habsCond="";
			$condChild="";
			$closecond="";
			
			if(!$habs['sgl'] && !$habs['twin'] && !$habs['mat'] && !$habs['tpl']){
				$habs['sgl']=1;
				$habs['twin']=1;
				$habs['mat']=1;
				$habs['tpl']=1;
			}
			if(isset($habs['sgl'])){
				$closecond.=" AND ds_cerrado_sgl = 0";
				$habsCond.=" AND hd_sgl > 0 AND ds_hab1 >= 1 ";
				$sglRequest=$habs['sgl'];
			}
			if(isset($habs['twin'])){
				$closecond.=" AND ds_cerrado_twn = 0";
				$habsCond.=" AND hd_dbl > 0 AND ds_hab2 >= 1 ";
				$twinRequest=$habs['twin'];				
			}
			if(isset($habs['mat'])){
				$closecond.=" AND ds_cerrado_mat = 0";
				$habsCond.=" AND hd_dbl > 0 AND ds_hab3 >= 1 ";
				$matRequest=$habs['mat'];				
			}
			if(isset($habs['tpl'])){
				$closecond.=" AND ds_cerrado_tpl = 0";
				$habsCond.=" AND hd_tpl > 0 AND ds_hab4 >= 1 ";
				$tplRequest=$habs['tpl'];				
			}		
			
			$habsCond = "((hm.ver=0) OR (hm.ver=1 ".$habsCond."))";
			$moneda = '1';
			
			if($extranjero){
				$area = 1;
			}else{
				if($this->allowCurrencyConvertion){
					$moneda.=',2';
				}else{
					$moneda='2';
				}
				$area = 2;
			}

			$sqlDisp="SELECT d.id_dispo,
				hd.id_hotdet, h.id_hotel,hot_direccion, h.id_pk, ds_fecha,DATEDIFF(ds_fecha, '$fechad') AS dayPos, hd_sgl, hd_dbl, hd_tpl, hd_iva, hd_usamarkup, ds_hab1, ds_hab2, ds_hab3, ds_hab4, ds_minnoche, h.id_ciudad,idpk_tipohabitacion as id_tipohabitacion, idpk_tipotarifa as id_tipotarifa, hd.id_cliente, id_hotdet_cliente, hot_nombre, hot_email, tt_nombre, th_nombre,
				tt_espromo, tt_minnoc, tt_maxnoc, id_mon, hotxag_estado, ca.modo AS modo_global, hm.ver AS es_global, 
				hot_markdin,
				hd_markup,
				$nightDiff as daysDiff,
				tipocambio_gandhi as tcambio,
				d.id_stock_cli,
				IFNULL(ha_dias,".$this->cancelationDays.") as ha_dias,
				date_add('$fechad', interval -(IFNULL(ha_dias,".$this->cancelationDays.")) day) as ha_fec,
				hd_refundable,
				id_regimen
				FROM ".$this->dbMain.".hotdet hd 
				INNER JOIN ".$this->dbMain.".dispo d ON hd.id_hotdet = d.id_hotdet
				INNER JOIN ".$this->dbMain.".hotel h ON hd.id_hotel = h.id_hotel
				INNER JOIN ".$this->dbMain.".hotelxagencia ha ON h.id_hotel = ha.id_hotel AND hd.id_cliente = ha.id_cliente
				INNER JOIN ".$this->dbhot.".hotelesmerge hm ON h.id_pk = hm.id_pk 
				INNER JOIN ".$this->dbhot.".clientes c ON hd.id_cliente = c.id_cliente
				INNER JOIN ".$this->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
				INNER JOIN ".$this->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion
				INNER JOIN ".$this->dbhot.".cadena ca ON hm.id_cadena = ca.id_cadena
				INNER JOIN ".$this->dbMain.".ciudad ciu ON h.id_ciudad = ciu.id_ciudad
				LEFT JOIN  ".$this->dbMain.".hotanula af ON h.id_hotel = af.id_hotel
				WHERE tt_sellotas = 0
				AND (h.hot_habxvender >= ($sglRequest+$twinRequest+$matRequest+$tplRequest) OR h.hot_habxvender = 0)
				AND tt_estado = 0
				AND th_estado = 0
				AND th_sellotas = 0
				AND hd_estado = 0
				AND ha.id_agencia = $id_agencia
				AND hotxag_estado = 0
				AND hot_estado = 0
				AND hot_sellota = 0
				AND ds_estado = 0
				AND ds_cerrado=0
				AND c.estado = 0
				AND hd.id_mon IN ($moneda)
				AND c.trabaja_gandhi = 0
				AND h.id_ciudad = $ciudad
				AND ciu_estado = 0
				AND ds_minnoche <= $nightDiff
				AND (tt_maxnoc >= $nightDiff OR tt_maxnoc = 0)
				AND ds_fecha BETWEEN '$fechad' AND $fechaHasta
				AND ds_tarifa = 0
				AND hd_prepago = 0
				$closecond
				AND $habsCond";
			if($id_hotel != null){
				$sqlDisp.=" AND hd.id_hotel IN (".((is_array($id_hotel))?implode(",", $id_hotel):$id_hotel).")";
			}			
			$sqlDisp.=" AND ((tt.tt_espromo = 0 AND tt_minnoc <= $nightDiff)";
			if($this->allowPromos){
				$sqlDisp.=" OR(tt.tt_espromo = 1 AND tt_minnoc = $nightDiff)";
			}
			
			$sqlDisp.=")";
			if($byhotel){
				$sqlDisp.=" GROUP BY hd.id_hotel";
			}
			$sqlDisp.=" ORDER BY hot_nombre ASC, hd.id_cliente ASC, th.id_tipohabitacion ASC, id_hotdet ASC, ds_fecha ASC";
			$resDisp = $db1->Execute($sqlDisp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($resDisp->RecordCount()>0){
				$daysDiff = $resDisp->Fields('daysDiff');
				$insertOnMain = false;
				$unsetHot = false;
				$curHot = -1;
				$curCli = -1;
				$hotName="";
				$penalty = 0;
				$arreglo = array();
				$arAux = array();
				$promos = array();
				$keeplooking = false;
				$ivaNational = $this->getImpuestoPais($db1,$this->nationalCountryID, $fechad);
				while(!$resDisp->EOF){					
					$valiDay = false;
					$tienedisp=(($resDisp->Fields('ds_hab1')>=$sglRequest) && ($resDisp->Fields('ds_hab2')>=$twinRequest) && ($resDisp->Fields('ds_hab3')>=$matRequest) && ($resDisp->Fields('ds_hab4')>=$tplRequest));
					//esta basura tiene que reiniciarse cuando cambia de habitacion o cliente u hotel.
					if($resDisp->Fields('ds_minoche') >= $resDisp->Fields('tt_minnoc')){
						$minimoNoches = $resDisp->Fields('ds_minoche');
					}else{
						$minimoNoches = $resDisp->Fields('tt_minnoc');
					}
					//////////////////////////////////////////////////////////////////////////
					/////////////////////////////Dispo Global 2.0/////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					$usaGlobal = false;
					$singleNeed = 0;
					$twinNeed = 0;
					$matNeed = 0;
					$tplNeed = 0;
					if(!$tienedisp && $resDisp->Fields('es_global')==0){
						if(!isset($globDisp[$resDisp->Fields('id_pk')])){
							//query global 2.0... trae todas las tarifas autorizadas para venta del hotel y las deja guardadas en el arreglo
							$sqlGlob = "SELECT sc.id_stock_global,
								sc.id_tipohab,
								sc_fecha,
								sc.id_pk,
								sc_minnoche,
								sc_hab1 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab1,0),0))) AS ds_hab1,
								sc_hab2 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab2,0),0))) AS ds_hab2,
								sc_hab3 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab3,0),0))) AS ds_hab3,
								sc_hab4 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab4,0),0))) AS ds_hab4
								FROM ".$this->dbhot.".stock_global sc 
								LEFT JOIN ".$this->dbhot.".hotocu hc ON
								sc.id_stock_global = hc.id_stock_global 
								INNER JOIN ".$this->dbhot.".tipohabitacion th ON
								sc.id_tipohab = th.id_tipohabitacion 
								INNER JOIN ".$this->dbhot." .hotdet hd ON
								sc.id_hotdet = hd.id_hotdet
								WHERE sc_estado = 0
								AND sc_cerrado = 0 
								AND hd_estado = 0
								AND sc.id_pk = ".$resDisp->Fields('id_pk')."
								AND th_sellotas = 0
								AND sc_fecha BETWEEN '$fechad' AND $fechaHasta
								".(($byhotel)?" GROUP BY sc.id_pk, sc_fecha ":" GROUP BY sc.id_stock_global, sc_fecha ")."
								ORDER BY id_tipohab ASC, sc_fecha ASC";       
							$resGlob = $db1->Execute($sqlGlob) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
							if($resGlob->RecordCount()>0){
								$globDisp = array();
								while(!$resGlob->EOF){
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab1'] = $resGlob->Fields('ds_hab1');
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab2'] = $resGlob->Fields('ds_hab2');
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab3'] = $resGlob->Fields('ds_hab3');
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab4'] = $resGlob->Fields('ds_hab4');
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['minnoc'] = $resGlob->Fields('sc_minnoche');
									$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['idscglob'] = $resGlob->Fields('id_stock_global');
									$resGlob->MoveNext();
								}
							}else{
								$globDisp[$resDisp->Fields('id_pk')] = -1;
							}
						}
						if(isset($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')])){
							$stockEnjoy = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1'];
							if($resDisp->Fields('ds_hab1')<$sglRequest){
								$singleNeed = $sglRequest-$resDisp->Fields('ds_hab1');
							}
							if($resDisp->Fields('ds_hab2')<$twinRequest){
								$twinNeed=$twinRequest-$resDisp->Fields('ds_hab2');
							}
							if($resDisp->Fields('ds_hab3')<$matRequest){
								$matNeed=$matRequest-$resDisp->Fields('ds_hab3');
							}
							if($resDisp->Fields('ds_hab4')<$tplRequest){
								$tplNeed=$tplRequest-$resDisp->Fields('0ds_hab4');
							}
							if($resDisp->Fields('modo_global')==1){
								$tienedisp = (($singleNeed + $twinNeed + $matNeed + $tplNeed)<=$stockEnjoy);
							}else{
								$tienedisp=(
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1']>=$singleNeed) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab3']>=$twinNeed) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab2']>=$matNeed) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab4']>=$tplNeed)
								);
							}
							if($tienedisp){
								$usaGlobal = true;
								if($minimoNoches < $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['minnoc']){
									$minimoNoches = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['minnoc'];
								}
							}
						}else{
							$tienedisp=false;
						}
					}
					//////////////////////////////////////////////////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					if(($sglRequest>0 && $resDisp->Fields('hd_sgl')==0) || (($matRequest + $twinRequest)>0 && $resDisp->Fields('hd_dbl')==0) || ($tplRequest>0 && $resDisp->Fields('hd_tpl')==0)){
						$tienedisp=false;
					}
					
					if($tienedisp){
						$arfec = array();
						$curHot = $resDisp->Fields('id_hotel');
						$hot_direccion = $resDisp->Fields('hot_direccion');
						$curCli = $resDisp->Fields('id_cliente');
						$curTh = $resDisp->Fields('id_tipohabitacion');
						$fecha = $resDisp->Fields('ds_fecha');
						$curPk = $resDisp->Fields('id_pk');
						$hotName=$resDisp->Fields('hot_nombre');
						$ha_days = $resDisp->Fields('ha_dias');
						$ha_fec = $resDisp->Fields('ha_fec');
						
						if(!$extranjero && $resDisp->Fields('id_mon')==2){
							$this->decimales = 0;
						}

						$sgl = round($resDisp->Fields('hd_sgl'),$this->decimales);
						$dbl = round($resDisp->Fields('hd_dbl'),$this->decimales);
						$tpl = round($resDisp->Fields('hd_tpl'),$this->decimales);
						$flagCur = true;
						if(!$extranjero && $resDisp->Fields('id_mon')==1){
							$sgl = $sgl * $resDisp->Fields('tcambio');
							$dbl = $dbl * $resDisp->Fields('tcambio');
							$tpl = $tpl * $resDisp->Fields('tcambio');
						}
						
						$dayValue = round(($sgl * $sglRequest) + ($dbl * 2 * $twinRequest) + ($dbl * 2 * $matRequest)  + ($tpl * 3 * $tplRequest) ,$this->decimales);
						
						//setiamos un arreglo con los datos para el dia
						$arfec['sgl']=$sgl;
						$arfec['dbl']=$dbl;
						$arfec['tpl']=$tpl;
						$arfec['tipo'] = $resDisp->Fields('id_tipotarifa');
						$arfec['idcliente'] = $curCli;
						$arfec['hotdet'] = $resDisp->Fields('id_hotdet');
						$arfec['dayValue'] = $dayValue;
						$arfec['hotdet_cli'] = $resDisp->Fields('id_hotdet_cliente');
						$arfec['ivaIncluded'] = ($resDisp->Fields('hd_iva')==0)?'N':'S';

						//solo si usa global agregamos la informacion ocupada de global al arreglo del dia.
						if($usaGlobal){
							//dependiendo de si es enjoy guardamos solo single o todas las habitaciones por separado
							if($resDisp->Fields('modo_global')==1){
								$arfec['sglDis'] = ($singleNeed + $twinNeed + $matNeed + $tplNeed);
								$arfec['modeGlob'] = 1;
							}else{
								$arfec['sglDis'] = $singleNeed;
								$arfec['twinDis'] = $twinNeed;
								$arfec['matDis'] = $matNeed;
								$arfec['tplDis'] = $tplNeed;
								$arfec['modeGlob'] = 0;
							}
							$arfec['idsl'] = $resDisp->Fields('id_stock_cli');
							$arfec['idscglob'] = $globDisp[$resDisp->Fields('id_pk')][$curTh][$fecha]['idscglob'];
						}
						
						//si la tarifa del dia es promo tiramos el arreglo del dia al segundo slot de promos y guardamos informacion general en el primer slot de promos
						if($resDisp->Fields('tt_espromo')==1){
							$promos[$resDisp->Fields('id_hotdet')][1][$fecha] = $arfec;
							$promos[$resDisp->Fields('id_hotdet')][0]['thab'] = $curTh;
							$promos[$resDisp->Fields('id_hotdet')][0]['thname'] = $resDisp->Fields('th_nombre');
							$promos[$resDisp->Fields('id_hotdet')][0]['aplan'] = $resDisp->Fields('id_regimen');
							$promos[$resDisp->Fields('id_hotdet')][0]['refundable'] = $resDisp->Fields('hd_refundable');
							//si la fecha recorrida es la misma que la fecha desde guardamos el valor del primer dia como penalty en el primer slot de promos
							if($fecha===$fechad){
								$promos[$resDisp->Fields('id_hotdet')][0]['penalty'] = $dayValue;
								if($resDisp->Fields('hd_mon')==1){
									$promos[$resDisp->Fields('id_hotdet')][0]['penalty'] = round($promos[$resDisp->Fields('id_hotdet')][0]['penalty'] * $ivaNational, $this->decimales);
								}
							}
							//si ya esta seteado el valor total de la estadía en promos, lo sumamos, si no, lo seteamos.
							if(isset($promos[$resDisp->Fields('id_hotdet')][0]['totValue'])){
								$promos[$resDisp->Fields('id_hotdet')][0]['totValue'] += $dayValue;
							}else{
								$promos[$resDisp->Fields('id_hotdet')][0]['totValue'] = $dayValue;
							}
						//si la tarifa no es promo validamos la tarifa contra el dia en el arreglo auxiliar
						}else{
							//si ya hay una tarifa para la habitacion y el hotel validamos:
							if(isset($arAux[$curHot][$curTh][$fecha])){
								//si la tarifa seteada para el dia es mas cara que la tarifa comparada, restamos el valor del dia seteado del total de la reserva y sumamos el precio comparado
								if($arAux[$curHot][$curTh][$fecha]['dayValue'] > $dayValue){
									$arAux[$curHot]['info'][$curTh]['totValue'] = $arAux[$curHot]['info'][$curTh]['totValue'] - $arAux[$curHot][$curTh][$fecha]['dayValue'] + $dayValue;
									$valiDay = true;
								}
							//si no hay nada seteado para la tarifa comparada, sumamos o setiamos el valor total de la reserva.
							}else{
								if(isset($arAux[$curHot]['info'][$curTh]['totValue'])){
									$arAux[$curHot]['info'][$curTh]['totValue']+=$dayValue;
								}else{
									$arAux[$curHot]['info'][$curTh]['totValue']=$dayValue;	
								}
								$valiDay = true;
							}
							//si la tarifa comparada es mas barata, guardamos el arreglo del dia en el arreglo auxiliar
							if($valiDay){
								$arAux[$curHot][$curTh][$fecha] = $arfec;
								$arAux[$curHot][$curTh][$fecha]['refundable'] = $resDisp->Fields('hd_refundable');
								$arAux[$curHot]['info'][$curTh]['thname'] = $resDisp->Fields('th_nombre');
								$arAux[$curHot]['info'][$curTh]['aplan'] = $resDisp->Fields('id_regimen');
								//si la fecha recorrida es la misma que la fecha desde guardamos el valor del primer dia como penalty en el slot de info del arreglo auxiliar
								if($fecha===$fechad){
									$arAux[$curHot]['info'][$curTh]['penalty'] = $dayValue;
									if($resDisp->Fields('hd_mon')==1){
										$arAux[$curHot]['info'][$curTh]['penalty'] = round($arAux[$curHot]['info'][$curTh]['penalty'] * $ivaNational, $this->decimales);
									}
								}
							}
						}
					}

					//////////////////////////////////////////////////////////////////
					/////avanzamos el cursor y preguntamos por la linea siguiente/////
					//////////////////////////////////////////////////////////////////
					$resDisp->MoveNext();
					//si el hotel actual y el cliente estan seteados empezamos a validar shits					
					if($curHot!=-1 && $curCli!=-1){
						//si es el final del recordset flagueamos para que agregue al arreglo principal
						if($resDisp->EOF){
							$insertOnMain = true;
							$unsetHot = true;
						//si no es el final validamos otro tipo de shits
						}else{
							//si el hotel marcado cambia marcamos para ingresar al arreglo principal las tarifas del hotel y para unsetear el hotel
							if($curHot!=$resDisp->Fields('id_hotel')){
								$insertOnMain = true;
								$unsetHot = true;
							}
							//si el cliente proveedor de tarifas del hotel cambia, marcamos para agregar al arreglo principal las tarifas del hotel
							if($curCli != $resDisp->Fields('id_cliente')){
								$insertOnMain = true;
							}
						}
						//si esta marcado para insertar en el arreglo principal, validamos un par de weas más
						if($insertOnMain){
							//recorremos las promos primero
							foreach($promos as $id_hotdet => $nodes){
								$insertPromo = false;
								//solo si el conteo de nodos en el arreglo de promos calza con la cantidad de dias solicitada validamos la inserción al arreglo principal
								if(count($nodes[1])==$daysDiff){
									//si esta seteada alguna tarifa para la misma habitacion, validamos el precio de la estadia contra el arreglo principal, si no la marcamos para insertarla.
									if(isset($arreglo[$curHot]['info'][$nodes[0]['thab']]['totValue'])){
										//si es mas barata que el arreglo principal, la marcamos para insertarla
										if($arreglo[$curHot]['info'][$nodes[0]['thab']]['totValue'] > $nodes[0]['totValue']){
											$insertPromo = true;
										}
									}else{
										$insertPromo = true;
									}
									//si hay tarifa normal seteada en el arreglo auxiliar, validamos el precio, si no, marcamos para insertarla.
									if(isset($arAux[$curHot]['info'][$nodes[0]['thab']])){
										//si el precio es mas barato que el del arreglo normal, marcamos para insertarla
										if($arAux[$curHot]['info'][$nodes[0]['thab']]['totValue'] > $nodes[0]['totValue']){
											$insertPromo = true;
										}
									}else{
										$insertPromo = true;
									}
									//si viene la marca para insertarla, la tiramos al arreglo principal.
									if($insertPromo){
										$arreglo[$curHot]['habs'][$nodes[0]['thab']] = $nodes[1];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['totValue'] = $nodes[0]['totValue'];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['thname'] = $nodes[0]['thname'];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['aplan'] = $nodes[0]['aplan'];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['refundable'] = $nodes[0]['refundable'];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['penalty'] = $nodes[0]['penalty'];
										
										//unseteamos la habitacion del arreglo normal para no revalidarla despues.
										unset($arAux[$curHot][$nodes[0]['thab']]);
									}
								}
							}
							//si esta seteado el arreglo de normales, lo validamos.
							if(isset($arAux[$curHot])){
								//dejamos el nodo info del arreglo auxiliar en el arreglo de info y unseteamos el nodo original.
								$infoArAux = $arAux[$curHot]['info'];
								unset($arAux[$curHot]['info']);
								//recorremos las normales.
								foreach($arAux[$curHot] as $id_tipohab => $fechas){
									//solo si el conteno de nodos calza con el numero de noches lo validamos.
									if(count($fechas)==$daysDiff){
										//si esta seteado el tipo de habitacion, comparamos el precio, si no, lo agregamos directo al arreglo principal
										if(isset($arreglo[$curHot]['habs'][$id_tipohab])){
											//si el precio es mas barato que el del arreglo principal, reemplazamos el arreglo principal
											if($arreglo[$curHot]['info']['habs'][$id_tipohab]['totValue']>$infoArAux[$id_tipohab]['totValue']){
												$arreglo[$curHot]['habs'][$id_tipohab] = $arAux[$curHot][$id_tipohab];
												$arreglo[$curHot]['info']['habs'][$id_tipohab] = $infoArAux[$id_tipohab];
											}
										}else{
											$arreglo[$curHot]['habs'][$id_tipohab] = $arAux[$curHot][$id_tipohab];
											$arreglo[$curHot]['info']['habs'][$id_tipohab] = $infoArAux[$id_tipohab];
										}
										//marcamos la info por default en que es refundable.
										$arreglo[$curHot]['info']['habs'][$id_tipohab]['refundable'] = 0;
										//recorremos los dias de la estadia para validar refundable.
										foreach($fechas as $fecha => $nodos){
											//si se encuentra algun dia con una tarifa not refundable, marca la estadia como not refundable.
											if($nodos['refundable']==1){
												$arreglo[$curHot]['info']['habs'][$id_tipohab]['refundable'] = 1;
											}
										}
									}
								}
							}
							unset($infoArAux);
							$arAux = array();
							$promos = array();
							$insertOnMain = false;
							$curCli = -1;
							if(isset($arreglo[$curHot]['habs']) && !isset($arreglo[$curHot]['info']['name'])){
								$arreglo[$curHot]['info']['name'] = $hotName;
								$arreglo[$curHot]['info']['idhot'] = $curHot;
								$arreglo[$curHot]['info']['cancellationPolicy'] = $ha_days;
								$arreglo[$curHot]['info']['deadline'] = $ha_fec;
								$arreglo[$curHot]['info']['id_pk'] = $curPk;
								$arreglo[$curHot]['info']['direccion'] = ($hot_direccion==null)?'':$hot_direccion;
							}
						}
						if($unsetHot){
							unset($armkhot);
							unset($globDisp);
							$curHot = -1;
							$unsetHot = false;
						}
					}
				}
				return $arreglo;
			}else{
				$this->clDebug();
				return false;
			}

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	
	
}
?>