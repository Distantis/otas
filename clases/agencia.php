<?php

require_once(dirname(__FILE__).'/distantis.php');
class Agencia extends distantis{
	//constructor... aqui se inicializan todas las shits que no se puedan declarar como constantes arriba... (arrays y otros inventos weones).
	function __construct(){     
    	
	}
	//public function insertAgen($db1,$code,$name,$phone,$rut,$hotRestriction,$servRestriction,$markImp,$agMarkAgen,$country,$group,$espAnu,$chargeNoShow,$isPush=1){
	public function insertAgen($db1,$vars){
		try{
			$this->opDebug(__FUNCTION__);
			$agMarkAgen =$this->valMk($vars['txt_markup']);
			$SQLinsAgen = sprintf("INSERT INTO ".$this->dbMain.".agencia 
				(ag_code, 
				ag_nombre, 
				ag_fono, 
				ag_rut, 
				ag_diasrestriccion_hot, 
				ag_diasrestriccion_serv, 
				ag_mark_imp, 
				ag_markup, 
				id_pais, 
				id_grupo, 
				ag_espanula, 
				ag_cobrarnoshow, 
				ag_espush,
				ag_razon,
				ag_giro,
				ag_direccion,
				ag_email,
				ag_applymkdir) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				GetSQLValueString($vars['txt_codcts'], "text"),
				GetSQLValueString($vars['txt_nombre'], "text"),
				GetSQLValueString($vars['txt_fono'], "text"),
				GetSQLValueString($vars['txt_rut'], "text"),
				GetSQLValueString($vars['txt_reshot'], "int"),
				GetSQLValueString($vars['txt_resserv'], "int"),
				GetSQLValueString($vars['chk_mkimp'], "int"),
				GetSQLValueString($agMarkAgen, "double"),
				GetSQLValueString($vars['id_pais'], "int"),
				GetSQLValueString($vars['id_grupo'], "int"),
				GetSQLValueString($vars['chk_anuesp'], "int"),
				GetSQLValueString($vars['chk_chargeNoShow'], "int"),
				GetSQLValueString($vars['chk_push'], "int"),
				GetSQLValueString($vars['txt_razon'], "text"),
				GetSQLValueString($vars['txt_giro'], "text"),
				GetSQLValueString($vars['txt_direccion'], "text"),
				GetSQLValueString($vars['txt_email'], "text"),
				GetSQLValueString($vars['chk_applymkdir'], "int")
			);
			$db1->Execute($SQLinsAgen) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $db1->Insert_ID();
		}catch (Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function updateAgen($db1,$id_agencia,$vars){
		try{
			$this->opDebug(__FUNCTION__);
			$agMarkAgen =$this->valMk($vars['txt_markup']);

			$SQLupAgencia = sprintf("UPDATE ".$this->dbMain.".agencia SET 
				ag_code = %s, 
				ag_nombre = %s, 
				ag_fono = %s, 
				ag_rut = %s, 
				ag_diasrestriccion_hot = %s, 
				ag_diasrestriccion_serv = %s, 
				ag_mark_imp = %s, 
				ag_markup = %s, 
				id_pais = %s, 
				id_grupo = %s, 
				ag_espanula = %s, 
				ag_cobrarnoshow = %s, 
				ag_espush = %s,
				ag_razon = %s,
				ag_giro = %s,
				ag_direccion = %s,
				ag_email = %s,
				ag_applymkdir = %s
				WHERE id_agencia = %s",
				GetSQLValueString($vars['txt_codcts'], "text"),
				GetSQLValueString($vars['txt_nombre'], "text"),
				GetSQLValueString($vars['txt_fono'], "text"),
				GetSQLValueString($vars['txt_rut'], "text"),
				GetSQLValueString($vars['txt_reshot'], "int"),
				GetSQLValueString($vars['txt_resserv'], "int"),
				GetSQLValueString($vars['chk_mkimp'], "int"),
				GetSQLValueString($agMarkAgen, "double"),
				GetSQLValueString($vars['id_pais'], "int"),
				GetSQLValueString($vars['id_grupo'], "int"),
				GetSQLValueString($vars['chk_anuesp'], "int"),
				GetSQLValueString($vars['chk_chargeNoShow'], "int"),
				GetSQLValueString($vars['chk_push'], "int"),
				GetSQLValueString($vars['txt_razon'], "text"),
				GetSQLValueString($vars['txt_giro'], "text"),
				GetSQLValueString($vars['txt_direccion'], "text"),
				GetSQLValueString($vars['txt_email'], "text"),
				GetSQLValueString($vars['chk_applymkdir'], "int"),
				GetSQLValueString($id_agencia, "int")
			);
			$db1->Execute($SQLupAgencia) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch (Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getMeThatAgency($db1,$id_agencia,$joincredit=false,$idmon=null){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLagency = "SELECT 
				a.id_agencia,
				a.ag_code,
				a.ag_nombre,
				a.ag_fono,
				a.ag_rut,
				a.ag_estado,
				a.ag_diasrestriccion_hot,
				a.ag_diasrestriccion_serv,
				a.ag_mark_imp,
				a.ag_markup,
				a.id_pais,
				a.id_grupo,
				a.ag_espanula,
				a.ag_cobrarnoshow,
				a.ag_espush,
				a.ag_razon,
				a.ag_giro,
				a.ag_direccion,
				a.ag_email,
				inf.id_infopush,
				inf.user,
				inf.pass,
				inf.url_receptor,
				inf.url_inyector,
				inf.infpu_estado,
				ag_applymkdir,
				ag_logo";
			if($joincredit){
				$SQLagency.=", m.id_mon,
				m.mon_nombre,
				crag.id_credage,
				creag_inicial,
				creag_restante,
				creag_shoutat,
				creag_stopsell";
			}
			$SQLagency.=" FROM ".$this->dbMain.".agencia a 
				LEFT JOIN ".$this->dbMain.".info_push inf 
				ON a.id_agencia = inf.id_agencia";
			if($joincredit){
				$SQLagency.=" JOIN ".$this->dbMain.".mon m ";
				if(isset($idmon)){
					$SQLagency.=" ON m.id_mon = $idmon";
				}
				$SQLagency.=" LEFT JOIN ".$this->dbMain.".credito_agencia crag ON m.id_mon = crag.id_mon AND a.id_agencia = crag.id_agencia";
			}
			$SQLagency.=" WHERE a.id_agencia = ".GetSQLValueString($id_agencia, "int");
			$RESagency = $db1->Execute($SQLagency) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESagency;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getMeThatAgencyByUser($db1,$usuario){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLagency = "SELECT * FROM ".$this->dbMain.".agencia a 
				LEFT JOIN ".$this->dbMain.".info_push inf
				ON a.id_agencia = inf.id_agencia
				LEFT JOIN ".$this->dbMain.".usuarios u ON u.id_agencia = a.id_agencia
				WHERE u.id_usuario = ".GetSQLValueString($usuario, "int");
			$RESagency = $db1->Execute($SQLagency) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESagency;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function getSubAgenciesByAgency($db1,$id_agencia, $onlyActives = true){
		try{
			$this->opDebug(__FUNCTION__);
			$sql = "SELECT * 
				FROM ".$this->dbMain.".agxsubag ax 
				INNER JOIN ".$this->dbMain.".subagencia sa 
				ON ax.id_subagencia = sa.id_subagencia
				WHERE id_agencia = $id_agencia";
			if($onlyActives){
				$sql.=" AND sa_estado = 0 AND axsa_estado = 0 ORDER BY sa_nombre";
			}
			$RESsubagency = $db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESsubagency;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	

	public function managePushInfo($db1, $user, $pass, $receptor, $inyector, $agencia,$estado=0){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLchkExist = "SELECT * FROM ".$this->dbMain.".info_push WHERE id_agencia = ".GetSQLValueString($agencia, "int");
			$RESchkExist = $db1->Execute($SQLchkExist) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($RESchkExist->RecordCount()==0){
				$SQLinsPushInfo =  sprintf("INSERT INTO ".$this->dbMain.".info_push (user, pass, url_receptor, url_inyector, infpu_estado, id_agencia) VALUES (%s,%s,%s,%s,%s,%s)",
					GetSQLValueString($user, "text"),
					GetSQLValueString($pass, "text"),
					GetSQLValueString($receptor, "text"),
					GetSQLValueString($inyector, "text"),
					GetSQLValueString($estado, "int"),
					GetSQLValueString($agencia, "int")
				);
				$db1->Execute($SQLinsPushInfo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$this->clDebug();
				return $db1->Insert_ID();
			}else{
				$SQLupPushInfo = sprintf("UPDATE ".$this->dbMain.".info_push SET user= %s, pass=%s, url_receptor=%s, url_inyector=%s, infpu_estado=%s WHERE id_agencia = $agencia",
					GetSQLValueString($user, "text"),
					GetSQLValueString($pass, "text"),
					GetSQLValueString($receptor, "text"),
					GetSQLValueString($inyector, "text"),
					GetSQLValueString($estado, "int")
				);
				$db1->Execute($SQLupPushInfo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$this->clDebug();
				return $RESchkExist->Fields('id_infopush');
			}
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//inserta el log de la agencia
	public function insAgencyLog($db1,$id_agencia,$id_user, $desc=''){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLinsert = "INSERT INTO ".$this->dbMain.".log_agencia (id_agencia, ag_code, ag_nombre, ag_fono, ag_rut, ag_estado, ag_diasrestriccion_hot, ag_diasrestriccion_serv, ag_mark_imp, ag_markup, id_pais, id_grupo, ag_espanula, ag_cobrarnoshow, ag_espush, desc_log, log_fec, id_user)
				select id_agencia, ag_code, ag_nombre, ag_fono, ag_rut, ag_estado, ag_diasrestriccion_hot, ag_diasrestriccion_serv, ag_mark_imp, ag_markup, id_pais, id_grupo, ag_espanula, ag_cobrarnoshow, ag_espush, '".$desc."', NOW(), $id_user 
				FROM ".$this->dbMain.".agencia WHERE id_agencia = ".$id_agencia;
			$db1->Execute($SQLinsert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return true;

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function manageAgencyCredit($db1,$id_agencia,$id_mon,$vars){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlgetdata = "SELECT * FROM ".$this->dbMain.".credito_agencia WHERE id_agencia = $id_agencia AND id_mon = $id_mon";
			$resgetdata = $db1->Execute($sqlgetdata) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($resgetdata->RecordCount()==0){
				$sqlman  = "INSERT INTO ".$this->dbMain.".credito_agencia (id_agencia, id_mon, creag_inicial, creag_restante, creag_shoutat, creag_stopsell) VALUES ($id_agencia, $id_mon, ".$vars['inicial'].",".$vars['restante'].",".$vars['shoutat'].",".$vars['stopsell'].")";
			}else{
				if(isset($vars['inicial'])){
					$extra=" creag_inicial = ".$vars['inicial'].", ";
				}else{
					$extra="";
				}
				$sqlman = "UPDATE ".$this->dbMain.".credito_agencia SET $extra creag_restante = ".$vars['restante'].", creag_shoutat = ".$vars['shoutat'].", creag_stopsell = ".$vars['stopsell']." WHERE id_agencia = $id_agencia AND id_mon = $id_mon";
			}
			$db1->Execute($sqlman) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return true;
		}catch(Exception $e){
		var_dump($e);
		$this->clDebug();
		}
	}

	public function updateAgencyCredit($db1,$agencyData, $taken,$decimales){
		try{
			$this->opDebug(__FUNCTION__);
			$newvalue = round($agencyData->Fields('creag_restante') - $taken,$decimales);
			if($newvalue <= $agencyData->Fields('creag_shoutat')){
				$comment = $agencyData->Fields('ag_nombre')." (".$agencyData->Fields('id_agencia').")";
				$sqlalert = "INSERT INTO ".$this->dbMain.".admin_alerts (id_tipoalert,ada_comemnt) VALUES (1,'$comment')";
				$db1->Execute($sqlalert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$sqlupdate =" UPDATE ".$this->dbMain.".credito_agencia SET creag_restante = $newvalue WHERE id_agencia = ".$agencyData->Fields('id_agencia')." AND id_mon = ".$agencyData->Fields('id_area');
			$db1->Execute($sqlupdate) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	public function updateAgencyImage($db1, $id_agencia, $ruta){
		try{
			$this->opDebug(__FUNCTION__);
			$sql = "UPDATE ".$this->dbMain.".agencia SET ag_logo = '".$ruta."' WHERE id_agencia = $id_agencia";
			$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
		}catch(Exception $e){
			echo $e->getMesagge();
			var_dump($e);
			$this->clDebug();
		}
	}

	//devuelve las agencias en sistema, puede filtrar el estado de las agencias.
	public function getMeAgencies($db1, $onlyActives = true, $nombre='', $pais=null, $id_agencia=null){
		try{
			
			$this->opDebug(__FUNCTION__);
			$arAux=array();
			$cond="";
			if($onlyActives){
				array_push($arAux, "ag_estado = 0");
			}
			if(isset($id_agencia)){
				array_push($araux, "id_agencia = $id_agencia");
			}
			if($nombre!=''){
				array_push($arAux, "ag_nombre like ".GetSQLValueString("%".$nombre."%", "text"));
			}
			if($pais!=null && $pais!=0){
				array_push($arAux, "a.id_pais = $pais");
			}
			if(count($arAux)>0){
				$cond = "WHERE ".implode(" AND ", $arAux);
			}
    		$SQLgetAgencies = "SELECT a.id_agencia, ag_code, ag_nombre, ag_fono, ag_rut, ag_estado, ag_diasrestriccion_hot, ag_diasrestriccion_serv, ag_mark_imp, ag_markup, a.id_pais, a.id_grupo, ag_espanula, ag_cobrarnoshow, ag_espush,cantusu  FROM ".$this->dbMain.".agencia a
				LEFT JOIN ".$this->dbMain.".pais p ON a.id_pais = p.id_pais		
    			LEFT JOIN ".$this->dbMain.".grupo g ON a.id_grupo = g.id_grupo
    			LEFT JOIN 
    			(SELECT COUNT(*) AS cantusu, u.id_agencia FROM ".$this->dbMain.".usuarios u
					WHERE usu_estado = 0 GROUP BY u.id_agencia) t1
				ON a.id_agencia = t1.id_agencia  $cond ORDER BY ag_nombre ASC";
			$RESgetAgencies = $db1->Execute($SQLgetAgencies) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetAgencies;
			
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function switchAgencyState($db1, $id_agencia){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLupState = "UPDATE ".$this->dbMain.".agencia SET ag_estado = IF(ag_estado = 0, 1,0) WHERE id_agencia = ".$id_agencia;
			$db1->Execute($SQLupState) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$aux = $this->getMeThatAgency($db1,$id_agencia);
			$this->clDebug();
			return $aux->Fields('ag_estado');
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	//trae los hoteles en sistema e indica si la agencia esta autorizada a venderlo.
	public function getAllHotsXagency($db1,$id_agencia, $onlyActives = true, $id_ciudad=null, $joinCliente=true, $agenull=true){
		try{
			$this->opDebug(__FUNCTION__);
			$cond="";

			$arconds = array();

			if($onlyActives){
				array_push($arconds," hot_estado = 0 ");
			}
			if($id_ciudad != null){
				array_push($arconds," id_ciudad =".$id_ciudad);
			}
			if(count($arconds)>0){
				$cond = " WHERE ".implode(" AND ", $arconds);
			}
			
			$SQLgetHots = "SELECT * FROM (SELECT h.id_hotel, hot_nombre, ".(($joinCliente)?'cl.id_cliente':'-1')." as id_cliente, IFNULL(hotxag_estado,1) AS hot_worked, hot_estado 
				FROM ".$this->dbMain.".hotel h ";
			if($joinCliente){
				$SQLgetHots.=" JOIN ".$this->dbhot.".clientes cl ON cl.estado = 0 AND cl.trabaja_gandhi = 0";	
			}
			$SQLgetHots.= " JOIN ".$this->dbMain.".agencia ag ON ag.id_agencia = $id_agencia
				LEFT JOIN ".$this->dbMain.".hotelxagencia ha 
				ON h.id_hotel = ha.id_hotel AND ag.id_agencia = ha.id_agencia ".(($joinCliente)?'AND cl.id_cliente = ha.id_cliente':'')." $cond ORDER BY h.id_hotel ASC, hotxag_estado ASC) t1";
			if(!$joinCliente){
				$SQLgetHots.=" GROUP BY id_hotel ORDER BY hot_nombre ASC";
			}				
			$RESgetHots = $db1->Execute($SQLgetHots) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetHots;
			
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}

	//desactiva todos los hoteles que una agencia esta autorizada a comprar y si recibe datos, activa hoteles.
	public function resetHotsXagency($db1, $hotData=null, $id_agencia, $id_cliente){
		try{
			$this->opDebug(__FUNCTION__);

			$SQLupAll = "UPDATE ".$this->dbMain.".hotelxagencia SET hotxag_estado = 1 WHERE id_agencia = $id_agencia AND id_cliente = $id_cliente";
			$db1->Execute($SQLupAll) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($hotData!=null){
				if(is_array($hotData)){
					foreach($hotData as $node => $id_hotel){
						$this->insHotsXagency($db1, $id_hotel, $id_agencia, $id_cliente);
					}
				}else{
					$this->insHotsXagency($db1, $hotData, $id_agencia, $id_cliente);
				}
			}
			$this->clDebug();
		} catch (Exception $e) {
			var_dump($e);
			$this->clDebug();
		}		
	}
	
	public function insHotsXagency($db1, $id_hotel, $id_agencia, $id_cliente){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLchkExist = "SELECT * FROM ".$this->dbMain.".hotelxagencia WHERE id_agencia = $id_agencia AND id_hotel = $id_hotel AND id_cliente = $id_cliente";
			$RESchkExist = $db1->Execute($SQLchkExist) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($RESchkExist->RecordCount()>0){
				$SQLup = "UPDATE ".$this->dbMain.".hotelxagencia SET hotxag_estado = 0 WHERE id_agencia = $id_agencia AND id_hotel = $id_hotel AND id_cliente = $id_cliente";
			}else{
				$SQLup = "INSERT INTO ".$this->dbMain.".hotelxagencia (id_agencia, id_hotel, id_cliente, hotxag_estado) VALUES ($id_agencia, $id_hotel, $id_cliente, 0)";
			}
			$db1->Execute($SQLup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			

			$this->clDebug();
		} catch (Exception $e) {
			var_dump($e);
			$this->clDebug();
		}
	}

	//trae los webservices en sistema e indica si la agencia lo utiliza.
	public function getAllWsXagency($db1,$id_agencia, $onlyActives = true){
		try{
			$cond="";
			if($onlyActives){
				$cond = " WHERE ws_estado = 0";
			}
			$this->opDebug(__FUNCTION__);
			$SQLgetWS = "SELECT id_webservice , ws_nombre, fecha_implementacion, ws_ruta, ifnull((SELECT ifnull(agws_estado,1) FROM ".$this->dbMain.".agencia_webservice WHERE id_webservice = w.id_webservice AND id_agencia = $id_agencia),1)AS ws_worked FROM ".$this->dbMain.".webservice w $cond";
			$RESgetWS = $db1->Execute($SQLgetWS) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetWS;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	//desactiva todos los webservices de una agencia y si recibe datos, le asigna nuevos.
	public function resetWsXagency($db1, $wsData=null, $id_agencia){
		try{
			$this->opDebug(__FUNCTION__);

			$SQLupAll = "UPDATE ".$this->dbMain.".agencia_webservice SET agws_estado = 1 WHERE id_agencia = $id_agencia";
			$db1->Execute($SQLupAll) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($wsData!=null){
				if(is_array($wsData)){
					foreach($wsData as $node => $id_webservice){
						$this->insWsXagency($db1, $id_webservice, $id_agencia);
					}
				}else{
					$this->insWsXagency($db1, $wsData, $id_agencia);
				}
			}
			$this->clDebug();
		} catch (Exception $e) {
			var_dump($e);
			$this->clDebug();
		}		
	}

	public function insWsXagency($db1, $id_webservice, $id_agencia){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLchkExist = "SELECT * FROM ".$this->dbMain.".agencia_webservice WHERE id_agencia = $id_agencia AND id_webservice = $id_webservice";
			$RESchkExist = $db1->Execute($SQLchkExist) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($RESchkExist->RecordCount()>0){
				$SQLup = "UPDATE ".$this->dbMain.".agencia_webservice SET agws_estado = 0 WHERE id_agencia = $id_agencia AND id_webservice = $id_webservice";
			}else{
				$SQLup = "INSERT INTO ".$this->dbMain.".agencia_webservice (id_agencia, id_webservice, agws_estado) VALUES ($id_agencia, $id_webservice, 0)";
			}
			$db1->Execute($SQLup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			

			$this->clDebug();
		} catch (Exception $e) {
			var_dump($e);
			$this->clDebug();
		}
	}
}
?>