<?php
require_once("distantis.php");

class Cotizacion extends distantis {
	//determina si una agencia puede tener markup imperativo = 1 (no aplica markup);
	public $allowMkImpAsOne = true;
	//determina si se pueden vender promos
	public $allowPromos = true;
	//dias por defecto de anulacion sin costo
	public $cancelationDays = 8;
	//define si se puede convertir la moneda
	public $allowCurrencyConvertion = false;
	//define si se le aplica markup comprador al cliente cuando ocupan compra opaca.
	public $applyMkToBuyerClient = true;
	//establece si ignorar el credito restante de la agencia en el flujo de compra.
	public $ignoreAgencyCredit = true;
	
	public function matrizDisponibilidadG($db1,
                                         $ciudad = null,
                                         $fechad,
                                         $fechah,
                                         $id_agencia,
                                         $habs,
                                         $id_hotel = null,
                                         $id_tipohabitacion = null,
                                         $extranjero = true,
                                         $childs=null,
                                         $informDisp = false,
                                         $airport = null,
                                         $id_subagencia = -1){
		try{
			$this->opDebug(__FUNCTION__);
			require_once("/var/www/otas/clases/agencia.php");
			$agencyClass = new Agencia();
			$agencyClass->debug = $this->debug;

			if(!isset($ciudad) && !isset($id_hotel) && !isset($airport)){
				$this->clDebug();
				return false;
			}

			$arMk = array();
			$lookForMk = true;
			$moneda = '1';
			
			if($extranjero){
				$area = 1;
			}else{
				if($this->allowCurrencyConvertion){
					$moneda.=',2';
				}else{
					$moneda='2';
				}
				$area = 2;
			}
			
			$agencia = $agencyClass->getMeThatAgency($db1,$id_agencia,true,$area);
			if(!$this->ignoreAgencyCredit){
				if($agencia->Fields('creag_stopsell')==0 || $agencia->Fields('id_credage')==''){
					$this->clDebug();
					return false;
				}
			}


         if($lookForMk){
				$markup = $this->getMkGral($db1,true,$extranjero)->Fields('mg_markup');
			}

			//////////////////////////////////////////////////////////
			//////////////////markup dinamico Part I//////////////////
			//////////////////////////////////////////////////////////
			$markupaux = $this->valMk($agencia->Fields('ag_markup'));
			
			//si el markup de agencia es distinto de 1 o si es 1 y esta permitido, pregunta si es imperativo.
			if(($markupaux==1 && $this->allowMkImpAsOne) || $markupaux!=1){
				$markup = $markupaux;
			}
			if($agencia->Fields('ag_mark_imp')==0){
				$lookForMk=false;
			}

			
			//////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////
			$sglRequest=0;
			$twinRequest=0;
			$matRequest=0;
			$tplRequest=0;



			$fechad = $this->checkDateFormat($fechad);
			if(!is_numeric($fechah)){
				$fechah = $this->checkDateFormat($fechah);
				$fechaHasta = "ADDDATE('".$fechah."',-1)";
				$nightDiff="DATEDIFF('$fechah','$fechad')";
			}else{
				$fechaHasta = "ADDDATE('".$fechad."', ".($fechah-1).")";
				$nightDiff = $fechah;
			}


			$sqlcheckdatediff = "SELECT DATEDIFF('$fechah','$fechad') AS days";

			$rescheckdiff = $db1->Execute($sqlcheckdatediff) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($rescheckdiff->Fields('days')>$this->maxconfdays){
				$this->clDebug();
				return false;
			}

         $childFlag=false;
			$habsCond="";
			$condChild="";
			$closecond="";
			$selectChild="";
			$joinChild="";
			$arChildJoin=array();

			if(isset($habs['sgl'])){
				if($habs['sgl']>0){
					$closecond.=" AND ds_cerrado_sgl = 0";
					$habsCond.=" AND hd_sgl > 0 AND ds_hab1 >= ".$habs['sgl'];
					$sglRequest=$habs['sgl'];
					//nodo[hab][i] = edad
					if(isset($childs['sgl'])){
						$childFlag=true;
						$condChild.= " AND cxhd_maxsgl >= ".round(count($childs['sgl'])/$sglRequest);
						$selectChild.=",SUM((";
						$auxAr=array();
						foreach ($childs['sgl'] as $i => $edad) {
							array_push($auxAr, "(IF($edad BETWEEN cxhd_edadfrom AND cxhd_edadto,1,0))");
							array_push($arChildJoin, "$edad BETWEEN cxhd.cxhd_edadfrom AND cxhd.cxhd_edadto");
						}
						$selectChild.=implode(" + ", $auxAr).")";
						$chkcantchld=$selectChild.") as cchldsgl";
						$selectChild.=" * (IF(cxhd_valsgl<1, ROUND(hd_sgl * cxhd_valsgl,".$this->decimales."), cxhd_valsgl))";
						$selectChild.=") as valchldsgl".$chkcantchld;
					}
				}
			}

			if(isset($habs['twin'])){
				if($habs['twin']>0){
					$closecond.=" AND ds_cerrado_twn = 0";
					$habsCond.=" AND hd_dbl > 0 AND ds_hab2 >= ".$habs['twin'];
					$twinRequest=$habs['twin'];
					if(isset($childs['twin'])){
						$childFlag=true;
						$condChild.= " AND cxhd_maxtwin >= ".round(count($childs['twin'])/$twinRequest);
						$selectChild.=",SUM((";
						$auxAr=array();
						foreach ($childs['twin'] as $i => $edad) {
							array_push($auxAr, "(IF($edad BETWEEN cxhd_edadfrom AND cxhd_edadto,1,0))");
							array_push($arChildJoin, "$edad BETWEEN cxhd.cxhd_edadfrom AND cxhd.cxhd_edadto");
						}
						$selectChild.=implode(" + ", $auxAr).")";
						$chkcantchld=$selectChild.") as cchldtwin";
						$selectChild.="* (IF(cxhd_valtwin<1, ROUND(hd_dbl * cxhd_valtwin * 2,".$this->decimales."), cxhd_valtwin))";
						$selectChild.=") as valchldtwin".$chkcantchld;
					}
				}
			}


			if(isset($habs['mat'])){
				if($habs['mat']>0){
					$closecond.=" AND ds_cerrado_mat = 0";
					$habsCond.=" AND hd_dbl > 0 AND ds_hab3 >= ".$habs['mat'];
					$matRequest=$habs['mat'];
					if(isset($childs['mat'])){
						$childFlag=true;
						$condChild.= " AND cxhd_maxmat >= ".round(count($childs['mat'])/$matRequest);
						$selectChild.=",SUM((";
						$auxAr=array();
						foreach ($childs['mat'] as $i => $edad) {
							array_push($auxAr, "(IF($edad BETWEEN cxhd_edadfrom AND cxhd_edadto,1,0))");
							array_push($arChildJoin, "$edad BETWEEN cxhd.cxhd_edadfrom AND cxhd.cxhd_edadto");
						}
						$selectChild.=implode(" + ", $auxAr).")";
						$chkcantchld=$selectChild.") as cchldmat";
						$selectChild.=" * (IF(cxhd_valmat<1, ROUND(hd_dbl * cxhd_valmat * 2,".$this->decimales."), cxhd_valmat))";
						$selectChild.=") as valchldmat".$chkcantchld;
					}
				}
			}
			if(isset($habs['tpl'])){
				if($habs['tpl']>0){
					$closecond.=" AND ds_cerrado_tpl = 0";
					$habsCond.=" AND hd_tpl > 0 AND ds_hab4 >= ".$habs['tpl'];
					$tplRequest=$habs['tpl'];
					if(isset($childs['tpl'])){
						$childFlag=true;
						$condChild = " AND cxhd_maxtpl >= ".round(count($childs['tpl'])/$tplRequest);
						$selectChild.=",SUM((";
						$auxAr=array();
						foreach ($childs['tpl'] as $i => $edad) {
							array_push($auxAr, "(IF($edad BETWEEN cxhd_edadfrom AND cxhd_edadto,1,0))");
							array_push($arChildJoin, "$edad BETWEEN cxhd.cxhd_edadfrom AND cxhd.cxhd_edadto");
						}
						$selectChild.=implode(" + ", $auxAr).")";
						$chkcantchld=$selectChild.") as cchldtpl";
						$selectChild.=" * (IF(cxhd_valtpl<1, ROUND(hd_tpl * cxhd_valtpl * 3,".$this->decimales."), cxhd_valtpl))";
						$selectChild.=") as valchldtpl".$chkcantchld;
					}
				}
			}
			if($childFlag){
				$condChild.=" AND cxhd_estado = 0";
				$joinChild.=" LEFT JOIN ".$this->dbMain.".childxhotdet cxhd ON hd.id_hotdet = cxhd.id_hotdet AND (";
				$joinChild.=implode(" OR ", $arChildJoin);
				$joinChild.=")";
				
			}
			$habsCond = "((hm.ver=0) OR (hm.ver=1 ".$habsCond."))";

			/*
			 *
			 * INICIO QUERY DE DISPONIBILIDAD
			 * */



			$sqlDisp = "SELECT d.id_dispo,
				hd.id_hotdet, h.id_hotel,hot_direccion, h.id_pk, ds_fecha,
				DATEDIFF(ds_fecha, '$fechad') AS dayPos, hd_sgl, hd_dbl, hd_tpl, hd_sgl_cli, hd_dbl_cli, hd_tpl_cli, hd_iva, hd_usamarkup, 
				ds_hab1, ds_hab2, ds_hab3, ds_hab4, ds_minnoche, 
				h.id_ciudad,idpk_tipohabitacion as id_tipohabitacion, idpk_tipotarifa as id_tipotarifa, hd.id_cliente, id_hotdet_cliente, 
				hot_nombre, hot_email, tt_nombre, th_nombre,
				tt_espromo, tt_minnoc, tt_maxnoc, id_mon, hotxag_estado, ca.modo AS modo_global, hm.ver AS es_global, c.id_agencia AS cli_ag_asoc, 
				trim(ciu.airportcode) as airportcode, trim(pai.pai_nombre) as pai_nombre, trim(ciu.ciu_nombre) as ciu_nombre,
				hot_markdin,
				hd_markup,
				IF(mk.`markdin_estado`=1,1,IFNULL(mk.markdin_valor,1)) AS mark_hot,
				agx.axsa_markup,
				$nightDiff as daysDiff,
				cat_des,
				tipocambio_gandhi as tcambio,
				d.id_stock_cli,
				IFNULL(ha_dias,".$this->cancelationDays.") as ha_dias,
				date_add('$fechad', interval -(IFNULL(ha_dias,".$this->cancelationDays.")) day) as ha_fec,
				hd_refundable,
				hd.id_regimen,
				re.nom_regimen,
				hot_test,
				IF(c.id_agencia = $id_agencia, 0,1) as directa,
				ciu.aplica_iva,
				hd_prepago,
				hf.estacionamiento,
				hf.room_service,
				hf.business_center,
				hf.gym,
				hf.spa,
				hf.restaurant,
				hf.bar,
				hf.caja_fuerte,
				hf.habs_discapacitados,
				hf.permite_mascotas,
				hf.serv_lavanderia,
				hf.check_in,
				hf.check_out,
				hf.ficha_img1,
				hf.pag_web,
				hf.descripcion,
				hf.tv_habs,
				hf.frigobar,
				hf.aire_acondicionado
				$selectChild
				FROM ".$this->dbMain.".hotdet hd 
				INNER JOIN ".$this->dbMain.".dispo d ON hd.id_hotdet = d.id_hotdet
				INNER JOIN ".$this->dbMain.".hotel h ON hd.id_hotel = h.id_hotel
				LEFT JOIN ".$this->dbMain.".hotficha hf ON hf.id_hotel = h.id_hotel
				INNER JOIN ".$this->dbMain.".hotelxagencia ha ON h.id_hotel = ha.id_hotel AND hd.id_cliente = ha.id_cliente
				INNER JOIN ".$this->dbhot.".hotelesmerge hm ON h.id_pk = hm.id_pk 
				INNER JOIN ".$this->dbhot.".clientes c ON hd.id_cliente = c.id_cliente
				INNER JOIN ".$this->dbhot.".tipotarifa tt ON hd.idpk_tipotarifa = tt.id_tipotarifa
				INNER JOIN ".$this->dbhot.".tipohabitacion th ON hd.idpk_tipohabitacion = th.id_tipohabitacion
				INNER JOIN ".$this->dbhot.".cadena ca ON hm.id_cadena = ca.id_cadena
				INNER JOIN ".$this->dbMain.".ciudad ciu ON h.id_ciudad = ciu.id_ciudad
				INNER JOIN ".$this->dbMain.".pais pai ON ciu.id_pais = pai.id_pais
				LEFT JOIN  ".$this->dbMain.".mk_hot_area mk ON h.id_hotel = mk.id_hotel AND mk.id_area = $area
				LEFT JOIN  ".$this->dbMain.".hotanula af ON h.id_hotel = af.id_hotel
				LEFT JOIN  ".$this->dbMain.".regimen re ON hd.id_regimen = re.id_regimen
				LEFT JOIN ".$this->dbMain.".agxsubag agx ON agx.id_agencia = $id_agencia AND agx.id_subagencia = $id_subagencia
				LEFT JOIN ".$this->dbMain.".cat ON h.id_cat = cat.id_cat
				$joinChild
				WHERE tt_sellotas = 0
				AND (h.hot_habxvender >= ($sglRequest+$twinRequest+$matRequest+$tplRequest) OR h.hot_habxvender = 0)
				AND tt_estado = 0
				AND th_estado = 0
				AND th_sellotas = 0
				AND hd_estado = 0
				AND ha.id_agencia = $id_agencia
				AND hotxag_estado = 0
				AND hot_estado = 0
				AND hot_sellota = 0
				AND ds_estado = 0
				AND ds_cerrado=0
				AND c.estado = 0
				AND hd.id_mon IN ($moneda)
				AND c.trabaja_gandhi = 0
				AND ciu_estado = 0
				AND ds_minnoche <= $nightDiff
				AND (tt_maxnoc >= $nightDiff OR tt_maxnoc = 0)
				AND ds_fecha BETWEEN '$fechad' AND $fechaHasta
				AND ds_tarifa = 0
				AND (hd_prepago = 0 OR (c.id_agencia = $id_agencia AND hd_prepago = 1))
				$closecond
				$condChild
				AND $habsCond";
			if($id_hotel != null){
				$sqlDisp.=" AND hd.id_hotel IN (".((is_array($id_hotel))?implode(",", $id_hotel):$id_hotel).")";
			}
			if(isset($ciudad)){
				$sqlDisp.= " AND h.id_ciudad = $ciudad ";
			}
			if(isset($airport)){
				$sqlDisp.=" AND TRIM(ciu.airportcode) = '$airport'";
			}
			if($id_tipohabitacion!=null){
				$sqlDisp.=" AND hd.idpk_tipohabitacion = ".$id_tipohabitacion;
			}
			$sqlDisp.=" AND ((tt.tt_espromo = 0 AND tt_minnoc <= $nightDiff)";
			if($this->allowPromos){
				$sqlDisp.=" OR(tt.tt_espromo = 1 AND tt_minnoc = $nightDiff)";
			}
			
			$sqlDisp.=")";
			if($childFlag){
				$sqlDisp.=" GROUP BY ds_fecha, hd.id_hotdet";
			}
			$sqlDisp.=" ORDER BY hot_nombre ASC, hd.id_cliente ASC, th.id_tipohabitacion ASC, id_hotdet ASC, ds_fecha ASC";

			// echo $sqlDisp;die();

         /*
          *
          * FIN QUERY DE DISPONIBILIDAD
          * */

			$resDisp = $db1->Execute($sqlDisp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($resDisp->RecordCount()>0){

				$daysDiff = $resDisp->Fields('daysDiff');
				$insertOnMain = false;
				$unsetHot = false;
				$curHot = -1;
				$curCli = -1;
				$hotName="";
				$penalty = 0;
				$arreglo = array();
				$arAux = array();
				$promos = array();
				$keeplooking = false;
				//if($agencia->Fields('ag_noshowiva')==0){
				$mksubag = $this->valMk($agencia->Fields('axsa_markup'));
				$arrayMarkup[1] = ($mksubag!=1)?$mksubag:$this->valMk($agencia->Fields('ag_markup_extra'));
					
				$impuesto = $this->getImpuestoPais($db1, $this->nationalCountryID);
				// }else{
				// 	$impuesto = 1;
				// }


				while(!$resDisp->EOF){
					if($lookForMk){
						$keeplooking = true;
					}
					$valiDay = false;
					if($informDisp){
						$dispoTotal = array();
					}
					//////////////////////////////////////////////////////////////////////////
					/////////////////////////////Dispo Global 2.0/////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					$usaGlobal = false;
					//esta basura tiene que reiniciarse cuando cambia de habitacion o cliente u hotel.
					if($resDisp->Fields('ds_minoche') >= $resDisp->Fields('tt_minnoc')){
						$minimoNoches = $resDisp->Fields('ds_minoche');
					}else{
						$minimoNoches = $resDisp->Fields('tt_minnoc');
					}

               #die("TestMundotour");


					//se busca por global siempre si el hotel es global.


					if($resDisp->Fields('es_global')==0){


						if(!isset($globDisp[$resDisp->Fields('id_pk')])){
							//esta basura va a buscar el arreglo con la dispo global
							$globDisp = $this->getGlobalDispo($db1, $fechad, $fechaHasta, $resDisp->Fields('id_pk'));
						}


						if(isset($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')])){
							$stockEnjoy = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1'];
							if($resDisp->Fields('modo_global')==1){
								$tienedisp = (($sglRequest + $twinRequest + $matRequest + $tplRequest)<=$stockEnjoy);
							}else{
								$tienedisp=(
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1']>=$sglRequest) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab3']>=$twinRequest) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab2']>=$matRequest) && 
									($globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab4']>=$tplRequest)
									);
							}
							if($tienedisp){
								$usaGlobal = true;	
								$minimoNoches = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['minnoc'];
							}
						}else{
							$tienedisp=false;
						}
					}else{
						$tienedisp=(($resDisp->Fields('ds_hab1')>=$sglRequest) && ($resDisp->Fields('ds_hab2')>=$twinRequest) && ($resDisp->Fields('ds_hab3')>=$matRequest) && ($resDisp->Fields('ds_hab4')>=$tplRequest));
					}

					//////////////////////////////////////////////////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					if(($sglRequest>0 && $resDisp->Fields('hd_sgl')==0) || (($matRequest + $twinRequest)>0 && $resDisp->Fields('hd_dbl')==0) || ($tplRequest>0 && $resDisp->Fields('hd_tpl')==0)){
						$tienedisp=false;
					}

					if($minimoNoches>$daysDiff){
						$tienedisp = false;
					}

					if(isset($childs['sgl'])){
						if(count($childs['sgl'])!=$resDisp->Fields('cchldsgl')){
							$tienedisp = false;
						}
					}
					if(isset($childs['twin'])){
						if(count($childs['twin'])!=$resDisp->Fields('cchldtwin')){
							$tienedisp = false;
						}
					}

					if(isset($childs['mat'])){
						if(count($childs['mat'])!=$resDisp->Fields('cchldmat')){
							$tienedisp = false;
						}
					}
					if(isset($childs['tpl'])){
						if(count($childs['tpl'])!=$resDisp->Fields('cchldtpl')){
							$tienedisp = false;
						}
					}

					if($tienedisp){
						$arfec = array();
						$curHot = $resDisp->Fields('id_hotel');
						$hot_direccion = $resDisp->Fields('hot_direccion');
						$curCli = $resDisp->Fields('id_cliente');
						$curTh = $resDisp->Fields('id_tipohabitacion');
						$fecha = $resDisp->Fields('ds_fecha');
						$curPk = $resDisp->Fields('id_pk');
						$hotName=$resDisp->Fields('hot_nombre');
						$ha_days = $resDisp->Fields('ha_dias');
						$ha_fec = $resDisp->Fields('ha_fec');
						$hot_test = $resDisp->Fields('hot_test');
						$airportcode = $resDisp->Fields('airportcode');
						$id_ciudad = $resDisp->Fields('id_ciudad');
						$ciu_name = $resDisp->Fields('ciu_nombre');
						$pai_name = $resDisp->Fields('pai_nombre');
						$checkin = $resDisp->Fields('check_in');
						$checkout = $resDisp->Fields('check_out');
						$hotimg = $resDisp->Fields('ficha_img1');
						$hotweb = $resDisp->Fields('pag_web');
						$hotdesc = $resDisp->Fields('descripcion');
						$categoria = $resDisp->Fields('cat_des');
						$roomAmenities = array();
						$hotelAmenities = array();


						if($resDisp->Fields('tv_habs')==1){
							array_push($roomAmenities, "televisión");
						}
						if($resDisp->Fields('frigobar')==1){
							array_push($roomAmenities, "frigobar");
						}
						if($resDisp->Fields('aire_acondicionado')==1){
							array_push($roomAmenities, "aire acondicionado");
						}
						if($resDisp->Fields('estacionamiento')==1){
							array_push($hotelAmenities, "estacionamiento");
						}
						if($resDisp->Fields('room_service')==1){
							array_push($hotelAmenities, "servicio a habitacion");
						}
						if($resDisp->Fields('business_center')==1){
							array_push($hotelAmenities, "centro de negocios");
						}
						if($resDisp->Fields('gym')==1){
							array_push($hotelAmenities, "gimnasio");
						}
						if($resDisp->Fields('spa')==1){
							array_push($hotelAmenities, "spa");
						}
						if($resDisp->Fields('restaurant')==1){
							array_push($hotelAmenities, "restaurant");
						}
						if($resDisp->Fields('bar')==1){
							array_push($hotelAmenities, "bar");
						}
						if($resDisp->Fields('caja_fuerte')==1){
							array_push($hotelAmenities, "caja fuerte");
						}
						if($resDisp->Fields('habs_discapacitados')==1){
							array_push($hotelAmenities, "habitaciones para discapacitados");
						}
						if($resDisp->Fields('permite_mascotas')==1){
							array_push($hotelAmenities, "permite mascotas");
						}
						if($resDisp->Fields('serv_lavanderia')==1){
							array_push($hotelAmenities, "servicio de lavanderia");
						}
						
						if($resDisp->Fields('directa')==0){
							$keeplooking = false;
							$arrayMarkup[0]['sgl'] = 1;
							$arrayMarkup[0]['twin'] = 1;
							$arrayMarkup[0]['mat'] = 1;
							$arrayMarkup[0]['tpl'] = 1;
						}else{
							$arrayMarkup[0]['sgl'] = $markup;
							$arrayMarkup[0]['twin'] = $markup;
							$arrayMarkup[0]['mat'] = $markup;
							$arrayMarkup[0]['tpl'] = $markup;
						}
						///////////////////////////////////////////////////////////////////////
						///////////////////////markup dinamico 2da parte///////////////////////
						////////////////chingo a su madre hasta definirlo bien/////////////////
						/*
						if($keeplooking){
							if($resDisp->Fields('hd_usamarkup')==0){
								if(!isset($armkhd[$resDisp->Fields('id_hotdet')])){
									$sqlMarkup = "SELECT ds_fecha, 
										mk.id_hotdet, 
										mk_markup_sgl AS mk_sgl, 
										mk_markup_dbt AS mk_twin, 
										mk_markup_dbm AS mk_mat, 
										mk_markup_tpl AS mk_tpl 
										FROM ".$this->dbMain.".dispo ds INNER JOIN ".$this->dbMain.".mk_ope_hotdet mk ON 
										ds_fecha BETWEEN mk_fecdesde AND mk_fechasta 
										AND ds.id_hotdet = mk.id_hotdet 
										WHERE mk.id_hotdet = ".$resDisp->Fields('id_hotdet')."
										AND id_agencia = $id_agencia
										AND ds_fecha BETWEEN '$fechad' AND $fechaHasta 
										AND mk.mk_estado = 0 AND ds_estado = 0";
									$resMarkup = $db1->Execute($sqlMarkup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
									if($resMarkup->RecordCount()>0){
										$armkhd = array();
										while(!$resMarkup->EOF){
											$armkhd[$resDisp->Fields('id_hotdet')][$resMarkup->Fields('ds_fecha')]['mk_sgl'] = $resMarkup->Fields('mk_sgl');
											$armkhd[$resDisp->Fields('id_hotdet')][$resMarkup->Fields('ds_fecha')]['mk_twin'] = $resMarkup->Fields('mk_twin');
											$armkhd[$resDisp->Fields('id_hotdet')][$resMarkup->Fields('ds_fecha')]['mk_mat'] = $resMarkup->Fields('mk_mat');
											$armkhd[$resDisp->Fields('id_hotdet')][$resMarkup->Fields('ds_fecha')]['mk_tpl'] = $resMarkup->Fields('mk_tpl');
											$resMarkup->MoveNext();
										}
									}else{
										$armkhd[$resDisp->Fields('id_hotdet')] = -1;
									}
								}
								//si encuentra markup especifico de operador por tarifa deja de buscar.
								if(isset($armkhd[$resDisp->Fields('id_hotdet')][$fecha])){
									$arrayMarkup['sgl'] = $armkhd[$resDisp->Fields('id_hotdet')][$fecha]['mk_sgl'];
									$arrayMarkup['twin'] = $armkhd[$resDisp->Fields('id_hotdet')][$fecha]['mk_twin'];
									$arrayMarkup['mat'] = $armkhd[$resDisp->Fields('id_hotdet')][$fecha]['mk_mat'];
									$arrayMarkup['tpl'] = $armkhd[$resDisp->Fields('id_hotdet')][$fecha]['mk_tpl'];
									$keeplooking = false;
								}
							}
						}
						if($keeplooking){
							if($resDisp->Fields('hot_markdin')==0){
								if(!isset($armkhot[$curHot])){
									$sqlMarkupHot="SELECT ds_fecha, 
										mk.id_hotel, 
										mk_markup_sgl AS mk_sgl, 
										mk_markup_dbt AS mk_twin, 
										mk_markup_dbm AS mk_mat, 
										mk_markup_tpl AS mk_tpl FROM dispo ds INNER JOIN hotdet hd ON
										ds.id_hotdet = hd.id_hotdet
										INNER JOIN mk_ope_hotdet mk ON hd.id_hotel = mk.id_hotel
										AND ds_fecha BETWEEN mk_fecdesde AND mk_fechasta
										WHERE id_agencia = $id_agencia
										AND ds_fecha BETWEEN '$fechad' AND $fechaHasta 
										AND mk.mk_estado = 0
										GROUP BY ds_fecha";
									$resMarkupHot = $db1->Execute($sqlMarkupHot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
									if($resMarkupHot->RecordCount()>0){
										$armkhot = array();
										while(!$resMarkupHot->EOF){
											$armkhot[$curHot][$resMarkupHot->Fields('ds_fecha')]['mk_sgl'] = $resMarkupHot->Fields('mk_sgl');
											$armkhot[$curHot][$resMarkupHot->Fields('ds_fecha')]['mk_twin'] = $resMarkupHot->Fields('mk_twin');
											$armkhot[$curHot][$resMarkupHot->Fields('ds_fecha')]['mk_mat'] = $resMarkupHot->Fields('mk_mat');
											$armkhot[$curHot][$resMarkupHot->Fields('ds_fecha')]['mk_tpl'] = $resMarkupHot->Fields('mk_tpl');
											$resMarkupHot->MoveNext();
										}
									}else{
										$armkhot[$resDisp->Fields('id_hotel')] = -1;
									}
								}
								//si encuentra markup especifico de operador por hotel deja de buscar.
								if(isset($armkhot[$resDisp->Fields('id_hotdet')][$fecha])){
									$arrayMarkup['sgl'] = $armkhot[$curHot][$fecha]['mk_sgl'];
									$arrayMarkup['twin']= $armkhot[$curHot][$fecha]['mk_twin'];
									$arrayMarkup['mat'] = $armkhot[$curHot][$fecha]['mk_mat'];
									$arrayMarkup['tpl'] = $armkhot[$curHot][$fecha]['mk_tpl'];
									$keeplooking = false;
								}
							}
						}
						if($keeplooking){
							if($resDisp->Fields('hd_markup')!=1){
								//die("entro");
								$arrayMarkup['sgl'] = $resDisp->Fields('hd_markup');
								$arrayMarkup['twin'] = $resDisp->Fields('hd_markup');
								$arrayMarkup['mat'] = $resDisp->Fields('hd_markup');
								$arrayMarkup['tpl'] = $resDisp->Fields('hd_markup');
								$keeplooking = false;
							}
						}
						if($keeplooking){
							if($resDisp->Fields('mark_hot')!=1){
								$arrayMarkup['sgl'] = $resDisp->Fields('mark_hot');
								$arrayMarkup['twin'] = $resDisp->Fields('mark_hot');
								$arrayMarkup['mat'] = $resDisp->Fields('mark_hot');
								$arrayMarkup['tpl'] = $resDisp->Fields('mark_hot');
								$keeplooking = false;	
							}
						}
						*/
						if(!$extranjero && $resDisp->Fields('id_mon')==2){
							$this->decimales = 0;
						}

						$flagCur = true;
						//si la agencia esta asociada a algun cliente/proveedor le mostramos los costos sin markup de la wea.
						if($resDisp->Fields('directa')==0){
							$sgl = round($resDisp->Fields('hd_sgl_cli'),$this->decimales);
							$dbl = round($resDisp->Fields('hd_dbl_cli'),$this->decimales);
							$tpl = round($resDisp->Fields('hd_tpl_cli'),$this->decimales);
						}else{
							$sgl = round($resDisp->Fields('hd_sgl'),$this->decimales);
							$dbl = round($resDisp->Fields('hd_dbl'),$this->decimales);
							$tpl = round($resDisp->Fields('hd_tpl'),$this->decimales);
						}

						if(!$extranjero && $resDisp->Fields('id_mon')==1){
							$sgl = $sgl * $resDisp->Fields('tcambio');
							$dbl = $dbl * $resDisp->Fields('tcambio');
							$tpl = $tpl * $resDisp->Fields('tcambio');
						}

						$dayvalsgl = round($sgl * $sglRequest / $arrayMarkup[0]['sgl'], $this->decimales);
						$dayvaltwin = round(($dbl * 2 * $twinRequest) / $arrayMarkup[0]['twin'], $this->decimales);
						$dayvalmat = round(($dbl * 2 * $matRequest) / $arrayMarkup[0]['mat'], $this->decimales);
						$dayvaltpl = round(($tpl * 3 * $tplRequest) / $arrayMarkup[0]['tpl'], $this->decimales);

						if(isset($childs['sgl'])){
							if(!$extranjero && $resDisp->Fields('id_mon')==1){
								$arfec['chdsgl'] = $resDisp->Fields('valchldsgl')*$resDisp->Fields('tcambio');
							}else{
								$arfec['chdsgl'] = $resDisp->Fields('valchldsgl');
							}
							$dayvalsgl+= round($arfec['chdsgl'] / $arrayMarkup[0]['sgl'], $this->decimales);
						}
						if(isset($childs['twin'])){
							if(!$extranjero && $resDisp->Fields('id_mon')==1){
								$arfec['chdtwin'] = $resDisp->Fields('valchldtwin')*$resDisp->Fields('tcambio');
							}else{
								$arfec['chdtwin'] = $resDisp->Fields('valchldtwin');
							}
							$dayvaltwin+= round($arfec['chdtwin'] / $arrayMarkup[0]['twin'], $this->decimales);
						}
						if(isset($childs['mat'])){
							if(!$extranjero && $resDisp->Fields('id_mon')==1){
								$arfec['chdmat'] = $resDisp->Fields('valchldmat')*$resDisp->Fields('tcambio');
							}else{
								$arfec['chdmat'] = $resDisp->Fields('valchldmat');
							}
							$dayvalmat+= round($arfec['chdmat'] / $arrayMarkup[0]['mat'], $this->decimales);
						}
						if(isset($childs['tpl'])){
							if(!$extranjero && $resDisp->Fields('id_mon')==1){
								$arfec['chdtpl'] = $resDisp->Fields('valchldtpl')*$resDisp->Fields('tcambio');
							}else{
								$arfec['chdtpl'] = $resDisp->Fields('valchldtpl');
							}
							$dayvaltpl+= round($arfec['chdtpl'] / $arrayMarkup[0]['tpl'], $this->decimales);
						}

						$aplicaiva = false;

						if($resDisp->Fields('aplica_iva')==0 && $resDisp->Fields('hd_iva')==0){
							if(!$extranjero){
								$aplicaiva = true;
							}
							$taxiva = true;
						}else{
							$taxiva = false;
						}


						if($arrayMarkup[1]!=1){
							$dayvalsgl = round($dayvalsgl / $arrayMarkup[1], $this->decimales);
							$dayvaltwin = round($dayvaltwin / $arrayMarkup[1], $this->decimales);
							$dayvalmat = round($dayvalmat / $arrayMarkup[1], $this->decimales);
							$dayvaltpl = round($dayvaltpl / $arrayMarkup[1], $this->decimales);
						}

						$dayValue = round($dayvalsgl + $dayvaltwin + $dayvalmat + $dayvaltpl, $this->decimales);

						//setiamos un arreglo con los datos para el dia
						$arfec['sgl']=$sgl;
						$arfec['dbl']=$dbl;
						$arfec['tpl']=$tpl;
						$arfec['dayValue'] = $dayValue;
						$arfec['dayvalsgl'] = $dayvalsgl;
						$arfec['dayvaltwin'] = $dayvaltwin;
						$arfec['dayvalmat'] = $dayvalmat;
						$arfec['dayvaltpl'] = $dayvaltpl;
						$arfec['tipo'] = $resDisp->Fields('id_tipotarifa');
						$arfec['idcliente'] = $curCli;
						$arfec['hotdet'] = $resDisp->Fields('id_hotdet');
						$arfec['markup'] = $arrayMarkup;
						$arfec['hotdet_cli'] = $resDisp->Fields('id_hotdet_cliente');

						//$arfec['ivaIncluded'] = ($resDisp->Fields('hd_iva')==0)?'N':'S';

						//solo si usa global agregamos la informacion ocupada de global al arreglo del dia.
						if($usaGlobal){
							//dependiendo de si es enjoy guardamos solo single o todas las habitaciones por separado
							if($resDisp->Fields('modo_global')==1){
								$arfec['sglDis'] = ($sglRequest + $twinRequest + $matRequest + $tplRequest);
								$arfec['modeGlob'] = 1;
								if($informDisp){
									$arfec['dispoTotal']['sgl'] = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1'];
								}
							}else{
								$arfec['sglDis'] = $sglRequest;
								$arfec['twinDis'] = $twinRequest;
								$arfec['matDis'] = $matRequest;
								$arfec['tplDis'] = $tplRequest;
								$arfec['modeGlob'] = 0;
								if($informDisp){
									$arfec['dispoTotal']['sgl'] = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab1'];
									$arfec['dispoTotal']['twin'] = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab2'];
									$arfec['dispoTotal']['mat'] = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab3'];
									$arfec['dispoTotal']['tpl'] = $globDisp[$resDisp->Fields('id_pk')][$resDisp->Fields('id_tipohabitacion')][$resDisp->Fields('ds_fecha')]['dhab4'];
								}
							}
							$arfec['idsl'] = $resDisp->Fields('id_stock_cli');
							$arfec['idscglob'] = $globDisp[$resDisp->Fields('id_pk')][$curTh][$fecha]['idscglob'];
						}else{
							$arfec['dispoTotal']['sgl'] = $resDisp->Fields('ds_hab1');
							$arfec['dispoTotal']['twin'] = $resDisp->Fields('ds_hab2');
							$arfec['dispoTotal']['mat'] = $resDisp->Fields('ds_hab3');
							$arfec['dispoTotal']['tpl'] = $resDisp->Fields('ds_hab4');
						}
						
						//si la tarifa del dia es promo tiramos el arreglo del dia al segundo slot de promos y guardamos informacion general en el primer slot de promos
						if($resDisp->Fields('tt_espromo')==1){
							$promos[$resDisp->Fields('id_hotdet')][1][$fecha] = $arfec;
							$promos[$resDisp->Fields('id_hotdet')][0]['thab'] = $curTh;
							$promos[$resDisp->Fields('id_hotdet')][0]['thname'] = $resDisp->Fields('th_nombre');
							$promos[$resDisp->Fields('id_hotdet')][0]['aplan'] = $resDisp->Fields('id_regimen');
							$promos[$resDisp->Fields('id_hotdet')][0]['aplaname'] = $resDisp->Fields('nom_regimen');
							$promos[$resDisp->Fields('id_hotdet')][0]['directa'] = $resDisp->Fields('directa');
							$promos[$resDisp->Fields('id_hotdet')][0]['refundable'] = $resDisp->Fields('hd_refundable');
							$promos[$resDisp->Fields('id_hotdet')][0]['prepago'] = $resDisp->Fields('hd_prepago');
							$promos[$resDisp->Fields('id_hotdet')][0]['roomamenities'] = implode(",",$roomAmenities);
							//si la fecha recorrida es la misma que la fecha desde guardamos el valor del primer dia como penalty en el primer slot de promos
							if($fecha===$fechad){
								$promos[$resDisp->Fields('id_hotdet')][0]['penalty'] = $dayValue;
								if($resDisp->Fields('hd_mon')==1){
									$promos[$resDisp->Fields('id_hotdet')][0]['penalty'] = round($promos[$resDisp->Fields('id_hotdet')][0]['penalty'] * (($taxiva)?$impuesto:1), $this->decimales);
								}
							}
							//si ya esta seteado el valor total de la estadía en promos, lo sumamos, si no, lo seteamos.
							if(isset($promos[$resDisp->Fields('id_hotdet')][0]['totValue'])){
								$promos[$resDisp->Fields('id_hotdet')][0]['totValue'] += $dayValue;
								$promos[$resDisp->Fields('id_hotdet')][0]['totsgl'] += $dayvalsgl;
								$promos[$resDisp->Fields('id_hotdet')][0]['tottwin'] += $dayvaltwin;
								$promos[$resDisp->Fields('id_hotdet')][0]['totmat'] += $dayvalmat;
								$promos[$resDisp->Fields('id_hotdet')][0]['tottpl'] += $dayvaltpl;
							}else{
								$promos[$resDisp->Fields('id_hotdet')][0]['totValue'] = $dayValue;
								$promos[$resDisp->Fields('id_hotdet')][0]['totsgl'] = $dayvalsgl;
								$promos[$resDisp->Fields('id_hotdet')][0]['tottwin'] = $dayvaltwin;
								$promos[$resDisp->Fields('id_hotdet')][0]['totmat'] = $dayvalmat;
								$promos[$resDisp->Fields('id_hotdet')][0]['tottpl'] = $dayvaltpl;
							}
						//si la tarifa no es promo validamos la tarifa contra el dia en el arreglo auxiliar
						}else{
							//si ya hay una tarifa para la habitacion y el hotel validamos:
							if(isset($arAux[$curHot][$curTh][$fecha])){
								//si la tarifa seteada para el dia es mas cara que la tarifa comparada, restamos el valor del dia seteado del total de la reserva y sumamos el precio comparado
								if($arAux[$curHot][$curTh][$fecha]['dayValue'] > $dayValue){
									$arAux[$curHot]['info'][$curTh]['totValue'] = $arAux[$curHot]['info'][$curTh]['totValue'] - $arAux[$curHot][$curTh][$fecha]['dayValue'] + $dayValue;
									$arAux[$curHot]['info'][$curTh]['totsgl'] = $arAux[$curHot]['info'][$curTh]['totsgl'] - $arAux[$curHot][$curTh][$fecha]['dayvalsgl'] + $dayvalsgl;
									$arAux[$curHot]['info'][$curTh]['tottwin'] = $arAux[$curHot]['info'][$curTh]['tottwin'] - $arAux[$curHot][$curTh][$fecha]['dayvaltwin'] + $dayvaltwin;
									$arAux[$curHot]['info'][$curTh]['totmat'] = $arAux[$curHot]['info'][$curTh]['totmat'] - $arAux[$curHot][$curTh][$fecha]['dayvalmat'] + $dayvalmat;
									$arAux[$curHot]['info'][$curTh]['tottpl'] = $arAux[$curHot]['info'][$curTh]['tottpl'] - $arAux[$curHot][$curTh][$fecha]['dayvaltpl'] + $dayvaltpl;
									$valiDay = true;
								}
							//si no hay nada seteado para la tarifa comparada, sumamos o setiamos el valor total de la reserva.
							}else{
								if(isset($arAux[$curHot]['info'][$curTh]['totValue'])){
									$arAux[$curHot]['info'][$curTh]['totValue']+=$dayValue;
									$arAux[$curHot]['info'][$curTh]['totsgl']+=$dayvalsgl;
									$arAux[$curHot]['info'][$curTh]['tottwin']+=$dayvaltwin;
									$arAux[$curHot]['info'][$curTh]['totmat']+=$dayvalmat;
									$arAux[$curHot]['info'][$curTh]['tottpl']+=$dayvaltpl;
								}else{
									$arAux[$curHot]['info'][$curTh]['totValue']=$dayValue;
									$arAux[$curHot]['info'][$curTh]['totsgl']=$dayvalsgl;
									$arAux[$curHot]['info'][$curTh]['tottwin']=$dayvaltwin;
									$arAux[$curHot]['info'][$curTh]['totmat']=$dayvalmat;
									$arAux[$curHot]['info'][$curTh]['tottpl']=$dayvaltpl;
								}
								$valiDay = true;
							}
							//si la tarifa comparada es mas barata, guardamos el arreglo del dia en el arreglo auxiliar
							if($valiDay){
								$arAux[$curHot][$curTh][$fecha] = $arfec;
								$arAux[$curHot]['info'][$curTh]['thname'] = $resDisp->Fields('th_nombre');
								$arAux[$curHot]['info'][$curTh]['aplan'] = $resDisp->Fields('id_regimen');
								$arAux[$curHot]['info'][$curTh]['aplaname'] = $resDisp->Fields('nom_regimen');
								$arAux[$curHot]['info'][$curTh]['directa'] = $resDisp->Fields('directa');
								$arAux[$curHot][$curTh][$fecha]['refundable'] = $resDisp->Fields('hd_refundable');
								$arAux[$curHot]['info'][$curTh]['prepago'] = $resDisp->Fields('hd_prepago');
								$arAux[$curHot]['info'][$curTh]['roomamenities'] = implode(",",$roomAmenities);

								//si la fecha recorrida es la misma que la fecha desde guardamos el valor del primer dia como penalty en el slot de info del arreglo auxiliar
								if($fecha===$fechad){
									$arAux[$curHot]['info'][$curTh]['penalty'] = $dayValue;
									if($resDisp->Fields('hd_mon')==1){
										$arAux[$curHot]['info'][$curTh]['penalty'] = round($arAux[$curHot]['info'][$curTh]['penalty'] * (($taxiva)?$impuesto:1), $this->decimales);
									}
								}
							}
						}
					}

					//////////////////////////////////////////////////////////////////
					/////avanzamos el cursor y preguntamos por la linea siguiente/////
					//////////////////////////////////////////////////////////////////
					$resDisp->MoveNext();

               #var_dump($curHot.'|'.$curCli);die();

					//si el hotel actual y el cliente estan seteados empezamos a validar shits					
					if($curHot!=-1 && $curCli!=-1){
						//si es el final del recordset flagueamos para que agregue al arreglo principal
						if($resDisp->EOF){
							$insertOnMain = true;
							$unsetHot = true;
						//si no es el final validamos otro tipo de shits
						}else{
							//si el hotel marcado cambia marcamos para ingresar al arreglo principal las tarifas del hotel y para unsetear el hotel
							if($curHot!=$resDisp->Fields('id_hotel')){
								$insertOnMain = true;
								$unsetHot = true;
							}
							//si el cliente proveedor de tarifas del hotel cambia, marcamos para agregar al arreglo principal las tarifas del hotel
							if($curCli != $resDisp->Fields('id_cliente')){
								$insertOnMain = true;
							}
						}
						//si esta marcado para insertar en el arreglo principal, validamos un par de weas más
                  #var_dump($insertOnMain);die();
						if($insertOnMain){
							//recorremos las promos primero

                     #var_dump($promos);die();

							foreach($promos as $id_hotdet => $nodes){
								$insertPromo = false;
								//solo si el conteo de nodos en el arreglo de promos calza con la cantidad de dias solicitada validamos la inserción al arreglo principal
								//siempre y cuando la agencia tenga credito suficiente o sea un hotel de prueba
								if(count($nodes[1])==$daysDiff && (($nodes[0]['totValue']<=$agencia->Fields('creag_restante') || $hot_test == 0 || $nodes[0]['directa']==0) || $this->ignoreAgencyCredit)){
									//si esta seteada alguna tarifa para la misma habitacion, validamos el precio de la estadia contra el arreglo principal, si no la marcamos para insertarla.
									if(isset($arreglo[$curHot]['info'][$nodes[0]['thab']]['totValue'])){
										//si es mas barata que el arreglo principal, la marcamos para insertarla
										if($arreglo[$curHot]['info'][$nodes[0]['thab']]['totValue'] > $nodes[0]['totValue']){
											$insertPromo = true;
										}
									}else{
										$insertPromo = true;
									}
									//si hay tarifa normal seteada en el arreglo auxiliar, validamos el precio, si no, marcamos para insertarla.
									if(isset($arAux[$curHot]['info'][$nodes[0]['thab']])){
										//si el precio es mas barato que el del arreglo normal, marcamos para insertarla
										if($arAux[$curHot]['info'][$nodes[0]['thab']]['totValue'] > $nodes[0]['totValue']){
											$insertPromo = true;
										}
									}else{
										$insertPromo = true;
									}
									//si viene la marca para insertarla, la tiramos al arreglo principal.

									if($insertPromo){
										$arreglo[$curHot]['habs'][$nodes[0]['thab']] = $nodes[1];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']] = $nodes[0];
										$arreglo[$curHot]['info']['habs'][$nodes[0]['thab']]['taxes'] = round($nodes[0]['totValue'] * (($aplicaiva)?(($impuesto)-1):0),$this->decimales);
										//unseteamos la habitacion del arreglo normal para no revalidarla despues.
										unset($arAux[$curHot][$nodes[0]['thab']]);
									}
								}
							}
							//si esta seteado el arreglo de normales, lo validamos.

                     #var_dump($arAux[$curHot]);die();

							if(isset($arAux[$curHot])){
								//dejamos el nodo info del arreglo auxiliar en el arreglo de info y unseteamos el nodo original.
								$infoArAux = $arAux[$curHot]['info'];
								unset($arAux[$curHot]['info']);
								//recorremos las normales.
								foreach($arAux[$curHot] as $id_tipohab => $fechas){


                           #var_dump([$id_tipohab,$fechas]);die();

									//solo si el conteno de nodos calza con el numero de noches lo validamos.
									if(count($fechas)==$daysDiff && (($infoArAux[$id_tipohab]['totValue']<=$agencia->Fields('creag_restante') || $hot_test==0 || $infoArAux[$id_tipohab]['directa']==0) || $this->ignoreAgencyCredit)){
										//si esta seteado el tipo de habitacion, comparamos el precio, si no, lo agregamos directo al arreglo principal
										$infoArAux[$id_tipohab]['taxes'] = round($infoArAux[$id_tipohab]['totValue'] * (($aplicaiva)?(($impuesto)-1):0),$this->decimales);
										if(isset($arreglo[$curHot]['habs'][$id_tipohab])){
											//si el precio es mas barato que el del arreglo principal, reemplazamos el arreglo principal
											if($arreglo[$curHot]['info']['habs'][$id_tipohab]['totValue']>$infoArAux[$id_tipohab]['totValue']){
												$arreglo[$curHot]['habs'][$id_tipohab] = $arAux[$curHot][$id_tipohab];
												$arreglo[$curHot]['info']['habs'][$id_tipohab] = $infoArAux[$id_tipohab];
											}
										}else{
											$arreglo[$curHot]['habs'][$id_tipohab] = $arAux[$curHot][$id_tipohab];
											$arreglo[$curHot]['info']['habs'][$id_tipohab] = $infoArAux[$id_tipohab];
										}
										//marcamos la info por default en que es refundable.
										$arreglo[$curHot]['info']['habs'][$id_tipohab]['refundable'] = 0;
										//recorremos los dias de la estadia para validar refundable.
										foreach($fechas as $fecha => $nodos){
											//si se encuentra algun dia con una tarifa not refundable, marca la estadia como not refundable.
											if($nodos['refundable']==1){
												$arreglo[$curHot]['info']['habs'][$id_tipohab]['refundable'] = 1;
											}
										}
									}
								}
							}


							unset($armkhd);
							unset($infoArAux);
							$arAux = array();
							$promos = array();
							$insertOnMain = false;
							$curCli = -1;

							#var_dump($arreglo[$curHot]['habs']);die();

							if(isset($arreglo[$curHot]['habs']) && !isset($arreglo[$curHot]['info']['name'])){

								$arreglo[$curHot]['info']['name'] = $hotName;
								$arreglo[$curHot]['info']['idhot'] = $curHot;
								$arreglo[$curHot]['info']['cancellationPolicy'] = $ha_days;
								$arreglo[$curHot]['info']['deadline'] = $ha_fec;
								$arreglo[$curHot]['info']['id_pk'] = $curPk;
								$arreglo[$curHot]['info']['direccion'] = ($hot_direccion==null)?'':$hot_direccion;
								$arreglo[$curHot]['info']['hot_test'] = $hot_test;
								$arreglo[$curHot]['info']['airportcode'] = $airportcode;
								$arreglo[$curHot]['info']['id_ciudad'] = $id_ciudad;
								$arreglo[$curHot]['info']['City'] = $ciu_name;
								$arreglo[$curHot]['info']['Country'] = $pai_name;
								$arreglo[$curHot]['info']['amenities'] = implode(",", $hotelAmenities);
								$arreglo[$curHot]['info']['checkin'] = $checkin;
								$arreglo[$curHot]['info']['checkout'] = $checkout;
								$arreglo[$curHot]['info']['hotimg'] = $hotimg;
								$arreglo[$curHot]['info']['hotweb'] = $hotweb;
								$arreglo[$curHot]['info']['hotdesc'] = $hotdesc;
								$arreglo[$curHot]['info']['cat'] = $categoria;
							}
						}
                  #var_dump($arreglo);die();
						if($unsetHot){
							unset($armkhot);
							unset($globDisp);
							$curHot = -1;
							$unsetHot = false;
						}
					}
				}
				return $arreglo;
			}else{
				$this->clDebug();
				return false;
			}

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function cotCreator($db1, $matriz, $habs, $paxes, $id_usuario, $id_ciudad, $extras = array(), $childs=null,$iscotclient=false){
		try{
			$this->opDebug(__FUNCTION__);
			$valid = true;
			$res = array();

			/*
			var_dump([
            //'valor1'=>$db1,
            //'valor2'=>$matriz,
            //'valor3'=>$habs,
            //'valor4'=>$paxes,
            'valor5'=>$id_usuario,
            'valor6'=>$id_ciudad,
            'valor7'=>$extras,
            'valor8'=>$childs,
            'valor9'=>$iscotclient
         ]);die();
         */

			#var_dump($this->dbMain);die();

			if(count($matriz)>0){
				$sgl = (isset($habs['sgl']))? $habs['sgl']  : 0;
				$twin = (isset($habs['twin']))? $habs['twin']  : 0;
				$mat = (isset($habs['mat']))? $habs['mat']  : 0;
				$tpl = (isset($habs['tpl']))? $habs['tpl']  : 0;
				$clientData= array();
				$client = array();
				
				$usaGlobal = false;
				require_once('/var/www/otas/clases/tarifa.php');
				$tarClass = new Tarifa();
				if($this->debug){
					$tarClass->debug=true;
				}

				#var_dump($tarClass);die();

				$cityData = $this->getCities($db1, true, 0, $id_ciudad);
				if($cityData->RecordCount()==0){
					$res['status']  = "ERROR";
					$res['errorcode'] = 104;
					$res['description'] = "City is not allowed to be sold";
					$this->clDebug();
					return $res;
				}


				foreach($matriz as $id_hotel => $data){
					$SQLidCot = "INSERT INTO ".$this->dbMain.".cot (cot_prihotel) VALUES ($id_hotel)";
					$db1->Execute($SQLidCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$idCotOtas = $db1->Insert_ID();

					$SQLidCotdes = "INSERT INTO ".$this->dbMain.".cotdes (id_cot) VALUES ($idCotOtas)";
					$db1->Execute($SQLidCotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$idCotdesOtas = $db1->Insert_ID();

					require_once('/var/www/otas/clases/agencia.php');
					$agencyClass = new Agencia();
					if($this->debug){
						$agencyClass->debug=true;
					}
					
					$clientTotalValue=0;

					if($this->strangerPax($paxes[0]['countryid'])){
						$area = 1;
						$currency = 1;
					}else{
						$area = 2;
						$currency = 2;
					}
					$agencyInfo = $agencyClass->getMeThatAgencyByUser($db1, $id_usuario,true,$area);

					#var_dump($agencyInfo);die();

					foreach($data['habs'] as $id_tipohabitacion => $dias){
						foreach($dias as $dia => $detalle){

						   var_dump($detalle['idcliente']);die();

							if(!isset($clientData[$detalle['hotdet_cli']])){

								$client = $this->getMeThatClient($db1, $detalle['idcliente']);
								$clientid = $detalle['idcliente'];

                        #var_dump($client);die();
								
								//AQUI WEON!!!! AQUI ES EL MAMBO!!!!
								if(!isset($calclines)){
									$calclines = $this->getCalcLines($db1,$detalle['idcliente'], $area);
								}

								if(trim($client['bd'])=='' || trim($client['id_user'])==''){
									$res['status']  = "ERROR";
									$res['errorcode'] = 113;
									$res['description'] = "Provider information incomplete, can't complete booking";
									$this->clDebug();
									return $res;
								}
								
								$getHdClient = "SELECT * FROM ".$client['bd'].".hotdet hd 
									INNER JOIN ".$client['bd'].".hotel h ON hd.id_hotel = h.id_hotel
									WHERE id_hotdet = ".$detalle['hotdet_cli'];
								$resHdClient = $db1->Execute($getHdClient) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								if($resHdClient->RecordCount()>0){
									$clientData[$detalle['hotdet_cli']]['hotel'] = $resHdClient->Fields('id_hotel');
									$clientData[$detalle['hotdet_cli']]['sgl'] = $resHdClient->Fields('hd_sgl');
									$clientData[$detalle['hotdet_cli']]['dbl'] = $resHdClient->Fields('hd_dbl');
									$clientData[$detalle['hotdet_cli']]['tpl'] = $resHdClient->Fields('hd_tpl');
									$clientData[$detalle['hotdet_cli']]['thab'] = $resHdClient->Fields('id_tipohabitacion');
									$clientData[$detalle['hotdet_cli']]['ttar'] = $resHdClient->Fields('id_tipotarifa');
									$clientData[$detalle['hotdet_cli']]['ciu'] = $resHdClient->Fields('id_ciudad');
								}else{
									$res['status']  = "ERROR";
									$res['errorcode'] = 103;
									$res['description'] = "Aditional info couldn't be retrieved to create this booking";
									$this->clDebug();
									return $res;
								}
								if(!isset($calclines[$detalle['idcliente']]['hotocu'][$area]) || !isset($calclines[$detalle['idcliente']]['cotdes'][$area])){
									$res['status']  = "ERROR";
									$res['errorcode'] = 102;
									$res['description'] = "Price lines couldn't be retrieved";
									$this->clDebug();
									return $res;
								}
								if(!isset($idCotCli)){
									$SQLidCotCli = "INSERT INTO ".$client['bd'].".cot (cot_prihotel) VALUES (".$clientData[$detalle['hotdet_cli']]['hotel'].")";
									$db1->Execute($SQLidCotCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
									$idCotCli = $db1->Insert_ID();
								}
								if(!isset($idCotdesCli)){
									$SQLidCotdesCli = "INSERT INTO ".$client['bd'].".cotdes (id_cot) VALUES ($idCotCli)";
									$db1->Execute($SQLidCotdesCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
									$idCotdesCli = $db1->Insert_ID();
								}
							}


                     #var_dump("end");die();
							/////////////////////////////otas//////////////////////////////
							$hab1 = round($detalle['sgl'] * $sgl / $detalle['markup'][0]['sgl'],$this->decimales);
							$hab2 = round($detalle['dbl'] * 2 * $twin / $detalle['markup'][0]['twin'],$this->decimales);
							$hab3 = round($detalle['dbl'] * 2 * $mat / $detalle['markup'][0]['mat'],$this->decimales);
							$hab4 = round($detalle['tpl'] * 3 * $tpl / $detalle['markup'][0]['tpl'],$this->decimales);

							$SQLinsHcOtas = "INSERT INTO ".$this->dbMain.".hotocu (id_hotel, id_cot, id_cotdes, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_estado, id_hotdet, hc_valor1, hc_valor2, hc_valor3, hc_valor4,  hc_mkhab1, hc_mkhab2, hc_mkhab3, hc_mkhab4, id_hotdet_cliente, id_cliente, id_cotcli) VALUES (
							$id_hotel , $idCotOtas, $idCotdesOtas,'$dia', $sgl, $twin, $mat, $tpl, 0, ".$detalle['hotdet'].", $hab1, $hab2, $hab3, $hab4,".$detalle['markup'][0]['sgl'].",".$detalle['markup'][0]['twin'].",".$detalle['markup'][0]['mat'].",".$detalle['markup'][0]['tpl'].",".$detalle['hotdet_cli']." ,".$detalle['idcliente'].", $idCotCli)";
							$db1->Execute($SQLinsHcOtas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							$lastHotocu = $db1->Insert_ID();
							$tarClass->dispoReducer($db1, $detalle['idcliente'], $sgl, $twin, $mat, $tpl,$dia, null, null, $detalle['hotdet']);
							
							if(isset($childs)){
								$sqlchd="INSERT INTO ".$this->dbMain.".childocu (id_hotocu, cc_cantsgl, cc_valsgl, cc_canttwin, cc_valtwin, cc_cantmat, cc_valmat, cc_canttpl, cc_valtpl,cc_estado, id_cot) VALUES ($lastHotocu,";
								if(isset($childs['sgl'])){
									$sqlchd.=$childs['sgl'].",".(round($detalle['chdsgl'] / $detalle['markup'][0]['sgl'],$this->decimales)).",";
								}else{
									$sqlchd.="0,0,";
								}
								if(isset($childs['twin'])){
									$sqlchd.=$childs['twin'].",".(round($detalle['chdtwin'] / $detalle['markup'][0]['twin'],$this->decimales)).",";
								}else{
									$sqlchd.="0,0,";
								}
								if(isset($childs['mat'])){
									$sqlchd.=$childs['mat'].",".(round($detalle['chdmat'] / $detalle['markup'][0]['mat'],$this->decimales)).",";
								}else{
									$sqlchd.="0,0,";
								}
								if(isset($childs['tpl'])){
									$sqlchd.=$childs['tpl'].",".(round($detalle['chdtpl'] / $detalle['markup'][0]['tpl'],$this->decimales)).",";
								}else{
									$sqlchd.="0,0,";
								}
								$chdsgl.="0, $idCotOtas)";
								$db1->Execute($sqlchd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							}
							///////////////////////////////////////////////////////////////
							////////////////////////////Cliente////////////////////////////

							$mkclient = $client['mk'][$area];

							$insUser = $client['id_user'];
							if($data['info']['habs'][$id_tipohabitacion]['directa']==0){
							
								$mkclient =  $detalle['markup'][1];
							}
							
							require_once("/var/www/otas/clases/usuario.php");
							$userClass = new Usuario();
							$userData = $userClass->getMeThatUser($db1, $id_usuario);
							if(is_numeric($userData->Fields('usu_codigo'))){
								$insUser = $userData->Fields('usu_codigo');
							}

							$aplicaiva = ($cityData->Fields('aplica_iva')==0 && $area ==2)?0:1;

							if($sgl>0){
								$sglVal = $this->evalCalcLines($mkclient, $clientData[$detalle['hotdet_cli']]['sgl'], 1, $aplicaiva, $sgl, $calclines[$detalle['idcliente']]['hotocu'][$area]);
							}else{
								$sglVal = 0;
							}
							if($twin>0){
								$twinVal = $this->evalCalcLines($mkclient, $clientData[$detalle['hotdet_cli']]['dbl'], 2, $aplicaiva, $twin, $calclines[$detalle['idcliente']]['hotocu'][$area]);
							}else{
								$twinVal = 0;
							}
							if($mat>0){
								$matVal = $this->evalCalcLines($mkclient, $clientData[$detalle['hotdet_cli']]['dbl'], 2, $aplicaiva, $mat, $calclines[$detalle['idcliente']]['hotocu'][$area]);
							}else{
								$matVal = 0;
							}
							if($tpl>0){
								$tplVal = $this->evalCalcLines($mkclient, $clientData[$detalle['hotdet_cli']]['tpl'], 3, $aplicaiva, $tpl, $calclines[$detalle['idcliente']]['hotocu'][$area]);
							}else{
								$tplVal = 0;
							}
							
							$clientTotalValue+= round($sglVal + $twinVal + $matVal + $tplVal, $this->decimales);

							$SQLinsHcCli = "INSERT INTO ".$client['bd'].".hotocu (id_hotel, id_cot, id_cotdes, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_estado, id_hotdet, hc_valor1, hc_valor2, hc_valor3, hc_valor4, hc_mkhab1, hc_mkhab2, hc_mkhab3, hc_mkhab4) VALUES (
								".$clientData[$detalle['hotdet_cli']]['hotel'].",
								".$idCotCli.",
								".$idCotdesCli.",
								'".$dia."',
								$sgl,
								$twin,
								$mat,
								$tpl,
								0,
								".$detalle['hotdet_cli'].",
								".$sglVal.",
								".$twinVal.",
								".$matVal.",
								".$tplVal.",
								".$mkclient.",
								".$mkclient.",
								".$mkclient.",
								".$mkclient.")";
							$db1->Execute($SQLinsHcCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							///////////////////////////////////////////////////////////////


							////////////////////////////////////////////////////////////////
							//////////////////////////dispo global//////////////////////////   
							////////////////////////////////////////////////////////////////
							if(isset($detalle['idscglob'])){
								$sqlUpdateSc = "UPDATE  ".$client['bd'].".stock SET 
									sc_hab1 = (sc_hab1 + ".$detalle['sglDis']."),
									sc_hab2 = (sc_hab2 + ".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['twinDis'])."),
									sc_hab3 = (sc_hab3 + ".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['matDis'])."),
									sc_hab4 = (sc_hab4 + ".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['tplDis']).")
									WHERE id_stock = ".$detalle['idsl'];
								$db1->Execute($sqlUpdateSc) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								
								$sqlocudis = "INSERT INTO ".$this->dbhot.".hotocu (id_hotel,id_cot,hc_fecha,hc_hab1, hc_hab2, hc_hab3, hc_hab4,id_hotdet,id_cotdes,id_stock_global,id_cliente,hc_estado) VALUES (
									".$clientData[$detalle['hotdet_cli']]['hotel'].",
									$idCotCli,
									'".$dia."',
									".$detalle['sglDis'].",
									".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['twinDis']).",
									".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['matDis']).",
									".(($detalle['modeGlob'] == 1)?$detalle['sglDis']:$detalle['tplDis']).",
									".$detalle['hotdet_cli'].",
									$idCotdesCli,
									".$detalle['idscglob'].",
									".$detalle['idcliente'].",
									0)";
								
								$db1->Execute($sqlocudis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								$usaGlobal = true;
							}
							////////////////////////////////////////////////////////////////
							////////////////////////////////////////////////////////////////
							////////////////////////////////////////////////////////////////
						}
					}
					/////////////////////////////////////otas//////////////////////////////////////
					$SQLupdaCotdesOtas= "UPDATE ".$this->dbMain.".cotdes SET
						id_ciudad = $id_ciudad,
						cd_hab1 = $sgl,
						cd_hab2 = $twin,
						cd_hab3 = $mat,
						cd_hab4 = $tpl,
						cd_fecdesde = (SELECT MIN(hc_fecha) FROM ".$this->dbMain.".hotocu WHERE id_cotdes = $idCotdesOtas),
						cd_fechasta = DATE_ADD((SELECT MAX(hc_fecha) FROM ".$this->dbMain.".hotocu WHERE id_cotdes = $idCotdesOtas), INTERVAL 1 DAY),
						id_hotel = $id_hotel,
						id_seg = 7,
						cd_numpas = ".count($paxes).",
						cd_valor = ".$matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['totValue'].",
						id_tipohabitacion = $id_tipohabitacion,
						usa_stock_dist = '".(($usaGlobal)?'S':'N')."',
						cd_mailope = 0,
						cd_mailhot = 0
						WHERE id_cotdes = ".$idCotdesOtas;
					$db1->Execute($SQLupdaCotdesOtas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					///////////////////////////////////////////////////////////////////////////////

					$cd_valor = $this->evalCalcLines($client['mk'][$area],$clientTotalValue, 1, $aplicaiva, 1, $calclines[$detalle['idcliente']]['cotdes'][$area]);
					////////////////////////////////////cliente////////////////////////////////////
					$SQLupdaCotdesCli = "UPDATE ".$client['bd'].".cotdes SET
						id_ciudad = ".$clientData[$detalle['hotdet_cli']]['ciu'].",
						cd_hab1 = $sgl, 
						cd_hab2 = $twin,
						cd_hab3 = $mat,
						cd_hab4 = $tpl,
						cd_fecdesde = (SELECT MIN(hc_fecha) FROM ".$this->dbMain.".hotocu WHERE id_cotdes = $idCotdesOtas),
						cd_fechasta = DATE_ADD((SELECT MAX(hc_fecha) FROM ".$this->dbMain.".hotocu WHERE id_cotdes = $idCotdesOtas), INTERVAL 1 DAY),
						id_hotel = ".$clientData[$detalle['hotdet_cli']]['hotel'].",
						id_hotdet = ".$detalle['hotdet_cli'].",
						id_seg = 7,
						cd_numpas = ".count($paxes).",
						cd_valor = $cd_valor,
						id_tipohabitacion = ".$clientData[$detalle['hotdet_cli']]['thab'].",
						usa_stock_dist = '".(($usaGlobal)?'S':'N')."'
						WHERE id_cotdes = $idCotdesCli";
					$db1->Execute($SQLupdaCotdesCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					//////////////////////////////////////////////////////////////////////////////
						

					$rsCancelDate = $this->getCancelDate($db1, $idCotOtas);
					$fullcredit = 0;
					$clientfcredit = "";
					$obsclient = $extras['notes'];
					if(isset($extras['fullcredit'])){
						if($extras['fullcredit']){
							$fullcredit = 1;
							if(isset($client['esColls']['cot']['cotcredit'])){
								if($client['esColls']['cot']['cotcredit']=='cot_obs'){
									$obsclient = "Full Credit - ".$extras['notes'].",";
								}else{
									$clientfcredit = "fullcredit = ".$fullcredit;
								}
							}else{
								$obsclient = "Full Credit - ".$extras['notes'];
							}
						}
					}


					$subagencia = 0;
					if(isset($extras['id_subag'])){
						$subagencia = $extras['id_subag'];
					}
				
					/////////////////////////////////////otas//////////////////////////////////////
					$SQLupdateCotOtas = "UPDATE ".$this->dbMain.".cot SET 
						cot_numpas = ".count($paxes).",
						cot_fecdesde = (SELECT MIN(cd_fecdesde) FROM ".$this->dbMain.".cotdes WHERE id_cot = $idCotOtas),
						cot_fechasta = (SELECT MAX(cd_fechasta) FROM ".$this->dbMain.".cotdes WHERE id_cot = $idCotOtas),
						cot_fecpago = (SELECT DATE_ADD(MAX(cd_fechasta), INTERVAL 15 DAY) FROM ".$this->dbMain.".cotdes WHERE id_cot = $idCotOtas),
						id_seg = 7,
						id_agencia = ".$agencyInfo->Fields('id_agencia').",
						id_subagencia = $subagencia,
						cot_mk_extra = ".$mkclient.",
						cot_fec = NOW(),
						cot_estado = 0,
						id_tipopack = 3,
						cot_obs = '".((isset($extras['notes']))?$extras['notes']:'')."',
						cot_contactos = '".((isset($extras['contactos']))?$extras['contactos']:'')."',
						cot_fullcredit = $fullcredit,
						cot_correlativo = '".(isset($extras['clientcode'])?$extras['clientcode']:'')."',
						cot_valor = ".$matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['totValue'].",
						id_usuario = $id_usuario, 
						ha_hotanula = '".$rsCancelDate->Fields('fecha_anulacion')."',
						id_usuconf = $id_usuario,
						cot_fecconf = NOW(),
						cot_pripas = '".$paxes[0]['name']."',
						cot_pripas2 = '".$paxes[0]['lastname']."',
						cot_pridestino = $id_ciudad,
						cot_prihotel = $id_hotel,
						id_mon = $currency,
						id_area = $area,
						cot_refundable = ".$matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['refundable'].",
						cot_compracliente = ".(($iscotclient)?0:1)."
						WHERE id_cot = $idCotOtas";
						
					$db1->Execute($SQLupdateCotOtas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					///////////////////////////////////////////////////////////////////////////////

					
					
					////////////////////////////////////cliente////////////////////////////////////
					$SQLuodateCotCli = "UPDATE ".$client['bd'].".cot SET
						cot_numpas = ".count($paxes).",
						cot_fecdesde = (SELECT MIN(cd_fecdesde) FROM ".$this->dbMain.".cotdes WHERE id_cot = $idCotOtas),
						cot_fechasta = (SELECT MAX(cd_fechasta) FROM ".$this->dbMain.".cotdes WHERE id_cot = $idCotOtas),
						id_seg = 7,
						id_operador = (SELECT id_empresa FROM ".$client['bd'].".usuarios WHERE id_usuario = ".$insUser."),
						cot_fec = NOW(),
						cot_estado = 0,
						id_tipopack = 3,
						$clientfcredit
						cot_obs = '".$obsclient."',
						".$client['esColls']['cot']['gandhi_id'][0]." = $idCotOtas,
						cot_valor = $cd_valor,
						id_usuario = ".$insUser.",
						ha_hotanula = '".$rsCancelDate->Fields('fecha_anulacion')."',
						id_usuconf = ".$insUser.",
						cot_fecconf = NOW(),
						cot_pripas = '".$paxes[0]['name']."',
						cot_pripas2 = '".$paxes[0]['lastname']."',
						cot_pridestino = ".$clientData[$detalle['hotdet_cli']]['ciu'].",
						cot_prihotel = ".$clientData[$detalle['hotdet_cli']]['hotel'].",
						cot_stcon = 0,
						id_mon = $currency,
						id_area = $area,
						id_tipocot = 2
						WHERE id_cot = $idCotCli";
					$db1->Execute($SQLuodateCotCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					//////////////////////////////////////////////////////////////////////////////

					/////////////////////////////////////otas//////////////////////////////////////
					foreach($paxes as $index => $infoPax){
						$sqlInsPax = "INSERT INTO ".$this->dbMain.".cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais, id_cotdes) VALUES 
							($idCotOtas,
							'".$infoPax['name']."',
							'".$infoPax['lastname']."',
							'".$infoPax['passport']."',
							".$infoPax['countryid'].",
							$idCotdesOtas)";
						$db1->Execute($sqlInsPax) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					}
					///////////////////////////////////////////////////////////////////////////////

					////////////////////////////////////cliente////////////////////////////////////
					$sqlInsPaxCli = "INSERT INTO ".$client['bd'].".cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais, id_cotdes, cp_numvuelo)
						SELECT $idCotCli, cp_nombres, cp_apellidos, cp_dni, p2.id_pais, $idCotdesCli, cp_numvuelo
						FROM ".$this->dbMain.".cotpas cp 
						INNER JOIN ".$this->dbMain.".pais p ON cp.id_pais = p.id_pais 
						INNER JOIN ".$client['bd'].".pais p2 ON TRIM(p.pai_nombre) = TRIM(p2.pai_nombre)
						WHERE id_cot = $idCotOtas";
					$db1->Execute($sqlInsPaxCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					//////////////////////////////////////////////////////////////////////////////


					
					$insMail = "INSERT INTO ".$this->dbMain.".mailxota (id_cot, id_tipomail) VALUES ($idCotOtas, 1)";
					$db1->Execute($insMail) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


					$insMail2 = "INSERT INTO ".$this->dbhot.".mailxclientes (id_cot, id_cotdes, id_cliente, conf_op, conf_hot) VALUES ($idCotCli, $idCotdesCli, $clientid,1,1)";
					$db1->Execute($insMail2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					if($matriz[$id_hotel]['info']['hot_test']==1 && $matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['directa']==1){
						$agencyClass->updateAgencyCredit($db1,$agencyInfo, $matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['totValue'],$this->decimales);
					}

					// $agencyClass->insertPushChange($db1,$agencyInfo->Fields('id_agencia'),$idCotOtas);
					
					$res['status']= "SUCCESS";
					$res['description'] = "Booking Complete";
					$res['bookingcode'] = $idCotOtas;
					$res['refundable'] = $matriz[$id_hotel]['info']['habs'][$id_tipohabitacion]['refundable'];
					$this->clDebug();
					return $res;
				}
			}else{
				$res['status']  = "ERROR";
				$res['errorcode'] = 101;
				$res['description'] = "Prices matrix is empty";
				$this->clDebug();
				return $res;
			}
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function getCancelDate($db1, $id_cot){
		try{
			$this->opDebug(__FUNCTION__);
			$SQLgetDate = "SELECT
				DATE_FORMAT(MIN(DATE_SUB(cd.cd_fecdesde,INTERVAL IF(ISNULL(ha.ha_dias),".$this->cancelationDays.",ha.ha_dias) DAY)),'%Y-%m-%d') AS fecha_anulacion
				FROM
				".$this->dbMain.".cotdes AS cd
				LEFT JOIN ".$this->dbMain.".hotanula AS ha ON ha.ha_fecdesde <= cd.cd_fecdesde
				AND ha.ha_fechasta >= cd.cd_fechasta
				AND ha.ha_estado = 0
				AND cd.id_hotel = ha.id_hotel
				WHERE
				cd.cd_estado = 0
				AND cd.id_cot = $id_cot";
			$RESgetDate = $db1->Execute($SQLgetDate) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $RESgetDate;

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function createBuyerCot($db1,$cliente,$idarea){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlIns = "INSERT INTO ".$cliente['bd'].".cot (id_tipocot, id_tipopack, cot_fec, id_area,id_mon) VALUES (3,3,NOW(),$idarea,$idarea)";
			$db1->Execute($sqlIns) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$idcotbuyer = $db1->Insert_ID();
			$this->clDebug();
			return $idcotbuyer;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function finishBuyerCot($db1,$cliente,$idcotbuyer,$idcotgandhi,$markup,$sesvars=null){
		try{
			$applycom = false;
			$decstouse = $this->decimales;
			if($sesvars!=null){
				$iduserbuyer = $sesvars['idexternalbuyer'];
				$idopebuyer = $sesvars['idexternalope'];
			}else{
				$iduserbuyer = $_SESSION['idexternalbuyer'];
				$idopebuyer = $_SESSION['idexternalope'];
			}

			$sqlGandhiCot = "SELECT id_area, cot_fecdesde, cot_fechasta, cot_pripas, cot_pripas2, ha_hotanula, cot_numpas FROM ".$this->dbMain.".cot WHERE id_cot = ".$idcotgandhi;
			$resGandhiCot = $db1->Execute($sqlGandhiCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$sqlGetPaxes = "SELECT * FROM ".$this->dbMain.".cotpas WHERE id_cot = $idcotgandhi";
			$resGetPaxes = $db1->Execute($sqlGetPaxes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($resGandhiCot->Fields('id_area')==2){
				$decstouse = 0;
				$applycom = true;
				$sqlGetCom = "SELECT com_comag 
					FROM  ".$cliente['bd'].".comision com 
					INNER JOIN ".$cliente['bd'].".hotel h ON com.id_grupo = h.id_grupo 
					WHERE com_tipo = 'HTL' AND id_hotel = ".$idopebuyer;
				$resGetCom = $db1->Execute($sqlGetCom) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				if($resGetCom->Fields('com_comag')!=0){
					$comision = $resGetCom->Fields('com_comag');
				}else{
					$applycom = false;
				}
			}
			$sqlInsCotdes = "INSERT INTO ".$cliente['bd'].".cotdes (id_cot,id_tipohabitacion) VALUES ($idcotbuyer,null)";
			$db1->Execute($sqlInsCotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$idcotdesbuyer = $db1->Insert_ID();

			//empezamos el mambo con hotocu de otas para calcular e insertar los hotocus de comprador.
			$sqlGandhiHotocus = "SELECT * FROM ".$this->dbMain.".hotocu WHERE id_cot = ".$idcotgandhi." AND hc_estado = 0";
			$resGandhiHotocus = $db1->Execute($sqlGandhiHotocus) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$hcvals = array();
			$totval = 0;
			while(!$resGandhiHotocus->EOF){
				for($i=1; $i <=4 ; $i++){
					if($resGandhiHotocus->Fields("hc_hab$i")>0){
						$hcvals[$i] = round($resGandhiHotocus->Fields("hc_valor$i") / $markup,$decstouse);
					}else{
						$hcvals[$i] = 0;
					}
					$totval+=$hcvals[$i];
					if($applycom){
						$hcvals[$i] = $hcvals[$i] - round($hcvals[$i] * $comision / 100, $decstouse);
					}
				}
				$insHotocuBuyer = "INSERT INTO ".$cliente['bd'].".hotocu (id_cot, id_cotdes, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotel, id_hotdet, hc_valor1, hc_valor2, hc_valor3, hc_valor4, hc_mkhab1, hc_mkhab2, hc_mkhab3, hc_mkhab4) VALUES (
					$idcotbuyer,
					$idcotdesbuyer,
					'".$resGandhiHotocus->Fields('hc_fecha')."',
					".$resGandhiHotocus->Fields('hc_hab1').",
					".$resGandhiHotocus->Fields('hc_hab2').",
					".$resGandhiHotocus->Fields('hc_hab3').",
					".$resGandhiHotocus->Fields('hc_hab4').",
					null,
					null,
					".$hcvals[1].",
					".$hcvals[2].",
					".$hcvals[3].",
					".$hcvals[4].",
					".(($resGandhiHotocus->Fields('hc_hab1')>0)?$markup:'null').",
					".(($resGandhiHotocus->Fields('hc_hab2')>0)?$markup:'null').",
					".(($resGandhiHotocus->Fields('hc_hab3')>0)?$markup:'null').",
					".(($resGandhiHotocus->Fields('hc_hab4')>0)?$markup:'null').")";
				$db1->Execute($insHotocuBuyer) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$resGandhiHotocus->MoveNext();
			}

			$updCotdesBuyer = "UPDATE ".$cliente['bd'].".cotdes cd 
				INNER JOIN (SELECT 
					id_cot,
					hc_hab1, 
					hc_hab2, 
					hc_hab3, 
					hc_hab4, 
					'".$resGandhiCot->Fields('cot_fecdesde')."' as fec1, 
					'".$resGandhiCot->Fields('cot_fechasta')."' as fec2, 
					7 as id_seg,
					".$resGetPaxes->RecordCount()." as numpas,
					SUM(hc_valor1+ hc_valor2+hc_valor3+hc_valor4) as valor,
					".(($applycom)?$totval:'null')." as venta
					FROM ".$cliente['bd'].".hotocu WHERE id_cot = $idcotbuyer 
				) t1 ON cd.id_cot = t1.id_cot
				SET cd_hab1 = hc_hab1,
					cd_hab2 = hc_hab2,
					cd_hab3 = hc_hab3,
					cd_hab4 = hc_hab4,
					cd_fecdesde = fec1,
					cd_fechasta = fec2,
					cd.id_seg = t1.id_seg,
					cd_numpas = numpas,
					cd_valor = valor,
					cd_venta = venta
				WHERE cd.id_cot = $idcotbuyer";
			$db1->Execute($updCotdesBuyer) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$updCotBuyer = "UPDATE ".$cliente['bd'].".cot  
				INNER JOIN ".$cliente['bd'].".cotdes cd ON cot.id_cot = cd.id_cot
				SET cot_valor = cd_valor,
					cot_pripas = '".$resGetPaxes->Fields('cp_nombres')."',
					cot_pripas2 = '".$resGetPaxes->Fields('cp_apellidos')."',
					cot_fecconf = NOW(),
					ha_hotanula = '".$resGandhiCot->Fields('ha_hotanula')."',
					id_usuario = $iduserbuyer,
					id_operador = $idopebuyer,
					cot.id_seg = 7,
					cot.id_usuconf = cot.id_usuario,
					cot_fecdesde = '".$resGandhiCot->Fields('cot_fecdesde')."',
					cot_fechasta = '".$resGandhiCot->Fields('cot_fechasta')."',
					cot_numpas = cd_numpas,
					cot_venta = cd_venta
				WHERE cot.id_cot = $idcotbuyer";
			$db1->Execute($updCotBuyer) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$sqlinspaxbuyer = "INSERT INTO ".$cliente['bd'].".cotpas (id_cot, id_cotdes, cp_nombres, cp_apellidos, cp_dni) VALUES ";
			$paxsentences = array();
			while(!$resGetPaxes->EOF){
				$strins = "($idcotbuyer, $idcotdesbuyer, '".$resGetPaxes->Fields('cp_nombres')."','".$resGetPaxes->Fields('cp_apellidos')."','".$resGetPaxes->Fields('cp_dni')."')";
				array_push($paxsentences, $strins);
				$resGetPaxes->MoveNext();
			}
			$sqlinspaxbuyer.= implode(",", $paxsentences);
			$db1->Execute($sqlinspaxbuyer) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			$this->clDebug();
			return true;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function cotAnulator($db1, $idcotgandhi, $id_user){
		try{
			$this->opDebug(__FUNCTION__);
			$clientes = $this->getClientes($db1);
			require_once('/var/www/otas/clases/tarifa.php');
			$tar = new Tarifa();
			
			$sqlGetCot = "SELECT 
				DATE_FORMAT(cot_fecdesde, '%Y-%m-%d') as cot_fecdesde,
				if(DATE_FORMAT(cot_fecdesde, '%Y-%m-%d') < DATE_FORMAT(NOW(),'%Y-%m-%d'), 's','n') as noshow, 
				if(DATE_FORMAT(ha_hotanula, '%Y-%m-%d') < DATE_FORMAT(NOW(),'%Y-%m-%d'), 's','n') as outdate,
				cot_refundable,
				id_mon,
				cot_valor,
				hot_test
				FROM ".$this->dbMain.".cot c
				INNER JOIN ".$this->dbMain.".ciudad ci 
				ON c.cot_pridestino = ci.id_ciudad 
				INNER JOIN ".$this->dbMain.".hotel h ON c.cot_prihotel = h.id_hotel
				WHERE c.id_cot = ".$idcotgandhi;
			$resGetCot = $db1->Execute($sqlGetCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$noshow = false;
			$outdate = false;

			require_once('/var/www/otas/clases/agencia.php');
			$ag = new Agencia();
			$agencia = $ag->getMeThatAgencyByUser($db1,$id_user);
			if($agencia->Fields('ag_cobrarnoshow')==0 && $resGetCot->Fields('noshow')=='s'){
				$noshow = true;
			}
			if($agencia->Fields('ag_cobrarnoshow')==0 && $resGetCot->Fields('outdate')=='s'){
				$outdate = true;
			}

			if($resGetCot->Fields('cot_refundable')==1){
				$res['status'] = "ERROR";
				$res['description'] = "Booking not refundable.";
				$res['errorcode'] = 114;
				return $res;
			}

			// obtenemos todas las cots de clientes distintas
			$GETcotClient = "SELECT id_cotcli, id_cliente, MIN(hc_fecha) as fecdesde FROM ".$this->dbMain.".hotocu WHERE id_cot = ".$idcotgandhi." GROUP BY id_cotcli, id_cliente";
			$REScotClient = $db1->Execute($GETcotClient) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//por cada cotizacion de cliente, vamos a shittiar
			$condNoShow = "";
			if($noshow || $outdate){
				$condNoShow=" AND hc_fecha != '".$resGetCot->Fields('cot_fecdesde')."'";
				if($resGetCot->Fields('id_mon')==2){
					$this->decimales=0;
				}

			}

			while(!$REScotClient->EOF){
				$cliente = $clientes[$REScotClient->Fields('id_cliente')];
				//traemos los hotocus con el cotdes del cliente
				$sqlCotdesCli = "SELECT cd.id_cotdes, hc_hab1, hc_hab2, hc_hab3, hc_hab4, (hc_valor1+hc_valor2+hc_valor3+hc_Valor4) as hc_valor, hc.id_hotdet, hc_fecha, cd.id_cot FROM ".$cliente['bd'].".cotdes cd
					INNER JOIN ".$cliente['bd'].".hotocu hc ON cd.id_cotdes = hc.id_cotdes
					WHERE cd.id_cot = ".$REScotClient->Fields('id_cotcli')." AND hc_estado = 0";
				$resCotdesCli = $db1->Execute($sqlCotdesCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				if($resCotdesCli->RecordCount()>0){
					//por cada hotocu del weas, vamos a validar shitties
					while(!$resCotdesCli->EOF){
						//if(($noshow || $outdate) && $resGetCot->Fields('cot_fecdesde')!=$resCotdesCli->Fields('hc_fecha')){
						if((($noshow || $outdate) && $resGetCot->Fields('cot_fecdesde')!=$resCotdesCli->Fields('hc_fecha')) || (!$noshow && !$outdate)){

							//si el cotdes del cliente ocupo global, va a buscar a esos pendejos pa chingarles su madre
							if($resCotdesCli->Fields('usa_stock_dist')=='S'){
								$SQLgetGlobalHc = "SELECT * FROM ".$this->dbhot.".hotocu 
									WHERE hc_estado = 0
									AND id_cliente = ".$REScotClient->Fields('id_cliente')." 
									AND id_hotdet = ".$resCotdesCli->Fields('id_hotdet')."
									AND hc_fecha = '".$resCotdesCli->Fields('hc_fecha')."'
									AND id_cot = ".$REScotClient->Fields('id_cotcli');
								$RESgetGlobalHc = $db1->Execute($SQLgetGlobalHc) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								//si pilla hotocu global, se chinga el stock del cliente
								if($RESgetGlobalHc->RecordCount()>0){
									$SQLupdaScCli = "UPDATE ".$cliente['bd'].".stock SET 
										sc_hab1 = (sc_hab1 - ".$RESgetGlobalHc->Fields('hc_hab1')."),
										sc_hab2 = (sc_hab2 - ".$RESgetGlobalHc->Fields('hc_hab2')."),
										sc_hab3 = (sc_hab3 - ".$RESgetGlobalHc->Fields('hc_hab3')."),
										sc_hab4 = (sc_hab4 - ".$RESgetGlobalHc->Fields('hc_hab4').")
										WHERE id_hotdet = ".$resCotdesCli->Fields('id_hotdet')." 
										AND sc_fecha = '".$resCotdesCli->Fields('hc_fecha')."'";
									$db1->Execute($SQLupdaScCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								}
							}else{
								//si no es global actualizamos dispo en gandhi.
								$tar->dispoReducer($db1, $REScotClient->Fields('id_cliente'),-$resCotdesCli->Fields('hc_hab1') , -$resCotdesCli->Fields('hc_hab2'), -$resCotdesCli->Fields('hc_hab3'), -$resCotdesCli->Fields('hc_hab4'), $resCotdesCli->Fields('hc_fecha'),null, $resCotdesCli->Fields('id_hotdet'));
							}
						}
						$resCotdesCli->MoveNext();
					}
					$resCotdesCli->MoveFirst();
				}
				//anulamos los hotocus de global
				$sqlUpGlobHc = "UPDATE ".$this->dbhot.".hotocu SET hc_estado = 1 WHERE  id_cliente = ".$REScotClient->Fields('id_cliente')." AND id_cot = ".$REScotClient->Fields('id_cotcli').$condNoShow;
				$db1->Execute($sqlUpGlobHc) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				//anulamos el hotocu del cliente
				$sqlUpHcCli = "UPDATE ".$cliente['bd'].".hotocu SET hc_estado = 1 WHERE id_cot = ".$REScotClient->Fields('id_cotcli').$condNoShow;
				$db1->Execute($sqlUpHcCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

				//anulamos la cotizacion del cliente si no es noshow o esas weas
				if(($noshow || $outdate) && $resGetCot->Fields('cot_fecdesde') == $REScotClient->Fields('fecdesde')){
					$sqlUpCotdesCli = "UPDATE ".$cliente['bd'].".cotdes 
						SET 
							cd_fechasta = DATE_ADD(cd_fecdesde, INTERVAL 1 DAY), 
							cd_valor = (SELECT ROUND((hc_valor1+hc_valor2+hc_valor3+hc_valor4),".$this->decimales." ) FROM ".$cliente['bd'].".hotocu WHERE id_cot = ".$REScotClient->Fields('id_cotcli')." AND hc_estado = 0)
						WHERE id_cot = ".$REScotClient->Fields('id_cotcli');
					$db1->Execute($sqlUpCotdesCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$sqlUpCotCli = "UPDATE ".$cliente['bd'].".cot 
						SET 
							cot_fechasta = DATE_ADD(cot_fecdesde, INTERVAL 1 DAY), 
							cot_valor = (SELECT ROUND((hc_valor1+hc_valor2+hc_valor3+hc_valor4),".$this->decimales." ) FROM ".$cliente['bd'].".hotocu WHERE id_cot = ".$REScotClient->Fields('id_cotcli')." AND hc_estado = 0)
						WHERE id_cot = ".$REScotClient->Fields('id_cotcli');
					$db1->Execute($sqlUpCotCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


					$insmailcli = "INSERT INTO ".$this->dbhot.".mailxclientes (id_cot, id_cliente, mod_hot, mod_op) VALUES (".$REScotClient->Fields('id_cotcli').",".$REScotClient->Fields('id_cliente').",1,1)";
					$db1->Execute($insmailcli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}else{
					$sqlUpCotCli = "UPDATE ".$cliente['bd'].".cot SET cot_estado = 1, cot_fecanula = NOW(), id_usuanula = ".$cliente['id_user']." WHERE id_cot = ".$REScotClient->Fields('id_cotcli');
					$db1->Execute($sqlUpCotCli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

					$insmailcli = "INSERT INTO ".$this->dbhot.".mailxclientes (id_cot, id_cliente, anula_hot, anula_op) VALUES (".$REScotClient->Fields('id_cotcli').",".$REScotClient->Fields('id_cliente').",1,1)";
					$db1->Execute($insmailcli) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
				}
				
				$REScotClient->MoveNext();
			}
			//updateamos la dispo en gandhi
			$sqlUpHcGandhi = "UPDATE ".$this->dbMain.".hotocu SET hc_estado = 1 WHERE id_cot = ".$idcotgandhi.$condNoShow;
			$db1->Execute($sqlUpHcGandhi) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//$sqlUpHcGandhi2 = "UPDATE ".$this->dbMain.".hotocu SET hc_valor1 = ROUND(hc_valor1 * $impuesto, ".$this->decimales."), 
			//	hc_valor2 = ROUND(hc_valor2 * $impuesto, ".$this->decimales."),
			//	hc_valor3 = ROUND(hc_valor3 * $impuesto, ".$this->decimales."),
			//	hc_valor4 = ROUND(hc_valor4 * $impuesto, ".$this->decimales.") WHERE hc_fecha = '".$resGetCot->Fields('cot_fecdesde')."' AND id_cot = ".$idcotgandhi;
			//$db1->Execute($sqlUpHcGandhi2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//$lastHotocu = $db1->Insert_ID();

			//updatea child como este hotocu
			$sqlupchd="UPDATE ".$this->dbMain.".childocu co INNER JOIN ".$this->dbMain.".hotocu hc ON co.id_hotocu = hc.id_hotocu 
				SET co.cc_estado = hc.hc_estado	WHERE hc.id_cot = ".$idcotgandhi;
			$db1->Execute($sqlupchd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());			

			//updatiamos cotdes pa que mande los correos de anulacion
			if($noshow || $outdate){
				$sqlUpCotdes = "UPDATE ".$this->dbMain.".cotdes SET 
					cd_fechasta = DATE_ADD(cd_fecdesde, INTERVAL 1 DAY), 
					cd_valor = (SELECT
						(SELECT ROUND((hc_valor1+hc_valor2+hc_valor3+hc_valor4),".$this->decimales." ) FROM ".$this->dbMain.".hotocu WHERE id_cot = $idcotgandhi AND hc_estado = 0) +
						IFNULL((SELECT ROUND((cc_valsgl + cc_valtwin + cc_valmat + cc_valtpl),".$this->decimales.") FROM ".$this->dbMain.".childocu WHERE id_cot = $idcotgandhi AND cc_estado = 0),0)
					)
					 WHERE id_cot = ".$idcotgandhi;
				$db1->Execute($sqlUpCotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}


			//updateamos la cotizacion en gandhi
			
			$sqlUpCotGandhi = "UPDATE ".$this->dbMain.".cot 
				SET 
				cot_estado = ".(($noshow || $outdate)?0:1).",
				id_usuanula = $id_user,
				cot_fecanula = NOW(),
				cot_noshow = ".(($noshow)?0:1).",
				cot_outdate = ".(($outdate)?0:1);
				if($noshow || $outdate){
					$sqlUpCotGandhi.=",cot_fechasta = DATE_ADD(cot_fecdesde, INTERVAL 1 DAY), 
					cot_valor = (SELECT
						(SELECT ROUND((hc_valor1+hc_valor2+hc_valor3+hc_valor4),".$this->decimales." ) FROM ".$this->dbMain.".hotocu WHERE id_cot = $idcotgandhi AND hc_estado = 0) +
						IFNULL((SELECT ROUND((cc_valsgl + cc_valtwin + cc_valmat + cc_valtpl),".$this->decimales.") FROM ".$this->dbMain.".childocu WHERE id_cot = $idcotgandhi AND cc_estado = 0),0)
					)";
				}
				$sqlUpCotGandhi.=" WHERE id_cot = ".$idcotgandhi;
			$db1->Execute($sqlUpCotGandhi) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($noshow||$outdate){
				$sqlGetPrice = "SELECT cot_valor FROM ".$this->dbMain.".cot WHERE id_cot = ".$idcotgandhi;
				$resGetPrice = $db1->Execute($sqlGetPrice) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$penalty = $resGetPrice->Fields('cot_valor');
			}else{
				$penalty = 0;
			}

			///credito agencia
			$REScotClient->MoveFirst();
			if($resGetCot->Fields('hot_test')==1 && $resGetCot->Fields('id_agencia')!=$clientes[$REScotClient->Fields('id_cliente')]['id_agencia']){
				$agencia = $ag->getMeThatAgency($db1,$agencia->Fields('id_agencia'),true,$resGetCot->Fields('id_mon'));
				$ag->updateAgencyCredit($db1,$agencia, -(round(($resGetCot->Fields('cot_valor') - $penalty),$this->decimales)),$this->decimales);
			}

			//$agencyClass->insertPushChange($db1,$agencia->Fields('id_agencia'),$idcotgandhi);
			
			$res['status'] = "SUCCESS";
			if($noshow||$outdate){
				$res['description'] = "Cancellation with penalty";
				$res['penalty'] = $penalty;
				$insMail = "INSERT INTO ".$this->dbMain.".mailxota (id_cot, id_tipomail) VALUES ($idcotgandhi, 3)";
				$db1->Execute($insMail) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


			}else{
				$res['description'] = "Cancellation complete.";
				$res['penalty'] = 0;

				$insMail = "INSERT INTO ".$this->dbMain.".mailxota (id_cot, id_tipomail) VALUES ($idcotgandhi, 2)";
				$db1->Execute($insMail) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$this->clDebug();
			return $res;

		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
		}
	}
	public function cotBuyerAnulator($db1,$idcotbuyer, $id_cotgandhi, $cliente, $markup,$remainsactive,$sesvars=null){
		try{
			$this->opDebug(__FUNCTION__);

			if(isset($sesvars)){
				$govars = $sesvars;
			}else{
				$govars = null;
			}

			$sqlcancelhc = "UPDATE ".$cliente['bd'].".hotocu SET hc_estado = 1 WHERE id_cot = $idcotbuyer";
			$db1->Execute($sqlcancelhc) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			if($remainsactive){
				$this->finishBuyerCot($cliente, $idcotbuyer,$id_cotgandhi, $markup,$govars);
			}else{
				$sqlcancelcot = "UPDATE ".$cliente['bd'].".cot_estado SET cot_estado = 1 WHERE id_cot = $idcotbuyer";
				$db1->Execute($sqlcancelcot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}


			$this->clDebug();
			return true;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
			return false;
		}
	}
	public function getHotocusBuyer($db1,$idcotbuyer,$cliente, $onlyfirst=false){
		try{
			$this->opDebug(__FUNCTION__);
			$sqlHc="SELECT * FROM ".$cliente['bd'].".hotocu WHERE hc_estado = 0 AND id_cot = $idcotbuyer";
			if($onlyfirst){
				$sqlHc.=" LIMIT 1";
			}
			$resHc = $db1->Execute($sqlHc) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$this->clDebug();
			return $resHc;
		}catch(Exception $e){
			var_dump($e);
			$this->clDebug();
			return false;
		}
	}

	public function getGlobalDispo($db1, $fechad, $fechah, $id_pk = null, $id_ciudad = null){
		try{
			$this->opDebug(__FUNCTION__);
			//query global 2.0... trae todas las tarifas autorizadas para venta del hotel y las deja guardadas en el arreglo
			
			$conds = array();

			if(strlen($fechah)==10){
				$condfh = "ADDDATE('".$fechah."',-1)";
			}else{
				$condfh = $fechah;
			}
			if(isset($id_pk)){
				array_push($conds, "sc.id_pk = ".$id_pk);
			}
			if(isset($id_ciudad)){
				array_push($conds, "h.id_ciudad = ".$id_ciudad);
			}
			if(count($conds)>0){
				$finalcond = " AND ".(implode(" AND ", $conds));
			}
			

			$sqlGlob = "SELECT sc.id_stock_global,
				sc.id_tipohab,
				sc_fecha,
				sc.id_pk,
				sc_minnoche,
				sc_hab1 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab1,0),0))) AS ds_hab1,
				sc_hab2 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab2,0),0))) AS ds_hab2,
				sc_hab3 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab3,0),0))) AS ds_hab3,
				sc_hab4 - (SUM(IF(hc_estado = 0, IFNULL(hc_hab4,0),0))) AS ds_hab4,
				sc_cerrado_sgl,
				sc_cerrado_twn,
				sc_cerrado_dbl,
				sc_cerrado_tpl,
				sc_cerrado,
				th_nombre
				FROM ".$this->dbhot.".stock_global sc 
				LEFT JOIN ".$this->dbhot.".hotocu hc ON
				sc.id_stock_global = hc.id_stock_global 
				INNER JOIN ".$this->dbhot.".tipohabitacion th ON
				sc.id_tipohab = th.id_tipohabitacion 
				INNER JOIN ".$this->dbhot." .hotdet hd ON
				sc.id_hotdet = hd.id_hotdet
				INNER JOIN ".$this->dbMain.".hotel h 
				ON sc.id_pk = h.id_pk
				WHERE sc_estado = 0
				AND sc_cerrado = 0 
				AND hd_estado = 0
				$finalcond
				AND th_sellotas = 0
				AND sc_fecha BETWEEN '$fechad' AND $condfh
				GROUP BY sc.id_stock_global, sc_fecha
				ORDER BY id_tipohab ASC, sc_fecha ASC";

			$resGlob = $db1->Execute($sqlGlob) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			//echo $sqlGlob; die();
			if($resGlob->RecordCount()>0){
				while(!$resGlob->EOF){
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab1'] = (($resGlob->Fields('ds_hab1')<0)?0:$resGlob->Fields('ds_hab1'));
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab2'] = (($resGlob->Fields('ds_hab2')<0)?0:$resGlob->Fields('ds_hab2'));
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab3'] = (($resGlob->Fields('ds_hab3')<0)?0:$resGlob->Fields('ds_hab3'));
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['dhab4'] = (($resGlob->Fields('ds_hab4')<0)?0:$resGlob->Fields('ds_hab4'));

					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')]['th_nombre'] = $resGlob->Fields('th_nombre');
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['cerrado1'] = $resGlob->Fields('sc_cerrado_sgl');
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['cerrado2'] = $resGlob->Fields('sc_cerrado_twn');
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['cerrado3'] = $resGlob->Fields('sc_cerrado_dbl');
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['cerrado4'] = $resGlob->Fields('sc_cerrado_tpl');

					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['minnoc'] = $resGlob->Fields('sc_minnoche');
					$globDisp[$resGlob->Fields('id_pk')][$resGlob->Fields('id_tipohab')][$resGlob->Fields('sc_fecha')]['idscglob'] = $resGlob->Fields('id_stock_global');
					$resGlob->MoveNext();
				}
			}else{
				$globDisp[$resGlob->Fields('id_pk')] = -1;
			}

			//var_dump($globDisp);die();
			return $globDisp;
		}catch(Exception $e){
			var_dump($e);
			if(isset($id_pk)){
				$globDisp[$id_pk]=-1;
			}else{
				$globDisp = array();
			}
			$this->clDebug();
			return $globDisp;
		}
	}

	
	

}


