<?php
class Language{
//esta mierda esta para poder agrupar las variables de la clase en el editor de texto que estes usando, you're welcome bitch
	//por defecto todas estas mierdas se declaran en español, si el construct recibe otro idioma, las cambia.
	public $ciudad = "ciudad";
	public $origen_paxes = "origen pasajeros";
	public $chileno = "chileno";
	public $extranjero = "extranjero";
	public $fecha_desde = "fecha desde";
	public $fecha_hasta = "fecha hasta";
	public $buscar = "buscar";
	public $confirmacion_instantanea = "confirmación instantanea";
	public $habitaciones = "habitaciones";
	public $programa_confirmado = "programa confirmado";
	public $programa_anulado = "programa anulado";
	public $operador = "comprador";
	public $precioventa = "precio venta";
	public $preciovalor = "monto a pagar";
	public $resumen_destino ="resumen destino";
	public $direccion = "dirección";
	public $destino = "destino";
	public $tipohabitacion = "tipo habitación";
	public $detallepax ="detalle pasajeros";
	public $pasajero = "pasajero";
	public $observaciones = "observaciones";
	public $anular = "anular";
	public $notrefundable = "no reembolsable";
	public $reservasrealizadas = "reservas realizadas";
	public $filtrarfechaspor = "filtrar fechas por";
	public $todos = "todos";
	public $todas = "todas";
	public $agencia = "agencia";
	public $proveedor = "proveedor";
	public $nofiltrar = "no filtrar";
	public $fechaconfirmacion = "fecha confirmación";
	public $anulado = "anulado";
	public $activo = "activo";
	public $noshow = "no show";
	public $aplicacobro = "con cobro";
	public $pasajeroprincipal = "pasajero principal";
	public $fechas = "fechas";
	public $comprador = "comprador";
	public $estado = "estado";
	public $correlativo = "correlativo";
	public $si = "si";
	public $subagencia = "sub comprador";
	public $ninguno = "ninguno";



	//mensajes largos
	public $erroranu = "error al tratar de anular reserva.";
	public $anuconcobro = "programa reducido, mantiene costo asociado.";
	public $msg_rooms = "cantidad de habitaciones debe ser mayor a 0";
	public $msg_timeout = "se ha exedido el tiempo de espera";
	public $msg_wrongcountry = "La nacionalidad del pasajero principal no coincide con el mercado escogido.";
	public $msg_couldntcreatecotbuyer = "No se pudo crear la reserva en cliente. Contacte a soporte.";
	public $msg_cotcreated = "La reserva ha sido confirmada.";
	public $msg_cotcreatedwitherrors = "La reserva se ha confirmado con errores. Contacte a soporte y entregue el siguiente ID:";
	public $msg_dispoconsumed = "La tarifa escogida ya no se encuentra disponible. Favor intente otra vez.";
	public $noresults = "No se encontraron resultados.";
	public $fecanula = "Fecha anulación sin costo";
	public $precioventadestino = "precio venta destino";
	public $preciovalordestino = "monto a pagar por destino";
	public $glosa1 = "gracias por comprar en __OPENAME__ Online. Ya deberías haber recibido una copia para tus registros, por lo tanto, revisa tu correo. Te recordamos que todas las políticas habituales de venta, modificaciones, y anulaciones de __OPENAME__ rigen para esta reserva.";
	public $glosa2 = "en caso de cualquier duda o consulta, escríbenos un mail a <a href='mailto:soporte@distantis.com'>soporte@distantis.com</a>.";
	public $esta_seguro="esto no puede deshacerse luego. ¿Realmente desea continuar?";
	public $erroragencia="el ID de la agencia compradora no corresponde con quien intenta anular.";
	public $errorcliente="no se pudo encontrar la credencial del cliente.";
	public $nodatafound = "no se pudo encontrar la información de la reserva.";
	public $noincluyeiva = "valores no incluyen IVA";

//
	function setLanguage($lanpicked){
		if(!is_numeric($lanpicked)){
			$lanpicked = $this->getLanId($general_code);
		}
		if($lanpicked==2){
			$this->ciudad = "city";
			$this->origen_paxes="passengers origin";
			$this->chileno = "chilean";
			$this->extranjero = "foreign";
			$this->fecha_desde = "date from";
			$this->fecha_hasta = "date to";
			$this->buscar = "search";
			$this->confirmacion_instantanea = "instant confirmation";
			$this->habitaciones = "rooms";
			$this->programa_confirmado = "confirmed program";
			$this->programa_anulado = "program cancelled";
			$this->operador = "Buyer";
			$this->precioventa = "selling price";
			$this->$preciovalor = "amount to pay";
			$this->resumen_destino = "destination detail";
			$this->direccion = "address";
			$this->pasajero = "passenger";
			$this->observaciones = "notes";
			$this->anular = "cancel";
			$this->notrefundable = "not refundable";
			$this->reservasrealizadas = "reservations made";
			$this->filtrarfechaspor = "filter dates by";
			$this->todos = "all";
			$this->todas = "all";
			$this->agencia = "agency";
			$this->proveedor = "provider";
			$this->nofiltrar = "don't filter";
			$this->fechaconfirmacion = "confirmation date";
			$this->anulado = "cancelled";
			$this->activo = "active";
			$this->noshow = "no show";
			$this->aplicacobro = "with penalty";
			$this->pasajeroprincipal = "main passenger";
			$this->fechas = "dates";
			$this->comprador = "buyer";
			$this->estado = "status";
			$this->correlativo = "correlative";
			$this->si="yes";
			$this->subagencia = "sub buyer";
			$this->$ninguno = "none";

			//mensajes largos
			$this->erroranu = "an error occurred while trying to cancel.";
			$this->anuconcobro = "program reduced, penalty will be charged.";
			$this->msg_rooms = "rooms request must be higher than 0";
			$this->msg_timeout ="timeout limit exceded";
			$this->msg_wrongcountry = "The nationality of the main passenger doesn't coincide with the chosen market";
			$this->msg_couldntcreatecotbuyer = "Couldn't create client booking. Get in contact with support.";
			$this->msg_cotcreated = "The reservation has been confirmed.";
			$this->msg_cotcreatedwitherrors = "The reservation has been confirmed with errors. Get in contact with support and deliver the following ID:";
			$this->msg_dispoconsumed = "The chosen rate is no longer available. Please try again.";
			$this->noresults = "No results where found.";
			$this->fecanula = "cancelation deadline";
			$this->precioventadestino = "selling price by destination";
			$this->preciovalordestino = "amount to pay by destination";
			$this->tipohabitacion = "room type";
			$this->detallepax ="passengers detail";
			$this->glosa1 = "Thank you for buying trough __OPENAME__ Online. Your reservation has been confirmed according to the information displayed above. At this moment you should have received an email with a copy of the reservation for your backup, so check your inbox. We remind you that that all OTSI common sales, modifications and cancellation policies applies to this reservation";
			$this->glosa2 = "Comments, doubts? Please send us an email to <a href='mailto:soporte@distantis.com'>soporte@distantis.com</a>.";
			$this->esta_seguro = "this can't be undone. Are you sure you want to proceed?";
			$this->$erroragencia="Buyer agency ID doesn't match with who's trying to cancel.";
			$this->$errorcliente="Client credentials couldn't be found.";
			$this->nodatafound = "program data couldn't be found.";
			$this->$noincluyeiva = "values do not include taxes";

		}

	}

//

	function getLanId($general_code){
		try{
			include("/var/www/otas/Connections/db1.php");
			$sqlLanId = "SELECT * FROM otas.idioma WHERE general_code = '".$general_code."'";
			$resLanId = $db1->Execute($sqlLanId) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			if($resLanId->RecordCount()>0){
				return $resLanId->Fields('id_idioma');
			}else{
				return 1;
			}

		}catch(Exception $e){
			var_dump($e);
		}
	}




}



?>