<?php
include("Connections/db1.php");
include("/var/www/otas/clases/cotizacion.php");
include("/var/www/otas/clases/language.php");
include("/var/www/otas/clases/usuario.php");
include("/var/www/otas/clases/agencia.php");
$cotClass = new Cotizacion();
$result = new \stdClass();
$userClass = new Usuario();
$lanClass = new Language();
$ageClass = new Agencia();

include('/var/www/otas/secure_opaco.php');


if(isset($_POST['flag'])){
	switch ($_POST['flag']) {
		case 'findcots':

			$inputs = str_replace('\"', '"',$_POST['inputs']);
			$inputs = json_decode($inputs,true);

			if(isset($_SESSION['idexternalope'])){
				$cliente = $_SESSION['id_cliente'];
				if($_SESSION['idexternalope']!=$cliente['id_agencia']){
					$extras['id_operador'] = $_SESSION['idexternalope'];
				}
				if($inputs['ciudad']!=0){
					$extras['cg.cot_pridestino'] = $inputs['ciudad'];
				}
				if($inputs['outsider']!=0){
					if($inputs['outsider']==1){
						$extras['cg.id_area'] == 2;
					}else{
						$extras['cg.id_area'] == 1;
					}
				}
				if($inputs['datefilter']!=0){
					if($inputs['datefilter']==1){
						$namecol = 'cg.cot_fecdesde';
					}else{
						$namecol = 'cg.cot_fecconf';
					}
					$extras[$namecol] = array("cond" => " between ","val" => "'".$inputs['dpfec_1']."' AND '".$inputs['dpfec_2']."'",);
				}
				
				if($inputs['correlativo']!=''){
					$extras['cb.cot_correlativo'] = $inputs['correlativo'];
				}

				$idcot = ($inputs['id_cotsearch']!='')?$inputs['id_cotsearch']:null;

				$rescot = $cotClass->getCotBuyer($db1, $idcot, $_SESSION['idagencia'] ,$cliente , $extras);

			}else{
				$data = array();
				$limit = null;
				if($inputs['datefilter']!=0){
					$limit = 0;
					if($inputs['datefilter']==1){
						$data['betcheckin']['fec1'] = $inputs['dpfec_1'];
						$data['betcheckin']['fec2'] = $inputs['dpfec_2'];
					}else{
						$data['cot_fecfrom'] = $inputs['dpfec_1'];
						$data['cot_fecto'] = $inputs['dpfec_2'];						
					}
				}else{
					$limit = 100;
				}
				if($inputs['ciudad']!=0){
					$data['id_ciudad'] = $inputs['ciudad'];
				}
				if($inputs['outsider']!=0){
					if($inputs['outsider']==1){
						$data['id_area'] == 2;
					}else{
						$data['id_area'] == 1;
					}
				}
				if($inputs['id_cotsearch']!=''){
					$data['id_cot'] = $inputs['id_cotsearch'];
				}
				if($inputs['correlativo']!=''){
					$data['clientcode'] = $inputs['correlativo'];
				}
				if($_SESSION['Usuario']->id_tipo == 1){
					if($inputs['agselect']!=0){
						$data['id_agencia'] = $inputs['agselect'];
					}
					if($inputs['cliselect']!=0){
						$data['idcliente'] = $inputs['cliselect'];
					}
				}else{
					$data['id_agencia'] = $_SESSION['idagencia'];
				}

				$rescot = $cotClass->GetBookings($db1,$data,$limit);
			}
			
			if($rescot->RecordCount()>0){
				$response['status'] = 'ok';
				while(!$rescot->EOF){
					$response['data'][$rescot->CurrentRow()]['buyername'] = ucfirst(strtolower($rescot->Fields('ope_namebuyer')));
					$response['data'][$rescot->CurrentRow()]['dates'] = substr($rescot->Fields('cot_fecdesde'), 0,10)." -> ".substr($rescot->Fields('cot_fechasta'), 0,10);
					$response['data'][$rescot->CurrentRow()]['firstpax'] = $rescot->Fields('cot_pripas')." ".$rescot->Fields('cot_pripas2');
					$response['data'][$rescot->CurrentRow()]['area'] = $rescot->Fields('id_area');
					$response['data'][$rescot->CurrentRow()]['ciudad'] = ucfirst(strtolower(trim($rescot->Fields('ciu_nombre'))));
					$response['data'][$rescot->CurrentRow()]['hotel'] = ucfirst(strtolower($rescot->Fields('hot_nombre')));
					$response['data'][$rescot->CurrentRow()]['correlativo'] = $rescot->Fields('cot_correlativo');
					if($rescot->Fields('cot_estado')==1){
						$response['data'][$rescot->CurrentRow()]['estado'] = ucfirst($lanClass->anulado);
					}elseif($rescot->Fields('cot_estado')==0){
						$response['data'][$rescot->CurrentRow()]['estado'] = ucfirst($lanClass->activo);
						if($rescot->Fields('cot_noshow')==0){
							$response['data'][$rescot->CurrentRow()]['estado'].=" - ".ucfirst($lanClass->noshow);
						}elseif($rescot->Fields('cot_outdate')==0){
							$response['data'][$rescot->CurrentRow()]['estado'].=" - ".ucfirst($lanClass->aplicacobro);
						}
					}

					if(isset($_SESSION['idexternalope'])){
						$response['data'][$rescot->CurrentRow()]['idcot'] = $rescot->Fields('id_cotbuyer');
						$response['data'][$rescot->CurrentRow()]['feconf'] = $rescot->Fields('fecconf_gand');
						$response['data'][$rescot->CurrentRow()]['cotvalor'] = $rescot->Fields('cot_valorbuyer');

					}else{
						$response['data'][$rescot->CurrentRow()]['idcot'] = $rescot->Fields('id_cot');
						$response['data'][$rescot->CurrentRow()]['feconf'] = $rescot->Fields('cot_fecconf');
						$response['data'][$rescot->CurrentRow()]['cotvalor'] = $rescot->Fields('cot_valor');
					}
					$rescot->MoveNext();
				}
			}else{
				$response['status'] = 'error';
			}
			die(json_encode($response));
			
		break;
		
		default:
			# code...
			break;
	}
}






include("superior.php");
?>
<html>
	<head>
		<title><?=$cotClass->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../otas/css/w3.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../otas/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
		<script src="../otas/js/jquery-3.2.1.min.js"></script>
		<script src="../otas/js/jquery-ui/jquery-ui.js"></script>
		<style>
			html, body {
			    height: 100%;
			    font-family: "Inconsolata", sans-serif;
			}
			input[type=date]::-webkit-inner-spin-button {
    			-webkit-appearance: none;
    			display: none;
			}
			.modal {
			    display:    none;
			    position:   fixed;
			    z-index:    1000;
			    top:        0;
			    left:       0;
			    height:     100%;
			    width:      100%;
			    background: rgba( 157, 248, 252, 0.3 ) 
			    url('http://i.stack.imgur.com/FhHRx.gif') 
			    50% 50% 
			    no-repeat;
			}
			body.loading {
			    overflow: hidden;   
			}
			body.loading .modal {
			    display: block;
			}
		</style>

		<script type="text/javascript">
			function buscaCots(){
				var inputs = {};
				$("#filters input[type!=radio]").each(function(){
					inputs[this.name] = this.value;
				});
				$("#filters select").each(function(){
					inputs[this.name] = this.value;
				});
				$("#filters input[type=radio]:checked").each(function(){
					inputs[this.name] = this.value;
				});
				inputs = JSON.stringify(inputs);

				$.ajax({
					type: 'POST',
					url: 'historial_reservas.php',
					dataType:'json',
					data:{
						flag: 'findcots',
						inputs},
					beforeSend:function(){
						$body = $("body");
						$body.addClass("loading");
					},
					complete:function(){
						$body.removeClass("loading");
					},
					success:function(result){
						$("#disptable").html(result);
						
						if(result.status=='ok'){
							
							$("#disptable").html("<tr class='w3-dark-grey'><td>Id</td><td><?=ucfirst($lanClass->correlativo)?></td><td>Hotel</td><td><?=ucfirst($lanClass->ciudad);?></td><td><?=ucfirst($lanClass->fechaconfirmacion)?></td><td><?=ucfirst($lanClass->pasajeroprincipal);?></td><td><?=ucfirst($lanClass->fechas);?></td><td><?=ucfirst($lanClass->comprador);?></td><td><?=ucfirst($lanClass->estado);?></td><td></td></tr>");
														
							var htmlrow = "";
							$.each(result.data, function (id_row, data){
								htmlrow="<tr class='w3-hover-grey'>";


								htmlrow+="<td>"+data.idcot+"</td>";
								htmlrow+="<td>"+data.correlativo+"</td>";
								htmlrow+="<td>"+data.hotel+"</td>";
								htmlrow+="<td>"+data.ciudad+"</td>";
								htmlrow+="<td>"+data.feconf+"</td>";
								htmlrow+="<td>"+data.firstpax+"</td>";
								htmlrow+="<td>"+data.dates+"</td>";
								htmlrow+="<td>"+data.buyername+"</td>";
								htmlrow+="<td>"+data.estado+"</td>";
								htmlrow+="";
								htmlrow+="<td><input type='button' value='Detalle' class='w3-button w3-black' onclick='submitealo(\""+data.idcot+"\")'/></td>";
								htmlrow+="";
								htmlrow+="</tr>";
								$("#disptable").append(htmlrow);
								$("#container").attr('style','height:100%');
							});
						}else{
							alert("<?=$lanClass->noresults?>");
						}
						
					},
					error:function(){
						alert("Error al traer los datos.");
					}
				});
				
}
function submitealo(cotid){
	$("[name=<?=((isset($_SESSION['idexternalope'])?'cotbuyer':'id_cot'))?>]").val(cotid);
	$("[name=listcotsform]").submit();
}



		</script>
	</head>
	<body> 
		<div id='container' class="w3-sand w3-grayscale w3-large" style='height:100%'>
			<div class='w3-content' style="max-width:700px">
				<table class='w3-table w3-bordered w3-small' id='filters'>
					<tr>
						<td colspan='2'><h5 class="w3-center"><span class="w3-tag w3-wide"><?=ucfirst($lanClass->reservasrealizadas);?></span></h5></td>
						<input type='hidden' name='debug' id='debug' value='0'>
					</tr>
					<tr>
						<td>ID Prog:<input type='text' name='id_cotsearch' id='id_cotsearch'></td>
						<td>Nº Correlativo: <input type='text' name='correlativo'></td>
					</tr>
					<tr>
						<td>
							<label><?=ucfirst($lanClass->fecha_desde);?></label>
							<input id='dpfec_1' name='dpfec_1' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d'); ?>"/>
						</td>
						<td>
							<label><?=ucfirst($lanClass->fecha_hasta);?></label>
							<input id='dpfec_2' name='dpfec_2' class="w3-input w3-border" type="date" required placeholder="Date" value="<?php echo date('Y-m-d'); ?>"/>
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<strong><?=ucfirst($lanClass->filtrarfechaspor);?>:</strong> 
							<input type='radio' name='datefilter' value='0' checked> <?=ucfirst($lanClass->nofiltrar);?>
							<input type='radio' name='datefilter' value='1'> Check In 
							<input type='radio' name='datefilter' value='2'> <?=ucfirst($lanClass->fechaconfirmacion);?>
						</td>
					</tr>
					<tr>
						<td>
							<label><?=ucfirst($lanClass->ciudad);?></label>
							<select name='ciudad' id='ciudad' class="w3-select"><option value='0'><?=ucfirst($lanClass->todas);?></option>
							<?
								$cities = $cotClass->getCities($db1, true,$cotClass->nationalCountryID);
								$arcities = array();
								while(!$cities->EOF){
									echo "<option value='".$cities->Fields('id_ciudad')."'>".utf8_encode($cities->Fields('ciu_nombre'))."</option>";
									$cities->MoveNext();
								}
							?>
							</select>
						</td>
						<td>
						<label><?=ucfirst($lanClass->origen_paxes);?></label>
							<select name='outsider' id='outsider' class="w3-select">
								<option value='0'><?=ucfirst($lanClass->todos);?></option>
								<option value='1'><?=ucfirst($lanClass->chileno);?></option>
								<option value='2'><?=ucfirst($lanClass->extranjero);?></option>
							</select>
						</td>
					</tr>

					<?
					
					if(isset($_SESSION['Usuario'])){
						if($_SESSION['Usuario']->id_tipo==1){
							$agencias = $ageClass-> getMeAgencies($db1,true);
							echo "<tr><td><label>".ucfirst($lanClass->agencia)."</label><br>";
							echo "<select name='agselect' id='agselect'><option value='0'>".ucfirst($lanClass->todas)."</option>";
							while(!$agencias->EOF){
								echo "<option value='".$agencias->Fields('id_agencia')."'>".ucfirst($agencias->Fields('ag_nombre'))."</option>";
								$agencias->MoveNext();
							}
							echo "</select></td>";

							echo "<td><label>".ucfirst($lanClass->proveedor)."</label><br>";
							echo "<select name='cliselect' id='cliselect'><option value='0'>".ucfirst($lanClass->todos)."</option>";
							$clientes = $cotClass->getClientes($db1);
							foreach ($clientes as $idcli => $nodos){
								echo "<option value='$idcli'>".$nodos['name']."</option>";
							}
							echo "</select></td>";
							echo "<tr>";
						}
					}
					?>
					<tr>
						<td></td>
						<td><button type="button" name='btnsrch' class='w3-button w3-black w3-right' onclick='buscaCots()'><?=$lanClass->buscar;?></button></td>
					</tr>
				</table>
				
			</div>
			<br>

			<div class='w3-content'>
				<form name='listcotsform' target='_blank' method='POST' action='detalle_opaca.php'>
					<input type='hidden' name='<?=((isset($_SESSION['idexternalope'])?'cotbuyer':'id_cot'))?>' value=''>
					<table id="disptable" class='w3-table w3-small'>

					</table>
				</form>
					
			</div>
		</div>
	<div class="modal"><!-- div para el loading --></div>

	</body>
</html>