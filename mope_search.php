<?php
//Connection statement
require_once('Connections/db1.php');
require_once('clases/agencia.php');

$agencia = new Agencia();
$permiso = 207;
require('secure.php');

if(isset($_POST['id_estado'])){
  $onlyActives=$_POST['id_estado'];
}else{
  $onlyActives = true;
}
if(isset($_POST['txt_nombre'])){
  $txtnombre=$_POST['txt_nombre'];
}else{
  $txtnombre = null;
}
if(isset($_POST['id_pais'])){
  $idpais = $_POST['id_pais'];
}else{
  $idpais = null;
}



if(isset($_POST['chgState']) && isset($_POST['id_agencia'])){

  $newState = $agencia->switchAgencyState($db1, $_POST['id_agencia']);
  $agencia->insAgencyLog($db1,$_SESSION['Usuario']->id_usuario, $_POST['id_agencia'], 'Actualiza estado agencia');
  if(is_numeric($newState)){
    $state[0] = $newState;
  }else{
    $state[0]='-1';
  }
  
  die(json_encode($state));
}


$listado = $agencia->getMeAgencies($db1, $onlyActives, $txtnombre, $idpais);
$pais = $agencia->getCountrys($db1);

?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?=$agencia->nombre_plataforma;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/test.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-3.2.1.min.js"></script>
    

</head>
<script type="text/javascript">
  function stateSwitcher(id_ag,btn){
    $.ajax({
      type: 'POST',
      url: 'mope_search.php',
      data: {
        chgState: 1,
        id_agencia: id_ag
      },
      success:function(result){

        arState = jQuery.parseJSON(result);
        if(arState[0]==-1){
          alert("Error al cambiar el estado de la agencia");
        }else{
          if(arState[0]==0){
            btn.className='btnEstados btnActivo';
            btn.value='Activa';
          }else{
            btn.className='btnEstados btnInactivo';
            btn.value='Inactiva';
          }
          
        }
      },
      error:function(){
        alert("Error al cargar webservices de la agencia");
      }
    });
    
  
  }



</script>
<body onLoad="document.form.txt_nombre.focus();">
  <table>
    <tr>
      <td class="titulo">Buscar Agencias</td>
    </tr>
  </table>
<table class='mainstream2' >
  <form id="form" name="form" method="post" action="">
    <tr>
      <td>
        <table id="filtros">
          <tr>
            <td>Nombre :</td>
            <td><input type="text" name="txt_nombre" value="<? if(isset($_POST['txt_nombre'])){ echo $_POST['txt_nombre'];} ?>"></td>
            <td>Activos :</td>
            <td>
              <select name="id_estado" id="id_estado">
                <option value="0" <? if(isset($_POST['id_estado'])){if($_POST['id_estado'] == '0'){?> selected <? }}?>>TODOS</option>
                <option value="1" <? if(isset($_POST['id_estado'])){if($_POST['id_estado'] == '1'){?> selected <? }}?>>SOLO ACTIVAS</option>
              </select>
            </td>
            <td></td>
            <td><td>
          </tr>
          <tr>
            <td>Pais :</td>
            <td>
              <select name="id_pais" id="id_pais">
                <option value="">-= TODOS =-</option>
                <?php
                while(!$pais->EOF){
                  ?>
                  <option value="<?php echo $pais->Fields('id_pais')?>" <?php 
                    if(isset($_POST['id_pais'])){
                      if ($pais->Fields('id_pais') == $_POST['id_pais']) {
                        echo "SELECTED";
                      }
                    } ?>
                  ><?php echo $pais->Fields('pai_nombre')?></option>
                  <?php
                  $pais->MoveNext();
                }
                $pais->MoveFirst();
                ?>
              </select>
            </td>
            <td></td>
            <td>
            </td>
            <td colspan="2">
              <button name="busca" type="submit" >Buscar</button>
              <button name="limpia" type="button" onClick="window.location='mope_search.php'">Limpiar</button>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </form>
</table>

<br>


<table class='listaBoni'  >
	<thead>
	  <tr>
      <th>N&ordm;</th>
      <th>ID</th>
      <th>Codigo</th>
  	  <th>Nombre</th>
  	  <th>Pais</th>
  	  <th>Markup</th>
      <th>Usuarios</th>
  	  <th>Estado</th>
  	  <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php
    while (!$listado->EOF) {
      ?>
  	  <tr title='N°<?php echo $listado->CurrentRow()+1;?>' >
    		<th><?php echo $listado->CurrentRow()+1;?></th>
        <td align="center"><?php echo $listado->Fields('id_agencia'); ?></td>
        <td><?php echo $listado->Fields('ag_code'); ?></td>
        <td><?php echo $listado->Fields('ag_nombre'); ?></td>
        <td><?php echo $listado->Fields('pai_nombre'); ?></td>
        <td><?php echo $listado->Fields('ag_markup'); ?></td>
        <td><?php echo $listado->Fields('cantusu');?></td>
        <td>
          <input type='button' onClick='stateSwitcher(<?=$listado->Fields('id_agencia');?>, this)' class=
            <?
            if($listado->Fields('ag_estado') == 0){
              echo "'btnEstados btnActivo' value='Activa'";
            }else{
              echo "'btnEstados btnInactivo' value='Inactiva'";
            }?>
          >
        <td align="center"><a href="mope_add.php?id_agencia=<?php echo $listado->Fields('id_agencia')?>"><img src="images/editar.png" border='0' alt="Editar Registro"></a>	</td>
      </tr>
      <?php 
  	  $listado->MoveNext(); 
  	}
    ?>
  </tbody>
</table>
<?php
$listado->Close();
?>
</body>
</html>