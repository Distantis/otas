<?php
require_once('Connections/db1.php');
require_once('clases/agencia.php');
$agencia = new Agencia();


$permiso = 206;


require_once('secure.php');
if(isset($_POST['inserta'])){

  if(isset($_POST['chkworked'])){ 
    $aux =  $_POST['chkworked'];
    unset($_POST['chkworked']);
  }else{
    $aux =  null;
  }
  $agencia->resetWsXagency($db1,$aux,$_POST['id_agencia']);
  
  die();
	KT_redir("mope_search.php");
}



if(isset($_POST['getWs'])){

  $html_content="<tr><th id='thtitulo' colspan='4'><div align='center'>Webservices de Agencia ";
  if($_POST['onlyActive']){
    $html_content.="(solo activos)";
    $filterbtn = "Cargar Todos";
  }else{
    $html_content.="(todos)";
    $filterbtn = "Solo Activos";
  }

  $html_content.="</div></th></tr>


  <tr>
    <th>Id Webservice</th>
    <th>Webservice &nbsp; <input type='button' value='$filterbtn' onClick='LoadWs(".!$_POST['onlyActive'].")'></th>
    <th>Fecha Implementación</th>
    <th>
      Autorizado&nbsp;<input type='button' id='btn_chkr' value='Marcar Todos' onClick='checkAll()'>
      <input type='hidden' id='switcher' value='1'>
      <input type='hidden' name='id_agencia' value='".$_POST['id_agencia']."'>
    </th>
  </tr>";
  $webservices = $agencia->getAllWsXagency($db1,$_POST['id_agencia'], $_POST['onlyActive']);
  while(!$webservices->EOF){
    $html_content.="<tr>";
    $html_content.="<th>".$webservices->Fields('id_webservice')."</th>";
    $html_content.="<td>".$webservices->Fields('ws_nombre')."</td>";
    $html_content.="<td>".$webservices->Fields('fecha_implementacion')."</td>";    
    $html_content.="<td><input type='checkbox' name='chkworked[]' id='chkworked[]' value='".$webservices->Fields('id_webservice')."'";
    if($webservices->Fields('ws_worked')==0){
      $html_content.=" checked";
    }
    $html_content.="></td></tr>";
    $webservices->MoveNext();
  }
  die($html_content);

}

?>

<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="css/test.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/MainJs.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
       LoadWs(false);
      });
    function checkAll(){
      if($("#switcher").val()==1){
        $("#btn_chkr").val("Desmarcar Todos");
        $("#targetTable input[type=checkbox]").prop("checked",true);
        $("#switcher").val(0);
      }else{
        $("#btn_chkr").val("Marcar Todos");
        $("#targetTable input[type=checkbox]").prop("checked",false);
        $("#switcher").val(1);
      }
    }

    function LoadWs(mode){
      $.ajax({
        type: 'POST',
        url: 'wsxmope.php',
        data: {
          getWs: 1,
          id_agencia: <?=$_GET['id_agencia'];?>,
          onlyActive: mode
        },
        success:function(result){
          $("#targetTable").html(result);
        },
        error:function(){
          alert("Error al cargar webservices de la agencia");
        }
      });
    }
    </script>
	</head>
	<body>
		
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
  			<table class='mainstream' id='targetTable'>
          
        </table>
	 		<center>
	 			<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
	    	<button name="buscar" type="button" onClick="window.location='mope_search.php'" style="width:100px; height:27px">Cancelar</button>&nbsp;
	 		</center>
		</form>
	</body>
</html>
