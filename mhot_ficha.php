<?php	 	 
require_once('Connections/db1.php');
require_once('clases/distantis.php');

$dis = new distantis();
// $dis->debug=true;
// $db1->debug=true; 
if(isset($_POST["inserta"]) && $_POST["cb_hotel"]!=0){
	$rs= $dis->getFichaHotel($db1, $_POST["cb_hotel"]);
	if($rs->RecordCount()==0){
		$dis->createFichaVacia($db1,$_POST["cb_hotel"]);
	}

	for($i=1; $i <=5 ; $i++){ 
		$varname = "imagen$i";
		if(isset($_FILES["img$i"]['name']) && $_FILES["img$i"]['name']!=""){
			if(strlen($rs->Fields("ficha_img$i"))>22){
				unlink($rs->Fields("ficha_img$i"));
			}
			$rutaDestino = "images/imgfichahotel/".$_POST['cb_hotel']."_".$_FILES["img$i"]['name'];
			
			move_uploaded_file($_FILES["img$i"]['tmp_name'], $rutaDestino);
			$$varname = "'".$_POST['cb_hotel']."_".$_FILES["img$i"]['name']."'";
		}else{
			$$varname="''";
		}
	}

	$data= array();
	$data['num_habs'] = $_POST['txt_habs'];
	$data['regimen'] = $_POST['cb_alimentacion'];
	$data['tipodesayuno'] = $_POST['cb_desayuno'];
	$data['serv_lavanderia'] = $_POST['rdo_lavanderia'];
	$data['inc_transfer'] = $_POST['rdo_transfer'];
	$data['estacionamiento'] = $_POST['rdo_estacionamiento'];
	$data['room_service'] = $_POST['rdo_room'];
	$data['business_center'] = $_POST['rdo_business'];
	$data['gym'] = $_POST['rdo_gimnasio'];
	$data['spa'] = $_POST['rdo_spa'];
	$data['restaurant'] = $_POST['rdo_restaurant'];
	$data['bar'] = $_POST['rdo_bar'];
	$data['tv_habs'] = $_POST['rdo_tv'];
	$data['caja_fuerte'] = $_POST['rdo_cajafuerte'];
	$data['habs_discapacitados'] = $_POST['rdo_discapacitados'];
	$data['tipowifi'] = $_POST['cb_wifi'];
	$data['check_in'] = "'".$_POST['cb_checkin']."'";
	$data['cod_latitud'] = "'".$_POST['txt_latitud']."'";
	$data['cod_longitud'] = "'".$_POST['txt_longitud']."'";
	$data['cod_mapa'] = "'".$_POST['txt_mapa']."'";
	$data['pag_web']= "'".$_POST['txt_web']."'";
	$data['observacion'] = "'".utf8_decode($_POST['txt_obs'])."'";
	$data['descripcion'] = "'".utf8_decode($_POST['txt_desc'])."'";
	$data['frigobar'] = $_POST['rdo_frigobar'];
	$data['permite_mascotas'] = $_POST['rdo_mascotas'];
	$data['check_out'] = "'".$_POST['cb_checkout']."'";
	$data['tipopiscina'] = $_POST['cb_piscina'];
	$data['ficha_img1'] = $imagen1;
	$data['ficha_img2'] = $imagen2;
	$data['ficha_img3'] = $imagen3;
	$data['ficha_img4'] = $imagen4;
	$data['ficha_img5'] = $imagen5;

	$dis->updateFichaHot($db1, $data, $_POST["cb_hotel"]);

}
if(isset($_GET["id_hotel"])){
	$hotel_rs = $dis->getFichaHotel($db1, $_GET["id_hotel"]);
	$datos["id_hotel"]=$hotel_rs->Fields("id_hotel");
	$datos["lavanderia"]=$hotel_rs->Fields("serv_lavanderia");
	$datos["transfer"]=$hotel_rs->Fields("inc_transfer");
	$datos["estacionamiento"]=$hotel_rs->Fields("estacionamiento");
	$datos["room_service"]=$hotel_rs->Fields("room_service");
	$datos["business_center"]=$hotel_rs->Fields("business_center");
	$datos["gym"]=$hotel_rs->Fields("gym");
	$datos["spa"]=$hotel_rs->Fields("spa");
	$datos["restaurant"]=$hotel_rs->Fields("restaurant");
	$datos["bar"]=$hotel_rs->Fields("bar");
	$datos["tv_habs"]=$hotel_rs->Fields("tv_habs");
	$datos["frigobar"]=$hotel_rs->Fields("frigobar");
	$datos["caja_fuerte"]=$hotel_rs->Fields("caja_fuerte");
	$datos["habs_discapacitados"]=$hotel_rs->Fields("habs_discapacitados");
	$datos["mascotas"]=$hotel_rs->Fields("permite_mascotas");
	$datos["num_habs"]=$hotel_rs->Fields("num_habs");
	$datos["regimen"]=$hotel_rs->Fields("regimen");
	$datos["tipodesayuno"]=$hotel_rs->Fields("tipodesayuno");
	$datos["wifi"]=$hotel_rs->Fields("tipowifi");
	$datos["check_in"]=$hotel_rs->Fields("check_in");
	$datos["check_out"]=$hotel_rs->Fields("check_out");
	$datos["piscina"]=$hotel_rs->Fields("tipopiscina");
	$datos["latitud"]=$hotel_rs->Fields("cod_latitud");
	$datos["longitud"]=$hotel_rs->Fields("cod_longitud");
	$datos["cod_mapa"]=$hotel_rs->Fields("cod_mapa");
	$datos["pag_web"]=$hotel_rs->Fields("pag_web");
	$datos["observacion"]=$hotel_rs->Fields("observacion");
	$datos["descripcion"]=$hotel_rs->Fields("descripcion");
	$datos["img1"]=$hotel_rs->Fields("ficha_img1");
	$datos["img2"]=$hotel_rs->Fields("ficha_img2");
	$datos["img3"]=$hotel_rs->Fields("ficha_img3");
	$datos["img4"]=$hotel_rs->Fields("ficha_img4");
	$datos["img5"]=$hotel_rs->Fields("ficha_img5");
}
//weas del load()

$hoteles = $dis->getHotFichas($db1);
$cb_hotel ="<select id='cb_hotel' name='cb_hotel' onChange=traeFicha(this.value)>
	<option value=0 >Seleccione</option>";
while(!$hoteles->EOF){
	if($hoteles->Fields("id_hotficha")==-1){
		//no tiene ficha
		$color="#FF5100";
		$tiene_ficha=1;
	}else{
		//tiene ficha
		$color="#54B10F";
		$tiene_ficha=0;
	}
	$id_hotel = $hoteles->Fields('id_hotel');
	$hotel = $hoteles->Fields('hot_nombre');
	$selected="";
	if(isset($_GET['id_hotel'])){
		if($id_hotel==$_GET["id_hotel"]){
			$selected = "selected";
		}
	}
	$cb_hotel.= "<option value='$id_hotel' class=$tiene_ficha style='color:$color' $selected>$hotel</option>";
	$array_hotel[$id_hotel][$tiene_ficha]=$hotel;
	$hoteles->MoveNext();
}
$cb_hotel.="</select>";

$cb_checkin ="<select name='cb_checkin' id='cb_checkin'>";
$cb_checkout ="<select name='cb_checkout' id='cb_checkout'>";
for($i=00;$i<=23;$i++){
	$selected1="";
	$selected2="";
	if(isset($datos)){
		$in_aux = explode(":", $datos["check_in"]);
		$out_aux = explode(":", $datos["check_out"]);
		if($i==$in_aux[0]){
			$selected1="selected";
		}
		if($i==$out_aux[0]){
			$selected2="selected";
		}
	}
	$cb_checkin.="<option value=$i $selected1>$i:00 hrs</option>";
	$cb_checkout.="<option value=$i $selected2>$i:00 hrs</option>";
}
$cb_checkin.="</select>";
$cb_checkout.="</select>";

$regimenes = $dis->GetMealPlan($db1);
$desayunos = $dis->getBreakfasts($db1);
$piscinas = $dis->getPoolTypes($db1);
$wifis = $dis->getWifiTypes($db1);


?>

<html>
<head>
<title><?=$dis->nombre_plataforma;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/test.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">


function traeFicha(id_hotel){
	if(id_hotel == 0){
		window.location.href = "mhot_ficha.php";
		return;
	}

	window.location.href = "mhot_ficha.php?id_hotel="+id_hotel;
}
</script>
</head>
<body>
	<form method="post" id="form" name="form" action="mhot_ficha.php?id_hotel=<?php if(isset($_GET["id_hotel"])){echo $_GET["id_hotel"]; }else{ echo 0;}?>" enctype="multipart/form-data" >
	<table class='mainstream'>
		<th colspan="6" id="thtitulo"><div align="center">Ficha técnica hotel</div></th>
		<tr valign="baseline">		
			<th>Hotel :</th>
			<td colspan='3'><?php echo $cb_hotel;?></td>		
			
		</tr>
		<tr valign="baseline">
			<th>Servicio de lavandería :</th>
			<td>
				Si<input type="radio" name="rdo_lavanderia" value="1" <?if(isset($datos)){if($datos["lavanderia"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_lavanderia" value="0" <?if(isset($datos)){if($datos["lavanderia"]==0)echo "checked";}?> />
			</td>
			<th>Transfer aeropuerto/hotel :</th>
			<td>
				Si<input type="radio" name="rdo_transfer" value="1" <?if(isset($datos)){if($datos["transfer"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_transfer" value="0" <?if(isset($datos)){if($datos["transfer"]==0)echo "checked";}?> />
			</td>
			<th>N° de habitaciones  :</th>
			<td><input type="number" name="txt_habs" id="txt_habs" value="<?if(isset($datos)){echo $datos['num_habs'];}?>" /></td>
		</tr>
		<tr valign="baseline">
			<th>Estacionamiento  :</th>
			<td>
				Si<input type="radio" name="rdo_estacionamiento" value="1" <?if(isset($datos)){if($datos["estacionamiento"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_estacionamiento" value="0" <?if(isset($datos)){if($datos["estacionamiento"]==0)echo "checked";}?> />
			</td>
			<th>Room service :</th>
			<td>
				Si<input type="radio" name="rdo_room" value="1" <?if(isset($datos)){if($datos["room_service"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_room" value="0" <?if(isset($datos)){if($datos["room_service"]==0)echo "checked";}?> />
			</td>
			<th>Business Center :</th>
			<td>
				Si<input type="radio" name="rdo_business" value="1" <?if(isset($datos)){if($datos["business_center"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_business" value="0" <?if(isset($datos)){if($datos["business_center"]==0)echo "checked";}?> />
			</td>
		</tr>
		<tr valign="baseline">
			<th>Gimnasio :</th>
			<td>
				Si<input type="radio" name="rdo_gimnasio" value="1" <?if(isset($datos)){if($datos["gym"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_gimnasio" value="0" <?if(isset($datos)){if($datos["gym"]==0)echo "checked";}?> />
			</td>
			<th>Spa :</th>
			<td>
				Si<input type="radio" name="rdo_spa" value="1" <?if(isset($datos)){if($datos["spa"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_spa" value="0" <?if(isset($datos)){if($datos["spa"]==0)echo "checked";}?> />
			</td>
			<th>Restaurant :</th>
			<td>
				Si<input type="radio" name="rdo_restaurant" value="1" <?if(isset($datos)){if($datos["restaurant"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_restaurant" value="0" <?if(isset($datos)){if($datos["restaurant"]==0)echo "checked";}?> />
			</td>
		</tr>
		<tr valign="baseline">
			<th>Bar  :</th>
			<td>
				Si<input type="radio" name="rdo_bar" value="1" <?if(isset($datos)){if($datos["bar"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_bar" value="0" <?if(isset($datos)){if($datos["bar"]==0)echo "checked";}?> />
			</td>
			<th>TV en habitaciones :</th>
			<td>
				Si<input type="radio" name="rdo_tv" value="1" <?if(isset($datos)){if($datos["tv_habs"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_tv" value="0" <?if(isset($datos)){if($datos["tv_habs"]==0)echo "checked";}?> />
			</td>
			<th>Frigobar :</th>
			<td>
				Si<input type="radio" name="rdo_frigobar" value="1" <?if(isset($datos)){if($datos["frigobar"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_frigobar" value="0" <?if(isset($datos)){if($datos["frigobar"]==0)echo "checked";}?> />
			</td>
		</tr>
		<tr valign="baseline">
			<th>Caja fuerte :</th>
			<td>
				Si<input type="radio" name="rdo_cajafuerte" value="1" <?if(isset($datos)){if($datos["caja_fuerte"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_cajafuerte" value="0" <?if(isset($datos)){if($datos["caja_fuerte"]==0)echo "checked";}?> />
			</td>
			<th>Habitaciones para discapacitados :</th>
			<td>
				Si<input type="radio" name="rdo_discapacitados" value="1" <?if(isset($datos)){if($datos["habs_discapacitados"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_discapacitados" value="0" <?if(isset($datos)){if($datos["habs_discapacitados"]==0)echo "checked";}?> />
			</td>
			<th>Permite mascotas :</th>
			<td>
				Si<input type="radio" name="rdo_mascotas" value="1" <?if(isset($datos)){if($datos["mascotas"]==1)echo "checked";}?> />
				No<input type="radio" name="rdo_mascotas" value="0" <?if(isset($datos)){if($datos["mascotas"]==0)echo "checked";}?> />
			</td>
		</tr>
		<tr valign="baseline">
			<th>Piscina :</th>
			<td>
				<select name="cb_piscina" id="cb_piscina">
					<?
					while(!$piscinas->EOF){
						echo "<option value ='".$piscinas->Fields('id_tipopiscina')."'";
						if(isset($datos)){
							if($piscinas->Fields('id_tipopiscina')==$datos['piscina']){
								echo "SELECTED";
							}
						}
						echo ">".$piscinas->Fields('desc_tpisc')."</option>";
						$piscinas->MoveNext();
					}
					?>
				</select>
			</td>

			<th>Régimen alimentación :</th>
			<td>
				<select name="cb_alimentacion" id="cb_alimentacion">
				<?
				while(!$regimenes->EOF){
					echo "<option value ='".$regimenes->Fields('id_regimen')."'";
					if(isset($datos)){
						if($regimenes->Fields('id_regimen')==$datos['regimen']){
							echo "SELECTED";
						}
					}
					echo ">".$regimenes->Fields('nom_regimen')."</option>";
					$regimenes->MoveNext();
				}
				?>
				</select>
			</td>
			<th>Desayuno :</th>
			<td>
				<select name="cb_desayuno" id="cb_desayuno">
				<?
				while(!$desayunos->EOF){
					echo "<option value ='".$desayunos->Fields('id_tipodesayuno')."'";
					if(isset($datos)){
						if($desayunos->Fields('id_tipodesayuno')==$datos['tipodesayuno']){
							echo "SELECTED";
						}
					}
					echo ">".$desayunos->Fields('desc_tipodes')."</option>";
					$desayunos->MoveNext();
				}
				?>

				</select>
			</td>
		</tr>
		<tr>
			<th>Wi-Fi Free:</th>
			<td>
				<select name="cb_wifi" id="cb_wifi">
					<?
					while(!$wifis->EOF){
						echo "<option value ='".$wifis->Fields('id_tipowifi')."'";
						if(isset($datos)){
							if($wifis->Fields('id_tipowifi')==$datos['wifi']){
								echo "SELECTED";
							}
						}
						echo ">".$wifis->Fields('desc_twifi')."</option>";
						$wifis->MoveNext();
					}
					?>
				</select>
			</td>
			<th>Check in :</th>
			<td><?php echo $cb_checkin; ?></td>
			<th>Check out :</th>
			<td><?php echo $cb_checkout; ?></td>
		</tr>

		<tr id="tr_cant" valign="baseline">
			<th>Latitud :</th>
			<td><input type="text" name="txt_latitud" id="txt_latitud" value="<?if(isset($datos)){echo $datos["latitud"];}?>" /></td>
			<th>Longitud :</th>
			<td><input type="text" name="txt_longitud" id="txt_longitud" value="<?if(isset($datos)){echo $datos["longitud"];}?>" /></td>
		</tr>
		<tr valign="baseline">
			<th colspan="3">Código mapa  :</th>
			<th colspan="3">Página web  :</th>
		</tr>
		<tr>
			<td colspan="3"><input type="text" name="txt_mapa" id="txt_mapa" style="width:500" value='<?if(isset($datos)){echo $datos["cod_mapa"];}?>' /></td>
			<td colspan="3"><input type="text" name="txt_web" id="txt_web" style="width:380" value="<?if(isset($datos)){echo $datos["pag_web"];}?>" /><font size="1">&nbsp;ej: www.ejemplo.cl</font></td>
		</tr>
		<tr valign="baseline">
			<th colspan="3">Observacion :</th>
			<th colspan="3">Descripcion del hotel:</th>
		</tr>
		<tr>
			<td colspan="3"><textarea rows="3" cols="60"  name="txt_obs" id="txt_obs"><? if(isset($datos)){echo utf8_encode($datos["observacion"]);}?></textarea></td>
			<td colspan="3"><textarea rows="3" cols="60"  name="txt_desc" id="txt_desc"><? if(isset($datos)){echo utf8_encode($datos["descripcion"]);}?></textarea></td>
		</tr>
		<tr valign="baseline">
			
			<th colspan="6" id="thtitulo"><div align="center">FOTOS DEL HOTEL </div></th>
		</tr>
		<tr>
			<td align="center" colspan="2">
				FOTO 1</br>
				<?php if(isset($datos['img1']) && trim($datos['img1']) != ""){ ?>
					<img src="/<?=$datos['img1']?>" alt="" width="150" height="100"/>
				<?php } ?>
				<input type="file" name="img1" id="img1"></br>
				<font size="1">* Nombre de imágen sin espacios ni tildes.</font>
			</td>
			<td align="center" colspan="2">
				FOTO 2</br>
				<?php if(isset($datos['img2']) && trim($datos['img2']) != ""){ ?>
					<img src="/<?=$datos['img2']?>" alt="" width="150" height="100"/>
				<?php } ?>
				<input type="file" name="img2" id="img2"></br>
				<font size="1">* Nombre de imágen sin espacios ni tildes.</font>
			</td>
			<td align="center" colspan="2">
				FOTO 3</br>
				<?php if(isset($datos['img3']) && trim($datos['img3']) != ""){ ?>
					<img src="/<?=$datos['img3']?>" alt="" width="150" height="100"/>
				<?php } ?>
				<input type="file" name="img3" id="img3"></br>
				<font size="1">* Nombre de imágen sin espacios ni tildes.</font>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				FOTO 4</br>
				<?php if(isset($datos['img4']) && trim($datos['img4']) != ""){ ?>
					<img src="/<?=$datos['img4']?>" alt="" width="150" height="100"/>
				<?php } ?>
				<input type="file" name="img4" id="img4"></br>
				<font size="1">* Nombre de imágen sin espacios ni tildes.</font>
			</td>
			<td align="center" colspan="2">
				FOTO 5</br>
				<?php if(isset($datos['img5']) && trim($datos['img5']) != ""){ ?>
					<img src="/<?=$datos['img5']?>" alt="" width="150" height="100"/>
				<?php } ?>
				<input type="file" name="img5" id="img5"></br>
				<font size="1">* Nombre de imágen sin espacios ni tildes.</font>
			</td>
		<tr>
		<tr valign="baseline">
		  <td colspan="2" align="right" nowrap>&nbsp;</td>
		</tr>
	</table>
	<br>
	<center>
		<button name="inserta" type="submit" style="width:100px; height:27px">Guardar</button>
		<button name="cancela" type="button" style="width:100px; height:27px" onClick="window.location.href='mhot_ficha.php';">Cancelar</button>
	</center>
	</form>
</body>
</html>