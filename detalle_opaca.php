<?php
include("Connections/db1.php");
include("/var/www/otas/clases/cotizacion.php");
include("/var/www/otas/clases/language.php");
include("/var/www/otas/clases/usuario.php");
$cotClass = new Cotizacion();
$userClass = new Usuario();
$lanClass = new Language();
include('/var/www/otas/secure_opaco.php');

if(isset($_SESSION['id_cliente'])){
	//cot comprador
	if(!isset($cliente)){
		$cliente = $cotClass->getMeThatClient($db1, $_SESSION['id_cliente']);
	}

	$cotres = $cotClass->getCotBuyer($db1,$_POST['cotbuyer'],$_SESSION['idagencia'],$cliente);
	$destinos = $cotClass->getDestinations($db1,$_POST['cotbuyer'],true,$_SESSION['idagencia'],$cliente);
	$idcotg = $cotres->Fields('id_cotgand');
}else{
	
	//aqui va la mierda que pregunta por la info de la cot si es directa de gandhi
	if($_SESSION['Usuario']->id_tipo!=1){
		$data['id_agencia'] = $_SESSION['Usuario']->id_agencia;
	}
	$data['id_cot'] = $_POST['id_cot'];
	$cotres = $cotClass->GetBookings($db1,$data);
	$destinos = $cotClass->getDestinations($db1,$_POST['id_cot']);
	$idcotg = $cotres->Fields('id_cot');
}
if($cotres->Fields('id_area')==2){
	$moname= "CLP$";
	$decimales = 0;
}else{ 
	$moname= "USD$";
	$decimales = $cotClass->decimales;
}
include("superior.php");
?>
<html>
	<head>
		<title><?=$cotClass->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../otas/css/w3.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../otas/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
		<script src="../otas/js/jquery-3.2.1.min.js"></script>
		<script src="../otas/js/jquery-ui/jquery-ui.js"></script>
		<style>
			html, body {
			    height: 100%;
			    font-family: "Inconsolata", sans-serif;
			}
			input[type=date]::-webkit-inner-spin-button {
    			-webkit-appearance: none;
    			display: none;
			}
		</style>
		<script type="text/javascript">
			function anular(){
				var rusure = confirm('<?=$lanClass->esta_seguro?>');
				if(rusure){
					$("#nulform").submit();
				}
			}

	</script>
	</head>
	<body>
		<div id='container' class="w3-sand w3-grayscale w3-large" style='height:100%'>
			<div class='w3-content' style="max-width:50%">
				<h5 class="w3-center">
					<span class="w3-tag w3-wide">
					<?
						if($cotres->Fields('cot_estado')==0){
							echo strtoupper($lanClass->programa_confirmado);
						}else{
							echo strtoupper($lanClass->programa_anulado);
						}
						echo " N°".((isset($_SESSION['id_cliente']))?$_POST['cotbuyer']:$_POST['id_cot']);
					?>
					</span>
				</h5>
				<table class='w3-table w3-small'>
					<form id='nulform' name='nulform' method='POST' action='cancel_opaca.php'>
						<input type='hidden' name='idcotg' value='<?=$idcotg;?>'>
						<?
						if(isset($_SESSION['id_cliente'])){
							echo "<input type='hidden' name='id_cliente' value='".$_SESSION['id_cliente']."'>";
						}
						?>
					</form>
					<tr>
						<th colspan='4'><span class="w3-tag"><?=ucfirst($lanClass->operador);?></span></th>
					</tr>
					<tr>
						<th><?=ucfirst($lanClass->correlativo);?>:</th>
						<td><?=$cotres->Fields('cot_correlativo');?></td>
						<th>Full credit:</th>
						<td><?=($cotres->Fields('cot_fullcredit')==1)?ucfirst($lanClass->si):'No';?></td>
					</tr>
					<tr>
						<th><?=ucfirst($lanClass->operador);?>:</th>
						<td><?=ucfirst(strtolower($cotres->Fields('ope_namebuyer')));?></td>
						<th><?=ucfirst($lanClass->fecanula);?>:</th>
						<td><?
							if($cotres->Fields('cot_refundable')==0){
								echo substr($cotres->Fields('ha_hotanula'), 0, 10);
							}else{
								echo "<strong>".ucfirst($lanClass->notrefundable)."</strong>";
							}

							?>
						</td>
					</tr>
					<tr>
						<th><?=ucfirst($lanClass->subagencia);?>:</th>
						<td><?=$cotres->Fields('sa_nombre');?></td>
					</tr>
					<tr>
						<th><?=ucfirst($lanClass->preciovalor);?>:</th>
						<td>
						<?
							echo $moname." ";
							if(isset($_SESSION['id_cliente'])){
								echo round($cotres->Fields('cot_valorbuyer'),$decimales);
							}else{
								echo round($cotres->Fields('cot_valor'),$decimales);
							}
						?>
						</td>
						<?
						$ventacot = (isset($_SESSION['id_cliente']))?$cotres->Fields('cot_ventabuyer'):$cotres->Fields('cot_venta');
							if($cotres->Fields('id_area')==2 && $ventacot!=0){
								echo "<th>".ucfirst($lanClass->precioventa).":</th>
								<td> $moname ".round($ventacot,$decimales)."</td>";
							}else{
								echo "<td></td><td></td>";
							}
						?>
					</tr>
				</table>
				<?
					while(!$destinos->EOF){
						$htmlres = "<table class='w3-table w3-small'>";
						$htmlres.="<tr><th colspan='4'><span class='w3-tag'>".ucfirst($lanClass->resumen_destino)." ".($destinos->CurrentRow()+1)." - ".$destinos->Fields('ciu_nombre')."</span></th></tr>";
						$htmlres.="
							<tr>
								<th>Hotel:</th>
								<td>".$destinos->Fields('hot_nombre')."</td>
								<th>".ucfirst($lanClass->direccion).":</th>
								<td>".$destinos->Fields('hot_direccion')."</td>
							</tr>
							<tr>
								<th>".ucfirst($lanClass->fecha_desde).":</th>
								<td>".$destinos->Fields('cd_fecdesde')."</td>
								<th>".ucfirst($lanClass->fecha_hasta).":</th>
								<td>".$destinos->Fields('cd_fechasta')."</td>
							</tr>
							<tr>
								<th>".ucfirst($lanClass->preciovalordestino)."</th>
								<td>$moname ";
						if(isset($_SESSION['id_cliente'])){
							$htmlres.= round($destinos->Fields('cd_valorbuyer'),$decimales);
						}else{
							$htmlres.= round($destinos->Fields('cd_valorgand'),$decimales);
						}

						$htmlres.="</td>";


						$ventacd = (isset($_SESSION['id_cliente']))?$destinos->Fields('cd_ventabuyer'):$destinos->Fields('cd_ventagand');
						if($cotres->Fields('id_area')==2 && $ventacd!=0){
							$htmlres.= "<th>".ucfirst($lanClass->precioventadestino).":</th>
							<td>$moname ".round($ventacd,$decimales)."</td>";
						}else{
							$htmlres.=  "<td></td><td></td>";
						}
						$htmlres.="</tr>
							<tr>
								<th colspan='4'><span class='w3-tag'>".ucfirst($lanClass->tipohabitacion).":</span>  ".$destinos->Fields('th_nombre')."</th>
							</tr>
							<tr>
								<td><strong>Single:</strong> ".$destinos->Fields('cd_hab1')."</td>
								<td><strong>Doble twin:</strong> ".$destinos->Fields('cd_hab2')."</td>
								<td><strong>Doble Matrimonial:</strong> ".$destinos->Fields('cd_hab3')."</td>
								<td><strong>Triple:</strong> ".$destinos->Fields('cd_hab4')."</td>
							</tr>
							<tr>
								<th colspan='4'><span class='w3-tag'>".ucfirst($lanClass->detallepax).":</span></th>
							</tr>";

							$paxes = $cotClass->getPaxes($db1, $destinos->Fields('id_cotdes'));
							while(!$paxes->EOF){
								$htmlres.="<tr><th>-".ucfirst($lanClass->pasajero)." ".($paxes->CurrentRow()+1).":</th>
									<td colspan='3'>".$paxes->Fields('cp_nombres')." ".$paxes->Fields('cp_apellidos').", ".$paxes->Fields('pai_nombre')."</td></tr>";
								$paxes->MoveNext();
							}
						if($cotres->Fields('cot_estado')==0 && $cotres->Fields('cot_noshow')!=0 && $cotres->Fields('cot_outdate')!=0 && $cotres->Fields('cot_refundable')==0){
							$htmlres.="<tr><td colspan='4'><input type='button' value='".ucfirst($lanClass->anular)."' class='w3-button w3-black w3-right' onclick='anular()'></td></tr>";
						} 
						$htmlres.="</table>";
						echo $htmlres;
						$destinos->MoveNext();
					}
				?>
			<p class="w3-small">
				<strong><?=ucfirst($lanClass->observaciones);?>:</strong> <?=$cotres->Fields('cot_obs');?>
			</p>
			<br>
			<p class="w3-text-grey w3-small">
				· <?=ucfirst(str_replace("__OPENAME__", (isset($cliente)?ucfirst($cliente['name']):"Distantis"), $lanClass->glosa1));?>
			</p>
			<p class="w3-text-grey w3-small">
				· <?=ucfirst($lanClass->glosa2);?>
			</p>


			</div>
		</div>
		<div class="modal"><!-- div para el loading --></div>
	</body>
</html>