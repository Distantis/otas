<?php
require_once('Connections/db1.php');
require_once('clases/agencia.php');
$agencia = new Agencia();
$clientes = $agencia->getClientes($db1);


$permiso = 205;


require_once('secure.php');
if(isset($_POST['inserta'])){
  foreach ($clientes as $id_cliente=>$shitty){
    if(isset($_POST['chkworked'.$id_cliente."_"])){
      $aux = $_POST['chkworked'.$id_cliente."_"];
    }else{
      $aux = null;
    }
    $agencia->resetHotsXagency($db1,$aux,$_POST['id_agencia'], $id_cliente);
    $agencia->insAgencyLog($db1,$_SESSION['Usuario']->id_usuario, $_POST['id_agencia'], 'Actualiza hoteles por agencia');
  }
	KT_redir("mope_search.php");
}



if(isset($_POST['getHots'])){
  //$db1->debug=true;
  $hoteles = $agencia->getAllHotsXagency($db1,$_POST['id_agencia'], $_POST['onlyActive'], null, true);
  $arHots = array();
  while(!$hoteles->EOF){
    $arHots[$hoteles->Fields('id_hotel')]['clientes'][$hoteles->Fields('id_cliente')] = $hoteles->Fields('hot_worked');
    if(!isset($arHots[$hoteles->Fields('id_hotel')]['nombre'])){
      $arHots[$hoteles->Fields('id_hotel')]['nombre'] = $hoteles->Fields('hot_nombre');
    }
    $hoteles->MoveNext();
  }



    
  $html_content="<thead><tr><th id='thtitulo' colspan='".(2+count($clientes))."'><div align='center'>Hoteles Autorizados <input type='hidden' name='id_agencia' value='".$_POST['id_agencia']."'>";
  if($_POST['onlyActive']){
    $html_content.="(solo activos)";
    $filterbtn = "Cargar Todos";
  }else{
    $html_content.="(todos)";
    $filterbtn = "Solo Activos";
  }

  $html_content.="</div></th></tr>
  <tr>
    <th>Id Hotel</th>
    <th>
      Hotel &nbsp; <input type='button' value='$filterbtn' onClick='loadHots(".!$_POST['onlyActive'].")'>
      <input type='text' id='buscadorhot'>
    </th>";

    foreach($clientes as $id_cliente => $cliente){
      if($cliente['estado']==0 && $cliente['tghandi']==0){
        $html_content.="<th>".$cliente['name']."<input type='button' id='btn_chkr".$id_cliente."' value='Marcar Todos' onClick='checkAll(".$id_cliente.")'><input type='hidden' id='switcher".$id_cliente."' value='1'></th>";
      }
    }
    $html_content.="</tr></thead><tbody>";

    foreach ($arHots as $id_hotel => $nodos){
      $html_content.="<tr><td>".$id_hotel."</td><td>".utf8_decode($nodos['nombre'])."</td>";
      foreach($clientes as $id_cliente => $cliente){
        if($cliente['estado']==0 && $cliente['tghandi']==0){
          $html_content.="<td><input type='checkbox' name='chkworked".$id_cliente."_[]' id='chkworked".$id_cliente."_[]' value='".$id_hotel."'";
          if($nodos['clientes'][$id_cliente]==0){
            $html_content.=" checked";
          }
          $html_content.="></td>";
        }
      }
      $html_content.="</tr>";
    }
    $html_content.="</tbody>";
    die($html_content);

}

?>

<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="css/test.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/MainJs.js"></script>
    <style>
      .modal {
          display:    none;
          position:   fixed;
          z-index:    1000;
          top:        0;
          left:       0;
          height:     100%;
          width:      100%;
          background: rgba( 157, 248, 252, 0.3 ) 
          url('http://i.stack.imgur.com/FhHRx.gif') 
          50% 50% 
          no-repeat;
      }
      body.loading {
          overflow: hidden;   
      }
      body.loading .modal {
          display: block;
      }
    </style>

    <script type="text/javascript">
      $(document).ready(function(){
       loadHots(false);
      });


    //buscadorhot
   

    function checkAll(idcli){
      if($("#switcher"+idcli).val()==1){
        $("#btn_chkr"+idcli).val("Desmarcar Todos");
        $("#targetTable input[type=checkbox][name*=chkworked"+idcli+"_] ").prop("checked",true);
        $("#switcher"+idcli).val(0);
      }else{
        $("#btn_chkr"+idcli).val("Marcar Todos");
        $("#targetTable input[type=checkbox][name*=chkworked"+idcli+"_]").prop("checked",false);
        $("#switcher"+idcli).val(1);
      }
    }

    function loadHots(mode){
      $.ajax({
        type: 'POST',
        url: 'hotxmope.php',
        data: {
          getHots: 1,
          id_agencia: <?=$_GET['id_agencia'];?>,
          onlyActive: mode
        },
        beforeSend:function(){
          $body = $("body");
          $body.addClass("loading");
        },
        complete:function(){
          $body.removeClass("loading");
        },
        success:function(result){
          $("#targetTable").html(result);
          $("#buscadorhot").on("keyup", function() {
            var value = $(this).val();
            $("table tbody tr").each(function(index) {
                
              $row = $(this);
              var $tdElement = $row.find("td:nth-child(2)");
              var id = $tdElement.text().toLowerCase();
              var matchedIndex = id.indexOf(value);
              if (matchedIndex != 0) {
                  $row.hide();
              }else{
                $row.show();
              }
            
            });
          });
        },
        error:function(){
          alert("Error al cargar hoteles autorizados de la agencia");
        }
      });
    }
    </script>
		
	</head>
	<body>
		
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
  			<table class='ListaBoni' id='targetTable'>
          
        </table>
	 		<center>
	 			<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
	    	<button name="buscar" type="button" onClick="window.location='mope_search.php'" style="width:100px; height:27px">Cancelar</button>&nbsp;
	 		</center>
		</form>
    <div class="modal"><!-- div para el loading --></div>
	</body>
</html>
