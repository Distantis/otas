<?
require_once('clases/usuario.php');
require_once('Connections/db1.php');
$usuario = $_SESSION['Usuario'];
//$permiso = 402;
require('secure.php');

$clientes = $usuario->getClientes($db1);
$texto=array();



if(isset($_POST["guardar"])){
	foreach($clientes as $cliente){
		$usuario->resetTipoTarCliente($db1,$cliente['bd'],$_POST["id_tipotar"]);
	}
	foreach ($_POST['seleccionados'] as $dbCli => $ttars){
		foreach($ttars as $ttCli){
			$respuesta = $usuario->UpdTipoTarClienteEsp($db1,$dbCli,$_POST["id_tipotar"],$ttCli);
			
		}
	}
	die($respuesta);
}

if(isset($_POST["add"])){
	$datos = $usuario->GetTipoTarCliente($db1,$_POST["cliente1"],false,$_POST["seleccionado"]);
	if($datos->Fields('tt_estado')==0){
		$color = "style='background-color: #80bf83'";
	}else{
		$color = "style='background-color: #fd9f98'";
	}
	die("<option ondblclick=remove('".$_POST["cliente1"]."') value='".$_POST["seleccionado"]."' $color>".utf8_encode($datos->Fields('tt_nombre'))."</option>");
}

?>

<html>
	<head>
		<title><?=$agencia->nombre_plataforma;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link href="css/test.css" rel="stylesheet" type="text/css" />
    	<script src="js/jquery-3.2.1.min.js"></script>
    	<script type="text/javascript">
			function add_this(seleccionado,cliente){
				$.ajax({
					type: 'POST',
					url: 'tipotar_mod.php',
					data: {
						add: true,
						seleccionado: seleccionado,
						cliente1: cliente
					},
					success:function(result){
						var existe = false;
						$("#carga_"+cliente+" option").each(function(){
							if($(this).val()==seleccionado){
								existe = true;
							}
						});
						if(!existe){
							$("#carga_"+cliente).append(result);
						}else{
							alert("El tipo de tarifa ya esta asignado");
						}
					},
					error:function(){
						alert("Error al actualizar los datos");
					}
				});
			}
			
			function remove(cliente){
				$("#carga_"+cliente+" option:selected").remove();
			}
		
			function guardar(){
				var arCli = {};
				$("select[name*=carga_]").each(function(){
					var nameCli = $(this).prop('name').replace("carga_","");
					arCli[nameCli] = Array();
					$("#"+this.name+" option").each(function(){
						arCli[nameCli].push(this.value);
					});
				});
				$.ajax({
					type: 'POST',
					url: 'tipotar_mod.php',
					data: {
						guardar: "true",
						seleccionados: arCli,
						id_tipotar: <?=$_GET['id']?>
					},
					success:function(result){
						alert("Agregados Correctamente");
					},
					error:function(){
						alert("Error al actualizar los datos");
					}
				});
			}

			
		</script>
	</head>
	<body>
		<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
		<br><br><br>
		<table class='mainstream'>
			<tr>
				<th colspan='2' id='thtitulo'>Tipo Tarifa Otas: <?=$usuario->GetTipoTarifa($db1,false,$_GET['id'])->Fields('tt_nombre');?></th>
			</tr>
			<?
			$clientCounter = 0;
			foreach($clientes as $idcliente => $cliente){
				$datos = $usuario->GetTipoTarCliente($db1,$cliente['bd'],false);

				$htmlLista="";
				$htmlCarga="";
				while (!$datos->EOF){
					if($datos->Fields('tt_estado')==0){
						$color = "style='background-color: #80bf83';";
					}else{
						$color = "style='background-color: #fd9f98';";
					}
					if($datos->Fields('id_tipotar')==$_GET['id']){
						$htmlCarga.="<option ondblclick=remove('".$cliente['bd']."') value='".$datos->Fields('id_tipotarifa')."' $color>".utf8_encode($datos->Fields('tt_nombre'))."</option>";
					}
					$htmlLista.="<option ondblclick=add_this(this.value,'".$cliente['bd']."') value='".$datos->Fields('id_tipotarifa')."' $color>".utf8_encode($datos->Fields('tt_nombre'))."</option>";
					

					$datos->MoveNext(); 
				}
				if(!$clientCounter & 1){
					echo "<tr>";
				}
				?>
				<td>
					<table class='mainstream2'>
						<tr>
							<th colspan='2' id='thtitulo'>Cliente <?=$cliente['name'];?></th>
						</tr>
						<tr>
							<th>Tipo Tarifa Cliente</th>
							<th>Tipo Tarifa a asociar</th>
						</tr>
						<tr>
							<td>
								
								<select name='lista_<?=$cliente['bd'];?>' id='lista_<?=$cliente['bd'];?>' multiple='multiple' style='width: 200px; height: 300px;'>
									<?=$htmlLista;?>
								</select>
							</td>
							<td>
								<select name='carga_<?=$cliente['bd'];?>' id='carga_<?=$cliente['bd'];?>' multiple='multiple' style='width: 200px; height: 300px;'>
									<?=$htmlCarga;?>
								</select>
							</td>
						</tr>
					</table>
				</td>
				<?
				if($clientCounter & 1){
				    echo '</tr>';
				}
				$clientCounter++;
			}
			?>
		</table>

		<center>
 			<button type="button" style="width:100px; height:27px" onclick=guardar()>&nbsp;Guardar</button>
			<button name="buscar" type="button" onClick="window.location='tipotar.php'" style="width:100px; height:27px">Cancelar</button>&nbsp;
 		</center>

			
		<form>
	</body>
</html>			